# Развертывание проекта на облачном сервере

*Так как в коде системы присутствуют элементы хардкода доступа к БД, то деплой проводится в несколько шагов. (Данная 
инструкция не является единственно верным способом развертывания системы.)*

## Этапы деплоя

* Копирование ветки мастер в произвольную директорию на облачном сервере. ("Деплой-инженер" может создать копию 
проекта в своей домашней директории на облачном сервере)
* Создание резервной копии текущего проекта. Проект размещается в директории /var/www/test_editorum. Рекомендуется 
перед развертыванием обновления сохранить текущую копию, для быстрого "отката" при необходимости. *Особое внимание: 
убедитесь, что обладаете соотв. правами для работы в диреткории /var/www.*
* Копирование проекта из временной диреткории в директорию размещения. (Например, из директории проекта выполнить 
команду '*cp -R /home/<deploy_username>/Editorum.dev/* .').
* Установка проекта. Выполнить команду: *composer install*. В процессе установки введите настройки доступа к MySQL 
(подробнее см. "Доступ к базам данных" - "MySQL").
* Создание твердых копий ресурсных файлов. Выполнить команду '*php app/console assets:install*'
* Добавить в конфигурацию проекта доступ к базе данных MongoDB. Подробнее см. "[Фрагменты конфигураций](#Фрагменты-конфигураций)" - "app/config/parameters.yml".
* Присвоить всем директориям и файлам проекта собственника и группу *www-data:www-data*.
* При первом развертывании (или при необходимости) произвести импорт данных из базы MySQL и базу MongoDB. [Импорт данных](import.md)
* Произвести настройку сортировки стран и областей. [Позиционирование стран и областей](
./src/Editorum/Bundle/Resources/doc/country_and_region_positioning.md)
* Выполнить инициализацию ролей командой:
```bash
php app/console doctrine:mongodb:fixtures:load --append
```
* Если операционная система LINUX, выставить необходимые права на папки кэша и логов:
```
HTTPDUSER=`ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`username`:rwX app/cache app/logs
sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`username`:rwX app/cache app/logs
```


## Доступ к системе по-умолчанию
### Администратор по-умолчанию
* *login* : super.admin@editorum.ru            
* *password* : _super3admin2editorum1ru_

## Фрагменты конфигураций
### app/config/parameters.yml
```php
    mongodb_database_host: <mongodb_host>
    mongodb_database_port: <mongodb_port>
    mongodb_database_name: <mongodb_database>
    mongodb_database_user: <mongodb_user>
    mongodb_database_password: <mongodb_user_password>
```