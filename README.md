# Editorum.dev
Editorum develop

Справка:
* [Регламент разработки проекта](regulations.md)
* [Установка проекта](installation.md)
* [Развертывание проекта](deploy.md)
* [Импорт данных](import.md)
* [Позиционирование стран и областей](./src/Editorum/Bundle/Resources/doc/country_and_region_positioning.md)
* [Таблица роутов (путей) проекта](./project_routes.md)
* [Загрузка файлов в системе](./src/Editorum/Bundle/Resources/doc/file_storage.md)
* [Использование генератора АПИ](./src/Asu/ApiBundle/Resources/doc/index.md)
