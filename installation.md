# Для установки проекта  :
1)	Установить компоненты
* Gitlab/git
* gitgui (для windows)
* mysql 5.3
* php 5.6
* php storm
* pecl
* php расширение для mongoDB (http://php.net/manual/ru/set.mongodb.php)
* composer  https://getcomposer.org/ 
* mongoDB 2.6 иливыше
* Установить mongoDB по инструкцииhttp://devcolibri.com/2091
* Установить плагин phpmongoexplorerили приложение robomongo для работы с БД

2)	Настройка проекта
*	Запускаем php storm
*	Выбираем Chekoutfromwersioncontrol
*	Указываем в поле GitReposytoryURLhttps://gitlab.com/infra-m/Editorum.dev.git
*	Parentdirectory (на свое усмотрение)
*	Directoryname(на свое усмотрение)

http://joxi.ru/Dr81RzntYE3wm6

*	Запускаем checkout
*	Запускаем

```bash
composer update
```
или
  
```bash
composer install
```
*	Впроцессе update/ install composer система создает файл `parameters.yml`
(Creating the “app/config/parameters.yml” file)
При создании файла, будет предложено указать некоторые параметры
(Some parameters are missing. Please provide them)
Нажимаем enter, для установки параметров по умолчанию
> Только в поле secret: Указываем любой ключ (1 или 123 и т.п.)

*	Послеupdate/ installcomposerсохранитьвКорень:\проект\web папку jsons_asu
Во вложении (jsons_asu.rar)
*	Запуститькоманду
```bash
php app/console do:mo:fi:lo
```
*	Установить базу Корень:\mongodb\bin\dump
Во вложении (editorum.rar)
*	Корень:\mongodb\binзапусить  mongorestore.exe

Запустить проект
```bash
start php app/console server:run
```

Для выбора среды 
```bash
startphp app/console server:run --env=dev
```
```bash
startphp app/console server:run --env=prod
```

Для чистки кэша
```bash
php app/console cache:clear
```

Для ассетс:
```bash
php app/console assets:install
```













