<?php
namespace UserBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Editorum\Bundle\Document\ContactType as ContactTypeDocument;
use Symfony\Component\Yaml\Yaml;

class ContactType extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     * @throws \Doctrine\ODM\MongoDB\LockException
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $types = $this->getDefaultContactTypes();
        foreach ($types as $record) {
            $type = $manager->getRepository('AsuBundle:ContactType')->findOneBy([
                'code'  => $record['code']
            ]);

            if ($type === null) {
                $type = new ContactTypeDocument();
                $type->setCode($record['code']);
            }
            
            $type->setName($record['name']);
            $type->setDescription($record['description']);
            
            $manager->persist($type);
        }

        $manager->flush();
    }

    public function getDefaultContactTypes()
    {
        $types = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'Data' . DIRECTORY_SEPARATOR . 'contact_type.yml');
        return Yaml::parse($types);
    }

    public function getOrder()
    {
        return 0;
    }
}
