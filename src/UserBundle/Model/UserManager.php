<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 11.02.16
 * Time: 15:25
 */

namespace UserBundle\Model;

use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserManager implements UserManagerInterface, UserProviderInterface
{
    /**
     * Creates an empty user instance.
     *
     * @return \FOS\UserBundle\Model\UserInterface
     */
    public function createUser()
    {
        // TODO: Implement createUser() method.
    }

    /**
     * Deletes a user.
     *
     * @param \FOS\UserBundle\Model\UserInterface $user
     *
     * @return void
     */
    public function deleteUser(\FOS\UserBundle\Model\UserInterface $user)
    {
        // TODO: Implement deleteUser() method.
    }

    /**
     * Finds one user by the given criteria.
     *
     * @param array $criteria
     *
     * @return \FOS\UserBundle\Model\UserInterface
     */
    public function findUserBy(array $criteria)
    {
        // TODO: Implement findUserBy() method.
    }

    /**
     * Find a user by its username.
     *
     * @param string $username
     *
     * @return \FOS\UserBundle\Model\UserInterface or null if user does not exist
     */
    public function findUserByUsername($username)
    {
        // TODO: Implement findUserByUsername() method.
    }

    /**
     * Finds a user by its email.
     *
     * @param string $email
     *
     * @return \FOS\UserBundle\Model\UserInterface or null if user does not exist
     */
    public function findUserByEmail($email)
    {
        // TODO: Implement findUserByEmail() method.
    }

    /**
     * Finds a user by its username or email.
     *
     * @param string $usernameOrEmail
     *
     * @return \FOS\UserBundle\Model\UserInterface or null if user does not exist
     */
    public function findUserByUsernameOrEmail($usernameOrEmail)
    {
        // TODO: Implement findUserByUsernameOrEmail() method.
    }

    /**
     * Finds a user by its confirmationToken.
     *
     * @param string $token
     *
     * @return \FOS\UserBundle\Model\UserInterface or null if user does not exist
     */
    public function findUserByConfirmationToken($token)
    {
        // TODO: Implement findUserByConfirmationToken() method.
    }

    /**
     * Returns a collection with all user instances.
     *
     * @return \Traversable
     */
    public function findUsers()
    {
        // TODO: Implement findUsers() method.
    }

    /**
     * Returns the user's fully qualified class name.
     *
     * @return string
     */
    public function getClass()
    {
        // TODO: Implement getClass() method.
    }

    /**
     * Reloads a user.
     *
     * @param \FOS\UserBundle\Model\UserInterface $user
     *
     * @return void
     */
    public function reloadUser(\FOS\UserBundle\Model\UserInterface $user)
    {
        // TODO: Implement reloadUser() method.
    }

    /**
     * Updates a user.
     *
     * @param \FOS\UserBundle\Model\UserInterface $user
     *
     * @return void
     */
    public function updateUser(\FOS\UserBundle\Model\UserInterface $user)
    {
        // TODO: Implement updateUser() method.
    }

    /**
     * Updates the canonical username and email fields for a user.
     *
     * @param \FOS\UserBundle\Model\UserInterface $user
     *
     * @return void
     */
    public function updateCanonicalFields(\FOS\UserBundle\Model\UserInterface $user)
    {
        // TODO: Implement updateCanonicalFields() method.
    }

    /**
     * Updates a user password if a plain password is set.
     *
     * @param \FOS\UserBundle\Model\UserInterface $user
     *
     * @return void
     */
    public function updatePassword(\FOS\UserBundle\Model\UserInterface $user)
    {
        // TODO: Implement updatePassword() method.
    }


    public function loadUserByUsername($username)
    {
        // TODO: Implement loadUserByUsername() method.
    }

    public function refreshUser(UserInterface $user)
    {
        // TODO: Implement refreshUser() method.
    }

    public function supportsClass($class)
    {
        // TODO: Implement supportsClass() method.
    }

}