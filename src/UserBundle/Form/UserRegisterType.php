<?php
namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserRegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('surname', 'text', [
                'attr' => [
                    'class' => 'form-control',
                    'id' => 'surname',
                    'placeholder' => 'Фамилия'
                ]
            ])
            ->add('name', 'text', [
                'attr' => [
                    'class' => 'form-control',
                    'id' => 'name',
                    'placeholder' => 'Имя'
                ]
            ])
            ->add('patronymic', 'text', [
                'attr' => [
                    'class' => 'form-control',
                    'id' => 'patronymic',
                    'placeholder' => 'Отчество'
                ]
            ])
            ->add('email', 'email', [
                'attr' => [
                    'class' => 'form-control',
                    'id' => 'email',
                    'placeholder' => 'Email'
                ]
            ])
            ->add('cell', 'text', [
                'attr' => [
                    'class' => 'form-control',
                    'id' => 'cell',
                    'placeholder' => 'Введите номер мобильного телефона'
                ]
            ])
            ->add(
                'plainPassword',
                'repeated',
                [
                    'type' => 'password',
                    'first_options'  => [
                        'label' => 'Password',
                        'attr' => [
                            'class' => 'form-control',
                            'id' => 'password',
                            'placeholder' => 'Введите пароль'
                        ]
                    ],
                    'second_options' => [
                        'label' => 'Repeat Password',
                        'attr' => [
                            'class' => 'form-control',
                            'id' => 'password_again',
                            'placeholder' => 'Повторите пароль'
                        ]
                    ],
                ]
            )
            ->add('register', 'submit', [
                'attr' => [
                    'class' => 'btn btn-primary'
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }

    public function getName()
    {
        return 'register';
    }
    
}
