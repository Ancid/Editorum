<?php
namespace UserBundle\Controller;

use Editorum\Bundle\Controller\BasicController;

class LoginController extends BasicController
{
    public function loginAction()
    {
        if (!$this->isGranted('IS_AUTHENTICATED_ANONYMOUSLY')) {
            return $this->redirectToRoute('homepage');
        }

        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();

        if ($error !== null) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans(
                $error->getMessageKey(),
                $error->getMessageData(),
                'security'
            ));
        }

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $csrfToken = $this->container->get('security.csrf.token_manager')->getToken('authenticate');

        return $this->render('UserBundle:Login:login.html.twig', array(
            'last_username' => $lastUsername,
            'csrf_token' => $csrfToken,
        ));
    }

    /**
     * Logout user.
     */
    public function logoutAction()
    {
    }
}
