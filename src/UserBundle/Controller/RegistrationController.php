<?php
namespace UserBundle\Controller;

use Editorum\Bundle\Document\Contact;
use Editorum\Bundle\Document\User;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Editorum\Bundle\Controller\BasicController;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use UserBundle\Form\UserRegisterType;
use FOS\UserBundle\Model\UserInterface;

class RegistrationController extends BasicController
{
    /**
     * @Route("/register", name="user_registration")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_ANONYMOUSLY')) {
            $form = $this->createForm(new UserRegisterType(), null);

            $form->handleRequest($request);

            $authUser = false;

            $response = $this->redirectToRoute('login');

            if ($request->get('agreement') !== null || empty($request->get('agreement'))) {
                $this->addFlash('danger', 'Необходимо согласиться с условиями пользовательского соглашения!');
            }

            if ($form->isSubmitted() && $form->isValid() && $request->get('agreement')) {
                $user = new User();
                if ($this->processForm($form, $user)) {
                    $this->addFlash(
                        'notice',
                        'Ваш аккаунт успешно зарегистрирован. <br />'
                        . 'На Ваш почтовый ящик было отправлено письмо со ссылкой для его активации.'
                    );

                    if ($authUser) {
                        $response = $this->redirectToRoute('homepage');
                        $this->authenticateUser($user, $response);
                    }

                    return $response;
                }
            }

            return $this->render(
                '@User/Registration/register.html.twig',
                [ 'form' => $form->createView() ]
            );
        }

        return $this->redirectToRoute('fos_user_profile_show');
    }

    /**
     * @param Form $form
     * @return bool
     */
    protected function processForm(Form $form, User &$user)
    {
        $dm = $this->get('doctrine.odm.mongodb.document_manager');
        $errors = [];
        
        // Create User
        $email = $form->get('email')->getData();

        $password = $this->get('security.password_encoder')
            ->encodePassword($user, $form->get('plainPassword')->getData());

        $user
            ->setUsername($email)
            ->setEmail($email)
            ->setPassword($password)
            ->setEnabled(false)
            ->setFirstName($form->get('name')->getData())
            ->setSecondName($form->get('patronymic')->getData())
            ->setLastName($form->get('surname')->getData())
        ;

        $dm->persist($user);

        $errors = $this->get('validator')->validate($user);

        // Create contacts by User
        foreach (['email', 'cell'] as $code) {
            if (count($errors) == 0) {
                $value = $form->get($code)->getData();
                $type = $dm->getRepository('AsuBundle:ContactType')->findOneBy(['code' => $code]);
                if ($value && $type) {
                    $contact = new Contact();
                    /** @var \Editorum\Bundle\Document\ContactType $type */
                    $contact->setType($type);
                    $contact->setUser($user);
                    $contact->setValue($value);
                    $dm->persist($contact);

                    $errors = $this->get('validator')->validate($contact);
                }
            }
        }

        if (count($errors) > 0) {
            foreach ($errors as $error) {
                /** @var \Symfony\Component\Validator\ConstraintViolation $error */
                if ($field = $form->get($error->getPropertyPath())) {
                    $field->addError(new FormError($error->getMessage()));
                } else {
                    $form->addError(new FormError($error->getMessage()));
                }
            }
            return false;
        }

        $dm->flush();

        $this->onSuccess($user, true);

        return true;
    }

    /**
     * Authenticate a user with Symfony Security
     *
     * @param \FOS\UserBundle\Model\UserInterface $user
     * @param \Symfony\Component\HttpFoundation\Response $response
     */
    protected function authenticateUser(UserInterface $user, Response $response)
    {
        try {
            $this->container->get('fos_user.security.login_manager')->loginUser(
                $this->container->getParameter('fos_user.firewall_name'),
                $user,
                $response
            );
        } catch (AccountStatusException $ex) {
            // We simply do not authenticate users which do not pass the user
            // checker (not enabled, expired, etc.).
        }
    }

    /**
     * @param boolean $confirmation
     */
    protected function onSuccess(UserInterface $user, $confirmation)
    {
        if ($confirmation) {
            $user->setEnabled(false);
            if (null === $user->getConfirmationToken()) {
                $user->setConfirmationToken($this->get('fos_user.util.token_generator')->generateToken());
            }

            $this->get('fos_user.mailer')->sendConfirmationEmailMessage($user);
        } else {
            $user->setEnabled(true);
        }

        $this->get('fos_user.user_manager')->updateUser($user);
    }

    /**
     * Receive the confirmation token from user email provider, login the user
     */
    public function confirmAction($token)
    {
        $user = $this->container->get('fos_user.user_manager')->findUserByConfirmationToken($token);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with confirmation token "%s" does not exist', $token));
        }

        $user->setConfirmationToken(null);
        $user->setEnabled(true);
        $user->setLastLogin(new \DateTime());

        $this->container->get('fos_user.user_manager')->updateUser($user);

        $this->addFlash('success', 'Аккаунт успешно активирован!');

        $response = $this->redirectToRoute('homepage');
        $this->authenticateUser($user, $response);

        return $response;
    }
}
