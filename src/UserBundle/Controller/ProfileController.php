<?php
namespace UserBundle\Controller;

use Editorum\Bundle\Controller\BasicController;

class ProfileController extends BasicController
{
    public function indexAction()
    {
        if (!$this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('homepage');
        }

        return $this->render(
            '@User/Profile/index.html.twig'
        );
    }
}
