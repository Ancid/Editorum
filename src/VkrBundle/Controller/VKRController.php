<?php
/**
 * Created by PhpStorm.
 * User: vgfilippov
 * Date: 20.11.15
 * Time: 14:01
 */

namespace VkrBundle\Controller;

use DocumentBundle\Controller\DocumentController;
use DocumentBundle\FileStorage\FileSystemStorage;
use DocumentBundle\Model\Document;
use Editorum\Bundle\Logic\Model;
use Editorum\Bundle\Logic\Universities;
use Editorum\Bundle\Services\GlobalMenus;
use Symfony\Component\HttpFoundation\Request;
use VkrBundle\Logic\Vkr;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use VkrBundle\Model\VkrModel;
use Symfony\Component\HttpFoundation\File\File;
use \simple_html_dom;

class VKRController extends Controller
{

    public function pageAction()
    {
        $authChecker = $this->container->get('security.authorization_checker');

        if ($authChecker->isGranted('ROLE_USER')) {

            $user = $this->getUser();

            $vkrs = Model::getByKeys(new Vkr(), array('university_id' => (int)$user->getUniversity()));
            $university = Model::getById(new Universities(), $user->getUniversity());
//            $vkr_count = iterator_count($vkrs);
            return $this->render('@Vkr/Default/vkr.html.twig', array(
                'id'            => $user->getId(),
                'university'    => $university->__get('title'),
                'vkrs'          => $vkrs,
                'username'      => $user->getUsername()
            ));
        } else {
            return $this->redirectToRoute('vkr_login', [], 301);
        }

    }


    public function editAction($id)
    {
        $authChecker = $this->container->get('security.authorization_checker');

        if ($authChecker->isGranted('ROLE_USER')) {
            $user = $this->getUser();

            $object = new Vkr();

            //Проверяем есть ли "права" у пользователя на текущий ВКР
            if ((int)$id > 0 && iterator_count($object->userHasVkr($user->getUniversity(), $id)) > 0) {
                $vkr = Model::getById(new Vkr(), (int)$id);
                return $this->render('@Vkr/Default/vkr_edit.html.twig', array(
                    'vkr'       => $vkr,
                    'username'  => $user->getUsername()
                ));
            }
            return $this->redirectToRoute('vkr_page', [], 301);
        } else {
            return $this->redirectToRoute('vkr_login', [], 301);
        }
    }


    public function saveAction($id = 0)
    {
        $data = json_decode($this->get('request')->request->get('data'), true);
        $vkr_id = Model::update(new Vkr(), $data);

        return new JsonResponse(json_encode(array('id' => $vkr_id)));
    }


    public function removeAction()
    {
        $data = json_decode($this->get('request')->request->get('data'), true);
        $res = Model::remove(new Vkr(), $data);

        return new JsonResponse(json_encode(array('res' => $res)));
    }


    public function uploadAction(Request $request)
    {
        $response = [
            'success'   => true,
            'data'      => null
        ];

        $authChecker = $this->container->get('security.authorization_checker');

        if ($authChecker->isGranted('ROLE_USER')) {
            try
            {
                $user_id = $this->getUser()->getId();
                $target_directory_name = 'documents' . DIRECTORY_SEPARATOR . $user_id . DIRECTORY_SEPARATOR . 'vkr';

                $form = $request->files->get('form');
                $fileStorage = new FileSystemStorage($target_directory_name);
                foreach ($form['files'] as $file) {
                    $file_data = $fileStorage->upload($file);

                    // ToDo: add UserID into DB behavior
                    $file_data['user_id'] = $user_id;

                    $file_data = array_merge($file_data, array('entity' => 'vkr', 'entity_id' => null));
                    $file_data['_id'] = Model::insert(new Document(), $file_data);

                    if (strpos($file_data['mimetype'], 'application') !== false ) {

                        $_file_arch = new File($file_data['fullpath']);
                        $_decompress_files_array = $fileStorage->unArch($_file_arch, $target_directory_name);
                        if (array_key_exists('error', $_decompress_files_array)) {
                            $response = [
                                'success' => false,
                                'error' => $_decompress_files_array['error']
                            ];
                        } else {
                            $response = [
                                'success'   => true,
                                'files'     => []
                            ];
                            foreach ($_decompress_files_array as $_file) {
                                $_file_data = array_merge($_file, array('entity' => 'vkr', 'entity_id' => null));
                                $_file_data['_id'] = Model::insert(new Document(), $_file_data);
                                $response['files'][] = $_file_data;
                            }
                            Model::remove(new Document(), array('_id' => $file_data['_id']));
                            $fileStorage->removeFile($_file_arch);
                        }
                    } else {
                        $response['data'][] = $file_data;
                    }
                }


                if (array_key_exists('files', $response)) {
                    foreach ($response['files'] as $file) {
                        VkrModel::parseTitleList(8, $file['dirname'], $file['filename'], $file['originalName']);
                    }
                }


            } catch(\Exception $e) {
                return $response = [
                    'success' => false,
                    'error' => "Error!: " . $e->getMessage() . "<br/>"
                ];
            }
        } else {
            $response['error'] = 'Access denied! Go away...';
        }


        return new Response(json_encode($response));
    }












}