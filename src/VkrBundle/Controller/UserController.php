<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 08.02.2016
 * Time: 14:29
 */

namespace VkrBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class UserController extends Controller
{
    public function adduserAction()
    {
        $authChecker = $this->container->get('security.authorization_checker');

//        if ($authChecker->isGranted('ROLE_ADMIN')) {
                $user = $this->getUser();
//        }

        return $this->render('@Vkr/Default/create_user.html.twig', array(
            'username'  => $user->getUsername()
        ));
    }

    public function createAction(Request $request)
    {
        var_dump($request->get('add_user'));
        die();

        $user = $this->userManager->createUser();
        $user->setUsername($username);
        $user->setEmail($email);
        $user->setPlainPassword($password);
        $user->setEnabled((Boolean) $active);
        $user->setSuperAdmin((Boolean) $superadmin);
        $user->setLatitude($latitude);
        $this->userManager->updateUser($user);

        return $user;
    }
}