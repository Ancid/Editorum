<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 26.01.2016
 * Time: 11:51
 */

namespace VkrBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;

class LoginController extends Controller
{

    public function loginAction()
    {
        $authChecker = $this->container->get('security.authorization_checker');

        if ($authChecker->isGranted('ROLE_USER')) {
//            $user = $this->container->get('fos_user.user_manager')->findUserByEmail('user@a.ru');
//            var_dump($user);
            return $this->redirectToRoute('vkr_page', ['id' => 1], 301);
        }


        $request = $this->container->get('request');
        /* @var $request \Symfony\Component\HttpFoundation\Request */
        $session = $request->getSession();
        /* @var $session \Symfony\Component\HttpFoundation\Session\Session */

        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } elseif (null !== $session && $session->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = '';
        }

        if ($error) {
            // TODO: this is a potential security risk (see http://trac.symfony-project.org/ticket/9523)
            $error = $error->getMessage();
        }
        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get(SecurityContext::LAST_USERNAME);

        $csrfToken = $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate');

        return $this->container->get('templating')->renderResponse('@Vkr/Default/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
            'csrf_token'    => $csrfToken,
        ));
    }


    /**
     * Logout user
     *
     */
    public function logoutAction() {
        try {
            $this->get("request")->getSession()->invalidate();
            $this->get("security.context")->setToken(null);
            return $this->redirectToRoute('vkr_login', [], 301);
        } catch (\Exception $e) {
            return false;
        }
    }
}