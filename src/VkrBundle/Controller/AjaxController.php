<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 28.01.2016
 * Time: 11:54
 */
namespace VkrBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use VkrBundle\Logic\Vkr;

class AjaxController extends Controller
{

    public function batchremoveAction()
    {
        $request = $this->get('request')->request->all();
        try
        {
            $response = [
                'success' => true
            ];
            $obj = new Vkr();
            foreach ($request['vkrs'] as $id) {
                $obj->remove(array(
                   '_id' => $id
                ));
            }
//
        } catch(Exception $e) {
            return $response = [
                'success' => false,
                'error' => "Error!: " . $e->getMessage() . "<br/>"
            ];
        }
        return new Response(json_encode($response));
    }

}