<?php


/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 25.01.2016
 * Time: 16:51
 */
namespace VkrBundle\Document;;

use FOS\UserBundle\Document\User as BaseUser;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="users")
 */
class User extends BaseUser
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @MongoDB\String(name="university_id")
     */
    protected $university_id = '';

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }


    public function getUniversity()
    {
        return $this->university_id;
    }
}
