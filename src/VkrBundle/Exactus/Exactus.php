<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 21.01.2016
 * Time: 15:53
 */

namespace VkrBundle\Exactus;

use DateTime;
use Exception;

class Exactus
{
    const REQ_PLAG = 'http://search.znanium.com:80/cgi-bin/plagsearch.cgi?PROTOCOL=JSONRPC';

    const TYPE_GET_SIZE         = "get_size";
    const TYPE_SEARCH_PLAG      = "search_plag";

    protected $requestObject = array(
        "jsonrpc"           => "2.0",
        "method"            => "",
        "params"            => array(),
        "id"                => ""
    );

    protected $result = false;

    protected $filename = false;

    protected $text = false;

    protected $id = "";

    protected $searchPlagParams = array(
        "COLLECTIONS"               => [31, 2000, 2010, 2031, 2032, 2033, 2039, 2060, 3000],
        "SEARCH_MODULES"            => ["internal"],
        "TOP_SENTS_PERCENT"         => 0.5,
        "MAX_SENTS_COUNT"           => 3000,
        "MIN_SENTS_COUNT"           => 5,
        "MIN_SENT_WEIGHT"           => 0.1,
        "MAX_WORDS_PER_SENT"        => 72,
        "MIN_WORDS_PER_SENT"        => 10,
        "MAX_TRASH_RATE_PER_SENT"   => 0.4,
        "SEARCH_AMONG_SIMILAR"      => 1,
        "TOP_PERCENT"               => 0.0055,
        "MAX_WORDS_COUNT"           => 40,
        "MIN_WORDS_COUNT"           => 10,
        "MIN_SIM"                   => 0.05,
        "MAX_DOCS_COUNT"            => 2000,
        "MIN_COMMON_RATING"         => 0.56,
        "MIN_BEST_RSENT_IDF_COVER"  => 0.56,
        "MIN_SENT_IDF_PERCENT"      => 0.56,
        "FORM_PENALTY_FACTOR"       => 0.8,
        "W_IDF_COVER"               => 0.3,
        "W_TFIDF"                   => 0.2,
        "W_SYNT"                    => 0.45,
        "W_RELS"                    => 0.025,
        "W_ROLES"                   => 0.025,
        "TOTAL_RATE_CALC_KIND"      => "simple",
        "DOC_RATE_CALC_KIND"        => "simple",
        "MIN_SOURCES_COUNT_PER_DOC" => 1,
        "MIN_DOC_RATING"            => 0.0,
        //            "CHUNK_KINDS"               => [],
        "GROUP_DOCS"                => 0,
        "FILTER_BIB_REF"            => 1,
        "RETURN_BIB_REF"            => 0,
        "PUBLICATION_DATE"          => "2008",
        "DOC"                       => array(
            "FILENAME"      => false,
            "BODY"          => false
        )
    );

    protected $statParams = array(
        "COLLECTIONS"           => [31, 2000, 2010, 2031, 2032, 2033, 2039, 2060, 3000]
    );

    //Ответ:
    //"misuses_rating" – оценка степени заимствований.
    //"originality_rating" – оценка оригинальности документа.

    public function __construct($filename = false, $text = false)
    {
        if ($filename !== false && $text !== false) {
            $this->filename = $filename;
            $this->text = $text;

            $this->setSearchPlagDocument();
        }

        $now = new DateTime();
        $this->requestObject['id'] = md5($filename.$now->format('Y-m-d H:i:s'));
    }


    /**
     * Возвращаем объект для отправки
     * @return array
     * @throws Exception
     */
    public  function getRequestObject()
    {
        if (count($this->requestObject['params']) == 0)
            throw new Exception("Не заданы параметры запроса!");

        if ($this->requestObject['params']['DOC']['FILENAME'] == false || $this->requestObject['params']['DOC']['BODY'] == false)
            throw new Exception("Не указан текст или имя файла для запроса!");

        return $this->requestObject;
    }


    /**
     * Создаем параметры объекта для отправки
     * @param $type
     * @return mixed
     */
    public function setRequestParams($type)
    {
        $this->requestObject['method'] = $type;

        switch ($type) {
            case self::TYPE_GET_SIZE:
                $this->requestObject['params'] = $this->statParams;
                break;
            case self::TYPE_SEARCH_PLAG:
                $this->requestObject['params'] = $this->searchPlagParams;
                break;
        }
    }


    /**
     * Добавляет в параметры запроса данные о файле
     */
    protected function setSearchPlagDocument()
    {
        $this->searchPlagParams['DOC']['FILENAME'] = base64_encode($this->filename);
        $this->searchPlagParams['DOC']['BODY'] = base64_encode($this->text);
    }


    /**
     * Отправляет запрос на оценку оригинальности
     * @return mixed
     * @throws Exception
     */
    public function sendRequest()
    {
        if (strlen($this->requestObject['method']) == 0)
            throw new Exception("Не указан тип запроса!");

        setlocale(LC_ALL, 'ru_RU.UTF8');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, Exactus::REQ_PLAG);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->getRequestObject()));
        $data = curl_exec($ch);
        //        if(!curl_errno($ch)) {
        //            echo 'Received raw data' . $data;
        //        }
        curl_close($ch);

        $this->result = json_decode($data, true);

    }


    public function getOriginality()
    {
        if (!array_key_exists('result', $this->result))
            throw new Exception("Некорректный результат. Нельзя вычислит оригинальность");

        return round($this->result['result']['originality_rating']*100, 1);
    }

}