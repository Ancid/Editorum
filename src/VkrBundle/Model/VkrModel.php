<?php


/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 25.01.2016
 * Time: 15:34
 */
namespace VkrBundle\Model;

use Editorum\Bundle\Logic\Model;
use JournalsBundle\Docxtohtml\Docx_reader;
use VkrBundle\Logic\Vkr;
use VkrBundle\Exactus\Exactus;
use ZipArchive;
use \simple_html_dom;

class VkrModel
{

    /**
     * Пересобирает массив $_FILES
     * @param $file_post
     * @return array
     */
    public static function reArrayFiles(&$file_post) {

        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }


    public static function processFile($university_id, $uploaddir, $tmpdir, $filename_ext)
    {
        //        $link = $uploaddir.$filename;

        $tmplink = $tmpdir.$filename_ext;
        $uploadlink = $uploaddir.$filename_ext;
        $finfo = new \finfo(FILEINFO_MIME_TYPE);
        $mime = $finfo->file($tmplink);
        //        $mime = mime_content_type($tmplink);

        //Если архив, распаковывем
        if ($mime == "application/zip") {
            $zip = new ZipArchive();
            $res = $zip->open($tmplink);
            if ($res === TRUE) {
                for( $i = 0; $i < $zip->numFiles; $i++ ){
                    $stat = $zip->statIndex( $i );
                    if(strpos($stat['name'], '.docx') !== false) {
                        $zip->extractTo($uploaddir, array($stat['name']));
                        self::parseTitleList($university_id, $uploaddir, $stat['name']);
                    }
                }
                $zip->close();
            } else {
                //                echo 'failed, code:' . $res;
            }
        } elseif (in_array($mime, ["application/x-rar-compressed", "application/x-rar"])) {
            $rar_file = rar_open($tmplink) or die("Невозможно открыть RAR архив");
            $entries = rar_list($rar_file);
            foreach ($entries as $entry) {
                if(strpos($entry->getName(), '.docx') !== false) {
                    $entry->extract($uploaddir);
                    self::parseTitleList($university_id, $uploaddir, $entry->getName());
                }
            }
            rar_close($rar_file);
        } elseif ($mime == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
            copy($tmplink, $uploadlink);
            $filename = explode('.', $filename_ext);

            self::parseTitleList($university_id, $uploaddir, $filename_ext);
        }
    }


    /**
     * парсит титульный лист документа docx, кладет в БД
     * @param $university_id
     * @param $filedir
     * @param $filename
     * @param bool $origin_filename
     */
    public static function parseTitleList($university_id, $filedir, $filename, $origin_filename = false)
    {
//        $filename = basename($filename_ext, '.docx');
        $filename = basename($filename);

        $doc = new Docx_reader();
        $doc->setFile($filedir . DIRECTORY_SEPARATOR . $filename);
        //        $html = $doc->to_html();
        $plain_text = $doc->to_plain_text();

        //Проверка на оригинальность
        $exactus = new Exactus($filename, $plain_text);
        $exactus->setRequestParams(Exactus::TYPE_SEARCH_PLAG);
        $exactus->sendRequest();
        $origin = $exactus->getOriginality();


//        $fp = fopen($filedir.$filename.".txt", "w");
//        fwrite($fp, $plain_text);
//        fclose($fp);

        //обрезаем текст для поиска до 1500 символов, титульный лист +-700
        $search_text = substr($plain_text, 0, 1500);

        preg_match('/(.+?)Институт /', $search_text, $organization);
        preg_match('/на тему: «(.+?)»/', $search_text, $title);
        preg_match('/Кафедра (.+?)УТВЕРЖДАЮ/', $search_text, $department);

        preg_match('/Студент группы (.+?)\/ /', $search_text, $group);
        preg_match('/Студент группы .*\/ (.+?)\(подпись\)\(Ф\.И\.О\.\)Руководитель/', $search_text, $author);
        preg_match('/степ.уч. звание\/ (.+?)\(подпись\)/', $search_text, $head);

        //Проверяем магистер или бакалавр
        if (strpos($search_text, 'Квалификация (степень):') === false) {
            //магистр
            //            preg_match('/Магистерская программа: (.+?) Студент группы', $search_text, $direction);
            preg_match('/Направление: (.+?)Магистерская программа:/', $search_text, $direction);
        } else {
            preg_match('/Направление: (.+?)Квалификация/', $search_text, $direction);
            //            preg_match('/Квалификация (степень): (.+?) Студент группы/', $search_text, $direction);
        }


        $vkr['university_id'] = (int)$university_id;
        $vkr['title'] = $title[1];
        $vkr['original'] = $origin;
        $vkr['organization'] = $organization[1];
        $vkr['direction'] = $direction[1];
        $vkr['department'] = $department[1];
        $vkr['author'] = $author[1];
        $vkr['group'] = $group[1];
        $vkr['head'] = $head[1];

        self::addVkr($vkr);
    }


    /**
     * Получает массив с метаданными ВКР и сохраняет в БД
     * @param $vkr
     */
    public static function addVkr($vkr)
    {
        Model::update(new Vkr(), $vkr);
    }

}