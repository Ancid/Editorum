<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 19.01.2016
 * Time: 16:59
 */

namespace VkrBundle\Logic;

use Editorum\Bundle\Logic\Model;
use Editorum\Bundle\Logic\MongoNode;


class Vkr extends MongoNode
{

    public function createEmpty()
    {
        $obj = array(
            'title'                  => '',
            'university_id'          => false,
            'path'                   => '',
            'organization'           => '',
            'department'             => '',
            'reviewer'               => '',
//            'head_department'        => '',
            'direction'              => '',
            'author'                 => '',
            'group'                  => '',
            'head'                   => '',
            'consultant'             => '',
            'original'               => 100
        );

        return $obj;
    }


    /**
     * Возвращает массив ВКР загруженных порльзователем
     * @param int $user_id
     * @return array
     */
    public static function uploadByUser($user_id = 0)
    {
        if ($user_id == 0)
            return array();
        else
            $find = array('user_id' => (int)$user_id);

        $db = self::getDB();
        $cursor = $db->vkr->find($find);
        $ar = array();

        foreach ($cursor as $j) {
            $ar[] = $j;
        }

        return $ar;
    }


    /**
     * Проверка видит ли пользователь конкретный ВКР
     * @param $university_id
     * @param $vkr_id
     * @return mixed
     */
    public function userHasVkr($university_id, $vkr_id)
    {
        $vkr = Model::getByKeys(new Vkr, array(
            'university_id' => (int)$university_id,
            '_id' => (int)$vkr_id
        ));

        return $vkr;
    }
}