<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 26.01.16
 * Time: 15:23
 */

namespace DocumentBundle\Model;

use Exception;
use Editorum\Bundle\Logic\MongoNode;
use VkrBundle\Document\User;

/**
 * Class Document
 * @package Editorum\Bundle\Logic
 */

class Document extends MongoNode {

    const STORAGE_LOCAL = 'local';

    /**
     * Create empty document instance.
     *
     * @return array
     */
    public function createEmpty() {
        $obj = array(
            'name'          => '',
            'user_id'       => '',
            'entity'        => '',
            'entity_id'     => '',
            'revision'      => 1,
            'storage'       => self::STORAGE_LOCAL,
            'fullpath'      => '',
            'dirname'       => '',
            'filename'      => '',
            'extension'     => '',
            'mimetype'          => '',
            'size'          => 0,
            'errors'        => '',
            'created_at'    => '',
            'updated_at'    => '',
            // These fields are not yet used
//            'created_by' => null,
//            'updated_by' => null,
        );
        return $obj;
    }

    /**
     * Get document owner
     */
    public function getUser()
    {
        // ToDo: write method after install User model
        return null; //Model::getById(new User(), $this->user_id);
    }

    /**
     * Set owner by this document
     *
     * @param User $user
     */
    public function setUser(User $user) {
        $this->user_id = $user->getId();
    }

    /**
     * Set entity of the document
     *
     * @param $entity
     * @param $entity_id
     */
    public function setEntity($entity, $entity_id) {
        if ($entity =! '' && !is_null($entity) && is_int($entity_id) && $entity_id >= 0) {
            // ToDo: add a test for the existence of class spirit and essence of ID reservation
            $this->entity = $entity;
            $this->entity_id = $entity_id;
        }
    }


    /**
     * Parse filename and fill the array elements
     *
     * @param array $obj
     * @param $filepath
     * @return array|bool
     */
    public function parseFilepath(Array $obj, $filepath) {
        if (empty($obj) || empty($filepath)) {
            return false;
        }
        try {
            $path_info = pathinfo($filepath);
            if (array_key_exists('dirname', $path_info))
                $obj['dirname'] = $path_info['dirname'];
            if (array_key_exists('filename', $path_info))
                $obj['filename'] = $path_info['filename'];
            if (array_key_exists('extension', $path_info))
                $obj['extension'] = $path_info['extension'];
        } catch (Exception $e) {
            $obj['error'] = $e->getMessage();
        }
        return $obj;
    }

    /**
     * Calculate size of file
     */
    public function setSize() {
        if (!is_null($this->fullpath)) {
            try {
                clearstatcache(true, $this->fullpath);
                $this->size = filesize($this->fullpath);
            } catch (Exception $e) {
                $this->errors[] = $e->getMessage();
            }
        }
    }

    /**
     * Get MIME-type of the file
     *
     * @param array $obj
     * @return array|bool
     */
    public function getMimeType(Array $obj) {
        if (!array_key_exists('filepath', $obj) || !array_key_exists('filename', $obj) || !array_key_exists('extension', $obj)) {
            return false;
        }
        $fullpath = $obj['filepath'] . DIRECTORY_SEPARATOR . $obj['filename'] . DIRECTORY_SEPARATOR . $obj['extension'];
        try {
            $finfo = new \finfo(FILEINFO_MIME_TYPE);
            $obj['mimetype'] = $finfo->file($fullpath);
        } catch (Exception $e) {
            $obj['error'] = $e->getMessage();
        }

        return $obj;
    }

    /**
     * Get documents list
     *
     * @param array $query
     * @return array
     */
    public static function getList($query = array())
    {
        $db = self::getDB();
        $collection = $db->document->find($query);
        $result = array();
        foreach ($collection as $item)
        {
            $result[] = $item;
        }

        return $result;
    }

}