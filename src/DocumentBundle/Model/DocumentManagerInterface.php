<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 08.02.16
 * Time: 16:50
 */

namespace DocumentBundle\Model;

use DocumentBundle\Model;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

interface DocumentManagerInterface
{
    /**
     *
     *
     * @param Request $request
     * @param Form $form
     * @return array
     */
//    public function createDocument(Request $request, Form $form);

    /**
     *
     *
     * @param array $document_data
     * @return array
     */
    public function createDocument(Array $document_data);

    /**
     *
     *
     * @param Request $request
     * @param Form $form
     * @return array
     */
//    public function updateDocument(Request $request, Form $form);

    /**
     *
     *
     * @param array $document_data
     * @return array
     */
    public function updateDocument(Array $document_data);

    /**
     *
     *
     * @param integer $document_id
     * @return bool
     */
    public function deleteDocument($document_id);

    /**
     *
     *
     * @param Request $request
     * @param Form $form
     * @return array
     */
    public function uploadDocument(Request $request, Form $form);

    /**
     *
     *
     * @param array $document_data
     * @return array
     */
    public function compressDocument(Array $document_data);

    /**
     *
     *
     * @param Request $request
     * @return array
     */
    public function decompressDocument(Array $document);

}