<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 08.02.16
 * Time: 16:51
 */

namespace DocumentBundle\Model;

use DocumentBundle\FileStorage\FileSystemStorage;
use Editorum\Bundle\Logic\Model;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Model\User;

class DocumentManager implements DocumentManagerInterface
{
    /**
     * @var \DocumentBundle\FileStorage\FileSystemStorage
     */
    protected $fileStorage;

    /**
     * @var
     */
    protected $userId;

    /**
     * @var
     */
    protected $entity_name;

    /**
     * @var
     */
    protected $entity_id;

    /**
     * DocumentManager constructor.
     *
     * @param User $user
     * @internal param ContainerInterface $container
     */
    public function __construct(User $user = null)
    {
        $this->userId = null;
        if ($user)
            $this->userId = $user->getId();
        $this->fileStorage = new FileSystemStorage();
        $this->entity_name = 'document';
        $this->entity_id = null;
    }

    public function setDefaultStorageDirectory($directory_name)
    {
        $this->fileStorage->setDir($directory_name);
        return $this;
    }

    public function setDefaultUser(User $user)
    {
        $this->userId = $user->getId();
        return $this;
    }

    /**
     * @inheritdoc
     */
//    public function createDocument(Request $request, Form $form)
    public function createDocument(Array $document_data)
    {
        $result = array();
        try {
            $document_data['_id'] = Model::insert(new Document(), $document_data);
            $result = $document_data;
        } catch (\Exception $e) {
            $result['error'] = 'Error! ' . $e->getMessage();
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
//    public function updateDocument(Request $request, Form $form)
    public function updateDocument(Array $document_data)
    {
        $result = array();
        if (array_key_exists('_id', $document_data) && is_int($document_data['_id'])) {
            $document = Model::getById(new Document(), $document_data['_id']);


        } else {
            $result['error'] = 'Error! Empty document ID.';
        }

    }

    /**
     * @inheritdoc
     */
    public function deleteDocument($document_id)
    {
        // TODO: Implement deleteDocument() method.
    }

    /**
     * @inheritdoc
     */
    public function uploadDocument(Request $request, Form $form)
    {
        // TODO: Implement uploadDocument() method.
    }

    /**
     * @inheritdoc
     */
    public function compressDocument(Array $document)
    {
        // TODO: Implement compressDocument() method.
    }

    /**
     * @inheritdoc
     */
    public function decompressDocument(Array $document)
    {
        // TODO: Implement decompressDocument() method.
    }

    public function processUpload(Request $request, Form $form)
    {
        $this->getUploadParameters($request);

        $files = array_values($request->files->get($form->getName()));
        foreach ($files as $file) {
            $a = $file;
        }

        foreach ($form['files'] as $file) {
            $file_data = $fileStorage->upload($file);

            // ToDo: add UserID into DB behavior
            $file_data['user_id'] = $user_id;

            if ('document' == $entity && !is_null($entity_id)) {
                $file_data['_id'] = $entity_id;
                Model::update(new Document(), $data);
            } else {
                $file_data = array_merge($file_data, array('entity' => $entity, 'entity_id' => $entity_id));
                $file_data['_id'] = Model::insert(new Document(), $file_data);
            }

            if (strpos($file_data['mimetype'], 'application') !== false ) {

                $_file_arch = new File($file_data['fullpath']);
                $_decompress_files_array = $fileStorage->unArch($_file_arch, $target_directory_name);
                if (array_key_exists('error', $_decompress_files_array)) {
                    $response = [
                        'success' => false,
                        'error' => $_decompress_files_array['error']
                    ];
                } else {
                    foreach ($_decompress_files_array as $_file) {
                        $_file_data = array_merge($_file, array('entity' => $entity, 'entity_id' => $entity_id));
                        $_file_data['_id'] = Model::insert(new Document(), $_file_data);
                    }
                    Model::remove(new Document(), array('_id' => $file_data['_id']));
                    $fileStorage->removeFile($_file_arch);
                }
            } else {
                $response['data'][] = $file_data;
            }
        }

    }

    /**
     * Get upload parameters by Request
     *
     * @param Request $request
     * @return array
     */
    protected function getUploadParameters(Request $request)
    {
        if (!$this->entity_name = $request->get('entity'))
            $this->entity_name = 'document';

        if (!$this->entity_id = $request->get('entity_id'))
            $this->entity_id = null;
    }

}