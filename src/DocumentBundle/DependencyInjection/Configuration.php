<?php

namespace DocumentBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('editorum_document');

//        $rootNode
//            ->children()
//                ->addDefaultsIfNotSet()
//                ->arrayNode('storage')->defaultValue('local')->end()
//                ->arrayNode('restrictions')
//                    ->children()
//                        ->arrayNode('publication')
//                            ->children()
//                                ->integerNode('max_limit_filesize')->defaultValue(15)->end()
//                                ->enumNode('allowed_file_extensions')
//                                    ->values(array('docx', 'doc', 'odt', 'zip', 'rar'))
//                                    ->isRequired()
//                                ->end() // allowed file extensions
//                            ->end()
//                        ->end() // publication
//                        ->integerNode('client_id')->end()
//                        ->scalarNode('client_secret')->end()
//                    ->end()
//                ->end() // restrictions
//            ->end()
//        ;

        return $treeBuilder;
    }
}
