<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 28.01.16
 * Time: 13:25
 *
 * StorageInterface.
 *
 * @author Vitiko <vitiko@mail.ru>
 */

namespace DocumentBundle\FileStorage;

use Symfony\Component\HttpFoundation\File\File;

interface FileStorageInterface {

    /**
     * Uploads the files in the uploadable fields of the
     * specified object according to the property configuration.
     *
     * @param File $file
     * @param String $directory
     * @param String $filename
     * @return
     * @internal param object $obj The object.
     */
    public function upload(File $file, $directory, $filename);

    /**
     * @abstract
     * @param $fullFileName
     * @return boolean
     */
    public function removeFile($fullFileName);

    /**
     * @abstract
     * @param $fullFileName
     * @return boolean
     */
    public function fileExists($fullFileName);

    /**
     * @abstract
     * @param \Symfony\Component\HttpFoundation\File\File $file
     * @param $fullFileName
     * @return boolean
     */
    public function isSameFile (File $file, $fullFileName);
}