<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 28.01.16
 * Time: 12:48
 */

namespace DocumentBundle\FileStorage;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use ZipArchive;

class FileSystemStorage implements FileStorageInterface{

    protected $storageDir;
    protected $sameFileChecker;

    /**
     * Constructs a new instance of FileSystemStorage.
     *
     * @param
     */
    public function __construct($dir = null) {
//        setlocale(LC_ALL, 'ru_RU.utf8');
        $this->storageDir = $dir;
        $this->sameFileChecker = function (File $file, $fullFileName)
        {
            return $file->getRealPath() == realpath($fullFileName);
        };
    }

    /**
     * Set storage directory
     *
     * @param String $dir
     */
    public function setDir($dir)
    {
        $this->storageDir = $dir;
    }

    /**
     * Return storage directory
     *
     * @return String|null
     */
    public function getDir()
    {
        return $this->storageDir;
    }

    /**
     * @param \Closure $checker
     */
    public function setSameFileChecker (\Closure $checker)
    {
        $this->sameFileChecker = $checker;
    }

    /**
     * @param File $file
     * @return string
     */
    protected function getOriginalName(File $file)
    {
        return $file instanceof UploadedFile ?
            $file->getClientOriginalName() : $file->getFilename();
    }

    /**
     * @param File $file
     * @return null|string
     */
    protected function getMimeType(File $file)
    {
        return $file instanceof UploadedFile ?
            $file->getClientMimeType() : $file->getMimeType();
    }

    /**
     * @param File $file
     * @param $fullFileName
     * @return mixed
     */
    public function isSameFile (File $file,  $fullFileName)
    {
        return  call_user_func(
            $this->sameFileChecker,
            $file,
            $fullFileName);
    }

    /**
     * @param $source
     * @param $directory
     * @param $name
     * @return File
     * @throws FileException
     */
    protected function copyFile($source, $directory, $name)
    {
        $this->checkDirectory($directory);
        $target = $directory . DIRECTORY_SEPARATOR . basename($name);
        if (!copy($source, $target)) {
            $error = error_get_last();
            throw new FileException(sprintf('Could not copy the file "%s" to "%s" (%s)', $source, $target, strip_tags($error['message'])));
        }
        chmod($target, 0666 & ~umask());
        return new File($target);
    }

    /**
     * @param $directory
     * @return bool
     * @throws FileException
     */
    protected function checkDirectory ($directory)
    {
        if (!is_dir($directory)) {
            if (false === @mkdir($directory, 0777, true)) {
                // @codeCoverageIgnoreStart
                throw new FileException(sprintf('Unable to create the "%s" directory', $directory));
                // @codeCoverageIgnoreEnd
            }
        } elseif (!is_writable($directory)) {
            // @codeCoverageIgnoreStart
            throw new FileException(sprintf('Unable to write in the "%s" directory', $directory));
            // @codeCoverageIgnoreEnd
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * File may be \Symfony\Component\HttpFoundation\File\File or \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    public function upload(File $file, $directory = '', $filename = null)
    {
        $originalName = $this->getOriginalName($file);
        if (is_null($filename))
            $filename = $originalName;

        $extension = $this->getFileExtension($originalName);
        $filename = md5($filename . time());
        $mimeType = $this->getMimeType($file);

        //transform filename and directory name if namer exists in mapping definition
        $fullFileName = $this->storageDir . DIRECTORY_SEPARATOR;
        if ($directory != '') {
            $fullFileName .= $directory . DIRECTORY_SEPARATOR;
        }
        $fullFileName .= $filename;

        //check if file already placed in needed position
        if (!$this->isSameFile($file, $fullFileName)) {
            $fileInfo = pathinfo($fullFileName);
            if ($file instanceof UploadedFile)
            {
                $this->checkDirectory($fileInfo['dirname']);
//                $file->move($fileInfo['dirname'], $fileInfo['basename']);
                $this->copyFile($file->getPathname(), $fileInfo['dirname'], $filename);
            } else {
                $this->copyFile($file->getPathname(), $fileInfo['dirname'], $filename);
            }
        }
        $fileData = array(
            'name' => $originalName,
            'filename' => $filename,
            'extension' => $extension,
            'originalName' => $originalName,
            'mimetype' => $mimeType,
            'size' => filesize($fullFileName),
            'dirname' => $fileInfo['dirname'],
            'fullpath' => $fullFileName
        );
//        if (!$fileData['fullpath'])
//            $fileData['fullpath'] = substr($fullFileName, strlen($this->storageDir));

        return $fileData;
    }


    public function unArch(File $file, $src_dir, $dst_dir = false)
    {
        if ($dst_dir == false)
            $dst_dir = $src_dir;

//        $src_dir = $this->storageDir . DIRECTORY_SEPARATOR .$src_dir;
//        $dst_dir = $this->storageDir . DIRECTORY_SEPARATOR .$dst_dir;

            $mime_type = $file->getMimeType();
        $filename = $src_dir . DIRECTORY_SEPARATOR . $file->getFilename();
        $ext_files = [];

        // @TODO Вынести настройки работы с архивами в конфигурацию
        if ($mime_type == "application/zip") {
            $zip = new ZipArchive();
            $res = $zip->open($filename);
            if ($res === TRUE) {
                for( $i = 0; $i < $zip->numFiles; $i++ ){
                    $stat = $zip->statIndex( $i );
                    if(strpos($stat['name'], '.docx') !== false) {
                        $zip->extractTo($src_dir, array($stat['name']));
                        $ext_files[] = $stat['name'];
//                        self::parseTitleList($university_id, $uploaddir, $stat['name']);
                    }
                }
                $zip->close();
            } else {
                $ext_files['error'] = "Не удалось открыть zip-архив";
            }
        } elseif (in_array($mime_type, ["application/x-rar-compressed", "application/x-rar"])) {

            $rar_file = rar_open($filename);
            if ($rar_file !== false) {
                $entries = rar_list($rar_file);
                foreach ($entries as $entry) {
                    if(strpos($entry->getName(), '.docx') !== false) {
                        $entry->extract($src_dir);
                        $ext_files[] = $entry->getName();
                        //                        self::parseTitleList($university_id, $uploaddir, $entry->getName());
                    }
                }
                rar_close($rar_file);
            } else {
                $ext_files['error'] = "Не удалось открыть rar-архив";
            }

        }

        if (!array_key_exists('error', $ext_files)) {
            $this->checkDirectory($dst_dir);

            $result = [];
            foreach ($ext_files as $ext_file) {
                $_file = new File($src_dir . DIRECTORY_SEPARATOR . $ext_file);
                $result[] = $this->upload($_file);
                $this->removeFile($_file);
            }
            return $result;
        }

        return $ext_files;
    }


    /**
     * @param $fullFileName
     * @return bool|null
     */
    public function removeFile($fullFileName) {
        if ($fullFileName && file_exists($fullFileName)) {
            unlink($fullFileName);
            return !file_exists($fullFileName);
        }
        return null;
    }

    /**
     * @param $fullFileName
     * @return bool
     */
    public function fileExists($fullFileName) {
        return file_exists($fullFileName);
    }

    /**
     * Return filename extension
     *
     * @param $filename
     * @return string
     */
    public function getFileExtension($filename) {
        $filename_array = explode('.', $filename);
        return sizeof($filename_array) > 0 ? end($filename_array) : '';
    }

}