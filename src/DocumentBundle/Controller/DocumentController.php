<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 27.01.16
 * Time: 10:51
 */

namespace DocumentBundle\Controller;

use DocumentBundle\FileStorage\FileSystemStorage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Editorum\Bundle\Logic\Model;
use DocumentBundle\Model\Document;
use ZipArchive;

/**
 * Class DocumentController
 * @package Editorum\Bundle\Controller
 */

class DocumentController extends Controller {

    /**
     * Show all documents
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $form = $this->createFormBuilder()
            ->add('files', 'file', array(
                'multiple' => true,
                'data_class' => null,
            ))
            ->add('save', 'submit', array('label' => 'Создать документ'))
            ->getForm();

        return $this->render(
            'DocumentBundle:Document:index.html.twig',
            array(
                'documents' => Document::getList(),
                'form' => $form->createView()
            )
        );
    }

    /**
     * Show list of the documents by selected entity
     *
     * @param String $entity
     * @param Integer $entity_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction($entity, $entity_id) {
        return $this->render(
            'DocumentBundle:Document:list.html.twig',
            array(
                'documents' => Document::getList(array('entity' => $entity, 'entity_id' => $entity_id))
            )
        );
    }

    /**
     * Show document by id
     *
     * @param Integer $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id) {
        return $this->render(
            'DocumentBundle:Document:show.html.twig',
            array(
                'document' => Model::getById(new Document(), $id)
            )
        );
    }

    /**
     * Add new document
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addAction(Request $request) {

        $form = $this->createFormBuilder()
            ->add('files', 'file', array(
                'multiple' => true,
                'data_class' => null,
            ))
            ->add('save', 'submit', array('label' => 'Создать документ'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $document_id = Model::insert(new Document(), $this->processForm($form, array()));
            $this->redirectToRoute('document_index');

        }

        return $this->render(
            'DocumentBundle:Document:add.html.twig',
            array(
                'form' => $form->createView(),
                'entity' => 'document',
                'entity_id' => null
            )
        );
    }

    /**
     * Create new document
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request) {
        return $this->render(
            'DocumentBundle:Document:add.html.twig'
        );
    }

    /**
     * Edit document by id
     *
     * @param Request $request
     * @param Integer $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id) {
        return $this->render(
            'DocumentBundle:Document:edit.html.twig',
            array(
                'document' => Model::getById(new Document(), $id)
            )
        );
    }

    public function updateAction(Request $request) {

    }

    public function deleteAction($id) {
        try {
            Model::remove(new Document(), $id);
            $this->redirectToRoute('document_index');
        } catch (\Exception $e) {

        }
    }


    /**
     * @param Request $request
     * @return array|Response
     */
    public function uploadAction(Request $request) {

        $dm = $this->get('document.manager');

        $user_id = null;
        if ($this->getUser())
            $user_id = $this->getUser()->getId();

        if (!$entity = $request->get('entity'))
            $entity = 'document';

        if (!$entity_id = $request->get('entity_id'))
            $entity_id = null;

        // ToDo: Change to directory structure: <storage_dir>/documents/<user_id>/<entity_name>
        $dm->setDefaultStorageDirectory('documents' . DIRECTORY_SEPARATOR . $user_id . DIRECTORY_SEPARATOR . $entity);
        $target_directory_name = 'documents' . DIRECTORY_SEPARATOR . $user_id . DIRECTORY_SEPARATOR . $entity;

        $response = [
            'success'   => true,
            'data'      => array()
        ];

        $form = $request->files->get('form');
        $fileStorage = new FileSystemStorage($target_directory_name);

        $form = $this->createFormBuilder()
            ->add('files', 'file', array(
                'multiple' => true,
                'data_class' => null,
            ))
            ->add('save', 'submit', array('label' => 'Создать документ'))
            ->getForm();

        $dm->processUpload($request, $form);

        try {

            foreach ($form['files'] as $file) {
                $file_data = $fileStorage->upload($file);

                // ToDo: add UserID into DB behavior
                $file_data['user_id'] = $user_id;

                if ('document' == $entity && !is_null($entity_id)) {
                    $file_data['_id'] = $entity_id;
                    Model::update(new Document(), $file_data);
                } else {
                    $file_data = array_merge($file_data, array('entity' => $entity, 'entity_id' => $entity_id));
                    $file_data['_id'] = Model::insert(new Document(), $file_data);
                }

                if (strpos($file_data['mimetype'], 'application') !== false ) {

                    $_file_arch = new File($file_data['fullpath']);
                    $_decompress_files_array = $fileStorage->unArch($_file_arch, $target_directory_name);
                    if (array_key_exists('error', $_decompress_files_array)) {
                        $response = [
                            'success' => false,
                            'error' => $_decompress_files_array['error']
                        ];
                    } else {
                        foreach ($_decompress_files_array as $_file) {
                            $_file_data = array_merge($_file, array('entity' => $entity, 'entity_id' => $entity_id));
                            $_file_data['_id'] = Model::insert(new Document(), $_file_data);
                        }
                        Model::remove(new Document(), array('_id' => $file_data['_id']));
                        $fileStorage->removeFile($_file_arch);
                    }
                } else {
                    $response['data'][] = $file_data;
                }
            }

        } catch(\Exception $e) {
            $response = [
                'success' => false,
                'error' => "Error!: " . $e->getMessage()
            ];
        }

        return new JsonResponse($response);
    }

    // ToDo: Remove this method after refactoring
    public function processForm(Form $form, $data = array()) {
        $form_data = $form->getData();
        $uploaded_file = $form_data['file'];
        $data['filename'] = $uploaded_file->getClientOriginalName();
        $data['size'] = $uploaded_file->getSize();
        $data['type'] = $uploaded_file->getMimeType();
        return $data;
    }

}