<?php
namespace SecurityBundle\Service;

use Doctrine\ODM\MongoDB\DocumentManager;
use SecurityBundle\Document\Role;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Role\RoleHierarchy;

class Hierarchy extends RoleHierarchy
{
    /** @var DocumentManager */
    private $odm;
    /** @var array */
    private $hierarchy = [];
    /** @var Session */
    private $session;
    /** @var string */
    private $env;

    /**
     * Hierarchy constructor.
     * @param Session $session
     * @param array $hierarchy
     * @param DocumentManager $odm
     */
    public function __construct(Session $session, array $hierarchy, DocumentManager $odm, $env)
    {
        $this->session = $session;
        $this->hierarchy = $hierarchy;
        $this->odm = $odm;
        $this->env = $env;

        parent::__construct($this->buildRoleTree());
    }

    /**
     * @return array
     */
    private function buildRoleTree()
    {
        if (php_sapi_name() === 'cli') {
            return [];
        }

        $sessionHierarchy = $this->session->get('roleHierarchy');
        $sessionCacheTime = $this->session->get('sessionCacheTime');
        $date = new \DateTime();
        $currentDate = clone $date;
        $date->add(\DateInterval::createFromDateString('10 minutes'));

        if ($sessionHierarchy !== null && $sessionCacheTime < $date && $this->env === 'prod') {
            return json_decode($sessionHierarchy, 1);
        }

        $hierarchy = [];

        $roles = $this->odm->getRepository('AsuSecurityBundle:Role')->findAll();

        foreach ($roles as $role) {
            if ($role->getParents()) {
                $parents = $role->getParents();
                foreach ($parents as $parent) {
                    if (!isset($hierarchy[$parent->getRole()])) {
                        $hierarchy[$parent->getRole()] = [];
                    }
                    $hierarchy[$parent->getRole()][] = $role->getRole();
                }
            } else {
                if (!isset($hierarchy[$role->getRole()])) {
                    $hierarchy[$role->getRole()] = [];
                }
            }
        }

        $hierarchy = array_merge_recursive($this->hierarchy, $hierarchy);

        if ($this->env === 'prod') {
            // cache hierarchy data
            $this->session->set('roleHierarchy', json_encode($hierarchy));
            $this->session->set('sessionCacheTime', $currentDate);
        }

        return $hierarchy;
    }
}
