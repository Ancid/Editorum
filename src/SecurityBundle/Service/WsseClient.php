<?php
namespace SecurityBundle\Service;

use Devster\GuzzleHttp\Subscriber\WsseAuth;
use GuzzleHttp\Client;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class WsseClient
 * @package SecurityBundle\Service
 */
class WsseClient
{
    /** @var Client */
    private $client;

    /**
     * WsseClient constructor.
     * @param Client $client
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(Client $client, TokenStorageInterface $tokenStorage)
    {
        $user = $tokenStorage->getToken()->getUser();
        if (!$user instanceof UserInterface) {
            throw new UnsupportedUserException;
        }
        $plugin = new WsseAuth($user->getEmail(), md5($user->getPassword()));
        $plugin->attach($client);
        $client->setDefaultOption('verify', false);
        $this->client = $client;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }
}
