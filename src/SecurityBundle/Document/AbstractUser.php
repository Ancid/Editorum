<?php
namespace SecurityBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\ReferenceMany;
use FOS\UserBundle\Document\User;
use Doctrine\Common\Collections\ArrayCollection;

abstract class AbstractUser extends User
{
    /**
     * @var array
     * @ReferenceMany(targetDocument="SecurityBundle\Document\Role")
     */
    protected $roles;

    /**
     * AbstractUser constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->roles = new ArrayCollection();
    }

    /**
     * Add role
     *
     * @param Role $role
     */
    public function addUserRole(Role $role)
    {
        $this->roles[] = $role;
    }

    /**
     * Remove role
     *
     * @param Role $role
     */
    public function removeUserRole(Role $role)
    {
        $this->roles->removeElement($role);
    }

    /**
     * Get roles
     *
     * @return Role[] $roles
     */
    public function getUserRoles()
    {
        return $this->roles;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        if (count($this->roles) === 0) {
            return [static::ROLE_DEFAULT];
        }

        return $this->roles->toArray();
    }

    /**
     * @inheritdoc
     */
    public function addRole($role)
    {
        return $this->addUserRole($role);
    }

    /**
     * @inheritdoc
     */
    public function hasRole($role)
    {
        return in_array($role, $this->getRoles());
    }

    /**
     * @inheritdoc
     */
    public function removeRole($role)
    {
        return $this->removeRole($role);
    }

}
