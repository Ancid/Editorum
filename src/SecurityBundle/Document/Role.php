<?php
namespace SecurityBundle\Document;

use Doctrine\Bundle\MongoDBBundle\Validator\Constraints as ODMAssets;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableDocument;
use Gedmo\Timestampable\Traits\TimestampableDocument;
use JMS\Serializer\Annotation as Serializer;
use Serializable;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Role
 * @package SecurityBundle\Document
 *
 * @ODM\Document(collection="roles")
 * @ODMAssets\Unique(fields={"name", "systemName"})
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Role implements RoleInterface, Serializable
{
    use SoftDeleteableDocument;
    use TimestampableDocument;

    /**
     * @var string
     * @ODM\Id(name="_id")
     */
    private $id;

    /**
     * @var string
     * @ODM\String()
     */
    private $name;

    /**
     * @var string
     * @ODM\String()
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $systemName;

    /**
     * @var string
     * @ODM\String(nullable=true)
     */
    private $description;

    /**
     * @var Role
     * @ODM\ReferenceMany(targetDocument="Role", simple=true, mappedBy="childrens")
     * @Serializer\Exclude()
     */
    private $parents;

    /**
     * @var Role
     * @ODM\ReferenceMany(targetDocument="Role", simple=true, inversedBy="parents")
     * @Serializer\Exclude()
     */
    private $childrens;

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->getSystemName();
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->systemName
        ]);
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list($this->id, $this->systemName) = unserialize($serialized);
    }

    /**
     * Role constructor.
     */
    public function __construct()
    {
        $this->parents = new ArrayCollection();
        $this->childrens = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * if name null or empty, then taking last part from system name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = null === $name || empty($name) ?
            preg_replace_callback('/^(ROLE\_){0,1}(.*)$/i', function ($match) {
                return $match[2];
            }, $this->getSystemName()) : $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set systemName
     *
     * @param string $systemName
     * @return self
     */
    public function setSystemName($systemName)
    {
        $this->systemName = preg_replace_callback('/^(ROLE\_){0,1}(.*)$/i', function ($match) {
            return 'ROLE_' . $match[2];
        }, $systemName);

        return $this;
    }

    /**
     * Get systemName
     *
     * @return string $systemName
     */
    public function getSystemName()
    {
        $return = preg_replace_callback('/^(ROLE\_){0,1}(.*)$/i', function ($match) {
            return 'ROLE_' . $match[2];
        }, $this->systemName);

        return $return;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add parent
     *
     * @param Role $parent
     */
    public function addParent(Role $parent)
    {
        $this->parents[] = $parent;
    }

    /**
     * Remove parent
     *
     * @param Role $parent
     */
    public function removeParent(Role $parent)
    {
        $this->parents->removeElement($parent);
    }

    /**
     * Get parents
     *
     * @return Role[] $parents
     */
    public function getParents()
    {
        return $this->parents;
    }

    /**
     * Add children
     *
     * @param Role $children
     */
    public function addChildren(Role $children)
    {
        $this->childrens[] = $children;
    }

    /**
     * Remove children
     *
     * @param Role $children
     */
    public function removeChildren(Role $children)
    {
        $this->childrens->removeElement($children);
    }

    /**
     * Get childrens
     *
     * @return Role[] $childrens
     */
    public function getChildrens()
    {
        return $this->childrens;
    }

    /**
     * @param Role $role
     * @return bool
     */
    public function hasChildren(Role $role)
    {
        return in_array($role, $this->getChildrens()->toArray());
    }
}
