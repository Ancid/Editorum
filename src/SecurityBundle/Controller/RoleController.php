<?php
namespace SecurityBundle\Controller;

use Editorum\Bundle\Controller\BasicController;
use SecurityBundle\Document\Role;
use SecurityBundle\Form\RoleType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RoleController
 * @package SecurityBundle\Controller
 *
 * @Security("is_granted('ROLE_ADMIN')")
 */
class RoleController extends BasicController
{
    public function indexAction(Request $request)
    {
        $roles = $this->getODM()
            ->getRepository('AsuSecurityBundle:Role')
            ->createQueryBuilder()
            ->sort('createdAt', 1)
        ;

        $roles = $this->get('knp_paginator')->paginate(
            $roles,
            $request->get('page', 1),
            30
        );

        return $this->render('AsuSecurityBundle:Role:list.html.twig', [
            'roles' => $roles
        ]);
    }

    public function newAction(Request $request)
    {
        $form =  $this->createForm(new RoleType());

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $role = $form->getData();

                $this->getODM()->persist($role);
                $this->getODM()->flush();

                return $this->redirectToRoute('role_list');
            }
        }

        return $this->render('AsuSecurityBundle:Role:new.html.twig', [
            'form'  => $form->createView()
        ]);
    }

    /**
     * @param Role $role
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @ParamConverter("role", class="SecurityBundle\Document\Role")
     */
    public function editAction(Role $role, Request $request)
    {
        $form = $this->createForm(new RoleType(), $role);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->getODM()->persist($role);
                $this->getODM()->flush();
            }
        }

        return $this->render('AsuSecurityBundle:Role:edit.html.twig', [
            'role'  => $role,
            'form'  => $form->createView(),
        ]);
    }

    /**
     * @param Role $role
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @ParamConverter("role", class="SecurityBundle\Document\Role")
     */
    public function removeAction(Role $role)
    {
        $this->getODM()->remove($role);
        $this->getODM()->flush();

        return $this->redirectToRoute('role_list');
    }
}
