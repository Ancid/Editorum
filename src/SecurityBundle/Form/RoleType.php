<?php

namespace SecurityBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', [
                'label'     => 'Название привилегии'
            ])
            ->add('system_name', 'text', [
                'label'     => 'Системное имя привилегии'
            ])
            ->add('description', 'textarea', [
                'label'     => 'Описание',
                'required'  => false
            ])
            ->add('childrens', 'document', [
                'class'     => 'SecurityBundle\Document\Role',
                'property'  => 'name',
                'multiple'  => true,
                'attr'      => [ 'rows' => 5 ],
                'label'     => 'Наследуемые привилегии'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class'    => 'SecurityBundle\Document\Role'
        ]);
    }

    public function getName()
    {
        return 'asu_security_bundle_role_type';
    }
}
