<?php
namespace SecurityBundle\Annotation\Driver;

use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\EventSubscriber;
use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use SecurityBundle\Annotation\Security\DocumentRoleChecker;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class DocumentRoleDriver
 * @package SecurityBundle\Annotation\Driver
 */
class DocumentRoleDriver implements EventSubscriber
{
    /** @var Reader */
    private $reader;
    /** @var DocumentRoleChecker */
    private $checker;

    /**
     * DocumentRoleDriver constructor.
     * @param Reader $reader
     * @param DocumentRoleChecker $checker
     */
    public function __construct(
        Reader $reader,
        DocumentRoleChecker $checker
    ) {
        $this->checker = $checker;
        $this->reader = $reader;
    }

    public function getSubscribedEvents()
    {
        return [
            'prePersist',
            'preUpdate',
            'preRemove',
            'preLoad'
        ];
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $document = $args->getDocument();
        $object =  new \ReflectionObject($document);

        $documentRole = $this->reader->getClassAnnotation(
            $object,
            '\\SecurityBundle\\Annotation\\DocumentRole'
        );

        if (!$documentRole) {
            return;
        }

        if (!$this->checker->checkAccess($documentRole->update)) {
            throw new AccessDeniedException;
        }
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $document = $args->getDocument();
        $object = new \ReflectionObject($document);

        $documentRole = $this->reader->getClassAnnotation(
            $object,
            '\\SecurityBundle\\Annotation\\DocumentRole'
        );

        if (!$documentRole) {
            return;
        }

        if (!$this->checker->checkAccess($documentRole->create)) {
            throw new AccessDeniedException;
        }
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $document = $args->getDocument();
        $object = new \ReflectionObject($document);

        $documentRole = $this->reader->getClassAnnotation(
            $object,
            '\\SecurityBundle\\Annotation\\DocumentRole'
        );

        if (!$documentRole) {
            return;
        }

        if (!$this->checker->checkAccess($documentRole->remove)) {
            throw new AccessDeniedException;
        }
    }

    public function preLoad(LifecycleEventArgs $args)
    {
        $document = $args->getDocument();
        $object = new \ReflectionObject($document);

        $documentRole = $this->reader->getClassAnnotation(
            $object,
            '\\SecurityBundle\\Annotation\\DocumentRole'
        );

        if (!$documentRole) {
            return;
        }

        if (!$this->checker->checkAccess($documentRole->view)) {
            throw new AccessDeniedException;
        }
    }
}
