<?php
namespace SecurityBundle\Annotation\Security;

use Symfony\Component\DependencyInjection\ContainerInterface;

class DocumentRoleChecker
{
    /** @var ContainerInterface */
    private $serviceContainer;

    /**
     * DocumentRoleChecker constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->serviceContainer = $container;
    }

    public function checkAccess(array $roles)
    {
        if (count($roles) == 0) {
            return true;
        }

        return $this->serviceContainer->get('security.authorization_checker')->isGranted($roles);
    }
}
