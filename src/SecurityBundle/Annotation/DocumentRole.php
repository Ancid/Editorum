<?php
namespace SecurityBundle\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * Class EntityRole
 * @package SecurityBundle\Annotation
 *
 * @Annotation
 * @Annotation\Target("CLASS")
 */
class DocumentRole
{
    public $create = [];
    public $update = [];
    public $remove = [];
    public $view = [];
}
