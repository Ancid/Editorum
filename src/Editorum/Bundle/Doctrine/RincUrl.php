<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 27.06.2016
 * Time: 16:32
 */

namespace Editorum\Bundle\Doctrine;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

trait RincUrl
{
    /** @ODM\Field(type="string") */
    protected $rinc_url;

    /**
     * Set rincUrl
     *
     * @param $rincUrl
     * @return $this
     */
    public function setRincUrl($rincUrl)
    {
        $this->rinc_url = $rincUrl;
        return $this;
    }

    /**
     * Get rincUrl
     *
     * @return string $rincUrl
     */
    public function getRincUrl()
    {
        return $this->rinc_url;
    }
}
