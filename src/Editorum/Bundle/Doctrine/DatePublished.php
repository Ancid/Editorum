<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 03.06.2016
 * Time: 11:19
 */

namespace Editorum\Bundle\Doctrine;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Publication date Trait
 *
 * @author Ancid
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
trait DatePublished
{
    /** @ODM\Field(type="date") */
    protected $date_published;


    /**
     * Set datePublished
     *
     * @param date $datePublished
     * @return self
     */
    public function setDatePublished($datePublished)
    {
        $this->date_published = $datePublished;
        return $this;
    }

    /**
     * Get datePublished
     *
     * @return date $datePublished
     */
    public function getDatePublished()
    {
        return $this->date_published;
    }
}
