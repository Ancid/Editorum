<?php
/**
 * User: Roman
 * Date: 26.07.16
 */
namespace Editorum\Bundle\Doctrine;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

trait PreviouslyPublished
{
    //Год Издания
    /** @ODM\Field(type="date") */
    protected $previously_published;

    /**
     * Set previously_published
     *
     * @param date $previously_published
     * @return self
     */
    public function setPreviouslyPublished($previously_published)
    {
        $this->previously_published = $previously_published;
        return $this;
    }

    /**
     * Get previously_published
     *
     * @return date $previously_published
     */
    public function getPreviouslyPublished()
    {
        return $this->previously_published;
    }
}
