<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 27.06.2016
 * Time: 16:32
 */

namespace Editorum\Bundle\Doctrine;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

trait DoiFields
{
    /** @ODM\Field(type="string") */
    protected $doi;

    /** @ODM\Field(type="string") */
    protected $doi_url;


    /**
     * Set doi
     *
     * @param string $doi
     * @return $this
     */
    public function setDoi($doi)
    {
        $this->doi = $doi;
        return $this;
    }

    /**
     * Get doi
     *
     * @return string $doi
     */
    public function getDoi()
    {
        return $this->doi;
    }

    /**
     * Set doiUrl
     *
     * @param string $doiUrl
     * @return $this
     */
    public function setDoiUrl($doiUrl)
    {
        $this->doi_url = $doiUrl;
        return $this;
    }

    /**
     * Get doiUrl
     *
     * @return string $doiUrl
     */
    public function getDoiUrl()
    {
        return $this->doi_url;
    }
}
