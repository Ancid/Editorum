<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 03.06.2016
 * Time: 11:19
 */

namespace Editorum\Bundle\Doctrine;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Multilanguage Trait
 *
 * @author Ancid
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
trait Multilanguage
{

    /** @ODM\Field(type="hash") */
    protected $ru = [];

    /** @ODM\Field(type="hash") */
    protected $en = [];


    /**
     * Set ru
     *
     * @param array $ru
     * @return self
     */
    public function setRu($ru)
    {
        $this->ru = $ru;
        return $this;
    }

    /**
     * Get ru
     *
     * @return array $ru
     */
    public function getRu()
    {
        return $this->ru;
    }

    /**
     * Set en
     *
     * @param array $en
     * @return self
     */
    public function setEn($en)
    {
        $this->en = $en;
        return $this;
    }

    /**
     * Get en
     *
     * @return array $en
     */
    public function getEn()
    {
        return $this->en;
    }
}
