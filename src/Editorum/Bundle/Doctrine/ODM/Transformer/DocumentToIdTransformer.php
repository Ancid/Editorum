<?php
namespace Editorum\Bundle\Doctrine\ODM\Transformer;

use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class DocumentToIdTransformer implements DataTransformerInterface
{
    /**
     * @var DocumentManager
     */
    protected $documentManager;

    /**
     * @var string
     */
    protected $class;

    /**
     * DocumentToIdTransformer constructor.
     * @param DocumentManager $documentManager
     * @param $class
     */
    public function __construct(DocumentManager $documentManager, $class)
    {
        $this->documentManager = $documentManager;
        $this->class = $class;
    }

    /**
     * @param mixed $document
     * @return mixed|void
     */
    public function transform($document)
    {
        if (null === $document) {
            return;
        }

        return $document->getId();
    }

    /**
     * @param mixed $id
     * @return null|object
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }

        $document = $this->documentManager
            ->getRepository($this->class)
            ->find($id);

        if (null === $document) {
            throw new TransformationFailedException();
        }

        return $document;
    }
}
