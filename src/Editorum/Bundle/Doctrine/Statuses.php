<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 17.08.2016
 * Time: 12:52
 */

namespace Editorum\Bundle\Doctrine;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

trait Statuses
{
    /** @ODM\Field(type="hash") */
    protected $statuses = [];

    /**
     * Set statuses
     *
     * @param array $statuses
     * @return self
     */
    public function setStatuses($statuses)
    {
        $this->statuses = $statuses;
        return $this;
    }

    /**
     * Get statuses
     *
     * @return array $statuses
     */
    public function getStatuses()
    {
        return $this->statuses;
    }

    /**
     * @param $key
     * @param $statuse
     */
    public function setStatus($key, $statuse)
    {
        $this->statuses[$key] = $statuse;
    }
}
