<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 03.06.2016
 * Time: 11:19
 */

namespace Editorum\Bundle\Doctrine;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use ReflectionClass;

/**
 * ASU External Id Trait
 *
 * @author Ancid
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
trait ObjectName
{
    /**
     * @return string
     */
    public function getObjectName()
    {
        $reflection = new ReflectionClass($this);
        return $reflection->getShortName();
//        return strtolower($reflection->getShortName());
    }
}
