<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 03.06.2016
 * Time: 11:19
 */

namespace Editorum\Bundle\Doctrine;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * ASU External Id Trait
 *
 * @author Ancid
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
trait AsuId
{
    /** @ODM\Field(type="int") */
    protected $external_id;

    /**
     * Set externalId
     *
     * @param int $externalId
     * @return self
     */
    public function setExternalId($externalId)
    {
        $this->external_id = $externalId;
        return $this;
    }

    /**
     * Get externalId
     *
     * @return int $externalId
     */
    public function getExternalId()
    {
        return $this->external_id;
    }
}
