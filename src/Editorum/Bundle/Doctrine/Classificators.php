<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 17.08.2016
 * Time: 12:52
 */

namespace Editorum\Bundle\Doctrine;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

trait Classificators
{
    /** @ODM\Field(type="collection") */
    protected $udk;

    /** @ODM\Field(type="collection") */
    protected $grnti;

    /** @ODM\Field(type="collection") */
    protected $okso;

    /** @ODM\Field(type="collection") */
    protected $bbk;

    /** @ODM\Field(type="collection") */
    protected $tbk;

    /**
     * @param string $name
     * @param string $classif
     * @return $this
     */
    public function addClassif($name, $classif)
    {
        if (null == $this->$name) {
            $this->$name = [];
        }
        if (!array_search($classif, $this->$name)) {
            array_push($this->$name, $classif);
        }
        return $this;
    }


    /**
     * Set udk
     *
     * @param collection $udk
     * @return $this
     */
    public function setUdk($udk)
    {
        $this->udk = $udk;
        return $this;
    }

    /**
     * Get udk
     *
     * @return collection $udk
     */
    public function getUdk()
    {
        return $this->udk;
    }

    /**
     * Set okso
     *
     * @param collection $okso
     * @return $this
     */
    public function setOkso($okso)
    {
        $this->okso = $okso;
        return $this;
    }

    /**
     * Get okso
     *
     * @return collection $okso
     */
    public function getOkso()
    {
        return $this->okso;
    }

    /**
     * Set bbk
     *
     * @param collection $bbk
     * @return $this
     */
    public function setBbk($bbk)
    {
        $this->bbk = $bbk;
        return $this;
    }

    /**
     * Get bbk
     *
     * @return collection $bbk
     */
    public function getBbk()
    {
        return $this->bbk;
    }

    /**
     * Set tbk
     *
     * @param collection $tbk
     * @return $this
     */
    public function setTbk($tbk)
    {
        $this->tbk = $tbk;
        return $this;
    }

    /**
     * Get tbk
     *
     * @return collection $tbk
     */
    public function getTbk()
    {
        return $this->tbk;
    }

    /**
     * Set grnti
     *
     * @param collection $grnti
     * @return self
     */
    public function setGrnti($grnti)
    {
        $this->grnti = $grnti;
        return $this;
    }

    /**
     * Get grnti
     *
     * @return collection $grnti
     */
    public function getGrnti()
    {
        return $this->grnti;
    }
}
