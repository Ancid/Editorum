<?php

/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 14.03.16
 * Time: 12:33
 */

namespace Editorum\Bundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class ControllerListener
{
    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();
        if (!is_array($controller)) {
            // not a controller do nothing
            return;
        }
        $controllerObject = $controller[0];
        if (is_object($controllerObject) && method_exists($controllerObject, "preExecute")) {
            $controllerObject->preExecute();
        }
    }
}
