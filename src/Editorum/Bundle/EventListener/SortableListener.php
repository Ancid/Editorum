<?php
namespace Editorum\Bundle\EventListener;

use Doctrine\Common\EventArgs;
use Gedmo\Sortable\SortableListener as GedmoSortableListener;

class SortableListener extends GedmoSortableListener
{
    /**
     * @var bool
     */
    private static $disabled = false;

    /**
     * @param $disabled
     */
    public static function stopListener($disabled)
    {
        self::$disabled = $disabled;
    }

    public function onFlush(EventArgs $args)
    {
        if (!self::$disabled) {
            parent::onFlush($args);
        }
    }

    public function prePersist(EventArgs $args)
    {
        if (!self::$disabled) {
            parent::prePersist($args);
        }
    }

    public function postPersist(EventArgs $args)
    {
        if (!self::$disabled) {
            parent::postPersist($args);
        }
    }

    public function preUpdate(EventArgs $args)
    {
        if (!self::$disabled) {
            parent::preUpdate($args);
        }
    }

    public function postRemove(EventArgs $args)
    {
        if (!self::$disabled) {
            parent::postRemove($args);
        }
    }

    public function postFlush(EventArgs $args)
    {
        if (!self::$disabled) {
            parent::postFlush($args);
        }
    }
}
