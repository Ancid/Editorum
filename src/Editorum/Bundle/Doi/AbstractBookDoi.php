<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 15.09.2016
 * Time: 13:07
 */

namespace Editorum\Bundle\Doi;

use Editorum\Bundle\Document\Chapter;
use Editorum\Bundle\Document\Reference;

abstract class AbstractBookDoi extends AbstractDoi
{
    protected function writeBody()
    {
        $this->xml->startElement('body');
//        $this->xml->writeAttribute('xmlns', 'http://www.crossref.org/schema/4.3.6');
            $this->xml->startElement('book');
            $this->xml->writeAttribute('book_type', 'monograph');
                $this->setBookMetadata();
                $this->setContentItem();
            $this->xml->endElement();
        $this->xml->endElement();
    }


    protected function setBookMetadata()
    {
        $this->xml->startElement('book_metadata');
        $this->xml->writeAttribute('language', 'ru');
//        $this->xml->writeAttribute('book_type', 'monograph');

        //Contributors
        $this->writeContributors($this->document);

        //Titles
        $this->writeTitles($this->document);

        //Abstract
        $this->writeAbstract($this->document);

        //Edition number
        $this->xml->writeElement('edition_number', $this->document->getPublicationNumber());

        //Publication_date
        $this->writePublicationDate($this->document);

        //Isbn
        $this->writeIsbn($this->document);

        //Publisher
        $this->writePublisher($this->document);

        //DOI
        $this->writeDoi($this->document);

        //Citation list
        $this->writeCitationList($this->document);

        $this->xml->endElement();
    }


    protected function setContentItem()
    {
        /** @var Chapter $chapter */
        foreach ($this->document->getChapters() as $chapter) {
            $this->xml->startElement('content_item');
            $this->xml->writeAttribute('component_type', 'chapter');
            $this->xml->writeAttribute('publication_type', 'bibliographic_record');

                $this->writeContributors($chapter);
                $this->writeTitles($chapter);
                $this->writeAbstract($chapter);

                $this->xml->writeElement('component_number', $chapter->getNumber());
                //Pages
                $this->writePages($chapter);
                //DOI
                $this->writeDoi($chapter);

            $this->xml->endElement();
        }
    }
}
