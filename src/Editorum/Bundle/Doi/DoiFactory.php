<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 15.09.2016
 * Time: 12:40
 */

namespace Editorum\Bundle\Doi;

use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DoiFactory
{
    protected $container = false;

    protected $dm = false;


    /**
     * DoiFactory constructor.
     * @param ContainerInterface $container
     * @param DocumentManager $documentManager
     */
    public function __construct(ContainerInterface $container, DocumentManager $documentManager)
    {
        $this->container = $container;
        $this->dm = $documentManager;
    }


    /**
     * @param $object
     * @return mixed
     * @throws \Exception
     */
    public function build($object)
    {
        $function = new \ReflectionClass(get_class($object));
        $rinc_object = 'Editorum\\Bundle\\Doi\\Objects\\' . $function->getShortName();
        if (class_exists($rinc_object)) {
            return new $rinc_object($object, $this->container, $this->dm);
        } else {
            throw new \Exception("Отсутствует DOI-объект для " . $function->getShortName().'('.$rinc_object.')');
        }
    }
}
