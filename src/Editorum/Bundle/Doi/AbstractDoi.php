<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 16.09.2016
 * Time: 15:24
 */

namespace Editorum\Bundle\Doi;

use Editorum\Bundle\Document\Reference;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ODM\MongoDB\DocumentManager;

abstract class AbstractDoi
{
    public $xml = false;

    public $doi_batch_id = false;

    public $doi_prefix = false;

    protected $document = false;

    protected $container;


    /**
     * AbstractDoi constructor.
     * @param ContainerInterface $container
     * @param DocumentManager $documentManager
     */
    public function __construct(ContainerInterface $container, DocumentManager $documentManager)
    {
        $this->container = $container;
        $this->dm = $documentManager;
        $this->xml = new \XMLWriter();
        $now = new \DateTime();
        $this->doi_batch_id = strtolower($this->document->getObjectName()).
            '_'.$this->document->getId().'_'.$now->format('Ymdhi');
    }


    public function generateXml()
    {
        $this->xml->openMemory();
        $this->xml->setIndent(true);

        $this->xml->startElement('doi_batch');
        $this->xml->writeAttribute('version', '4.3.7');
        $this->xml->writeAttribute('xmlns', 'http://www.crossref.org/schema/4.3.7');
        $this->xml->writeAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        $this->xml->writeAttribute('xmlns:jats', 'http://www.ncbi.nlm.nih.gov/JATS1');
        $this->xml->writeAttribute(
            'xsi:schemaLocation',
            'http://www.crossref.org/schema/4.3.7 http://www.crossref.org/schema/deposit/crossref4.3.7.xsd'
        );

        //Head
        $this->writeHead();
        //Body
        $this->writeBody();

        $this->xml->endElement();

        return $this->clearEmptyTags($this->xml->outputMemory());
    }


    protected function writeHead()
    {
        $publisher = $this->document->getPublisher();

        $this->xml->startElement('head');
        //doi_batch_id
        $this->xml->writeElement('doi_batch_id', $this->doi_batch_id);
        //timestamp
        $this->xml->writeElement('timestamp', time());
        //depositor
        $this->xml->startElement('depositor');
            $this->checkLangField('title', $publisher);
            $this->xml->writeElement('depositor_name', $publisher->getEn()['title']);
            $this->xml->writeElement('email_address', $publisher->getEmail());
        $this->xml->endElement();
        //registrant
        $this->xml->writeElement('registrant', $publisher->getEn()['title']);
        $this->xml->endElement();
    }


    /**
     * Похоже какая то нормализация
     * @param $xml
     * @return string
     */
    public function clearEmptyTags($xml)
    {
        $doc = new \DOMDocument();
        $doc->preserveWhiteSpace = false;
        $doc->loadXML($xml);
        $doc->encoding = 'UTF-8';
        $xpath = new \DOMXPath($doc);
        while (($node_list = $xpath->query('//*[not(*) and not(text()[normalize-space()])]')) && $node_list->length) {
            foreach ($node_list as $node) {
                $node->parentNode->removeChild($node);
            }
        }
        $doc->formatOutput = true;

        return $doc->saveXML();
    }


    /**
     * Write contributors block
     * @param $document
     * @throws \Exception
     */
    protected function writeContributors($document)
    {
        $this->xml->startElement('contributors');
        foreach ($document->getAuthors() as $author) {
            //ru
            $this->xml->startElement('person_name');
            $this->xml->writeAttribute('sequence', 'first');
            $this->xml->writeAttribute('contributor_role', 'author');
            $this->checkLangField('first_name', $author);
            $this->checkLangField('last_name', $author);
            $this->xml->writeAttribute('language', 'ru');
                $this->xml->writeElement('given_name', $author->getRu()['first_name']);
                $this->xml->writeElement('surname', $author->getRu()['last_name']);
            $this->xml->endElement();
            //en
            $this->xml->startElement('person_name');
            $this->xml->writeAttribute('sequence', 'first');
            $this->xml->writeAttribute('contributor_role', 'author');
            $this->xml->writeAttribute('language', 'en');
                $this->xml->writeElement('given_name', $author->getEn()['first_name']);
                $this->xml->writeElement('surname', $author->getEn()['last_name']);
            $this->xml->endElement();
        }
        $this->xml->endElement();
    }


    /**
     * Write publication date
     * @param $document
     */
    protected function writePublicationDate($document)
    {
        if (!empty($document->getDatePublished())) {
            $date = $document->getDatePublished();
        } else {
//            throw new \Exception('Невозможно отправить запрос на получение DOI, не указана дата публикации!');
            $date = $document->getCreatedAt();
        }
        $this->xml->startElement('publication_date');
        $this->xml->writeAttribute('media_type', 'print');
        $this->xml->writeElement('month', $date->format('m'));
        $this->xml->writeElement('day', $date->format('d'));
        $this->xml->writeElement('year', $date->format('Y'));
        $this->xml->endElement();
    }


    /**
     * Write DOI
     * @param $document
     */
    protected function writeDoi($document)
    {
        if (empty($document->getDoi())) {
            $doi = $this->doi_prefix.'/'.uniqid(strtolower($document->getObjectName()).'_');
            $document->setDoi($doi);
            $this->dm->flush();
        } else {
            $doi = $document->getDoi();
        }

        if (empty($document->getDoiUrl())) {
            $link = $this->getDocumentLink($document);
        } else {
            $link = $document->getDoiUrl();
        }

        $this->xml->startElement('doi_data');
        $this->xml->writeElement('doi', $doi);
            $this->xml->startElement('resource');
            $this->xml->writeAttribute('mime_type', 'text/html');
                $this->xml->text($link);
            $this->xml->endElement();
        $this->xml->endElement();
    }


    /**
     * Write titles block
     * @param $document
     */
    protected function writeTitles($document)
    {
        $this->checkLangField('title', $document);
        $this->xml->startElement('titles');
            $this->xml->writeElement('title', $document->getEn()['title']);
            $this->xml->startElement('original_language_title');
            $this->xml->writeAttribute('language', 'ru');
                $this->xml->text($document->getRu()['title']);
            $this->xml->endElement();
        $this->xml->endElement();
    }


    /**
     * Write abstract block
     * @param $document
     */
    protected function writeAbstract($document)
    {
        $this->checkLangField('annotation', $document);
        $this->xml->startElement('jats:abstract');
            $this->xml->startElement('jats:p');
//            $this->xml->writeAttribute('lang', 'en');
                $this->xml->text($document->getEn()['annotation']);
            $this->xml->endElement();
//            $this->xml->startElement('jats:p');
//            $this->xml->writeAttribute('lang', 'ru');
//                $this->xml->text($document->getRu()['annotation']);
//            $this->xml->endElement();
        $this->xml->endElement();
    }


    /**
     * Write pages
     * @param $document
     */
    protected function writePages($document)
    {
        $this->xml->startElement('pages');
            $this->xml->writeElement('first_page', $document->getFirstPage());
            $this->xml->writeElement('last_page', $document->getLastPage());
        $this->xml->endElement();
    }


    /**
     * Write references
     * @param $document
     * @throws \Exception
     */
    protected function writeCitationList($document)
    {
        $this->xml->startElement('citation_list');
        /** @var Reference $ref */
        foreach ($document->getReferences() as $ref) {
            $this->checkLangField('text', $ref);
            $this->xml->startElement('citation');
            $this->xml->writeAttribute('key', 'ref'.$ref->getPosition());
                $this->xml->writeElement('unstructured_citation', $ref->getRu()['text'].', '.$ref->getEn()['text']);
            $this->xml->endElement();
        }
        $this->xml->endElement();
    }


    /**
     * Write publisher
     * @param $document
     */
    protected function writePublisher($document)
    {
        $this->xml->startElement('publisher');
            $this->xml->writeElement('publisher_name', $document->getPublisher()->getEn()['title']);
            $this->xml->writeElement('publisher_place', $document->getPublisher()->getTown());
        $this->xml->endElement();
    }


    /**
     * Write isbn
     * @param $document
     */
    protected function writeIsbn($document)
    {
        $this->xml->startElement('isbn');
        $this->xml->writeAttribute('media_type', 'print');
            $this->xml->text($document->getIsbn());
        $this->xml->endElement();
        $this->xml->startElement('isbn');
        $this->xml->writeAttribute('media_type', 'electronic');
            $this->xml->text($document->getIsbnOnline());
        $this->xml->endElement();
    }


    /**
     * @param string $field
     * @param $document
     * @throws \Exception
     */
    protected function checkLangField($field, $document)
    {
        if (!array_key_exists($field, $document->getRu())) {
            throw new \Exception('Пустое поле ru['.$field.'] у сущности '.$document->getObjectName().
                '[id='.$document->getId().']');
        }
        if (!array_key_exists($field, $document->getEn())) {
            throw new \Exception('Пустое поле en['.$field.'] у сущности '.$document->getObjectName().
                '[id='.$document->getId().']');
        }
    }

    abstract protected function writeBody();


    /**
     * Генерирует ссылку на объект по имени роута
     * @param $document
     * @return string
     */
    public function getDocumentLink($document)
    {
        if (!empty($document->getDoiUrl())) {
            return $document->getDoiUrl();
        }
        $prefix = 'http:/';
        $r_name = 'nauka_'.strtolower($document->getObjectName());

        return $prefix.$this->container->get('router')->generate($r_name, [
            'id' => $document->getId()
        ]);
    }
}
