<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 15.09.2016
 * Time: 13:07
 */

namespace Editorum\Bundle\Doi;

use Editorum\Bundle\Document\Article;
use Editorum\Bundle\Document\Chapter;
use Editorum\Bundle\Document\Issue;
use Editorum\Bundle\Document\Journal;
use Editorum\Bundle\Document\Reference;

abstract class AbstractJournalDoi extends AbstractDoi
{
    /**
     * @param Journal $journal
     * @param bool $forced
     */
    protected function writeJournalMetadata(Journal $journal, $forced = true)
    {
        $this->xml->startElement('journal_metadata');
        $this->xml->writeAttribute('language', 'en');
            //full name
            $this->xml->writeElement('full_title', $journal->getEn()['title']);
            //Issn
            $this->xml->startElement('issn');
            $this->xml->writeAttribute('media_type', 'print');
            $this->xml->text($journal->getIssnPrint());
            $this->xml->endElement();
            $this->xml->startElement('issn');
            $this->xml->writeAttribute('media_type', 'electronic');
            $this->xml->text($journal->getIssnOnline());
            $this->xml->endElement();
        if ($forced) {
            //DOI
            $this->writeDoi($journal);
        }
        $this->xml->endElement();
    }


    /**
     * @param Issue $issue
     */
    protected function writeJournalIssue(Issue $issue)
    {
        $this->xml->startElement('journal_issue');
            //Publication_date
            $this->writePublicationDate($issue);
            //Journal volume
            $this->xml->startElement('journal_volume');
            $this->xml->writeElement('volume', $issue->getVolume());
            $this->xml->endElement();
            //Issue
            $this->xml->writeElement('issue', $issue->getIssue());
            //Special numbering
            $this->xml->writeElement('special_numbering', $issue->getPart());
            //DOI
            $this->writeDoi($issue);
        $this->xml->endElement();
    }


    /**
     * @param null $articles
     */
    protected function writeJournalArticle($articles = null)
    {
        if (null == $articles) {
            $articles = $this->document->getArticles();
        }
        /** @var Article $article */
        foreach ($articles as $article) {
            $this->xml->startElement('journal_article');
            $this->xml->writeAttribute('publication_type', 'bibliographic_record');
                //Titles
                $this->writeTitles($article);
                //Contributors
                $this->writeContributors($article);
                //Abstract
                $this->writeAbstract($article);
                //Publication_date
            if (null == $article->getIssue()) {
                $object = $article;
            } else {
                $object = $article->getIssue();
            }
                $this->writePublicationDate($object);
                //Pages
                $this->writePages($article);
                //DOI
                $this->writeDoi($article);
                //Citation list
                $this->writeCitationList($article);
            $this->xml->endElement();
        }
    }
}
