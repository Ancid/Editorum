<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 16.09.2016
 * Time: 16:41
 */

namespace Editorum\Bundle\Doi\Objects;

use Doctrine\ODM\MongoDB\DocumentManager;
use Editorum\Bundle\Document\Monography as MonographyDocument;
use Editorum\Bundle\Doi\AbstractBookDoi;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Monography extends AbstractBookDoi
{
    /**
     * Monography constructor.
     * @param MonographyDocument $monography
     * @param ContainerInterface $container
     * @param DocumentManager $documentManager
     */
    public function __construct(
        MonographyDocument $monography,
        ContainerInterface $container,
        DocumentManager $documentManager
    ) {
        $this->document = $monography;
        $this->doi_prefix = $monography->getPublisher()->getDoiPrefix();
        parent::__construct($container, $documentManager);
    }
}
