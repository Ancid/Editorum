<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 15.09.2016
 * Time: 13:07
 */

namespace Editorum\Bundle\Doi\Objects;

use Doctrine\ODM\MongoDB\DocumentManager;
use Editorum\Bundle\Document\Collection as CollectionDocument;
use Editorum\Bundle\Doi\AbstractConferenceDoi;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Collection extends AbstractConferenceDoi
{
    /**
     * Journal constructor.
     * @param CollectionDocument $collection
     * @param ContainerInterface $container
     * @param DocumentManager $documentManager
     */
    public function __construct(
        CollectionDocument $collection,
        ContainerInterface $container,
        DocumentManager $documentManager
    ) {
        $this->document = $collection;
        $this->doi_prefix = $collection->getPublisher()->getDoiPrefix();
        parent::__construct($container, $documentManager);
    }

    protected function writeBody()
    {
        $this->xml->startElement('body');
        $this->xml->writeAttribute('xmlns', 'http://www.crossref.org/schema/4.3.7');
            $this->xml->startElement('conference');
                $this->writeEventMetadata($this->document->getConference());
                $this->writeProceedingsMetadata($this->document);
                $this->writeConferencePaper($this->document->getArticles());
            $this->xml->endElement();
        $this->xml->endElement();
    }
}
