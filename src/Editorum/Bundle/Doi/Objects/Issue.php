<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 15.09.2016
 * Time: 13:07
 */

namespace Editorum\Bundle\Doi\Objects;

use Doctrine\ODM\MongoDB\DocumentManager;
use Editorum\Bundle\Document\Issue as IssueDocument;
use Editorum\Bundle\Doi\AbstractJournalDoi;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Issue extends AbstractJournalDoi
{
    /**
     * Issue constructor.
     * @param IssueDocument $issue
     * @param ContainerInterface $container
     * @param DocumentManager $documentManager
     */
    public function __construct(
        IssueDocument $issue,
        ContainerInterface $container,
        DocumentManager $documentManager
    ) {
        $this->document = $issue;
        $this->doi_prefix = $issue->getPublisher()->getDoiPrefix();
        parent::__construct($container, $documentManager);
    }

    protected function writeBody()
    {
        $this->xml->startElement('body');
        $this->xml->writeAttribute('xmlns', 'http://www.crossref.org/schema/4.3.7');
            $this->xml->startElement('journal');
                $this->writeJournalMetadata($this->document->getJournal(), false);
                $this->writeJournalIssue($this->document);
                $this->writeJournalArticle($this->document->getArticles());
            $this->xml->endElement();
        $this->xml->endElement();
    }



}
