<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 15.09.2016
 * Time: 13:07
 */

namespace Editorum\Bundle\Doi\Objects;

use Doctrine\ODM\MongoDB\DocumentManager;
use Editorum\Bundle\Document\Conference as ConferenceDocument;
use Editorum\Bundle\Doi\AbstractConferenceDoi;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Conference extends AbstractConferenceDoi
{
    /**
     * Journal constructor.
     * @param ConferenceDocument $conference
     * @param ContainerInterface $container
     * @param DocumentManager $documentManager
     */
    public function __construct(
        ConferenceDocument $conference,
        ContainerInterface $container,
        DocumentManager $documentManager
    ) {
        $this->document = $conference;
        $this->doi_prefix = $conference->getPublisher()->getDoiPrefix();
        parent::__construct($container, $documentManager);
    }

    protected function writeBody()
    {
        $this->xml->startElement('body');
        $this->xml->writeAttribute('xmlns', 'http://www.crossref.org/schema/4.3.7');
            $this->xml->startElement('conference');
                $this->writeEventMetadata($this->document);
            $this->xml->endElement();
        $this->xml->endElement();
    }
}
