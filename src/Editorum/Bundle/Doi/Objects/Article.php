<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 15.09.2016
 * Time: 13:07
 */

namespace Editorum\Bundle\Doi\Objects;

use Doctrine\ODM\MongoDB\DocumentManager;
use Editorum\Bundle\Document\Article as ArticleDocument;
use Editorum\Bundle\Doi\AbstractJournalDoi;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Article extends AbstractJournalDoi
{
    /**
     * Issue constructor.
     * @param ArticleDocument $article
     * @param ContainerInterface $container
     * @param DocumentManager $documentManager
     */
    public function __construct(
        ArticleDocument $article,
        ContainerInterface $container,
        DocumentManager $documentManager
    ) {
        $this->document = $article;
        $this->doi_prefix = $article->getPublisher()->getDoiPrefix();
        parent::__construct($container, $documentManager);
    }

    protected function writeBody()
    {
        $this->xml->startElement('body');
        $this->xml->writeAttribute('xmlns', 'http://www.crossref.org/schema/4.3.7');
            $this->xml->startElement('journal');
                $this->writeJournalMetadata($this->document->getJournal(), false);
                $this->writeJournalArticle([$this->document]);
            $this->xml->endElement();
        $this->xml->endElement();
    }
}
