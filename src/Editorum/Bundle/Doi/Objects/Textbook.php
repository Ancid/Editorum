<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 16.09.2016
 * Time: 16:41
 */

namespace Editorum\Bundle\Doi\Objects;

use Doctrine\ODM\MongoDB\DocumentManager;
use Editorum\Bundle\Document\Textbook as TextbookDocument;
use Editorum\Bundle\Doi\AbstractBookDoi;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Textbook extends AbstractBookDoi
{
    /**
     * Textbook constructor.
     * @param TextbookDocument $textbook
     * @param ContainerInterface $container
     * @param DocumentManager $documentManager
     */
    public function __construct(
        TextbookDocument $textbook,
        ContainerInterface $container,
        DocumentManager $documentManager
    ) {
        $this->document = $textbook;
        $this->doi_prefix = $textbook->getPublisher()->getDoiPrefix();
        parent::__construct($container, $documentManager);
    }
}
