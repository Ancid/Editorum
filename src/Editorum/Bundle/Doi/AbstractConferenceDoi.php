<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 15.09.2016
 * Time: 13:07
 */

namespace Editorum\Bundle\Doi;

use Editorum\Bundle\Document\Article;
use Editorum\Bundle\Document\Chapter;
use Editorum\Bundle\Document\Collection;
use Editorum\Bundle\Document\Conference;
use Editorum\Bundle\Document\ConferenceArticle;
use Editorum\Bundle\Document\Issue;
use Editorum\Bundle\Document\Reference;

abstract class AbstractConferenceDoi extends AbstractDoi
{
    /**
     * @param Conference $conference
     */
    protected function writeEventMetadata(Conference $conference)
    {
        $this->xml->startElement('event_metadata');
        //Conference name
        $this->xml->writeElement('conference_name', $conference->getEn()['title']);
        //Conference ntheme
        $html = strip_tags($conference->getHtml());
        $html = str_replace('&#13;', '', $html);
//        var_dump($html);die();
        $this->xml->writeElement('conference_theme', $html);
        //Conference number
        $this->xml->writeElement('conference_number', $conference->getNumber());
        //Conference location
        if (array_key_exists('city', $conference->getEn())) {
            $this->xml->writeElement('conference_location', $conference->getEn()['city']);
        }
        //Conference date
        $this->xml->startElement('conference_date');
        $this->xml->writeAttribute('start_day', $conference->getDateStart()->format('d'));
        $this->xml->writeAttribute('start_month', $conference->getDateStart()->format('m'));
        $this->xml->writeAttribute('start_year', $conference->getDateStart()->format('Y'));
        $this->xml->writeAttribute('end_day', $conference->getDateFinish()->format('d'));
        $this->xml->writeAttribute('end_month', $conference->getDateFinish()->format('m'));
        $this->xml->writeAttribute('end_year', $conference->getDateFinish()->format('Y'));
        $this->xml->endElement();
        $this->xml->endElement();
    }


    /**
     * @param Collection $collection
     */
    protected function writeProceedingsMetadata(Collection $collection)
    {
        $this->xml->startElement('proceedings_metadata');
        //@ToDo для каждого языка?
        $this->xml->writeAttribute('language', 'en');
        //Proceedings title
        $this->xml->writeElement('proceedings_title', $collection->getEn()['title']);
        //Publisher
        $this->writePublisher($collection);
        //Publication date
        $this->writePublicationDate($collection);
        //ISBN
        $this->writeIsbn($collection);
        //Doi data
        $this->writeDoi($collection);
        $this->xml->endElement();
    }


    /**
     * @param null $articles
     */
    protected function writeConferencePaper($articles = null)
    {
        if (null == $articles) {
            $articles = $this->document->getArticles();
        }
        /** @var ConferenceArticle $article */
        foreach ($articles as $article) {
            $this->xml->startElement('conference_paper');
            $this->xml->writeAttribute('publication_type', 'bibliographic_record');
            //Contributors
            $this->writeContributors($article);
            //Titles
            $this->writeTitles($article);
            //Abstract
            $this->writeAbstract($article);
            //Publication_date
            $this->writePublicationDate($article);
            //Pages
            $this->writePages($article);
            //DOI
            $this->writeDoi($article);
            //Citation list
            $this->writeCitationList($article);
            $this->xml->endElement();
        }
    }
}
