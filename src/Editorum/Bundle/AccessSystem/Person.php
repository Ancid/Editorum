<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 05.11.2015
 * Time: 20:56
 */

namespace Editorum\Bundle\AccessSystem;

use Symfony\Component\HttpFoundation\Session\Session;
use Editorum\Bundle\Logic\Model;

class Person
{
    protected static $instance = false;

    public $user_id = false;
    public $user_fio = false;
    public $user_credentials = null;

    private function __constructor()
    {
    }

    public static function instance()
    {
        if (self::$instance == false)
        {
            self::$instance = new Person();
            self::$instance->refreshLogged();
        }

        return self::$instance;
    }


    /**
     * Осуществить попытку входа
     * @param $login
     * @param $password
     * @return bool
     */
    public function login($login, $password)
    {
        $user = Model::getByKeys(new User(), array('login' => $login, 'password' => md5(trim($password))));

        if (!is_null($user) && $user->isActive($login)) {
            $session = new Session();
            $session->set('user_id', $user->__get('_id'));
            $this->refreshLogged();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Залогинен ли пользователь
     */
    public function isLogged()
    {
        if ($this->user_id !== false)
            return true;
        else
            return false;
    }


    /**
     * Обновить информацию о залогиненном пользователе
     */
    public function refreshLogged()
    {
        $session = new Session();

        if ($session->get('user_id') > 0)
        {
            $this->user_id = $session->get('user_id');
            $user = Model::getById(new User(), $this->user_id);
            $this->user_fio = $user->last_name.' '.$user->first_name;

        }
        else
        {
            $this->user_id = false;
            $this->user_fio = false;
        }
    }

    /**
     * Получить ФИО для отображения на сайте
     */
    public function getName()
    {
        return $this->user_fio;
    }

    /**
     * Выход пользователя
     */
    public function logout()
    {
        $session = new Session();
        $session->remove('user_id');
        $this->refreshLogged();
    }


    /**
     * Активирует пользователя
     * @param $login
     * @param $password
     * @return bool
     */
    public function activatePerson($login, $password)
    {
        $user = Model::getByKeys(new User(), array('login' => $login, 'password' => $password));

//        if ($user->isEmpty()) {
//            return false;
//        } else {
//            $user->
//            return true;
//        }
    }
}
