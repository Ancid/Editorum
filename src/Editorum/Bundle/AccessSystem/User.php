<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 15.11.2015
 * Time: 15:45
 */

namespace Editorum\Bundle\AccessSystem;
use Editorum\Bundle\Logic\MongoNode;
use Editorum\Bundle\Logic\Model;

class User extends MongoNode
{
    public function createEmpty()
    {
        $obj = array(
            'first_name'                            => '',
            'last_name'                             => '',
            'second_name'                           => '',
            'login'                                 => '',
            'password'                              => '',
            'email'                                 => '',
            'phone'                                 => '',
            'code'                                  => '',
            'active'                                => false,
        );

        return $obj;
    }


    /**
     * Собрать ФИО пользователя
     */
    public function calcFio()
    {
        return $this->__get('last_name').' '.$this->__get('first_name').' '.$this->__get('second_name');
    }


    public function isActive($login)
    {
        $user = Model::getOneByKeys(new User(), array('login' => $login), array('active'));
        return $user['active'];
    }

    public static function activate($login)
    {
        $user = Model::getOneByKeys(new User(), array('login' => $login), array('active'));
        $user['active'] = true;
        Model::update(new User, $user);
    }
}