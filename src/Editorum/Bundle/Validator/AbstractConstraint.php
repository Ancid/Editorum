<?php
namespace Editorum\Bundle\Validator;

use Symfony\Component\Validator\Constraint;

abstract class AbstractConstraint extends Constraint
{
    /**
     * @return string
     */
    public function validatedBy()
    {
        return str_replace('Constraints\\', '', get_class($this)) . 'Validator';
    }
}
