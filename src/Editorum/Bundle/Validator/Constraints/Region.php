<?php
namespace Editorum\Bundle\Validator\Constraints;

use Editorum\Bundle\Validator\AbstractConstraint;

/**
 * @Annotation
 */
class Region extends AbstractConstraint
{
    public $message = 'region_has_wrong_country';
    public $region  = 'region';
    public $country = 'country';

    /**
     * @return string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
