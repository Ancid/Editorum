<?php
namespace Editorum\Bundle\Validator;

use Doctrine\Common\Util\Inflector;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class RegionValidator extends ConstraintValidator
{
    /**
     * Checks if the passed value is valid.
     *
     * @param mixed      $object      The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($object, Constraint $constraint)
    {
        $regionMethod = 'get' . Inflector::classify($constraint->region);
        $countryMethod = 'get' . Inflector::classify($constraint->country);

        if (!method_exists($object, $regionMethod)
            || !method_exists($object, $countryMethod)) {
            return;
        }

        $country = call_user_func([$object, $countryMethod]);
        $region = call_user_func([$object, $regionMethod]);

        if ($country !== null && $country->getRu()['title'] !== 'Россия' && $region !== null) {
            $this->context->addViolation($constraint->message);
        }

        return;
    }
}
