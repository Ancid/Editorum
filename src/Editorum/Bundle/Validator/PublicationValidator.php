<?php
namespace Editorum\Bundle\Validator;

use Editorum\Bundle\Document\Article;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Translation\TranslatorInterface;

class PublicationValidator
{
    protected $translator;

    protected $document;

    protected $success = false;

    protected $errors = [];

    protected $art_errors = [];


    public function __construct(TranslatorInterface $translator, Router $router)
    {
        $this->translator = $translator;
        $this->router = $router;
    }


    public function validate($document)
    {
        $this->document = $document;
        $errors = [];
        $art_errors = [];

        if (array_key_exists('valid', $document->getStatuses()) && $document->getStatuses()['valid'] == 1) {
            return [
                'errors' => $errors,
                'art_errors' => $art_errors,
            ];
        }

        $this->notEmpty(['field' => 'title', 'lang' => 'Ru'], $errors);
        $this->notEmpty(['field' => 'annotation', 'lang' => 'Ru'], $errors);
        $this->notEmpty(['field' => 'keywords', 'lang' => 'Ru'], $errors);
        $this->notEmpty(['field' => 'Volume'], $errors);
        $this->notEmpty(['field' => 'Issue'], $errors);
        $this->notEmpty(['field' => 'PageCount'], $errors);
        $this->notEmpty(['field' => 'Languages'], $errors);
        $this->moreZero(['field' => 'FirstPage'], $errors);
        $this->moreZero(['field' => 'LastPage'], $errors);
        $this->moreZero(['field' => 'AuthorPages'], $errors);
        $this->moreZero(['field' => 'PageTotal'], $errors);
        $this->notEmpty(['field' => 'Text'], $errors);
        $this->notEmpty(['field' => 'DateRegCertificate'], $errors);
        $this->moreZero(['field' => 'PublishedPageTotal'], $errors);
        $this->notEmpty(['field' => 'Isbn'], $errors);
        $this->notEmpty(['field' => 'IsbnOnline'], $errors);
        $this->notEmpty(['field' => 'PublicationNumber'], $errors);
//        $this->notEmpty(['field' => 'Cover']);

        $this->atLeastOne(['field' => 'Chapters', 'msg' => 'Должна быть указана хотя бы одна глава'], $errors);
        $this->atLeastOne(['field' => 'Rubric', 'msg' => 'Должна быть указана рубрика'], $errors);
        $this->atLeastOne(['field' => 'Section', 'msg' => 'Должна быть указана секция'], $errors);
        $this->atLeastOne([
            'field' => 'CollectionSectionList',
            'msg' => 'Должна быть указана хотя бы одна секция'
        ], $errors);
        $this->atLeastOne(['field' => 'References', 'msg' => 'Должен быть указан список литературы'], $errors);
        $this->atLeastOne(['field' => 'Authors', 'msg' => 'Должен быть указан хотя бы один автор'], $errors);
        $this->atLeastOne(['field' => 'Issuerubriclist', 'msg' => 'Должна быть указана хотя бы одна рубрика'], $errors);

        //Проверяем статьи документа
        $this->validateArticles($errors, $art_errors);

        //Проверяем классификаторы
        $this->checkClassif($errors);

        if (count($errors) == 0 && count($art_errors) == 0) {
            $document->setStatus('valid', 1);
        }

        return [
            'errors' => $errors,
            'art_errors' => $art_errors,
        ];
    }


    /**
     * @param $options
     * @param $erors
     */
    protected function notEmpty($options, &$erors)
    {
        $this->checkField($options, $erors, function ($value) {
            return empty($value);
        });
    }


    /**
     * @param $options
     * @param $errors
     */
    protected function moreZero($options, &$errors)
    {
        $this->checkField($options, $errors, function ($value) {
            return $value < 1;
        });
    }


    /**
     * @param array $options
     * @param array $errors
     * @param $func //передаем колбэк с условием ошибки
     * @throws \Exception
     */
    protected function checkField($options, &$errors, $func)
    {
        if (!array_key_exists('field', $options)) {
            throw new \Exception('Массив $options должен содержать поле field');
        }
        if (!is_callable($func)) {
            throw new \Exception('$func должна быть функцией');
        }

        $value = true;
        if (!empty($options['lang'])) {
            $method = 'get' . $options['lang'];
            if (method_exists($this->document, $method)) {
                $value = $this->document->$method()[$options['field']];
            }
        } else {
            $method = 'get' . $options['field'];
            if (method_exists($this->document, $method)) {
                $value = $this->document->$method();
            }
        }

        if ($func($value)) {
            $errors[] = $this->emassage($options);
        }
    }


    /**
     * @param $options
     * @param $errors
     * @throws \Exception
     */
    protected function atLeastOne($options, &$errors)
    {
        if (!array_key_exists('field', $options)) {
            throw new \Exception('Массив $options должен содержать поле field');
        }

        $method = 'get' . $options['field'];

        if (method_exists($this->document, $method)) {
            if (empty(count($this->document->$method()))) {
                $errors[] = $options['msg'];
            }
        }
    }


    /**
     * @param $errors
     * @param $art_errors
     */
    protected function validateArticles(&$errors, &$art_errors)
    {
        if (method_exists($this->document, 'getArticles')) {
            $articles = $this->document->getArticles();
            if (count($articles) < 1) {
                $errors[] = 'В '. $this->translator->trans('entity.' . $this->document->getObjectName()) .
                ' должна быть включена хотя бы одна статья';
            } else {
                /** @var Article $article */
                foreach ($articles as $article) {
                    $result = $this->validate($article);
                    if (count($result['errors']) > 0) {
                        $art_errors[] = [
                            'url' => $this->router->generate('publication_validate', [
                                'id' => $article->getId(),
                                'type' => $article->getObjectName(),
                            ]),
                            'title' => $article->getRu()['title'],
                        ];
                    }
                }
            }
        }
    }


    protected function checkClassif(&$errors)
    {
        $classifs = ['Udk', 'Grnti', 'Bbk', 'Tbk', 'Okso'];
        $count = 0;

        foreach ($classifs as $cl) {
            $method = 'get'.$cl;
            if (method_exists($this->document, $method)) {
                $count += count($this->document->$method());
            }
        }

        if ($count < 1) {
            $errors[] = 'Должен быть указан хотя бы один классификатор';
        }
    }


    /**
     * @param $options
     * @return string
     */
    public function emassage($options)
    {
        if (!empty($options['lang'])) {
            $field = lcfirst($options['lang']) . '.' . $options['field'];
        } else {
            $field = lcfirst($options['field']);
        }
        $e = ' Поле "' . $this->translator->trans('validation.' . $field) . '" не заполнено.';

        return $e;
    }
}
