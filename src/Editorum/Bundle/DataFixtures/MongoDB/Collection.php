<?php
namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Editorum\Bundle\Document\Collection as CollectionDocument;
use Editorum\Bundle\Document\CollectionSectionList;
use Editorum\Bundle\Document\Conference as ConferenceDocument;
use Editorum\Bundle\Document\Section;

class Collection extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
//        return; 
        $colls = json_decode(file_get_contents('web/jsons_asu/collections.json'), true);

        $manager->clear();
        foreach ($colls as $col) {
            $exists = $manager->getRepository('AsuBundle:Collection')->findOneBy([
                'external_id' => $col['id']
            ]);

            if (null !== $exists) {
                continue;
            }

            $newEntity = new CollectionDocument();
            $ru = [
                'title'      => $col['title'],
                'keywords'   => $col['keywords'],
                'annotation' => $col['annotation'],
            ];
            $en = [
                'title'      => $col['eng_title'],
                'keywords'   => $col['eng_keywords'],
                'annotation' => $col['eng_annotation'],
            ];
            $status = ConferenceDocument::ST_IN_PROGRESS;
            if ((int)$col['is_published'] == 1) {
                $status = ConferenceDocument::ST_CLOSED;
            }
            $statuses = [
                'status'        => $status,
                'is_show_nauka' => (int)$col['is_show_nauka'],
                'is_published'  => (int)$col['is_published'],
                'is_opened'     => 0,
                'in_rinc'     => 0,
                'in_vak'      => 0,
            ];
            $newEntity
                ->setRu($ru)
                ->setEn($en)
                ->setStatuses($statuses)
                ->setExternalId($col['id'])
                ->setAuthorPages($col['author_pages'])
                ->setIsbn($col['isbn'])
                ->setIsbnOnline($col['isbn_online'])
                ->setPublishedPageTotal($col['published_page_total'])
                ->setDoi($col['doi'])
                ->setRincUrl($col['uri'])
                ->setCreatedAt(date_create_from_format('Y-m-d', $col['dt_create']))
            ;
            if ($col['dt_published'] != '0000-00-00') {
                $newEntity->setDatePublished($col['dt_published']);
            }


            if ($col['publisher_id'] > 0 && $this->hasReference('publisher_'.$col['publisher_id'])) {
                $newEntity->setPublisher($this->getReference('publisher_'.$col['publisher_id']));
            } else {
                $newEntity->setPublisher($this->getReference('default_publisher'));
            }

            if ($col['conference_id'] > 0 && $this->hasReference('conference_'.$col['conference_id'])) {
                $conference = $this->getReference('conference_'.$col['conference_id']);
            } else {
                $conference = $this->getReference('empty_conference');
            }

            $newEntity->setConference($conference);
            $manager->persist($newEntity);

            //создаем пустую секцию
            if (!$this->hasReference('empty_section_'.$conference->getId())) {
                //Пустая секция конференции
                $section = new Section();
                $section
                    ->setRu(['title' => 'Пустая секция'])
                    ->setEn(['title' => 'Empty section'])
                    ->setConference($conference);
                $manager->persist($section);

                $this->addReference('empty_section_'.$conference->getId(), $section);

            } else {
                $section = $this->getReference('empty_section_'.$conference->getId());
            }

            $manager->getRepository('AsuBundle:Collection')
                ->addSection($newEntity, $section);

            $this->addReference('collection_'.$col['id'], $newEntity);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 15;
    }
}
