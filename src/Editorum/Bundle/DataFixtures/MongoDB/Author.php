<?php
namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Editorum\Bundle\Document\Author as AuthorDocument;

class Author extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        echo "Start: ".date("Y-m-d H:i:s", time())."\n";

        $authors = json_decode(file_get_contents('web/jsons_asu/authors.json'), true);

        $i = 0;
        $manager->clear();
        foreach ($authors as $author) {
            $i++;
            $exists = $manager->getRepository('AsuBundle:Author')->findOneBy([
                'external_id' => $author['id']
            ]);

            if (null !== $exists) {
                continue;
            }

            $newEntity = new AuthorDocument();
            $ru = [
                'first_name'     => $author['first_name'],
                'last_name'      => $author['last_name'],
                'second_name'    => $author['second_name'],
                'academic_title' => $author['academic_title'],
                'degree'         => $author['degree'],
                'department'     => $author['department'],
                'position'       => $author['position'],
                'adv_title'      => $author['adv_title'],
                'work_title'     => $author['work_title'],
                'work_city'      => $author['work_city'],
                'short'          => $author['last_name'].' '.mb_substr($author['first_name'], 0, 1).'.'.
                    mb_substr($author['second_name'], 0, 1).'.',
                'fio'            =>
                    ucfirst($author['last_name']).' '.ucfirst($author['first_name']).' '.ucfirst($author['second_name'])
            ];
            $en = [
                'first_name'     => $author['eng_first_name'],
                'last_name'      => $author['eng_last_name'],
                'academic_title' => $author['eng_academic_title'],
                'degree'         => $author['eng_degree'],
                'department'     => $author['eng_department'],
                'position'       => $author['eng_position'],
                'adv_title'      => $author['eng_adv_title'],
                'work_title'     => $author['eng_work_title'],
                'work_city'      => $author['eng_work_city'],
                'short'          => $author['eng_last_name'].' '.mb_substr($author['eng_first_name'], 0, 1).'.',
                'fio'            => ucfirst($author['eng_last_name']).' '.ucfirst($author['eng_first_name'])
            ];
            $newEntity
                ->setRu($ru)
                ->setEn($en)
                ->setExternalId($author['id'])
                ->setAdderId($author['adder_id'])
                ->setSnils($author['snils'])
                ->setInn($author['inn'])
                ->setPassSeria($author['pass_seria'])
                ->setPassNumber($author['pass_number'])
                ->setPassDt($author['pass_dt'])
                ->setPassCode($author['pass_code'])
                ->setPassGiven($author['pass_given'])
                ->setRegAdress($author['reg_adress'])
                ->setFactAdress($author['fact_adress'])
                ->setBankReciver($author['bank_reciver'])
                ->setBankName($author['bank_name'])
                ->setBankAccount($author['bank_account'])
                ->setBankCoaccount($author['bank_coaccount'])
                ->setBankBik($author['bank_bik'])
                ->setBirth($author['birth'])
                ->setIsReviewer($author['is_reviewer'])
                ->setCity($author['city'])
                ->setRegIndex($author['reg_index'])
                ->setFactIndex($author['fact_index'])
                ->setScopus($author['scopus'])
                ->setResearcherid($author['researcherid'])
                ->setRinc($author['rinc']) //@ToDo что это???
                ->setEmail($author['email'])
                ->setOrcidId($author['orcid_id'])
                ->setCellPhone($author['phone'])
            ;
            if ($author['region_id'] > 0) {
                $newEntity->setRegion($this->getReference('region_'.$author['region_id']));
            }
            if ($author['country_id'] > 0) {
                if ($this->hasReference('country_'.$author['country_id'])) {
                    $newEntity->setCountry($this->getReference('country_'.$author['country_id']));
                }
            }
            if ($author['university_id'] > 0) {
                if ($this->hasReference('university_'.$author['university_id'])) {
                    $newEntity->setUniversity($this->getReference('university_'.$author['university_id']));
                }
            }

            $manager->persist($newEntity);
            $this->addReference('author_'.$author['id'], $newEntity);

            if ($i%1000 == 0) {
                echo '$i = '.($i)."\n";
                break;
            }
        }

        $manager->flush();

        echo "End: ".date("Y-m-d H:i:s", time())."\n";
    }

    public function getOrder()
    {
        return 10;
    }
}
