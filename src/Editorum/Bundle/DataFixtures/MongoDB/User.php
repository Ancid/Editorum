<?php
namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Editorum\Bundle\Document\User as UserDocument;
use Symfony\Component\Yaml\Yaml;

class User extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $usersData = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'Data' . DIRECTORY_SEPARATOR . 'users.yml');
        $usersData = Yaml::parse($usersData);

        foreach ($usersData as $name => $data) {
            $exists = $manager->getRepository('AsuBundle:User')->findOneBy([
                'email' => $data['email']
            ]);

            /**
             * Если пользователь существует в базе, то просто добавляем роль для него, если она еще не добавлена
             * Срабатывает при установленном параметре --append
             */
            if (null !== $exists && null !== $data['roles']) {
                foreach ($data['roles'] as $role) {
                    if (!$exists->hasRole($this->getReference($role))) {
                        $exists->addRole($this->getReference($role));
                    }
                }

                $this->setReference($data['email'], $exists);

                $manager->persist($exists);
                continue;
            }

            $newUser = new UserDocument();
            $newUser
                ->setUsername($data['username'])
                ->setEmail($data['email'])
                ->setPlainPassword($data['plainPassword'])
                ->setIsAdmin($data['admin'])
                ->setEnabled($data['enabled'])
            ;

            if ($data['roles'] !== null) {
                foreach ($data['roles'] as $role) {
                    $newUser->addRole($this->getReference($role));
                }
            }

            $manager->persist($newUser);

            $this->setReference($data['email'], $newUser);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
