<?php
namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Editorum\Bundle\Document\Common\AbstractPublication;
use Editorum\Bundle\Document\Monography as MonographyDocument;

class Monography extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
//        return;
        echo "Start: ".date("Y-m-d H:i:s", time())."\n";

        $monos = json_decode(file_get_contents('web/jsons_asu/monography.json'), true);
        $manager->clear();
        $i = 0;
        foreach ($monos as $mono) {
            $i++;
            $exists = $manager->getRepository('AsuBundle:Monography')->findOneBy([
                'external_id' => $mono['id']
            ]);

            if (null !== $exists) {
                continue;
            }

            $newEntity = new MonographyDocument();
            $ru = [
                'title'         => $mono['title'],
                'annotation'    => $mono['annotation'],
                'keywords'      => $mono['keywords'],
            ];
            $en = [
                'title'         => $mono['eng_title'],
                'annotation'    => $mono['eng_annotation'],
                'keywords'      => $mono['eng_keywords'],
            ];
            $status = AbstractPublication::ST_IN_WORK;
            if ((int)$mono['is_published'] != 0) {
                $status = AbstractPublication::ST_IS_PUBLISHED;
            }
            $statuses = [
                'status'            => $status,
                'st_doi'            => (int)$mono['st_doi'],
                'is_published'      => (int)$mono['is_published'],
                'is_show_nauka'     => (int)$mono['show_nauka'],
                'has_reviewers'     => (int)$mono['has_reviewers'],
                'has_published'     => (int)$mono['has_published'],
                'has_agreement'     => (int)$mono['has_agreement'],
                'has_images_owned'  => (int)$mono['has_images_owned'],

            ];
            $newEntity
                ->setRu($ru)
                ->setEn($en)
                ->setStatuses($statuses)
                ->setExternalId($mono['id'])
                ->setCreatedAt(date_create_from_format('Y-m-d', $mono['date_create']))
                ->setDatePublished($mono['dt_publication_print'])
                ->setDoi($mono['doi'])
                ->setDateMax($mono['max_dt'])
                ->setRincUrl($mono['uri'])
                ->setAuthorPages($mono['author_pages'])
                ->setPageTotal($mono['published_page_total'])
                ->setPublicationNumber($mono['publication_number'])
                ->setLang(empty($pub['lang']) ? 'RUS' : $pub['lang'])
            ;
            if (!empty($mono['isbn'])) {
                $newEntity->setIsbn($mono['isbn']);
            }
            if (!empty($mono['isbn_online'])) {
                $newEntity->setIsbnOnline($mono['isbn_online']);
            }

            if ($mono['publisher_id'] > 0 && $this->hasReference('publisher_'.$mono['publisher_id'])) {
                $newEntity->setPublisher($this->getReference('publisher_'.$mono['publisher_id']));
            } else {
                $newEntity->setPublisher($this->getReference('default_publisher'));
            }
            if (null != $mono['authors']) {
                $ids = explode(',', $mono['authors']);

                foreach ($ids as $id) {
                    $author = $manager->getRepository('AsuBundle:Author')->findOneBy([
                        'external_id' => (int)$id
                    ]);
                    if (null != $author) {
                        $newEntity->addAuthor($author);
                    }
                }
            }
            
            $manager->persist($newEntity);
            
            if ($i%1000 == 0) {
                echo '$i = '.$i."\n";
                $manager->flush();
                $manager->clear();
            }
        }

        $manager->flush();
        echo "End: ".date("Y-m-d H:i:s", time())."\n";
    }

    public function getOrder()
    {
        return 17;
    }
}
