<?php
namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Editorum\Bundle\Document\Article as ArticleDocument;
use Editorum\Bundle\Document\Journal;

class Article extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
//        return;
        echo "Start: ".date("Y-m-d H:i:s", time())."\n";
        $articles = json_decode(file_get_contents('web/jsons_asu/articles.json'), true);
        $i = 0;
        $manager->clear();
        foreach ($articles as $article) {
            $i++;

            if ($article['parent_id'] > 0) {
                continue;
            }

            $exists = $manager->getRepository('AsuBundle:Article')->findOneBy([
                'external_id' => $article['id']
            ]);

            if (null !== $exists) {
                continue;
            }

            $ru = [
                'title'         => $article['title'],
                'annotation'    => $article['annotation'],
                'keywords'      => $article['keywords'],
            ];
            $en = [
                'title'         => $article['eng_title'],
                'annotation'    => $article['eng_annotation'],
                'keywords'      => $article['eng_keywords'],
            ];
            $statuses = [
                'status'        => Journal::ST_IN_PROGRESS,
                'st_doi'        => (int)$article['st_doi'],
                'is_published'  => (int)$article['is_published'],
                'has_reviewers' => (int)$article['has_reviewers'],
                'has_published' => (int)$article['has_published'],
                'has_agreement' => (int)$article['has_agreement'],
                'is_show_nauka' => (int)$article['show_nauka'],

            ];
            $newEntity = new ArticleDocument();
            $newEntity
                ->setRu($ru)
                ->setEn($en)
                ->setStatuses($statuses)
                ->setExternalId($article['id'])
                ->setCreatedAt(date_create_from_format('Y-m-d', $article['date_create']))
                ->setDatePublished($article['dt_publication_print'])
                ->setDoi($article['doi'])
                ->setAuthorPages($article['author_pages'])
                ->setRincUrl($article['uri'])
                ->setFirstPage($article['first_page'])
                ->setLastPage($article['last_page'])
                ->setAuthorPages($article['published_page_total'])
                ->setLang(empty($article['lang']) ? 'RUS' : $article['lang'])
                ->setText($article['text'])
                ->setPublishedJournal($article['published_journal'])
                ->setPublishedIssue($article['published_issue'])
                ->setIssueOrder($article['issue_iorder'])
            ;

            if (!empty($article['grnti'])) {
                $grnti = $manager->getRepository('AsuBundle:ClassificatorGrnti')->findAll()[0];
                $this->findClassif($newEntity, 'grnti', $grnti->getChildren(), $article['grnti']);
            }

            if ($article['journal'] > 0) {
                if ($this->hasReference('journal_'.$article['journal'])) {
                    $newEntity->setJournal($this->getReference('journal_'.$article['journal']));
                }
            }
            if ($article['issue_id'] > 0) {
                if ($this->hasReference('issue_'.$article['issue_id'])) {
                    $newEntity
                        ->setIssue($this->getReference('issue_'.$article['issue_id']))
                        ->setStatus(
                            'status',
                            $this->getReference('issue_'.$article['issue_id'])->getStatuses()['status']
                        );
                }
            }
            if ($article['issue_rubric_id'] > 0) {
                if ($this->hasReference('rubric_'.$article['issue_rubric_id'])) {
                    $newEntity->setRubric($this->getReference('rubric_'.$article['issue_rubric_id']));
                }
            }
            if ($article['publisher_id'] && $this->hasReference('publisher_'.$article['publisher_id'])) {
                $newEntity->setPublisher($this->getReference('publisher_'.$article['publisher_id']));
            } else {
                $newEntity->setPublisher($this->getReference('default_publisher'));
            }
            
            if (null != $article['authors']) {
                $ids = explode(',', $article['authors']);

                foreach ($ids as $id) {
                    $author = $manager->getRepository('AsuBundle:Author')->findOneBy([
                        'external_id' => (int)$id
                    ]);
                    if (null != $author) {
                        $newEntity->addAuthor($author);
                    }
                }
            }
            $manager->persist($newEntity);

            if ($i%1000 == 0) {
                echo '$i = '.$i."\n";
                $manager->flush();
                $manager->clear();
                break;
            }
//            if ($i%1000 == 0) break;
        }
        $manager->flush();
        echo "End: ".date("Y-m-d H:i:s", time())."\n";
    }


    public function findClassif(ArticleDocument $article, $classif_name, $classif_array, $code)
    {
        foreach ($classif_array as $cl) {
            if (array_key_exists('children', $cl)) {
                $this->findClassif($article, $classif_name, $cl['children'], $code);
            }
            if ($cl['new_code'] == $code) {
//                var_dump($article->getDoi());
                $article->addClassif($classif_name, $cl['new_code']);
                break;
            }
        }
    }

    public function getOrder()
    {
        return 16;
    }
}
