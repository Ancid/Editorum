<?php
namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Editorum\Bundle\Document\Chapter as ChapterDocument;

class Chapter extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $chapters = json_decode(file_get_contents('web/jsons_asu/chapters.json'), true);

        $i = 0;
        foreach ($chapters as $chapter) {
            $i++;
            $exists = $manager->getRepository('AsuBundle:Journal')->findOneBy([
                'external_id' => $chapter['id']
            ]);

            if (null !== $exists) {
                continue;
            }

            $newEntity = new ChapterDocument();
            $ru = [
                'title'      => $chapter['title'],
                'annotation' => $chapter['annotation'],
                'keywords'   => $chapter['keywords'],
            ];
            $en = [
                'title'      => $chapter['title_eng'],
                'annotation' => $chapter['annotation_eng'],
                'keywords'   => $chapter['eng_keywords'],
            ];
            $statuses = [
              'st_text' => (int)$chapter['st_text']
            ];
            $newEntity
                ->setRu($ru)
                ->setEn($en)
                ->setStatuses($statuses)
                ->setExternalId($chapter['id'])
                ->setNumber($chapter['num'])
                ->setText($chapter['text'])
                ->setCreatedAt(date_create_from_format('Y-m-d', $chapter['dt_create']))
                ->setDatePublished($chapter['dt_published'])
                ->setDoi($chapter['doi'])
                ->setZnaniumLink($chapter['znanium_link'])
                ->setFirstPage($chapter['page_from'])
                ->setLastPage($chapter['page_to'])
            ;

            $manager->persist($newEntity);
            switch ($chapter['kind']) {
                case 101:
                    $pub = $manager->getRepository('AsuBundle:Textbook')->findOneBy([
                        'external_id' => (int)$chapter['pub_id']
                    ]);
                    if (null != $pub) {
                        $pub->addChapter($newEntity);
                    }
                    break;
                case 102:
                    $pub = $manager->getRepository('AsuBundle:Monography')->findOneBy([
                        'external_id' => (int)$chapter['pub_id']
                    ]);
                    if (null != $pub) {
                        $pub->addChapter($newEntity);
                    }
                    break;
                default:
                    break;
            }
        }

        $manager->flush();

    }

    public function getOrder()
    {
        return 21;
    }
}
