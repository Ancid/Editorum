<?php
namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SecurityBundle\Document\Role as RoleDocument;
use Symfony\Component\Yaml\Yaml;

/**
 * Class Role
 * @package Editorum\Bundle\DataFixtures\MongoDB
 */
class Role extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $rolesData = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'Data' . DIRECTORY_SEPARATOR . 'roles.yml');
        $rolesData = Yaml::parse($rolesData);

        foreach ($rolesData as $systemName => $additional) {
            $role = $manager->getRepository('AsuSecurityBundle:Role')->findOneBy([
                'systemName'    => $systemName
            ]);

            if (null !== $role) {
                if (null !== $additional['childrens']) {
                    foreach ($additional['childrens'] as $children) {
                        if (!$role->hasChildren($this->getReference($children))) {
                            $role->addChildren($this->getReference($children));
                        }
                    }
                }

                $manager->persist($role);
                $this->addReference($systemName, $role);
                continue;
            }

            $newRole = new RoleDocument();
            $newRole
                ->setSystemName($systemName)
                ->setName($additional['name'])
                ->setDescription($additional['description'])
            ;

            if (null !== $additional['childrens']) {
                foreach ($additional['childrens'] as $children) {
                    $newRole->addChildren($this->getReference($children));
                }
            }

            $manager->persist($newRole);

            $this->addReference($systemName, $newRole);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
