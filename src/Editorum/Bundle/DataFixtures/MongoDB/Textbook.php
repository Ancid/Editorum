<?php
namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Editorum\Bundle\Document\Common\AbstractPublication;
use Editorum\Bundle\Document\Textbook as TextbookDocument;

class Textbook extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
//        return;
        $textbooks = json_decode(file_get_contents('web/jsons_asu/textbooks.json'), true);

        $manager->clear();
        $i = 0;
        foreach ($textbooks as $book) {
            $i++;
            $exists = $manager->getRepository('AsuBundle:Textbook')->findOneBy([
                'external_id' => $book['id']
            ]);

            if (null !== $exists) {
                continue;
            }

            $newEntity = new TextbookDocument();
            $ru = [
                'title'         => $book['title'],
                'annotation'    => $book['annotation'],
                'keywords'      => $book['keywords'],
            ];
            $en = [
                'title'         => $book['eng_title'],
                'annotation'    => $book['eng_annotation'],
                'keywords'      => $book['eng_keywords'],
            ];
            $status = AbstractPublication::ST_IN_WORK;
            if ((int)$book['is_published'] != 0) {
                $status = AbstractPublication::ST_IS_PUBLISHED;
            }
            $statuses = [
                'status'            => $status,
                'st_doi'            => (int)$book['st_doi'],
                'is_published'      => (int)$book['is_published'],
                'is_show_nauka'     => (int)$book['show_nauka'],
                'has_reviewers'     => (int)$book['has_reviewers'],
                'has_published'     => (int)$book['has_published'],
                'has_agreement'     => (int)$book['has_agreement'],
                'has_images_owned'  => (int)$book['has_images_owned'],
                'has_grif'          => (int)$book['has_grif'],

            ];
            $newEntity
                ->setRu($ru)
                ->setEn($en)
                ->setStatuses($statuses)
                ->setExternalId($book['id'])
                ->setCreatedAt(date_create_from_format('Y-m-d', $book['date_create']))
                ->setDatePublished($book['dt_publication_print'])
                ->setDoi($book['doi'])
                ->setIsbn($book['isbn'])
                ->setIsbnOnline($book['isbn_online'])
                ->setDateMax($book['max_dt'])
                ->setRincUrl($book['uri'])
                ->setAuthorPages($book['author_pages'])
                ->setPageTotal($book['published_page_total'])
                ->setPublicationNumber($book['publication_number'])
                ->setLang(empty($pub['lang']) ? 'RUS' : $pub['lang'])
            ;
            if ($book['publisher_id'] > 0 && $this->hasReference('publisher_'.$book['publisher_id'])) {
                $newEntity->setPublisher($this->getReference('publisher_'.$book['publisher_id']));
            } else {
                $newEntity->setPublisher($this->getReference('default_publisher'));
            }
            if (null != $book['authors']) {
                $ids = explode(',', $book['authors']);

                foreach ($ids as $id) {
                    $author = $manager->getRepository('AsuBundle:Author')->findOneBy([
                        'external_id' => (int)$id
                    ]);
                    if (null != $author) {
                        $newEntity->addAuthor($author);
                    }
                }
            }
            
            $manager->persist($newEntity);
            
            if ($i%1000 == 0) {
                echo '$i = '.$i."\n";
                $manager->flush();
                $manager->clear();
            }
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 18;
    }
}
