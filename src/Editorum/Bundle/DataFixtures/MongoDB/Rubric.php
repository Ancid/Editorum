<?php
namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Editorum\Bundle\Document\IssueRubricList;
use Editorum\Bundle\Document\Rubric as RubricDocument;

class Rubric extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
//        return;
        echo "Start: ".date("Y-m-d H:i:s", time())."\n";
        $rubrics = json_decode(file_get_contents('web/jsons_asu/rubrics.json'), true);

        $i = 0;
        foreach ($rubrics as $rubric) {
            $i++;
            $exists = $manager->getRepository('AsuBundle:Rubric')->findOneBy([
                'external_id' => $rubric['id']
            ]);

            if (null !== $exists) {
                continue;
            }

            $newEntity = new RubricDocument();
            $ru = [
              'title'   =>  $rubric['title']
            ];
            $en = [
              'title'   =>  $rubric['title_eng']
            ];
            $newEntity
                ->setRu($ru)
                ->setEn($en)
                ->setExternalId($rubric['id'])
//                ->setOrder($rubric['iorder'])
            ;
            $manager->persist($newEntity);
            
            $journal = $manager->getRepository('AsuBundle:Journal')->findOneBy([
                'external_id' => (int)$rubric['journal_id']
            ]);
            if (null != $journal) {
                $journal->addRubric($newEntity);
            }
                
            $issue_ids = explode(',', $rubric['issue_ids']);
            foreach ($issue_ids as $id) {
                //Cоздаем IssueRubricList для номеров
                if (strpos($id, '-')) {
                    $values = explode('-', $id);
                    $issue = $manager->getRepository('AsuBundle:Issue')->findOneBy([
                        'external_id' => (int)$values[0]
                    ]);
                    if (null != $issue) {
                        $issrub = new IssueRubricList();
                        $issrub->setOrder($values[1]);
                        $issrub->setIssue($issue);
                        $issrub->setRubric($newEntity);
                        $manager->persist($issrub);
                    }
                }
            }
            $pub_ids = explode(',', $rubric['pubs_ids']);
            foreach ($pub_ids as $id) {
                //Связваем статьи с Rubric
                $ids = explode(',', $id);
                foreach ($ids as $art_id) {
                    $art = $manager->getRepository('AsuBundle:Article')->findOneBy([
                        'external_id' => (int)$art_id
                    ]);
                    if (null != $art) {
                        $art->setRubric($newEntity);
                    }
                }
            }
//            $this->addReference('rubric_'.$rubric['id'], $newEntity);

            if ($i%1000 == 0) {
                echo '$i = '.$i."\n";
                $manager->flush();
                $manager->clear();
            }
        }
        
        $manager->flush();
        echo "End: ".date("Y-m-d H:i:s", time())."\n";
    }

    public function getOrder()
    {
        return 23;
    }
}
