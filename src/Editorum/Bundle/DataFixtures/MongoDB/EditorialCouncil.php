<?php
namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Editorum\Bundle\Document\EditorialCouncil as EditorialCouncilDocument;

class EditorialCouncil extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
//        return;
        $editorials = json_decode(file_get_contents('web/jsons_asu/editorial.json'), true);

        foreach ($editorials as $editorial) {
            $exists = $manager->getRepository('AsuBundle:EditorialCouncil')->findOneBy([
                'external_id' => $editorial['id']
            ]);

            if (null !== $exists) {
                continue;
            }

            $ru = [
                'position'    => $editorial['position'],
                'description' => $editorial['description'],
                'fio'         => $editorial['fio'],
                'city'        => $editorial['city'],

            ];
            $en = [
                'position'    => $editorial['position_eng'],
                'description' => $editorial['description_eng'],
                'fio'         => $editorial['fio_eng'],
                'city'        => $editorial['city_eng'],
            ];
            $newEntity = new EditorialCouncilDocument();
            $newEntity
                ->setRu($ru)
                ->setEn($en)
                ->setExternalId($editorial['id'])
                ->setOrder($editorial['iorder'])
            ;
            if ($editorial['journal_id'] > 0) {
                if ($this->hasReference('journal_'.$editorial['journal_id'])) {
                    $newEntity->setJournal($this->getReference('journal_'.$editorial['journal_id']));
                }
            }
            if ($editorial['region_id'] > 0) {
                if ($this->hasReference('region_'.$editorial['region_id'])) {
                    $newEntity->setRegion($this->getReference('region_'.$editorial['region_id']));
                }
            }
            if ($editorial['country_id'] > 0) {
                if ($this->hasReference('country_'.$editorial['country_id'])) {
                    $newEntity->setCountry($this->getReference('country_'.$editorial['country_id']));
                }
            }
            $manager->persist($newEntity);
//            $this->addReference('issue_'.$editorial['id'], $newEntity);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 22;
    }
}
