<?php
namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Editorum\Bundle\Document\Country as CountryDocument;

class Country extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $countries = json_decode(file_get_contents('web/jsons_asu/countries.json'), true);

        foreach ($countries as $country) {
            $exists = $manager->getRepository('AsuBundle:Country')->findOneBy([
                'external_id' => $country['id']
            ]);

            if (null !== $exists) {
                continue;
            }

            $newEntity = new CountryDocument();
            $ru = [
              'title'   =>  $country['title']
            ];
            $en = [
              'title'   =>  $country['eng_title']
            ];
            $newEntity
                ->setRu($ru)
                ->setEn($en)
                ->setExternalId($country['id'])
                ->setCont($country['cont'])
                ->setCity($country['city'])
                ->setCoord1($country['coord1'])
                ->setCoord2($country['coord2'])
            ;

            $manager->persist($newEntity);
            $this->addReference('country_'.$country['id'], $newEntity);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 4;
    }
}
