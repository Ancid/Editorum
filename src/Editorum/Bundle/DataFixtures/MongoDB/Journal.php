<?php
namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Editorum\Bundle\Document\Journal as JournalDocument;

class Journal extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
//        return;
        $journals = json_decode(file_get_contents('web/jsons_asu/journals.json'), true);

        $i = 0;
        foreach ($journals as $journal) {
            $i++;
            $exists = $manager->getRepository('AsuBundle:Journal')->findOneBy([
                'external_id' => $journal['id']
            ]);

            if (null !== $exists) {
                continue;
            }

            $newEntity = new JournalDocument();
            $ru = [
                'title' => $journal['title'],
                'about' => $journal['about'],
                'requirements_for_articles' => $journal['requirements_for_articles'],
                'ethics_publications' => $journal['ethics_publications'],
                'order_of_publication' => $journal['order_of_publication'],
            ];
            $en = [
                'title' => $journal['eng_title'],
                'about' => $journal['about_eng'],
                'requirements_for_articles' => $journal['requirements_for_articles_eng'],
                'ethics_publications' => $journal['ethics_publications_eng'],
                'order_of_publication' => $journal['order_of_publication_eng'],
            ];
            $status = JournalDocument::ST_IN_PROGRESS;
            if ((int)$journal['is_in_work'] == 1) {
                $status = JournalDocument::ST_IS_PUBLISHING;
            }
            $statuses = [
                'status'         => $status,
                'is_vak'         => (int)$journal['is_vak'],
                'is_rinc'        => (int)$journal['is_rinc'],
    //               'is_in_work'     => (int)$journal['is_in_work'],
                'is_show_nauka'  => (int)$journal['is_show_nauka'],
                'st_rubricated'  => (int)$journal['st_rubricated'],
                'is_request_allow' => 0,
            ];
            $newEntity
                ->setRu($ru)
                ->setEn($en)
                ->setStatuses($statuses)
                ->setExternalId($journal['id'])
                ->setCover($journal['cover'])
                ->setSregister($journal['sregister'])
                ->setSdate($journal['sdt'])
                ->setTranslitTitle(empty($journal['translit_title']) ? $journal['hru'] : $journal['translit_title'])
//                ->setGrnti($journal['grnti'])
                ->setAgreementCode($journal['agreement_code'])
                ->setDoi($journal['doi'])
                ->setRincId($journal['rinc_id'])
                ->setRincUrl($journal['rinc_url'])
                ->setIssnOnline($journal['issn_online'])
                ->setIssnPrint($journal['issn'])
            ;
            if ($journal['publisher_id'] > 0 && $this->hasReference('publisher_'.$journal['publisher_id'])) {
                $newEntity->setPublisher($this->getReference('publisher_'.$journal['publisher_id']));
            } else {
                $newEntity->setPublisher($this->getReference('default_publisher'));
            }
            if ($journal['seria_id'] > 0) {
                if ($this->hasReference('serie_'.$journal['seria_id'])) {
                    $newEntity->setSerie($this->getReference('serie_'.$journal['seria_id']));
                }
            }

            $manager->persist($newEntity);
            $this->addReference('journal_'.$journal['id'], $newEntity);
        }

        $manager->flush();

    }

    public function getOrder()
    {
        return 14;
    }
}
