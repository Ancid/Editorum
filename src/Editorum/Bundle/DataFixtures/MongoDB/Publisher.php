<?php
namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Editorum\Bundle\Document\Common\AbstractCorporateEntity;
use Editorum\Bundle\Document\Organization;
use Editorum\Bundle\Document\Publisher as PublisherDocument;

class Publisher extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $publishers = json_decode(file_get_contents('web/jsons_asu/publishers.json'), true);

        $organization = new Organization();
        $organization
            ->setFullname('Группа компаний ИНФРА-М')
            ->setName('Группа компаний ИНФРА-М')
        ;
        $manager->persist($organization);

        foreach ($publishers as $publisher) {
            $exists = $manager->getRepository('AsuBundle:Publisher')->findOneBy([
                'external_id' => $publisher['id']
            ]);

            if (null !== $exists) {
                continue;
            }

            $newEntity = new PublisherDocument();
            $ru = [
                'title' => $publisher['title']
            ];
            $en = [
                'title' => $publisher['eng_title']
            ];
            if (in_array($publisher['kind'], ['Общество с ограниченной ответственностью', 'Corporation'])) {
                $kind = AbstractCorporateEntity::KIND_PUBLISHER;
            } else {
                $kind = $publisher['kind'];
            }
            $newEntity
                ->setRu($ru)
                ->setEn($en)
                ->setExternalId($publisher['id'])
                ->setKind($kind)
                ->setHeadFio($publisher['head_fio'])
                ->setHeadFioGenitive($publisher['head_fio_genitive'])
                ->setDetail($publisher['detail'])
                ->setHeadPost($publisher['head_post'])
                ->setHeadPostGenitive($publisher['head_post_genitive'])
                ->setAddress($publisher['address'])
                ->setAgreementCount($publisher['agreement_count'])
                ->setShortTitle($publisher['short_title'])
                ->setSystemCode($publisher['system_code'])
                ->setTown($publisher['town'])
            ;
            //@ToDo прорефакторить для формы
            $newEntity->setDoiPrefix('ca1011f');

            $manager->persist($newEntity);

            $organization->addCorporate($newEntity);
            $this->addReference('publisher_'.$publisher['id'], $newEntity);
            if ($publisher['title'] == "Общество с ограниченной ответственностью «Научно-издательский центр ИНФРА-М»") {
                $this->addReference('default_publisher', $newEntity);
                $admin = $this->getReference('super.admin@editorum.ru');
                $admin->setCorporate($newEntity);
                $user = $this->getReference('user@test.ru');
                $user->setCorporate($newEntity);
            }
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 11;
    }
}
