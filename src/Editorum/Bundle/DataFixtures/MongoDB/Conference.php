<?php
namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Editorum\Bundle\Document\Conference as ConferenceDocument;

class Conference extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $conferences = json_decode(file_get_contents('web/jsons_asu/conferences.json'), true);

        $empty = new ConferenceDocument();
        $empty
            ->setRu(['title'     => ConferenceDocument::DEFAULT_TITLE])
            ->setEn(['title'     => ConferenceDocument::DEFAULT_TITLE])
            ->setStatuses([
                'status'           => ConferenceDocument::ST_CLOSED,
                'is_show_nauka'    => 0,
                'is_rinc'          => 0,
            ])
//            ->setDateStart('0000-00-00')
//            ->setDateFinish('0000-00-00')
        ;
        $manager->persist($empty);
        $manager->flush();
        $this->addReference('empty_conference', $empty);

        $manager->clear();
        foreach ($conferences as $conf) {
            $exists = $manager->getRepository('AsuBundle:Conference')->findOneBy([
                'external_id' => $conf['id']
            ]);

            if (null !== $exists) {
                continue;
            }

            $newEntity = new ConferenceDocument();
            $ru = [
                'title'     => $conf['title'],
                'city'      => $conf['city'],
            ];
            $en = [
                'title'     => $conf['title_eng'],
                'city'      => $conf['city_eng'],
            ];

            $now = new \DateTime();
            $st = ConferenceDocument::ST_INACTIVE;
            if (strtotime(strtotime($conf['dt_start'])) - strtotime($now->format('Y-m-d')) < 0) {
                $st = ConferenceDocument::ST_ACTIVE;
            }
            if (strtotime(strtotime($conf['dt_finish'])) - strtotime($now->format('Y-m-d')) < 0) {
                $st = ConferenceDocument::ST_CLOSED;
            }
            $statuses = [
                'status'        => $st,
                'is_show_nauka' => (int)$conf['is_show_nauka'],
                'is_in_work'    => 0,
                'is_in_rinc'    => 0,
            ];
            $newEntity
                ->setRu($ru)
                ->setEn($en)
                ->setStatuses($statuses)
                ->setExternalId($conf['id'])
                ->setNumber($conf['num'])
                ->setHtml($conf['html'])
                ->setDateStart($conf['dt_start'])
                ->setDateFinish($conf['dt_finish'])
            ;
            if ($this->hasReference('publisher_'.$conf['publisher_id'])) {
                $newEntity->setPublisher($this->getReference('publisher_'.$conf['publisher_id']));
            } else {
                $newEntity->setPublisher($this->getReference('default_publisher'));
            }
            if ($conf['country_id'] > 0 && $this->hasReference('country_'.$conf['country_id'])) {
                $newEntity->setCountry($this->getReference('country_'.$conf['country_id']));
            }

            $manager->persist($newEntity);
            $this->addReference('conference_'.$conf['id'], $newEntity);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 12;
    }
}
