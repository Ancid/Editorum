<?php
namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Editorum\Bundle\Document\Serie as SerieDocument;

class Serie extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $series = json_decode(file_get_contents('web/jsons_asu/series.json'), true);

        foreach ($series as $serie) {
            $exists = $manager->getRepository('AsuBundle:Serie')->findOneBy([
                'external_id' => $serie['id']
            ]);

            if (null !== $exists) {
                continue;
            }

            $newEntity = new SerieDocument();
            $ru = [
              'title'   =>  $serie['title'],
              'about'   =>  $serie['about'],
            ];
            $en = [
              'title'   =>  $serie['eng_title'],
              'about'   =>  $serie['eng_about'],
            ];
            $newEntity
                ->setRu($ru)
                ->setEn($en)
                ->setExternalId($serie['id'])
            ;

            $manager->persist($newEntity);
            $this->addReference('serie_'.$serie['id'], $newEntity);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 13;
    }
}
