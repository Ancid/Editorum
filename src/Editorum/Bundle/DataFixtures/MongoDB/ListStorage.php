<?php
namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Editorum\Bundle\Document\ListStorage as ListStorageDocument;
use Symfony\Component\Yaml\Yaml;

class ListStorage extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $listsData = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'Data' . DIRECTORY_SEPARATOR . 'lists.yml');
        $listsData = Yaml::parse($listsData);

        foreach ($listsData as $listName => $list) {
            foreach ($list as $value) {
                $listStorage = $manager->getRepository('AsuBundle:ListStorage')->findOneBy([
                    'listName'  => $listName,
                    'ru'  => $value['ru']['title'],
                ]);

                if (null === $listStorage) {
                    $listStorage = new ListStorageDocument();
                }

                $listStorage
                    ->setListName($listName)
                    ->setRu($value['ru'])
                    ->setEn($value['en'])
                ;

                $manager->persist($listStorage);
                $this->setReference($listName . '_' . $listStorage->getId(), $listStorage);
            }
        }

        $manager->flush();
    }


    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 0;
    }
}
