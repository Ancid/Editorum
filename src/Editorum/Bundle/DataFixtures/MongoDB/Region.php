<?php
namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Editorum\Bundle\Document\Region as RegionDocument;

class Region extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $regions = json_decode(file_get_contents('web/jsons_asu/regions.json'), true);

        foreach ($regions as $region) {
            $exists = $manager->getRepository('AsuBundle:Region')->findOneBy([
                'external_id' => $region['id']
            ]);

            if (null !== $exists) {
                continue;
            }

            $newEntity = new RegionDocument();
            $ru = [
              'title'   =>  $region['title']
            ];
            $en = [
              'title'   =>  $region['title_eng']
            ];
            $newEntity
                ->setRu($ru)
                ->setEn($en)
                ->setExternalId($region['id'])
                ->setFederal($region['federal'])
                ->setCity($region['city'])
                ->setCoord1($region['coord1'])
                ->setCoord2($region['coord2'])
            ;

            $manager->persist($newEntity);
            $this->addReference('region_'.$region['id'], $newEntity);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }
}
