<?php
namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Editorum\Bundle\Document\Issue as IssueDocument;
use Editorum\Bundle\Document\Journal;


class Issue extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
//        return;
        $issues = json_decode(file_get_contents('web/jsons_asu/issues.json'), true);

        foreach ($issues as $issue) {
            $exists = $manager->getRepository('AsuBundle:Issue')->findOneBy([
                'external_id' => $issue['id']
            ]);

            if (null !== $exists) {
                continue;
            }

            $status = Journal::ST_IN_PROGRESS;
            if ((int)$issue['is_published'] == 1) {
                $status = Journal::ST_IS_PUBLISHED;
            }
            $statuses = [
                'status'         => $status,
                'st_rinc'        => (int)$issue['st_rinc'],
                'is_published'   => (int)$issue['is_published'],
                'is_show_nauka'  => (int)$issue['is_published'],
                'is_finalized'   => (int)$issue['is_finalized'],
            ];
            $newEntity = new IssueDocument();
            $newEntity
                ->setExternalId($issue['id'])
                ->setStatuses($statuses)
                ->setCreatedAt(date_create_from_format('Y-m-d', $issue['dt_created']))
                ->setDatePublished($issue['dt_published'])
                ->setVolume($issue['volume'])
                ->setIssue($issue['issue'])
                ->setDoi($issue['doi'])
                ->setPageCount($issue['pagecount'])
                ->setPart($issue['part'])
                ->setTypeAccess($issue['type_access'])
            ;
            if ($issue['journal_id'] > 0) {
                if ($this->hasReference('journal_'.$issue['journal_id'])) {
                    /** @var Journal $journal */
                    $journal = $this->getReference('journal_'.$issue['journal_id']);
                    $newEntity
                        ->setJournal($journal)
                        ->setPublisher($journal->getPublisher());
                    //Добавляем номер только если есть его журнал
                    $manager->persist($newEntity);
                    $this->addReference('issue_'.$issue['id'], $newEntity);
                }
            }
        }
        
        $manager->flush();
    }

    public function getOrder()
    {
        return 15;
    }
}
