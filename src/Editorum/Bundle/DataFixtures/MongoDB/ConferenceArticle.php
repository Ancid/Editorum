<?php
namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Editorum\Bundle\Document\Common\AbstractClassificator;
use Editorum\Bundle\Document\Conference as ConferenceDocument;
use Editorum\Bundle\Document\ConferenceArticle as ConferenceArticleDocument;

class ConferenceArticle extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        echo "Start: ".date("Y-m-d H:i:s", time())."\n";
        $articles = json_decode(file_get_contents('web/jsons_asu/conference_articles.json'), true);
        $i = 0;
        $manager->clear();
        foreach ($articles as $article) {
//            var_dump($article['grnti']);
//            var_dump($article['udk']);
//            die();
            $i++;

            $newEntity = new ConferenceArticleDocument();
            $exists = $manager->getRepository('AsuBundle:ConferenceArticle')->findOneBy([
                'external_id' => $article['id']
            ]);

            if (null !== $exists) {
                continue;
            }

            $ru = [
                'title'         => $article['title'],
                'annotation'    => $article['annotation'],
                'keywords'      => $article['keywords'],
            ];
            $en = [
                'title'         => $article['eng_title'],
                'annotation'    => $article['eng_annotation'],
                'keywords'      => $article['eng_keywords'],
            ];
            $statuses = [
                'status'        => ConferenceDocument::ST_IN_PROGRESS,
                'st_doi'        => (int)$article['st_doi'],
                'is_published'  => (int)$article['is_published'],
                'has_reviewers' => (int)$article['has_reviewers'],
                'has_published' => (int)$article['has_published'],
                'has_agreement' => (int)$article['has_agreement'],
                'is_show_nauka' => (int)$article['show_nauka'],

            ];
            $newEntity
                ->setRu($ru)
                ->setEn($en)
                ->setStatuses($statuses)
                ->setExternalId($article['id'])
                ->setCreatedAt(date_create_from_format('Y-m-d', $article['date_create']))
                ->setDatePublished($article['dt_publication_print'])
                ->setDoi($article['doi'])
                ->setAuthorPages($article['author_pages'])
                ->setRincUrl($article['uri'])
                ->setFirstPage($article['first_page'])
                ->setLastPage($article['last_page'])
                ->setAuthorPages($article['published_page_total'])
                ->setLang(empty($article['lang']) ? 'RUS' : $article['lang'])
                ->setText($article['text'])
                ->setOrder($article['parent_order'])
            ;

            if (!empty($article['grnti'])) {
                $grnti = $manager->getRepository('AsuBundle:ClassificatorGrnti')->findAll()[0];
                $this->findClassif($newEntity, 'grnti', $grnti->getChildren(), $article['grnti']);
            }

            if ($article['publisher_id'] > 0 && $this->hasReference('publisher_'.$article['publisher_id'])) {
                $newEntity->setPublisher($this->getReference('publisher_'.$article['publisher_id']));
            } else {
                $newEntity->setPublisher($this->getReference('default_publisher'));
            }

//            if ($article['conference_id'] > 0) {
//                if ($this->hasReference('conference_'.$article['conference_id'])) {
//                    $newEntity->setConference($this->getReference('conference_'.$article['conference_id']));
//                }
//            }

            if ($article['collection_id'] > 0 && $this->hasReference('collection_'.$article['collection_id'])) {
                $collection = $this->getReference('collection_'.$article['collection_id']);
                //Привязываем к сборнику
                $newEntity->setCollection($collection);
                //Привязываем к конференции сборника
                $newEntity->setConference($collection->getConference());

                $conf_id = $collection->getConference()->getId();
                if ($this->hasReference('empty_section_'.$conf_id)) {
                    $newEntity->setSection($this->getReference('empty_section_'.$conf_id));
                }
            } else {
                $newEntity->setConference($this->getReference('empty_conference'));
            }


            if (null != $article['authors']) {
                $ids = explode(',', $article['authors']);

                foreach ($ids as $id) {
                    $author = $manager->getRepository('AsuBundle:Author')->findOneBy([
                        'external_id' => (int)$id
                    ]);
                    if (null != $author) {
                        $newEntity->addAuthor($author);
                    }
                }
            }
            $manager->persist($newEntity);

            if ($i%1000 == 0) {
                echo '$i = '.$i."\n";
                $manager->flush();
                $manager->clear();
//                break;
            }
        }
        $manager->flush();
        echo "End: ".date("Y-m-d H:i:s", time())."\n";
    }


    public function findClassif(ConferenceArticleDocument $article, $classif_name, $classif_array, $code)
    {
        foreach ($classif_array as $cl) {
            if (array_key_exists('children', $cl)) {
                $this->findClassif($article, $classif_name, $cl, $code);
            }
            if ($cl['new_code'] == $code) {
                $article->addClassif($classif_name, $cl['new_code']);
                break;
            }
        }
    }

    public function getOrder()
    {
        return 16;
    }
}
