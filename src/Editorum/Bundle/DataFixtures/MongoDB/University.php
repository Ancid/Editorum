<?php
namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Editorum\Bundle\Document\University as UniversityDocument;

class University extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $univers = json_decode(file_get_contents('web/jsons_asu/universities.json'), true);
        foreach ($univers as $univ) {
            $exists = $manager->getRepository('AsuBundle:Publisher')->findOneBy([
                'external_id' => $univ['id']
            ]);

            if (null !== $exists) {
                continue;
            }

            $newEntity = new UniversityDocument();
            $ru = [
                'title' => $univ['title'],
                'city' => $univ['city'],
            ];
            $en = [
                'title' => $univ['eng_title'],
                'city' => $univ['eng_city'],
            ];
            $newEntity
                ->setRu($ru)
                ->setEn($en)
                ->setExternalId($univ['id'])
            ;
            if ($univ['country_id'] > 0) {
                if ($this->hasReference('country'.$univ['country_id'])) {
                    $newEntity->setCountry($this->getReference('country'.$univ['country_id']));
                }
            }
            $manager->persist($newEntity);
            $this->addReference('university_'.$univ['id'], $newEntity);
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 5;
    }
}
