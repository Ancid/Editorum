<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 05.09.2016
 * Time: 12:38
 */

namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
//use Editorum\Bundle\Document\ClassificatorUdk;


class Classificator extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        echo "Start: ".date("Y-m-d H:i:s", time())."\n";

        $files = ['udk', 'grnti', 'okso', 'bbk', 'tbk'];
        $this->loadClassifs($files, $manager);

        echo "End: ".date("Y-m-d H:i:s", time())."\n";
    }


    public function getOrder()
    {
        return 6;
    }


    protected function loadClassifs($files, $manager)
    {
        foreach ($files as $file) {
            $items = json_decode(file_get_contents('web/rubricator/'.$file.'.json'), true);
            $class = 'Classificator'.ucfirst($file);

            $object = 'Editorum\\Bundle\\Document\\' . $class;


            if (!class_exists($object)) {
                var_dump("Class $object does not exist!");
                die();
            }

            $classif = new $object();
            $classif->setSystemName($file);
            $this->addChildren($items, $classif);

            $manager->persist($classif);
            $manager->flush();
        }
    }


    protected function addChildren($array, $classif)
    {
        if (array_key_exists('children', $array)) {
            foreach ($array['children'] as $item) {
                $classif->addChild($item);
            }
        }
    }
}
