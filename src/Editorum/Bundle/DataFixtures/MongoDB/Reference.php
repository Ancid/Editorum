<?php
namespace Editorum\Bundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Editorum\Bundle\Document\Reference as ReferenceDocument;

class Reference extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
//        return;
        echo "Start: ".date("Y-m-d H:i:s", time())."\n";
        $references = json_decode(file_get_contents('web/jsons_asu/references.json'), true);
        $chunks = array_chunk($references, 20000);
        foreach ($chunks as $ref) {
            $this->loadRefs($manager, $ref, 1000);
            break;
        }

        echo "End: ".date("Y-m-d H:i:s", time())."\n";
    }

    public function getOrder()
    {
        return 30;
    }

    public function loadRefs($manager, $refs, $print_separator = 10000, $flush_separator = 1000)
    {
        $manager->clear();
        $i = 0;
        foreach ($refs as $ref) {
            $i++;
            $exists = $manager->getRepository('AsuBundle:Reference')->findOneBy([
                'external_id' => $ref['id']
            ]);

            if (null !== $exists) {
                continue;
            }

            $newEntity = new ReferenceDocument();
            $newEntity
                ->setExternalId($ref['id'])
                ->setRu([
                    'text' => $ref['text']
                ])
                ->setEn([
                    'text' => $ref['text_translit']
                ])
                ->setPosition($ref['number'])
            ;
            $manager->persist($newEntity);
            
            //@ToDo Добавить определение статьи конференции
            switch ($ref['kind']) {
                case 100:
                    $odm = $manager->getRepository('AsuBundle:Article');
                    break;
                case 101:
                    $odm = $manager->getRepository('AsuBundle:Textbook');
                    break;
                case 102:
                    $odm = $manager->getRepository('AsuBundle:Monography');
                    break;
                default:
                    break;
            }
            $pub = $odm->findOneBy([
                'external_id' => (int)$ref['pub_id']
            ]);
            if (null != $pub) {
                $pub->addReference($newEntity);
            }

            if ($i%$print_separator == 0) {
                echo '$i = '.($i);
                echo ', size:'.$manager->getUnitOfWork()->size();
                echo ", ".date("i:s", time())."\n";
            }
            
            if ($i%$flush_separator == 0) {
                $manager->flush();
                $manager->clear();
                gc_collect_cycles();
            }
            
            if ($i == 10000) {
                return;
            }
        }
    }
}
