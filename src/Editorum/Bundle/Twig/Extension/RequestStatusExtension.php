<?php
namespace Editorum\Bundle\Twig\Extension;

use Editorum\Bundle\Document\Request;

class RequestStatusExtension extends \Twig_Extension implements \Twig_ExtensionInterface
{
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('request_status', [$this, 'status']),
        ];
    }

    /**
     * @param int $sid
     *
     * @return array
     */
    public function status($sid)
    {
        $array = [];
        switch ($sid) {
            case Request::STATUS_NEW:
                $array = [ 'class' => 'primary', 'msg' => 'новая' ];
                break;
            case Request::STATUS_APPROVED:
                $array = ['class' => 'success', 'msg' => 'принята'];
                break;
            case Request::STATUS_DECLINED:
                $array = ['class' => 'danger', 'msg' => 'отклонена'];
                break;
            case Request::STATUS_RETURNED:
                $array = ['class' => 'warning', 'msg' => 'возвращена на доработку'];
                break;
        }

        return $array;
    }
    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'request_status';
    }
}
