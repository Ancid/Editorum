<?php
namespace Editorum\Bundle\Twig\Extension;

use Twig_Extension;

class SwitchExtension extends Twig_Extension
{

    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('price', array($this, 'priceFilter')),
        );
    }

    /**
     * @return array
     */
    public function getTokenParsers()
    {
        return [
            new SwitchTokenParser(),
        ];
    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'switch_extension';
    }
}
