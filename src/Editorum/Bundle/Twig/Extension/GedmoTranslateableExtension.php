<?php
namespace Editorum\Bundle\Twig\Extension;

use Doctrine\ODM\MongoDB\DocumentManager;
use Twig_Extension;

class GedmoTranslateableExtension extends Twig_Extension
{
    /** @var  DocumentManager $odm */
    protected $odm;

    /**
     * GedmoTranslateableExtension constructor.
     * @param DocumentManager $odm
     */
    public function __construct(DocumentManager $odm)
    {
        $this->odm = $odm;
    }

    /**
     * @param object $document
     * @return array
     */
    public function gedmoTranslate($document)
    {
        $repository = $this->odm->getRepository('Gedmo\Translatable\Document\Translation');
        return $repository->findTranslations($document);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gedmo_translate';
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('gedmoTranslate', [ $this, 'gedmoTranslate']),
        ];
    }
}
