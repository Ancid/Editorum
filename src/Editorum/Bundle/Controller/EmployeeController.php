<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 28.08.2015
 * Time: 12:19
 *
 * @ToDO: отрефакторить при слиянии с веткой VKR
 */

namespace Editorum\Bundle\Controller;

use Editorum\Bundle\AgreementSystem\AgreementCache;
use Editorum\Bundle\AgreementSystem\BasicSide;
use Editorum\Bundle\Logic\Organization;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Document\User;
use Editorum\Bundle\Logic\Universities;
use Editorum\Bundle\Logic\Model;

/**
 * Class EmployeeController
 * @package Editorum\Bundle\Controller
 *
 * @Security("is_granted('ROLE_ADMIN')")
 */
class EmployeeController extends BasicController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function defaultAction()
    {
    }

    /**
     * Управление пользователями
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $user = $this->getUser();
        
        $users = $this->get('doctrine.odm.mongodb.document_manager')
            ->getRepository('AsuBundle:User')->createQueryBuilder();

        $users = $this
            ->get('knp_paginator')
            ->paginate(
                $users,
                $request->get('page', 1),
                30
            );

        return $this->render('AsuBundle:Default:_user_manager.html.twig', [
            'username' => isset($user) ? $user->getUsername() : '',
            'users' => $users
        ]);
    }

    /**
     * Создание/редактирование пользователя
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id = 0)
    {
        $user = $this->getUser();
        if ($id == 0) {
            $user_to_edit = new User();
            $title = 'Создание нового пользователя';
        } else {
            $user_to_edit = $this->get('doctrine_mongodb')
                ->getRepository('UserBundle:User')
                ->findOneBy(array('_id' => $id));
            $title = 'Редактирование пользователя';
        }
        $agreements = $this->ASys->getAgreementsByUser($user_to_edit->getId(), null, null);
        $orgs = Model::getAll(new Organization());
        $univ = Model::getAll(new Universities());
        return $this->render('AsuBundle:Default:_user_edit.html.twig', [
            'username' => isset($user) ? $user->getUsername() : '',
            'user' => $user_to_edit,
            'agreements' => $agreements,
            'organizations' => $orgs,
            'universities' => $univ,
            'title' => $title
        ]);
    }

    /**
     * Сохранение данных пользователя
     * @param Request $request
     * @return mixed
     */
    public function saveAction(Request $request)
    {
        /* @var \UserBundle\Document\User $user_to_save */
        $user_to_save = null;
        if ($request->get('_id') !== '') {
            $user_to_save = $this->get('fos_user.user_manager')->findUserBy(array('id' => $request->get('_id')));
        } else {
            $user_to_save = $this->get('fos_user.user_manager')->createUser();
        }
        $user_to_save->setUsername($request->get('username'));
        $user_to_save->setEmail($request->get('email'));
        $user_to_save->setPlainPassword($request->get('password'));
        $user_to_save->setEnabled(true);
        $user_to_save->setUniversityId($request->get('university_id'));
        $this->get('fos_user.user_manager')->updateUser($user_to_save);
        
        if ($request->get('organization_id')) {
            /*
             * Registered EmployeeAgreement
             */
            /* @var \Editorum\Bundle\AgreementSystem\Agreements\EmployeeAgreement $agreement */
            $agreement = $this->ASys->getAgreementObject('EmployeeAgreement');
            $agreement->create($request->get('organization_id'), $user_to_save->getId());
            if ($this->ASys->saveAgreement($agreement)) {
                $this->ASys->executeAgreement($agreement);
            }
            
            if ($agreement_type = $request->get('agreement_type')) {
                $flag = false;
                switch ($agreement_type) {
                    case 'chief_editor':
                        $agreement_type = 'ChiefEditorAgreement';
                        $flag = true;
                        break;
                    
                    case 'editor':
                        $agreement_type = 'EditorAgreement';
                        $flag = true;
                        break;
                    
                    case 'editoral_manager':
                        $agreement_type = 'EditoralManagerAgreement';
                        $flag = true;
                        break;
                    
                    case 'elsa_admin':
                        $agreement_type = 'ELSAAdministratorAgreement';
                        $flag = true;
                        break;
                }
                if ($flag) {
                    /* @var \Editorum\Bundle\AgreementSystem\Agreements\EmployeeAgreement $agreement */
                    $agreement = $this->ASys->getAgreementObject($agreement_type);
                    $agreement->create($request->get('organization_id'), $user_to_save->getId());
                    if ($this->ASys->saveAgreement($agreement)) {
                        $this->ASys->executeAgreement($agreement);
                    }
                }
            }
        }
        
        if ($request->get('university_id')) {
            /*
             * Registered UniversityManagerAgreement
             */
            /* @var \Editorum\Bundle\AgreementSystem\Agreements\UniversityManagerAgreement $agreement */
            $agreement = $this->ASys->getAgreementObject('UniversityManagerAgreement');
            $agreement->create($request->get('university_id'), $user_to_save->getId());
            if ($this->ASys->saveAgreement($agreement)) {
                $this->ASys->executeAgreement($agreement);
            }
        }

        return $this->redirectToRoute('employee_index', [], 301);
    }

    /**
     * Удаление пользователя
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @internal param int $id
     */
    public function dropAction($id)
    {
        if ($user = $this->get('fos_user.user_manager')->findUserBy(['id' => $id])) {
            $this->get('fos_user.user_manager')->deleteUser($user);
            
            // Clean user agreements cache
            Model::removeDataset(new AgreementCache(), ['user_id' => $id]);
            
            // Clean user agreements
            $source = new BasicSide(BasicSide::SRC_SIDE);
            $source->addPerson($id);

            $destination = new BasicSide(BasicSide::DST_SIDE);
            $destination->addPerson($id);
            
            Model::removeDataset(
                $this->ASys->getAgreementObject('EmployeeAgreement'),
                [
                    '$or' => [
                        $source->save([]),
                        $destination->save([])
                    ]
                ]
            );
        }
        
        return $this->redirectToRoute('employee_index', [], 301);
    }

}
