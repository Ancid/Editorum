<?php
namespace Editorum\Bundle\Controller\Request;

use Editorum\Bundle\Controller\BasicController;
use Editorum\Bundle\Document\Common\AbstractPublication;
use Editorum\Bundle\Document\Conference;
use Editorum\Bundle\Document\ConferenceArticle;
use Editorum\Bundle\Document\Request as RequestDocument;
use Symfony\Component\HttpFoundation\JsonResponse;
use \Symfony\Component\HttpFoundation\Request;

class ConferenceController extends BasicController
{
    public function indexAction(Request $request)
    {
        $requestQb = $this->getODM()->getRepository('AsuBundle:Request')->createQueryBuilder();

        $this->get('publication.statuses_filter')
            ->prepareData($request->get('filter'))
            ->setPrefix(null)
            ->makeFilter($requestQb)
        ;

        $requestQb
            ->addAnd(
                $requestQb->expr()->field('reference.$ref')->equals('conference')
            );

        //Проверка издателя с учетом роли пользователя
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $corp = [];
            foreach ($this->getUser()->getCorporate()->getOrganization()->getCorporates()->toArray() as $co) {
                $corp[$co->getKind()][] = $co->getId();
            }
            foreach ($corp as $type => $ids) {
                $requestQb
                    ->addOr(
                        $requestQb->expr()
                            ->addAnd($requestQb->expr()->field('publisher.$ref')->equals($type))
                            ->addAnd($requestQb->expr()->field('publisher.$id')->in($ids))
                    );
            }
        } else {
            $requestQb->field('publisher')->references($this->getUser()->getCorporate());
        }

        /** @var RequestDocument[]|null $requests */
        $requests = $this->get('knp_paginator')->paginate(
            $requestQb,
            $request->get('page', 1),
            30
        );

        return $this->render('AsuBundle:Default/Request:index.html.twig', [
            'requests' => $requests
        ]);
    }

    public function setNewStatusAction(RequestDocument $requestDocument, Request $request)
    {
        $requestDocument->setStatus((int) $request->get('status'));
        if ((int)$request->get('status') === $requestDocument::STATUS_APPROVED) {
            /** @var ConferenceArticle $publication */
            $publication = $requestDocument->getPublication();
            $publication
                ->setConference($requestDocument->getReference())
                ->setPublisher($requestDocument->getReference()->getPublisher())
                ->setStatus('status', Conference::ST_IN_PROGRESS);

            $this->getODM()->persist($publication);
        }


        if ($request->get('comment')) {
            $requestDocument->setComment($request->get('comment'));
        }

        $this->getODM()->persist($requestDocument);
        $this->getODM()->flush();
        $this->addFlash('success', 'Статус изменен');

        $message = \Swift_Message::newInstance()
            ->setTo($requestDocument->getUser()->getEmail())
            ->setFrom($this->getParameter('mailer_sender_address'))
            ->setSubject('Изменился статус заявки на публикацию на портале Editorum!')
            ->setBody(
                $this->renderView('AsuBundle:Parts/Mail:request_mail.html.twig', [
                    'id'  => $requestDocument->getId(),
                    'sid' => (int)$request->get('status')
                ]),
                'text/html'
            );

        $this->get('mailer')->send($message);

        return new JsonResponse([ 'id' => $requestDocument->getId() ]);
    }
}
