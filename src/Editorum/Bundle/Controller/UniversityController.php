<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 18.03.2016
 * Time: 11:48
 *
 * @ToDO: отрефакторить при слиянии с веткой VKR
 */

namespace Editorum\Bundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Editorum\Bundle\Logic\Countries;
use Editorum\Bundle\Logic\Model;
use Editorum\Bundle\Logic\Universities;

/**
 * Class UniversityController
 * @package Editorum\Bundle\Controller
 *
 * @Security("is_granted('ROLE_UNIVERSITY_GROUP')")
 */
class UniversityController extends BasicController
{
    /**
     * Менеджер ВУЗов
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function managerAction()
    {
        $user = $this->getUser();
        $universities = Model::getAll(new Universities());
        return $this->render('AsuBundle:Default:_university_manager.html.twig', [
            'username'      => isset($user) ? $user->getUsername() : '',
            'universities'  => $universities
        ]);
    }
    
    /**
     * Добавление/редактирование ВУЗа
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id = 0)
    {
        $user = $this->getUser();
        if ($id > 0) {
            $university = Model::getById(new Universities(), $id);
            $title = 'Редактирование ВУЗа';
        } else {
            $university = new Universities();
            $title = 'Создание нового ВУЗа';
        }
        $countries = Model::getAll(new Countries());
        return $this->render('AsuBundle:Default:_university_edit.html.twig', array(
            'username'      => isset($user) ? $user->getUsername() : '',
            'university'    => $university,
            'countries'     => $countries,
            'title'         => $title
        ));
    }
    
    /**
     * Созранение ВУЗа
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function saveAction(Request $request)
    {
        $univ = new Universities();
        $data_to_save['_id'] = $request->get('_id');
        $data_to_save['title'] = $request->get('title');
        $data_to_save['eng_title'] = $request->get('eng_title');
        $data_to_save['city'] = $request->get('city');
        $data_to_save['eng_city'] = $request->get('eng_city');
        $data_to_save['country_id'] = $request->get('country_id');
        $univ->save($data_to_save);
        
        return $this->redirectToRoute('university_index', [], 301);
    }
    
    /**
     * Удаление ВУЗа
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id = 0)
    {
        if ($id > 0) {
            Model::remove(new Universities(), array('_id' => $id));
        }
        return $this->redirectToRoute('university_index', [], 301);
    }
    
}
