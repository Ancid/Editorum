<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 24.08.2015
 * Time: 9:07
 */
namespace Editorum\Bundle\Controller;

use Doctrine\ORM\NoResultException;
use Editorum\Bundle\Document\Article;
use Editorum\Bundle\Document\Common\AbstractPublication;
use Editorum\Bundle\Document\Doi;
use Editorum\Bundle\Document\Issue as IssueDocument;
use Editorum\Bundle\Document\Issue;
use Editorum\Bundle\Document\IssueRubricList;
use Editorum\Bundle\Document\Journal as JournalDocument;
use Editorum\Bundle\Document\Rubric;
use Editorum\Bundle\Form\Journal\IssueType;
use Editorum\Bundle\Form\Rubric\IssueRubricType;
use JournalsBundle\Logic\Search;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class IssueController
 * @package Editorum\Bundle\Controller
 *
 * @Security("is_granted('ROLE_ISSUE_GROUP')")
 */
class IssueController extends BasicController
{
    /**
     * Список номеров
     *
     * @param $journal_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($journal_id, Request $request)
    {
        $user = $this->getUser();
        $search_txt = $request->get('search');
        $qb = $this->getODM()->createQueryBuilder('AsuBundle:Issue');

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $corp = [];
            foreach ($this->getUser()->getCorporate()->getOrganization()->getCorporates()->toArray() as $co) {
                $corp[$co->getKind()][] = $co->getId();
            }
            foreach ($corp as $type => $ids) {
                $qb
                    ->addOr(
                        $qb->expr()
                            ->addAnd($qb->expr()->field('publisher.$ref')->equals($type))
                            ->addAnd($qb->expr()->field('publisher.$id')->in($ids))
                    );
            }
        } else {
            $qb->field('publisher')->references($user->getCorporate());
        }

        //@ToDo прорефакторить
        if (!is_null($search_txt) && strlen($search_txt) > 0) {
            $results = Search::getIds($search_txt, Journal::KIND_JOURNAL);
            $qb
                ->field('journal')
                ->in($results['matches']);
        } else {
            if ($journal_id > 0) {
                $qb
                    ->field('journal')
                    ->equals((int) $journal_id);
            }
        }
        $issues = $qb->sort('id', 'DESC');

        if ($request->get('filter')) {
            $filter = $this->get('publication.statuses_filter');

            $filter
                ->prepareData($request->get('filter'))
                ->makeFilter($issues);
        }

        $paginator = $this->get('knp_paginator');
        $issues = $paginator->paginate(
            $issues,
            $request->get('page', 1),
            30
        );

        return $this->render('AsuBundle:Default/issue:issues.html.twig', [
            'issues'        => $issues,
            'kinds'         => JournalDocument::getKinds(),
            'st_published'  => JournalDocument::ST_IS_PUBLISHED,
        ]);
    }


    /**
     * Просмотр номера
     *
     * @param IssueDocument $issue
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(IssueDocument $issue)
    {
        return $this->render('AsuBundle:Default/issue:issue_show.html.twig', [
            'issue'     => $issue,
        ]);
    }

    
    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param JournalDocument $journal
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(JournalDocument $journal, Request $request)
    {
        $issue = new Issue();
        $statuses = [
            'status'         => JournalDocument::ST_NEW,
            'is_published'   => 0,
            'is_show_nauka'  => 0,
        ];
        $issue
            ->setStatuses($statuses)
            ->setJournal($journal)
            ->setPublisher($journal->getPublisher());

        //Наследуем все классификаторы журнала
        $this->getODM()->getRepository('AsuBundle:Common\AbstractPublication')->inheritClassifs($journal, $issue);

        $form = $this->createForm($this->get('asu.form.issue.type'), $issue)
            ->add('save', 'submit', [
                'label' => 'Сохранить',
                'attr' => ['class' => 'btn btn-success pull-right']
            ]);
        $form->handleRequest($request);

        $response = $this->render('AsuBundle:Default/issue:issue_create.html.twig', [
            'journal' => $journal,
            'form' => $form->createView()
        ]);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var IssueDocument $issue */
            $issue = $form->getData();
            if ($journal->getStatuses()['status'] != JournalDocument::ST_IS_PUBLISHING) {
                $journal->setStatus('status', JournalDocument::ST_IN_PROGRESS);
            }
            $this->getODM()->persist($issue);
            $this->getODM()->flush();
            $this->addFlash('notice', 'Номер создан!');

            $response = $this->redirectToRoute('journal_issues', ['id' => $issue->getJournal()->getId()]);
        } else {
            $errors = $this->get('validator')->validate($form);
            foreach ($errors as $e) {
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $response;
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param IssueDocument $issue
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(IssueDocument $issue, Request $request)
    {
        $form = $this->createForm($this->get('asu.form.issue.type'), $issue)
            ->add('save', 'submit', [
                'label' => 'Сохранить',
                'attr' => ['class' => 'btn btn-success pull-right']
            ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getODM()->flush();
            $this->addFlash('notice', 'Номер сохранен!');

            return $this->redirectToRoute('issue_rubrics', ['id' => $issue->getId()]);
        }

        $errors = $this->get('validator')->validate($form);
        foreach ($errors as $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->render('AsuBundle:Default/issue:issue_edit.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('issue', [$issue]),
            'form'      => $form->createView(),
            'issue'     => $issue,
            'st_published' => JournalDocument::ST_IS_PUBLISHED,
        ]);
    }


    /**
     * @param Issue $issue
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function classifsAction(Issue $issue)
    {
        $classifs = [
            'udk' => $this->getODM()->getRepository('AsuBundle:ClassificatorUdk')->findAll()[0],
            'grnti' => $this->getODM()->getRepository('AsuBundle:ClassificatorGrnti')->findAll()[0],
            'okso' => $this->getODM()->getRepository('AsuBundle:ClassificatorOkso')->findAll()[0],
            'bbk' => $this->getODM()->getRepository('AsuBundle:ClassificatorBbk')->findAll()[0],
            'tbk' => $this->getODM()->getRepository('AsuBundle:ClassificatorTbk')->findAll()[0],
        ];

        return $this->render('AsuBundle:Default/templates:classificators.html.twig', [
            'sidemenu' => $this->get('navigation.menu')->get('Side')->getMenu('issue', [$issue]),
            'entity'  => $issue,
            'classifs' => $classifs,
        ]);
    }


    /**
     * @param IssueDocument $issue
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function composeAction(IssueDocument $issue)
    {
        $available = $this->getODM()->getRepository('AsuBundle:Journal')->getAvailableArticles($issue->getJournal());
        $order_rubric_list = $this->getODM()->getRepository('AsuBundle:Issue')->getOrderRubricList($issue);
        
        return $this->render('AsuBundle:Default/issue:issue_compose.html.twig', [
                'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('issue', [$issue]),
                'issue'             => $issue,
                'available'         => $available,
                'issue_rubric_list' => $order_rubric_list
        ]);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function composesaveAction(Request $request)
    {
        $data = json_decode($request->get('data'), true);
        $issue = $this->getODM()->getRepository('AsuBundle:Issue')->find($data['_id']);

        foreach ((array)$data['articles'] as $rubric_id => $articles) {
            if (count($articles) > 0) {
                //Проходим по каждой рубрике
                foreach ($articles as $article_id) {
                    $article = $this->getODM()->getRepository('AsuBundle:Article')->find($article_id);
                    $article->setIssue($issue);
                    //Если у статьи есть рубрика
                    if (null != $article->getRubric()) {
                        $this->getODM()->getRepository('AsuBundle:Issue')->addRubric($issue, $article->getRubric());
                    } else {
                        //добавляем рубрику статье
                        $new_rubric = $this->getODM()->getRepository('AsuBundle:Rubric')->find($rubric_id);
                        $article->setRubric($new_rubric);
                    }
                }
                $issue->setStatus('status', JournalDocument::ST_IN_PROGRESS);
            }
        }

        //Отвязываем статьи от номера
        foreach ((array)$data['article_heap'] as $key => $id) {
            $article = $this->getODM()->getRepository('AsuBundle:Article')->find($id);
            $article->setIssue(null);
        }

        $this->getODM()->flush();

        return new JsonResponse(json_encode(['id' => $issue->getId()]));
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param Issue $issue
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction(Issue $issue, Request $request)
    {
        if ($issue->getStatuses()['status'] != JournalDocument::ST_IS_PUBLISHED) {
            $this->getODM()->remove($issue);
            $this->getODM()->flush();
            $this->addFlash('notice', 'Номер успешно удален!');
        } else {
            $this->addFlash('notice', 'Нельзя удалить опубликованный номер!');
        }

        //#redirecttoreferer
        return $this->redirect($request->headers->get('referer'));
    }


    /**
     * @param IssueDocument $issue
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function rubricsAction(IssueDocument $issue)
    {
        $qb = $this->getODM()->getRepository('AsuBundle:IssueRubricList')->createQueryBuilder();
        $rubriclist = $qb->field('issue')->references($issue)->sort('order', 'asc')->getQuery()->execute();

        return $this->render('AsuBundle:Default/issue:issue_rubrics.html.twig', [
            'sidemenu'      => $this->get('navigation.menu')->get('Side')->getMenu('issue', [$issue]),
            'issue'         => $issue,
            'rubriclist'    => $rubriclist
        ]);
    }


    /**
     * @param IssueDocument $issue
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function articlesAction(IssueDocument $issue)
    {
        return $this->render('AsuBundle:Default/issue:issue_articles.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('issue', [$issue]),
            'issue'     => $issue,
            'st_published' => JournalDocument::ST_IS_PUBLISHED,
        ]);
    }


    /**
     * @param IssueDocument $issue
     * @param $article_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function articleRemoveAction(IssueDocument $issue, $article_id)
    {
        $article = $this->getODM()->getRepository('AsuBundle:Article')->find($article_id);
        $article->setIssue(null);
        $this->getODM()->flush();
        $this->addFlash('notice', 'Статья удалена из номера!');

        return $this->redirectToRoute('issue_articles', ['id' => $issue->getId() ], 301);
    }


    /**
     * Добавление рубрики в номер
     * @param IssueDocument $issue
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function rubricNewAction(IssueDocument $issue, Request $request)
    {
        $form = $this->createForm(new IssueRubricType(), $issue, ['new'   => true])
            ->add('save', 'submit', [
                'label' => 'Добавить',
                'attr' => ['class' => 'btn btn-success pull-right']
            ]);

        $response = $this->render('AsuBundle:Default:rubric_add.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('issue', [$issue]),
            'form' => $form->createView()
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if (!$form->isValid()) {
                foreach ($form->getErrors() as $error) {
                    $this->addFlash('error', $error->getMessage());
                }

                return $response;
            }

            $data = $request->get((new IssueRubricType())->getName());
            $rubric = $this->getODM()->getRepository('AsuBundle:Rubric')->find($data['issue_rubric_list']);
            $issueRubricList = $this->getODM()->getRepository('AsuBundle:Issue')->addRubric($issue, $rubric);

            if (isset($data['order'])) {
                $issueRubricList->setOrder((int)$data['order']);
                $this->getODM()->flush($issueRubricList);
            }

            $this->addFlash('notice', 'Рубрика добавлена!');

            $response = $this->redirectToRoute('issue_rubrics', [ 'id' => $issue->getId() ]);
        }

        return $response;
    }


    /**
     * @param IssueRubricList $issueRubricList
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function rubricEditAction(IssueRubricList $issueRubricList, Request $request)
    {
        $data = ['order' => $issueRubricList->getOrder()];
        $form = $this->createForm(new IssueRubricType(), $data)
            ->add('save', 'submit', [
                'label' => 'Сохранить',
                'attr' => ['class' => 'btn btn-success pull-right']
            ]);
        $response = $this->render('AsuBundle:Default:rubric_add.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('issue', [$issueRubricList->getIssue()]),
            'new'       => false,
            'form'      => $form->createView(),
            'list'      => $issueRubricList
        ]);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if (!$form->isValid()) {
                foreach ($form->getErrors() as $error) {
                    $this->addFlash('error', $error->getMessage());
                }

                return $response;
            }

            $issueRubricList->setOrder($request->request->get('order'));
            $this->getODM()->flush($issueRubricList);
            $this->addFlash('notice', 'Позиция рубрики изменена!');
            $response = $this->redirectToRoute('issue_rubrics', ['id' => $issueRubricList->getIssue()->getId()]);
        }

        return $response;
    }


    /**
     * @param IssueDocument $issue
     * @param $rubric
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function rubricRemoveAction(IssueDocument $issue, $rubric)
    {
        $rubric = $this->getODM()->getRepository('AsuBundle:Rubric')->find($rubric);
        $this->getODM()->getRepository('AsuBundle:Issue')->removeRubric($issue, $rubric);
        $this->addFlash('notice', 'Рубрика удалена из номера!');

        return $this->redirectToRoute('issue_rubrics', ['id' => $issue->getId() ], 301);
    }


    /**
     * @param IssueDocument $issue
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function rincAction(IssueDocument $issue)
    {
        return $issue->createRincArchieve();
    }
}
