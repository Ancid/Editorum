<?php
namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Document\Author;
use Editorum\Bundle\Document\Common\AbstractPublication;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @package Editorum\Bundle\Controller
 *
 * @Security("is_granted('ROLE_PUBLICATION_GROUP')")
 */
class AjaxController extends BasicController
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function getAuthorsAction(Request $request)
    {
        $qb = $this->getODM()->getRepository('AsuBundle:Author')->createQueryBuilder();

        /** @var Author[] $authors */
        $authors = $qb
            ->addOr($qb->expr()->field('ru.fio')->equals(new \MongoRegex('/.*'.$request->get('q').'.*/i')))
            ->addOr($qb->expr()->field('en.fio')->equals(new \MongoRegex('/'.$request->get('q').'/i')))
            ->limit(100)
            ->getQuery()
            ->execute()
        ;

        $data = [
            'success' => true,
            'resultCount'  => iterator_count($authors),
            'items' => []
        ];

        foreach ($authors as $author) {
            $data['items'][] = [
                'id'    => $author->getId(),
                'fio'   => $author->getRu()['fio'],
                'birth' => $author->getBirth()->format('d.m.Y')
            ];
        }

        return new JsonResponse($data);
    }
}
