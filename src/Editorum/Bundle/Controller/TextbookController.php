<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 29.06.2016
 * Time: 16:55
 */

namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Document\Chapter;
use Editorum\Bundle\Document\Common\AbstractPublication;
use Editorum\Bundle\Document\Reference;
use Editorum\Bundle\Document\Textbook;
use Editorum\Bundle\Form\ChapterEditType;
use Editorum\Bundle\Form\ChapterType;
use Editorum\Bundle\Form\Monography\AuthorType;
use Editorum\Bundle\Form\Monography\NewAuthorType;
use Editorum\Bundle\Form\Reference\AddForm;
use Editorum\Bundle\Form\Reference\EditForm;
use Editorum\Bundle\Form\TextType;
use Editorum\Bundle\Form\FileStorage as FileStorageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Editorum\Bundle\Document\FileStorage as FileStorageDocument;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * @package Editorum\Bundle\Controller
 *
 * @Security("is_granted('ROLE_TEXTBOOK_GROUP')")
 */
class TextbookController extends BasicController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $user = $this->getUser();
        $qb = $this->getODM()->createQueryBuilder('AsuBundle:Textbook');

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $corp = [];
            foreach ($this->getUser()->getCorporate()->getOrganization()->getCorporates()->toArray() as $co) {
                $corp[$co->getKind()][] = $co->getId();
            }
            foreach ($corp as $type => $ids) {
                $qb
                    ->addOr(
                        $qb->expr()
                            ->addAnd($qb->expr()->field('publisher.$ref')->equals($type))
                            ->addAnd($qb->expr()->field('publisher.$id')->in($ids))
                    );
            }
        } else {
            $qb->field('publisher')->references($user->getCorporate());
        }

        $textbooks = $qb->sort('id', 'DESC');

        if ($request->get('filter')) {
            $filter = $this->get('publication.statuses_filter');

            $filter
                ->prepareData($request->get('filter'))
                ->makeFilter($textbooks)
            ;
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $textbooks,
            $request->get('page', 1),
            30
        );

        return $this->render('AsuBundle:Default/textbook:textbooks.html.twig', [
            'pagination' => $pagination,
            'kinds'        => AbstractPublication::getKinds(),
            'st_published' => AbstractPublication::ST_IS_PUBLISHED,
        ]);
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $textbook = new Textbook();
        $statuses = [
            'status'        => AbstractPublication::ST_IN_WORK,
            'is_published'  => 0,
            'is_show_nauka' => 0,
            'has_reviewers' => 0,
            'has_published' => 0,
            'has_agreement' => 0,
            'has_images_owned' => 0,
        ];
        $textbook
            ->setPublisher($this->getUser()->getCorporate())
            ->setStatuses($statuses);

        $form = $this->createForm($this->get('asu.form.publication.textbook_type'), $textbook)
            ->add('save', 'submit', [
                'label' => 'Сохранить',
                'attr' => ['class' => 'btn btn-success pull-right']
            ]);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $constraints = $this->get('validator')->validate($form);
            foreach ($constraints as $constraint) {
                $form->addError(new FormError($constraint->getMessage()));
            }

            if ($form->isValid()) {
                /** @var Textbook $textbook */
                $textbook = $form->getData();
                if ($request->files->get($form->getName()) !== null) {
                    $filename = $this->get('upload_helper')->upload('cover', $form->getName());

                    if ($filename !== null && !is_array($filename)) {
                        $textbook->setCover($filename);
                    }
                }

                $this->getODM()->persist($textbook);
                $this->getODM()->flush();

                if ($textbook->getStatuses()['status'] == AbstractPublication::ST_IS_PUBLISHED) {
                    $redirect = $this->redirectToRoute('textbook_list');
                } else {
                    $redirect = $this->redirectToRoute('textbook_authors', ['id'    => $textbook->getId()]);
                }
                return $redirect;
            }
        }
        
        return $this->render('AsuBundle:Default/textbook:textbook_edit.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('textbook', [$textbook]),
            'form'      => $form->createView(),
            'textbook'  => $textbook,
            'st_published' => AbstractPublication::ST_IS_PUBLISHED,
        ]);
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param Textbook $textbook
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Textbook $textbook, Request $request)
    {
        $form = $this->createForm($this->get('asu.form.publication.textbook_type'), $textbook)
            ->add('save', 'submit', [
                'label' => 'Сохранить',
                'attr' => ['class' => 'btn btn-success pull-right']
            ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($request->files->get($form->getName()) !== null) {
                $filename = $this->get('upload_helper')->upload('cover', $form->getName());

                if ($filename !== null && !is_array($filename)) {
                    $textbook->setCover($filename);
                }
            }

            $this->getODM()->flush();
            $this->addFlash('notice', 'Учебник сохранен!');
            return $this->redirectToRoute('textbook_authors', ['id' => $textbook->getId()]);
        }

        return $this->render('AsuBundle:Default/textbook:textbook_edit.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('textbook', [$textbook]),
            'form'      => $form->createView(),
            'textbook'  => $textbook,
            'st_published' => AbstractPublication::ST_IS_PUBLISHED,
        ]);
    }


    /**
     * @Security("is_granted('ROLE_DELETE')")
     * @param Textbook $textbook
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction(Textbook $textbook)
    {
        if ($textbook->getStatuses()['status'] != AbstractPublication::ST_IS_PUBLISHED) {
            $this->getODM()->remove($textbook);
            $this->getODM()->flush();
            $this->addFlash('notice', 'Учебник успешно удален!');
        } else {
            $this->addFlash('notice', 'Нельзя удалить опубликованный учебник!');
        }

        return $this->redirectToRoute('textbook_list');
    }


    /**
     * @param Textbook $textbook
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function authorsAction(Textbook $textbook, Request $request)
    {
        $this->get('session')->remove('way[route]');
        $this->get('session')->remove('way[id]');

        $form = $this->createForm(new AuthorType());
        $newAuthorForm = $this->createForm(new NewAuthorType());

        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);
            $newAuthorForm->handleRequest($request);

            if ($form->isValid()) {
                $author = $form->getData()['author'];

                if (!in_array($author, $textbook->getAuthors()->toArray())) {
                    $textbook->addAuthor($author);

                    $this->getODM()->persist($textbook);
                    $this->getODM()->flush();

                    return $this->redirectToRoute('textbook_authors', ['id' => $textbook->getId()]);
                }
            }

            if ($newAuthorForm->isValid()) {
                $author = $newAuthorForm->getData();
                $author->setFio(trim(
                    $author->getRu()['last_name'] . ' ' .
                    $author->getRu()['first_name'] . ' ' .
                    $author->getRu()['second_name']
                ));

                if (!in_array($author, $textbook->getAuthors()->toArray())) {
                    $textbook->addAuthor($author);

                    $this->getODM()->persist($author);
                    $this->getODM()->persist($textbook);
                    $this->getODM()->flush();

                    return $this->redirectToRoute('author_edit', [
                        'id'         => $author->getId(),
                        'way[route]' => 'textbook_authors',
                        'way[id]'    => $textbook->getId(),
                    ]);
                }
            }
        }

        return $this->render('AsuBundle:Default/textbook:textbook_authors.html.twig', [
            'sidemenu'      => $this->get('navigation.menu')->get('Side')->getMenu('textbook', [$textbook]),
            'entity'        => $textbook,
            'form'          => $form->createView(),
            'newAuthorForm' => $newAuthorForm->createView(),
        ]);
    }


    /**
     * @param Textbook $textbook
     * @param $authorId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function removeAuthorAction(Textbook $textbook, $authorId)
    {
        $author = $this->getODM()->getRepository('AsuBundle:Author')->find($authorId);

        if (null !== $author && in_array($author, $textbook->getAuthors()->toArray())) {
            $textbook->removeAuthor($author);
            $this->getODM()->persist($textbook);
            $this->getODM()->flush();
        }

        return $this->redirectToRoute('textbook_authors', [ 'id' => $textbook->getId() ]);
    }


    /**
     * @param Textbook $textbook
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Textbook $textbook)
    {
//        $classif = new Classif();
//        $classifs = $classif->loadClassif(['okso_journal', 'grnti', 'tbk']);
        $classifs = [];
        return $this->render('AsuBundle:Default/textbook:textbook_show.html.twig', [
            'entity'    => $textbook,
            'classifs'  => $classifs
        ]);
    }


    /**
     * @param Textbook $textbook
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function chaptersAction(Textbook $textbook, Request $request)
    {
        $form = $this->createForm(new ChapterType(), null, [
            'publication'   => $textbook
        ]);
        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                /** @var Chapter $chapter */
                $chapter = $form->getData();

                if (!in_array($chapter, $textbook->getChapters()->toArray())) {
                    $textbook->addChapter($chapter);
                    $chapter->setPublication($textbook);
                    $this->getODM()->persist($chapter);
                    $this->getODM()->flush();

                    return $this->redirectToRoute('textbook_chapter_edit', [
                        'publication_id' => $textbook->getId(),
                        'id' => $chapter->getId(),
                    ]);
                }
            }
        }

        return $this->render('AsuBundle:Default/textbook:textbook_chapters.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('textbook', [$textbook]),
            'form'      => $form->createView(),
            'entity'    => $textbook,
        ]);
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param Chapter $chapter
     * @param $publication_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function chapterEditAction(Chapter $chapter, $publication_id, Request $request)
    {
        $textbook = $this->getODM()->getRepository('AsuBundle:Textbook')->find($publication_id);
        $form = $this->createForm(new ChapterEditType(), $chapter, [
            'publication' => $textbook
        ])->add('save', 'submit', [
            'label' => 'Сохранить',
            'attr' => ['class' => 'btn btn-success pull-right']
        ]);
        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $this->getODM()->flush();

                return $this->redirectToRoute('textbook_chapters', [ 'id' => $textbook->getId() ]);
            }
        }

        $request->attributes->set('_route', 'textbook_chapters');
        $request->attributes->set('_route_params', ['id' => $publication_id]);

        return $this->render('AsuBundle:Default/templates:chapter_edit.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('textbook', [$textbook]),
            'form'      => $form->createView(),
            'entity'    => $chapter,
        ]);
    }


    /**
     * @Security("is_granted('ROLE_DELETE')")
     * @param Chapter $chapter
     * @param $publication_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function chapterRemoveAction(Chapter $chapter, $publication_id)
    {
        $textbook = $this->getODM()->getRepository('AsuBundle:Textbook')->find($publication_id);
        $textbook->removeChapter($chapter);

        $this->getODM()->remove($chapter);
        $this->getODM()->flush();

        return $this->redirectToRoute('textbook_chapters', [ 'id' => $publication_id ]);
    }


    /**
     * @param Textbook $textbook
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function textAction(Textbook $textbook, Request $request)
    {
        $form = $this->createForm(new TextType(), $textbook);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getODM()->flush();
        } else {
            $session = $this->get('session');
            foreach ($form->getErrors() as $error) {
                $session->getFlashBag()->add('error', $error->getMessage());
            }
        }

        return $this->render('AsuBundle:Default/textbook:textbook_text.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('textbook', [$textbook]),
            'form'      => $form->createView(),
            'entity'    => $textbook,
        ]);
    }


    /**
     * @param Textbook $textbook
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function referencesAction(Textbook $textbook)
    {
        return $this->render('AsuBundle:Default/textbook:textbook_references.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('textbook', [$textbook]),
            'active'    => 'refs',
            'entity'    => $textbook,
        ]);
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param Textbook $textbook
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function referenceAddAction(Textbook $textbook, Request $request)
    {
        $form = $this->createForm(new AddForm(), null);

        if ($request->getMethod() === "POST") {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $this->getODM()->getRepository('AsuBundle:Reference')
                    ->addReferences($data, $textbook, $this->get('validator'));

                return $this->redirectToRoute('textbook_references', [ 'id' => $textbook->getId() ]);
            }
        }

        return $this->render('AsuBundle:Default/templates:reference_add.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('textbook', [$textbook]),
            'active'    => 'refs',
            'form'      => $form->createView()
        ]);
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param $publication_id
     * @param Reference $reference
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function referenceEditAction($publication_id, Reference $reference, Request $request)
    {
        $textbook = $this->getODM()->getRepository('AsuBundle:Textbook')->find($publication_id);
        $form = $this->createForm(new EditForm(), $reference);
        if ($request->getMethod() === "PUT") {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->getODM()->flush($reference);

                return $this->redirectToRoute('textbook_references', [ 'id' => $publication_id ]);
            }
        }

        return $this->render('AsuBundle:Default/templates:reference_edit.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('textbook', [$textbook]),
            'active'    => 'refs',
            'form'      => $form->createView()
        ]);
    }


    /**
     * @Security("is_granted('ROLE_DELETE')")
     * @param $publication_id
     * @param Reference $reference
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function referenceRemoveAction($publication_id, Reference $reference)
    {
        $textbook = $this->getODM()->getRepository('AsuBundle:Textbook')->find($publication_id);
        $textbook->removeReference($reference);
        $this->getODM()->flush();

        return $this->redirectToRoute('textbook_references', [ 'id' => $publication_id ]);
    }


    /**
     * @param Textbook $textbook
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function filesUploadAction(Textbook $textbook)
    {
        $odm = $this->getODM();

        /** @var null|FileStorageDocument[] $existsFiles */
        $existsFiles = $odm->getRepository('AsuBundle:FileStorage')
            ->getFilesByDocument($textbook);

        $form = $this->createForm(new FileStorageType(), null, [
            'document_hidden_class' => $textbook,
            'field_name'            => 'path',
            'multi_upload'          => true
        ]);

        $fileStorageHelper = $this->get('file_storage.helper');
        $errors = $fileStorageHelper->upload($form);

        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $error->getClientMimeType();

                $this->addFlash(
                    'error',
                    sprintf(
                        'File "%s" has wrong mime-type (%s). Allowed mime-types: "%s"',
                        $error->getClientOriginalName(),
                        $error->getClientMimeType(),
                        implode(', ', $fileStorageHelper->getAllowedMimeTypes())
                    )
                );
            }
        }

        return $this->render('AsuBundle:Default/textbook:textbook_files.html.twig', [
            'form'     => $form->createView(),
            'sidemenu' => $this->get('navigation.menu')->get('Side')->getMenu('textbook', [$textbook]),
            'files'    => $existsFiles,
            'article'  => $textbook
        ]);
    }


    /**
     * @param Textbook $textbook
     * @param $file_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function filesRemoveAction(Textbook $textbook, $file_id)
    {
        $remove = $this->get('file_storage.helper')->remove($file_id);

        if (!$remove) {
            throw new NotFoundHttpException(
                'File which you want remove not found in database'
            );
        }

        return $this->redirectToRoute('textbook_files', ['id' => $textbook->getId()]);
    }


    /**
     * @param Textbook $textbook
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function rincAction(Textbook $textbook)
    {
        return $textbook->createRincArchieve();
    }


    /**
     * @param Textbook $textbook
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function classifsAction(Textbook $textbook)
    {
        $classifs = [
            'udk' => $this->getODM()->getRepository('AsuBundle:ClassificatorUdk')->findAll()[0],
            'grnti' => $this->getODM()->getRepository('AsuBundle:ClassificatorGrnti')->findAll()[0],
            'okso' => $this->getODM()->getRepository('AsuBundle:ClassificatorOkso')->findAll()[0],
            'bbk' => $this->getODM()->getRepository('AsuBundle:ClassificatorBbk')->findAll()[0],
            'tbk' => $this->getODM()->getRepository('AsuBundle:ClassificatorTbk')->findAll()[0],
        ];

        return $this->render('AsuBundle:Default/templates:classificators.html.twig', [
            'sidemenu' => $this->get('navigation.menu')->get('Side')->getMenu('textbook', [$textbook]),
            'entity'  => $textbook,
            'classifs' => $classifs,
        ]);
    }

    public function referencesDeleteAllAction(Textbook $article)
    {
        $allReferences = $article->getReferences();
        $article->clearReferences();
        foreach ($allReferences as $reference) {
            $this->getODM()->remove($reference);
        }

        $this->getODM()->flush();

        return $this->redirectToRoute('textbook_references', [
            'id' => $article->getId()
        ]);
    }
}
