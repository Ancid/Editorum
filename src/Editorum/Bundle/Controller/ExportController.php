<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 27.08.2015
 * Time: 21:59
 */

namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Logic\Export;
use Editorum\Bundle\Logic\Model;
use Editorum\Bundle\Services\GlobalMenus;

class ExportController extends BasicController
{

    /**
     * Список логов
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $this->get('navigation.global')->setInterface(GlobalMenus::INT_PUBLISHER);
        $list = Export::loadList();
        return $this->render('AsuBundle:Default:logs.html.twig', array('logs' => $list));
    }

    public function viewAction($id)
    {
        $this->get('navigation.global')->setInterface(GlobalMenus::INT_PUBLISHER);
        $exp = Model::getById(new Export(), $id);
        return $this->render('AsuBundle:Default:log.html.twig', array('data' => $exp));
    }

}
