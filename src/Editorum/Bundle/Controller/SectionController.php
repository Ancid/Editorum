<?php
/**
 * Created by PhpStorm.
 * User: vgfilippov
 * Date: 17.12.15
 * Time: 16:16
 */

namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Form\Sections\SectionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Editorum\Bundle\Document\Conference as ConferenceDocument;
use Editorum\Bundle\Document\Section;

class SectionController extends BasicController
{

    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param $section_id
     * @param ConferenceDocument $conference
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function editAction($section_id, ConferenceDocument $conference, Request $request)
    {
        $form = $this->createForm(new SectionType())
            ->add('save', 'submit', [
                'label' => 'Создать',
                'attr' => ['class' => 'btn btn-success pull-right']
            ]);
        
        if ($section_id > 0) {
            $section = $this->getODM()->getRepository('AsuBundle:Section')->find($section_id);
            $form->setData($section);
        }
            
        $response = $this->render(
            'AsuBundle:Default/section:section_edit.html.twig',
            [
                'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('conference', [$conference]),
                'section_id'    => $section_id,
                'form'          => $form->createView(),
            ]
        );
        
        if ($request->getMethod() === 'POST') {
            $dm = $this->getODM();
            $form->handleRequest($request);
            if (!$form->isValid()) {
                foreach ($form->getErrors() as $error) {
                    $this->addFlash('error', $error->getMessage());
                }
                return $response;
            }
            $section = $form->getData();
            $dm->persist($section);
            if (!in_array($section, $conference->getSections()->toArray())) {
                $conference->addSection($section);
            }
            $dm->persist($conference);
            $dm->flush();
            $this->addFlash('notice', 'Секция добавлена!');
            $response = $this->redirectToRoute('conference_sections', ['id' => $conference->getId()]);
        }
        
        return $response;
    }


    /**
     * @Security("is_granted('ROLE_DELETE')")
     * @param $section_id
     * @param ConferenceDocument $conference
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function removeAction($section_id, ConferenceDocument $conference)
    {
        $section = $this->getODM()->getRepository('AsuBundle:Section')->find($section_id);
        if (null != $section) {
            $conference->removeSection($section);
            $this->getODM()->flush($conference);
        }

        return $this->redirectToRoute('conference_sections', ['id' => $conference->getId()]);
    }
}
