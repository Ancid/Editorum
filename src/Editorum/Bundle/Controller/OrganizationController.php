<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 11.11.2015
 * Time: 13:22
 */

namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Document\Common\AbstractCorporateEntity;
use Editorum\Bundle\Document\Organization;
use Editorum\Bundle\Form\Organization\OrganizationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class OrganizationController
 * @package Editorum\Bundle\Controller
 *
 * @Security("is_granted('ROLE_ADMIN')")
 */
class OrganizationController extends BasicController
{
    /**
     * Список организаций, участников системы Editorum
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $org = $this->getUser()->getCorporate()->getOrganization();
        return $this->render('AsuBundle:Default/organization:organizations.html.twig', [
            'organizations' => [$org]
        ]);
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm(new OrganizationType())->add('save', 'submit', [
            'label' => 'Сохранить',
            'attr' => ['class' => 'btn btn-success pull-right']
        ]);

        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                /** @var Organization $organization */
                $organization = $form->getData();

                $this->getODM()->persist($organization);
                $this->getODM()->flush();

                return $this->redirectToRoute('organization_edit', ['id' => $organization->getId()]);
            }
        }

        return $this->render('AsuBundle:Default/organization:organization_edit.html.twig', [
            'sidemenu'  => [],
            'form'      => $form->createView()
        ]);
    }


    /**
     * @param Organization $organization
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editAction(Organization $organization, Request $request)
    {
        $form = $this->createForm(new OrganizationType(), $organization)->add('save', 'submit', [
            'label' => 'Сохранить',
            'attr' => ['class' => 'btn btn-success pull-right']
        ]);

        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->getODM()->flush($organization);

                return $this->redirectToRoute('organization_edit', ['id' => $organization->getId()], 301);
            }
        }

        return $this->render('AsuBundle:Default/organization:organization_edit.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('organization', [$organization]),
            'form'      => $form->createView()
        ]);
    }


    /**
     * @param Organization $organization
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction(Organization $organization)
    {
        $this->getODM()->remove($organization);
        $this->getODM()->flush();
        $this->addFlash('notice', 'Организация успешно удалена!');

        return $this->redirectToRoute('organization_list');
    }


    /**
     * @param Organization $organization
     * @return Response
     */
    public function corporatesAction(Organization $organization)
    {
        return $this->render('AsuBundle:Default/organization:organization_corporates.html.twig', array(
            'sidemenu'      => $this->get('navigation.menu')->get('Side')->getMenu('organization', [$organization]),
            'organization'  => $organization,
            'kinds'         => AbstractCorporateEntity::getKinds(),
        ));
    }
}
