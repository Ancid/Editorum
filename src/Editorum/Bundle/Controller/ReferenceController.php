<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 23.08.2015
 * Time: 13:06
 */

namespace Editorum\Bundle\Controller;

use DateTime;
use Editorum\Bundle\Document\FileStorage as FileStorageDocument;
use Editorum\Bundle\Document\Reference;
use Editorum\Bundle\Form\FileStorage;
use Editorum\Bundle\Form\Publication\PublicationConferenceType;
use Editorum\Bundle\Form\Reference\AddForm;
use Editorum\Bundle\Form\Reference\EditForm;
use Editorum\Bundle\Form\TextForm;
use Editorum\Bundle\Logic\Rubrics;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Editorum\Bundle\Logic\Classif;
use Editorum\Bundle\Logic\Journal;
use Editorum\Bundle\Logic\Model;
use Editorum\Bundle\Logic\Publication;
use Editorum\Bundle\Logic\Publisher;
use JournalsBundle\Logic\Search;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ReferenceController
 * @package Editorum\Bundle\Reference
 *
 * @Security("is_granted('ROLE_PUBLICATION_GROUP')")
 */
class ReferenceController extends BasicController
{
    /**
     * @param $reference_id
     * @param $publication_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function referenceEditAction($reference_id, $publication_id, Request $request)
    {
        $odm = $this->get('doctrine.odm.mongodb.document_manager');

        $reference = $odm->getRepository('AsuBundle:Reference')->find($reference_id);
        $form = $this->createForm(new EditForm(), $reference);

        if ($request->getMethod() === "PUT") {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $odm->flush($reference);

                return $this->redirectToRoute('publication_references', [ 'publication_id' => $publication_id ]);
            }
        }

        return $this->render('AsuBundle:Default/publication/reference:edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
