<?php
/**
 * Created by PhpStorm.
 * User: xor
 * Date: 24.08.15
 * Time: 12:02
 */
namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Form\Author\DegreeType;
use Editorum\Bundle\Form\Author\JobType;
use Editorum\Bundle\Form\AuthorType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use JournalsBundle\Logic\Search;
use Editorum\Bundle\Logic\Model;
use Editorum\Bundle\Logic\Author;
use Editorum\Bundle\Document\Author as AuthorDocument;
use MongoRegex;

/**
 * @Security("is_granted('ROLE_AUTHOR_GROUP')")
 */
class AuthorController extends BasicController
{
    /**
     * Список авторов
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $search_txt = $request->get('search');
        $alphabet = $request->get('alphabet');

        $dm = $this->get('doctrine.odm.mongodb.document_manager');
        $author_list = $dm->createQueryBuilder('AsuBundle:Author');

        if (!is_null($search_txt) && strlen($search_txt) > 0) {
            $results = Search::getAuthors($search_txt);
            $author_list
                ->find()
                ->field('_id')->in($results['matches']);
        } else {
            $author_list->find();
            if ($alphabet !== null && !empty($alphabet)) {
                $author_list->field('ru.last_name')
                    ->equals(new MongoRegex('/^'.$alphabet.'/i'));
            }
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $author_list->sort('fio', 'asc'),
            $this->get('request')->query->getInt('page', 1),
            30
        );

        return $this->render('AsuBundle:Default/author:authors.html.twig', [
            'pagination' => $pagination,
            'search_txt' => $search_txt,
        ]);
    }


    /**
     * @param AuthorDocument $author
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param $id
     */
    public function showAction(AuthorDocument $author)
    {
        $pubs = $this->getODM()->getRepository('AsuBundle:Author')->getAllPublications($author);
        return $this->render('AsuBundle:Default/author:author_show.html.twig', [
            'author' => $author,
            'pubs'   => $pubs,
        ]);
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $author = new AuthorDocument();
        $form = $this->createForm(new AuthorType(), $author)->add('save', 'submit', [
            'label' => 'Сохранить',
            'attr' => ['class' => 'btn btn-success pull-right']
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $author->setFio();
            $this->getODM()->persist($author);
            $this->getODM()->flush();

            return $this->redirectToRoute('author_edit', ['id' => $author->getId()]);
        }

        return $this->render('AsuBundle:Default/author:author_edit.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('author', [$author]),
            'form'      => $form->createView(),
        ]);
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param AuthorDocument $author
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(AuthorDocument $author, Request $request)
    {
        if (null !== $request->get('way')) {
            $this->get('session')->set('way[route]', $request->get('way')['route']);
            $this->get('session')->set('way[id]', $request->get('way')['id']);
        }

        $form = $this->createForm(new AuthorType(), $author);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $author->setFio();
            $this->getODM()->flush();

            return $this->redirectToRoute('author_edit', ['id' => $author->getId()]);
        }

        return $this->render('AsuBundle:Default/author:author_edit.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('author', [$author]),
            'form'      => $form->createView(),
        ]);
    }


    /**
     * @param AuthorDocument $author
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function jobAction(AuthorDocument $author)
    {
        return $this->render('AsuBundle:Default/author/job:list.html.twig', [
            'sidemenu' => $this->get('navigation.menu')->get('Side')->getMenu('author', [$author]),
            'entity'   => $author,
        ]);
    }

    /**
     * @param AuthorDocument $author
     * @param Request        $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addJobAction(AuthorDocument $author, Request $request)
    {
        $form = $this->createForm(new JobType());

        $request->attributes->set('_route', 'author_job');
        $request->attributes->set('_route_params', [ 'id' => $author->getId() ]);

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $data = $form->getData();
            $company = null;

            if ((bool) $data['is_exists']) {
                $company = $data['corporate'];

                if ((bool) $data['is_vuz']) {
                    $company = $data['vuz'];
                }
            } else {
                $company = new AuthorDocument\JobPlace();
                $company
                    ->setRu(['title' => $data['hand']['title']])
                    ->setEn(['title' => null])
                    ->setCountry($data['hand']['country'])
                    ->setCity($data['hand']['city'])
                ;

                $this->getODM()->persist($company);
            }

            $entry = new AuthorDocument\JobList();
            $entry
                ->setPlace($company)
                ->setDepartment($data['department'])
                ->setPosition($data['position'])
            ;

            $this->getODM()->persist($entry);

            $author->addWorkplace($entry);
            $this->getODM()->persist($author);
            $this->getODM()->flush();

            return $this->redirectToRoute('author_job', [ 'id' => $author->getId() ]);
        }

        return $this->render('AsuBundle:Default/author/job:add.html.twig', [
            'sidemenu' => $this->get('navigation.menu')->get('Side')->getMenu('author', [ $author ]),
            'form'     => $form->createView(),
        ]);
    }

    /**
     * @param AuthorDocument $author
     * @param                $jobId
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeJobAction(AuthorDocument $author, $jobId)
    {
        $job = $this->getODM()->getRepository('AsuBundle:Author\JobList')->find($jobId);

        if (null !== $job) {
            $author->removeWorkplace($job);

            $place = $job->getPlace();
            $refl = new \ReflectionClass($place);
            if (strtolower($refl->getShortName()) == 'jobplace') {
                $this->getODM()->remove($place);
            }

            $this->getODM()->remove($job);
            $this->getODM()->persist($author);
            $this->getODM()->flush();
        }

        return $this->redirectToRoute('author_job', [ 'id' => $author->getId() ]);
    }

    /**
     * @param AuthorDocument $author
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function degreeAction(AuthorDocument $author, Request $request)
    {
        $form = $this->createForm(new DegreeType());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $degree = $form->getData();
            $this->getODM()->persist($degree);

            $author->addDegreeList($degree);
            $this->getODM()->persist($author);
            $this->getODM()->flush();

            return $this->redirectToRoute('author_degree_list', [ 'id' => $author->getId() ]);
        }

        return $this->render('AsuBundle:Default/author:author_degree.html.twig', [
            'entity'    => $author,
            'sidemenu' => $this->get('navigation.menu')->get('Side')->getMenu('author', [$author]),
            'form'     => $form->createView(),
        ]);
    }

    /**
     * @param AuthorDocument $author
     * @param                $degreeId
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeDegreeAction(AuthorDocument $author, $degreeId)
    {
        $degree = $this->getODM()->getRepository('AsuBundle:Author\DegreeList')->find($degreeId);
        $author->removeDegreeList($degree);

        $this->getODM()->remove($degree);
        $this->getODM()->persist($author);
        $this->getODM()->flush();

        return $this->redirectToRoute('author_degree_list', [ 'id' => $author->getId() ]);
    }

    /**
     * @param $query
     * @return JsonResponse
     */
    public function typeaheadAction($query)
    {
        $ar = [];

        if (strlen($query) > 1) {
            $authors = Author::loadTypeahead($query);
            foreach ($authors as $author) {
                $ar[ $author['_id'] ] = $author['fio'];
            }
        }

        return new JsonResponse(json_encode($ar));
    }


    /**
     * @return JsonResponse
     */
    public function saveAction()
    {
        $data = json_decode($this->get('request')->request->get('data'), true);

        if (isset($data['country_id'])) {
            $data['country_id'] = (int)$data['country_id'];
        }

        if (isset($data['region_id'])) {
            $data['region_id'] = (int)$data['region_id'];
        }

        if (isset($data['reg_region_id'])) {
            $data['reg_region_id'] = (int)$data['reg_region_id'];
        }

        if (isset($data['reg_country_id'])) {
            $data['reg_country_id'] = (int)$data['reg_country_id'];
        }

        if (isset($data['work_country'])) {
            $data['work_country'] = (int)$data['work_country'];
        }

        $author_id = isset($data['_id']) ? Model::update(new Author(), $data) : Model::insert(new Author(), $data);

        return new JsonResponse(json_encode(['id' => $author_id]));
    }


    /**
     * @Security("is_granted('ROLE_DELETE')")
     * @param AuthorDocument $author
     * @return JsonResponse
     */
    public function removeAction(AuthorDocument $author)
    {
        $this->getODM()->remove($author);
        $this->getODM()->flush();
        return $this->redirectToRoute('authors_list');
    }
}
