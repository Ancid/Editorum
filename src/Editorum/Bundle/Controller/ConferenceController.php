<?php
/**
 * Created by PhpStorm.
 * User: vgfilippov
 * Date: 17.12.15
 * Time: 16:16
 */

namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Document\Conference;
use Editorum\Bundle\Document\Conference as ConferenceDocument;
use Editorum\Bundle\Document\Doi;
use Editorum\Bundle\Form\Sections\SectionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("is_granted('ROLE_CONFERENCE_GROUP')")
 */
class ConferenceController extends BasicController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $user = $this->getUser();
        $qb = $this->getODM()->createQueryBuilder('AsuBundle:Conference');

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $corp = [];
            foreach ($this->getUser()->getCorporate()->getOrganization()->getCorporates()->toArray() as $co) {
                $corp[$co->getKind()][] = $co->getId();
            }
            foreach ($corp as $type => $ids) {
                $qb
                    ->addOr(
                        $qb->expr()
                            ->addAnd($qb->expr()->field('publisher.$ref')->equals($type))
                            ->addAnd($qb->expr()->field('publisher.$id')->in($ids))
                    );
            }
        } else {
            $qb->field('publisher')->references($user->getCorporate());
        }

        $conference = $qb->sort('id', 'DESC');

        if ($request->get('filter')) {
            $filter = $this->get('publication.statuses_filter');

            $filter
                ->prepareData($request->get('filter'))
                ->makeFilter($conference);
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $conference,
            $request->query->getInt('page', 1),
            30
        );

        return $this->render('AsuBundle:Default/conference:conferences.html.twig', [
            'pagination'    => $pagination,
            'kinds'         => Conference::getKinds(),
        ]);
    }


    /**
     * Просмотр конференции
     *
     * @param ConferenceDocument $conference
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Conference $conference)
    {
        return $this->render('AsuBundle:Default/conference:conference_show.html.twig', [
            'conference' => $conference
        ]);
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $conference = new ConferenceDocument();
        $conference
            ->setPublisher($this->getUser()->getCorporate())
            ->setStatuses([
                'status'           => Conference::ST_INACTIVE,
                'is_show_nauka'    => 0,
                'is_rinc'          => 0,
            ]);

        $form = $this->createForm($this->get('asu.form.publication.conference_type'), $conference)
            ->add('save', 'submit', [
                'label' => 'Сохранить',
                'attr' => ['class' => 'btn btn-success']
            ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->updateStatus($conference);
            $this->getODM()->persist($conference);
            $this->getODM()->flush();
            $this->addFlash('notice', 'Конференция сохранена!');

            return $this->redirectToRoute('conference_sections', ['id' => $conference->getId()]);
        }

        $errors = $this->get('validator')->validate($form);
        foreach ($errors as $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->render('AsuBundle:Default/conference:conference_edit.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('conference', [$conference]),
            'form'      => $form->createView(),
            'conference' => $conference,
            'is_publishing' => $conference->getStatuses()['status'] == Conference::ST_CLOSED ? true : false,
        ]);
    }


    /**
     * @param ConferenceDocument $conference
     */
    public function updateStatus(Conference $conference)
    {
        $now = new \DateTime();
        $status = Conference::ST_INACTIVE;
        //Если дата позже текущей
        if (strtotime($conference->getDateStart()->format('Y-m-d')) - strtotime($now->format('Y-m-d')) < 0) {
            $status = Conference::ST_ACTIVE;
        }
        if (strtotime($conference->getDateFinish()->format('Y-m-d')) - strtotime($now->format('Y-m-d')) < 0) {
            $status = Conference::ST_CLOSED;
        }
        $conference->setStatus('status', $status);
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param ConferenceDocument $conference
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Conference $conference, Request $request)
    {
        $c_repo = $this->getODM()->getRepository('AsuBundle:Conference');
        $old_pid = $conference->getPublisher()->getId();

        $form = $this->createForm($this->get('asu.form.publication.conference_type'), $conference)
            ->add('save', 'submit', [
                'label' => 'Сохранить',
                'attr' => ['class' => 'btn btn-success']
            ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($old_pid !== $conference->getPublisher()->getId()) {
                $c_repo->updatePublisher($conference);
            }
            $this->updateStatus($conference);

            $this->getODM()->persist($conference);
            $this->getODM()->flush();
            $this->addFlash('notice', 'Конференция сохранена!');

            return $this->redirectToRoute('conference_sections', ['id' => $conference->getId()]);
        }

        // #вывод ошибок, #валидатор
        $errors = $this->get('validator')->validate($form);
        foreach ($errors as $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->render('AsuBundle:Default/conference:conference_edit.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('conference', [$conference]),
            'form'      => $form->createView(),
            'conference' => $conference,
            'is_publishing' => $conference->getStatuses()['status'] == Conference::ST_CLOSED ? true : false,
        ]);
    }


    /**
     * @Security("is_granted('ROLE_DELETE')")
     * @param ConferenceDocument $conference
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction(Conference $conference)
    {
        $this->getODM()->remove($conference);
        $this->getODM()->flush();
        $this->addFlash('notice', 'Коференция удалена!');

        return $this->redirectToRoute('conference_list');
    }


    /**
     * @param ConferenceDocument $conference
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function sectionsAction(ConferenceDocument $conference)
    {
        $form = $this->createForm(new SectionType());
        return $this->render('AsuBundle:Default/conference:conference_sections.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('conference', [$conference]),
            'conference' => $conference,
            'form'       => $form->createView(),
        ]);
    }


    /**
     * Редактирование секции через x-editable
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Doctrine\ODM\MongoDB\LockException
     * @throws \Throwable
     * @throws \TypeError
     */
    public function sectionFieldEditAction(Request $request)
    {
        $data = $request->request->all();
        $client = $this->get('wsse.security.client')->getClient();
        $req = $client->put(
            $this->get('router')->generate('asu_api_editable', ['alias' => 'section'], Router::ABSOLUTE_URL),
            [
                'json'    => $data,
                'headers' => [
                    'Content-Type' => 'application/json',
                ]
            ]
        );

        $result = $req->getBody()->getContents();

        return new JsonResponse($result);
    }


    /**
     * @param ConferenceDocument $conference
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function collectionsAction(ConferenceDocument $conference)
    {
        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $conference->getCollections(),
            $this->get('request')->query->getInt('page', 1),
            30
        );

        return $this->render('@Asu/Default/conference/conference_collections.html.twig', [
            'sidemenu'   => $this->get('navigation.menu')->get('Side')->getMenu('conference', [$conference]),
            'conference' => $conference,
            'pagination' => $pagination,
            'kinds'      => Conference::getKinds(),
            'st_published' => Conference::ST_IS_PUBLISHED,
        ]);
    }


    /**
     * @param ConferenceDocument $conference
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function classifsAction(Conference $conference)
    {
        $classifs = [
            'udk' => $this->getODM()->getRepository('AsuBundle:ClassificatorUdk')->findAll()[0],
            'grnti' => $this->getODM()->getRepository('AsuBundle:ClassificatorGrnti')->findAll()[0],
            'okso' => $this->getODM()->getRepository('AsuBundle:ClassificatorOkso')->findAll()[0],
            'bbk' => $this->getODM()->getRepository('AsuBundle:ClassificatorBbk')->findAll()[0],
            'tbk' => $this->getODM()->getRepository('AsuBundle:ClassificatorTbk')->findAll()[0],
        ];

        return $this->render('AsuBundle:Default/templates:classificators.html.twig', [
            'sidemenu' => $this->get('navigation.menu')->get('Side')->getMenu('conference', [$conference]),
            'entity'  => $conference,
            'classifs' => $classifs,
        ]);
    }
}
