<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 29.06.2016
 * Time: 16:55
 */

namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Document\Author;
use Editorum\Bundle\Document\Chapter;
use Editorum\Bundle\Document\Common\AbstractPublication;
use Editorum\Bundle\Document\Doi;
use Editorum\Bundle\Document\FileStorage as FileStorageDocument;
use Editorum\Bundle\Document\Monography;
use Editorum\Bundle\Document\Reference;
use Editorum\Bundle\Form\ChapterEditType;
use Editorum\Bundle\Form\ChapterType;
use Editorum\Bundle\Form\FileStorage;
use Editorum\Bundle\Form\Monography\AuthorType;
use Editorum\Bundle\Form\Monography\NewAuthorType;
use Editorum\Bundle\Form\Reference\AddForm;
use Editorum\Bundle\Form\Reference\EditForm;
use Editorum\Bundle\Form\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @package Editorum\Bundle\Controller
 *
 * @Security("is_granted('ROLE_MONOGRAPHY_GROUP')")
 */
class MonographyController extends BasicController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $user = $this->getUser();
        $qb = $this->getODM()->createQueryBuilder('AsuBundle:Monography');

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $corp = [];
            foreach ($this->getUser()->getCorporate()->getOrganization()->getCorporates()->toArray() as $co) {
                $corp[$co->getKind()][] = $co->getId();
            }
            foreach ($corp as $type => $ids) {
                $qb
                    ->addOr(
                        $qb->expr()
                            ->addAnd($qb->expr()->field('publisher.$ref')->equals($type))
                            ->addAnd($qb->expr()->field('publisher.$id')->in($ids))
                    );
            }
        } else {
            $qb->field('publisher')->references($user->getCorporate());
        }

        $monographs = $qb->sort('id', 'DESC');

        if ($request->get('filter')) {
            $filter = $this->get('publication.statuses_filter');

            $filter
                ->prepareData($request->get('filter'))
                ->makeFilter($monographs)
            ;
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $monographs,
            $request->get('page', 1),
            30
        );

        return $this->render('AsuBundle:Default/monography:monographys.html.twig', [
            'pagination'   => $pagination,
            'kinds'        => AbstractPublication::getKinds(),
            'st_published' => AbstractPublication::ST_IS_PUBLISHED,
        ]);
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $monography = new Monography();
        $statuses = [
            'status'        => AbstractPublication::ST_IN_WORK,
            'is_published'  => 0,
            'is_show_nauka' => 0,
            'has_reviewers' => 0,
            'has_published' => 0,
            'has_agreement' => 0,
            'has_images_owned' => 0,
        ];

        $monography
            ->setPublisher($this->getUser()->getCorporate())
            ->setStatuses($statuses);

        $form = $this->createForm($this->get('asu.form.publication.monography_type'), $monography)
            ->add('save', 'submit', [
                'label' => 'Сохранить',
                'attr' => ['class' => 'btn btn-success pull-right']
            ]);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $constraints = $this->get('validator')->validate($form);
            foreach ($constraints as $constraint) {
                $form->addError(new FormError($constraint->getMessage()));
            }

            if ($form->isValid()) {
                /** @var Monography $monography */
                $monography = $form->getData();

                if ($request->files->get($form->getName()) !== null) {
                    $filename = $this->get('upload_helper')->upload('cover', $form->getName());

                    if ($filename !== null && !is_array($filename)) {
                        $monography->setCover($filename);
                    }
                }

                $this->getODM()->persist($monography);
                $this->getODM()->flush();

                if ($monography->getStatuses()['status'] == AbstractPublication::ST_IS_PUBLISHED) {
                    $redirect = $this->redirectToRoute('monography_list');
                } else {
                    $redirect = $this->redirectToRoute('monography_authors', [
                        'id'    => $monography->getId()
                    ]);
                }
                return $redirect;
            }
        }
        
        return $this->render('AsuBundle:Default/monography:monography_edit.html.twig', [
            'sidemenu' => $this->get('navigation.menu')->get('Side')->getMenu('monography', [ $monography ]),
            'form'  => $form->createView(),
            'monography' => $monography,
            'st_published' => AbstractPublication::ST_IS_PUBLISHED,
        ]);
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param Monography $monography
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Monography $monography, Request $request)
    {
        $form = $this->createForm($this->get('asu.form.publication.monography_type'), $monography)
            ->add('save', 'submit', [
                'label' => 'Сохранить',
                'attr' => ['class' => 'btn btn-success pull-right']
            ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($monography->getStatuses()['is_published'] == 1) {
                $monography
                    ->setDatePublished(new \DateTime())
                    ->setStatus('status', AbstractPublication::ST_IS_PUBLISHED);
                $redirect = $this->redirectToRoute('monography_list');
            } else {
                $redirect = $this->redirectToRoute('monography_authors', [
                    'id'    => $monography->getId()
                ]);
            }


            if ($request->files->get($form->getName()) !== null) {
                $filename = $this->get('upload_helper')->upload('cover', $form->getName());

                if ($filename !== null && !is_array($filename)) {
                    $monography->setCover($filename);
                }
            }

            $this->getODM()->flush();
            $this->addFlash('notice', 'Монография сохранена!');

            return $redirect;
        } else {
            $errors = $this->get('validator')->validate($form);
            foreach ($errors as $e) {
                $this->addFlash('error', $e->getMessage());
            }
        }

        return $this->render('AsuBundle:Default/monography:monography_edit.html.twig', [
            'sidemenu' => $this->get('navigation.menu')->get('Side')->getMenu('monography', [ $monography ]),
            'form'  => $form->createView(),
            'monography' => $monography,
            'st_published' => AbstractPublication::ST_IS_PUBLISHED,
        ]);
    }

    /**
     * @param Monography $monography
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function textAction(Monography $monography, Request $request)
    {
        $form = $this->createForm(new TextType(), $monography);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getODM()->flush();
        } else {
            $session = $this->get('session');
            foreach ($form->getErrors() as $error) {
                $session->getFlashBag()->add('error', $error->getMessage());
            }
        }

        return $this->render('AsuBundle:Default/monography:monography_text.html.twig', [
            'sidemenu'   => $this->get('navigation.menu')->get('Side')->getMenu('monography', [$monography]),
            'form'       => $form->createView(),
            'entity'     => $monography
        ]);
    }

    
    /**
     * @param Monography $monography
     * @param Request    $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function authorsAction(Monography $monography, Request $request)
    {
        $this->get('session')->remove('way[route]');
        $this->get('session')->remove('way[id]');

        $form = $this->createForm(new AuthorType());
        $newAuthorForm = $this->createForm(new NewAuthorType());

        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);
            $newAuthorForm->handleRequest($request);
            
            if ($form->isValid()) {
                $author = $form->getData()['author'];

                if (!in_array($author, $monography->getAuthors()->toArray())) {
                    $monography->addAuthor($author);

                    $this->getODM()->persist($monography);
                    $this->getODM()->flush();

                    return $this->redirectToRoute('monography_authors', [ 'id' => $monography->getId() ]);
                }
            }

            if ($newAuthorForm->isValid()) {
                $author = $newAuthorForm->getData();
                $author->setFio(trim(
                    $author->getRu()['last_name'] . ' ' .
                    $author->getRu()['first_name'] . ' ' .
                    $author->getRu()['second_name']
                ));

                if (!in_array($author, $monography->getAuthors()->toArray())) {
                    $monography->addAuthor($author);

                    $this->getODM()->persist($author);
                    $this->getODM()->persist($monography);
                    $this->getODM()->flush();

                    return $this->redirectToRoute('author_edit', [
                        'id'         => $author->getId(),
                        'way[route]' => 'monography_authors',
                        'way[id]'    => $monography->getId(),
                    ]);
                }
            }
        }

        return $this->render('AsuBundle:Default/monography:monography_authors.html.twig', [
            'sidemenu'      => $this->get('navigation.menu')->get('Side')->getMenu('monography', [$monography]),
            'entity'        => $monography,
            'form'          => $form->createView(),
            'newAuthorForm' => $newAuthorForm->createView(),
        ]);
    }
    
    /**
     * Загрузить файл
     *
     * @param Monography $monography
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function filesUploadAction(Monography $monography)
    {
        $odm = $this->getODM();

        /** @var null|FileStorageDocument[] $existsFiles */
        $existsFiles = $odm->getRepository('AsuBundle:FileStorage')
            ->getFilesByDocument($monography);

        $form = $this->createForm(new FileStorage(), null, [
            'document_hidden_class' => $monography,
            'field_name'            => 'path',
            'multi_upload'          => true
        ]);

        $fileStorageHelper = $this->get('file_storage.helper');
        $errors = $fileStorageHelper->upload($form);

        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $error->getClientMimeType();

                $this->addFlash(
                    'error',
                    sprintf(
                        'File "%s" has wrong mime-type (%s). Allowed mime-types: "%s"',
                        $error->getClientOriginalName(),
                        $error->getClientMimeType(),
                        implode(', ', $fileStorageHelper->getAllowedMimeTypes())
                    )
                );
            }
        }

        return $this->render(
            'AsuBundle:Default/monography:monography_files.html.twig',
            [
                'form'     => $form->createView(),
                'sidemenu' => $this->get('navigation.menu')->get('Side')->getMenu('monography', [ $monography ]),
                'files'    => $existsFiles,
                'article'  => $monography,
            ]
        );
    }

    /**
     * Удалить файл
     *
     * @param Monography $monography
     * @param            $file_id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function filesRemoveAction(Monography $monography, $file_id)
    {
        $remove = $this->get('file_storage.helper')->remove($file_id);

        if (!$remove) {
            throw new NotFoundHttpException(
                'File which you want remove not found in database'
            );
        }

        return $this->redirectToRoute('monography_files', ['id' => $monography->getId()]);
    }


    /**
     * @Security("is_granted('ROLE_DELETE')")
     * @param Monography $monography
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction(Monography $monography)
    {
        if ($monography->getStatuses()['status'] != AbstractPublication::ST_IS_PUBLISHED) {
            $this->getODM()->remove($monography);
            $this->getODM()->flush();
            $this->addFlash('notice', 'Монография успешно удалена!');
        } else {
            $this->addFlash('notice', 'Нельзя удалить опубликованную монографию!');
        }

        return $this->redirectToRoute('monography_list');
    }


    /**
     * @Security("is_granted('ROLE_DELETE')")
     * @param Monography $monography
     * @param $authorId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function removeAuthorAction(Monography $monography, $authorId)
    {
        $author = $this->getODM()->getRepository('AsuBundle:Author')->find($authorId);

        if (null !== $author && in_array($author, $monography->getAuthors()->toArray())) {
            $monography->removeAuthor($author);
            $this->getODM()->persist($monography);
            $this->getODM()->flush();
        }

        return $this->redirectToRoute('monography_authors', [ 'id' => $monography->getId() ]);
    }


    /**
     * @param Monography $monography
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function chaptersAction(Monography $monography, Request $request)
    {
        $form = $this->createForm(new ChapterType(), null, [
            'publication' => $monography,
        ]);

        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                /** @var Chapter $chapter */
                $chapter = $form->getData();

                if (!in_array($chapter, $monography->getChapters()->toArray())) {
                    $monography->addChapter($chapter);
                    $chapter->setPublication($monography);
                    $this->getODM()->persist($chapter);
                    $this->getODM()->flush();

                    return $this->redirectToRoute('monography_chapter_edit', [
                        'publication_id' => $monography->getId(),
                        'id' => $chapter->getId(),
                    ]);
                }
            }
        }

        return $this->render(
            'AsuBundle:Default/monography:monography_chapters.html.twig',
            [
                'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('monography', [$monography]),
                'entity'    => $monography,
                'form'      => $form->createView()
            ]
        );
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param Chapter $chapter
     * @param $publication_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function chapterEditAction(Chapter $chapter, $publication_id, Request $request)
    {
        $monography = $this->getODM()->getRepository('AsuBundle:Monography')->find($publication_id);
        $form = $this->createForm(new ChapterEditType(), $chapter, [
            'publication'   => $monography
        ])->add('save', 'submit', [
            'label' => 'Сохранить',
            'attr' => ['class' => 'btn btn-success pull-right']
        ]);
        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $this->getODM()->flush();

                return $this->redirectToRoute('monography_chapters', [ 'id' => $monography->getId() ]);
            }
        }

        $request->attributes->set('_route', 'monography_chapters');
        $request->attributes->set('_route_params', ['id' => $publication_id]);

        return $this->render('AsuBundle:Default/templates:chapter_edit.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('monography', [$monography]),
            'form'      => $form->createView(),
            'entity'    => $chapter,
        ]);
    }


    /**
     * @Security("is_granted('ROLE_DELETE')")
     * @param Chapter $chapter
     * @param $publication_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function chapterRemoveAction(Chapter $chapter, $publication_id)
    {
        $monography = $this->getODM()->getRepository('AsuBundle:Monography')->find($publication_id);
        $monography->removeChapter($chapter);

        $this->getODM()->remove($chapter);
        $this->getODM()->flush();

        return $this->redirectToRoute('monography_chapters', [ 'id' => $publication_id ]);
    }


    /**
     * @param Monography $monography
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function referencesAction(Monography $monography)
    {
        return $this->render('AsuBundle:Default/monography:monography_references.html.twig', [
            'sidemenu'   => $this->get('navigation.menu')->get('Side')->getMenu('monography', [$monography]),
            'active'     => 'refs',
            'entity'     => $monography,
        ]);
    }


    /**
     * @param Monography $monography
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Monography $monography)
    {
        //        $classif = new Classif();
        //        $classifs = $classif->loadClassif(['okso_journal', 'grnti', 'tbk']);
        $classifs = [];
        return $this->render('AsuBundle:Default/monography:monography_show.html.twig', [
            'entity'    => $monography,
            'classifs'  => $classifs
        ]);
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param Monography $monography
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addReferenceAction(Monography $monography, Request $request)
    {
        $form = $this->createForm(new AddForm(), null);

        if ($request->getMethod() === "POST") {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $this->getODM()->getRepository('AsuBundle:Reference')
                    ->addReferences($data, $monography, $this->get('validator'));

                return $this->redirectToRoute('monography_references', [ 'id' => $monography->getId() ]);
            }
        }

        return $this->render('AsuBundle:Default/templates:reference_add.html.twig', [
            'sidemenu'   => $this->get('navigation.menu')->get('Side')->getMenu('monography', [$monography]),
            'active'     => 'refs',
            'form' => $form->createView()
        ]);
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param $publication_id
     * @param Reference $reference
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function referenceEditAction($publication_id, Reference $reference, Request $request)
    {
        $monography = $this->getODM()->getRepository('AsuBundle:Monography')->find($publication_id);
        $form = $this->createForm(new EditForm(), $reference);
        if ($request->getMethod() === "PUT") {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->getODM()->flush($reference);

                return $this->redirectToRoute('monography_references', [ 'id' => $publication_id ]);
            }
        }

        return $this->render('AsuBundle:Default/templates:reference_edit.html.twig', [
            'sidemenu'   => $this->get('navigation.menu')->get('Side')->getMenu('monography', [$monography]),
            'active'     => 'refs',
            'form' => $form->createView()
        ]);
    }


    /**
     * @Security("is_granted('ROLE_DELETE')")
     * @param Monography $monography
     * @param Reference $reference
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @ParamConverter("monography", class="Editorum\Bundle\Document\Monography", options={"id" = "publication_id"})
     */
    public function referenceRemoveAction(Monography $monography, Reference $reference)
    {
        $monography->removeReference($reference);
        $this->getODM()->flush();

        return $this->redirectToRoute('monography_references', [ 'id' => $monography->getId() ]);
    }


    /**
     * @param Monography $monography
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function rincAction(Monography $monography)
    {
        return $monography->createRincArchieve();
    }


    /**
     * @param Monography $monography
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function classifsAction(Monography $monography)
    {
        $classifs = [
            'udk' => $this->getODM()->getRepository('AsuBundle:ClassificatorUdk')->findAll()[0],
            'grnti' => $this->getODM()->getRepository('AsuBundle:ClassificatorGrnti')->findAll()[0],
            'okso' => $this->getODM()->getRepository('AsuBundle:ClassificatorOkso')->findAll()[0],
            'bbk' => $this->getODM()->getRepository('AsuBundle:ClassificatorBbk')->findAll()[0],
            'tbk' => $this->getODM()->getRepository('AsuBundle:ClassificatorTbk')->findAll()[0],
        ];

        return $this->render('AsuBundle:Default/templates:classificators.html.twig', [
            'sidemenu' => $this->get('navigation.menu')->get('Side')->getMenu('monography', [$monography]),
            'entity'  => $monography,
            'classifs' => $classifs,
        ]);
    }

    public function referencesDeleteAllAction(Monography $article)
    {
        $allReferences = $article->getReferences();
        $article->clearReferences();
        foreach ($allReferences as $reference) {
            $this->getODM()->remove($reference);
        }

        $this->getODM()->flush();

        return $this->redirectToRoute('monography_references', [
            'id' => $article->getId()
        ]);
    }
}
