<?php
/**
 * Created by PhpStorm.
 * User: xor
 * Date: 26.08.15
 * Time: 11:33
 */

namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Logic\Organization;
use Editorum\Bundle\Services\GlobalMenus;
use Symfony\Component\HttpFoundation\Request;

class CompanyController extends BasicController
{

    /**
     *  Форма редактирования сведений об организации
     */
    public function indexAction()
    {
        GlobalMenus::setInterface(GlobalMenus::INT_PUBLISHER);
        $org = new Organization();
        $data = $org->loadConfig();
        
        return $this->render('AsuBundle:Default:organization_edit.html.twig', array(
            'sidemenu' => '',
            'active' => '',
            'data' => $data
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function saveAction(Request $request)
    {
        $data = $request->get('form');
        $o = new Organization();
        $data_current = $o->getById(1);
        // Объекта в БД нет
        if (is_null($data_current))
        {
            $data_current = $o->createEmpty();
            $o->save($data_current);
        }
        $id = $o->save($data);
        return $this->redirectToRoute('employee_index', [], 301);
    }

}
