<?php
namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Document\Article;
use Editorum\Bundle\Validator\PublicationValidator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class PublicationValidatorController
 * @package Editorum\Bundle\Controller
 */
class PublicationValidatorController extends BasicController
{
    /**
     * @param $id
     * @param $type
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function validateAction($id, $type)
    {
        $low_type = strtolower($type);
        $document = $this->getODM()->getRepository('AsuBundle:'.$type)->find($id);

        if (empty($document)) {
            throw new \Exception('Объект '.$type. '[id='.$id.'] не найден!');
        }

        $validator = $this->get('publication.validator');
        $result = $validator->validate($document);
        $this->getODM()->flush();

        return $this->render('AsuBundle:Default/templates:validation.html.twig', [
            'sidemenu' => $this->get('navigation.menu')->get('Side')->getMenu($low_type, [$document]),
            'result' => $result,
            'document' => $document,
        ]);
    }


    /**
     * @param $id
     * @param $type
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function publicateAction($id, $type)
    {
        $type = ucfirst($type);
        $document = $this->getODM()->getRepository('AsuBundle:' . $type)->find($id);

        if (empty($document)) {
            throw new \Exception('Объект '.$type. '[id='.$id.'] не найден!');
        }

        if (method_exists($document, 'publicate') && $document->getStatuses()['valid'] == 1) {
            $document->publicate();
            $this->getODM()->flush();

            return $this->redirectToRoute('document_doi_list', [
                'id' => $id,
                'type' => $document->getObjectName(),
            ]);
        }
        $this->addFlash('error', 'Объект не может быть опубликован!');

        return $this->redirectToRoute('publication_validate', [
            'id' => $id,
            'type' => $document->getObjectName(),
        ]);
    }
}
