<?php
namespace Editorum\Bundle\Controller;

use Doctrine\Common\Util\Inflector;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadataInfo;
use Editorum\Bundle\Services\Menu\GlobalMenu;
use Gedmo\Translatable\Document\Translation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class BasicController extends Controller
{
    /** @var GlobalMenu */
    protected $globalMenu;

    /**
     * Получить переводы для документа одм
     *
     * @param $document
     * @return array
     */
    protected function getTranslations($document)
    {
        $odm = $this->get('doctrine.odm.mongodb.document_manager');
        $repository = $odm->getRepository('Gedmo\Translatable\Document\Translation');
        return $repository->findTranslations($document);
    }

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
    }
    /**
     * @throws \Exception
     */
    public function preExecute()
    {
        // set top menu
        $this->globalMenu = $this->get('navigation.menu')->get('Global');
        $this->globalMenu->setInterface(GlobalMenu::INT_PUBLISHER);
    }

    /**
     * @param $route_name
     * @return mixed
     * @internal param $routename
     */
    private function forwardToRoute($route_name)
    {
        $routes = $this->get('router')->getRouteCollection();

        return $routes->get($route_name)->getDefaults()['_controller'];
    }

    /**
     * Save form data to document
     * @param $form
     * @param $document
     * @param Request $request
     * @return bool|\Symfony\Component\Validator\ConstraintViolationListInterface
     */
    public function saveTranslatedForm($form, &$document, Request $request)
    {
        $odm = $this->get('doctrine.odm.mongodb.document_manager');
        $form = $this->createForm($form, null, [
            'method' => $request->getMethod(),
        ]);
        $form->handleRequest($request);
        $validator = $this->get('validator');

        if ($form->isValid()) {
            $data = $form->getData();

            /** @var Translation $repository */
            $repository = $odm->getRepository('\\Gedmo\\Translatable\\Document\\Translation');

            $reflection = new \ReflectionClass($document);
            $metadata = $odm->getRepository($reflection->name)->getClassMetadata();

            $assoc = [];
            foreach ($metadata->getAssociationNames() as $name) {
                $assoc[$name] = $metadata->fieldMappings[$name]['type'];
            }

            foreach ($data as $name => $value) {
                if (is_array($value)) {
                    foreach ($value as $lang => $value) {
                        $repository->translate($document, $name, $lang, $value);
                    }
                } else {
                    $method = null;

                    if (isset($assoc[$name]) && ($assoc[$name] == ClassMetadataInfo::MANY)) {
                        $type = 'add';
                        $methodName = $type . Inflector::classify($name);
                        $methodName = Inflector::singularize($methodName);
                        $method = $methodName;
                    }

                    if ($method === null) {
                        $method = 'set' . Inflector::classify($name);
                    }

                    call_user_func([$document, $method], $value);
                }
            }

            $errors = $validator->validate($document);
            if ($errors->count() > 0) {
                return $errors;
            } else {
                $odm->persist($document);
                $odm->flush();
            }
        }

        return true;
    }

    /**
     * @return \Doctrine\ODM\MongoDB\DocumentManager
     */
    public function getODM()
    {
        return $this->get('doctrine.odm.mongodb.document_manager');
    }

    /**
     * Redirect to referer
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectToReferer()
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        $url = $request->headers->get('referer') === null ?
            $this->generateUrl('homepage', ['_locale' => $request->get('_locale')]) : $request->headers->get('referer');

        return $this->redirect($url);
    }
}
