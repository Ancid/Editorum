<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 05.11.2015
 * Time: 18:42
 */

namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Logic\Journal;
use Symfony\Component\HttpFoundation\JsonResponse;
use Editorum\Bundle\AccessSystem\Person;
use Editorum\Bundle\Services\GlobalMenus;
use Editorum\Bundle\Logic\Model;
use Editorum\Bundle\AccessSystem\User;

class PersonController extends BasicController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $list = Model::getByKeys(new Journal(), array());
        
        return $this->render('AsuBundle:Default:journals.html.twig', array('journals' => $list));
    }

    /**
     * Регистрация нового пользователя
     */
    public function registerAction()
    {
        $data = json_decode($this->get('request')->request->get('data'), true);
        $error = false;

        // Проверяем нет ли уже пользователя с таким email
        $user_by_email = Model::getByKeys(new User(), array('email' => $data['email']));
        if (!$user_by_email->isEmpty())
        {
            $error = true;
            $msg = '[111] Пользователь с указанным email уже существует. Воспользуйтесь кнопкой "Забыли пароль", чтобы войти.';
        }

        // Проверяем нет ли уже пользователя с таким телефоном
        $user_by_phone = Model::getByKeys(new User(), array('phone' => $data['cell_phone']));
        if (!$user_by_phone->isEmpty())
        {
            $error = true;
            $msg = '[110] Пользователь с указанным телефоном уже существует. Воспользуйтесь кнопкой "Забыли пароль", чтобы войти.';
        }

        // Проверяем на одинаковость паролей
        if ($data['password1'] != $data['password2'])
        {
            $error = true;
            $msg = '[101] Пароли не совпадают';
        }

        // Не пустой пароль
        if (strlen($data['password1']) == 0)
        {
            $error = true;
            $msg = '[102] Пароли не может быть пустым';
        }

        // Номер телефона не может быть пустым
        if (strlen($data['cell_phone']) == 0)
        {
            $error = true;
            $msg = '[103] Номер телефона не может быть пустым';
        }

        // Используем мобильный в качестве логина
        $login = $data['email'];

        // Ошибок нет, создаем аккаунт
        if ($error == false) {
            Model::insert(new User(), array(
                'first_name'                            => $data['first_name'],
                'last_name'                             => $data['last_name'],
                'second_name'                           => $data['second_name'],
                'login'                                 => $login,
                'password'                              => md5($data['password1']),
                'email'                                 => $data['email'],
                'phone'                                 => $data['cell_phone'],
                'code'                                  => rand(1000, 9999),
                'active'                                => false,
                'profiles'                              => array()
            ));

            //Отправляем код подтверждения на почту
//                mail($data['email'], 'Подтверждение регистрации', 'Ваш код подтверждения: '.$code);

            return new JsonResponse(json_encode(array(
                'do'    => 'code',
            )));

        } else {
            // Создаем ответ сервера. Не отправляем, так как нужно установить cookie
            $response = array(
                'do'    => 'error',
                'msg'   => $msg
            );
        }

        // Отправляем сообщение об ошибке или сигнал для обновления страницы
        return new JsonResponse(json_encode($response));
    }

    /**
     * Проверяем отправленный код и логиним пользователя
     */
    public function registerCheckAction()
    {
        $data = json_decode($this->get('request')->request->get('data'), true);
        //Если введен код подтверждения
        if (strlen($data['code']) > 0) {

            $login = $data['email'];
            $user_code = Model::getOneByKeys(new User(), array('login' => $login, 'password' => md5(trim($data['password1']))), array('code'));

             //Проверяем ввденный код
            if ((string)$data['code'] === (string)$user_code['code']) {
                User::activate($login);
                // Входим под вновь созданным пользователем
                Person::instance()->login($login, $data['password1']);

                $response = array(
                    'do'    => 'refresh',
                );
            } else {
                $response = array(
                    'do'    => 'error',
                    'msg'   => 'Неверный код подтверждения'
                );
            }
        } else {
            $response = array(
                'do'    => 'error',
                'msg'   => 'Не введен код подтверждения'
            );
        }

        return new JsonResponse(json_encode($response));
    }

    /**
     * Вход
     */
    public function loginAction()
    {
        $data = json_decode($this->get('request')->request->get('data'), true);
        Person::instance()->login($data['login'], $data['password']);

        return new JsonResponse(json_encode(array(
            'do'    => 'refresh',
        )));
    }

    /**
     * Выход
     */
    public function logoutAction()
    {
        Person::instance()->logout();
        return $this->redirectToRoute(GlobalMenus::getInterfaceHomepageUrl());
    }

    /**
     * Полный список всех пользователей
     */
    public function allUsersAction()
    {
        GlobalMenus::setInterface(GlobalMenus::INT_EDITORUM);
        $persons = Model::getAll(new User());
        return $this->render('AsuBundle:Default:users.html.twig', array('persons' => $persons));
    }
    
}
