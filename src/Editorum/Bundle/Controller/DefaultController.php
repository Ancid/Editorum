<?php

namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Document\Conference;
use Editorum\Bundle\Document\Journal;
use Editorum\Bundle\Document\Section;
use Editorum\Bundle\Form\CreateEntityType;
use Editorum\Bundle\Logic\Collections;
use Editorum\Bundle\Logic\MongoNode;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use MongoClient;
use MongoId;

class DefaultController extends BasicController
{

    /**
     * Отдаем систему администрирования
     */
    public function indexAction()
    {
        return $this->render('AsuBundle:Default:admin.html.twig');
    }

    /**
     * Homepage
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function homepageAction()
    {
        return $this->render('AsuBundle:Default:homepage.html.twig');
    }


    /**
     * Работа с сущностями (CRUD)
     * @param $action
     * @param $entity
     * @param $id
     * @return JsonResponse
     */
    public function entityAction($entity, $action, $id)
    {
        $m = new MongoClient();
        $ar = array();

        // @TODO Проверка авторизации
        // @TODO Проверка прав доступа к таблице
        $dataset = $m->editorum->__get($entity);

        if ($action == 'save') {
            $data = json_decode($this->get('request')->request->get('data'));
            $dataset->insert($data);
            $ar = array('last_insert_id' => $data->_id);
        } elseif ($action == 'publist') {
            $cursor  = $dataset->find();
            $ar = array();
            foreach ($cursor as $pub) {
                $ar[] = $pub;
            }
        } elseif ($action == 'getbyid') {
            $ar[] = $dataset->findOne(array('_id' => new MongoId($id)));
        }

        return new JsonResponse(json_encode($ar, true));
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createEntityAction(Request $request)
    {
        $form = $this->createForm(new CreateEntityType());

        $form->handleRequest($request);
        if ($form->isValid()) {
            $data = $form->getData();
            $to_save = ['ru' => ['title' => $data['title']]];
            $path = $data['entity_type'];

            $entity = null;
            switch ($data['entity_type']) {
                case 'journal':
                    $entity = new Journal();
                    $to_save = ['title' => $data['title']];
                    $entity->setRu($to_save);

                    if (null !== $this->getUser()->getCorporate()) {
                        $entity->setPublisher($this->getUser()->getCorporate());
                    }
                    $path = 'journal';
                    break;
                case 'conference ':
                    $entity = new Conference();
                    $to_save = ['title' => $data['title']];
                    $entity->setRu($to_save);
                    if (null !== $this->getUser()->getCorporate()) {
                        $entity->setPublisherId($this->getUser()->getCorporate()->getId());
                    }
                    $path = 'conference';
                    break;
                case 'collection':
                    $entity = new Collections();
                    break;
            }

            if ($entity instanceof MongoNode) {
                $entity_id = $entity->save($to_save);
            } else {
                $this->getODM()->persist($entity);
                $entity_id = $entity->getId();
            }

            /*Create default section*/
            if ($data['entity_type'] === 'conferences') {
                $conference = $this->getODM()->getRepository('AsuBundle:Conference')->find($entity_id);
                $default_section = new Section();
                $default_section->setTitle('Без секции');
                $default_section->setTitleEng('Default section');
                $default_section->setConference($conference);
                $default_section->setCreatedAt(time());
                $default_section->setUpdatedAt(time());
                $this->getODM()->persist($default_section);
            }

            $this->getODM()->flush();

            return $this->redirectToRoute($path.'_edit', ['id' => $entity_id], 301);
        }
        return $this->render('AsuBundle:Default:create_entity.html.twig', [
            'form'  => $form->createView()
        ]);
    }
}
