<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 17.12.2015
 * Time: 12:59
 */

namespace Editorum\Bundle\Controller;

use Editorum\Bundle\AgreementSystem\ASys;
use Editorum\Bundle\Logic\Model;
use Editorum\Bundle\Logic\Publication;
use Editorum\Bundle\Services\GlobalMenus;
use Symfony\Component\HttpFoundation\JsonResponse;

class SelfpublishingController extends BasicController
{
    /**
     * Список собственных и полученных автором произведений
     */
    public function indexAction()
    {
        GlobalMenus::setInterface(GlobalMenus::INT_SELFPUBLISHING);
        $publications = ASys::instance($this->get('session'))->getMyPublicationsByAgreementKinds(array(
                ASys::AGR_Owner,
                ASys::AGR_Coauthor
            ));

        return $this->render('AsuBundle:Default:selfpublishing_publist.html.twig', [
            'publications' => $publications
        ]);
    }

    /**
     * Форма создания нового произведения
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        GlobalMenus::setInterface(GlobalMenus::INT_PUBLISHER);
        $kinds = Publication::getPublicationKinds();
        return $this->render('AsuBundle:Default:publication_create.html.twig', array('kinds' => $kinds));
    }

    /**
     * Сохранение созданного произведения
     * @return JsonResponse
     */
    public function saveAction()
    {
        $data = json_decode($this->get('request')->request->get('data'), true);
        $pub_id = Model::update(new Publication(), $data);
        return new JsonResponse(json_encode(array('id' => $pub_id)));
    }

    /**
     * Редактирование общих сведений о произведении
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function metadataAction($id)
    {
        $pub = Model::getById(new Publication(), $id);

        if ($pub->__get('kind') == Publication::KIND_ARTICLE) {
            $page = $this->render('AsuBundle:Default:publication_metadata.html.twig', array(
                'id'            => $id,
                'pub'           => $pub,
                'ptypes'        => Publication::getPublicationKinds(), //Не используется
                'journals'      => Model::getByKeys(new Journal(), array()),
                'sidemenu'      => $this->get('navigation.side')->pubMenu($pub),
                'active'        => 'metadata'
            ));
        } else {
            $page = $this->render('AsuBundle:Default:publication_unknown.html.twig', array(
                'id'            => $id,
                'pub'           => $pub,
                'sidemenu'      => array(),
                'active'        => ''
            ));
        }

        return $page;
    }

    /**
     * Редактирование описания произведения
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function descriptionAction($id)
    {
        $pub = Model::getById(new Publication(), $id);
        if ($pub->__get('kind') == Publication::KIND_ARTICLE) {
            $classif = new Classif();
            $classifs = $classif->loadClassif(['okso_journal', 'grnti', 'tbk']);

            $page = $this->render('AsuBundle:Default:publication_descr.html.twig', array(
                'id'            => $id,
                'pub'           => $pub,
                'sidemenu'      => $this->get('navigation.side')->pubMenu($pub),
                'active'        => 'descr',
                'classifs'          => $classifs,
            ));
        } else {
            $page = $this->render('AsuBundle:Default:publication_unknown.html.twig', array(
                'id'            => $id,
                'pub'           => $pub,
                'sidemenu'      => array(),
                'active'        => ''
            ));
        }

        return $page;
    }

    /**
     * Управление списком авторов произведения
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function authorsAction($id)
    {
        $pub = Model::getById(new Publication(), $id);
        $authors = Publication::loadAuthors($id);

        return $this->render('AsuBundle:Default:publication_authors.html.twig', array(
            'id'                => $id,
            'sidemenu'          => $this->get('navigation.side')->pubMenu($pub),
            'authors'           => $authors,
            'active'            => 'authors'
        ));
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function authorssaveAction($id)
    {
        $data = json_decode($this->get('request')->request->get('data'), true);

        $p = new Publication();
        $id = $p->save($data);
        
        return new JsonResponse(json_encode(array('id' => $id)));
    }

    /**
     * Удаление
     * @param bool $id
     * @return JsonResponse
     */
    public function removeAction($id = false)
    {
        if ($id !== false  && $id > 0) {
            $data['_id'] = $id;
            $res = Model::remove(new Publication(), $data);
            return $this->redirectToRoute('publication_list', [], 301);
        } else {
            $data = json_decode($this->get('request')->request->get('data'), true);
            $res = Model::remove(new Publication(), $data);
            return new JsonResponse(json_encode(array('res' => $res)));
        }
    }

    /**
     * Управление списком глав произведения
     * @param $publication_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function chapterManagementAction($publication_id)
    {
        $publication = Model::getById(new Publication(), $publication_id);

        return $this->render('AsuBundle:Default:publication_chapters.html.twig', array(
            'sidemenu'  => $this->get('navigation.side')->pubMenu($publication),
            'pub'       => $publication,
            'active'    => 'chapters',
            'id'        => $publication_id
        ));
    }

    /**
     * Управление списком ссылок на источники
     * @param $publication_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function referencesManagementAction($publication_id)
    {
        $publication = Model::getById(new Publication(), $publication_id);

        return $this->render('AsuBundle:Default:publication_references.html.twig', array(
            'sidemenu'  => $this->get('navigation.side')->pubMenu($publication),
            'pub'       => $publication,
            'active'    => 'refs',
            'id'        => $publication_id
        ));
    }

}
