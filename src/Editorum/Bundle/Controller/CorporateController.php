<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 11.11.2015
 * Time: 13:22
 */

namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Document\Commercial;
use Editorum\Bundle\Document\Common\AbstractCorporateEntity;
use Editorum\Bundle\Document\Organization;
use Editorum\Bundle\Document\Publisher;
use Editorum\Bundle\Document\Research;
use Editorum\Bundle\Document\University;
use Editorum\Bundle\Document\User;
use Editorum\Bundle\Form\Corporate\CommercialType;
use Editorum\Bundle\Form\Corporate\CorporateSelectType;
use Editorum\Bundle\Form\Corporate\PublisherType;
use Editorum\Bundle\Form\Corporate\ResearchType;
use Editorum\Bundle\Form\Corporate\UniversityType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CorporateController
 * @package Editorum\Bundle\Controller
 *
 * @Security("is_granted('ROLE_ADMIN')")
 */
class CorporateController extends BasicController
{
    /**
     * @param Publisher $publisher
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function publisherEditAction(Publisher $publisher, Request $request)
    {
        $organization = $publisher->getOrganization();
        $form = $this->createForm(new PublisherType(), $publisher)->add('save', 'submit', [
            'label' => 'Сохранить',
            'attr' => ['class' => 'btn btn-success pull-right']
        ]);
        $form->handleRequest($request);

        if ($request->isMethod($request::METHOD_POST)) {
            if ($form->isValid()) {
                if (!empty($publisher->getStatuses()['editorum_prefix'])) {
                    $publisher->setDoiPrefix(Organization::EDITORUM_DOI_PREFIX);
                }
                $this->getODM()->flush();

                return $this->redirectToRoute('organization_corporates', ['id' => $organization->getId()], 301);
            }
        }

        return $this->render('AsuBundle:Default/organization:publisher_edit.html.twig', [
            'sidemenu'      => $this->get('navigation.menu')->get('Side')->getMenu('corporate', [$publisher]),
            'form'          => $form->createView(),
        ]);
    }


    /**
     * @param University $university
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function universityEditAction(University $university, Request $request)
    {
        $organization = $university->getOrganization();
        $form = $this->createForm(new UniversityType(), $university)->add('save', 'submit', [
            'label' => 'Сохранить',
            'attr' => ['class' => 'btn btn-success pull-right']
        ]);
        $form->handleRequest($request);

        if ($request->isMethod($request::METHOD_POST)) {
            if ($form->isValid()) {
                if (!empty($university->getStatuses()['editorum_prefix'])) {
                    $university->setDoiPrefix(Organization::EDITORUM_DOI_PREFIX);
                }
                $this->getODM()->flush();

                return $this->redirectToRoute('organization_corporates', ['id' => $organization->getId()], 301);
            }
        }

        return $this->render('AsuBundle:Default/organization:university_edit.html.twig', [
            'sidemenu'      => $this->get('navigation.menu')->get('Side')->getMenu('corporate', [$university]),
            'form'          => $form->createView(),
        ]);
    }


    /**
     * @param Research $research
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function researchEditAction(Research $research, Request $request)
    {
        $organization = $research->getOrganization();
        $form = $this->createForm(new ResearchType(), $research)->add('save', 'submit', [
            'label' => 'Сохранить',
            'attr' => ['class' => 'btn btn-success pull-right']
        ]);
        $form->handleRequest($request);

        if ($request->isMethod($request::METHOD_POST)) {
            if ($form->isValid()) {
                if (!empty($research->getStatuses()['editorum_prefix'])) {
                    $research->setDoiPrefix(Organization::EDITORUM_DOI_PREFIX);
                }
                $this->getODM()->flush();

                return $this->redirectToRoute('organization_corporates', ['id' => $organization->getId()], 301);
            }
        }

        return $this->render('AsuBundle:Default/organization:research_edit.html.twig', [
            'sidemenu'      => $this->get('navigation.menu')->get('Side')->getMenu('corporate', [$research]),
            'form'          => $form->createView(),
        ]);
    }


    /**
     * @param Commercial $commercial
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function commercialEditAction(Commercial $commercial, Request $request)
    {
        $organization = $commercial->getOrganization();
        $form = $this->createForm(new CommercialType(), $commercial)->add('save', 'submit', [
            'label' => 'Сохранить',
            'attr' => ['class' => 'btn btn-success pull-right']
        ]);
        $form->handleRequest($request);

        if ($request->isMethod($request::METHOD_POST)) {
            if ($form->isValid()) {
                if (!empty($commercial->getStatuses()['editorum_prefix'])) {
                    $commercial->setDoiPrefix(Organization::EDITORUM_DOI_PREFIX);
                }
                $this->getODM()->flush();

                return $this->redirectToRoute('organization_corporates', ['id' => $organization->getId()], 301);
            }
        }

        return $this->render('AsuBundle:Default/organization:research_edit.html.twig', [
            'sidemenu'      => $this->get('navigation.menu')->get('Side')->getMenu('corporate', [$commercial]),
            'form'          => $form->createView(),
        ]);
    }


    /**
     * @param Organization $organization
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Exception
     */
    public function createAction(Organization $organization, Request $request)
    {
        $form = $this->createForm(new CorporateSelectType())->add('save', 'submit', [
            'label' => 'Далее',
            'attr' => ['class' => 'btn btn-success pull-right']
        ]);

        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $kind = $request->get($form->getName())['kind'];
                $object = "Editorum\\Bundle\\Document\\".ucfirst($kind);
                if (class_exists($object)) {
                    $corporate = new $object;
                    $corporate->setKind($kind);

                    $this->getODM()->persist($corporate);
                    $organization->addCorporate($corporate);
                    $this->getODM()->flush();

                    return $this->redirectToRoute('corporate_'.$kind.'_edit', ['id' => $corporate->getId()]);
                }

                throw new \Exception('Не существует для юр.лица класса '.$object);
            }
        }

        return $this->render('AsuBundle:Default/organization:corporate_create.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @param $type
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction($type, $id)
    {
        /** @var AbstractCorporateEntity $corporate */
        $corporate = $this->getODM()->getRepository('AsuBundle:'.ucfirst($type))->find($id);
        if (null !== $corporate) {
            /** @var Organization $organization */
            $organization = $corporate->getOrganization();
            $userManager = $this->get('fos_user.user_manager');
            foreach ($corporate->getUsers() as $user) {
                $userManager->deleteUser($user);
            }
            $organization->removeCorporate($corporate);
            $this->getODM()->remove($corporate);
            $this->getODM()->flush();
            $this->addFlash('notice', 'Юр.лицо успешно удалено');

            return $this->redirectToRoute('organization_corporates', ['id' => $organization->getId()]);
        }

        return $this->redirectToRoute('organization_list');
    }


    /**
     * @param $id
     * @param $type
     * @return Response
     */
    public function employeeListAction($id, $type)
    {
        $corporate = $this->getODM()->getRepository('AsuBundle:'.ucfirst($type))->find($id);

        return $this->render('AsuBundle:Default/organization:employee_list.html.twig', [
            'sidemenu'      => $this->get('navigation.menu')->get('Side')->getMenu('corporate', [$corporate]),
            'corporate'     => $corporate,
        ]);
    }


    /**
     * @param $type
     * @param $id Corporate ID
     * @param Request $request
     * @return Response
     */
    public function employeeCreateAction($type, $id, Request $request)
    {
        $corporate = $this->getODM()->getRepository('AsuBundle:'.ucfirst($type))->find($id);

        $form = $this->createForm($this->get('asu_bundle.corporate.form.user.type'))->add('save', 'submit', [
            'label' => 'Сохранить',
            'attr' => ['class' => 'btn btn-success pull-right']
        ]);

        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                /** @var User $user */
                $user = $form->getData();
                $user->setUsername($user->getEmail());
                $user->setEnabled(true);

                $user->setCorporate($corporate);
                $this->getODM()->persist($user);
                $this->getODM()->flush();

                return $this->redirectToRoute('corporate_employees', [
                    'id' => $id,
                    'type' => $corporate->corporateName()
                ], 301);
            } else {
                $this->addFlash('danger', 'Неверно заполнена форма!');
            }
        }

        return $this->render('AsuBundle:Default/organization:employee_edit.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('corporate', [$corporate]),
            'form'      => $form->createView()
        ]);
    }


    /**
     * @param User $user
     * @param Request $request
     * @return Response
     */
    public function employeeEditAction(User $user, Request $request)
    {
        $corporate = $user->getCorporate();
        $form = $this->createForm($this->get('asu_bundle.corporate.form.user.type'), $user)->add('save', 'submit', [
            'label' => 'Сохранить',
            'attr' => ['class' => 'btn btn-success pull-right']
        ]);

        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $user->setPassword(
                    $this->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword())
                );
                $user->setEnabled(true);
//                $this->getODM()->persist($user);
                $this->getODM()->flush();

                return $this->redirectToRoute('corporate_employees', [
                    'id'    => $corporate->getId(),
                    'type'  => $corporate->corporateName(),
                ], 301);
            }
        }

        return $this->render('AsuBundle:Default/organization:employee_edit.html.twig', [
            'sidemenu'      => $this->get('navigation.menu')->get('Side')->getMenu('corporate', [$corporate]),
            'form'      => $form->createView()
        ]);
    }


    /**
     * Форма приглашения сотрудника в организацию
     * @param $id
     * @param $type
     * @return Response
     */
    public function employeeInviteAction($id, $type)
    {
        $corporate = $this->getODM()->getRepository('AsuBundle:'.ucfirst($type))->find($id);

        return $this->render('AsuBundle:Default/organization:employee_invite.html.twig', array(
            'sidemenu'      => $this->get('navigation.menu')->get('Side')->getMenu('corporate', [$corporate]),
            'corporate'     => $corporate
        ));
    }


    /**
     * @param Request $request
     * @return Response
     * @internal param User $user
     */
    public function employeeRemoveAction(Request $request)
    {
        $employee = $this->getODM()->getRepository('AsuBundle:User')->find($request->get('id'));
        if (null !== $employee) {
            $this->getODM()->remove($employee);
            $this->getODM()->flush();
        }

        return new Response(json_encode(['success'  => true]));
    }
}
