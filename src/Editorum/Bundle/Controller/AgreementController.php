<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 16.11.2015
 * Time: 11:33
 */
namespace Editorum\Bundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Editorum\Bundle\Logic\Model;
use Editorum\Bundle\Logic\Publication;
use Editorum\Bundle\AgreementSystem\ASys;
use Editorum\Bundle\AgreementSystem\Agreements\PrintableAgreement;
use Editorum\Bundle\AgreementSystem\Agreements\PrintableAgreementTemplate;

class AgreementController extends BasicController
{

    /**
     * Выводим список шаблонов договоров
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function templatesAction()
    {
        $templates = Model::getAll(new PrintableAgreementTemplate());
        return $this->render('AsuBundle:Default:templates.html.twig', array('templates' => $templates));
    }


    /**
     * Редактирование / создание шаблона договора
     * @param $template_id - id шаблона для редактирования
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function templateEditAction($template_id)
    {
        if ($template_id > 0) {
            $data = Model::getById(new PrintableAgreementTemplate(), $template_id);
        } else {
            $data = Model::create(new PrintableAgreementTemplate());
        }

        return $this->render('AsuBundle:Default:template_edit.html.twig', array(
            'data' => $data,
            'id' => $template_id,
            'sidemenu' => array(),
            'active' => '',
            'master_classes' => ASys::instance($this->get('session'))->getPrintableAgreements()
        ));
    }

    /**
     * Интерфейс издателя. Сохраняем шаблон договора
     */
    public function templateSaveAction()
    {
        $data = json_decode($this->get('request')->request->get('data'), true);
        $agreement_id = Model::update(new PrintableAgreementTemplate(), $data);

        return new JsonResponse(json_encode(array('id' => $agreement_id)));
    }


    /**
     * Интерфейс издателя. Выбор шаблона договора и список договоров на произведение
     */
    public function templateSelectAction($publication_id)
    {
        $pub = Model::getById(new Publication(), $publication_id);
        $templates = Model::getAll(new PrintableAgreementTemplate());

        return $this->render('AsuBundle:Default:agreement_select.html.twig', array(
            'pub'       => $pub,
            'templates' => $templates,
            'sidemenu'  => $this->get('navigation.side')->pubMenu($pub),
            'active'    => 'agreement'
        ));
    }

    /**
     * Сохраняем выбранный шаблон договора
     * @param $publication_id
     * @return JsonResponse
     */
    public function templateSelectDoAction($publication_id)
    {
        $data = json_decode($this->get('request')->request->get('data'), true);
        $publication_id = Model::update(new Publication(), array(
            '_id'                   => $data['_id'],
            'agreement_template_id' => $data['agreement_template_id']
        ));

        return new JsonResponse(json_encode(array('id' => $publication_id)));

    }


    /**
     * Просмотр сгенерированного договора HTML
     * @param $agreement_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction($agreement_id)
    {
        $agr = Model::getById(new PrintableAgreement(), $agreement_id);

        return $this->render('AsuBundle:Default:agreement_view.html.twig', array(
            'agreement' => $agr
        ));

    }

    /**
     * Интерфейс издателя. Генерация договора(ов)
     */
    public function generateAction($publication_id)
    {
        $pub = Model::getById(new Publication(), $publication_id);
        if ($pub->agreement_template_id > 0) {
            // получаем выбранный шаблон
            $template = Model::getById(new PrintableAgreementTemplate(), $pub->agreement_template_id);

            // @TODO Удаляем все договора, связанные с текущим произведением

            // Подключаем TWIG
            $twig = new \Twig_Environment(new \Twig_Loader_String());

            if ($template->is_collective == PrintableAgreementTemplate::T_INDIVIDUAL) {
                // Генерируем набор индивидуальных договоров
                $authors = $pub->getAuthors();

                foreach ($authors as $a) {
                    $number = $pub->_id.'.'.$a->_id;

                    $html = $twig->render(
                        $template->template,
                        array(
                            'number'    => $number,
                            'pub'       => $pub,
                            'author'    => $a,
                            'org'       => array(), // @TODO Сделать передачу организации
                        )
                    );

                    Model::update(new PrintableAgreement(), array(
                        'number'            => $number,
                        'text'              => $html,
                        'pdf_file'          => '',
                        'pdf_url'           => '',
                        'agreement_id'      => 0,
                        'pub_id'            => $pub->_id
                    ));
                }
            } else {
                // Генерируем коллективный договор
                $authors = $pub->getAuthors();

                $number = $pub->_id.'.collective';

                    $html = $twig->render(
                        $template->template,
                        array(
                            'number'    => $number,
                            'pub'       => $pub,
                            'authors'   => $pub->getAuthors(),
                            'org'       => array(), // @TODO Сделать передачу организации
                        )
                    );

                    Model::update(new PrintableAgreement(), array(
                        'number'            => $number,
                        'text'              => $html,
                        'pdf_file'          => '',
                        'pdf_url'           => '',
                        'agreement_id'      => 0,
                        'pub_id'            => $pub->_id
                    ));

            }


            $agreements = Model::getByKeys(new PrintableAgreement(), array('pub_id' => $pub->_id));
            return $this->render('AsuBundle:Default:agreements.html.twig', array('agreements' => $agreements));

        } else {
            return $this->render('AsuBundle:Default:error.html.twig', array(
                'text'      => 'Невозможно сгенерировать договор. Отсутствует шаблон',
                'sidemenu'  => $this->get('navigation.side')->pubMenu($pub),
                'active'    => 'agreement'
            ));
        }
    }

    /*
     * Удаляем шаблон договора
     */
    public function templateRemoveAction()
    {
        $data = json_decode($this->get('request')->request->get('data'), true);
        $agreement_id = Model::remove(new PrintableAgreementTemplate(), $data);

        return new JsonResponse(json_encode(array('result' => $agreement_id)));
    }

}
