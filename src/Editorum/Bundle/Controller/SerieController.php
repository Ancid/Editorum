<?php
/**
 * Created by PhpStorm.
 * User: vgfilippov
 * Date: 02.10.15
 * Time: 17:31
 */

namespace Editorum\Bundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Editorum\Bundle\Logic\Serie;
use Editorum\Bundle\Logic\Model;

/**
 * Class SerieController
 * @package Editorum\Bundle\Controller
 * 
 * @Security("is_granted('ROLE_SERIE_GROUP')")
 */
class SerieController extends BasicController
{
    /**
     * @param $serie_id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function indexAction($serie_id)
    {
        if ($serie_id > 0) {
            $list = Serie::loadList();
            $series = Model::getList(new Serie(), $list);
        } else {
            $series = Model::getByKeys(new Serie(), array());
        }
        
        return $this->render('AsuBundle:Default/serie:series.html.twig', array('series' => $series));
    }

    public function showAction($id)
    {
        $serie = $this->get('doctrine.odm.mongodb.document_manager')->find('AsuBundle:Serie', $id);

        return $this->render('AsuBundle:Default/serie:serie_show.html.twig', [
            'serie' => $serie
        ]);
    }

    /**
     * @param $serie_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction($serie_id)
    {
        if ($serie_id > 0) {
            $data = Model::getById(new Serie(), $serie_id);
        } else {
            $data = Model::create(new Serie());
        }

        return $this->render('AsuBundle:Default/serie:serie_edit.html.twig', array('data' => $data, 'id' => $serie_id));
    }

    /**
     * @return JsonResponse
     */
    public function saveAction()
    {
        $data = json_decode($this->get('request')->request->get('data'), true);
        $serie_id = Model::update(new Serie(), $data);
        
        return new JsonResponse(json_encode(array('id' => $serie_id)));
    }

    /**
     * @return JsonResponse
     */
    public function removeAction()
    {
        $data = json_decode($this->get('request')->request->get('data'), true);
        $res = Model::remove(new Serie(), $data);
        
        return new JsonResponse(json_encode(array('res' => $res)));

    }
    
}
