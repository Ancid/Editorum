<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 23.08.2015
 * Time: 13:06
 */

namespace Editorum\Bundle\Controller;

use DocumentBundle\Model\Document;
use Editorum\Bundle\Document\Article;
use Editorum\Bundle\Document\Doi;
use Editorum\Bundle\Document\FileStorage;
use Editorum\Bundle\Document\IssueRubricList;
use Editorum\Bundle\Document\Journal;
use Editorum\Bundle\Document\Rubric;
use Editorum\Bundle\Document\Issue;
use Editorum\Bundle\Form\Rubric\RubricType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class JournalController
 * @package Editorum\Bundle\Controller
 *
 * @Security("is_granted('ROLE_JOURNAL_GROUP')")
 */
class JournalController extends BasicController
{
    /**
     * Список журналов
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $user = $this->getUser();
        $qb = $this->getODM()->createQueryBuilder('AsuBundle:Journal');

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $corp = [];
            foreach ($this->getUser()->getCorporate()->getOrganization()->getCorporates()->toArray() as $co) {
                $corp[$co->getKind()][] = $co->getId();
            }
            foreach ($corp as $type => $ids) {
                $qb
                    ->addOr(
                        $qb->expr()
                            ->addAnd($qb->expr()->field('publisher.$ref')->equals($type))
                            ->addAnd($qb->expr()->field('publisher.$id')->in($ids))
                    );
            }
        } else {
            $qb->field('publisher')->references($user->getCorporate());
        }

        // @ToDo прорефакторить
        $search_txt = $request->get('search');
//        if (!is_null($search_txt) && strlen($search_txt) > 0) {
//            $results = Search::getIds($search_txt, Journal::KIND_JOURNAL);
//            $journals->addAnd($journals->expr()->field('_id')->in($results['matches']));
//        }

        $journals = $qb->sort('id', 'DESC');

        if ($request->get('filter')) {
            $filter = $this->get('publication.statuses_filter');

            $filter
                ->prepareData($request->get('filter'))
                ->makeFilter($journals);
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $journals,
            $this->get('request')->query->getInt('page', 1),
            30
        );

        return $this->render('AsuBundle:Default/journal:journals.html.twig', [
            'pagination' => $pagination,
            'search_txt' => $search_txt,
            'kinds'      => Journal::getKinds(),
        ]);
    }


    /**
     * Просмотр журналов
     *
     * @param Journal $journal
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Journal $journal)
    {
//        $classif = new Classif();
//        $classifs = $classif->loadClassif(['okso_journal', 'grnti', 'tbk']);

        return $this->render(
            'AsuBundle:Default/journal:journal_show.html.twig',
            [
                'journal' => $journal,
//                'classifs' => $classifs,
            ]
        );
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $journal = new Journal();
        $journal
            ->setPublisher($this->getUser()->getCorporate())
            ->setStatuses([
                'status'         => Journal::ST_NEW,
                'is_vak'         => 0,
                'is_rinc'        => 0,
                'is_show_nauka'  => 0,
                'st_rubricated'  => 0,
                'is_request_allow' => 0,
            ]);

        $form = $this->createForm($this->get('asu.form.journal.type'), $journal)
            ->add('save', 'submit', [
                'label' => 'Сохранить',
                'attr' => ['class' => 'btn btn-success pull-right']
            ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($request->files->get($form->getName()) !== null) {
                $filename = $this->get('upload_helper')->upload('cover', $form->getName());

                if ($filename !== null && !is_array($filename)) {
                    $journal->setCover($filename);
                }
            }

            $this->getODM()->persist($journal);
            $this->getODM()->flush();
            $this->addFlash('notice', 'Журнал создан!');

            return $this->redirectToRoute('journal_rubrics', [ 'id' => $journal->getId() ]);
        }

        return $this->render('AsuBundle:Default/journal:journal_edit.html.twig', [
            'sidemenu'      => $this->get('navigation.menu')->get('Side')->getMenu('journal', [$journal]),
            'form'          => $form->createView(),
            'st_publishing' => Journal::ST_IS_PUBLISHING,
            'journal'       => $journal
        ]);
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param Journal $journal
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Journal $journal, Request $request)
    {
        $j_repo = $this->getODM()->getRepository('AsuBundle:Journal');
        $old_pid = $journal->getPublisher()->getId();

        $form = $this->createForm($this->get('asu.form.journal.type'), $journal)->add('save', 'submit', [
            'label' => 'Сохранить',
            'attr' => ['class' => 'btn btn-success pull-right']
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($old_pid !== $journal->getPublisher()->getId()) {
                $j_repo->updatePublisher($journal);
            }

            if ($request->files->get($form->getName()) !== null) {
                $filename = $this->get('upload_helper')->upload('cover', $form->getName());

                if ($filename !== null && !is_array($filename)) {
                    $journal->setCover($filename);
                }
            }

            $this->getODM()->flush();
            $this->addFlash('notice', 'Сохранено!');

            return $this->redirectToRoute('journal_rubrics', ['id' => $journal->getId()]);
        }

        return $this->render('AsuBundle:Default/journal:journal_edit.html.twig', [
            'sidemenu'      => $this->get('navigation.menu')->get('Side')->getMenu('journal', [$journal]),
            'form'          => $form->createView(),
            'st_publishing' => Journal::ST_IS_PUBLISHING,
            'journal'       => $journal,
        ]);
    }
    
    /**
     * @param Journal $journal
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function classifsAction(Journal $journal)
    {
        $classifs = [
            'udk' => $this->getODM()->getRepository('AsuBundle:ClassificatorUdk')->findAll()[0],
            'grnti' => $this->getODM()->getRepository('AsuBundle:ClassificatorGrnti')->findAll()[0],
            'okso' => $this->getODM()->getRepository('AsuBundle:ClassificatorOkso')->findAll()[0],
            'bbk' => $this->getODM()->getRepository('AsuBundle:ClassificatorBbk')->findAll()[0],
            'tbk' => $this->getODM()->getRepository('AsuBundle:ClassificatorTbk')->findAll()[0],
        ];

        return $this->render('AsuBundle:Default/templates:classificators.html.twig', [
            'sidemenu' => $this->get('navigation.menu')->get('Side')->getMenu('journal', [$journal]),
            'entity'  => $journal,
            'classifs' => $classifs,
        ]);
    }


    /**
     * @Security("is_granted('ROLE_DELETE')")
     * @param Journal $journal
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction(Journal $journal)
    {
        $this->getODM()->remove($journal);
        $this->getODM()->flush();
        $this->addFlash('notice', 'Журнал удален!');

        return $this->redirectToRoute('journal_list');
    }


    /**
     * Статьи журнала не входящие ни в один из номеров
     * @param Journal $journal
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function journalArticlesAction(Journal $journal, Request $request)
    {
        $articles = $this->getODM()->getRepository('AsuBundle:Journal')->getAvailableArticlesQuery($journal);
        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $articles,
            $request->query->getInt('page', 1),
            30
        );

        return $this->render('AsuBundle:Default/journal:journal_articles.html.twig', [
            'sidemenu'   => $this->get('navigation.menu')->get('Side')->getMenu('journal', [$journal]),
            'pagination' => $pagination,
            'journal'    => $journal,
            'kinds'      => Journal::getKinds(),
            'st_published' => Journal::ST_IS_PUBLISHED,
        ]);
    }


    /**
     * @param Journal $journal
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function rubricsAction(Journal $journal, Request $request)
    {
        $form = $this->createForm(new RubricType());
        $response = $this->render('AsuBundle:Default/journal:journal_rubrics.html.twig', [
            'sidemenu' => $this->get('navigation.menu')->get('Side')->getMenu('journal', [$journal]),
            'journal'  => $journal,
            'form'     => $form->createView()
        ]);
        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if (!$form->isValid()) {
                foreach ($form->getErrors() as $error) {
                    $this->addFlash('error', $error->getMessage());
                }
                return $response;
            }

            $rubric = $form->getData();

            if (!in_array($rubric, $journal->getRubrics()->toArray())) {
                $journal->addRubric($rubric);
            }

            $this->getODM()->persist($rubric);
            $this->getODM()->flush();
            $this->addFlash('notice', 'Рубрика добавлена!');

            $response = $this->redirectToRoute('journal_rubrics', ['id' => $journal->getId()]);
        }

        return $response;
    }


    /**
     * @param Journal $journal
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function issuesAction(Journal $journal, Request $request)
    {
        $paginator = $this->get('knp_paginator');
        $issues = $paginator->paginate(
            $journal->getIssues(),
            $request->get('page', 1),
            30
        );

        return $this->render('AsuBundle:Default/journal:journal_issues.html.twig', array(
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('journal', [$journal]),
            'issues'    => $issues,
            'journal'   => $journal,
            'kinds'     => Journal::getKinds(),
        ));
    }


    /**
     * @Security("is_granted('ROLE_DELETE')")
     * @param Journal $journal
     * @param $rubric_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function rubricRemoveAction(Journal $journal, $rubric_id)
    {
        $rubric = $this->getODM()->getRepository('AsuBundle:Rubric')->find($rubric_id);
        $journal->removeRubric($rubric);

        //удаляем рубрику из номеров
        $qb = $this->getODM()->getRepository('AsuBundle:IssueRubricList')->createQueryBuilder();
        $rubriclist = $qb->field('rubric')->references($rubric)->getQuery()->execute()
        ;
        foreach ($rubriclist as $r) {
            $this->getODM()->remove($r);
        }

        $this->getODM()->remove($rubric);
        $this->getODM()->flush();

        $this->addFlash('notice', 'Рубрика удалена!');

        return $this->redirectToRoute('journal_rubrics', ['id' => $journal->getId()]);
    }


    /**
     * @param Journal $journal
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function composeMultiplyAction(Journal $journal)
    {
        $articles = $this->getODM()->getRepository('AsuBundle:Journal')->getAvailableArticles($journal);
        $issues = $this->getODM()->getRepository('AsuBundle:Journal')->getAvailableIssues($journal);

        return $this->render(
            'AsuBundle:Default/issue:issue_compose_multiply.html.twig',
            [
                'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('journal', [$journal]),
                'journal'   => $journal,
                'articles'  => $articles,
                'issues'    => $issues,

            ]
        );
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function saveMultiplyAction(Request $request)
    {
        $issues = json_decode($request->get('issues'), true);
        $dm = $this->getODM();

        $j_init = false;
        foreach ((array)$issues as $id => $value) {
            if (is_array($value)) {
                $issue = $dm->getRepository('AsuBundle:Issue')->find($id);

                if ($j_init != true) {
                    $journal = $issue->getJournal();
                    $j_init = true;
                }

                foreach (array_unique(array_map('intval', $value)) as $art_id) {
                    $article = $dm->getRepository('AsuBundle:Article')->find($art_id);
                    $article->setIssue($issue);

                    //Если у статьи есть рубрика
                    if (null != $article->getRubric()) {
                        $dm->getRepository('AsuBundle:Issue')->addRubric($issue, $article->getRubric());
                        $dm->refresh($issue);
                    } else {
                        //добавляем рубрику "без рубрики"
                        $new_rubric = $dm->getRepository('AsuBundle:Journal')->addEmptyRubric($journal);
                        $dm->getRepository('AsuBundle:Issue')->addRubric($issue, $new_rubric);
                        $dm->refresh($issue);
                        $article->setRubric($new_rubric);
                    }
                }
            }
        }

        $articles = json_decode($request->get('articles'), true);
        foreach ((array)$articles as $article_id) {
            $art = $dm->getRepository('AsuBundle:Article')->find($article_id);

            if (null != $art->getIssue()) {
                $art->setIssue(null);
            }
        }
        $dm->flush();

        return new JsonResponse(json_encode(['success' => true]));
    }


    /**
     * Редактирование рубрики через x-editable
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\ODM\MongoDB\LockException
     * @throws \Throwable
     * @throws \TypeError
     */
    public function rubricFieldEditAction(Request $request)
    {
        $data = $request->request->all();
        $client = $this->get('wsse.security.client')->getClient();
        $req = $client->put(
            $this->get('router')->generate('asu_api_editable', [ 'alias' => 'rubric' ], Router::ABSOLUTE_URL),
            [
                'json'         => $data,
                'headers'      => [
                    'Content-Type' => 'application/json',
                ]
            ]
        );

        $result = $req->getBody()->getContents();

        return new JsonResponse($result);
    }
}
