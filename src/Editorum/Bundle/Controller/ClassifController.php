<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 23.08.2015
 * Time: 13:06
 */

namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Document\Article;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @package Editorum\Bundle\Controller
 *
 * @Security("is_granted('ROLE_ARTICLE_GROUP')")
 */
class ClassifController extends BasicController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function ajaxClassifAction(Request $request)
    {
        $response = [
            'success' => $this->getODM()->getRepository('AsuBundle:Common\AbstractClassificator')
                ->updateClassif($request),
            'errors'  => []
        ];

        return new JsonResponse(json_encode($response));
    }
}
