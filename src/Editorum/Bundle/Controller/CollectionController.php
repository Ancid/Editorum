<?php
/**
 * Created by PhpStorm.
 * User: vgfilippov
 * Date: 19.01.16
 * Time: 14:03
 */

namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Document\Collection;
use Editorum\Bundle\Document\CollectionSectionList;
use Editorum\Bundle\Document\Common\AbstractPublication;
use Editorum\Bundle\Document\Conference;
use Editorum\Bundle\Document\ConferenceArticle;
use Editorum\Bundle\Document\Doi;
use Editorum\Bundle\Form\Sections\CollectionSectionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Editorum\Bundle\Document\Collection as CollectionDocument;
use Editorum\Bundle\Document\Conference as ConferenceDocument;

/**
 * @Security("is_granted('ROLE_COLLECTION_GROUP')")
 */
class CollectionController extends BasicController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $user = $this->getUser();
        $qb = $this->getODM()->createQueryBuilder('AsuBundle:Collection');

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $corp = [];
            foreach ($this->getUser()->getCorporate()->getOrganization()->getCorporates()->toArray() as $co) {
                $corp[$co->getKind()][] = $co->getId();
            }
            foreach ($corp as $type => $ids) {
                $qb
                    ->addOr(
                        $qb->expr()
                            ->addAnd($qb->expr()->field('publisher.$ref')->equals($type))
                            ->addAnd($qb->expr()->field('publisher.$id')->in($ids))
                    );
            }
        } else {
            $qb->field('publisher')->references($user->getCorporate());
        }

        $collections = $qb->sort('_id', 'desc');

        if ($request->get('filter')) {
            $filter = $this->get('publication.statuses_filter');

            $filter
                ->prepareData($request->get('filter'))
                ->makeFilter($collections);
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $collections,
            $request->query->getInt('page', 1),
            30
        );

        return $this->render('AsuBundle:Default/collection:collections.html.twig', [
            'pagination'  => $pagination,
            'kinds'       => Conference::getKinds(),
            'st_published' => Conference::ST_IS_PUBLISHED,
        ]);
    }


    /**
     * @param CollectionDocument $collection
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Collection $collection)
    {
        return $this->render('AsuBundle:Default/collection:collection_show.html.twig', [
            'collection' => $collection,
        ]);
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param ConferenceDocument $conference
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function createAction(ConferenceDocument $conference, Request $request)
    {
        $collection = new Collection();
        $statuses = [
            'status'      => ConferenceDocument::ST_IN_PROGRESS,
            'is_show_nauka' => 0,
            'is_published'  => 0,
            'is_opened'     => 0,
            'in_rinc'     => 0,
            'in_vak'      => 0,
        ];
        $collection
            ->setConference($conference)
            ->setPublisher($conference->getPublisher())
            ->setStatuses($statuses);

        //Наследуем все классификаторы конференции
        $this->getODM()->getRepository('AsuBundle:Common\AbstractPublication')
            ->inheritClassifs($conference, $collection);

        $form = $this->createForm($this->get('asu.form.collection.type'), $collection)->add('save', 'submit', [
            'label' => 'Сохранить',
            'attr' => ['class' => 'btn btn-success']
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($request->files->get($form->getName()) !== null) {
                $filename = $this->get('upload_helper')->upload('cover', $form->getName());

                if ($filename !== null && !is_array($filename)) {
                    $collection->setCover($filename);
                }
            }
            $this->getODM()->persist($collection);
            $this->getODM()->flush();
            $this->addFlash('notice', 'Сборник создан!');

            return $this->redirectToRoute('conference_collections', ['id' => $conference->getId()]);
        }

        return $this->render('AsuBundle:Default/collection:collection_create.html.twig', [
            'form' => $form->createView(),
            'collection' => $collection,
        ]);
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param CollectionDocument $collection
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Collection $collection, Request $request)
    {
        $form = $this->createForm($this->get('asu.form.collection.type'), $collection)->add('save', 'submit', [
            'label' => 'Сохранить',
            'attr' => ['class' => 'btn btn-success']
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($request->files->get($form->getName()) !== null) {
                $filename = $this->get('upload_helper')->upload('cover', $form->getName());

                if ($filename !== null && !is_array($filename)) {
                    $collection->setCover($filename);
                }
            }

            $this->getODM()->persist($collection);
            $this->getODM()->flush();
            return $this->redirectToRoute('collection_sections', ['id' => $collection->getId()]);
        }

        return $this->render('AsuBundle:Default/collection:collection_edit.html.twig', [
            'sidemenu'     => $this->get('navigation.menu')->get('Side')->getMenu('collection', [$collection]),
            'form'         => $form->createView(),
            'collection'   => $collection,
            'st_published' => ConferenceDocument::ST_IS_PUBLISHED,
        ]);

    }


    /**
     * @param CollectionDocument $collection
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ODM\MongoDB\LockException
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function composeAction(CollectionDocument $collection)
    {
        $available = $this->getODM()->getRepository('AsuBundle:Conference')
            ->getAvailableArticles($collection->getConference());
        $collection_section_list = $this->getODM()->getRepository('AsuBundle:Collection')
            ->getOrderSectionList($collection);

        return $this->render(
            'AsuBundle:Default/collection:collection_compose.html.twig',
            [
                'sidemenu' => $this->get('navigation.menu')->get('Side')->getMenu('collection', [$collection]),
                'collection'              => $collection,
                'available'               => $available,
                'collection_section_list' => $collection_section_list,
            ]
        );
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function saveComposeAction(Request $request)
    {
        $data = json_decode($request->get('data'), true);
        $collection = $this->getODM()->getRepository('AsuBundle:Collection')->find($data['_id']);

        foreach ((array)$data['articles'] as $section_id => $articles) {
            //Проходим по каждой рубрике
            foreach ($articles as $article_id) {
                $article = $this->getODM()->getRepository('AsuBundle:ConferenceArticle')->find($article_id);
                $article->setCollection($collection);
                //Если у статьи есть рубрика
                if (null != $article->getSection()) {
                    $this->getODM()->getRepository('AsuBundle:Collection')
                        ->addSection($collection, $article->getSection());
                } else {
                    //добавляем рубрику статье
                    $new_section = $this->getODM()->getRepository('AsuBundle:Section')->find($section_id);
                    $article->setSection($new_section);
                }
            }
        }

        //Отвязываем статьи от номера
        foreach ((array)$data['article_heap'] as $key => $id) {
            $article = $this->getODM()->getRepository('AsuBundle:ConferenceArticle')->find($id);
            $article->setCollection(null);
        }

        $this->getODM()->flush();

        return new JsonResponse(json_encode(['id' => $collection->getId()]));
    }


    /**
     * @Security("is_granted('ROLE_DELETE')")
     * @param CollectionDocument $collection
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction(Collection $collection, Request $request)
    {
        if ($collection->getStatuses()['status'] != Conference::ST_IS_PUBLISHED) {
            $this->getODM()->remove($collection);
            $this->getODM()->flush();
            $this->addFlash('notice', 'Сборник успешно удален!');
        } else {
            $this->addFlash('notice', 'Нельзя удалить опубликованную статью!');
        }

        return $this->redirect($request->headers->get('referer'));
    }

    
    /**
     * @param CollectionDocument $collection
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function sectionsAction(CollectionDocument $collection)
    {
        $qb = $this->getODM()->getRepository('AsuBundle:CollectionSectionList')->createQueryBuilder();
        $sectionlist = $qb->field('collection')->references($collection)->sort('order', 'asc')->getQuery()->execute();

        return $this->render('AsuBundle:Default/collection:collection_sections.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('collection', [$collection]),
            'collection' => $collection,
            'sectionlist'   => $sectionlist,
        ]);
    }


    /**
     * Добавление рубрики в номер
     * @param CollectionDocument $collection
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function sectionNewAction(CollectionDocument $collection, Request $request)
    {
        $form = $this->createForm(new CollectionSectionType(), $collection, [
            'new'   => true
        ]);

        $response = $this->render('AsuBundle:Default/section:section_add.html.twig', array(
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('collection', [$collection]),
            'form'      => $form->createView()
        ));

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if (!$form->isValid()) {
                foreach ($form->getErrors() as $error) {
                    $this->addFlash('error', $error->getMessage());
                }

                return $response;
            }

            $data = $request->get((new CollectionSectionType())->getName());

            $section = $this->getODM()->getRepository('AsuBundle:Section')->find($data['collection_section_list']);

            $collectionSectionList = $this->getODM()->getRepository('AsuBundle:Collection')
                ->addSection($collection, $section);

            if (isset($data['order'])) {
                $collectionSectionList->setOrder((int)$data['order']);
                $this->getODM()->flush($collectionSectionList);
            }

            $this->addFlash('notice', 'Секция добавлена!');

            $response = $this->redirectToRoute('collection_sections', [ 'id' => $collection->getId() ]);
        }

        return $response;
    }


    /**
     * @param CollectionSectionList $collectionSectionList
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editSectionAction(CollectionSectionList $collectionSectionList, Request $request)
    {
        $data = ['order' => $collectionSectionList->getOrder()];
        $form = $this->createForm(new CollectionSectionType(), $data);
        $response = $this->render('AsuBundle:Default/section:section_add.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('collection', [
                $collectionSectionList->getCollection()
            ]),
            'new'   => false,
            'form'  => $form->createView(),
            'list'  => $collectionSectionList
        ]);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if (!$form->isValid()) {
                foreach ($form->getErrors() as $error) {
                    $this->addFlash('error', $error->getMessage());
                }

                return $response;
            }

            $collectionSectionList->setOrder($request->request->get('order'));
            $this->getODM()->flush($collectionSectionList);
            $this->addFlash('notice', 'Позиция секции изменена!');
            $response = $this->redirectToRoute('collection_sections', [
                'id' => $collectionSectionList->getCollection()->getId()
            ]);
        }

        return $response;
    }


    /**
     * @param CollectionDocument $collection
     * @param $section_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeSectionAction(Collection $collection, $section_id)
    {
        $section = $this->getODM()->getRepository('AsuBundle:Section')->find($section_id);
        $this->getODM()->getRepository('AsuBundle:Collection')->removeSection($collection, $section);
        $this->addFlash('notice', 'Секция удалена из сборника!');

        return $this->redirectToRoute('collection_sections', ['id' => $collection->getId()]);
    }


    /**
     * @param CollectionDocument $collection
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function articlesAction(Collection $collection)
    {
        $articles = $collection->getArticles();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $articles,
            $this->get('request')->query->getInt('page', 1),
            30
        );


        return $this->render('AsuBundle:Default/collection:collection_articles.html.twig', [
            'sidemenu'     => $this->get('navigation.menu')->get('Side')->getMenu('collection', [$collection]),
            'pagination'   => $pagination,
            'collection'   => $collection,
            'st_published' => Conference::ST_IS_PUBLISHED,
        ]);
    }


    /**
     * @param CollectionDocument $collection
     * @param $article_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function articleRemoveAction(Collection $collection, $article_id)
    {
        $article = $this->getODM()->getRepository('AsuBundle:ConferenceArticle')->find($article_id);
        $article->setCollection(null);
        $this->getODM()->flush();
        $this->addFlash('notice', 'Статья удалена из сборника!');

        return $this->redirectToRoute('collection_articles', ['id' => $collection->getId() ], 301);
    }


    /**
     * @param CollectionDocument $collection
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function rincAction(CollectionDocument $collection)
    {
        return $collection->createRincArchieve();
    }


    /**
     * @param CollectionDocument $collection
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function classifsAction(Collection $collection)
    {
        $classifs = [
            'udk' => $this->getODM()->getRepository('AsuBundle:ClassificatorUdk')->findAll()[0],
            'grnti' => $this->getODM()->getRepository('AsuBundle:ClassificatorGrnti')->findAll()[0],
            'okso' => $this->getODM()->getRepository('AsuBundle:ClassificatorOkso')->findAll()[0],
            'bbk' => $this->getODM()->getRepository('AsuBundle:ClassificatorBbk')->findAll()[0],
            'tbk' => $this->getODM()->getRepository('AsuBundle:ClassificatorTbk')->findAll()[0],
        ];

        return $this->render('AsuBundle:Default/templates:classificators.html.twig', [
            'sidemenu' => $this->get('navigation.menu')->get('Side')->getMenu('collection', [$collection]),
            'entity'  => $collection,
            'classifs' => $classifs,
        ]);
    }
}
