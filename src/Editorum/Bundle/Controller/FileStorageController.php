<?php
namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Document\FileStorage;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FileStorageController extends BasicController
{
    public function uploadAction(Request $request)
    {
        $funcNum = $request->get('CKEditorFuncNum');
        $CKEditor = $request->get('CKEditor');
        $langCode = $request->get('langCode');

        $fileStorage = new FileStorage();
        $fileStorage->setDocument($this->getUser());
        $this->getODM()->persist($fileStorage);

        /** @var UploadedFile $uploadable */
        $uploadable = $request->files->get('upload');

        $this->get('stof_doctrine_extensions.uploadable.manager')
            ->markEntityToUpload($fileStorage, $uploadable);

        $fileStorage->setOriginalName($uploadable->getClientOriginalName());

        $this->getODM()->persist($fileStorage);
        $this->getODM()->flush();

        $url = $this->get('assets.packages')->getUrl($fileStorage->getPath());
        $message = 'Файл успешно загружен!';

        return new Response(
            '<script type=\'text/javascript\'>window.parent.CKEDITOR.tools.callFunction('
            .$funcNum.', \''
            .$url.'\', \''
            .$message.'\');</script>'
        );
    }

    public function browseAction(Request $request)
    {
        $fileItems = $this->getODM()->getRepository('AsuBundle:FileStorage')
            ->getFilesByDocument($this->getUser(), ['_id' => 'desc']);

        if ($fileItems === null) {
            return new Response('У вас нет загруженных файлов');
        }

        $files = [];
        foreach ($fileItems as $fileItem) {
            if (!preg_match('/\.(jpg|jpeg|png|gif)$/i', $fileItem->getPath())) {
                continue;
            }

            $thumbSrc = $this->get('assets.packages')->getUrl($fileItem->getPath());

            $width = 150;
            $height = 150;

            $files[] = [ 'src' => str_replace("'", "\\'", $thumbSrc), 'id' => $fileItem->getId() ];
        }
        return $this->render('@Asu/Default/ckeditor_browse.html.twig', [ 'images' => $files ]);
    }

    public function removeAction($id)
    {
        $this->get('file_storage.helper')->remove($id);
        
        return $this->redirectToReferer();
    }
}
