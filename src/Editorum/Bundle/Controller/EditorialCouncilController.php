<?php
namespace Editorum\Bundle\Controller;

use Doctrine\Common\Util\Inflector;
use Editorum\Bundle\Document\EditorialCouncil;
use Editorum\Bundle\Document\Journal;
use Editorum\Bundle\Form\EditorialCouncilType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Class EditorialTeamController
 * @package Editorum\Bundle\Controller
 *
 * @Security("is_granted('ROLE_JOURNAL_GROUP')")
 */
class EditorialCouncilController extends BasicController
{
    /**
     * @param Journal $journal
     * @param Request $request
     * @return Response
     */
    public function listAction(Journal $journal, Request $request)
    {
        $form = $this->createForm(new EditorialCouncilType())
            ->add('save', 'submit', [
                'label' => 'Создать',
                'attr' => ['class' => 'btn btn-success']
            ]);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {
                /** @var EditorialCouncil $council */
                $council = $form->getData();
                $council->setJournal($journal);
                $this->getODM()->persist($council);
                $this->getODM()->flush();
                $this->addFlash('notice', 'Член редсовета добавлен!');

                //Пересоздаем форму чтоб не заполнялась при успешном сабмите
                $form = $this->createForm(new EditorialCouncilType())
                    ->add('save', 'submit', [
                        'label' => 'Сохранить',
                        'attr' => ['class' => 'btn btn-success']
                    ]);
            } else {
                $errors = $this->get('validator')->validate($form);
                foreach ($errors as $e) {
                    $this->addFlash('error', $e->getMessage());
                }
            }
        }

        /** @var EditorialCouncil[] $team */
        $team = $this->getODM()->getRepository('AsuBundle:EditorialCouncil')
            ->getBySortableGroupsQueryBuilder([ 'journal' => $journal->getId() ]);

        $paginator = $this->get('knp_paginator')->paginate(
            $team,
            $request->get('page', 1),
            30
        );

        return $this->render('AsuBundle:Default/Council/Journal:council_list.html.twig', [
            'sidemenu' => $this->get('navigation.menu')->get('Side')->getMenu('journal', [$journal]),
            'form' => $form->createView(),
            'entity' => $journal,
            'team' => $paginator,
        ]);
    }


    /**
     * @param $id
     * @return Response
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function showAction($id)
    {
        $odm = $this->get('doctrine.odm.mongodb.document_manager');

        return $this->render('AsuBundle:Default/Council:show.html.twig', [
            'member' => $odm->getRepository('AsuBundle:EditorialCouncil')->find($id)
        ]);
    }


    /**
     * @param $string
     * @param $fieldName
     * @param Request $request
     * @return JsonResponse
     */
    public function getValueByFieldAction($string, $fieldName, Request $request)
    {
        $odm = $this->get('doctrine.odm.mongodb.document_manager');

        $qb = $odm->createQueryBuilder('AsuBundle:EditorialCouncil');

        $meta = $odm->getRepository('AsuBundle:EditorialCouncil')->getClassMetadata();

        $editorialTeam = $qb
            ->find()
            ->field($fieldName)
            ->equals(new \MongoRegex('/^'.$string.'/i'))
            ->distinct('_id')
        ;

        if ($request->get('journalId') !== null) {
            $editorialTeam->field('journal_id')->notEqual((int)$request->get('journalId'));
        }

        $editorialTeam = $editorialTeam->getQuery();

        $array = $editorialTeam->getIterator()->toArray();

        $result = $qb->find()->field('_id')->in($array)->getQuery()->execute();

        $properties = $meta->reflClass->getProperties();

        $methodsArray = [];
        foreach ($properties as $property) {
            $name = $property->getName();
            $method = 'get' . Inflector::classify($name);

            $methodsArray[$name] = $method;
        }

        $resultArray = [];
        $i = 0;
        foreach ($result as $item) {
            foreach ($methodsArray as $key => $method) {
                $temporary = call_user_func([$item, $method]);
                $reference = false;
                if (in_array($key, $meta->getAssociationNames()) && null !== $temporary) {
                    $resultArray[$i][$key] = call_user_func([$temporary, 'getId']);
                    $reference = true;
                }

                if (!$reference) {
                    $resultArray[$i][$key.'_ru'] = $temporary;
                }
            }

            $translations = $this->getTranslations($item);

            foreach ($translations as $lang => $translation) {
                foreach ($translation as $key => $value) {
                    $resultArray[$i][$key.'_'.$lang] = $value;
                }
            }

            ++$i;
        }

        return new JsonResponse($resultArray);
    }

    /**
     * @Security("is_granted('ROLE_DELETE')")
     * @param $id
     * @param $journal_id
     * @return Response
     */
    public function removeAction($id, $journal_id)
    {
        $odm = $this->get('doctrine.odm.mongodb.document_manager');
        $odm->createQueryBuilder('AsuBundle:EditorialCouncil')
            ->findAndRemove()
            ->field('_id')->equals($id)
            ->getQuery()
            ->execute()
        ;

        return new Response($this->get('router')->generate('editorial_council_list', [
            'id' => $journal_id
        ]));
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param EditorialCouncil $editorialCouncil
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editAction(EditorialCouncil $editorialCouncil, Request $request)
    {
        $form = $this->createForm(new EditorialCouncilType(), $editorialCouncil)
            ->add('save', 'submit', [
                'label' => 'Сохранить',
                'attr' => ['class' => 'btn btn-primary']
            ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getODM()->flush();
            $this->addFlash('notice', 'Сохранено!');

            return $this->redirectToRoute('editorial_council_list', ['id' => $editorialCouncil->getJournal()->getId()]);
        }

        return $this->render('AsuBundle:Default/Council/Journal:edit.html.twig', [
            'form' => $form->createView(),
            'member' => $editorialCouncil
        ]);
    }

    /**
     * @return \Doctrine\ODM\MongoDB\DocumentManager
     */
    public function getODM()
    {
        return $this->get('doctrine.odm.mongodb.document_manager');
    }
}
