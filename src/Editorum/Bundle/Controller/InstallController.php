<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 23.08.2015
 * Time: 19:50
 */

namespace Editorum\Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Editorum\Bundle\Logic\Organization;
use Editorum\Bundle\Logic\Model;
use Editorum\Bundle\AccessSystem\User;

class InstallController extends Controller
{

    /**
     *  Первичное развертывание системы
     */
    public function indexAction()
    {
        /*
         * Название файла И ПУТЬ, который отображает признак того, что проект уже установлен
         */
        $flag_file = 'installed.dat';

        /**
         * Не допускаем повторной инсталляции проекта
         */
        if (file_exists($flag_file)) {
            die();
        }

        /**
         * Устанавливаем флаг
         */
        file_put_contents($flag_file, '*');


        /**
         * Создаем системную организацию Editorum
         */
        $editorum_id = Model::insert(new Organization(), array(
            'title'             => 'Editorum',
            'full_title'        => 'Editorum System Organization',

            'director_fio'      => '',  // ФИО директора
            'director_fio_r'    => '',  // ФИО директора в родительном падеже

            'feedback_email'    => '',
            'depositor'         => '',
            'registrant'        => '',
            'doi_prefix'        => '',
        ));

        /**
         * Создаем пользователя администратора Editorum
         */
        $administrator_id = Model::insert(new User(), array(
                'first_name'                            => 'Администратор',
                'last_name'                             => 'Эдиторума',
                'second_name'                           => 'Главный',
                'login'                                 => 'admin_editorum',
                'password'                              => '324fdgr435',
                'email'                                 => 'xor@bmax.ru',
                'phone'                                 => '',
                'code'                                  => '12893h8vxx2193yregfdba8gd7810837',
                'active'                                => true,
        ));

        /**
         * Берем на работу в Editorum пользователя администратор
         */


        /** Этот код явно никогда не работал
        $db = new MongoClient();
        $db->system->js->save(array(
            '_id'       => 'getNextSequence',
            'value'     => new MongoCode('function getNextSequence(name) {
                var ret = db.counters.findAndModify(
                  {
                    query: { _id: name },
                    update: { $inc: { seq: 1 } },
                    new: true
                  }
                );

                return ret.seq;
            }')
        ));
         */
        return $this->render('AsuBundle:Default:install.html.twig');
    }
}
