<?php
/**
 * Created by PhpStorm.
 * User: Ancid
 * Date: 23.08.2015
 * Time: 13:06
 */

namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Document\Organization;
use Editorum\Bundle\Form\Organization\DoiType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class ManageController
 * @package Editorum\Bundle\Controller
 *
 * Security("is_granted('ROLE_SUPER_ADMIN')")
 */
class ManageController extends BasicController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function doiAction()
    {
        $organizations = $this->getODM()->getRepository('AsuBundle:Organization')->findAll();

        return $this->render('@Asu/Default/manage/doi.html.twig', [
            'organizations'  => $organizations,
        ]);
    }


    /**
     * @param Organization $organization
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function doiEditAction(Organization $organization, Request $request)
    {
        $form = $this->createForm(new DoiType(), $organization)->add('save', 'submit', [
            'label' => 'Сохранить',
            'attr' => ['class' => 'btn btn-success pull-right']
        ]);

        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->getODM()->flush();

                return $this->redirectToRoute('doi_edit', ['id' => $organization->getId()], 301);
            }
        }

        return $this->render('@Asu/Default/manage/doi_edit.html.twig', [
            'organization'  => $organization,
            'form'  => $form->createView()
        ]);
    }
}
