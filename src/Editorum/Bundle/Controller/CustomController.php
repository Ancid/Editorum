<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 07.10.2015
 * Time: 16:29
 */
namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Document\Collections;
use Editorum\Bundle\Logic\Issue;
use Editorum\Bundle\Logic\MongoDB;
use Editorum\Bundle\Logic\Conference;
use Editorum\Bundle\Logic\Organization;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use MongoClient;
use PDO;
use Editorum\Bundle\Logic\Publication;
use Editorum\Bundle\Logic\References;
use Editorum\Bundle\Logic\Model;
use MongoDate;

ini_set('memory_limit', '2048M');
set_time_limit(0);

class CustomController extends Controller
{
    protected $db;

    /**
     * CustomController constructor.
     * @throws \Exception
     * @internal param $db
     */
    public function __construct()
    {
        $this->db = MongoDB::getDB();
        //TODO: change get db
    }

    public function defaultAccessAction()
    {
        /**
         * Создаем системную организацию Editorum
         */        
        $editorum = Model::getByKeys(new Organization(), ['is_default' => true]);
        if ($editorum = $editorum->current()->getCurrentRecord()) {
            $editorum_id = $editorum['_id'];
        } else {
            $editorum_id = Model::insert(new Organization(), [
                'name' => 'Editorum',
                'fullname' => 'Editorum System Organization',
                'director_fio' => 'Сафонов Михаил Николаевич',
                'director_fio_r' => 'Сафонова Михаила Николаевича',
                'feedback_email' => 'director@editorum.ru',
                'is_default' => true,
            ]);
        }
        if (!$editorum_id) {
            echo 'Организация "по умолчанию" отсутствует.';
            die();
        }
        echo 'Организация "по умолчанию" зарегистрирована.';
        echo '<br />';

        /**
         * Создаем системного администратора Editorum
         */
        /* @var \UserBundle\Document\User $admin */
        $admin = null;
        if ($admin = $this->get('fos_user.user_manager')->findUserBy(['email' => 'super.admin@editorum.ru'])) {
            echo 'Пользователь "по умолчанию" уже зарегистрирован в системе.';
        } else {
            $admin = $this->get('fos_user.user_manager')->createUser();
            $admin->setUsername('Администратор Едиторума');
            $admin->setEmail('super.admin@editorum.ru');
            $admin->setPlainPassword('_super3admin2editorum1ru_');
            $admin->setEnabled(true);
            $this->get('fos_user.user_manager')->updateUser($admin);
            echo 'Пользователь "по умолчанию" зарегистрирован.';
            echo '<br />';
        }
        
        if ($editorum_id) {
            /** @var \Editorum\Bundle\AgreementSystem\ASys $ASys */
            $ASys = $this->get('app.asys');
            
            /** Registered EmployeeAgreement */
            /* @var \Editorum\Bundle\AgreementSystem\Agreements\EmployeeAgreement $employee_agreement */
            $employee_agreement = $ASys->getAgreementObject('EmployeeAgreement');
            $employee_agreement->create($editorum_id, $admin->getId());
            if($ASys->saveAgreement($employee_agreement))
                $ASys->executeAgreement($employee_agreement);
            
            if ($employee_agreement->isActive()) {
                echo 'Договор найма зарегистрирован.';
                echo '<br />';
            } else {
                echo 'Договор найма был зарегистрирован ранее.';
                echo '<br />';
            }
            
            /** Registered AdministratorAgreement */
            /* @var \Editorum\Bundle\AgreementSystem\Agreements\AdministratorAgreement $admin_agreement */
            $admin_agreement = $ASys->getAgreementObject('AdministratorAgreement');
            $admin_agreement->create($editorum_id, $admin->getId());
            if($ASys->saveAgreement($admin_agreement))
                $ASys->executeAgreement($admin_agreement);

            if ($admin_agreement->isActive()) {
                echo 'Договор администрирования зарегистрирован.';
                echo '<br />';
            } else {
                echo 'Договор администрирования был зарегистрирован ранее.';
                echo '<br />';
            }
        }
        die();
    }
    
    public function rubricatorsAction()
    {
        $mc = new MongoClient();
        $db = $mc->editorum;
        $collection = $db->createCollection('classif');
        $collection->drop();
        var_dump("Collection \"classif\" created succsessfully");

        $names = [
            3001 => 'okso_full',
            3002 => 'grnti',
            3003 => 'tbk',
            3004 => 'udk',
            3007 => 'bbk',
            3010 => 'okso_journal',
        ];

        $map_names = [
            'okso_journal' => 'ОКСО',
            'okso_full'    => 'ОКСО',
            'grnti'        => 'ГРНТИ',
            'tbk'          => 'ТБК',
            'udk'          => 'УДК',
            'bbk'          => 'ББК',
        ];

        $classifs = [];
        $count = 1;
        foreach ($names as $rubricator => $name) {
            if (file_exists('rubricator/'.$name.'.json')) {
                $object = json_decode(file_get_contents('rubricator/'.$name.'.json'), true);
                $object['name'] = $map_names[$name];
                $object['type'] = $name;
                $object['_id'] = $count;
                $classifs[] = $object;
                $count++;
            }
        }

        $collection->batchInsert($classifs);
        var_dump('Success!');
    }


    public function pdfAction()
    {

        set_time_limit(600);
        ini_set("memory_limit", "512M");

        $mpdfService = $this->get('tfox.mpdfport');
        $mPDF = $mpdfService->getMpdf();

        $stylesheet = file_get_contents('bundles/asu/lib/bootstrap-3.3.5-dist/css/bootstrap.min.css');
        $html = '<div class="container">
                <div class="row">
                <table class="table table-striped" ><tr><td>Пример 1</td><td>Пример 2</td><td>Пример 3</td><td>Пример 4</td></tr>
                <tr><td>Пример 5</td><td>Пример 6</td><td>Пример 7</td><td><a href="http://mpdf.bpm1.com/" title="mPDF">mPDF</a></td></tr></table>
                </div>
                </div>';
        $mPDF->Image('bundles/journals/images/journal.jpg', 0, 0, 210, 297, 'jpg', '', true, false);
        $mPDF->WriteHTML($stylesheet, 1);
        $mPDF->WriteHTML($html, 2);

        //        $mPDF->Output('temp/123.pdf', 'F'); //Сохранить в файл
        $content = $mPDF->Output('temp/123.pdf', 'S');

        $response = new Response();
        $response->headers->set('Content-Type', 'application/pdf');

        $response->setContent($content);


        return $response;

        //        $response =  $mpdfService->generatePdfResponse($html, $options);
        //
        //        return $response;

    }


    public function makejsonAction()
    {
        if (!is_dir('jsons_asu')) {
            mkdir('jsons_asu');
        }


        $pdo = $this->getPdo();
        $stmt = $pdo->query('
SELECT DISTINCT 
a.id, a.first_name, a.last_name, a.second_name,
a.adder_id,
a.snils,
a.inn,
a.pass_seria,
a.pass_number,
a.pass_dt,
a.pass_code,
a.pass_given,
a.reg_adress,
a.fact_adress,
a.bank_reciver,
a.bank_name,
a.bank_account,
a.bank_coaccount,
a.bank_bik,
a.birth,
a.academic_title,
a.eng_academic_title,
a.degree,
a.eng_degree,
a.department,
a.eng_department,
a.univercity_id as university_id,
a.is_reviewer,
a.region_id,
a.city,
a.position,
a.eng_position,
a.country_id,
a.reg_city,
a.reg_country_id,
a.reg_region_id,
a.reg_index,
a.fact_index,
a.adv_title,
a.eng_adv_title,
a.scopus,
a.researcherid,
a.eng_first_name,
a.eng_last_name,
a.rinc,
a.eng_univercity,
a.work_title,
a.work_country,
a.work_city,
a.eng_work_title,
a.eng_work_city,
a.work_type,
a.date_end,
a.you_work,
CONCAT(a.last_name, " ",a.first_name, " ", a.second_name) as fio,
a.email, a.orcid_id, a.cell_phone as phone
FROM NAuthor a
inner join ndblinks l on l.src_id = a.id
left join npub p on p.id = l.dst_id
WHERE LENGTH(CONCAT(a.last_name, " ",a.first_name, " ", a.second_name)) > 5
and l.src_class = "NAuthor" and l.dst_class = "NPub"
');
        $authors = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $stmt = $pdo->query('
SELECT s.id, s.title, s.title_eng as eng_title, s.about, s.about_eng as eng_about
FROM NSeria s
');
        $series = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Журналы
        $stmt = $pdo->query('
SELECT *
FROM NJournal j
        ');
        $journals = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Номера
        $stmt = $pdo->query('
SELECT i.id, i.volume, i.issue, i.st_doi as doi, i.journal_id,is_published,
i.dt_created,
i.dt_published,
i.is_finalized,
i.status,
i.st_rinc,
i.rinc_xml,
i.pagecount,
i.rinc_id,
i.part,
i.rinc_,
i.type_access

FROM NIssue i
'
        );
        $issues = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $stmt = $pdo->query('
SELECT p.id, p.issue_id
FROM NPub p
WHERE p.kind = 100
AND p.issue_id > 0'
        );
        $issue_articles = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $stmt = $pdo->query('
SELECT o.object_id as pub_id, o.code, r.title, r.sysname, o.object_class
FROM NObjRubrics o
JOIN NDictRubric r ON r.code = o.code
'
        );
        $pub_rubrics = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Conference_articles
        $stmt = $pdo->prepare('
SELECT p.id, p.worktitle as title, p.annotation, p.keywords, p.eng_worktitle as eng_title, p.eng_keywords, p.eng_keywords,
p.eng_annotation, p.st_doi, p.is_published, p.has_reviewers, p.has_published, p.has_agreement, p.show_nauka,
p.dt_create as date_create, p.dt_agree as date_agree, p.dt_published as dt_publication_print, p.doi, p.author_pages,
p.url as uri, p.grnti, p.udk, p.published_year, p.published_page_from as first_page, p.published_page_to as last_page,
p.published_page_total, p.published_url, p.lang, p.text, p.parent_order, p.publisher_id, p.parent_id as collection_id,
c.id as conference_id, (
	SELECT GROUP_CONCAT(a.id ORDER BY a.id)
	FROM nauthor a
	LEFT JOIN ndblinks AS link ON link.src_id = a.id
	WHERE link.src_class = \'NAuthor\'
	AND link.dst_class = \'NPub\'
	AND link.dst_id = p.id
	GROUP BY link.dst_id) AS authors
FROM NPub p
LEFT JOIN nconference c ON c.publication_id = p.parent_id
WHERE p.kind = :kind AND p.parent_id > 0
');

        $stmt->execute([':kind' => 100]);
        $conference_articles = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $stmt = $pdo->prepare('SELECT * from (SELECT
									p.id,
									p.user_id,
									p.has_images_owned,
									p.depofile,
									p.comment,
									p.has_reviewers,
									p.has_published,
									p.has_agreement,
									p.issue_id,
									p.issue_rubric_id,
									p.issue_iorder,
									p.is_finalized,
									p.max_dt,
									p.author_pages,
									p.grnti,
									p.show_znanium,
									p.publisher_id,
									p.st_agreement_printed,
									p.published_journal,
									p.published_year,
									p.published_issue,
									p.published_url,
									p.edu_type,
									p.grif,
									p.show_nauka,
									p.isbn_online,
									p.parent_id,
									p.parent_order,
									p.published_page_total,
									p.udk,
									p.rinc_url,
									p.worktitle as title,
									p.eng_worktitle as eng_title,
									p.lang, p.isbn,
		        					p.dt_published as dt_publication_print,
									p.published_page_from as first_page,
									p.published_page_to as last_page,
									p.doi,
									p.st_doi,
									p.url as uri,
									p.is_published,
        							p.keywords,
									p.eng_keywords,
									p.annotation,
									p.eng_annotation,
									p.dt_create as date_create,
									p.dt_agree as date_agree,
									p.text,
        							p.journal,
        							p.status, p.publication_number, p.has_grif,
									p.kind,
									GROUP_CONCAT(au.id) as authors from NPub as p
									left join NDblinks as link on link.dst_id = p.id
									left join NAuthor as au on au.id = link.src_id
									where p.kind = :kind and link.dst_class = "NPub" GROUP BY p.id
											UNION All SELECT p.id,
											p.user_id,
											p.has_images_owned,
											p.depofile,
											p.comment,
											p.has_reviewers,
											p.has_published,
											p.has_agreement,
											p.issue_id,
											p.issue_rubric_id,
											p.issue_iorder,
											p.is_finalized,
											p.max_dt,
											p.author_pages,
											p.grnti,
											p.show_znanium,
											p.publisher_id,
											p.st_agreement_printed,
											p.published_journal,
											p.published_year,
											p.published_issue,
											p.published_url,
											p.edu_type,
											p.grif,
											p.show_nauka,
											p.isbn_online,
											p.parent_id,
											p.parent_order,
											p.published_page_total,
											p.udk,
											p.rinc_url,
											p.worktitle as title,
											p.eng_worktitle as eng_title,
											p.lang, p.isbn,
		        							p.dt_published as dt_publication_print,
											p.published_page_from as first_page, p.published_page_to as last_page, p.doi, p.st_doi,p.url as uri, p.is_published,
        									p.keywords, p.eng_keywords, p.annotation, p.eng_annotation,p.dt_create as date_create,p.dt_agree as date_agree,p.text,
        									p.journal, p.status, p.publication_number, p.has_grif, p.kind,
											null as authors
											from NPub as p where p.kind = :kind
											group by p.id) as t
											group by id'
        );
        $stmt->execute([':kind' => 100]);
        $articles = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $stmt->execute([':kind' => 102]);
        $monography = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        $stmt->execute([':kind' => 101]);
        $textbooks = $stmt->fetchAll(PDO::FETCH_ASSOC);

//        $stmt->execute([':kind' => 100]);
//        $articles = $stmt->fetchAll(PDO::FETCH_ASSOC);

        /*Конференции*/
        $stmt = $pdo->query(
            'SELECT * FROM NConference'
        );
        $conference = $stmt->fetchAll(PDO::FETCH_ASSOC);

        /*References*/
        $stmt = $pdo->query('
SELECT r.*, p.kind
FROM NReferences r
LEFT JOIN npub p ON p.id = r.pub_id
ORDER BY id
');

        $references = $stmt->fetchAll(PDO::FETCH_ASSOC);

        /*chapters*/
        $stmt = $pdo->query('
SELECT 
c.*, p.kind
from NPubChapter c
LEFT JOIN npub p ON p.id = c.pub_id 
');

        $chapters = $stmt->fetchAll(PDO::FETCH_ASSOC);

        /*Rubrics*/

        $stmt = $pdo->query('
SELECT R.*, GROUP_CONCAT(DISTINCT(CONCAT_WS("-", IR.issue_id, IR.iorder)) SEPARATOR",") as issue_ids,
GROUP_CONCAT(DISTINCT(P.id) ORDER BY P.id SEPARATOR\',\') as pubs_ids
from NJournalRubric  R
left join NIssueRubrics  IR on R.id = IR.journal_rubric_id
left join NPub P on IR.id = P.issue_rubric_id
GROUP BY R.id
');
        $rubrics = $stmt->fetchAll(PDO::FETCH_ASSOC);

        /*publisher*/

        $stmt = $pdo->query(
            'select * from NPublishers'
        );
        $publishers = $stmt->fetchAll(PDO::FETCH_ASSOC);

        /*Ncounries - Страны*/

        $stmt = $pdo->query(
            'Select * from NCountries'
        );

        $countries = $stmt->fetchAll(PDO::FETCH_ASSOC);

        /*Nregions - области,регионы*/

        $stmt = $pdo->query(
            'Select * from NRegions'
        );

        $regions = $stmt->fetchAll(PDO::FETCH_ASSOC);

        /*NDictVuz - Видимо вузы*/

        $stmt = $pdo->query(
            'Select * from NDictVuz'
        );

        $universities = $stmt->fetchAll(PDO::FETCH_ASSOC);

        /*Сборники*/

        $stmt = $pdo->query('
SELECT
    GROUP_CONCAT(p.id) as articles,
    c.id,
    c.user_id,
    c.is_finalized,
    c.max_dt,
    c.author_pages,
    c.publisher_id,
    c.isbn_online,
    c.published_page_total,
    c.udk,
    c.worktitle as title,
    c.eng_worktitle as eng_title,
    c.isbn,
    c.dt_published,
    c.doi,
    c.url as uri,
    c.is_published,
    c.keywords,
    c.eng_keywords,
    c.annotation,
    c.eng_annotation,
    c.dt_create,
    c.dt_agree,
    c.show_nauka as is_show_nauka,
    co.id as conference_id
    from NPub c
    left join NPub as p on c.id = p.parent_id
    LEFT JOIN nconference AS co ON co.publication_id = c.id
    where c.kind = 105
    group by c.id
    ');

        $collections = $stmt->fetchAll(PDO::FETCH_ASSOC);

        /*chapters*/
        $stmt = $pdo->query('
SELECT *
from NJournalEditorial
        ');

        $editorial = $stmt->fetchAll(PDO::FETCH_ASSOC);

        file_put_contents('jsons_asu/authors.json', json_encode($authors));
        file_put_contents('jsons_asu/series.json', json_encode($series));
        file_put_contents('jsons_asu/journals.json', json_encode($journals));
        file_put_contents('jsons_asu/issues.json', json_encode($issues));
        file_put_contents('jsons_asu/issue_articles.json', json_encode($issue_articles));
        file_put_contents('jsons_asu/pub_rubrics.json', json_encode($pub_rubrics));
        file_put_contents('jsons_asu/articles.json', json_encode($articles));
        file_put_contents('jsons_asu/conference_articles.json', json_encode($conference_articles));
        file_put_contents('jsons_asu/monography.json', json_encode($monography));
        file_put_contents('jsons_asu/textbooks.json', json_encode($textbooks));
//        file_put_contents('jsons_asu/pubs.json', json_encode($pubs));
        file_put_contents('jsons_asu/conferences.json', json_encode($conference));
        file_put_contents('jsons_asu/references.json', json_encode($references));
//        $chunked = array_chunk($references, 20000);
//        file_put_contents('jsons_asu/references1.json', json_encode($chunked[0]));
//        file_put_contents('jsons_asu/references2.json', json_encode($chunked[1]));
        file_put_contents('jsons_asu/chapters.json', json_encode($chapters));
        file_put_contents('jsons_asu/rubrics.json', json_encode($rubrics));
        file_put_contents('jsons_asu/publishers.json', json_encode($publishers));
        file_put_contents('jsons_asu/countries.json', json_encode($countries));
        file_put_contents('jsons_asu/regions.json', json_encode($regions));
        file_put_contents('jsons_asu/universities.json', json_encode($universities));
        file_put_contents('jsons_asu/collections.json', json_encode($collections));
        file_put_contents('jsons_asu/editorial.json', json_encode($editorial));

        return new Response('Success!');
    }

    public function getgrntiAction()
    {
        $pdo = $this->getPdo();

        $stmt = $pdo->query('
select r.id, r.parent_code ,r.code, r.title
from NDictRubric r
where r.sysname = "grnti"
'
        );

        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($items as &$item) {

            if (strlen($item['parent_code']) > 1) {
                $stmt_c = $pdo->query('
select r.id
from NDictRubric r
where r.code = '.$item["parent_code"].'
'
                );
                $cur_id = $stmt_c->fetchAll(PDO::FETCH_COLUMN);

                $id = (int)$cur_id[0];
            } else {
                $id = null;
            }
            $item['id'] = (int)$item['id'];
            $item['parent_id'] = $id;
            $item['new_code'] = $item['code'];
            unset($item['parent_code']);
            unset($item['code']);
        }

        // Сортируем массив по коду классификатора
        usort(
            $items,
            function ($b, $c) {
                if ($b['new_code'] == $c['new_code']) {
                    return 0;
                }

                return ($b['new_code'] < $c['new_code']) ? -1 : 1;
            }
        );

        $links = [];
        $tree = [];
        for ($q = 0; $q < count($items); $q++) {
            $elem = $items[$q];
            if ($elem['parent_id'] === null) {
                $tree[$elem['id']] = $elem;
                $links[$elem['id']] = &$tree[$elem['id']];
            } else {
                $links[$elem['parent_id']]['childrens'][$elem['id']] = $elem;
                $links[$elem['id']] = &$links[$elem['parent_id']]['childrens'][$elem['id']];
            }
        }

        $new_tree = [];
        $res_grnti = $this->mongoTree($tree, $new_tree);

        $result = file_put_contents('rubricator/grnti.json', json_encode($res_grnti));
        var_dump('SUCCESS!');
    }


    /**
     * @param $entity
     */
    public function importAction($entity)
    {
        echo "Start: ".date("Y-m-d H:i:s", time())."\n<br>";
        ini_set("max_execution_time", "600");

        if ($entity == 'authors') {

            $authors = json_decode(file_get_contents('jsons_asu/authors.json'), true);

            foreach ($authors as &$author) {
                $author['university_id'] = $author['univercity_id'];
                $author['university_city'] = $author['univercity_city'];
                $author['university_country_id'] = $author['univercity_country_id'];
                $author['eng_university'] = $author['eng_univercity'];
                unset($author['univercity_id']);
                unset($author['univercity_city']);
                unset($author['univercity_country_id']);
                unset($author['eng_univercity']);
            }

            $mc = new MongoClient();
            $db = $mc->editorum;
            $import_col = $db->selectCollection('import');

            //Записываем в БД
            $this->insertEntity($db, 'author', $authors, $import_col);

            var_dump($db->lastError());
            var_dump('Success!');


        } elseif ($entity == 'journals') {
            $mc = new MongoClient();
            $db = $mc->editorum;
            $pubs_collection = $db->selectCollection('publication');
            $import_col = $db->selectCollection('import');

            //Серии
            $series = json_decode(file_get_contents('jsons_asu/series.json'), true);

//            //Импорт серий
            $this->insertEntity($db, 'serie', $series, $import_col);
//
//            //Журналы
            $journals = json_decode(file_get_contents('jsons_asu/journals.json'), true);

            //Привязываем журналы к сериям
             foreach ($journals as &$journal) {

                $journal['issn_electronic'] = $journal['issn_online'];
                $journal['university_id'] = $journal['univercity_id'];
                $journal['issn_print'] = $journal['issn'];
                unset($journal['issn']);
                unset($journal['issn_online']);
                unset($journal['univercity_id']);

                if ((int)$journal['seria_id'] > 0) {
                    $cursor = $import_col->findOne(
                        ['type' => 'serie_asu'],
                        [
                            'elements' => [
                                '$elemMatch' => [
                                    'external_id' => (int)$journal['seria_id'],
                                ],
                            ],
                        ]
                    );

                    $journal['serie_id'] = (int)$cursor['elements'][0]['internal_id'];

                }
                $journal['is_show_nauka'] = (int)$journal['is_show_nauka'];
                $journal['is_in_work'] = (int)$journal['is_in_work'];
                unset($journal['seria_id']);
            }

            //Импорт журналов
            $this->insertEntity($db, 'journal', $journals, $import_col);

            //Обновляем id журнала в публикациях
            $new_journals = $db->selectCollection('journal')->find();

            foreach ($new_journals as $nj) {
                $journal_id = $import_col->findOne(
                    ['type' => 'journal_asu'],
                    [
                        'elements' => [
                            '$elemMatch' => [
                                'internal_id' => (int)$nj['_id'],
                            ],
                        ],
                    ]
                );
                $pubs_collection->update(
                    [
                        'journal_id' => (int)$journal_id['elements'][0]['external_id'],
                    ],
                    ['$set' => ['journal_id' => $journal_id['elements'][0]['internal_id']]],
                    ['upsert' => false, 'multiple' => true]
                );


                $pubs_collection->update(
                    [
                        'journal_id' => (int)$nj['_id'],
                    ],
                    ['$set' => ['journal_title' =>$nj['title']]],
                    ['upsert' => false, 'multiple' => true]
                );
            }

            //Номера
            $issues = json_decode(file_get_contents('jsons_asu/issues.json'), true);

            //Обновляем id журнала
            foreach ($issues as &$issue) {
                $cursor = $import_col->findOne(
                    ['type' => 'journal_asu'],
                    [
                        'elements' => [
                            '$elemMatch' => [
                                'external_id' => (int)$issue['journal_id'],
                            ],
                        ],
                    ]
                );

                //Если журнала нет
                if (!isset($cursor['elements'])) {
                    unset($issue);
                    continue;
                }

                $issue['journal_id'] = (int)$cursor['elements'][0]['internal_id'];
                $issue['articles'] = [];
                $issue['is_show_nauka'] = (int)$issue['is_published'];
                $issue['is_published'] = (int)$issue['is_published'];
            }

            //Импорт номеров
            $this->insertEntity($db, 'issue', $issues, $import_col);

            // Обновляем Id номера в публикации
            foreach ($issues as $iss) {
                $issue_id = $import_col->findOne(
                    ['type' => 'issue_asu'],
                    [
                        'elements' => [
                            '$elemMatch' => [
                                'external_id' => (int)$iss['id'],
                            ],
                        ],
                    ]
                );
                $pubs_collection->update(
                    [
                        'issue_id' => (int)$issue_id['elements'][0]['external_id'],
                    ],
                    ['$set' => ['issue_id' => $issue_id['elements'][0]['internal_id']]],
                    ['upsert' => false, 'multiple' => true]
                );
            }

            $issue_articles = json_decode(file_get_contents('jsons_asu/issue_articles.json'), true);

            //Связываем номера и статьи
            $prev_article_id = -1;
            foreach ($issue_articles as &$art) {

                if ((int)$art['id'] != $prev_article_id) {
                    $cursor = $import_col->findOne(
                        ['type' => 'publication_asu'],
                        [
                            'elements' => [
                                '$elemMatch' => [
                                    'external_id' => (int)$art['id'],
                                ],
                            ],
                        ]
                    );
                    if (isset($cursor['elements'])) {
                        $article_id = (int)$cursor['elements'][0]['internal_id'];
                        $prev_article_id = $article_id;
                    }

                } else {
                    $article_id = $prev_article_id;
                }

                $cursor = $import_col->findOne(
                    ['type' => 'issue_asu'],
                    [
                        'elements' => [
                            '$elemMatch' => [
                                'external_id' => (int)$art['issue_id'],
                            ],
                        ],
                    ]
                );

                if (!isset($cursor['elements'])) {
                    continue;
                }

                $issue_id = $cursor['elements'][0]['internal_id'];

                $db->issue->update(
                    ['_id' => $issue_id],
                    ['$push' => ['articles' => (int)$article_id]]
                );
            }

            //Классификаторы произведений
            $pub_rubrics = json_decode(file_get_contents('jsons_asu/pub_rubrics.json'), true);

//            $grnti = Classif::loadClassif(['grnti']);
            foreach ($pub_rubrics as &$pub_r) {
                //Ищем произведение или журнал
                if ($pub_r['object_class'] == 'NJournal') {
                    $class = 'journal';
                    $coll = $db->journal;
                } elseif ($pub_r['object_class'] == 'NPub') {
                    $class = 'publication';
                    $coll = $db->publication;
                }
                $cursor = $import_col->findOne(
                    ['type' => $class.'_asu'],
                    [
                        'elements' => [
                            '$elemMatch' => [
                                'external_id' => (int)$pub_r['pub_id'],
                            ],
                        ],
                    ]
                );


                if (!isset($cursor['elements'])) {
                    continue;
                }

                $pub_id = $cursor['elements'][0]['internal_id'];

                //Ищем классификатор
                $cursor = $db->classif->findOne(
                    ['type' => strtolower($pub_r['sysname'])],
                    [
                        'childrens' => [
                            '$elemMatch' => [
                                'new_code' => $pub_r['code']
                            ],
                        ],
                    ]
                );

                //Нашел в родителях
                if (isset($cursor['childrens'])) {
                    $classif_code = $cursor['childrens'][0]['new_code'];
                } else {
                    //Ищем в дочерних
                    $first_code = explode('.', $pub_r['code']);
                    $parent_code = $first_code[0].'.00';
                    $find_child = $db->classif->aggregate(
                        ['$match' => ['type' => strtolower($pub_r['sysname'])]],
                        ['$unwind' => '$childrens'],
                        ['$match' => ['childrens.new_code' => $parent_code]],
                        ['$unwind' => '$childrens.childrens'],
                        ['$match' => ['childrens.childrens.new_code' => $pub_r['code']]]
                    );
                    $classif_code = $find_child['result'][0]['childrens']['childrens']['new_code'];

                }

                $coll->update(
                    ['_id' => $pub_id],
                    ['$addToSet' => ['classif_'.strtolower($pub_r['sysname']) => $classif_code]]
                );
            }

            var_dump($db->lastError());
            var_dump('Success!');

        } elseif ($entity == 'pubs') {
            $pubs = json_decode(file_get_contents('jsons_asu/pubs.json'), true);

            $mc = new MongoClient();
            $db = $mc->editorum;
            $db->createCollection('references');

            $import_col = $db->selectCollection('import');
            foreach ($pubs as &$pub) {
                // переобновлем структуру публикаций, под языковую поддержку
                if (!isset($pub['ru']) && !isset($pub['en'])) {
                    // создаем список для русского языка
                    $pub['ru'] = [
                        'title'      => iconv_substr($pub['title'], 0, 500, 'UTF-8'),
                        'keywords'   => $pub['keywords'],
                        'annotation' => $pub['annotation'],
                    ];

                    $pub['en'] = [
                        'title'      => iconv_substr($pub['eng_title'], 0, 500, 'UTF-8'),
                        'keywords'   => $pub['eng_keywords'],
                        'annotation' => $pub['eng_annotation'],
                    ];

                    unset($pub['title'], $pub['keywords'], $pub['annotation'],
                        $pub['eng_title'], $pub['eng_keywords'], $pub['eng_annotation']);
                }

                $pub['id'] = (int)$pub['id'];
                $pub['has_images_owned'] = (int)$pub['has_images_owned'];
                $pub['author_pages'] = (string)$pub['author_pages'];
                $pub['user_id'] = (int)$pub['user_id'];
                $pub['issue_id'] = (int)$pub['issue_id'];
                $pub['has_reviewers'] = (int)$pub['has_reviewers'];
                $pub['has_published'] = (int)$pub['has_published'];
                $pub['issue_rubric_id'] = (int)$pub['issue_rubric_id'];
                $pub['issue_iorder'] = (int)$pub['issue_iorder'];
                $pub['is_finalized'] = (int)$pub['is_finalized'];
                //$pub['max_dt'] = new MongoDate(strtotime($pub['max_dt']));
                $pub['show_znanium'] = (int)$pub['show_znanium'];
                $pub['publisher_id'] = (int)$pub['publisher_id'];
                $pub['st_agreement_printed'] = (int)$pub['st_agreement_printed'];
                $pub['parent_id'] = (int)$pub['parent_id'];
                $pub['parent_order'] = (int)$pub['parent_order'];
                $pub['published_page_total'] = (int)$pub['published_page_total'];
                $pub['first_page'] = (int)$pub['first_page'];
                $pub['last_page'] = (int)$pub['last_page'];
                $pub['isbn'] = (string)$pub['isbn'];
                $pub['doi'] = (string)$pub['doi'];
                $pub['classif_okso_journal'] = isset($pub['classif_okso_journal']) ? $pub['classif_okso_journal'] : [];
                $pub['classif_grnti'] = isset($pub['classif_grnti']) ? $pub['classif_grnti'] : [];
                $pub['classif_tbk'] = isset($pub['classif_tbk']) ? $pub['classif_tbk'] : [];
                $pub['lang'] = empty($pub['lang']) ? 'RUS':$pub['lang'];
                $pub['is_show_nauka'] = 1;

                $pub['created_at'] = new \MongoDate(strtotime((!empty($pub['date_create']) ? $pub['date_create'] : date('Y-m-d H:i'))));
                unset($pub['date_create']);
                $pub['updated_at'] = new \MongoDate(strtotime(date('Y-m-d H:i')));

                if ($pub['kind'] == 100) {
                    $kind = Publication::KIND_ARTICLE;
                } elseif ($pub['kind'] == 102) {
                    $kind = Publication::KIND_MONOGRAPHY;
                }

                $pub['kind'] = $kind;
                $pub['statuses'] = [
                    'is_published' => (int)$pub['is_published'],
                ];
                $pub['journal_id'] = (int)$pub['journal'];
                unset($pub['is_published']);
                unset($pub['journal']);
                unset($pub['show_nauka']);
                /*если есть авторы */
                if ($pub['authors'] != null) {
                    $ids = explode(',', $pub['authors']);

                    array_walk($ids, [$this, 'array_conversion']);

                    $author_ids = $import_col->aggregate(
                        ['$match' => ['type' => 'author_asu']],
                        ['$unwind' => '$elements'],
                        [
                            '$match' => [
                                '$or' =>
                                    $ids,
                            ],
                        ],
                        ['$project' => ['_id' => 0, 'elements' => 1]]
                    );

                    foreach ($author_ids['result'] as $new_ids) {
                        $authors[] = $new_ids['elements']['internal_id'];
                    }
                    $pub['authors'] = isset($authors) ? $authors : [];

                    unset($authors);
                }
            }
            $this->insertEntity($db, 'publication', $pubs, $import_col);

            $pubs = new Publication();
            $pubs->createIndex(['ru.title' => 1]);
            $pubs->createIndex(['en.title' => 1]);

            $pubs->createIndex(['created_at' => 1]);
            $pubs->createIndex(['updated_at' => 1]);

            $pubs->createIndex(['ibsn' => 1]);
            $pubs->createIndex(['kind' => 1]);
            $pubs->createIndex(['doi' => 1]);
            $pubs->createIndex(['journal_title' => 1]);

        } elseif ($entity == 'conferences') {
            $conferences = json_decode(file_get_contents('jsons_asu/conferences.json'), true);

            $mc = new MongoClient();
            $db = $mc->editorum;
            $import_col = $db->selectCollection('import');

            foreach ($conferences as &$items) {
                $items['id'] = (int)$items['id'];
                if (!isset($items['ru']) && !isset($items['en'])) {
                    $items['ru'] = [
                        'title' => $items['title'],
                        'city'  => $items['city'],
                    ];
                    $items['en'] = [
                        'title' => $items['title_eng'],
                        'city'  => $items['city_eng'],
                    ];

                    unset($items['title'], $items['city'], $items['title_eng'], $items['city_eng']);
                }
                $items['publication_id'] = (int)$items['publication_id'];
                $items['is_show_nauka'] = (int)$items['is_show_nauka'];
                $items['num'] = (int)$items['num'];
                $items['country_id'] = (int)$items['country_id'];
                $items['publisher_id'] = (int)$items['publisher_id'];
                $items['is_in_work'] = 0;

//                $items['dt_start'] = new \MongoDate(strtotime($items['dt_start']));
//                $items['dt_finish'] = new \MongoDate(strtotime($items['dt_finish']));

                $items['created_at'] = new \MongoDate(time());
                $items['updated_at'] = new \MongoDate(time());
            }

            $pubs = new Conference();
            $pubs->createIndex(['ru.title' => 1]);
            $pubs->createIndex(['en.title' => 1]);

            $pubs->createIndex(['created_at' => 1]);
            $pubs->createIndex(['updated_at' => 1]);

            $this->insertEntity($db, 'conference', $conferences, $import_col);

        } elseif ($entity == 'chapters') {
            $chapters = json_decode(file_get_contents('jsons_asu/chapters.json'), true);
            $mc = new MongoClient();
            $db = $mc->editorum;
            $import_col = $db->selectCollection('import');

            foreach ($chapters as &$chap) {
                if ((int)$chap['pub_id'] > 0) {
                    $cursor = $import_col->findOne(['type' => 'publication_asu'], [
                        'elements' => ['$elemMatch' => [
                            'external_id' => (int)$chap['pub_id']
                        ]]
                    ]);
                    if (isset($cursor['elements'])) {
                        $chap['pub_id'] = (int)$cursor['elements'][0]['internal_id'];
                    }
                }
            }
            $this->insertEntity($db, 'chapters', $chapters, $import_col);

        } elseif ($entity == 'references') {
            $mc = new MongoClient();
            $db = $mc->editorum;

            $references = json_decode(file_get_contents('jsons_asu/references.json'), true);
            $import_col = $db->selectCollection('import');

            $db->selectCollection('references')->drop();

            $collection = $db->createCollection('references');

            $count = 0;
            $arr = [];
            foreach ($references as &$ref) {
                $cursor = $import_col->findOne(
                    ['type' => 'publication_asu'],
                    [
                        'elements' => [
                            '$elemMatch' => [
                                'external_id' => (int)$ref['pub_id'],
                            ],
                        ],
                    ]
                );
                if (isset($cursor['elements'])) {
                    $ref['pub_id'] = (int)$cursor['elements'][0]['internal_id'];
                } else {
                    unset($ref['pub_id']);
                }

                // Приведение типов
                $ref['_id'] = (int)$ref['_id'];
                $ref['number'] = (int)$ref['number'];

                $arr[] = $ref;
                $count++;

                if ($count % 1000 == 0) {
                    $collection->batchInsert($arr);
                    $arr = [];
                }
            }
            $collection->batchInsert($arr);

            // Update sequencies
            $ref = new References();
            $max_ref = Model::getByKeys($ref, [], '_id', -1, true)->getCurrentRecord();
            $db->sequences->save(['_id' => $ref->getCollectionName(), 'seq' => ++$max_ref['_id']]);

        } elseif ($entity == 'rubrics') {

            $rubrics = json_decode(file_get_contents('jsons_asu/rubrics.json'), true);
            $mc = new MongoClient();
            $db = $mc->editorum;
            $import_col = $db->selectCollection('import');
//          $collection = $db->createCollection('rubrics');
            $issue_collection = $db->selectCollection('issue');
            $pubs_collection = $db->selectCollection('publication');

            foreach ($rubrics as $rubric) {
                if (!empty($rubric['issue_ids'])) {
                    $issue_ids[] = ['id' => $rubric['id'], 'issue_ids' => $rubric['issue_ids']];
                }
                if (!empty($rubric['pubs_ids'])) {
                    $pubs_ids[] = ['id' => $rubric['id'], 'pubs_ids' => $rubric['pubs_ids']];
                }
                unset($rubric['issue_ids']);
                unset($rubric['pubs_ids']);
                $rubric['journal_id'] = (int)$rubric['journal_id'];
                $rubric['iorder'] = (int)$rubric['iorder'];
                $to_mongo[] = $rubric;
            }
            $this->insertEntity($db, 'rubrics', $to_mongo, $import_col);

            $issue_collection->update(
                [],
                ['$unset' => ['rubrics' => 1]],
                ['upsert' => false, 'multiple' => true]
            );

            foreach ($issue_ids as $issue) {
                $arr = explode(',', $issue['issue_ids']);
                array_walk($arr, [$this, 'array_conversion_rubric']);
                //Сохраняем iorder для номеров в отдельный массив
                $iorder = [];
                foreach ($arr as &$a) {
                    $iorder[$a['elements.external_id']] = $a['iorder'];
                    unset($a['iorder']);
                }
                $import_col->find();
                
                $new_issue_ids = $import_col->aggregate(
                    ['$match' => ['type' => 'issue_asu']],
                    ['$unwind' => '$elements'],
                    [
                        '$match' => [
                            '$or' =>
                                $arr,
                        ],
                    ],
                    ['$project' => ['_id' => 0, 'elements' => 1]]
                );
//                var_dump($issue['issue_ids']);
//                echo 'ARR';
//                echo '<br>';
//                var_dump($arr);


                $rubrica_id = $import_col->findOne(
                    ['type' => 'rubrics_asu'],
                    [
                        'elements' => [
                            '$elemMatch' => [
                                'external_id' => (int)$issue['id'],
                            ],
                        ],
                    ]
                );
//                echo '<br>';
//                echo 'RESULTS';
//                echo '<br>';
//                var_dump($new_issue_ids['result']);
//                echo '<br>';
//                echo 'FOREACH';
                foreach ($new_issue_ids['result'] as $new_issue) {
//                    echo '<br>';
//                    var_dump($new_issue['elements']['external_id']);
//                    var_dump($iorder[$new_issue['elements']['external_id']]);
                    $issue = Model::getById(new Issue(), $new_issue['elements']['internal_id']);
                    if (!empty($issue)) {
                        $issue->addRubric($rubrica_id['elements'][0]['internal_id'], $iorder[$new_issue['elements']['external_id']]);                        
                    }
//                    var_dump($new_issue['elements']['internal_id']);

//                    $issue_collection->update(
//                        ['_id' => $new_issue['elements']['internal_id']],
//                        ['$push' => [
//                            'rubrics' => [
//                                $iorder[$new_issue['elements']['external_id']] => $rubrica_id['elements'][0]['internal_id']
//                            ]
//                        ]]
//                    );
                }
            }

            /*Обновляем Id рубрики в PUBS*/
            foreach ($pubs_ids as $pub) {
                $arr = explode(',', $pub['pubs_ids']);
                array_walk($arr, [$this, 'array_conversion']);
                $new_pub_ids = $import_col->aggregate(
                    ['$match' => ['type' => 'publication_asu']],
                    ['$unwind' => '$elements'],
                    [
                        '$match' => [
                            '$or' =>
                                $arr,
                        ],
                    ],
                    ['$project' => ['_id' => 0, 'elements' => 1]]
                );
                $rubrica_id = $import_col->findOne(
                    ['type' => 'rubrics_asu'],
                    [
                        'elements' => [
                            '$elemMatch' => [
                                'external_id' => (int)$pub['id'],
                            ],
                        ],
                    ]
                );
                foreach ($new_pub_ids['result'] as $new_pub) {
                    $pubs_collection->update(
                        ['_id' => $new_pub['elements']['internal_id']],
                        ['$set' => ['issue_rubric_id' => (int)$rubrica_id['elements'][0]['internal_id']]]
                    );
                }
            }
        } elseif ($entity == 'publishers') {
            $publishers = json_decode(file_get_contents('jsons_asu/publishers.json'), true);
            $mc = new MongoClient();
            $db = $mc->editorum;
            $import_col = $db->selectCollection('import');
            $pubs_collection = $db->selectCollection('publication');
            $journal_collection = $db->selectCollection('journal');
            $this->insertEntity($db, 'publisher', $publishers, $import_col);

            foreach ($publishers as $publisher) {
                $publisher_id = $import_col->findOne(
                    ['type' => 'publisher_asu'],
                    [
                        'elements' => [
                            '$elemMatch' => [
                                'external_id' => (int)$publisher['id'],
                            ],
                        ],
                    ]
                );
                $pubs_collection->update(
                    [
                        'publisher_id' => (int)$publisher_id['elements'][0]['external_id'],
                    ],
                    ['$set' => ['publisher_id' => $publisher_id['elements'][0]['internal_id']]],
                    ['upsert' => false, 'multiple' => true]
                );

                $journal_collection->update(
                    [
                        'publisher_id' => (string)$publisher_id['elements'][0]['external_id'],
                    ],
                    ['$set' => ['publisher_id' => $publisher_id['elements'][0]['internal_id']]],
                    [
                        'upsert'   => false,
                        'multiple' => true,
                    ]
                );
            }

        } elseif ($entity == 'countries') {
            $countries = json_decode(file_get_contents('jsons_asu/countries.json'), true);
            $mc = new MongoClient();
            $db = $mc->editorum;
            $import_col = $db->selectCollection('import');
            $author_collection = $db->selectCollection('author');
            $conference_collection = $db->selectCollection('conference');

            $this->insertEntity($db, 'countries', $countries, $import_col);

            foreach ($countries as $country) {
                $country_id = $import_col->findOne(
                    ['type' => 'countries_asu'],
                    [
                        'elements' => [
                            '$elemMatch' => [
                                'external_id' => (int)$country['id'],
                            ],
                        ],
                    ]
                );

                $author_collection->update(
                    [
                        'country_id' => (string)$country_id['elements'][0]['external_id'],
                    ],
                    ['$set' => ['country_id' => $country_id['elements'][0]['internal_id']]],
                    ['upsert' => false, 'multiple' => true]
                );

                $author_collection->update(
                    [
                        'reg_country_id' => (string)$country_id['elements'][0]['external_id'],
                    ],
                    ['$set' => ['reg_country_id' => $country_id['elements'][0]['internal_id']]],
                    ['upsert' => false, 'multiple' => true]
                );

                $conference_collection->update(
                    [
                        'country_id' => (string)$country_id['elements'][0]['external_id'],
                    ],
                    ['$set' => ['country_id' => $country_id['elements'][0]['internal_id']]],
                    ['upsert' => false, 'multiple' => true]
                );
            }


        } elseif ($entity == 'regions') {
            $regions = json_decode(file_get_contents('jsons_asu/regions.json'), true);
            $mc = new MongoClient();
            $db = $mc->editorum;
            $import_col = $db->selectCollection('import');
            $author_collection = $db->selectCollection('author');

            $this->insertEntity($db, 'regions', $regions, $import_col);

            foreach ($regions as $region) {
                $region_id = $import_col->findOne(
                    ['type' => 'regions_asu'],
                    [
                        'elements' => [
                            '$elemMatch' => [
                                'external_id' => (int)$region['id'],
                            ],
                        ],
                    ]
                );

                $author_collection->update(
                    [
                        'region_id' => (string)$region_id['elements'][0]['external_id'],
                    ],
                    ['$set' => ['region_id' => $region_id['elements'][0]['internal_id']]],
                    ['upsert' => false, 'multiple' => true]
                );

                $author_collection->update(
                    [
                        'reg_region_id' => (string)$region_id['elements'][0]['external_id'],
                    ],
                    ['$set' => ['reg_region_id' => $region_id['elements'][0]['internal_id']]],
                    ['upsert' => false, 'multiple' => true]
                );
            }

        } elseif ($entity == 'universities') {
            $universities = json_decode(file_get_contents('jsons_asu/universities.json'), true);
            $mc = new MongoClient();
            $db = $mc->editorum;
            $import_col = $db->selectCollection('import');
            $author_collection = $db->selectCollection('author');

            foreach ($universities as &$univer) {
                $country_id = $import_col->findOne(
                    ['type' => 'countries_asu'],
                    [
                        'elements' => [
                            '$elemMatch' => [
                                'external_id' => (int)$univer['country_id'],
                            ],
                        ],
                    ]
                );
                if (isset($country_id['elements'][0]['external_id'])) {
                    $univer['country_id'] = (int)$country_id['elements'][0]['external_id'];
                } else {
                    $univer['country_id'] = 0;
                }
            }
            $this->insertEntity($db, 'universities', $universities, $import_col);

            foreach ($universities as $univer) {
                $univer_id = $import_col->findOne(
                    ['type' => 'universities_asu'],
                    [
                        'elements' => [
                            '$elemMatch' => [
                                'external_id' => (int)$univer['id'],
                            ],
                        ],
                    ]
                );

                $author_collection->update(
                    [
                        'university_id' => (string)$univer_id['elements'][0]['external_id'],
                    ],
                    ['$set' => ['university_id' => $univer_id['elements'][0]['internal_id']]],
                    ['upsert' => false, 'multiple' => true]
                );
            }
        } elseif ($entity == 'collections') {
            $collections = json_decode(file_get_contents('jsons_asu/collections.json'), true);
//            $mc = new MongoClient();
//            $db = $mc->editorum;
            $this->db->createCollection('collections');
            $conference_collection = $this->db->selectCollection('conference');
            $import_col = $this->db->selectCollection('import');

            foreach ($collections as &$col) {
                if ($col['articles'] != null) {

                    $ids = explode(',', $col['articles']);

                    array_walk($ids, [$this, 'array_conversion']);

                    $articles_ids = $import_col->aggregate(
                        ['$match' => ['type' => 'publication_asu']],
                        ['$unwind' => '$elements'],
                        [
                            '$match' => [
                                '$or' =>
                                    $ids,
                            ],
                        ],
                        ['$project' => ['_id' => 0, 'elements' => 1]]
                    );

                    foreach ($articles_ids['result'] as $new_ids) {
                        $articles[] = $new_ids['elements']['internal_id'];
                    }
                    $col['articles'] = $articles;
                    unset($articles);
                }
                $publisher_id = $import_col->findOne(
                    ['type' => 'publisher_asu'],
                    [
                        'elements' => [
                            '$elemMatch' => [
                                'external_id' => (int)$col['publisher_id'],
                            ],
                        ],
                    ]
                );
                $col['publisher_id'] = (int)$publisher_id['elements'][0]['internal_id'];
                if (!isset($col['ru']) && !isset($col['en'])) {
                    $col['ru'] = [
                        'title'      => $col['title'],
                        'keywords'   => $col['keywords'],
                        'annotation' => $col['annotation'],
                    ];
                    $col['en'] = [
                        'title'      => $col['eng_title'],
                        'keywords'   => $col['eng_keywords'],
                        'annotation' => $col['eng_annotation'],
                    ];

                    unset($col['title'], $col['keywords'], $col['annotation'], $col['eng_title'], $col['eng_keywords'], $col['eng_annotation']);
                }

                $col['created_at'] = new \MongoDate(strtotime((isset($col['date_create']) ? $col['date_create'] : date('Y-m-d H:i'))));
                unset($col['date_create']);
                $col['updated_at'] = new \MongoDate(strtotime(date('Y-m-d H:i')));
                $col['is_show_nauka'] = (int)$col['is_published'];
                $col['is_published'] = (int)$col['is_published'];

            }
            $this->insertEntity($this->db, 'collections', $collections, $import_col);
            foreach ($collections as $coll) {
                $collection_id = $import_col->findOne(
                    ['type' => 'collections_asu'],
                    [
                        'elements' => [
                            '$elemMatch' => [
                                'external_id' => (int)$coll['id'],
                            ],
                        ],
                    ]
                );

                $conference_collection->update(
                    [
                        'publication_id' => (string)$collection_id['elements'][0]['external_id'],
                    ],
                    ['$set' => ['publication_id' => $collection_id['elements'][0]['internal_id']]],
                    ['upsert' => false, 'multiple' => true]
                );
            }

            $model = new Collections();
            $model->createIndex(['ru.title' => 1]);
            $model->createIndex(['en.title' => 1]);

            $model->createIndex(['created_at' => 1]);
            $model->createIndex(['updated_at' => 1]);

        }

        echo "End: ".date("Y-m-d H:i:s", time())."\n";
        die();

    }

    protected function getPdo()
    {
        $host = $this->container->getParameter('mysql_database_host');
        $db_name = $this->container->getParameter('mysql_database_name');
        $user = $this->container->getParameter('mysql_database_user');
        $pass = $this->container->getParameter('mysql_database_password');

        try {
            $pdo = new PDO("mysql:host={$host};dbname={$db_name}", $user, $pass);
            $pdo->exec("set names utf8");
        } catch (PDOException $e) {
            $pdo = "Error!: ".$e->getMessage()."<br/>";
        }

        return $pdo;
    }


    /**
     * @param $db
     * @param $name
     * @param $objects
     * @param $import_col
     * @param bool $force принудительное обновление существующих элементов
     */
    protected function insertEntity($db, $name, $objects, $import_col, $force = true)
    {
        $new_objects = json_encode($objects);
        $objects = json_decode($new_objects, true);

        $object_col = $db->selectCollection($name);

        $sequence_value = $object_col->count();
        if ($sequence_value > 0) {
            $sequence = $object_col->find([], ['_id' => 1])->sort(['_id' => -1])->limit(1);
            $sequence->next();
            $sequence_value = $sequence->current()['_id'];
        } else {
            $sequence_value = 1;
        }

        if (is_null($db->sequences->findOne(['_id' => $name]))) {
            $db->sequences->insert(['_id' => $name, 'seq' => $sequence_value]);
        } else {
            $db->sequences->update(['_id' => $name], ['_id' => $name, 'seq' => $sequence_value]);
        }

        foreach ($objects as &$object) {

            $obj_id = $object['id'];
            unset($object['id']);
            //Проверяем объект в таблице импорта
            $cursor = $import_col->findOne(
                ['type' => $name.'_asu'],
                [
                    'elements' => [
                        '$elemMatch' => [
                            'external_id' => (int)$obj_id,
                        ],
                    ],
                ]
            );

            if (isset($cursor['elements'])) {
                if ($force == true)
                    $object_col->update(['_id' => $cursor['elements'][0]['internal_id']], $object);
            } else {
                //Добавляем данные в импорт, добавляем объект в БД
                $sequence = $db->sequences->findOne(['_id' => $name]);
                $object['_id'] = (int) $sequence['seq'];
                $cur_import = [
                    'external_id' => (int)$obj_id,
                    'internal_id' => (int)$object['_id'],
                ];

                $imp_cursor = $import_col->findOne(['type' => $name.'_asu']);

                if (is_null($imp_cursor)) {
                    //Выполняется если нет такого документа
                    $object_import = [];
                    $object_import['type'] = $name.'_asu';
                    $object_import['elements'] = [$cur_import];
                    $import_col->insert($object_import);
                } else {
                    $import_col->update(
                        ['_id' => $imp_cursor['_id']],
                        ['$push' => ['elements' => $cur_import]]
                    );
                }

                $object_col->insert($object);
                $new_sequence = $object['_id'] + 1;
                $db->sequences->update(['_id' => $name], ['$set' => ['seq' => $new_sequence]]);
            }
        }
    }

    protected function array_conversion_rubric(&$value)
    {
        if (!strpos($value, '-')) {
            var_dump($value);die();
        }
        $values = explode('-', $value);
        $value = [
            'elements.external_id' => (int)$values[0],
            'iorder'               => (int)$values[1]
        ];
    }

    protected function array_conversion(&$value)
    {
        $value = ['elements.external_id' => (int)$value];
    }

    public function mongoTree($array, $parent)
    {
        foreach ($array as $key => $item) {
            if (isset($item['childrens'])) {
                $copy['id'] = (int)$item['id'];
                $copy['parent_id'] = (int)$item['parent_id'];
                $copy['new_code'] = $item['new_code'];
                $copy['title'] = $item['title'];
                $new_item = $this->mongoTree($item['childrens'], $copy);
            } else {
                $new_item = $item;
                $new_item['id'] = (int)$new_item['id'];
                $new_item['parent_id'] = (int)$new_item['parent_id'];
                unset($new_item['level_id']);
                unset($new_item['type']);
                unset($new_item['qualification']);
                unset($new_item['old_code']);
                unset($new_item['code']);
                unset($new_item['rubricator']);
            }
            $parent['childrens'][] = $new_item;
        }

        return $parent;
    }

}
