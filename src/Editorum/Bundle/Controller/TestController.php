<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 09.10.2015
 * Time: 14:33
 */


namespace Editorum\Bundle\Controller;

use Editorum\Bundle\AccessSystem\Person;
use Editorum\Bundle\Logic\Organization;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TestController extends Controller
{
    /**
     * Тестирование системы папок
     */
    public function folderAction()
    {
    }


    public function indexAction()
    {
         $entity_cache = array();

        /**
         * ---------------------------------------
         * Заполняем БД контрагентов
         * --------------------------------------
         */
        $ct = array();


        // Создаем авторов
        $a = new Person();
        $a->create('Виктор', 'Тьмутараканов');
        $ct['author1'] = $a;

        $a = new Person();
        $a->create('Данил', 'Багров');
        $ct['author2'] = $a;

        // Создаем организацию
        $o = new Organization();
        $o->create('Инфра-М');
        $ct['infra'] = $o;

        $o = new Organization();
        $o->create('Znanium');
        $ct['znanium'] = $o;

        /**
         * Создаем предмет спора
         */
        $ent = array();
        $e = new PublicationEntity();
        $e->create('Роман в стихах');
        $ent['roman'] = $e;

        /**
         * Создаем договор
         */
        $agr = array();

        $el = new FixedEntityList();
        $el->setList(array($ent['roman']));

        // Передаем права от Виктора Даниле
        $a = new CoauthorAgreement();
        $a->create(
            $ct['author1'],
            $ct['author2'],
            $el,
            array(),
            new BasicRights()
        );
        $agr['agr1'] = $a;

        // Передаем права от Данилы Инфре
        $a = new OrderAgreement();
        $a->create(
            $ct['author2'],
            $ct['infra'],
            $el,
            array(),
            new BasicRights()
        );
        $agr['agr2'] = $a;

        // Передаем права от Виктора Инфре
        $a = new OrderAgreement();
        $a->create(
            $ct['author1'],
            $ct['infra'],
            $el,
            array(),
            new BasicRights()
        );
        $agr['agr3'] = $a;

        // Передаем права от Инфры Знаниуму
        $a = new OrderAgreement();
        $a->create(
            $ct['infra'],
            $ct['znanium'],
            $el,
            array(),
            new BasicRights()
        );
        $agr['agr4'] = $a;

        /***
         *  Эксперименты над БД.
         *  Часть 1. Проверка передачи прав.
         */

        $ids = EntityCache::getInstance()->getEntities($ct['infra'], 'PublicationEntity');
        var_dump($ids);
        die();

        //return $this->render('AsuBundle:Default:series.html.twig', array('series' => $series));
    }

    /**
     * Новый тест договорной системы
     */
    public function newagrAction()
    {
        $ct = array();

//        $ct['author1'] = Model::insert(new User(), array('last_name' => 'Суслов', 'login' => 'suslov@kremlin.ru'));
//        $ct['author1'] = Model::getById(new User(), 211);
        $ct['author1'] = 211;

//        $ct['author2'] = Model::insert(new User(), array('last_name' => 'Брежнев', 'login' => 'brezhnev@kremlin.ru'));

        // Создаем организацию
//        $ct['infra'] = Model::insert(new Organization(), array('title' => 'Менатеп'));
//        $ct['infra'] = Model::getById(new Organization(), 191);
        $ct['infra'] = 191;

//        $ct['znanium'] = Model::insert(new Organization(), array('title' => 'АФК Система'));

//        var_dump($ct['infra']);
//        var_dump($ct['author1']);
        
        $agreement = new EmployeeAgreement();
        $agreement->create($ct['infra'], $ct['author1']);

        $asys = ASys::instance();
        var_dump($asys->getRegisteredAgreementsTitles());
        die();
        
        $agreement = $asys->saveAgreement($agreement);

//        var_dump($agreement);

        var_dump('Создан договор: ' . $agreement->_id);
        
        if ($agreement->_id) {
            $agreement = $asys->executeAgreement($agreement);
        }
        
        var_dump('Статус договора: ' . $agreement->isActive());
        
        die();
    }
    
}
