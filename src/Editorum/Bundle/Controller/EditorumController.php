<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 11.11.2015
 * Time: 13:22
 */

namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Document\Common\AbstractCorporateEntity;
use Editorum\Bundle\Document\CorporateEntity;
use Editorum\Bundle\Document\User;
use Editorum\Bundle\Form\CorporateForm;
use Editorum\Bundle\Document\Organization;
use Editorum\Bundle\Form\OrganizationForm;
use Editorum\Bundle\Form\UserForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class EditorumController
 * @package Editorum\Bundle\Controller
 *
 * @Security("is_granted('ROLE_ADMIN')")
 */
class EditorumController extends BasicController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('AsuBundle:Default:editorum_main.html.twig');
    }

    /**
     * Список организаций, участников системы Editorum
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function organizationListAction()
    {
        $odm = $this->get('doctrine.odm.mongodb.document_manager');
        $orgs = $odm->getRepository('AsuBundle:Organization')->findAll();
        return $this->render('AsuBundle:Default/organization:organizations.html.twig', array('orgs' => $orgs));
    }


    /**
     * Создание/редактирование организации
     * @param int $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function organizationEditAction($id = 0, Request $request)
    {
        $odm = $this->get('doctrine.odm.mongodb.document_manager');
        if ((int)$id > 0) {
            $org = $odm->getRepository('AsuBundle:Organization')->find((int)$id);
        } else {
            $org = new Organization();
        }
        $form = $this->createForm(new OrganizationForm(), $org);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $org = $form->getData();
            $odm->persist($org);
            $odm->flush();
            return $this->redirectToRoute('editorum_orgs', [], 301);
        }
        return $this->render('AsuBundle:Default/organization:organization_edit.html.twig', array(
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('textbook', [$or]),
            'form'      => $form->createView()
        ));
    }


    /**
     * Сохраняем организацию
     * @param Request $request
     * @return Response
     */
    public function organizationRemoveAction(Request $request)
    {
        $id = $request->get('id');
        $odm = $this->get('doctrine.odm.mongodb.document_manager');
        $org = $odm->getRepository('AsuBundle:Organization')->find((int)$id);
        if (!is_null($org)) {
            $odm->remove($org);
            $odm->flush();
        }

        return new Response(json_encode([
            'success'  => true,
            'id'       => $id
        ]));
    }


    /**
     * Список сотрудников организации
     * @param Organization $organization
     * @return Response
     */
    public function corporateListAction(Organization $organization)
    {
//        $org = (int)$id > 0 ? $odm->getRepository('AsuBundle:Organization')->find((int)$id) : new Organization();

        return $this->render('AsuBundle:Default/organization:corporate_list.html.twig', array(
            'sidemenu'      => $this->get('navigation.menu')->get('Side')->getMenu('organization', [$organization]),
            'organization'  => $organization,
            'kinds'         => AbstractCorporateEntity::getKinds()
        ));
    }


//    /**
//     * @param int $organization_id
//     * @param int $id
//     * @param Request $request
//     * @return Response
//     * @throws \Doctrine\ODM\MongoDB\LockException
//     */
//    public function corporateEditAction($organization_id = 0, $id = 0, Request $request)
//    {
//        $odm = $this->get('doctrine.odm.mongodb.document_manager');
//        $corporate = (int)$id > 0 ?
//            $odm->getRepository('AsuBundle:AbstractCorporateEntity')->find((int)$id) : new AbstractCorporateEntity();
//
//        $organization = $odm->getRepository('AsuBundle:Organization')->find((int)$organization_id);
//
//        $form = $this->createForm(new CorporateForm(), $corporate);
//        $form->handleRequest($request);
//        if ($form->isSubmitted() && $form->isValid()) {
//            $corporate = $form->getData();
//            $corporate->setOrganization($organization);
//            $odm->persist($corporate);
//            $odm->flush();
//            return $this->redirectToRoute('corporate_list', ['id' => $organization_id], 301);
//        }
//        return $this->render('AsuBundle:Default/organization:corporate_edit.html.twig', array(
//            'sidemenu'      => $this->get('navigation.side')->corporateMenu($corporate),
//            'active'        => 'edit',
//            'form'          => $form->createView(),
//        ));
//    }


    /**
     * Сохраняем организацию
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function corporateRemoveAction(Request $request)
    {
        $id = $request->get('id');
        $organization_id = $request->get('organization_id');
        $odm = $this->get('doctrine.odm.mongodb.document_manager');
        $org = $odm->getRepository('AsuBundle:Organization')->find((int)$organization_id);
        $corporate = $odm->getRepository('AsuBundle:CorporateEntity')->find((int)$id);
        if (!is_null($corporate)) {
            $org->removeCorporate($corporate);
            $odm->remove($corporate);
            $odm->flush();
        }

        return new Response(json_encode([
            'success'  => true,
            'id'       => $id
        ]));
    }


    /**
     * Список сотрудников организации
     * @param $id
     * @return Response
     */
    public function employeeListAction($id)
    {
        $odm = $this->get('doctrine.odm.mongodb.document_manager');
        $corporate = $odm->getRepository('AsuBundle:CorporateEntity')->find((int)$id);

        return $this->render('AsuBundle:Default/organization:employee_list.html.twig', array(
            'sidemenu'      => $this->get('navigation.side')->corporateMenu($corporate),
            'active'        => 'employee',
            'corporate'     => $corporate,
        ));
    }


    /**
     * Форма приглашения сотрудника в организацию
     * @param $id
     * @param Request $request
     * @return Response
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
//    public function employeeEditAction($id, Request $request)
//    {
//        $odm = $this->get('doctrine.odm.mongodb.document_manager');
//        if ((int)$id > 0) {
//            $user = $odm->getRepository('AsuBundle:User')->find((int)$id);
//        } else {
//            $user = new \Editorum\Bundle\Document\User();
//        }
//
//        $form = $this->createForm($this->get('asu_bundle.corporate.form.user.type'), $user);
//        $form->handleRequest($request);
//        if ($form->isSubmitted() && $form->isValid()) {
//            $user->setPassword(
//                $this->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword())
//            );
//            $user->setEnabled(true);
//            $odm->persist($user);
//            $odm->flush();
//
//            return $this->redirectToRoute(
//                'editorum_employees',
//                ['id' => $user->getCorporate()->getId()],
//                301
//            );
//        }
//        return $this->render('AsuBundle:Default/organization:employee_edit.html.twig', array(
//            'form'      => $form->createView()
//        ));
//
//    }


    /**
     * Сохраняем организацию
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function employeeRemoveAction(Request $request)
    {
        $id = $request->get('id');
        $odm = $this->get('doctrine.odm.mongodb.document_manager');
        $employee = $odm->getRepository('AsuBundle:User')->find((int)$id);
        if (!is_null($employee)) {
//            $org->removeCorporate($corporate);
            $odm->remove($employee);
            $odm->flush();
        }

        return new Response(json_encode([
            'success'  => true,
            'id'       => $id
        ]));
    }





    /**
     * Отправить приглашение на присоединение к организации
     */
    public function employeeInvitationSendAction()
    {
        $data = json_decode($this->get('request')->request->get('data'), true);

        /**
         * @TODO Реализовать процедуру присоединения к организации - отправка письма и переход по ссылке в нем.
         * в случае если произошел переход по ссылке, то содание EmployeeAgreement
         */

        return new JsonResponse(json_encode(array('id' => $data['corporate_id'])));
    }



}
