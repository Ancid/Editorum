<?php
/**
 * Created by PhpStorm.
 * User: vgfilippov
 * Date: 30.11.15
 * Time: 12:35
 */

namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Logic\Rinc\RincBase;
use Editorum\Bundle\Rinc\RincFactory;
use ErrorException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use \ZipArchive;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Editorum\Bundle\Logic\RincGenerator;

class RincController extends BasicController
{
    /**
     * @param $id
     * @param $collection
     * @return BinaryFileResponse|Response
     */
    public function rincAction($id, $collection)
    {
        $xml = RincGenerator::factory($id, $collection, $this->getODM());
        if (!$xml instanceof RincBase) {
            throw new NotFoundHttpException($xml);
        }

        $xml_text = $xml->getXml();
        $xml_clear = $xml->clearEmptyTags($xml_text);

        $bom = chr(239).chr(187).chr(191);
        $content = iconv('UTF-8', 'UTF-16LE', $bom.$xml_clear);

        if (!$this->get('filesystem')->exists('temp')) {
            try {
                $this->get('filesystem')->mkdir('temp');
            } catch (ErrorException $e) {
                return new Response($e->getMessage());
            }
        }

        $dir = 'temp'.DIRECTORY_SEPARATOR;
        $xml_file_name = $id.'.xml';
        $zip_name = 'user_'.date('Ymd').'.zip';
        file_put_contents($dir.$xml_file_name, $content);
//        $this->get('filesystem')->touch($dir . 'test.zip');
        if (class_exists('ZipArchive')) {
            $zip = new ZipArchive();
            $zip->open($dir . 'test.zip');
            for ($i = 0; $i < $zip->numFiles; $i++) {
                $zip->deleteIndex($i);
            }
            $zip->addFile($dir . $xml_file_name, $xml_file_name);
        }
        $response = new BinaryFileResponse($dir.'test.zip');
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $zip_name
        );

        return $response;
    }

}
