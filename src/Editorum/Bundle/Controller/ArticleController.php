<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 23.08.2015
 * Time: 13:06
 */

namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Document\Article;
use Editorum\Bundle\Document\Doi;
use Editorum\Bundle\Document\FileStorage as FileStorageDocument;
use Editorum\Bundle\Document\Journal as JournalDocument;
use Editorum\Bundle\Document\Reference;
use Editorum\Bundle\Form\Article\ArticleClassifType;
use Editorum\Bundle\Form\FileStorage;
use Editorum\Bundle\Form\Monography\AuthorType;
use Editorum\Bundle\Form\Monography\NewAuthorType;
use Editorum\Bundle\Form\Reference\AddForm;
use Editorum\Bundle\Form\Reference\EditForm;
use Editorum\Bundle\Form\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Editorum\Bundle\Logic\Classif;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @package Editorum\Bundle\Controller
 *
 * @Security("is_granted('ROLE_ARTICLE_GROUP')")
 */
class ArticleController extends BasicController
{
    /**
     * Список статей
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $user = $this->getUser();
        $request = $this->get('request');
        $search_txt = $request->get('search');
        $qb = $this->getODM()->createQueryBuilder('AsuBundle:Article');

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $corp = [];
            foreach ($this->getUser()->getCorporate()->getOrganization()->getCorporates()->toArray() as $co) {
                $corp[$co->getKind()][] = $co->getId();
            }
            foreach ($corp as $type => $ids) {
                $qb
                    ->addOr(
                        $qb->expr()
                            ->addAnd($qb->expr()->field('publisher.$ref')->equals($type))
                            ->addAnd($qb->expr()->field('publisher.$id')->in($ids))
                    );
            }
        } else {
            $qb->field('publisher')->references($user->getCorporate());
        }

//        if (!is_null($search_txt) && strlen($search_txt) > 2) {
//            $results = Search::getIds($search_txt);
//            $articles->field('id')->in($results['matches']);
//        } else {
//            $search_txt = '';
//        }
        $articles = $qb->sort('id', 'DESC');

        if ($request->get('filter')) {
            $filter = $this->get('publication.statuses_filter');

            $filter
                ->prepareData($request->get('filter'))
                ->makeFilter($articles);
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $articles,
            $this->get('request')->query->getInt('page', 1),
            30
        );

        return $this->render('AsuBundle:Default/article:articles.html.twig', [
            'pagination'   => $pagination,
            'search_txt'   => $search_txt,
            'kinds'        => JournalDocument::getKinds(),
            'st_published' => JournalDocument::ST_IS_PUBLISHED,
        ]);
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $article = new Article();
        $statuses = [
            'status'        => JournalDocument::ST_IN_PROGRESS,
            'st_doi'        => 0,
            'has_reviewers' => 0,
            'has_published' => 0,
            'has_agreement' => 0,
            'is_show_nauka' => 0,
        ];
        $article
            ->setStatuses($statuses);

        $form = $this->createForm($this->get('asu.form.publication.article_create_type'), $article)
            ->add('save', 'submit', [
                'label' => 'Создать',
                'attr' => ['class' => 'btn btn-success pull-right']
            ]);

        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                /** @var Article $article */
                $article = $form->getData();
                $article->setPublisher($article->getJournal()->getPublisher());

                //Наследуем все классификаторы журнала
                $this->getODM()->getRepository('AsuBundle:Common\AbstractPublication')
                    ->inheritClassifs($article->getJournal(), $article);

                $this->getODM()->persist($article);
                $this->getODM()->flush();

                return $this->redirectToRoute('article_edit', ['id' => $article->getId()]);
            }
        }

        return $this->render('AsuBundle:Default/article:article_create.html.twig', [
            'form'  => $form->createView()
        ]);
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param Article $article
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Article $article, Request $request)
    {
        $form = $this->createForm($this->get('asu.form.publication.article_type'), $article)
            ->add('save', 'submit', [
                'label' => 'Сохранить',
                'attr' => ['class' => 'btn btn-success pull-right']
            ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Upload depofile
            if ($article->getDepofile() instanceof UploadedFile) {
                /** @var UploadedFile $file */
                $file = $article->getDepofile();

                if (in_array($file->getClientOriginalExtension(), ['odt', 'rtf', 'pdf', 'doc', 'docx'])) {
                    $path = 'upload/conference/';
                    if (!$this->get('filesystem')->exists($path)) {
                        $this->get('filesystem')->mkdir($path);
                    }
                    $name = $file->getClientOriginalName();
                    $file->move($path, $name);
                    $article->setDepofile($path . $name);
                } else {
                    $this->addFlash('error', 'Неверный тип депонированного файла.');
                    $article->setDepofile(null);
                }
            }
            $this->getODM()->flush();
            $this->addFlash('notice', 'Сохранено!');

            return $this->redirectToRoute('article_authors', [ 'id' => $article->getId() ]);
        }

        $udk = $this->getODM()->getRepository('AsuBundle:ClassificatorUdk')->findAll();

        return $this->render('AsuBundle:Default/article:article_edit.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('article', [$article]),
            'form'      => $form->createView(),
            'article'   => $article,
            'udk'  => $udk,
            'st_published' => JournalDocument::ST_IS_PUBLISHED,
        ]);
    }


    /**
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function showAction(Article $article)
    {
        $classif = new Classif();
        $classifs = $classif->loadClassif(['okso_journal', 'grnti', 'tbk']);

        return $this->render('AsuBundle:Default/article:article_show.html.twig', [
            'article'   => $article,
            'kinds'     => JournalDocument::getKinds(),
            'classifs'  => $classifs
        ]);
    }


    /**
     * @param Article $article
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function authorsAction(Article $article, Request $request)
    {
        $this->get('session')->remove('way[route]');
        $this->get('session')->remove('way[id]');

        $form = $this->createForm(new AuthorType());
        $newAuthorForm = $this->createForm(new NewAuthorType());

        if ($request->isMethod($request::METHOD_POST)) {
            $form->handleRequest($request);
            $newAuthorForm->handleRequest($request);

            if ($form->isValid()) {
                $author = $form->getData()['author'];

                if (!in_array($author, $article->getAuthors()->toArray())) {
                    $article->addAuthor($author);

                    $this->getODM()->persist($article);
                    $this->getODM()->flush();

                    return $this->redirectToRoute('article_authors', ['id' => $article->getId()]);
                }
            }

            if ($newAuthorForm->isValid()) {
                $author = $newAuthorForm->getData();
                $author->setFio();

                if (!in_array($author, $article->getAuthors()->toArray())) {
                    $article->addAuthor($author);

                    $this->getODM()->persist($author);
                    $this->getODM()->persist($article);
                    $this->getODM()->flush();

                    return $this->redirectToRoute('author_edit', [
                        'id' => $author->getId(),
                        'way[route]' => 'article_authors',
                        'way[id]'    => $article->getId(),
                    ]);
                }
            }
        }

        return $this->render('AsuBundle:Default/article:article_authors.html.twig', [
            'sidemenu'      => $this->get('navigation.menu')->get('Side')->getMenu('article', [ $article ]),
            'entity'        => $article,
            'form'          => $form->createView(),
            'newAuthorForm' => $newAuthorForm->createView(),
        ]);
    }


    /**
     * @Security("is_granted('ROLE_DELETE')")
     * @param Article $article
     * @param $authorId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function removeAuthorAction(Article $article, $authorId)
    {
        $author = $this->getODM()->getRepository('AsuBundle:Author')->find($authorId);

        if (null !== $author && in_array($author, $article->getAuthors()->toArray())) {
            $article->removeAuthor($author);
            $this->getODM()->persist($article);
            $this->getODM()->flush();
        }

        return $this->redirectToRoute('article_authors', [ 'id' => $article->getId() ]);
    }


    /**
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function referencesAction(Article $article)
    {
        return $this->render('AsuBundle:Default/article:article_references.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('article', [$article]),
            'active'    => 'refs',
            'entity'    => $article,
        ]);
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param Article $article
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addReferenceAction(Article $article, Request $request)
    {
        $form = $this->createForm(new AddForm(), null);

        if ($request->getMethod() === "POST") {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $this->getODM()->getRepository('AsuBundle:Reference')
                    ->addReferences($data, $article, $this->get('validator'));

                return $this->redirectToRoute('article_references', [ 'id' => $article->getId() ]);
            }
        }

        return $this->render('AsuBundle:Default/templates:reference_add.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('article', [$article]),
            'active'    => 'refs',
            'form'      => $form->createView()
        ]);
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param $publication_id
     * @param Reference $reference
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function referenceEditAction($publication_id, Reference $reference, Request $request)
    {
        $article = $this->getODM()->getRepository('AsuBundle:Article')->find($publication_id);
        $form = $this->createForm(new EditForm(), $reference);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getODM()->flush($reference);

            return $this->redirectToRoute('article_references', [ 'id' => $publication_id ]);
        }

        return $this->render('AsuBundle:Default/templates:reference_edit.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('article', [$article]),
            'active'    => 'refs',
            'form'      => $form->createView()
        ]);
    }


    /**
     * @Security("is_granted('ROLE_DELETE')")
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction(Article $article)
    {
        if ($article->getStatuses()['status'] != JournalDocument::ST_IS_PUBLISHED) {
            $this->getODM()->remove($article);
            $this->getODM()->flush();
            $this->addFlash('notice', 'Статья успешно удалена!');
        } else {
            $this->addFlash('notice', 'Нельзя удалить опубликованную статью!');
        }

        return $this->redirectToRoute('article_list');
    }


    /**
     * @param $publication_id
     * @param Reference $reference
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function referenceRemoveAction($publication_id, Reference $reference)
    {
        $textbook = $this->getODM()->getRepository('AsuBundle:Article')->find($publication_id);
        $textbook->removeReference($reference);
        $this->getODM()->flush();

        return $this->redirectToRoute('article_references', [ 'id' => $publication_id ]);
    }


    /**
     * @param Article $article
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function textAction(Article $article, Request $request)
    {
        $form = $this->createForm(new TextType(), $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getODM()->flush();
        } else {
            $session = $this->get('session');
            foreach ($form->getErrors() as $error) {
                $session->getFlashBag()->add('error', $error->getMessage());
            }
        }

        return $this->render('AsuBundle:Default/templates:text.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('article', [$article]),
            'active'    => 'text',
            'form'      => $form->createView(),
            'entity'    => $article,
        ]);
    }


    /**
     * Загрузить файл
     *
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function filesUploadAction(Article $article)
    {
        /** @var null|FileStorageDocument[] $existsFiles */
        $existsFiles = $this->getODM()->getRepository('AsuBundle:FileStorage')
            ->getFilesByDocument($article);

        $form = $this->createForm(new FileStorage(), null, [
            'document_hidden_class' => $article,
            'field_name'            => 'path',
            'multi_upload'          => true
        ]);

        $fileStorageHelper = $this->get('file_storage.helper');
        $errors = $fileStorageHelper->upload($form);

        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $error->getClientMimeType();

                $this->addFlash(
                    'error',
                    sprintf(
                        'File "%s" has wrong mime-type (%s). Allowed mime-types: "%s"',
                        $error->getClientOriginalName(),
                        $error->getClientMimeType(),
                        implode(', ', $fileStorageHelper->getAllowedMimeTypes())
                    )
                );
            }
        }

        return $this->render(
            'AsuBundle:Default/article:article_files.html.twig',
            [
                'form'     => $form->createView(),
                'article'  => $article,
                'sidemenu' => $this->get('navigation.menu')->get('Side')->getMenu('article', [$article]),
                'files'    => $existsFiles
            ]
        );
    }


    /**
     * Удалить файл
     *
     * @param Article $article
     * @param $file_id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function filesRemoveAction(Article $article, $file_id)
    {
        $remove = $this->get('file_storage.helper')->remove($file_id);

        if (!$remove) {
            throw new NotFoundHttpException(
                'File which you want remove not found in database'
            );
        }

        return $this->redirectToRoute('article_files', ['id' => $article->getId() ]);
    }


    /**
     * @return \Doctrine\ODM\MongoDB\DocumentManager
     */
    public function getODM()
    {
        return $this->get('doctrine.odm.mongodb.document_manager');
    }


    /**
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function rincAction(Article $article)
    {
        return $article->createRincArchieve();
    }


    /**
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function classifsAction(Article $article)
    {
        $classifs = [
            'udk' => $this->getODM()->getRepository('AsuBundle:ClassificatorUdk')->findAll()[0],
            'grnti' => $this->getODM()->getRepository('AsuBundle:ClassificatorGrnti')->findAll()[0],
            'okso' => $this->getODM()->getRepository('AsuBundle:ClassificatorOkso')->findAll()[0],
            'bbk' => $this->getODM()->getRepository('AsuBundle:ClassificatorBbk')->findAll()[0],
            'tbk' => $this->getODM()->getRepository('AsuBundle:ClassificatorTbk')->findAll()[0],
        ];

        return $this->render('AsuBundle:Default/templates:classificators.html.twig', [
            'sidemenu' => $this->get('navigation.menu')->get('Side')->getMenu('article', [$article]),
            'entity'  => $article,
            'classifs' => $classifs,
        ]);
    }

    public function referencesDeleteAllAction(Article $article)
    {
        $allReferences = $article->getReferences();
        $article->clearReferences();
        foreach ($allReferences as $reference) {
            $this->getODM()->remove($reference);
        }

        $this->getODM()->flush();

        return $this->redirectToRoute('article_references', [
            'id'    => $article->getId()
        ]);
    }
}
