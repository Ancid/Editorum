<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 09.12.2015
 * Time: 14:57
 */
namespace Editorum\Bundle\Controller;

use Editorum\Bundle\AccessSystem\User;
use Editorum\Bundle\Logic\Publication;
use Editorum\Bundle\Logic\Model;
use Editorum\Bundle\AccessSystem\Person;
use Editorum\Bundle\Logic\Task;

class TaskController extends BasicController
{
    /**
     * Список всех задач пользователя
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function indexAction()
    {
        /**
         * @TODO Нужна механика, для точного определения интерфейса, из которого пользоатель перешел в этот,
         * пока считаем, что это интерфейс самопубликации
         */
        $tasks = Model::getByKeys(new Task(), array('reciver_id' => Person::instance()->user_id));

        return $this->render('AsuBundle:Default:tasks.html.twig', array('tasks' => $tasks));
    }

    /**
     * Управление задачами, связанными с произведениями
     * @param $publication_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function manageAction($publication_id)
    {
        $pub = Model::getById(new Publication(), $publication_id);

        $tasks = Model::getByKeys(new Task(), array(
            'object'        => 'Publication',
            'object_id'     => $publication_id
            ));

        return $this->render('AsuBundle:Default:task_manage.html.twig', array(
            'id'            => $publication_id,
            'tasks'         => $tasks,
            'sidemenu'      => $this->get('navigation.side')->pubMenu($pub),
            'active'        => 'tasks'
        ));
    }

    /**
     * Форма создания новой задачи
     * @param $publication_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction($publication_id)
    {
        $pub = Model::getById(new Publication(), $publication_id);

        if ($publication_id > 0)
            $data = Model::getById(new Task(), $publication_id);
        else
            $data = Model::create(new Task());

        $employee = Model::getAll(new User());

        return $this->render('AsuBundle:Default:task_edit.html.twig', array(
            'data'      => $data,
            'id'        => $publication_id,
            'authors'   => $pub->getAuthors(),
            'employee'  => $employee,
            'atasks'    => Task::getTaskList(Task::TYPE_AUTHOR),
            'etasks'    => Task::getTaskList(Task::TYPE_EMPLOYEE),
            'sidemenu'  => $this->get('navigation.side')->pubMenu($pub),
            'active'    => 'tasks'
        ));
    }
    
}
