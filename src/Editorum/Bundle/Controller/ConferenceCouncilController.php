<?php
namespace Editorum\Bundle\Controller;

use Doctrine\Common\Util\Inflector;
use Editorum\Bundle\Document\ConferenceCouncil;
use Editorum\Bundle\Document\Conference;
use Editorum\Bundle\Form\ConferenceCouncilType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Class ConferenceCouncilController
 * @package Editorum\Bundle\Controller
 *
 * @Security("is_granted('ROLE_CONFERENCE_GROUP')")
 */
class ConferenceCouncilController extends BasicController
{
    /**
     * @param Conference $conference
     * @param Request $request
     * @return Response
     * @ParamConverter("conference", class="Editorum\Bundle\Document\Conference")
     */
    public function listAction(Conference $conference, Request $request)
    {
        $form = $this->createForm(new ConferenceCouncilType())
            ->add('save', 'submit', [
                'label' => 'Сохранить',
                'attr' => ['class' => 'btn btn-primary']
            ]);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {
                /** @var ConferenceCouncil $council_member */
                $council_member = $form->getData();
                $council_member->setConference($conference);
                $this->getODM()->persist($council_member);
                $this->getODM()->flush();
                $this->addFlash('notice', 'Член ред.совета добавлен!');

                //Пересоздаем форму чтоб не заполнялась при успешном сабмите
                $form = $this->createForm(new ConferenceCouncilType())
                    ->add('save', 'submit', [
                        'label' => 'Сохранить',
                        'attr' => ['class' => 'btn btn-primary']
                    ]);
            } else {
                $errors = $this->get('validator')->validate($form);
                foreach ($errors as $e) {
                    $this->addFlash('error', $e->getMessage());
                }
            }
        }

        /** @var ConferenceCouncil[] $team */
        $team = $this->getODM()->getRepository('AsuBundle:ConferenceCouncil')
            ->getBySortableGroupsQueryBuilder([ 'conference' => $conference->getId() ]);

        $team = $this->get('knp_paginator')->paginate(
            $team,
            $request->get('page', 1),
            30
        );

        return $this->render('AsuBundle:Default/Council/Conference:council_list.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('conference', [$conference]),
            'form'      => $form->createView(),
            'conference' => $conference,
            'team'      => $team,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function showAction($id)
    {
        $odm = $this->get('doctrine.odm.mongodb.document_manager');

        return $this->render('AsuBundle:Default/Council:show.html.twig', [
            'member' => $odm->getRepository('AsuBundle:ConferenceCouncil')->find($id)
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function saveAction(Request $request)
    {
        $saved = $this->saveTranslatedForm(
            $this->get('asu_bundle.conference_council.form.type'),
            new ConferenceCouncil(),
            $request
        );

        if (true !== $saved) {
            $session = $this->get('session');
            $session->getFlashBag()->add('error', (string) $saved);
        }

        return $this->redirectToRoute('conference_council_list', [
            'id' => $request->get('asu_bundle_conference_council')['conference']
        ]);
    }

    /**
     * @param $string
     * @param $fieldName
     * @param Request $request
     * @return JsonResponse
     */
    public function getValueByFieldAction($string, $fieldName, Request $request)
    {
        $odm = $this->get('doctrine.odm.mongodb.document_manager');

        $qb = $odm->createQueryBuilder('AsuBundle:ConferenceCouncil');

        $meta = $odm->getRepository('AsuBundle:ConferenceCouncil')->getClassMetadata();

        $conferenceTeam = $qb
            ->find()
            ->field($fieldName)
            ->equals(new \MongoRegex('/^'.$string.'/i'))
            ->distinct('_id')
        ;

        if ($request->get('conferencelId') !== null) {
            $conferenceTeam->field('conference_id')->notEqual((int)$request->get('conferenceId'));
        }

        $conferenceTeam = $conferenceTeam->getQuery();

        $array = $conferenceTeam->getIterator()->toArray();

        $result = $qb->find()->field('_id')->in($array)->getQuery()->execute();

        $properties = $meta->reflClass->getProperties();

        $methodsArray = [];
        foreach ($properties as $property) {
            $name = $property->getName();
            $method = 'get' . Inflector::classify($name);

            $methodsArray[$name] = $method;
        }

        $resultArray = [];
        $i = 0;
        foreach ($result as $item) {
            foreach ($methodsArray as $key => $method) {
                $temporary = call_user_func([$item, $method]);
                $reference = false;
                if (in_array($key, $meta->getAssociationNames()) && null !== $temporary) {
                    $resultArray[$i][$key] = call_user_func([$temporary, 'getId']);
                    $reference = true;
                }

                if (!$reference) {
                    $resultArray[$i][$key.'_ru'] = $temporary;
                }
            }

            $translations = $this->getTranslations($item);

            foreach ($translations as $lang => $translation) {
                foreach ($translation as $key => $value) {
                    $resultArray[$i][$key.'_'.$lang] = $value;
                }
            }

            ++$i;
        }

        return new JsonResponse($resultArray);
    }

    /**
     * @Security("is_granted('ROLE_DELETE')")
     * @param $id
     * @param $conference_id
     * @return Response
     */
    public function removeAction($id, $conference_id)
    {
        $odm = $this->get('doctrine.odm.mongodb.document_manager');
        $odm->createQueryBuilder('AsuBundle:ConferenceCouncil')
            ->findAndRemove()
            ->field('_id')->equals($id)
            ->getQuery()
            ->execute()
        ;

        return new Response($this->get('router')->generate('conference_council_list', [
            'id' => $conference_id
        ]));
    }


    /**
     * @Security("is_granted('ROLE_EDIT')")
     * @param ConferenceCouncil $conferenceCouncil
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editAction(ConferenceCouncil $conferenceCouncil, Request $request)
    {
        $conference = $conferenceCouncil->getConference();
        $form = $this->createForm(new ConferenceCouncilType(), $conferenceCouncil)
            ->add('save', 'submit', [
                'label' => 'Сохранить',
                'attr' => ['class' => 'btn btn-primary']
            ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getODM()->flush($conferenceCouncil);

            return $this->redirectToRoute('conference_council_list', [
                'id' => $conference->getId()
            ]);
        }

        return $this->render('AsuBundle:Default/Council/Conference:council_edit.html.twig', [
            'sidemenu'  => $this->get('navigation.menu')->get('Side')->getMenu('conference', [$conference]),
            'form' => $form->createView(),
        ]);
    }
}
