<?php
namespace Editorum\Bundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Util\Inflector;
use Doctrine\ODM\MongoDB\PersistentCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Class SortableController
 * @package Editorum\Bundle\Controller
 *
 * @Security("is_granted('ROLE_USER')")
 */
class SortableController extends BasicController
{
    /**
     * @param Request $request
     * @param $bundleName
     * @param $documentId
     * @param $propertyName
     * @param string $moveTo
     * @return Response
     */
    public function upDownEditorialAction(Request $request, $bundleName, $documentId, $propertyName, $moveTo = 'up')
    {
        $documentName = null === $request->get('documentName') ? 'EditorialCouncil' : $request->get('documentName');
        $odm = $this->get('doctrine.odm.mongodb.document_manager');

        $document = $odm->find($bundleName.':'.$documentName, $documentId);
        $maxElements = $odm->getRepository($bundleName . ':' . $documentName)
            ->createQueryBuilder()
            ->field('journal_id')->equals($document->getJournal()->getId())
            ->getQuery()->getIterator()->count();

        $method = 'set' . ucfirst($propertyName);

        $oldPosition = call_user_func([$document, 'get'.ucfirst($propertyName) ]);

        if ($moveTo != 'up') {
            $newPosition = $oldPosition - 1;
            $newPosition = $newPosition < 0 ? 0 : $newPosition;
        } else {
            $newPosition = $oldPosition + 1;
            $newPosition = $newPosition > $maxElements-1 ? $maxElements-1 : $newPosition;
        }

        call_user_func([ $document, $method ], $newPosition);
        $odm->flush($document);

        return new Response($newPosition);
    }

    /**
     * @param $bundleName
     * @param $documentId
     * @param $propertyName
     * @param string $moveTo
     * @return Response
     */
    public function upDownConferenceAction($bundleName, $documentId, $propertyName, $moveTo = 'up')
    {
        $documentName = 'ConferenceCouncil';
        $odm = $this->get('doctrine.odm.mongodb.document_manager');

        $document = $odm->find($bundleName.':'.$documentName, $documentId);
        $maxElements = $odm->getRepository($bundleName . ':' . $documentName)
            ->createQueryBuilder()
            ->field('conference_id')->equals($document->getConference()->getId())
            ->getQuery()->getIterator()->count();

        $method = 'set' . ucfirst($propertyName);

        $oldPosition = call_user_func([$document, 'get'.ucfirst($propertyName) ]);

        if ($moveTo != 'up') {
            $newPosition = $oldPosition - 1;
            $newPosition = $newPosition < 0 ? 0 : $newPosition;
        } else {
            $newPosition = $oldPosition + 1;
            $newPosition = $newPosition > $maxElements-1 ? $maxElements-1 : $newPosition;
        }

        call_user_func([ $document, $method ], $newPosition);
        $odm->flush($document);

        return new Response($newPosition);
    }

    /**
     * Positioning collection element
     *
     * @param int    $docId
     * @param int    $newPos
     * @param int    $oldPos
     * @param string $docName
     * @param string $property
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function positionCollectionAction($docId, $newPos, $oldPos, $docName, $property)
    {
        $response = $this->redirectToRoute(strtolower($docName) . '_' . $property, ['id' => $docId]);

        // Если позиция не меняется, ничего не делаем
        if ($oldPos === $newPos) {
            $this->addFlash('info', 'Позиция автора не может быть изменена!');
            return $response;
        }

        $entity = $this->getODM()->getRepository('AsuBundle:'.$docName)->find($docId);
//        $entity = $this->getODM()->getRepository('AsuBundle:'.ucfirst($docName))->find($docId);

        if (null === $entity) {
            $this->addFlash('error', 'Документ не найден!');
            return $response;
        }

        $accessor = new PropertyAccessor();
        $collection = $accessor->getValue($entity, $property);

        // Делаем новую сортировку
        $newCollection = [];
        foreach ($collection as $key => $item) {
            if (($key < $newPos || $key > $newPos) && $key != $oldPos) {
                $newCollection[] = $item;
            }
            if ($key == $newPos) {
                $newCollection[] = $collection[ $oldPos ];
                $newCollection[] = $item;
            }
        }

        // Если новая позиция выше количества элементов, то помещаем в конец списка
        if ($newPos >= $collection->count()) {
            $newCollection[] = $collection[ $oldPos ];
        }

        // Чистим память
        unset($collection);

        // Если метод существует, то используем его для очистки списка коллекции
        // IMPORTANT: если этого метода не существует, сортировка не запишется в базу!!!
        if (method_exists($entity, 'clear'. Inflector::classify($property))) {
            $entity->{'clear' . Inflector::classify($property)}();
        }

        // Записываем новый отсортированный список
        $accessor->setValue($entity, $property, $newCollection);

        $this->getODM()->persist($entity);
        $this->getODM()->flush();

        $this->addFlash('info', 'Позиция изменена!');

        return $response;
    }
}
