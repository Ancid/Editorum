<?php

namespace Editorum\Bundle\Controller;

use Editorum\Bundle\Document\Doi;
use Editorum\Bundle\Document\Organization;
use Symfony\Component\HttpFoundation\Request;

class DoiController extends BasicController
{

    /**
     * @param Request $request
     * @return bool
     */
    public function updateAction(Request $request)
    {
        $doi = $this->getODM()->getRepository('AsuBundle:Doi')->findOneBy([
            'doi_batch_id' => $request->headers->get('CROSSREF-EXTERNAL-ID')
        ]);
        $doi->setStatus(Doi::ST_PROCESSED);

        return true;
    }


    /**
     * @param string $type
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function doiListAction($type, $id, Request $request)
    {
        $low_type = strtolower($type);
        $document = $this->getODM()->getRepository('AsuBundle:'.$type)->find($id);
        $doi_list =  $this->getODM()->getRepository('AsuBundle:Doi')->findByDocument($document);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $doi_list,
            $request->get('page', 1),
            30
        );

        return $this->render('AsuBundle:Default/templates:doi.html.twig', [
            'sidemenu'   => $this->get('navigation.menu')->get('Side')->getMenu($low_type, [$document]),
            'pagination' => $pagination,
            'document_type' => $type,
            'document'  => $document,
            'kinds'      => Doi::getKinds(),
        ]);

    }


    /**
     * @param string $type
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function doiCreateAction($type, $id)
    {
        $low_type = strtolower($type);
        $document = $this->getODM()->getRepository('AsuBundle:'.$type)->find($id);
        //Проверка лимита запросов
        /** @var Organization $org */
        $org = $document->getPublisher()->getOrganization();
        if ($org->getDoiCurrent() >= $org->getDoiLimit()) {
            $this->addFlash('error', 'Превышен лимит запросов DOI');

            return $this->redirectToRoute('document_doi_list', [
                'type' => $type,
                'id' => $document->getId()
            ]);
        }

        $doi = $this->getODM()->getRepository('AsuBundle:Doi')->findOneBy([
            'document' => $document,
            'status'   => Doi::ST_QUEUE
        ]);

        if (empty($doi)) {
            $doi = new Doi();
            $doi->setDocument($document);
            $org->incrementDoiLimit();
            $this->getODM()->persist($doi);
        }

        //Инициализируем DOI объект
        try {
            $doi_object = $this->get('doi.factory')->build($document);
            $xml = $doi_object->generateXml();
            $doi_batch_id = $doi_object->doi_batch_id;
            $doi->setXml($xml);
            $doi->setDoiBatchId($doi_batch_id);
            $this->getODM()->flush();
        } catch (\Exception $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('document_doi_list', [
            'type' => $type,
            'id' => $document->getId()
        ]);
    }


    /**
     * @param Doi $doi
     * @param string $type
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function doiRemoveAction(Doi $doi, $type)
    {
        $doc_id = $doi->getDocument()->getId();
        $this->getODM()->remove($doi);
        $this->getODM()->flush();

        return $this->redirectToRoute('document_doi_list', [
            'type' => $type,
            'id' => $doc_id
        ]);
    }
}
