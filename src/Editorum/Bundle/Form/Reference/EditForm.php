<?php

namespace Editorum\Bundle\Form\Reference;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('position', 'text', [
//                'label' => 'form.editorum.document.reference.edit.position'
//            ])
            ->add(
                $builder->create('ru', 'form')
                    ->add('text', 'textarea', [
                        'label' => 'form.editorum.document.reference.edit.text_ru',
                        'attr'  => [
                            'rows' => 8
                        ]
                    ])
            )
            ->add(
                $builder->create('en', 'form')
                    ->add('text', 'textarea', [
                        'label' => 'form.editorum.document.reference.edit.text_en',
                        'attr'  => [
                            'rows' => 8
                        ]
                    ])
            )
            ->add('save', 'submit', [
                'label' => 'form.editorum.document.reference.save',
                'attr' => [
                    'class' => 'btn-success'
                ]
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     * @return OptionsResolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'method' => 'PUT',
            'data_class' => 'Editorum\Bundle\Document\Reference'
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'asu_bundle_reference_edit_form';
    }
}
