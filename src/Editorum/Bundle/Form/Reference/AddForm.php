<?php

namespace Editorum\Bundle\Form\Reference;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AddForm
 * @package Editorum\Bundle\Form
 */
class AddForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $form
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $form, array $options)
    {
        $form
            ->add('list_ru', 'textarea', [
                'label' => 'form.editorum.document.reference.add.list_ru',
                'required' => false,
                'attr' => [
                    'rows' => 8
                ]
            ])
            ->add('list_en', 'textarea', [
                'label' => 'form.editorum.document.reference.add.list_en',
                'required' => false,
                'attr' => [
                    'rows' => 8
                ]
            ])
            ->add('save', 'submit', [
                'label' => 'form.editorum.document.reference.save',
                'attr' => [
                    'class' => 'btn-success'
                ]
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     * @return OptionsResolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'method' => 'POST',
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'asu_bundle_reference_add_form';
    }
}
