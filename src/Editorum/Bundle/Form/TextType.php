<?php

namespace Editorum\Bundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TextType extends AbstractType
{
    public function buildForm(FormBuilderInterface $form, array $options)
    {
        $form
            ->add('text', 'textarea', [
                'label' => 'Text',
                'attr'  => ['rows' => 5]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'method'     => 'PUT',
        ]);
    }

    public function getName()
    {
        return 'asu_bundle_publication_text_form';
    }
}
