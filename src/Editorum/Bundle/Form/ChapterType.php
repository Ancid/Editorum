<?php
namespace Editorum\Bundle\Form;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChapterType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                $builder->create('ru', 'form')
                    ->add('title', 'text', [
                        'label' => 'form.editorum.document.chapter.title',
                        'attr' => [
                            'placeholder' => 'form.editorum.document.chapter.placeholder'
                        ]
                    ])
            )
        ;

        if (null != $options['publication']) {
            $builder->add('authors', 'document', [
                'class' => 'Editorum\Bundle\Document\Author',
                'placeholder'   => 'form.select.author',
                'choice_label'  => "ru[fio]",
                'label'         => 'form.editorum.document.chapter.authors',
                'query_builder' => function (DocumentRepository $repository) use ($options) {
                    $qb = $repository->createQueryBuilder();
                    $authors = $options['publication']->getAuthors();

                    $ids = [];
                    foreach ($authors as $author) {
                        $ids[] = $author->getId();
                    }
                    $qb->field('_id')->in($ids);

                    return $qb;
                },
                'multiple'  => true,
            ]);
        }
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class' => 'Editorum\Bundle\Document\Chapter',
            'publication'  => null,
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'chapter';
    }
}
