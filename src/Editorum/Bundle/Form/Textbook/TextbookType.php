<?php
namespace Editorum\Bundle\Form\Textbook;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Editorum\Bundle\Document\Common\AbstractPublication;
use Editorum\Bundle\Document\Textbook;
use SecurityBundle\Document\AbstractUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class TextbookType extends AbstractType
{
    /** @var AbstractUser */
    private $user;

    /** @var AuthorizationCheckerInterface */
    private $security;

    /** @var Textbook */
    private $textbook;


    /**
     * MonographyType constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param AuthorizationCheckerInterface $security
     */
    public function __construct(TokenStorageInterface $tokenStorage, AuthorizationCheckerInterface $security)
    {
        $this->user = $tokenStorage->getToken()->getUser();
        $this->security = $security;
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->textbook = $builder->getData();
        $statusform =  $builder->create('statuses', 'form');

        if ($this->textbook->getStatuses()['status'] !== AbstractPublication::ST_IS_PUBLISHED) {
            $builder
                ->add('languages', 'choice', [
                    'label'             => 'form.editorum.document.textbook.languages',
                    'label_attr'        => [
                        'help'          => 'form.editorum.document.textbook.help.languages'
                    ],
                    'choices_as_values' => true,
                    'choices'           => [
                        AbstractPublication::LANG_RU => AbstractPublication::LANG_RU,
                        AbstractPublication::LANG_EN => AbstractPublication::LANG_EN,
                    ],
                    'multiple'          => true,
                ])
                ->add('previously_published', 'date', [
                    'widget' => 'single_text',
                    'label' => 'form.editorum.document.issue.previously_published',
                    'required' => false,
                ])
                ->add(
                    $builder->create('ru', 'form')
                        ->add('title', 'text', [
                            'label' => 'form.editorum.document.textbook.title',
                        ])
                        ->add('annotation', 'textarea', [
                            'label'       => 'form.editorum.document.textbook.annotation',
                            'attr'        => ['rows' => 5],
                            'constraints' => [
                                new Length(['max' => 500]),
                            ],'required' => false,
                        ])
                        ->add('keywords', 'text', [
                            'label'       => 'form.editorum.document.textbook.keywords',
                            'constraints' => [
                                new Length(['max' => 500]),
                            ],'required' => false,
                        ])
                )
                ->add(
                    $builder->create('en', 'form')
                        ->add('title', 'text', [
                            'label' => 'form.editorum.document.textbook.title',
                        ])
                        ->add('annotation', 'textarea', [
                            'label'       => 'form.editorum.document.textbook.annotation',
                            'attr'        => ['rows' => 5],
                            'constraints' => [
                                new Length(['max' => 500]),
                            ],'required' => false,
                        ])
                        ->add('keywords', 'text', [
                            'label'       => 'form.editorum.document.textbook.keywords',
                            'constraints' => [
                                new Length(['max' => 500]),
                            ],'required' => false,
                        ])
                )
                ->add('page_total', 'integer', [
                    'label' => 'form.editorum.document.textbook.page_total',
                    'attr'  => [
                        'min' => 0,
                    ],'required' => false,
                ])
                ->add('author_pages', 'integer', [
                    'label' => 'form.editorum.document.textbook.author_pages',
                    'attr'  => [
                        'min' => 0,
                    ],'required' => false,
                ])
                ->add('isbn', 'text', [
                    'label' => 'form.editorum.document.textbook.isbn'
                ])
                ->add('isbn_online', 'text', [
                    'label' => 'form.editorum.document.textbook.isbn_online',
                    'required' => false,
                ])
                ->add('cover', 'file', [
                    'required' => false,
                    'mapped'   => false,
                ])
                ->add('publication_number', 'integer', [
                    'label' => 'form.editorum.document.textbook.publication_number',
                    'attr' => [
                        'min' => 0,
                    ],'required' => false,
                ])
                ->add('comment', 'textarea', [
                    'label' => 'form.editorum.document.textbook.comment',
                    'required' => false,
                ])
            ;
            $statusform
                ->add('has_published', 'choice_button', [
                    'label'   => 'form.editorum.document.publication.statuses.has_published',
                    'choices' => ['form.choice.no', 'form.choice.yes'],
                    'required' => false,
                ])
                ->add('has_agreement', 'choice_button', [
                    'label'   => 'form.editorum.document.publication.statuses.has_agreement',
                    'choices' => ['form.choice.no', 'form.choice.yes'],
                    'required' => false,
                ])
                ->add('has_grif', 'choice_button', [
                    'label'   => 'form.editorum.document.publication.statuses.has_grif',
                    'choices' => ['form.choice.no', 'form.choice.yes'],
                    'required' => false,
                ])
            ;
            if ($this->security->isGranted('ROLE_ADMIN')) {
                $builder
                    ->add('publisher', 'document', [
                        'label' => 'form.editorum.document.textbook.publisher',
                        'class'         => 'Editorum\Bundle\Document\Publisher',
                        'choice_label'  => 'ru[title]',
                        'choices'   => $ar = $this->user->getCorporate()->getOrganization()->getCorporates()->toArray(),
                    ]);
            }
        }

        $statusform
            ->add('is_show_nauka', 'choice_button', [
                'label'   => 'form.editorum.document.publication.statuses.is_show_nauka',
                'choices' => ['form.choice.no', 'form.choice.yes'],
            ])
            ->add('is_opened', 'choice_button', [
                'label'   => 'form.editorum.document.publication.statuses.is_opened',
                'choices' => ['form.choice.closed', 'form.choice.opened'],
            ])
        ;

        $builder
            ->add('rinc_url', 'text', [
                'label' => 'form.editorum.document.common.rinc_url',
                'required' => false,
            ])
            ->add('doi_url', 'text', [
                'label' => 'form.editorum.document.common.doi_url',
                'required' => false,
            ])
            ->add($statusform);

    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class' => 'Editorum\Bundle\Document\Textbook',
            'is_new'     => true,
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'textbook';
    }
}
