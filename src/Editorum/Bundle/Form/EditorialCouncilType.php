<?php

namespace Editorum\Bundle\Form;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditorialCouncilType extends AbstractLanguageFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                $builder->create('ru', 'form', [ 'label_attr' => [ 'class' => 'hide' ]])
                    ->add('city', 'text', [
                        'label' => 'form.editorum.document.council.city',
                        'required' => false,
                    ])
                    ->add('fio', 'text', [
                        'label' => 'form.editorum.document.council.fio',
                        'required' => false,
                    ])
                    ->add('position', 'text', [
                        'label' => 'form.editorum.document.council.position',
                        'required' => false,
                    ])
                    ->add('workPlace', 'text', [
                        'label' => 'form.editorum.document.council.workPlace',
                        'required' => false,
                    ])
                    ->add('workPosition', 'text', [
                        'label' => 'form.editorum.document.council.workPosition',
                        'required' => false,
                    ])
                    ->add('scienceLevel', 'text', [
                        'label' => 'form.editorum.document.council.scienceLevel',
                        'required' => false,
                    ])
            )
            ->add(
                $builder->create('en', 'form', [ 'label_attr' => [ 'class' => 'hide' ]])
                    ->add('city', 'text', [
                        'label' => 'form.editorum.document.council.city',
                        'required' => false,
                    ])
                    ->add('fio', 'text', [
                        'label' => 'form.editorum.document.council.fio',
                        'required' => false,
                    ])
                    ->add('position', 'text', [
                        'label' => 'form.editorum.document.council.position',
                        'required' => false,
                    ])
                    ->add('workPlace', 'text', [
                        'label' => 'form.editorum.document.council.workPlace',
                        'required' => false,
                    ])
                    ->add('workPosition', 'text', [
                        'label' => 'form.editorum.document.council.workPosition',
                        'required' => false,
                    ])
                    ->add('scienceLevel', 'text', [
                        'label' => 'form.editorum.document.council.scienceLevel',
                        'required' => false,
                    ])
            )
            ->add('country', 'document', [
                'class' => 'Editorum\Bundle\Document\Country',
                'choice_label' => 'ru[title]',
                'placeholder' => 'form.select.country',
                'query_builder' => function (DocumentRepository $repository) {
                    return $repository->createQueryBuilder()->sort('position', 'asc');
                },
                'label' => 'form.editorum.document.council.country',
                'attr' => [
                    'class' => 'col-md-6',
                    'data-live-search' => 'true',
                    'data-size' => '15',
                ],
                'required' => false,
            ])->add('region', 'document', [
                'class' => 'Editorum\Bundle\Document\Region',
                'choice_label' => 'ru[title]',
                'placeholder' => 'form.select.region',
                'query_builder' => function (DocumentRepository $repository) {
                    return $repository->createQueryBuilder()->sort('position', 'asc');
                },
                'label' => 'form.editorum.document.council.region',
                'attr' => [
                    'class' => 'col-md-6',
                    'data-live-search' => 'true',
                    'data-size' => '15',
                ],
                'required' => false,
            ])
            ->add('order', 'integer', [
                'label' => 'form.editorum.document.council.order',
                'required' => false,

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'method' => 'POST',
            'csrf_protection' => false,
            'data_class'    => 'Editorum\Bundle\Document\EditorialCouncil'
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'asu_bundle_editorial_council';
    }
}
