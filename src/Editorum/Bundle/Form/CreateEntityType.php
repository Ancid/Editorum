<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 25.04.2016
 * Time: 16:26
 */

namespace Editorum\Bundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CreateEntityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', [

            ])
            ->add('entity_type', 'choice', [
                'choices'  => [
                    'journal'       => 'Журнал',
                    'conferences'   => 'Конференцию',
                    'collection'    => 'Сборник',
                ],
            ])
            ->add('create', 'submit');
    }

    public function getName()
    {
        return 'create_entity_form';
    }
}