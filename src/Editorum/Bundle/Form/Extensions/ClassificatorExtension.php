<?php
namespace Editorum\Bundle\Form\Extensions;

use Symfony\Component\Form\AbstractTypeExtension;

class ClassificatorExtension extends AbstractTypeExtension
{
    public function getExtendedType()
    {
        return 'select_classif';
    }
}
