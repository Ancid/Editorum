<?php
namespace Editorum\Bundle\Form\Extensions;

use Symfony\Component\Form\AbstractTypeExtension;

class ChoiceButtonExtension extends AbstractTypeExtension
{
    public function getExtendedType()
    {
        return 'choice_button';
    }
}
