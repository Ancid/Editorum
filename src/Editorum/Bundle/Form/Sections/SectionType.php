<?php
namespace Editorum\Bundle\Form\Sections;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SectionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                $builder->create('ru', 'form')
                    ->add('title', null, [
                        'label' => 'form.editorum.document.section.title'
                    ])
            )
            ->add(
                $builder->create('en', 'form')
                    ->add('title', null, [
                        'label' => 'form.editorum.document.section.title'
                    ])
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(['data_class' => 'Editorum\Bundle\Document\Section']);
    }

    public function getName()
    {
        return 'asu_form_edit_section';
    }

}