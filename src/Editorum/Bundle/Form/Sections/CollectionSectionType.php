<?php
namespace Editorum\Bundle\Form\Sections;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class CollectionSectionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = $builder->getData();

        if ($options['new']) {
            $builder
                ->add('collection_section_list', 'document', [
                    'class' => 'Editorum\Bundle\Document\Section',
                    'choice_label' => 'ru[title]',
                    'attr' => ['class' => 'show-tick'],
                    'label' => 'form.editorum.document.collection.section.section',
                    'query_builder' => function (DocumentRepository $repository) use ($data) {
                        $qb = $repository->createQueryBuilder();

                        $ids = [];
                        if (!empty($data->getConference())) {
                            foreach ($data->getConference()->getSections() as $section) {
                                $ids[] = $section->getId();
                            }
                        }

                        $nin = [];
                        foreach ($data->getCollectionSectionList() as $sectionlist) {
                            $nin[] = $sectionlist->getSection()->getId();
                        }

                        return $qb
                            ->addAnd($qb->expr()->field('_id')->in($ids))
                            ->addAnd($qb->expr()->field('_id')->notIn($nin));
                    },
                    'mapped' => false,
                    'constraints' => [
                        new NotNull(),
                        new NotBlank(),
                    ]
                ]);
        }

        $builder
            ->add('order', 'integer', [
                'required'      => false,
                'mapped'        => false,
                'label'         => 'form.editorum.document.collection.section.order',
                'data'          => !$options['new'] ? $data['order'] : null,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'new'   => false
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'collection_section';
    }
}
