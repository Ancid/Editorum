<?php
namespace Editorum\Bundle\Form\Rubric;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class IssueRubricType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = $builder->getData();

        if ($options['new']) {
            $builder
                ->add('issue_rubric_list', 'document', [
                    'class' => 'Editorum\Bundle\Document\Rubric',
                    'choice_label' => 'ru[title]',
                    'attr' => ['class' => 'show-tick'],
                    'label' => 'form.editorum.document.issue.rubric.rubric',
                    'query_builder' => function (DocumentRepository $repository) use ($data) {
                        $qb = $repository->createQueryBuilder();

                        $ids = [];
                        if (!empty($data->getJournal())) {
                            foreach ($data->getJournal()->getRubrics() as $rubric) {
                                $ids[] = $rubric->getId();
                            }
                        }

                        $nin = [];
                        foreach ($data->getIssueRubricList() as $rubriclist) {
                            $nin[] = $rubriclist->getRubric()->getId();
                        }

                        return $qb
                            ->addAnd($qb->expr()->field('_id')->in($ids))
                            ->addAnd($qb->expr()->field('_id')->notIn($nin));
                    },
                    'mapped' => false,
                    'constraints' => [
                        new NotNull(),
                        new NotBlank(),
                    ]
                ]);
        }

        $builder
            ->add('order', 'integer', [
                'required'      => false,
                'mapped'        => false,
                'label'         => 'form.editorum.document.issue.rubric.order',
                'data'          => !$options['new'] ? $data['order'] : null,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'new'   => false
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'issue_rubric';
    }
}
