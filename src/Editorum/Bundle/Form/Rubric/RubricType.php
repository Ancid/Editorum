<?php
namespace Editorum\Bundle\Form\Rubric;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RubricType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options = [])
    {
        $builder
            ->add(
                $builder->create('ru', 'form')
                    ->add('title', null, [
                        'label' => 'form.editorum.document.rubric.title'
                    ])
            )
            ->add(
                $builder->create('en', 'form')
                    ->add('title', null, [
                        'label' => 'form.editorum.document.rubric.title'
                    ])
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class'    => 'Editorum\Bundle\Document\Rubric'
        ]);
    }

    public function getName()
    {
        return 'rubric';
    }
}
