<?php
namespace Editorum\Bundle\Form\Corporate;

use Symfony\Component\OptionsResolver\OptionsResolver;

class ResearchType extends AbstractCorporateType
{
    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class' => 'Editorum\Bundle\Document\Research',
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'research_form';
    }
}
