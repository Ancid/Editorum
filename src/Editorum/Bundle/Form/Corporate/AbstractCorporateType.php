<?php
namespace Editorum\Bundle\Form\Corporate;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractCorporateType extends AbstractType
{
    protected $corporate = false;

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->corporate = $builder->getData();
        $builder
            ->add(
                $builder->create('ru', 'form')
                    ->add('title', 'text', [
                        'label' => 'form.editorum.document.corporate.title',
                    ])
            )
            ->add(
                $builder->create('en', 'form')
                    ->add('title', 'text', [
                        'label' => 'form.editorum.document.corporate.title',
                    ])
            )
            ->add(
                $builder->create('statuses', 'form')
                    ->add('editorum_prefix', 'choice_button', [
                        'choices'   => [
                            'form.editorum.document.corporate.button.no',
                            'form.editorum.document.corporate.button.yes',
                        ],
                        'label' => 'form.editorum.document.corporate.editorum_prefix',
                    ])
            )
            ->add('email', 'text', [
                'label' => 'form.editorum.document.corporate.email',
            ])
            ->add('doi_prefix', 'text', [
                'label' => 'form.editorum.document.corporate.doi_prefix',
            ])
            ->add('doi_login', 'text', [
                'label' => 'form.editorum.document.corporate.doi_login',
            ])
            ->add('doi_pass', 'password', [
                'label' => 'form.editorum.document.corporate.doi_pass',
            ])
        ;
        if (method_exists($this->corporate, 'setRincTitle')) {
            $builder
                ->add('rinc_id', 'integer', [
                    'label' => 'form.editorum.document.corporate.rinc_id'
                ])
                ->add('rinc_title', 'text', [
                    'label' => 'form.editorum.document.corporate.rinc_title'
                ])
            ;
        }
    }
}
