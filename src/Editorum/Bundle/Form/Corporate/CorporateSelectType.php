<?php
namespace Editorum\Bundle\Form\Corporate;

use Editorum\Bundle\Document\Common\AbstractCorporateEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CorporateSelectType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('kind', 'choice', [
                'label'             => 'form.editorum.document.corporate.type',
                'choices'           => AbstractCorporateEntity::getKinds(),
//                'mapped'   => false,
            ])
        ;
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'corporate_type_form';
    }
}
