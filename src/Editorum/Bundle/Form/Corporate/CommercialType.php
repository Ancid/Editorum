<?php
namespace Editorum\Bundle\Form\Corporate;

use Symfony\Component\OptionsResolver\OptionsResolver;

class CommercialType extends AbstractCorporateType
{
    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class' => 'Editorum\Bundle\Document\Commercial',
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'commercial_form';
    }
}
