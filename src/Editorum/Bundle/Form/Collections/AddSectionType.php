<?php
namespace Editorum\Bundle\Form\Collections;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddSectionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->data = $builder->getData();

//        dump($this->data);die();

        $builder->add(
            'sections',
            'document',
            [
                'class'         => 'Editorum\Bundle\Document\Section',
                'choice_label'  => 'title',
                'query_builder' => function (DocumentRepository $repository) {
                    return $repository->createQueryBuilder()->field('conference_id')
                        ->equals(
                            $this->data->getConference()->getId()
                        )
                        ->field('_id')
                        ->notIn($this->data->getSectionsArray());
                },
                'attr'          => [
                    'class'     => 'col-md-6',
                    'data-size' => '15',
                ],
                'multiple' => true,
        ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(['data_class' => 'Editorum\Bundle\Document\Collection']);
    }

    public function getName()
    {
        return 'asu_form_collection_add_sections';
    }

}