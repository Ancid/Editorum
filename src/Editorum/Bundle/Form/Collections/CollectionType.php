<?php
namespace Editorum\Bundle\Form\Collections;

use Editorum\Bundle\Document\Collection;
use SecurityBundle\Document\AbstractUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Editorum\Bundle\Document\Collection as CollectionDocument;
use Editorum\Bundle\Document\Conference as ConferenceDocument;

class CollectionType extends AbstractType
{
    /** @var  CollectionDocument $collection */
    private $collection;

    /** @var AbstractUser */
    private $user;

    /** @var AuthorizationCheckerInterface */
    private $security;


    /**
     * MonographyType constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param AuthorizationCheckerInterface $security
     */
    public function __construct(TokenStorageInterface $tokenStorage, AuthorizationCheckerInterface $security)
    {
        $this->user = $tokenStorage->getToken()->getUser();
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->collection = $builder->getData();
        $statusform =  $builder->create('statuses', 'form');

        if ($this->collection->getStatuses()['status'] !== ConferenceDocument::ST_IS_PUBLISHED) {
            $builder
                ->add(
                    $builder->create('ru', 'form', [])
                        ->add('title', 'text', [
                            'label' => 'Название',
                            'attr' => ['class' => 'form-control']
                        ])
                        ->add('annotation', 'textarea', [
                            'label' => 'Аннотация',
                            'attr' => [
                                'class' => 'form-control',
                                'rows' => 5
                            ]
                        ])
                        ->add('keywords', 'textarea', [
                            'label' => 'Ключевые слова',
                            'attr' => ['class' => 'form-control']
                        ])
                )
                ->add(
                    $builder->create('en', 'form', [])
                        ->add('title', 'text', [
                            'label' => 'Название на английском',
                            'attr' => ['class' => 'form-control']
                        ])
                        ->add('annotation', 'textarea', [
                            'label' => 'Аннотация на английском',
                            'attr' => ['class' => 'form-control', 'rows' => 5]
                        ])
                        ->add('keywords', 'textarea', [
                            'label' => 'Ключевые слова на английском',
                            'attr' => ['class' => 'form-control']
                        ])
                )
                ->add('date_reg_certificate', 'date', [
                    'widget' => 'single_text',
                    'label' => 'Дата выдачи сертификата для сборника',
                ])
                ->add('author_pages', 'integer', [
                    'label' => 'Авторских листов',
                    'attr' => ['class' => 'form-control']
                ])
                ->add('published_page_total', 'integer', [
                    'label' => 'Всего страниц',
                    'attr' => ['class' => 'form-control']
                ])
                ->add('isbn', 'text', [
                    'label' => 'ISBN печатной версии',
                    'attr' => ['class' => 'form-control']
                ])
                ->add('isbn_online', 'text', [
                    'label' => 'ISBN электронной версии',
                    'attr' => ['class' => 'form-control']
                ])
                ->add('cover', 'file', [
                    'label'    => 'form.editorum.document.collection.cover',
                    'required' => false,
                    'mapped'   => false,
                ])
            ;
        }
        $statusform
            ->add('is_opened', 'choice_button', [
                'choices' => ['Нет', 'Да'],
                'label' => 'form.editorum.document.collection.statuses.is_opened',
            ])
            ->add('is_show_nauka', 'choice_button', [
                'choices'   => [ 'Нет', 'Да' ],
                'label' => 'form.editorum.document.collection.statuses.is_show_nauka',
            ])
            ->add('in_rinc', 'choice_button', [
                'choices'   => [ 'Нет', 'Да' ],
                'label' => 'form.editorum.document.collection.statuses.in_rinc',
            ])
            ->add('in_vak', 'choice_button', [
                'choices'   => [ 'Нет', 'Да' ],
                'label' => 'form.editorum.document.collection.statuses.in_vak',
            ]);
        $builder
            ->add('rinc_url', 'text', [
                'label' => 'form.editorum.document.common.rinc_url',
                'required' => false,
            ])
            ->add('doi_url', 'text', [
                'label' => 'form.editorum.document.common.doi_url',
                'required' => false,
            ])
            ->add($statusform);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(['data_class' => 'Editorum\Bundle\Document\Collection']);
    }

    public function getName()
    {
        return 'asu_form_collection_create';
    }

}