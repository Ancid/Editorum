<?php

namespace Editorum\Bundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FileStorage extends AbstractType
{
    public function buildForm(FormBuilderInterface $form, array $options)
    {
        if (!isset($options['field_label'])) {
            $options['field_label'] = $options['field_name'];
        }

        $form->add($options['field_name'], 'file', [
            'multiple' => $options['multi_upload'],
            'label'    => $options['field_label'],
            'required' => false
        ]);

        if (null !== $options['document_hidden_class']) {
            $form->add('document', 'document_hidden', [
                'class' => get_class($options['document_hidden_class']),
                'data_class' => null,
                'data' => $options['document_hidden_class']
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'method' => 'POST',
            'document_hidden_class' => null,
            'field_name' => 'file',
            'field_label' => null,
            'multi_upload' => true,
        ]);
    }

    public function getName()
    {
        return 'asu_bundle_file_storage';
    }
}
