<?php
namespace Editorum\Bundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

abstract class AbstractLanguageFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param string $title
     * @param string $fieldType
     * @param string $translationPrefix
     * @param array $langs
     * @return FormBuilderInterface
     */
    final public function createMultiLangFields(
        FormBuilderInterface $builder,
        $title,
        $fieldType,
        $translationPrefix = 'form.editorum.document',
        array $langs = [ 'ru' => true, 'en' => false ]
    ) {
        $form = $builder->create($title, 'form', [
            'by_reference' => false,
            'label' => $translationPrefix . '.' . $title
        ]);
        foreach ($langs as $lang => $required) {
            $form->add($lang, $fieldType, [
                'label' => $translationPrefix . '.lang.' . $lang,
                'required' => $required
            ]);
        }

        return $form;
    }
}
