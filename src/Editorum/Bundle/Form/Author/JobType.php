<?php
namespace Editorum\Bundle\Form\Author;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class JobType
 * Метаформа для выбора места работы автора
 *
 * @package Editorum\Bundle\Form\Author
 */
class JobType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_exists', 'choice_button', [
                'choices' => ['form.choice.from_hand', 'form.choice.from_list'],
                'required' => false,
            ])
            ->add('is_vuz', 'choice_button', [
                'label' => 'form.editorum.author.job.is_vuz',
                'choices' => ['form.choice.no', 'form.choice.yes'],
                'required' => false,
            ])
            ->add('vuz', 'document', [
                'label' => 'form.editorum.author.job.vuz',
                'class' => 'Editorum\Bundle\Document\University',
                'choice_label' => 'ru[title]',
                'required' => false,
            ])
            ->add('corporate', 'document', [
                'label' => 'form.editorum.author.job.corporate',
                'class' => 'Editorum\Bundle\Document\Publisher',
                'choice_label' => 'ru[title]',
                'required' => false,
            ])
            ->add(
                $builder->create('hand', 'form')
                    ->add('title', null, [
                        'label' => 'form.editorum.author.job.hand.title',
                        'required' => false,
                    ])
                    ->add(
                        $builder->create('city', 'form')
                            ->add('ru', null, [
                                'label'    => 'form.editorum.author.job.hand.city',
                                'required' => false,
                            ])
                            ->add('en', null, [
                                'label'    => 'form.editorum.author.job.hand.city',
                                'required' => false,
                            ])
                    )
                    ->add('country', 'document', [
                        'label' => 'form.editorum.author.job.hand.country',
                        'class' => 'Editorum\Bundle\Document\Country',
                        'choice_label'  => 'ru[title]',
                        'query_builder' => function (DocumentRepository $repository) {
                            return $repository->createQueryBuilder()->sort('position', 'asc');
                        },
                        'required' => false,
                    ])
            )
            ->add('position', null, [
                'label' => 'form.editorum.author.job.position',
                'required' => false,
                'label_attr'     => [
                    'help'  => 'form.editorum.author.job.help.position'
                ],
            ])
            ->add('department', null, [
                'label' => 'form.editorum.author.job.department',
                'required' => false,
                'label_attr' => [
                    'help' => 'form.editorum.author.job.help.department'
                ],
            ])
        ;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'author_job';
    }
}
