<?php
namespace Editorum\Bundle\Form\Author;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class DegreeType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('degree', 'document', [
                'class'         => 'Editorum\Bundle\Document\ListStorage',
                'choice_label'  => 'ru[title]',
                'query_builder' => function (DocumentRepository $repository) {
                    $qb = $repository->createQueryBuilder();

                    return $qb->field('listName')->equals('degree');
                },
                'constraints'   => [
                    new NotNull(),
                    new NotBlank(),
                ],
            ])
            ->add('science', 'document', [
                'class'         => 'Editorum\Bundle\Document\ListStorage',
                'choice_label'  => 'ru[title]',
                'query_builder' => function (DocumentRepository $repository) {
                    $qb = $repository->createQueryBuilder();

                    return $qb->field('listName')->equals('science');
                },
                'constraints' => [
                    new NotNull(),
                    new NotBlank(),
                ],
            ])
        ;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class'    => 'Editorum\Bundle\Document\Author\DegreeList'
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'author_degree';
    }
}
