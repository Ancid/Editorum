<?php
namespace Editorum\Bundle\Form\Monography;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Editorum\Bundle\Document\Common\AbstractPublication;
use Editorum\Bundle\Document\Monography;
use SecurityBundle\Document\AbstractUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class MonographyType extends AbstractType
{
    /** @var AbstractUser */
    private $user;

    /** @var AuthorizationCheckerInterface */
    private $security;

    /** @var Monography */
    private $monography;


    /**
     * MonographyType constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param AuthorizationCheckerInterface $security
     */
    public function __construct(TokenStorageInterface $tokenStorage, AuthorizationCheckerInterface $security)
    {
        $this->user = $tokenStorage->getToken()->getUser();
        $this->security = $security;
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->monography = $builder->getData();
        $statusform =  $builder->create('statuses', 'form');

        if ($this->monography->getStatuses()['status'] !== AbstractPublication::ST_IS_PUBLISHED) {
            $builder
                ->add('previously_published', 'date', [
                    'widget' => 'single_text',
                    'label' => 'form.editorum.document.issue.previously_published',
                    'required' => false,
                ])
                ->add('languages', 'choice', [
                    'label'             => 'form.editorum.document.monography.languages',
                    'label_attr'        => [
                        'help' => 'form.editorum.document.monography.help.languages',
                    ],
                    'choices_as_values' => true,
                    'choices'           => [
                        AbstractPublication::LANG_RU => AbstractPublication::LANG_RU,
                        AbstractPublication::LANG_EN => AbstractPublication::LANG_EN,
                    ],
                    'multiple'          => true,
                ])
                ->add(
                    $builder->create('ru', 'form')
                        ->add('title', 'text', [
                            'label' => 'form.editorum.document.monography.title',
                        ])
                        ->add('annotation', 'textarea', [
                            'label'       => 'form.editorum.document.monography.annotation',
                            'attr'        => ['rows' => 5],
                            'constraints' => [
                                new Length(['max' => 3000]),
                            ],
                            'required' => false,
                        ])
                        ->add('keywords', 'text', [
                            'label'       => 'form.editorum.document.monography.keywords',
                            'constraints' => [
                                new Length(['max' => 1000]),
                            ],
                            'required' => false,
                        ])
                )
                ->add(
                    $builder->create('en', 'form')
                        ->add('title', 'text', [
                            'label' => 'form.editorum.document.monography.title',
                            'required' => false,
                        ])
                        ->add('annotation', 'textarea', [
                            'label'       => 'form.editorum.document.monography.annotation',
                            'attr'        => ['rows' => 5],
                            'constraints' => [
                                new Length(['max' => 3000]),
                            ],
                            'required' => false,
                        ])
                        ->add('keywords', 'text', [
                            'label'       => 'form.editorum.document.monography.keywords',
                            'constraints' => [
                                new Length(['max' => 1000]),
                            ],
                            'required' => false,
                        ])
                )
                ->add('page_total', 'integer', [
                    'label' => 'form.editorum.document.monography.page_total',
                    'attr'  => [
                        'min' => 0,
                    ],
                    'required' => false,
                ])
                ->add('author_pages', 'integer', [
                    'label' => 'form.editorum.document.monography.author_pages',
                    'attr'  => [
                        'min' => 0,
                    ],
                    'required' => false,
                ])
                ->add('isbn', 'text', [
                    'label' => 'form.editorum.document.monography.isbn',
                ])
                ->add('isbn_online', 'text', [
                    'label' => 'form.editorum.document.monography.isbn_online',
                    'required' => false,
                ])
                ->add('cover', 'file', [
                    'label' => 'form.editorum.document.monography.cover',
                    'required' => false,
                    'mapped'   => false,
                ])
                ->add('publication_number', 'integer', [
                    'label' => 'form.editorum.document.monography.publication_number',
                    'attr'  => [
                        'min' => 0,
                    ],
                    'required' => false,
                ])
                ->add('comment', 'textarea', [
                    'label' => 'form.editorum.document.monography.comment',
                    'attr'  => [
                        'rows' => 2,
                    ],
                    'required' => false,
                ])
                ->add('additional_information', 'textarea', [
                    'label' => 'form.editorum.document.monography.additional_information',
                    'attr'  => [
                        'rows' => 2,
                    ],
                    'required' => false,
                ])
                ->add('copublisher', 'textarea', [
                    'label' => 'form.editorum.document.monography.copublisher',
                    'attr'  => [
                        'rows' => 2,
                    ],
                    'required' => false,
                ])
            ;
            $statusform
                ->add('has_published', 'choice_button', [
                    'label'   => 'form.editorum.document.publication.statuses.has_published',
                    'choices' => ['form.choice.no', 'form.choice.yes'],
                    'required' => false,
                ])
                ->add('has_agreement', 'choice_button', [
                    'label'   => 'form.editorum.document.publication.statuses.has_agreement',
                    'choices' => ['form.choice.no', 'form.choice.yes'],
                    'required' => false,
                ])
            ;
            if ($this->security->isGranted('ROLE_ADMIN')) {
                $builder
                    ->add('publisher', 'document', [
                    'label' => 'form.editorum.document.textbook.publisher',
                    'class'         => 'Editorum\Bundle\Document\Publisher',
                    'choice_label'  => 'ru[title]',
                    'choices'   => $ar = $this->user->getCorporate()->getOrganization()->getCorporates()->toArray(),
                    ]);
            }

//            $builder->get('cover')->addViewTransformer(new CallbackTransformer(
//                function ($val) {},
//                function ($val) {}
//            ));
        }
        $statusform
            ->add('is_show_nauka', 'choice_button', [
                'label'   => 'form.editorum.document.publication.statuses.is_show_nauka',
                'choices' => ['form.choice.no', 'form.choice.yes'],
            ])
            ->add('is_opened', 'choice_button', [
                'label'   => 'form.editorum.document.publication.statuses.is_opened',
                'choices' => ['form.choice.closed', 'form.choice.opened'],
            ])
        ;

        $builder
            ->add('rinc_url', 'text', [
                'label' => 'form.editorum.document.common.rinc_url',
                'required' => false,
            ])
            ->add('doi_url', 'text', [
                'label' => 'form.editorum.document.common.doi_url',
                'required' => false,
            ])
            ->add($statusform);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class' => 'Editorum\Bundle\Document\Monography',
            'is_new'     => true,
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'monography';
    }
}
