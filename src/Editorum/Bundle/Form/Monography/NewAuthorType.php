<?php
namespace Editorum\Bundle\Form\Monography;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewAuthorType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                $builder->create('ru', 'form')
                    ->add('last_name', null, [
                        'label' => 'form.editorum.author.last_name',
                    ])
                    ->add('first_name', null, [
                        'label' => 'form.editorum.author.first_name',
                    ])
                    ->add('second_name', null, [
                        'required' => false,
                        'label' => 'form.editorum.author.second_name',
                    ])
            )
            ->add(
                $builder->create('en', 'form')
                    ->add('last_name', null, [
                        'required' => false,
                        'label'    => 'form.editorum.author.last_name',
                    ])
                    ->add('first_name', null, [
                        'required' => false,
                        'label' => 'form.editorum.author.first_name',
                    ])
                    ->add('second_name', null, [
                        'required' => false,
                        'label' => 'form.editorum.author.second_name',
                    ])
            )
            ->add('birth', 'date', [
                'label'  => 'form.editorum.author.birth',
                'widget' => 'single_text',
            ])
            ->add('reg_country', 'document', [
                'class'        => 'Editorum\Bundle\Document\Country',
                'choice_label' => 'ru[title]',
                'label' => 'form.editorum.author.country',
                'query_builder' => function (DocumentRepository $repository) {
                    return $repository->createQueryBuilder()->sort('position', 'asc');
                },
                'attr' => [
                    'data-live-search' => 'true',
                    'data-size'        => 15
                ]
            ])
            ->add('reg_region', 'document', [
                'class'        => 'Editorum\Bundle\Document\Region',
                'choice_label' => 'ru[title]',
                'required'     => false,
                'label' => 'form.editorum.author.region',
                'query_builder' => function (DocumentRepository $repository) {
                    return $repository->createQueryBuilder()->sort('position', 'asc');
                },
                'attr' => [
                    'data-live-search' => 'true',
                    'data-size'        => 15
                ]
            ])
//            ->add('city', 'text', [
//                'label'  => 'form.editorum.author.city',
//            ])
        ;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class'    => 'Editorum\Bundle\Document\Author'
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'new_authors_type';
    }
}
