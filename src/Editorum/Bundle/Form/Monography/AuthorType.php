<?php
namespace Editorum\Bundle\Form\Monography;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class AuthorType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('author', 'document', [
            'label'         => 'form.editorum.document.monography.author',
            'class'         => 'Editorum\Bundle\Document\Author',
            'choice_label'  => "ru[fio]",
            'query_builder' => function (DocumentRepository $repository) {
                $qb = $repository->createQueryBuilder();
                $qb
                    ->sort('ru.first_name', 'asc')
                    ->limit(10)
                ;

                return $qb;
            },
            'constraints'   => [
                new NotBlank(),
                new NotNull(),
            ],
            'required'      => false,
            'placeholder'   => 'form.select.author'
        ]);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'monography_author';
    }
}
