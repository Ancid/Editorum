<?php

namespace Editorum\Bundle\Form\Journal;

use Editorum\Bundle\Document\Journal;
use SecurityBundle\Document\AbstractUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class JournalType extends AbstractType
{
    /** @var Journal */
    private $journal;

    /** @var AbstractUser */
    private $user;

    /** @var AuthorizationCheckerInterface */
    private $security;


    /**
     * MonographyType constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param AuthorizationCheckerInterface $security
     */
    public function __construct(TokenStorageInterface $tokenStorage, AuthorizationCheckerInterface $security)
    {
        $this->user = $tokenStorage->getToken()->getUser();
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->journal = $builder->getData();
        $statusform =  $builder->create('statuses', 'form');
        $ruform = $builder->create('ru', 'form', [ 'label_attr' => [ 'class' => 'hide' ]]);
        $enform = $builder->create('en', 'form', [ 'label_attr' => [ 'class' => 'hide' ]]);

        if ($this->journal->getStatuses()['status'] !== Journal::ST_IS_PUBLISHING) {
            $builder
                ->add('translit_title', 'text', [
                    'label' => 'form.editorum.document.journal.translit_title',
                    'required' => false,
                ])
//                ->add('serie', 'document', [
//                    'class' => 'Editorum\Bundle\Document\Serie',
//                    'property' => 'ru[title]',
//                    'label' => 'form.editorum.document.journal.serie'
//                ])
            ;
            $ruform
                ->add('title', 'text', [
                    'label' => 'form.editorum.document.journal.title'
                ])
            ;
            $enform
                ->add('title', 'text', [
                    'label' => 'form.editorum.document.journal.title',
                    'required'  => false,
                ])
            ;
            if ($this->security->isGranted('ROLE_ADMIN')) {
                $builder
                    ->add('publisher', 'document', [
                        'label' => 'form.editorum.document.journal.publisher',
                        'class'         => 'Editorum\Bundle\Document\Publisher',
                        'choice_label'  => 'ru[title]',
                        'choices'   => $ar = $this->user->getCorporate()->getOrganization()->getCorporates()->toArray(),
                    ]);
            }
        }

        $statusform
            ->add('is_opened', 'choice_button', [
                'choices' => ['form.choice.closed', 'form.choice.opened'],
                'label' => 'form.editorum.document.publication.statuses.is_opened',
            ])
            ->add('is_show_nauka', 'choice_button', [
                'choices'   => [ 'Нет', 'Да' ],
                'label' => 'form.editorum.document.publication.statuses.is_show_nauka',
            ])
            ->add('is_vak', 'choice_button', [
                'choices' => ['Нет', 'Да'],
                'label' => 'form.editorum.document.journal.is_vak',
            ])
            ->add('is_rinc', 'choice_button', [
                'choices' => ['Нет', 'Да'],
                'label' => 'form.editorum.document.journal.is_rinc',
            ])
            ->add('is_request_allow', 'choice_button', [
                'choices' => ['Нет', 'Да'],
                'label' => 'form.editorum.document.publication.statuses.is_request_allow',
            ])
        ;
        $ruform
            ->add('about', 'textarea', [
                'label' => 'form.editorum.document.journal.mission',
                'required' => false,
            ])
            ->add('requirements_for_articles', 'textarea', [
                'label' => 'form.editorum.document.journal.article_requirements',
                'required' => false,
            ])
            ->add('ethics_publications', 'textarea', [
                'label' => 'form.editorum.document.journal.ethics',
                'required' => false,
            ])
            ->add('order_of_publication', 'textarea', [
                'label' => 'form.editorum.document.journal.rules',
                'required' => false,
            ])
        ;
        $enform
            ->add('about', 'textarea', [
                'label' => 'form.editorum.document.journal.mission',
                'required' => false,
            ])
            ->add('requirements_for_articles', 'textarea', [
                'label' => 'form.editorum.document.journal.article_requirements',
                'required' => false,
            ])
            ->add('ethics_publications', 'textarea', [
                'label' => 'form.editorum.document.journal.ethics',
                'required' => false,
            ])
            ->add('order_of_publication', 'textarea', [
                'label' => 'form.editorum.document.journal.rules',
                'required' => false,
            ])
        ;

        $builder
            ->add('languages', 'choice', [
                'multiple' => true,
                'choices' => [
                    Journal::LANG_RU => Journal::LANG_RU,
                    Journal::LANG_EN => Journal::LANG_EN
                ],
                'label' => 'form.editorum.document.journal.languages'
            ])
            ->add('cover', 'file', [
                'label' => 'form.editorum.document.journal.cover',
                'required' => false,
                'mapped'   => false,
            ])
            ->add('co_publisher', null, [
                'label' => 'form.editorum.document.journal.co_publisher',
                'required' => false,
            ])
            ->add('founder', null, [
                'label' => 'form.editorum.document.journal.founder',
                'required' => false,
            ])
            ->add('sregister', null, [
                'label' => 'form.editorum.document.journal.sregister',
                'required' => false,
            ])
            ->add('sdate', 'date', [
                'widget' => 'single_text',
                'label' => 'form.editorum.document.journal.sdate',
                'required' => false,
            ])
            ->add('issn_print', null, [
                'label' => 'form.editorum.document.journal.issn_print',
                'required' => false,
            ])
            ->add('issn_online', null, [
                'label' => 'form.editorum.document.journal.issn_online',
                'required' => false,
            ])->add('rinc_url', 'text', [
                'label' => 'form.editorum.document.common.rinc_url',
                'required' => false,
            ])
            ->add('doi_url', 'text', [
                'label' => 'form.editorum.document.common.doi_url',
                'required' => false,
            ])
            ->add('rinc_id', 'text', [
                'label' => 'form.editorum.document.journal.rinc_id',
                'required' => false,
            ])
            ->add($ruform)
            ->add($enform)
            ->add($statusform);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class'    => 'Editorum\Bundle\Document\Journal'
        ]);
    }

    public function getName()
    {
        return 'journal';
    }
}
