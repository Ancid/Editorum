<?php
namespace Editorum\Bundle\Form\Journal;

use Editorum\Bundle\Document\Issue;
use SecurityBundle\Document\AbstractUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Editorum\Bundle\Document\Conference as ConferenceDocument;

class IssueType extends AbstractType
{
    /** @var  Issue $issue */
    private $issue;

    /** @var AbstractUser */
    private $user;

    /** @var AuthorizationCheckerInterface */
    private $security;


    /**
     * MonographyType constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param AuthorizationCheckerInterface $security
     */
    public function __construct(TokenStorageInterface $tokenStorage, AuthorizationCheckerInterface $security)
    {
        $this->user = $tokenStorage->getToken()->getUser();
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->issue = $builder->getData();
        $statusform =  $builder->create('statuses', 'form');

        if ($this->issue->getStatuses()['status'] !== ConferenceDocument::ST_IS_PUBLISHED) {
            $builder
                ->add('volume', 'integer', [
                    'label' => 'form.editorum.document.issue.volume',
                    'required' => false,
                ])
                ->add('issue', 'integer', [
                    'label' => 'form.editorum.document.issue.issue',
                    'required' => false,
                ])
                ->add('part', null, [
                    'required' => false,
                    'label' => 'form.editorum.document.issue.part',
                ])
                ->add('page_count', 'integer', [
                    'required'  => false,
                    'label' => 'form.editorum.document.issue.page_count',
                ])
                ->add('type_access', 'choice_button', [
                    'choices'   => [
                        'form.editorum.document.issue.button.closed',
                        'form.editorum.document.issue.button.opened',
                    ],
                    'label' => 'form.editorum.document.issue.type_access',
                ])
            ;

        }
        $statusform
            ->add('is_show_nauka', 'choice_button', [
                'choices'   => [ 'Нет', 'Да' ],
                'label' => 'form.editorum.document.issue.is_show_nauka',
            ]);
        $builder
            ->add('rinc_url', 'text', [
                'label' => 'form.editorum.document.common.rinc_url',
                'required' => false,
            ])
            ->add('doi_url', 'text', [
                'label' => 'form.editorum.document.common.doi_url',
                'required' => false,
            ])
            ->add($statusform);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class'    => 'Editorum\Bundle\Document\Issue'
        ]);
    }


    public function getName()
    {
        return 'issue';
    }
}
