<?php
namespace Editorum\Bundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClassificatorType extends AbstractType
{
    public function getParent()
    {
        return 'classificator';
    }

    public function getName()
    {
        return 'classificator';
    }
}
