<?php

namespace Editorum\Bundle\Form\Article;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Editorum\Bundle\Document\Article;
use Editorum\Bundle\Document\Common\AbstractPublication;
use Editorum\Bundle\Document\Journal;
use SecurityBundle\Document\AbstractUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ArticleClassifType extends AbstractType
{
    /** @var Article */
    private $article;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->article = $builder->getData();

        $builder
            ->add('udk', 'classificator', [
                'label'         => 'Выберите УДК',
                'class'         => 'Editorum\Bundle\Document\ClassificatorUdk',
                'choice_label'  => 'name',
            ]);
    }


    public function getName()
    {
        return 'article_classif';
    }
}
