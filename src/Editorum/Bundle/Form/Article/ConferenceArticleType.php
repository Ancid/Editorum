<?php

namespace Editorum\Bundle\Form\Article;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Editorum\Bundle\Document\Article;
use Editorum\Bundle\Document\Common\AbstractPublication;
use Editorum\Bundle\Document\Conference;
use Editorum\Bundle\Document\ConferenceArticle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use SecurityBundle\Document\AbstractUser;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ConferenceArticleType extends AbstractType
{
    /** @var AbstractUser */
    private $user;

    /** @var AuthorizationCheckerInterface */
    private $security;

    /** @var ConferenceArticle */
    private $article;


    /**
     * MonographyType constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param AuthorizationCheckerInterface $security
     */
    public function __construct(TokenStorageInterface $tokenStorage, AuthorizationCheckerInterface $security)
    {
        $this->user = $tokenStorage->getToken()->getUser();
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->article = $builder->getData();
        $statusform =  $builder->create('statuses', 'form');

        if ($this->article->getStatuses()['status'] !== Conference::ST_IS_PUBLISHED) {
            $builder
                ->add('languages', 'choice', [
                    'multiple' => true,
                    'choices' => [
                        Article::LANG_RU => Article::LANG_RU,
                        Article::LANG_EN => Article::LANG_EN
                    ],
                    'label' => 'form.editorum.document.article.languages',
                ])
                ->add('previously_published', 'date', [
                    'widget' => 'single_text',
                    'label' => 'form.editorum.document.issue.previously_published',
                    'required' => false,
                ])
                ->add(
                    $builder->create('ru', 'form', [ 'label_attr' => [ 'class' => 'hide' ]])
                        ->add('title', 'textarea', [
                            'label' => 'form.editorum.document.article.title',
                            'attr'  => ['rows' => 2],
                        ])
                        ->add('annotation', 'textarea', [
                            'label' => 'form.editorum.document.article.annotation',
                            'attr'  => ['rows' => 5],
                            'required' => false,
                        ])
                        ->add('keywords', 'textarea', [
                            'label' => 'form.editorum.document.article.keywords',
                            'attr'  => ['rows' => 5],
                            'required' => false,
                        ])
                )
                ->add(
                    $builder->create('en', 'form', [ 'label_attr' => [ 'class' => 'hide' ]])
                        ->add('title', 'textarea', [
                            'label' => 'form.editorum.document.article.title',
                            'attr'  => ['rows' => 2],
                            'required' => false,
                        ])
                        ->add('annotation', 'textarea', [
                            'label' => 'form.editorum.document.article.annotation',
                            'attr'  => ['rows' => 5],
                            'required' => false,
                        ])
                        ->add('keywords', 'textarea', [
                            'label' => 'form.editorum.document.article.keywords',
                            'attr'  => ['rows' => 5],
                            'required' => false,
                        ])
                )
                ->add('depofile', 'file', [
                    'required'  => false,
                ])
                ->add('author_pages', null, [
                    'label' => 'form.editorum.document.article.author_pages',
                    'required' => false,
                ])
                ->add('first_page', null, [
                    'label' => 'form.editorum.document.article.first_page',
                    'required' => false,
                ])
                ->add('last_page', null, [
                    'label' => 'form.editorum.document.article.last_page',
                    'required' => false,
                ])
                ->add('section', 'document', [
                    'class' => 'Editorum\Bundle\Document\Section',
                    'property' => 'ru[title]',
                    'choices' => $this->article->getConference()->getSections(),
                    'placeholder' => 'Ничего не выбрано',
                    'label' => 'form.editorum.document.article.section',
                    'required' => false,
                ])
            ;
            $statusform
                ->add('has_published', 'choice_button', [
                    'choices' => ['Нет', 'Да'],
                    'label' => 'form.editorum.document.publication.statuses.has_published',
                    'required' => false,
                ])
                ->add('has_reviewers', 'choice_button', [
                    'choices' => ['Нет', 'Да'],
                    'label' => 'form.editorum.document.publication.statuses.has_reviewers',
                    'required' => false,
                ])
            ;
            if ($this->security->isGranted('ROLE_ADMIN')) {
                $builder
                    ->add('conference', 'document', [
                        'required'      => true,
                        'label'         => 'form.editorum.document.article.conference',
                        'class'         => 'Editorum\Bundle\Document\Conference',
                        'choice_label'  => 'ru[title]',
                        'query_builder' => function (DocumentRepository $repository) {
                            $qb = $repository->createQueryBuilder();

                            $corp = [];
                            foreach ($this->user->getCorporate()->getOrganization()->getCorporates()->toArray() as $co) {
                                $corp[$co->getKind()][] = $co->getId();
                            }
                            foreach ($corp as $type => $ids) {
                                $qb
                                    ->addOr(
                                        $qb->expr()
                                            ->addAnd($qb->expr()->field('publisher.$ref')->equals($type))
                                            ->addAnd($qb->expr()->field('publisher.$id')->in($ids))
                                    );
                            }

                            $now = new \DateTime();
                            $qb
                                ->addAnd($qb->expr()->field('request_start')->lte($now))
                                ->addAnd($qb->expr()->field('request_finish')->gte($now));

                            return $qb;
                        },

                    ]);
            }
            $builder->get('depofile')->addViewTransformer(new CallbackTransformer(
                function ($value) {
                    $file = null;
                    if ($value != null) {
                        $file = new File($value);
                    }
                    return $file;
                },
                function ($value) {
                    return $value;
                }
            ));
        }

        $statusform
            ->add('is_show_nauka', 'choice_button', [
                'choices'   => [ 'Нет', 'Да' ],
                'label' => 'form.editorum.document.publication.statuses.is_show_nauka',
                'required' => false,
            ])
            ->add('is_opened', 'choice_button', [
                'choices' => ['Нет', 'Да'],
                'label' => 'form.editorum.document.publication.statuses.is_opened',
            ])
        ;

        $builder
            ->add('rinc_url', 'text', [
                'label' => 'form.editorum.document.common.rinc_url',
                'required' => false,
            ])
            ->add('doi_url', 'text', [
                'label' => 'form.editorum.document.common.doi_url',
                'required' => false,
            ])
            ->add($statusform);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class'    => 'Editorum\Bundle\Document\ConferenceArticle'
        ]);
    }

    public function getName()
    {
        return 'article';
    }
}
