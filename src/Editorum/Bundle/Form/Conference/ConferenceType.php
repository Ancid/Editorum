<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 30.06.2016
 * Time: 15:51
 */

namespace Editorum\Bundle\Form\Conference;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Editorum\Bundle\Document\Conference;
use SecurityBundle\Document\AbstractUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class ConferenceType extends AbstractType
{
    /** @var Conference */
    private $conference;

    /** @var AbstractUser */
    private $user;

    /** @var AuthorizationCheckerInterface */
    private $security;


    /**
     * MonographyType constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param AuthorizationCheckerInterface $security
     */
    public function __construct(TokenStorageInterface $tokenStorage, AuthorizationCheckerInterface $security)
    {
        $this->user = $tokenStorage->getToken()->getUser();
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->conference = $builder->getData();
        $statusform =  $builder->create('statuses', 'form');
        $ruform = $builder->create('ru', 'form', [ 'label_attr' => [ 'class' => 'hide' ]]);
        $enform = $builder->create('en', 'form', [ 'label_attr' => [ 'class' => 'hide' ]]);

        if ($this->conference->getStatuses()['status'] !== Conference::ST_CLOSED) {
            $ruform
                ->add('title', 'text', [
                    'label' => 'form.editorum.document.conference.title'
                ])
            ;
            $enform
                ->add('title', 'text', [
                    'label' => 'form.editorum.document.conference.title',
                    'required'  => false,
                ])
            ;
            if ($this->security->isGranted('ROLE_ADMIN')) {
                $builder
                    ->add('publisher', 'document', [
                        'label' => 'form.editorum.document.journal.publisher',
                        'class'         => 'Editorum\Bundle\Document\Publisher',
                        'choice_label'  => 'ru[title]',
                        'choices'   => $ar = $this->user->getCorporate()->getOrganization()->getCorporates(),
                    ]);
            }
        }
        $ruform
            ->add('city', 'text', [
                'label' => 'form.editorum.document.conference.city',
                'required' => false,
            ]);
        $enform
            ->add('city', 'text', [
                'label' => 'form.editorum.document.conference.city',
                'required' => false,
            ]);
        $builder
            ->add('date_start', 'date', [
                'widget' => 'single_text',
                'label' => 'form.editorum.document.conference.date_start',
            ])
            ->add('date_finish', 'date', [
                'widget' => 'single_text',
                'label' => 'form.editorum.document.conference.date_finish',
            ])
            ->add('request_start', 'date', [
                'widget' => 'single_text',
                'label' => 'form.editorum.document.conference.request_start',
            ])
            ->add('request_finish', 'date', [
                'widget' => 'single_text',
                'label' => 'form.editorum.document.conference.request_finish',
            ])
            ->add('number', 'integer', [
                'label' => 'form.editorum.document.conference.number',
                'required' => false,
            ])
            ->add('html', 'textarea', [
                'label' => 'form.editorum.document.conference.html',
                'required' => false,
                'attr' => ['rows' => '7'],
            ])
            ->add('country', 'document', [
                'class' => 'Editorum\Bundle\Document\Country',
                'property' => 'ru[title]',
                'label' => 'form.editorum.document.conference.country',
                'query_builder' => function (DocumentRepository $repository) {
                    return $repository->createQueryBuilder()->sort('position', 'asc');
                },
                'attr'          => [
                    'data-live-search' => 'true',
                    'data-size'        => 15
                ]
            ])
            ->add('languages', 'choice', [
                'multiple' => true,
                'choices' => [
                    Conference::LANG_RU => Conference::LANG_RU,
                    Conference::LANG_EN => Conference::LANG_EN
                ],
                'label' => 'form.editorum.document.conference.languages'
            ])
            ->add(
                $builder->create('statuses', 'form', [ 'label_attr' => [ 'class' => 'hide' ]])
                    ->add('is_show_nauka', 'choice_button', [
                        'choices'   => [ 'Нет', 'Да' ],
                        'label' => 'form.editorum.document.publication.statuses.is_show_nauka',
                    ])
                    ->add('is_in_rinc', 'choice_button', [
                        'choices'   => [ 'Нет', 'Да' ],
                        'label' => 'form.editorum.document.conference.is_rinc',
                    ])
            )
            ->add($ruform)
            ->add($enform);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class'    => 'Editorum\Bundle\Document\Conference'
        ]);
    }

    public function getName()
    {
        return 'conference';
    }
}
