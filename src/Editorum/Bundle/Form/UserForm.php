<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 25.04.2016
 * Time: 16:26
 */
namespace Editorum\Bundle\Form;

use Symfony\Component\Form\AbstractType;
use Doctrine\ODM\MongoDB\DocumentRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class UserForm extends AbstractType
{
    /** @var AuthorizationCheckerInterface */
    private $security;

    /**
     * UserForm constructor.
     * @param AuthorizationCheckerInterface $security
     */
    public function __construct(AuthorizationCheckerInterface $security)
    {
        $this->security = $security;

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_name', 'text')
            ->add('last_name', 'text')
            ->add('email', 'text')
            ->add('plain_password', 'repeated', [
                'type'           => 'password',
                'first_options'  => [
                    'label' => 'Пароль',
                ],
                'second_options' => [
                    'label' => 'Повторите пароль'
                ]
            ])
        ;

        if ($this->security->isGranted('ROLE_ADMIN')) {
            $builder
                ->add('userRoles', 'document', [
                    'class' => 'SecurityBundle\Document\Role',
                    'property'  => 'name',
                    'multiple'  => true,
                    'required'  => false,
                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class'    => 'Editorum\Bundle\Document\User'
        ]);
    }

    public function getName()
    {
        return 'create_entity_form';
    }
}
