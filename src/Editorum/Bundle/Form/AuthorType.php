<?php
namespace Editorum\Bundle\Form;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Editorum\Bundle\Document\Common\AbstractPublication;
use SecurityBundle\Document\AbstractUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class AuthorType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                $builder->create('ru', 'form')
                    ->add('first_name', 'text', [
                        'label' => 'form.editorum.author.first_name',
                    ])
                    ->add('second_name', 'text', [
                        'label'       => 'form.editorum.author.second_name',
                        'required' => false,
                    ])
                    ->add('last_name', 'text', [
                        'label'       => 'form.editorum.author.last_name',
                    ])
            )
            ->add('has_fact_addr', 'choice_button', [
                'choices'   => ['form.choice.no', 'form.choice.yes' ]
            ])
            ->add(
                $builder->create('en', 'form')
                    ->add('first_name', 'text', [
                        'label' => 'form.editorum.author.first_name',
                        'required' => false,
                    ])
                    ->add('second_name', 'text', [
                        'label'       => 'form.editorum.author.second_name',
                        'required' => false,
                    ])
                    ->add('last_name', 'text', [
                        'label'       => 'form.editorum.author.last_name',
                        'required' => false,
                    ])
            )
            ->add('birth', 'date', [
                'widget'   => 'single_text',
                'required' => false,
            ])
            ->add('date_end', 'date', [
                'widget'   => 'single_text',
                'required' => false,
            ])
            ->add('reg_country', 'document', [
                'class' => 'Editorum\Bundle\Document\Country',
                'property' => 'ru[title]',
                'label' => 'form.editorum.author.country',
                'query_builder' => function (DocumentRepository $repository) {
                    return $repository->createQueryBuilder()->sort('position', 'asc');
                },
                'attr'  => [
                    'data-live-search'  => 'true',
                    'data-size'         => 15
                ]
            ])
            ->add('reg_region', 'document', [
                'class' => 'Editorum\Bundle\Document\Region',
                'property' => 'ru[title]',
                'label' => 'form.editorum.author.region',
                'query_builder' => function (DocumentRepository $repository) {
                    return $repository->createQueryBuilder()->sort('position', 'asc');
                },
                'attr'          => [
                    'data-live-search' => 'true',
                    'data-size'        => 15
                ],
                'required' => false,
            ])
            ->add('reg_index', 'text', [
                'required'  => false,
            ])
            ->add('reg_city', 'text', [
                'required' => false,
            ])
            ->add('reg_adress', 'text', [
                'required' => false,
            ])
            ->add('country', 'document', [
                'class'         => 'Editorum\Bundle\Document\Country',
                'property'      => 'ru[title]',
                'label'         => 'form.editorum.author.country',
                'query_builder' => function (DocumentRepository $repository) {
                    return $repository->createQueryBuilder()->sort('position', 'asc');
                },
                'attr'          => [
                    'data-live-search' => 'true',
                    'data-size'        => 15
                ]
            ])
            ->add('region', 'document', [
                'class'         => 'Editorum\Bundle\Document\Region',
                'property'      => 'ru[title]',
                'label'         => 'form.editorum.author.region',
                'query_builder' => function (DocumentRepository $repository) {
                    return $repository->createQueryBuilder()->sort('position', 'asc');
                },
                'attr'          => [
                    'data-live-search' => 'true',
                    'data-size'        => 15
                ],
                'required'      => false,
            ])
            ->add('fact_index', 'text', [
                'required' => false,
            ])
            ->add('city', 'text', [
                'required' => false,
            ])
            ->add('fact_adress', 'text', [
                'required' => false,
            ])
            ->add('cell_phone', 'text', [
                'required'  => false,
            ])
            ->add('email', 'email', [
                'required'  => false,
            ])
            ->add('pass_seria', 'text', [
                'required'  => false,
            ])
            ->add('pass_number', 'text', [
                'required' => false,
            ])
            ->add('pass_code', 'text', [
                'required' => false,
            ])
            ->add('pass_dt', 'date', [
                'widget'    => 'single_text',
                'required' => false,
            ])
            ->add('pass_given', 'text', [
                'required' => false,
            ])
            ->add('is_reviewer', 'choice_button', [
                'label'     => 'form.editorum.author.is_reviewer',
                'choices'   => [ 'form.choice.no', 'form.choice.yes' ]
            ])
            ->add('snils', 'text', [
                'required'  => false,
            ])
            ->add('inn', 'text', [
                'required' => false,
            ])
            ->add('scopus', 'text', [
                'required' => false,
            ])
            ->add('researcherid', 'text', [
                'required' => false,
            ])
            ->add('rinc', 'text', [
                'required' => false,
            ])
            ->add('orcid_id', 'text', [
                'required' => false,
            ])
            ->add('science_rang', 'document', [
                'class'         => 'Editorum\Bundle\Document\ListStorage',
                'choice_label'  => 'ru[title]',
                'query_builder' => function (DocumentRepository $repository) {
                    $qb = $repository->createQueryBuilder();

                    return $qb->field('listName')->equals('rang');
                },
                'required'      => false,
            ])
            ->add('academic_title', 'text', [
                'required' => false,
            ])
            ->add('bank_reciver', null, [
                'label' => 'form.editorum.author.bank.reciver',
                'required' => false,
            ])
            ->add('bank_account', null, [
                'label'    => 'form.editorum.author.bank.account',
                'required' => false,
            ])
            ->add('bank_name', null, [
                'label'    => 'form.editorum.author.bank.name',
                'required' => false,
            ])
            ->add('bank_coaccount', null, [
                'label'    => 'form.editorum.author.bank.coaccount',
                'required' => false,
            ])
            ->add('bank_bik', null, [
                'label'    => 'form.editorum.author.bank.bik',
                'required' => false,
            ])
        ;

        $builder->get('pass_dt')->addModelTransformer(new CallbackTransformer(
            function ($value) {
                return null == $value ? null : new \DateTime($value);
            },
            function ($value) {
                return empty($value) ? null : $value->format('Y-m-d');
            }
        ));
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class' => 'Editorum\Bundle\Document\Author',
            'is_new'     => true,
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'author';
    }
}
