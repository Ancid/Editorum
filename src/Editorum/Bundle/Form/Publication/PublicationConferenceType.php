<?php
namespace Editorum\Bundle\Form\Publication;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PublicationConferenceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('conference',
            'document',
            [
                'class'        => 'AsuBundle:Conference',
                'choice_label' => 'ru[title]',
                'empty_value' => 'Ничего не выбрано',
                'empty_data' => null,
                'required'  => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(['data_class' => 'Editorum\Bundle\Document\Publication']);
    }

    public function getName()
    {
        return 'asu_form_publication_conference';
    }
}