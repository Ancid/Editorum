<?php
namespace Editorum\Bundle\Form;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Editorum\Bundle\Document\Common\AbstractPublication;
use SecurityBundle\Document\AbstractUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ChapterEditType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                $builder->create('ru', 'form')
                    ->add('title', 'text', [
                        'label' => 'form.editorum.document.chapter.title',
                    ])
                    ->add('annotation', 'text', [
                        'label' => 'form.editorum.document.chapter.annotation',
                        'attr' => ['rows' => 3],
                        'required' => false,
                    ])
                    ->add('keywords', 'text', [
                        'label' => 'form.editorum.document.chapter.keywords',
                        'required' => false,
                    ])
            )
            ->add(
                $builder->create('en', 'form')
                    ->add('title', 'text', [
                        'label' => 'form.editorum.document.chapter.title',
                    ])
                    ->add('annotation', 'text', [
                        'label' => 'form.editorum.document.chapter.annotation',
                        'attr' => ['rows' => 3],
                        'required' => false,
                    ])
                    ->add('keywords', 'text', [
                        'label' => 'form.editorum.document.chapter.keywords',
                        'required' => false,
                    ])
            )
        ;

        if (null !== $options['publication']) {
            $builder->add('authors', 'document', [
                'class'         => 'Editorum\Bundle\Document\Author',
                'placeholder'   => 'form.select.author',
                'choice_label'  => "ru[fio]",
                'label'         => 'form.editorum.document.chapter.authors',
                'query_builder' => function (DocumentRepository $repository) use ($options) {
                    $qb = $repository->createQueryBuilder();
                    $authors = $options['publication']->getAuthors();

                    $ids = [];
                    foreach ($authors as $author) {
                        $ids[] = $author->getId();
                    }
                    $qb->field('_id')->in($ids);

                    return $qb;
                },
                'multiple'      => true,
            ]);
        }

        $builder
            ->add('text', 'textarea', [
                'label' => 'form.editorum.document.chapter.text',
                'attr' => [
                    'rows' => 5
                ],
                'required' => false,
            ])
            ->add('doi_url', 'text', [
                'label' => 'form.editorum.document.common.doi_url',
                'required' => false,
            ])
            ->add('first_page', 'integer', [
                'label' => 'form.editorum.document.chapter.first_page',
            ])
            ->add('last_page', 'integer', [
                'label' => 'form.editorum.document.chapter.last_page',
            ])
        ;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class' => 'Editorum\Bundle\Document\Chapter',
            'publication' => null,
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'chapter_edit';
    }
}
