<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 20.05.2016
 * Time: 13:09
 */

namespace Editorum\Bundle\Form\Organization;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrganizationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', [
                'label' => 'form.editorum.document.organization.title'
            ])
            ->add('fullname', 'text', [
                'label' => 'form.editorum.document.organization.fulltitle'
            ])
        ;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class' => 'Editorum\Bundle\Document\Organization',
        ]);
    }

    public function getName()
    {
        return 'edit_organization_form';
    }
}
