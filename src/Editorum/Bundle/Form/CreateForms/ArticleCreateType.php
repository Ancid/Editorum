<?php
namespace Editorum\Bundle\Form\CreateForms;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Editorum\Bundle\Document\Common\AbstractPublication;
use Editorum\Bundle\Document\Journal;
use SecurityBundle\Document\AbstractUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class ArticleCreateType extends AbstractType
{
    /** @var AbstractUser */
    private $user;

    /** @var AuthorizationCheckerInterface */
    private $security;


    /**
     * MonographyType constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param AuthorizationCheckerInterface $security
     */
    public function __construct(TokenStorageInterface $tokenStorage, AuthorizationCheckerInterface $security)
    {
        $this->user = $tokenStorage->getToken()->getUser();
        $this->security = $security;
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                $builder->create('ru', 'form')
                    ->add('title', 'text', [
                        'label' => 'form.editorum.document.article.title',
                    ])
            )
            ->add('journal', 'document', [
                'label'         => 'form.editorum.document.article.journal',
                'class'         => 'Editorum\Bundle\Document\Journal',
                'choice_label'  => 'ru[title]',
                'query_builder' => function (DocumentRepository $repository) {
                    $qb = $repository->createQueryBuilder();
                    $user = $this->user;

                    if ($this->security->isGranted('ROLE_ADMIN')) {
                        $corp = [];
                        foreach ($this->user->getCorporate()->getOrganization()->getCorporates()->toArray() as $co) {
                            $corp[$co->getKind()][] = $co->getId();
                        }
                        foreach ($corp as $type => $ids) {
                            $qb
                                ->addOr(
                                    $qb->expr()
                                        ->addAnd($qb->expr()->field('publisher.$ref')->equals($type))
                                        ->addAnd($qb->expr()->field('publisher.$id')->in($ids))
                                );
                        }
                    } else {
                        $qb->field('publisher')->references($user->getCorporate());
                    }

                    return $qb;
                },
            ])
        ;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class' => 'Editorum\Bundle\Document\Article',
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'article_create';
    }
}
