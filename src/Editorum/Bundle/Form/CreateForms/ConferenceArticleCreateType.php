<?php
namespace Editorum\Bundle\Form\CreateForms;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Editorum\Bundle\Document\Common\AbstractPublication;
use Editorum\Bundle\Document\Conference;
use SecurityBundle\Document\AbstractUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ConferenceArticleCreateType extends AbstractType
{
    /** @var AbstractUser */
    private $user;


    /**
     * MonographyType constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->user = $tokenStorage->getToken()->getUser();
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                $builder->create('ru', 'form')
                    ->add('title', 'text', [
                        'label' => 'form.editorum.document.article.title',
                    ])
            )
            ->add('conference', 'document', [
                'required'      => true,
                'label'         => 'form.editorum.document.article.conference',
                'class'         => 'Editorum\Bundle\Document\Conference',
                'choice_label'  => 'ru[title]',
                'query_builder' => function (DocumentRepository $repository) {
                    $qb = $repository->createQueryBuilder();

                    $corp = [];
                    foreach ($this->user->getCorporate()->getOrganization()->getCorporates()->toArray() as $co) {
                        $corp[$co->getKind()][] = $co->getId();
                    }
                    foreach ($corp as $type => $ids) {
                        $qb
                            ->addOr(
                                $qb->expr()
                                    ->addAnd($qb->expr()->field('publisher.$ref')->equals($type))
                                    ->addAnd($qb->expr()->field('publisher.$id')->in($ids))
                            );
                    }

                    $now = new \DateTime();
                    $qb
                        ->addAnd($qb->expr()->field('request_start')->lte($now))
                        ->addAnd($qb->expr()->field('request_finish')->gte($now));

                    return $qb;
                },
            ]);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class' => 'Editorum\Bundle\Document\ConferenceArticle',
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'article_create';
    }
}
