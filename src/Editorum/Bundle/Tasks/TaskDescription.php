<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 23.12.2015
 * Time: 22:59
 */

namespace Editorum\Bundle\Tasks;

use Editorum\Bundle\Logic\Task;

class TaskDescription extends Task
{
    public $type = Task::TYPE_BOTH;

    public $title = 'Введите аннотацию и выберите рубрики произведения';

}