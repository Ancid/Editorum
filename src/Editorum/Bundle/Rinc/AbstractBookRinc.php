<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 27.06.2016
 * Time: 14:33
 */

namespace Editorum\Bundle\Rinc;


use Editorum\Bundle\Document\Reference;


/**
 * Class AbstractBookRinc
 * @package Editorum\Bundle\Rinc
 */
abstract class AbstractBookRinc extends AbstractRinc
{
    abstract protected function generateOperCard();

    abstract protected function generateMetaData();

    abstract protected function generateAuthors();

    abstract protected function generateArticles();


    public function generateCodes($document)
    {
        $this->xml->startElement('codes');

        $this->xml->writeElement('isbn', $document->getIsbn());
        $this->xml->writeElement('isbn_online', $document->getIsbnOnline());
        if (null !== $document->getUdk()) {
            $this->xml->writeElement('udk', $document->getUdk()[0]);
        }
        if (null !== $document->getGrnti()) {
            $this->xml->writeElement('grnti', $document->getGrnti()[0]);
        }
        $this->xml->writeElement('doi', $document->getDoi());
        $this->xml->endElement();
    }


    protected function generateBookInfo()
    {
        $this->xml->startElement('bookInfo');
        $this->xml->writeAttribute('lang', 'RUS');
        $this->xml->writeElement('title', $this->document->getRu()['title']);
        $this->xml->writeElement('langPubl', $this->document->getLang());
        $this->xml->writeElement('url', $this->document->getRincUrl());
        $this->xml->endElement();

        $this->xml->startElement('bookInfo');
        $this->xml->writeAttribute('lang', 'ENG');
        $this->xml->writeElement('title', $this->document->getEn()['title']);
        $this->xml->writeElement('langPubl', $this->document->getLang());
        $this->xml->writeElement('url', $this->document->getRincUrl());
        $this->xml->endElement();
    }


    /** Аннотации */
    protected function generateAbstracts()
    {
        $this->xml->startElement('abstracts');
        $this->xml->startElement('abstract');
        $this->xml->writeAttribute('lang', 'RUS');
        if (array_key_exists('annotation', $this->document->getRu())) {
            $this->xml->text($this->document->getRu()['annotation']);
        }
        $this->xml->endElement();
        $this->xml->startElement('abstract');
        $this->xml->writeAttribute('lang', 'ENG');
        if (array_key_exists('annotation', $this->document->getEn())) {
            $this->xml->text($this->document->getEn()['annotation']);
        }
        $this->xml->endElement();
        $this->xml->endElement();
    }


    /** Ключевые слова */
    protected function generateKeywords()
    {
        $kewds = array_merge(
            preg_split("/[;,]+/", $this->document->getRu()['keywords']),
            preg_split("/[;,]+/", $this->document->getEn()['keywords'])
        );
        $this->xml->startElement('keywords');
        $this->xml->startElement('kwdGroup');
        $this->xml->writeAttribute('lang', 'ANY');
        foreach ($kewds as $kewd) {
            $this->xml->writeElement('keyword', trim($kewd));
        }
        $this->xml->endElement();
        $this->xml->endElement();
    }


    protected function generateFiles()
    {
        //@ToDo реализовать метод
        $this->xml->startElement('files');
        $this->xml->endElement();
    }


    protected function generateContent()
    {
        //@ToDo реализовать метод
        $this->xml->startElement('contents');
        $this->xml->endElement();
    }


    /** Конструктор XML */
    final public function generateXml()
    {
        $this->xml->openMemory();
        $this->xml->setIndent(true);
        $this->xml->startDocument('1.0');
        $this->xml->startElement('book');

        $this->generateOperCard();
        $this->generateMetaData();
        $this->generateAuthors();
        $this->generateBookInfo();
        $this->generateAbstracts();
        $this->generateCodes($this->document);
        $this->generateKeywords();
        $this->generateReferences($this->document);
        $this->generateFiles();
        $this->generateContent();
        $this->generateArticles();
        
        $this->xml->endElement();

        return $this->clearEmptyTags($this->xml->outputMemory());
    }
}
