<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 19.07.2016
 * Time: 18:46
 */

namespace Editorum\Bundle\Rinc\Objects;

use Editorum\Bundle\Document\Chapter;
use Editorum\Bundle\Rinc\AbstractBookRinc;
use Editorum\Bundle\Document\Monography as MonographyDocument;

class Monography extends AbstractBookRinc
{
    /**
     * RincCollection constructor.
     * @param MonographyDocument $monography
     */
    public function __construct(MonographyDocument $monography)
    {
        parent::__construct();
        $this->document = $monography;
    }

    public function generateOperCard()
    {
        parent::abstractOperCard(count($this->document->getChapters()));
    }

    public function generateMetaData()
    {
        $publisher = $this->document->getPublisher();

        if (null === $publisher) {
            throw new \Exception('У произведения нет издателя!');
        }

        $this->xml->writeElement('bookType', 'MNG');
        if (null !== $this->document->getDatePublished()) {
            $this->xml->writeElement('dateUni', $this->document->getDatePublished()->format('Ymd'));
        }
        $this->xml->writeElement('publ', $publisher->getShortTitle());
        $this->xml->writeElement('publId', $publisher->getRincId());
        $this->xml->writeElement('placePubl', $publisher->getTown());
        $this->xml->writeElement('pages', $this->document->getPageTotal());
    }

    public function generateAuthors()
    {
        $this->collectAuthors($this->document->getAuthors());
    }

    public function generateBookInfo()
    {
        parent::generateBookInfo();
    }

    public function generateAbstracts()
    {
        parent::generateAbstracts();
    }

    public function generateArticles()
    {
        $chapters = $this->document->getChapters();

        /** @var Chapter $chapter */
        foreach ($chapters as $chapter) {
            $this->xml->startElement('article');
            $this->xml->writeElement('pages', $chapter->getFirstPage().' - '.$chapter->getLastPage());
            $this->xml->writeElement('artType', 'BKC');

            $this->xml->startElement('artTitles');
            $this->xml->startElement('artTitle');
            $this->xml->writeAttribute('lang', 'RUS');
            $this->xml->text($chapter->getRu()['title']);
            $this->xml->endElement();

            $this->xml->startElement('artTitle');
            $this->xml->writeAttribute('lang', 'ENG');
            $this->xml->text($chapter->getEn()['title']);
            $this->xml->endElement();

            $this->xml->endElement();

            $this->xml->startElement('abstracts');
            $this->xml->startElement('abstract');
            $this->xml->writeAttribute('lang', 'RUS');
            $this->xml->text($chapter->getRu()['annotation']);
            $this->xml->endElement();

            $this->xml->startElement('abstract');
            $this->xml->writeAttribute('lang', 'ENG');
            $this->xml->text($chapter->getEn()['annotation']);
            $this->xml->endElement();
            $this->xml->endElement();

//            $this->generateCodes($this->document);

            $this->metaKeywords($chapter);
//            $this->metaReferences($chapter);

            $this->xml->endElement();
        }
    }
}
