<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 29.06.2016
 * Time: 12:08
 */

namespace Editorum\Bundle\Rinc\Objects;

use Editorum\Bundle\Rinc\AbstractBookRinc;
use Editorum\Bundle\Document\Article as ArticleDocument;

class Article extends AbstractBookRinc
{
    protected $manager = false;

    /**
     * RincCollection constructor.
     * @param ArticleDocument $article
     */
    public function __construct(ArticleDocument $article)
    {
        parent::__construct();
        $this->document = $article;
    }
    
    public function generateOperCard()
    {
        $this->xml->startElement('operCard');
        $this->xml->writeElement('operator', 'user');
        $this->xml->writeElement('pid', $this->document->getId());
        $this->xml->writeElement('date', date('Y-m-d H:i:s'));

        $this->xml->writeElement('cntNode', 0);
        $this->xml->writeElement('cs', 0);
        $this->xml->endElement();
    }
    
    public function generateMetaData()
    {
        $publisher = $this->document->getPublisher();
        $this->xml->writeElement('bookType', 'MNG');
        if (null !== $this->document->getDatePublished()) {
            $this->xml->writeElement('dateUni', $this->document->getIssue()->getDatePublished()->format('Ymd'));
        }
        $this->xml->writeElement('publ', $publisher->getShortTitle());
        $this->xml->writeElement('publId', $publisher->getRincId());
        $this->xml->writeElement('placePubl', $publisher->getTown());
        $this->xml->writeElement('pages', $this->document->getAuthorPages());
    }
    
    protected function generateAuthors()
    {
        $this->collectAuthors($this->document->getAuthors());
    }
    
    public function generateArticles()
    {
        // TODO: Implement generateArticles() method.
    }


    /**
     * Переопределяем метод для статей журнала
     * @param ArticleDocument $document
     */
    public function generateCodes($document)
    {
        $this->xml->startElement('codes');

        if (null !== $document->getUdk()) {
            $this->xml->writeElement('udk', $document->getUdk()[0]);
        }
        if (null !== $document->getGrnti()) {
            $this->xml->writeElement('grnti', $document->getGrnti()[0]);
        }
        $this->xml->writeElement('doi', $document->getDoi());
        $this->xml->endElement();
    }
}
