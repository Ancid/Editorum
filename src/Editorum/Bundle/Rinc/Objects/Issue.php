<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 29.06.2016
 * Time: 15:15
 */

namespace Editorum\Bundle\Rinc\Objects;

use Editorum\Bundle\Rinc\AbstractJournalRinc;
use Editorum\Bundle\Document\Issue as IssueDocument;

class Issue extends AbstractJournalRinc
{
    /**
     * Issue constructor.
     * @param IssueDocument $issue
     */
    public function __construct(IssueDocument $issue)
    {
        parent::__construct();
        $this->document = $issue;
    }

    public function generateOperCard()
    {
        $this->xml->startElement('operCard');
        $this->xml->writeElement('operator', 'user');
        $this->xml->writeElement('pid', $this->document->getId());
        $this->xml->writeElement('date', date('Y-m-d H:i:s'));
        $this->xml->writeElement('cntArticle', count($this->document->getArticles()));
        $this->xml->writeElement('cntNode', 0);
        $this->xml->writeElement('cs', 0);
        $this->xml->writeElement('titleid', $this->document->getJournal()->getRincId());
        $this->xml->endElement();
    }
    
    public function generateMetaData()
    {
        $journal = $this->document->getJournal();
        $publisher = $journal->getPublisher();

        $this->xml->writeElement('titleid', $journal->getRincId());
        $this->xml->writeElement('publ', $publisher->getShortTitle());
        $this->xml->writeElement('publId', $publisher->getRincId());
        if (null !== $this->document->getDatePublished()) {
            $this->xml->writeElement('DateUni', $this->document->getDatePublished()->format('Ymd'));
        }
        $this->xml->writeElement('placePubl', $publisher->getTown());
        $this->xml->writeElement('pages', $this->document->getPageCount());
        $this->xml->writeElement('issn', $journal->getIssnPrint());
        $this->xml->writeElement('codeNEB', str_replace('-', '', $journal->getIssnPrint()));
        $this->xml->startElement('journalInfo');
        $this->xml->writeAttribute('lang', 'RUS');
        $this->xml->writeElement('title', $journal->getRu()['title']);
        $this->xml->endElement();
    }
    
    public function generateContent()
    {
        $this->xml->startElement('issue');
        $this->xml->writeElement('volume', $this->document->getVolume());
        $this->xml->writeElement('number', $this->document->getIssue());
        $this->xml->writeElement('part', $this->document->getPart());
        $this->xml->writeElement('pages', $this->document->getPageCount());

        $this->xml->startElement('articles');
        foreach ($this->document->getIssuerubriclist() as $rubric_list) {
            $rubric = $rubric_list->getRubric();


            $this->xml->startElement('section');

            $this->xml->startElement('secTitle');
            $this->xml->writeAttribute('lang', 'RUS');
            $this->xml->text($rubric->getRu()['title']);
            $this->xml->endElement();

            $this->xml->startElement('secTitle');
            $this->xml->writeAttribute('lang', 'ENG');
            $this->xml->text($rubric->getEn()['title']);
            $this->xml->endElement();
            $this->xml->endElement();

            $articles = $this->document->getArticlesByRubric($rubric);
            foreach ($articles as $article) {
                $this->xml->startElement('article');
                $this->xml->writeElement('pages', $article->getFirstPage().' - '.$article->getLastPage());
                $this->xml->writeElement('artType', 'RAR');

                $this->collectAuthors($article->getAuthors());

                $this->xml->startElement('artTitles');
                $this->xml->startElement('artTitle');
                $this->xml->writeAttribute('lang', 'RUS');
                $this->xml->text($article->getRu()['title']);
                $this->xml->endElement();

                $this->xml->startElement('artTitle');
                $this->xml->writeAttribute('lang', 'ENG');
                $this->xml->text($article->getEn()['title']);
                $this->xml->endElement();

                $this->xml->endElement();

                $this->xml->startElement('abstracts');
                $this->xml->startElement('abstract');
                $this->xml->writeAttribute('lang', 'RUS');
                $this->xml->text($article->getRu()['annotation']);
                $this->xml->endElement();

                $this->xml->startElement('abstract');
                $this->xml->writeAttribute('lang', 'ENG');
                $this->xml->text($article->getEn()['annotation']);
                $this->xml->endElement();
                $this->xml->endElement();

                $this->generateCodes($article);

                $this->metaKeywords($article);
                $this->metaReferences($article);

                $this->xml->endElement();
            }
        }
        $this->xml->endElement();
        $this->xml->endElement();
    }
}
