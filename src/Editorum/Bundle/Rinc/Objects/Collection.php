<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 27.06.2016
 * Time: 14:35
 */

namespace Editorum\Bundle\Rinc\Objects;

use Editorum\Bundle\Document\Collection as CollectionDocument;
use Editorum\Bundle\Rinc\AbstractJournalRinc;

/**
 * Rinc generator for Collection document
 * Class Collection
 * @package Editorum\Bundle\Rinc
 */
class Collection extends AbstractJournalRinc
{
    /**
     * RincCollection constructor.
     * @param CollectionDocument $collection
     */
    public function __construct(CollectionDocument $collection)
    {
        parent::__construct();
        $this->document = $collection;
    }


    public function generateOperCard()
    {
        parent::abstractOperCard(count($this->document->getArticles()));
    }

    
    public function generateMetaData()
    {
        $publisher = $this->document->getPublisher();

        $this->xml->writeElement('bookType', 'COL');
        $this->xml->writeElement('publ', $publisher->getShortTitle());
        $this->xml->writeElement('publId', $publisher->getRincId());
        if (null !== $this->document->getDatePublished()) {
            $this->xml->writeElement('DateUni', $this->document->getDatePublished()->format('Ymd'));
        }
        $this->xml->writeElement('placePubl', $publisher->getTown());
        $this->xml->writeElement('pages', $this->document->getPublishedPageTotal());
        $this->xml->writeElement('isbn', $this->document->getIsbn());
        $this->xml->writeElement('codeNEB', str_replace('-', '', $this->document->getIsbn()));

        $this->xml->startElement('journalInfo');
        $this->xml->writeAttribute('lang', 'RUS');
        $this->xml->writeElement('title', $this->document->getRu()['title']);
        $this->xml->writeElement('Конференция');
        $this->xml->endElement();
    }

    
    public function generateContent()
    {
        $this->xml->startElement('articles');

        foreach ($this->document->getCollectionSectionList() as $section_list) {
            $section = $section_list->getSection();
            $this->xml->startElement('section');

            $this->xml->startElement('secTitle');
            $this->xml->writeAttribute('lang', 'RUS');
            $this->xml->text($section->getRu()['title']);
            $this->xml->endElement();

            $this->xml->startElement('secTitle');
            $this->xml->writeAttribute('lang', 'ENG');
            $this->xml->text($section->getEn()['title']);
            $this->xml->endElement();

            $this->xml->endElement();

            foreach ($this->document->getArticlesBySection($section) as $article) {
                $this->xml->startElement('article');
                $this->xml->writeElement('pages', $article->getFirstPage().' - '.$article->getLastPage());
                $this->xml->writeElement('artType', 'RAR');

                $this->collectAuthors($article->getAuthors());

                $this->xml->startElement('artTitles');
                $this->xml->startElement('artTitle');
                $this->xml->writeAttribute('lang', 'RUS');
                $this->xml->text($article->getRu()['title']);
                $this->xml->endElement();

                $this->xml->startElement('artTitle');
                $this->xml->writeAttribute('lang', 'ENG');
                $this->xml->text($article->getEn()['title']);
                $this->xml->endElement();

                $this->xml->endElement();

                $this->xml->startElement('abstracts');
                $this->xml->startElement('abstract');
                $this->xml->writeAttribute('lang', 'RUS');
                $this->xml->text($article->getRu()['annotation']);
                $this->xml->endElement();

                $this->xml->startElement('abstract');
                $this->xml->writeAttribute('lang', 'ENG');
                $this->xml->text($article->getEn()['annotation']);
                $this->xml->endElement();
                $this->xml->endElement();

                $this->xml->startElement('codes');
//                $this->xml->writeElement('udk', $article->getUdk());
//                $this->xml->writeElement('bbk', $pub->getBbk());
//                $this->xml->writeElement('grnti', implode(',', $pub->__get('classif_grnti')));
                $this->xml->writeElement('doi', $article->getDoi());
                $this->xml->endElement();

                $this->metaKeywords($article);
                $this->generateReferences($article);

                $this->xml->endElement();
            }
        }
        $this->xml->endElement();
    }
}
