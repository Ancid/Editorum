<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 27.06.2016
 * Time: 14:33
 */

namespace Editorum\Bundle\Rinc;

/**
 * Class AbstractJournalRinc
 * @package Editorum\Bundle\Rinc
 */
abstract class AbstractJournalRinc extends AbstractRinc
{
    public $xml = false;
    public $document = false;

    abstract public function generateOperCard();

    abstract public function generateMetaData();

    abstract public function generateContent();
    
    final public function generateXml()
    {
        $this->xml->openMemory();
        $this->xml->setIndent(true);
        $this->xml->startDocument('1.0');
        $this->xml->startElement('journal');
        
        $this->generateOperCard();
        $this->generateMetaData();
        $this->generateContent();
        
        $this->xml->endElement();

        return $this->clearEmptyTags($this->xml->outputMemory());
    }
}
