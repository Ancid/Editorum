<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 27.06.2016
 * Time: 15:10
 */

namespace Editorum\Bundle\Rinc;

class RincFactory
{
    public static function build($object)
    {
        $function = new \ReflectionClass(get_class($object));
        $rinc_object = 'Editorum\\Bundle\\Rinc\\Objects\\' . $function->getShortName();
        if (class_exists($rinc_object)) {
            return new $rinc_object($object);
        } else {
            throw new \Exception("Отсутствует РИНЦ-объект для " . $function->getShortName().'('.$rinc_object.')');
        }
    }
}
