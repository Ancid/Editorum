<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 29.06.2016
 * Time: 16:14
 */

namespace Editorum\Bundle\Rinc;

use Editorum\Bundle\Document\Reference;


abstract class AbstractRinc
{
    public $xml = false;
    protected $document = false;


    /**
     * AbstractRinc constructor.
     */
    public function __construct()
    {
        $this->xml = new \XMLWriter();
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getXmlTitle()
    {
        return $this->document->getId();
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getZipTitle()
    {
        if (null === $this->document->getPublisher()) {
            throw new \Exception('У произведения нет издателя!');
        }
        if (null == $this->document->getPublisher()->getRincTitle()) {
            throw new \Exception('Проверьте поле rinc_title у издателя.');
        }
        return $this->document->getPublisher()->getRincTitle();
    }

    /**
     * Похоже какая то нормализация
     * @param $xml
     * @return string
     */
    public function clearEmptyTags($xml)
    {
        $doc = new \DOMDocument();
        $doc->preserveWhiteSpace = false;
        $doc->loadXML($xml);
        $doc->encoding = 'UTF-8';
        $xpath = new \DOMXPath($doc);
        foreach ($xpath->query('//*[not(node())]') as $node) {
            $node->parentNode->removeChild($node);
        }
        $doc->formatOutput = true;
        return $doc->saveXML();
    }


    /**
     * @param $count
     */
    public function abstractOperCard($count)
    {
        $this->xml->startElement('operCard');
        $this->xml->writeElement('operator', 'user');
        $this->xml->writeElement('pid', $this->document->getId());
        $this->xml->writeElement('date', date('Y-m-d H:i:s'));
        $this->xml->writeElement('cntArticle', $count);
        $this->xml->writeElement('cntNode', 0);
        $this->xml->writeElement('cs', 0);
        $this->xml->endElement();
    }


    /**
     * Авторы
     * @param $authors
     */
    protected function collectAuthors($authors)
    {
        $this->xml->startElement('authors');
        $num = 1;
        foreach ($authors as $author) {
            $this->xml->startElement('author');
            $this->xml->writeAttribute('num', $num);

            $this->checkLangField('first_name', $author);
            $this->checkLangField('last_name', $author);

            $this->xml->startElement('individInfo');
            $this->xml->writeAttribute('lang', 'RUS');
            $this->xml->writeElement('surname', $author->getRu()['last_name']);
            $this->xml->writeElement('initials', $author->getRu()['first_name'].' '.$author->getRu()['second_name']);
            if (array_key_exists('work_title', $author->getRu())) {
                $this->xml->writeElement('orgName', $author->getRu()['work_title']);
            }
            $this->xml->writeElement('email', $author->getEmail());
            $this->xml->endElement();

            $this->xml->startElement('individInfo');
            $this->xml->writeAttribute('lang', 'ENG');
            $this->xml->writeElement('surname', $author->getEn()['last_name']);
            $this->xml->writeElement('initials', $author->getEn()['first_name']);
            if (array_key_exists('work_title', $author->getEn())) {
                $this->xml->writeElement('orgName', $author->getEn()['work_title']);
            }
            $this->xml->writeElement('email', $author->getEmail());
            $this->xml->endElement();

            $this->xml->endElement();
            $num++;
        }
        $this->xml->endElement();
    }


    /**
     * @param $article
     */
    protected function metaKeywords($article)
    {
        $kewds = array_merge(
            preg_split("/[;,]+/", $article->getRu()['keywords']),
            preg_split("/[;,]+/", $article->getEn()['keywords'])
        );
        $this->xml->startElement('keywords');
        $this->xml->startElement('kwdGroup');
        $this->xml->writeAttribute('lang', 'ANY');
        foreach ($kewds as $kewd) {
            $this->xml->writeElement('keyword', trim($kewd));
        }
        $this->xml->endElement();
        $this->xml->endElement();
    }


    /**
     * @param $article
     */
    protected function metaReferences($article)
    {
        $this->xml->startElement('references');
        /** @var Reference $ref */
        foreach ($article->getReferences() as $ref) {
            $this->xml->writeElement('reference', $ref->getRu()['text']);
        }
        $this->xml->endElement();
    }


    /**
     * @param $field
     * @param $document
     * @throws \Exception
     */
    protected function checkLangField($field, $document)
    {
        if (!array_key_exists($field, $document->getRu())) {
            throw new \Exception('Пустое поле ru['.$field.'] у сущности '.$document->getObjectName().
                '[id='.$document->getId().']');
        }
        if (!array_key_exists($field, $document->getEn())) {
            throw new \Exception('Пустое поле en['.$field.'] у сущности '.$document->getObjectName().
                '[id='.$document->getId().']');
        }
    }


    /**
     * Ссылки на источники
     * @param $document
     */
    protected function generateReferences($document)
    {
        $this->xml->startElement('references');
        /** @var Reference $ref */
        foreach ($document->getReferences() as $ref) {
            $this->xml->writeElement('reference', $ref->getRu()['text']);
        }
        $this->xml->endElement();
    }

    /**
     * @param $document
     */
    public function generateCodes($document)
    {
        $this->xml->startElement('codes');
        if (null !== $document->getUdk()) {
            $this->xml->writeElement('udk', $document->getUdk()[0]);
        }
        if (null !== $document->getGrnti()) {
            $this->xml->writeElement('grnti', $document->getGrnti()[0]);
        }
        $this->xml->writeElement('doi', $document->getDoi());
        $this->xml->endElement();
    }
}
