/**
 * Created by sidorov_ar on 30.07.2015.
 */

/**
 * Модель данных для автора
 * @constructor
 */
var AuthorModel = function() {
    var that = this;
    this.entity_name = 'author';


    this.getList = function(dataset_name, params) {
        return BasicModel.prototype.getList.apply(that, arguments);
    }

    this.read = function(id) {
        return BasicModel.prototype.read.apply(that, arguments);
    }

    this.create = function() {
        return BasicModel.prototype.create.apply(that, arguments);
    }

    this.save = function() {
        return BasicModel.prototype.save.apply(that);
    }
}

inheritsFrom(AuthorModel, BasicModel);