/**
 * Created by xor on 18.06.15.
 */
var TopMenuBlock = function () {
    var that = this;

    this.block_name = 'top_menu';

    this.onDraw = function() {
        that.loadTemplate();
    }
}

inheritsFrom(TopMenuBlock, BasicBlock);