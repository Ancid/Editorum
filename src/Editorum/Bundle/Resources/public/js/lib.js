/**
 * Created by Xor on 24.08.2015.
 */
function save_data(form_id, url, callback) {
    var data = {};
    $('#' + form_id + ' :input:not([type=image],[type=button],[type=submit],button)').each(function () {
        var $this = $(this),
            id = $this.attr('id'),
            text = $this.val();
        //для радио батонов
        if (id == undefined) {
            if ($this.prop('checked') == true) {
                data[$this.attr('name')] = text;
            }
        } else {
            data[id] = text;
        }
    });
    var tosend = JSON.stringify(data);
    var output = 0;

    $('.json_button').attr('disabled', 'true');

    $.ajax(url, {async: false, data: {data: tosend}, type: 'POST', dataType: 'json'})
        .done(function (result) {
            try {
                var res = jQuery.parseJSON(result);
                output = res.id;
            }
            catch (err) {
                alert('error!');
                console.log(err);
            }
            $('.json_button').removeAttr('disabled');
            if (callback !== false) {
                callback = callback.replace('%id%', res['id']);
                window.location.href = callback;
            }
        })
        .fail(function () {
            alert('Ошибка сохранения')
        });
    return output;
}

/**
 * Сохранение формы с расширенными результатами действий
 * @param form_id
 * @param url
 * @param callback
 * @returns {number}
 */
function submit_data(form_id, url, callback) {
    var data = {};

    $('#' + form_id + ' input,select,textarea').each(function () {
        data[$(this).attr('id')] = $(this).val();
    });

    var tosend = JSON.stringify(data);
    var output = 0;

    $('.json_button').attr('disabled', 'true');

    $.ajax(url, {async: false, data: {data: tosend}, type: 'POST'})
        .done(function (result) {
            try {
                var res = jQuery.parseJSON(result);
            }
            catch (err) {
                alert('Ошибочный JSON ответ сервера');
            }
            $('.json_button').removeAttr('disabled');

            // Осуществляем действие в зависимости от ответа сервера
            console.log(res);

            // Если ошибка, то выводим
            if (res.do == 'error') {
                alert(result);
            }

            // Если обновление, то рефрешим страницу
            if (res.do == 'refresh') {
                window.location.reload();
            }

            // Если переход, то переходим
            if (res.do == 'go') {
                window.location.href = res.url;
            }

            // Если отправили код, ждем пока подтвердит
            if (res.do == 'code' && typeof(callback) === "function") {
                callback();
            }
        })
        .fail(function () {
            alert('Ошибка сохранения')
        });
    return output;
}

function validation_clear(fields) {
    console.log('Clear validation markup');
    for (var i = 0; i < fields.length; i++) {
        console.log(fields[i]);
        $('#' + fields[i] + '_error').addClass('bf-hidden');
        $('#' + fields[i] + '_ok').addClass('bf-hidden');
        $('#' + fields[i] + '_div').removeClass('has-success');
        $('#' + fields[i] + '_div').removeClass('has-error');
    }
}

function validate_empty(fields) {
    var errors = 0;
    for (var i = 0; i < fields.length; i++) {
        var tagName = $('#' + fields[i])[0].tagName;

        //console.log(fields[i], tagName);
        if (tagName == 'INPUT') {
            if (($('#' + fields[i]).val().length < 1) || ($('#' + fields[i]).val() == '0')) {
                $('#' + fields[i] + '_error').removeClass('bf-hidden');
                $('#' + fields[i] + '_ok').addClass('bf-hidden');
                $('#' + fields[i] + '_div').addClass('has-error');
                $('#' + fields[i] + '_div').removeClass('has-success');
                errors++;
            }
            else {
                $('#' + fields[i] + '_error').addClass('bf-hidden');
                $('#' + fields[i] + '_ok').removeClass('bf-hidden');
                $('#' + fields[i] + '_div').addClass('has-success');
                $('#' + fields[i] + '_div').removeClass('has-error');
            }
        }
        if (tagName == 'SELECT') {
            if ($('#' + fields[i]).val() == 0) {
                console.log('Empty');
                $('#' + fields[i] + '_error').removeClass('bf-hidden');
                $('#' + fields[i] + '_ok').addClass('bf-hidden');
                $('#' + fields[i] + '_div').addClass('has-error');
                $('#' + fields[i] + '_div').removeClass('has-success');
                errors++;
            }
            else {
                console.log('Not empty');
                $('#' + fields[i] + '_error').addClass('bf-hidden');
                $('#' + fields[i] + '_ok').removeClass('bf-hidden');
                $('#' + fields[i] + '_div').addClass('has-success');
                $('#' + fields[i] + '_div').removeClass('has-error');
            }
        }
    }
    return errors;
}

function validate_keywords(fields) {
    var errors = 0;
    for (var i = 0; i < fields.length; i++) {

        if (fields[i] == 'keywords' || fields[i] == 'eng_keywords') {

            if ($('#' + fields[i]).val().split(/[.,]+/).length > 3) {
                $('#' + fields[i] + '_error').removeClass('bf-hidden');
                $('#' + fields[i] + '_ok').addClass('bf-hidden');
                $('#' + fields[i] + '_div').addClass('has-error');
                $('#' + fields[i] + '_div').removeClass('has-success');
                errors++;
            } else {
                $('#' + fields[i] + '_error').addClass('bf-hidden');
                $('#' + fields[i] + '_ok').removeClass('bf-hidden');
                $('#' + fields[i] + '_div').addClass('has-success');
                $('#' + fields[i] + '_div').removeClass('has-error');
            }
        }
    }
    return errors;
}

function remove_data(id, url, callback) {
    var data = {'_id': id};
    console.log(data._id, url);
    var output = 0;

    var tosend = JSON.stringify(data);
    //var output = 0;

    if (confirm('Точно удалить ?')) {
        $.ajax(url, {async: false, data: {data: tosend}, type: 'POST'})
            .done(function (result) {
                try {
                    var res = jQuery.parseJSON(result);
                    console.log(res.res)
                    output = res;
                }
                catch (err) {
                    alert('err'.result);
                }
                //$('.json_button').removeAttr('disabled');
                if (callback !== false) {
                    callback = callback.replace('%id%', res['id']);
                    window.location.href = callback;
                }
            })
            .fail(function () {
                alert('Ошибка удаления')
            });
    }
    return output;
}

$(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

/**
 * Функция для радио кнопок
 * @param name Строковый параметр из data-toggle для привязки к отдельной группе кнопок
 */
var radioButton = function(name, invert) {
    $('.btn-group[data-toggle="'+name+'"]').find('.btn').click(function () {
        $(this).closest('.btn-group').find('.btn').removeClass('active');
        $(this).addClass('active');
        var hiddenIdName = 'input[name='+$(this).parent().attr('data-toggle-name')+']';
        if ($(hiddenIdName).val() == 0) {
            $(hiddenIdName).val(1);
            if (invert === true) {
                $(hiddenIdName).val(0);
            }
        } else {
            $(hiddenIdName).val(0);
            if (invert === true) {
                $(hiddenIdName).val(1);
            }
        }
    });
};


(function ($) {
    // Показать или спрятать элемент в зависимости от положения переключателя
    // Добавил возможность инвертировать кнопки. Антон.
    $.fn.showAndHide = function (showElement, inverse) {
        inverse = inverse || false;
        var yes = 1, no = 0;

        if (Boolean(inverse)) {
            yes = 0;
            no = 1;
        }

        var check = function (showElement, object) {
            if ($(object).val() == no) {
                $(showElement).hide(200).find('input').prop('disabled', true);
            }
            if ($(object).val() == yes) {
                $(showElement).show(200).find('input').prop('disabled', false);
            }
        };

        this.each(function () {
            var input = $(this).find('input:checked');
            check(showElement, input);

            $(this).on('change', function () {
                var input = $(this).find('input:checked');
                check(showElement, input);
            });
        });

        return this;
    };

    // Показать один или другой элемент в зависимости от переключателя
    $.fn.showBySelect = function (showElement1, showElement2) {
        var check = function (showElement1, showElement2, object) {
            if ($(object).val() == 0) {
                $(showElement1).hide();
                $(showElement2).show();
            }
            if ($(object).val() == 1) {
                $(showElement2).hide();
                $(showElement1).show();
            }
        };

        this.each(function () {
            var input = $(this).find('input:checked');
            check(showElement1, showElement2, input);

            $(this).on('change', function () {
                var input = $(this).find('input:checked');
                check(showElement1, showElement2, input);
            });
        });

        return this;
    };


    // Плагин для селектов бутстрапа, отключить один, если условие другого не совпадает
    $.fn.disableSelect = function (disableElement, needle) {
        if (isNaN(needle)) {
            needle = this.find("option:contains('Россия')").val();
        }

        var disableSelect = function (object, target) {
            if ($(object).selectpicker('val') == needle) {
                $(target).removeAttr("disabled");
                $(target).selectpicker('render');
            } else {
                $(target).attr("disabled", "disabled");
                $(target).val(0);
            }

            $(target).selectpicker('refresh');
            $(target).selectpicker('render');

        };

        if (this.find('option:selected').val() !== needle) {
            disableSelect(this, $(disableElement));
        }

        this.on('changed.bs.select', function () {
            disableSelect($(this), $(disableElement));
        });

        return this;
    };

    // Добавить маски скопом
    $.maskAll = function (object) {
        for (el in object) {
            $(el).mask(object[el]);
        }

        return this;

    };


    // Плагин пошагового перехода в табах
    $.fn.tabbedStep = function (callable) {
        if (!callable) {
            callable = function (tabIndex) {
                return true;
            };
        }

        var makeStep = function (object, current, max) {
            var size = '6';
            var haveSizeBefore = false;
            object.find('.btnNext').unbind();
            object.find('.btnPrevious').unbind();
            object.empty();
            object.html('<hr />');

            if (current > 0) {
                object.append('<div class="col-md-' + size + ' text-left">' + '<a class="btn btn-primary btnPrevious">Назад</a>' + '</div>');

                haveSizeBefore = true;
            }

            if (current < max) {
                if (!haveSizeBefore) {
                    size = '12';
                }

                object.append('<div class="col-md-' + size + ' text-right">' + '<a class="btn btn-primary btnNext">Вперед</a>' + '</div>');
            }

            if (current == max) {
                if (!haveSizeBefore) {
                    size = '12';
                }

                object.append('<div class="col-md-' + size + ' text-right">' + '<button class="btn btn-success" type="submit">Сохранить</button>' + '</div>');
            }

            $('.nav-tabs > li').each(function () {
                var handler = function () {
                    current = $(this).index();
                    makeStep(object, current, max);
                };

                $(this).unbind();
                $(this).on('click', handler);

            });


            object.find('.btnNext').on('click', function () {
                var currentTabNum = current + 1;
                if (callable(current)) {
                    makeStep(object, currentTabNum, max);
                    $('.nav-tabs > .active').next('li').find('a').trigger('click');
                }
            });


            object.find('.btnPrevious').on('click', function () {
                var currentTabNum = current - 1;
                makeStep(object, currentTabNum, max);
                $('.nav-tabs > .active').prev('li').find('a').trigger('click');
            });
        };
        makeStep(this, $('.nav-tabs > .active').index(), $('.nav-tabs').find('li').length - 1);
    };
})(jQuery);