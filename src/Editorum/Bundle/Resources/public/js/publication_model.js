/**
 * Created by xor on 16.06.15.
 */


/**
 * Модель данных для произведения
 * @constructor
 */
var PublicationModel = function() {
    var that = this;
    this.entity_name = 'pubs';


    this.getList = function(dataset_name, params) {
        return BasicModel.prototype.getList.apply(that, arguments);
    }

    this.read = function(id) {
        return BasicModel.prototype.read.apply(that, arguments);
    }

    this.create = function() {
        return BasicModel.prototype.create.apply(that, arguments);
    }

    this.save = function() {
        return BasicModel.prototype.save.apply(that);
    }
}

inheritsFrom(PublicationModel, BasicModel);