/**
 * Created by sidorov_ar on 30.07.2015.
 */
var AuthorController = function() {
    var that = this;

    var current_publication = -1;

    this.load = function(params) {
        //console.log(params);

        bframe.models['pubs'].read(params[0]);
        //console.log('+++++', bframe.models['pubs'].record);
        bframe.go('publication.metadata');
    }

    /**
     * Список произведений
     */
    this.list = function() {
        bframe.loadLayout('fullscreen', {
            topmenu:        ['TopMenuBlock'],
            main:     ['AuthorListBlock']
        });
    }

    /**
     * Создать произведение
     */
    this.create = function() {
        bframe.loadLayout('fullscreen', {
            topmenu:        ['TopMenuBlock'],
            main:     ['AuthorCreateBlock']
        });
    }
}



