$(function(){
    // Check if selector exists
    $.fn.exists = function(callback) {
        var args = [].slice.call(arguments, 1);
        if (this.length) {
            callback.call(this, args);
        }
        return this;
    };

    // Selectpicker init
    $('select').exists(function(){
        $(this).selectpicker();
    });

    // Date
    var date = new Date(),
        year = date.getFullYear(),
        month = ('0' + date.getMonth()).slice(-2),
        day = ('0' + date.getDate()).slice(-2),
        today = day + '.' + month + '.' + year;
    $('.year').text(year);

    // Datepicker init
    $('[data-provide="datepicker"]').exists(function(){
        $(this).datepicker({language: 'ru', format: 'dd.mm.yyyy'}).on('changeDate', function() {
            var form = $(this).closest('form'),
                field = $(this).find(':text').attr('name');

            form.formValidation('revalidateField', field);
        });
        $(this).find(':text').mask('99.99.9999');
    });

    // File Upload init
    //$('input:file').exists(function(){
    //    $(this).fileinput({
    //        language: 'ru',
    //        previewFileIcon: '<span class="fa fa-file"></span>',
    //        allowedPreviewTypes: null,
    //        previewFileIconSettings: {
    //            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
    //            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
    //            'rar': '<i class="fa fa-file-archive-o text-muted"></i>'
    //        }
    //    });
    //});

    // Checkboxes
    var checkbox = $('.checkbox');
    checkbox.exists(function(){
        checkbox.find(':checkbox').each(function(){
            if($(this).is(':checked')) $(this).closest(checkbox).addClass('checked');
        });

        checkbox.find(':checkbox').change(function(){
            var parent = $(this).closest(checkbox);
            $(this).is(':checked') ? parent.addClass('checked') : parent.removeClass('checked');
        });
    });

    $('.media-list.list-group').exists(function(){
        var checboxes = $(this).find('.media').find(':checkbox');

        checboxes.prop('checked', false);
        checboxes.change(function(){
            var btn = $(this).closest('.btn'),
                row =  $(this).closest('.btn-group').next('a');

            if($(this).is(':checked')){
                btn.addClass('active');
                row.addClass('alert-success');
            }else{
                btn.removeClass('active');
                row.removeClass('alert-success');
            }
        });
    });

    // Spinners
    var spinner = $('.spinner');
    spinner.exists(function(){
        var validateSpinner = function(input){
            var form = input.closest('form'),
                field = input.attr('name');

            form.formValidation('revalidateField', field);
        }

        spinner.find(':text').mask('999999');
        spinner.find('.btn:first-of-type').on('click', function() {
            var inp = $(this).closest('.spinner').find(':text');
            inp.val() == '' ? inp.val(1) : inp.val( parseInt(inp.val(), 10) + 1);
            validateSpinner(inp);
        });
        spinner.find('.btn:last-of-type').on('click', function(e) {
            var inp = $(this).closest('.spinner').find(':text');
            parseInt(inp.val(), 10) > 0 ? inp.val(parseInt(inp.val(), 10) - 1) : e.preventDefault();
            validateSpinner(inp);
        });
    });

    // Scroll to top
    var up = $('#up');
    up.click(function(){
        $('body, html').animate({
            scrollTop: 0
        });
    });

    if($(window).scrollTop() > 500) up.removeClass('hide');
    $(window).scroll(function(){
        $(this).scrollTop() > 500 ? up.removeClass('hide') : up.addClass('hide');
    });


    // DRAG & DROP
    $('[data-draggable="target"]').exists(function(){
        if(!document.querySelectorAll || !('draggable' in document.createElement('span')) || window.opera) return;

        for(var targets = document.querySelectorAll('[data-draggable="target"]'), len = targets.length, i = 0; i < len; i ++){
            targets[i].setAttribute('aria-dropeffect', 'none');
        }

        for(var items = document.querySelectorAll('[data-draggable="item"]'), len = items.length, i = 0; i < len; i ++){
            items[i].setAttribute('draggable', 'true');
            items[i].setAttribute('aria-grabbed', 'false');
            items[i].setAttribute('tabindex', '0');
        }

        var selections = {
            items      : [],
            owner      : null,
            droptarget : null
        };

        function addSelection(item){
            if(!selections.owner){
                selections.owner = item.parentNode;
            }else if(selections.owner != item.parentNode){
                return;
            }
            item.setAttribute('aria-grabbed', 'true');
            selections.items.push(item);
        }

        function removeSelection(item){
            item.setAttribute('aria-grabbed', 'false');
            for(var len = selections.items.length, i = 0; i < len; i ++){
                if(selections.items[i] == item){
                    selections.items.splice(i, 1);
                    break;
                }
            }
        }

        function clearSelections(){
            if(selections.items.length){
                selections.owner = null;
                for(var len = selections.items.length, i = 0; i < len; i ++){
                    selections.items[i].setAttribute('aria-grabbed', 'false');
                }
                selections.items = [];
            }
        }

        function addDropeffects(){
            for(var len = targets.length, i = 0; i < len; i ++){
                if(targets[i] != selections.owner && targets[i].getAttribute('aria-dropeffect') == 'none'){
                    targets[i].setAttribute('aria-dropeffect', 'move');
                    targets[i].setAttribute('tabindex', '0');
                }
            }

            for(var len = items.length, i = 0; i < len; i ++){
                if(items[i].parentNode != selections.owner && items[i].getAttribute('aria-grabbed')){
                    items[i].removeAttribute('aria-grabbed');
                    items[i].removeAttribute('tabindex');
                }
            }
        }

        function clearDropeffects(){
            if(selections.items.length){
                for(var len = targets.length, i = 0; i < len; i ++){
                    if(targets[i].getAttribute('aria-dropeffect') != 'none'){
                        targets[i].setAttribute('aria-dropeffect', 'none');
                        targets[i].removeAttribute('tabindex');
                    }
                }

                for(var len = items.length, i = 0; i < len; i ++){
                    if(!items[i].getAttribute('aria-grabbed')){
                        items[i].setAttribute('aria-grabbed', 'false');
                        items[i].setAttribute('tabindex', '0');
                    }else if(items[i].getAttribute('aria-grabbed') == 'true'){
                        items[i].setAttribute('tabindex', '0');
                    }
                }
            }
        }

        function getContainer(element){
            do{
                if(element.nodeType == 1 && element.getAttribute('aria-dropeffect')){
                    return element;
                }
            }
            while(element = element.parentNode);
            return null;
        }

        document.addEventListener('mousedown', function(e){
            if(e.target.getAttribute('draggable')){
                clearDropeffects();
                if(e.target.getAttribute('aria-grabbed') == 'false'){
                    addSelection(e.target);
                }else if(e.target.getAttribute('aria-grabbed') == 'true'){
                    removeSelection(e.target);
                    if(!selections.items.length){
                        selections.owner = null;
                    }
                }
            }
        }, false);

        document.addEventListener('dragstart', function(e){
            if(selections.owner != e.target.parentNode){
                e.preventDefault();
                return;
            }
            if(e.target.getAttribute('aria-grabbed') == 'false'){
                addSelection(e.target);
            }
            e.dataTransfer.setData('text', '');
            addDropeffects();
        }, false);

        var related = null;

        document.addEventListener('dragenter', function(e){
            related = e.target;
        }, false);

        document.addEventListener('dragleave', function(e){
            var droptarget = getContainer(related);
            if(droptarget == selections.owner){
                droptarget = null;
            }
            if(droptarget != selections.droptarget){
                if(selections.droptarget){
                    selections.droptarget.className = selections.droptarget.className.replace(/ dragover/g, '');
                }
                if(droptarget){
                    droptarget.className += ' dragover';
                }
                selections.droptarget = droptarget;
            }
        }, false);

        document.addEventListener('dragover', function(e){
            if(selections.items.length){
                e.preventDefault();
            }
        }, false);

        document.addEventListener('dragend', function(e){
            if(selections.droptarget){
                for(var len = selections.items.length, i = 0; i < len; i ++){
                    selections.items[i].className += ' list-group-item-success';
                    selections.droptarget.appendChild(selections.items[i]);
                }
                e.preventDefault();
            }

            if(selections.items.length){
                clearDropeffects();
                if(selections.droptarget){
                    clearSelections();
                    selections.droptarget.className = selections.droptarget.className.replace(/ dragover/g, '');
                    selections.droptarget = null;
                }
            }
        }, false);

        document.getElementById('move_left').addEventListener('click', function(e){
            for(var len = selections.items.length, i = 0; i < len; i ++){
                selections.items[i].className += ' list-group-item-success';
                document.querySelectorAll('[data-draggable="target"]')[0].appendChild(selections.items[i]);
            }
            clearDropeffects();
            clearSelections();
            e.preventDefault();
        }, false);

        document.getElementById('move_right').addEventListener('click', function(e){
            for(var len = selections.items.length, i = 0; i < len; i ++){
                selections.items[i].className += ' list-group-item-success';
                $('.right').find('[data-draggable="target"]').append(selections.items[i]);
            }
            clearDropeffects();
            clearSelections();
            e.preventDefault();
        }, false);

    });
});