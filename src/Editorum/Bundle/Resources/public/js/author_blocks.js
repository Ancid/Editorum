/**
 * Created by sidorov_ar on 30.07.2015.
 */

var AuthorListBlock = function() {
    var that = this;

    this.block_name = 'author_list';

    this.onDraw = function() {
        console.log('~~~ondraw!!');
        that.loadTemplate();
        var model = bframe.models['pubs'];
        model.getList('publist', {});

        $('#author_list_create').on('click', function(e) {
            bframe.go('author.create');
            e.preventDefault();
        });

        $('#author_list_content').empty();
        for (var i in model.data)
        {
            var r = model.data[i];
            $('#author_list_content')
                .append('<tr></td><td>'+ r.type+'</td><td><a href="#author.load.'+ r._id['$id']+'">'+ r.title+'</a></td><td>'+r._id['$id']+'</td></tr>');
        }
    }
}

inheritsFrom(AuthorListBlock, ListBlock);


/**
 * Блок создания произведения
 * @constructor
 */
var AuthorCreateBlock = function () {
    var that = this;

    this.block_name = 'author_create';
    this.model = 'author';

    this.onDraw = function() {
        that.loadTemplate();
        $('#gfirst_name').on('change', this.validate);

        var model = bframe.models['pubs'];

        $('#author_create_btn').on('click touchstart', function() {
            model.create({
                title:  $('#i_title').val(),
                type:   $('#i_type').val()
            });
            var id = model.save();
        });

        $('#i_dont_work').on('click', function() {
            //if ($(this).hasClass('btn-default')) {
                $(this).addClass('btn-primary');
            $('#gvuz_order').show();
            $('#glabel1').show();
            $('#gunivercity_id').show();
            $('#glabel2').show();

            $('#gwork_title').hide();
            $('#gwork_country').hide();
            $('#gwork_country').hide();
            $('#gwork_city').hide();
            $('#geng_work_title').hide();
            $('#geng_work_city').hide();
            //}

        });
        $('#i_work').on('click', function() {
            //if ($(this).hasClass('btn-default')) {
                $(this).addClass('btn-primary');
                $('#i_dont_work').removeClass('btn-primary');

            $('#i_work').removeClass('btn-primary');
            $('#gvuz_order').hide();
            $('#glabel1').hide();
            $('#gunivercity_id').hide();
            $('#glabel2').hide();

            $('#gwork_title').show();
            $('#gwork_country').show();
            $('#gwork_country').show();
            $('#gwork_city').show();
            $('#geng_work_title').show();
            $('#geng_work_city').show();
            //}
        });

        $('#exist_vuz').on('click', function(){
            $(this).addClass('btn-primary');
            $('#custom_vuz').removeClass('btn-primary');

            $('#gunivercity_title').hide();
            $('#gunivercity_eng_title').hide();
            $('#gvuz_adress').hide();
            $('#gunivercity_eng_city').hide();
        });

        $('#custom_vuz').on('click', function(){
            $(this).addClass('btn-primary');
            $('#exist_vuz').removeClass('btn-primary');

            $('#gunivercity_title').show();
            $('#gunivercity_eng_title').show();
            $('#gvuz_adress').show();
            $('#gunivercity_eng_city').show();
        });
    }

    this.onEnter = function() {
        //that.fillForm();
    }

    this.validate = function() {
        var cc = 0;
        if ($('#gfirst_name').val() !== 'none') {
            $('#gfirst_name').removeClass('ed-message-error').addClass('ed-message-success');
            cc++;
        }
        else {
            $('#gfirst_name').removeClass('ed-message-success').addClass('ed-message-error');
        }
        if (cc == 1) {
            $('#author_create_btn').removeClass('disabled').addClass('btn-success');
        }
    }

    this.reciveData = function(data) {
        $('#gfirst_name').val(data['test']);
    }
}

inheritsFrom(AuthorCreateBlock, FormBlock);