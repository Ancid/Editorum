/**
 * Created by xor on 23.06.15.
 */
/**
 * Список произведений
 */

var JournalListBlock = function() {
    var that = this;

    this.block_name = 'journal_list';

    this.onDraw = function() {

        that.loadTemplate();
        var model = bframe.models['journal'];
        model.getList();

        $('#journal_list_create').on('click', function(e) {
            bframe.go('journal.create');
            e.preventDefault();
        });

        $('#journal_content').empty();
        for (var i in model.data)
        {
            var r = model.data[i];
            /*$('#journal_content')
                .append('<tr></td><td>'+ r.type+'</td><td><a href="#publication.load.'+ r._id['$id']+'">'+ r.title+'</a></td><td>'+r._id['$id']+'</td></tr>');*/
        }

    }
}

inheritsFrom(JournalListBlock, ListBlock);


var IssueListBlock = function() {
    var that = this;

    this.block_name = 'issue_list';

    this.onDraw = function() {
        that.loadTemplate();
    }
}

inheritsFrom(IssueListBlock, ListBlock);

/**
 * Блок создания произведения
 * @constructor
 */
var JournalCreateBlock = function () {
    var that = this;

    this.block_name = 'journal_create';
    this.model = 'pubs';

    this.onDraw = function() {
        that.loadTemplate();
        $('#i_journal_title').on('change', this.validate);

        var model = bframe.models['pubs'];

        $('#journal_create_btn').on('click touchstart', function() {
            model.create({
                title:  $('#i_title').val(),
                type:   $('#i_type').val()
            });
            var id = model.save();
        });

        $('#gis_rinc').off();
        $('#gis_rinc').on('change', function(){
            if ($(this).find('input:checked').val() === '1') {
                $('#gmethod_transmitting_rinc').show();
            } else {
                $('#gmethod_transmitting_rinc').hide();
            }
        });
    }

    this.onEnter = function() {
        //that.fillForm();
    }

    this.validate = function() {
        var cc = 0;
        if ($('#i_journal_title').val() !== 'none') {
            $('#i_journal_title').removeClass('ed-message-error').addClass('ed-message-success');
            cc++;
        } else {
            $('#i_journal_title').removeClass('ed-message-success').addClass('ed-message-error');
        }
        if (cc == 1) {
            $('#journal_create_btn').removeClass('disabled').addClass('btn-success');
        }
    }

    this.reciveData = function(data) {
        //$('#i_journal_title').val(data['test']);
    }
}

inheritsFrom(JournalCreateBlock, FormBlock);