/**
 * Created by xor on 16.06.15.
 */
var bframe = false;

var ClassBFrame = function() {
    var that = this;
    this.config = false;

    // Текущий layout страницы
    this.current_layout = false;

    // Блоки, инициализированные на странице
    this.blocks = {};

    // Блоки, закешированные
    this.cache_blocks = {};

    // Объекты контроллеров
    this.controllers = {};

    // Объекты БД
    this.models = {};

    // Внешние параметры, переданные в контроллер
    this.global_params = {};

    /**
     * Инициализируем приложение
     */
    this.init = function (config) {
        this.config = config;
        // Инициализируем все классы контроллеров
        this.controllers = this.config.controllers;
        this.models = this.config.models;

        if ($('#bf-canvas').length === 0) displayError('bf-canvas div is not found');
        if ($('#bf-cemetery').length === 0) displayError('bf-cemetery div is not found');

        // Вешаем обработчик смены hash ссылки
        $(window).bind('hashchange', that.route);

        document.location.hash = '#'+this.config.default_controller;
        that.route();
    };

    this.go = function(route) {
        document.location.hash = '#'+route;
        that.route();
    }

    /**
     * Инициализировать шаблон страницы
     * @param layout_name название шаблона страницы для импорта
     */
    this.loadLayout = function(layout_name, blocks, local_params) {
        var canvas = document.querySelector('#bf-canvas');

        local_params = local_params || {};

        /**
         * Сообщить всем блокам, которые уже отображаются на странице
         */
        for (var position_name in that.blocks)
        {
            for (var i in that.blocks[position_name])
            {
                that.blocks[position_name][i].exit();
            }
        }

        // Создаем/показываем/прячем шаблон
        if (layout_name !== this.current_layout)
        {
            // Прячем текущий layout
            console.log('CL '+$('#'+this.current_layout).length);
            $('#'+this.current_layout).addClass('bf-hidden');

            if ($('#bf-canvas #'+layout_name).length == 0)
            {
                // Создаем новый layout
                var layout = document.querySelector('#layout_'+layout_name);
                if (layout === null) displayError('Layout '+layout_name+' is not defined');

                var layout_copy = document.importNode(layout.content, true);

                layout_copy.firstElementChild.id = layout_name;
                canvas.appendChild(layout_copy);
            }
            else
            {
                // Оживляем предыдущий
                $('#'+layout_name).removeClass('bf-hidden');
            }

        }

        this.current_layout = layout_name;


        /**
         * Собираем параметры среды исполнения для блоков
         */
        var params = {};
        $().extend(params, this.global_params, local_params);



        /**
         * Инициализируем блоки, переданные контроллером
         */
        for (var position_name in blocks)
        {
            that.blocks[position_name] = [];

            for (var i in blocks[position_name]) {
                var block = that.loadBlock(blocks[position_name][i], params);
                block.draw('#position_'+position_name);

                that.blocks[position_name].push(block);
            }
        }

    }


    /**
     * Фабрика загрузки модулей приложения
     * @param module_name инициализируем модуль приложения
     * @returns {*}
     */
    this.loadBlock = function(block_name, params) {
        console.log('loadBlock: '+block_name);
        if (typeof that.cache_blocks[block_name] === 'undefined') {

            if (typeof window[block_name] === 'undefined')
                displayError('Block '+block_name+' is not defined');

            // @TODO Загрузка соответствующего JS файла
            // Создаем новый экземпляр блока, если он еще не был ни разу использован
            that.cache_blocks[block_name] = new window[block_name]();
            // Запускаем метод блока, который отвечает за инициализацию "кишок" блока
            that.cache_blocks[block_name].onInit(params);
        }

        return that.cache_blocks[block_name];
    }


    /**
     * Обработчик перехода между контроллерами. Роутер
     * @param event
     */
    this.route = function(event) {
        var tmp = window.location.hash.split('#');
        var tmp2 = tmp[1].split('.');
        var controller = tmp2[0];
        if (typeof that.controllers[controller] === 'undefined')
            displayError('Controller '+controller+' is not defined');

        var method = tmp2[1];
        if (typeof that.controllers[controller][method] === 'undefined')
            displayError('Method '+method+' of controller '+controller+' is not defined');

        // Генерируем массив параметров
        tmp2.shift();
        tmp2.shift();
        that.global_params = tmp2;


        // Запускаем метод контроллера, передавая ему параметры из URL
        that.controllers[controller][method](tmp2);
        if (!typeof event === 'undefined')
            event.preventDefault();
    }

    /**
     * Передать данные другому блоку
     * @param block_name
     * @param data
     */
    this.sendData = function(block_name, data) {
        var destination_block = this.loadBlock(block_name);
        return destination_block.reciveData(data);
    }

}

/**
 *
 *  --------------------- Классы блоков ---------------------------------
 *
 */

var BasicBlock = function() {};
BasicBlock.prototype.container = false;
BasicBlock.prototype.first_draw = false;
BasicBlock.prototype.loadTemplate = function(container_id)
{
    if ($('#'+this.block_name).length > 0)
        return;

    var container_id = this.container;
    var template_id = '#template_'+this.block_name;
    console.log('Loading template '+template_id+' to container '+container_id);
    var block = document.querySelector(template_id);
    var block_copy = document.importNode(block.content, true);
    block_copy.firstElementChild.id = this.block_name;
    var container = document.querySelector(container_id);
    console.log(container);
    container.appendChild(block_copy);
}
BasicBlock.prototype.sendData = function(block_name, data) {
    return bframe.sendData(block_name, data);
}
// Вызывается при инициализации блока
BasicBlock.prototype.onInit = function() {console.log('Warning! onInit is not defined')};

// Системный метод, вызывается при каждой отрисовке блока
BasicBlock.prototype.draw = function(container_id) {
    this.container = container_id;
    if (this.first_draw === false)
        this.onDraw();

    this.enter();
};

BasicBlock.prototype.enter = function() {
    console.log(this.container);
    console.log('Get me back into the page: '+this.block_name+' i am here: '+$('#'+this.block_name).length);
    if ($(this.container+' #'+this.block_name).length === 0)
        $('#'+this.block_name).appendTo($(this.container));
    this.onEnter();
};

/**
 * Системная процедура выхода из блока
 */
BasicBlock.prototype.exit = function() {
    this.onExit();
    console.log('I want to break free: '+'#'+this.block_name);
    if ($('#bf-cemetery #'+this.block_name).length === 0)
        $('#'+this.block_name).appendTo('#bf-cemetery');
}

BasicBlock.prototype.onDraw = function() {};

// Переопределяемая пользовательская функция выполняемая при каждом входе в блок
BasicBlock.prototype.onEnter = function() {};

// Переопределяемая пользовательская функция выполняемая при каждом выходе из блока
BasicBlock.prototype.onExit = function() {};

// Блок получает данные извне
BasicBlock.prototype.reciveData = function(data) {};

var FormBlock = function() {};

/**
 * Заполнить форму данными из модели
 */
FormBlock.prototype = BasicBlock.prototype;
FormBlock.prototype.model = false;
FormBlock.prototype.id = 0;

FormBlock.prototype.fillForm = function() {
    console.log('jashdkjashs', bframe.models, this.model);
    var model = bframe.models[this.model];

    // Создаем пустую запись в БД или загружаем запись для редактирования по id
    if (this.id > 0)
        model.read(this.id);


    // Устновить значения из модели в поля формы
    $('#'+this.block_name+' [data-src]').each(function() {
        console.log('++++asds!!!!', model.record);

        console.log($(this).attr('data-src'), typeof model.record[$(this).attr('data-src')]);
        if (typeof model.record[$(this).attr('data-src')] != 'undefined') {
            $(this).val(model.record[$(this).attr('data-src')]);
        }

        /** @TODO
         *  1. Установка значений "по-умолчанию"
         *  2. Расчетные, трансформируемые и комплексные поля
         */

    });

}

/**
 * Сохраняем данные
 */
FormBlock.prototype.saveData = function() {
    var model = bframe.models[this.model];
    $('#'+this.block_name+' [data-src]').each(function() {
        model.record[$(this).attr('data-src')] = $(this).val();
    });

    model.save();
}

/**
 * Блок, отображающий список
 * @constructor
 */
var ListBlock = function() {};
ListBlock.prototype = BasicBlock.prototype;
ListBlock.prototype.model = false;

/**
 *
 *    --------------------- Классы модели ---------------------------------
 *
 */

var BasicModel = function() {
    var that = this;
};
BasicModel.prototype.data = [];
BasicModel.prototype.isFetched = true;
BasicModel.prototype.record = {};

/**
 * Создать новую запись
 * @param data
 */

BasicModel.prototype.create = function(data) {

    if (this.isFetched === false)
        displayError('Model data is not synced with server');

    this.data = [];
    this.record = {};
    this.isFetched = false;
};

BasicModel.prototype.save = function() {
    var that = this;
    var last_insert_id = false;

    var id = -1;
    if (typeof this.record['id'] !== 'undefined')
        id = this.record['id'];

    var data = JSON.stringify(this.record);
    // @TODO Сохранить на сервер
    $.ajax('/api/entity/'+this.entity_name+'/save/'+id, {async: false, data: {data: data}, type: 'POST'})
        .done(function(result) {
            that.isFetched = true;
            var data = JSON.parse(result);
            console.log(data);
            if (typeof data['last_insert_id'] !== 'undefined')
            {
                that.record['id'] = data['last_insert_id'];
            }
        })
        .fail(function() {alert('Error saving entity to server')});

    return that.record['id'];
};

/**
 * Получить один элемент по ID
 * @param id
 */
BasicModel.prototype.read = function(id) {
    var that = this;

    $.ajax('/api/entity/'+this.entity_name+'/read/'+id, {async: false})
        .done(function(result) {
            that.data = JSON.parse(result);

        })
        .fail(function() {
            alert('Error loading JSON from server');
        });

    if (this.data.length == 0)
    {
        this.data = [];
        this.record = {};
    }

        this.record = this.data[0];
    console.log('REEADD ME DEAR!!', id, this.data);
}


/**
 * Получение произвольной выборки от модели
 * @param dataset_name - название выборки (чаще для отправления на сервер)
 * @param params - массив параметров для формирования выборки
 */
BasicModel.prototype.getList = function(dataset_name, params) {
    var that = this;
    /**
     * @TODO Передача дополнительных параметров
     */
    $.ajax('/api/entity/'+this.entity_name+'/'+dataset_name, {async: false})
        .done(function(result) {

            that.data = JSON.parse(result);

        })
        .fail(function() {
            alert('Error loading JSON from server');
        });

    this.record = this.data[0];
}



/**
 *
 *    --------------------- Классы и методы утилиты ---------------------------------
 *
 */


/**
 * Метод, обеспечивающий наследование
 */
var inheritsFrom = function (child, parent) {
    child.prototype = Object.create(parent.prototype);
};


/**
 * Единая точка для отображения и логирования ошибок
 * @param message
 */
function displayError(message)
{
    alert(message);
}