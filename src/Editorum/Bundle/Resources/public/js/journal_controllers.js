/**
 * Created by xor on 23.06.15.
 */
var JournalController = function() {
    var that = this;

    /**
     * Список журналов
     */
    this.list = function () {
        bframe.loadLayout('fullscreen', {
            topmenu: ['TopMenuBlock'],
//            left:       ['PublicationMenuBlock'],
            main: ['JournalListBlock']
        });
    }

    /**
     * Список номеров
     */
    this.issuelist = function () {
        bframe.loadLayout('fullscreen', {
            topmenu: ['TopMenuBlock'],
            main: ['IssueListBlock']
        });
    }

    /**
     * Создать произведение
     */
    this.create = function() {
        bframe.loadLayout('fullscreen', {
            topmenu:        ['TopMenuBlock'],
//            left:       ['PublicationMenuBlock'],
            main:     ['JournalCreateBlock']
        });
    }
}