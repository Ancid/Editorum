/**
 * Created by xor on 16.06.15.
 */


/**
 * Список произведений
 */

var PublicationListBlock = function() {
    var that = this;

    this.block_name = 'publication_list';

    this.onDraw = function() {
        that.loadTemplate();
        var model = bframe.models['pubs'];
        model.getList('publist', {});

        $('#publication_list_create').on('click', function(e) {
            bframe.go('publication.create');
            e.preventDefault();
        });

        $('#publication_list_content').empty();
        for (var i in model.data)
        {
            var r = model.data[i];
            $('#publication_list_content')
                .append('<tr></td><td>'+ r.type+'</td><td><a href="#publication.load.'+ r._id['$id']+'">'+ r.title+'</a></td><td>'+r._id['$id']+'</td></tr>');
        }
    }
}

inheritsFrom(PublicationListBlock, ListBlock);

/**
 * Меню произведения
 * @constructor
 */
var PublicationMenuBlock = function () {
    var that = this;

    this.block_name = 'publication_menu';

    this.onDraw = function() {
        that.loadTemplate();
        var authors = that.sendData('PublicationAuthorslistBlock', {action: 'authorslist'});
        //$('#')
        that.reciveData({action: 'authorslist', authors: authors});
    }

    this.reciveData = function(data) {
        if (data['action'] == 'activate') {
            $('#publication_menu ul li').removeClass('active');
            $('#publication_menu #' + data['item']).addClass('active');
        }

        if (data['action'] == 'complete') {
            $('#publication_menu #' + data['item']).addClass('isfilled');
        }

        if (data['action'] == 'authorslist') {
            $('#authorsmenu').empty();

            console.log(data['authors']);

            for (var a in data['authors'])
            {
                console.log(a);
                var aclass = '';
                if (data['authors'][a]['isfilled'])
                    aclass='isfilled';

                var afio = data['authors'][a]['last_name']+' '+data['authors'][a]['first_name'];

                $('#authorsmenu').append('<li class="'+aclass+'"><a href="#publication.authoredit.'+a+'">'+afio+'</a></li>');
            }

        }
    }

}

inheritsFrom(PublicationMenuBlock, BasicBlock);

/**
 * Блок создания произведения
 * @constructor
 */
var PublicationCreateBlock = function () {
    var that = this;

    this.block_name = 'publication_create';
    this.model = 'pubs';

    this.onDraw = function() {
        that.loadTemplate();
        $('#i_type').on('change', this.validate);
        $('#i_title').on('change', this.validate);

        var model = bframe.models['pubs'];

        $('#publication_create_btn').on('click touchstart', function() {
            model.create({
                title:  $('#i_title').val(),
                type:   $('#i_type').val()
            });
            var id = model.save();
        });
    }

    this.onEnter = function() {
        //that.fillForm();
    }

    this.validate = function() {
        var cc = 0;
        if ($('#i_type').val() !== 'none') {
            $('#m_type').removeClass('ed-message-error').addClass('ed-message-success');
            cc++;
        }
        else
            $('#m_type').removeClass('ed-message-success').addClass('ed-message-error');

        if ($('#i_title').val().length > 0) {
            $('#m_title').removeClass('ed-message-error').addClass('ed-message-success');
            cc++;
        }
        else
            $('#m_title').removeClass('ed-message-success').addClass('ed-message-error');

        if (cc == 2) {
            $('#publication_create_btn').removeClass('disabled').addClass('btn-success');
        }
    }

    this.reciveData = function(data) {
        $('#i_title').val(data['test']);
    }
}

inheritsFrom(PublicationCreateBlock, FormBlock);



/**
 * Блок метаданных произведения
 * @constructor
 */
var PublicationMetadataBlock = function () {
    var that = this;

    this.block_name = 'publication_metadata';
    this.model = 'pubs';
    //this.id = 1;

    this.onDraw = function() {
        that.loadTemplate();
        that.fillForm();
//        return FormBlock.prototype.fillForm.apply(that);

        console.log($('.btn-selector button').length);

        // Включаем обработку кнопок
        form.useButtons();

        // Показываем/прячем сведения о предыдущей публикации
        $('#i_published_before').on('click', function() {$('#journal_before').removeClass('bf-hidden');});
        $('#i_unpublished_before').on('click', function() {$('#journal_before').addClass('bf-hidden');});

        // Валидация
        $('#i_journal').on('change', this.validate);
        $('#i_authorpages').on('change', this.validate);

        // Успех
        $('#publication_metadata_btn').on('click', function() {
            that.sendData('PublicationMenuBlock', {action: 'complete', item: 'metadata'})
            that.saveData();
        });
    }

    this.validate = function() {
        var cc = 0;
        if ($('#i_journal').val() != 0)
            cc++;

        if ($('#i_authorpages').val() > 0)
            cc++;

        if (cc == 2)
            $('#publication_metadata_btn').removeClass('disabled').addClass('btn-success');
    }

}

inheritsFrom(PublicationMetadataBlock, FormBlock);

var PublicationAuthorslistBlock = function() {
    var that = this;

    this.block_name = 'publication_authorslist';
    this.model = 'pubs';
    this.id = 1;

    this.authorslist = {17: {last_name: 'Шемятенков', first_name: 'Игорь', second_name: 'Викторович', isfilled: true}};

    this.onDraw = function() {
        that.loadTemplate();
        $('#coauthor_add').bind('click touchstart', function() {
            $('#coauthor_add_form').removeClass('ed-hidden');
            $('#coauthor_add_msg').removeClass('ed-hidden');
            $('#coauthor_add').attr('disabled', 'true');
        });

        $('#coauthor_add_cancel').bind('click touchstart', function() {
            // Прячем форму и делаем активной кнопку "Добавить автора"
            $('#coauthor_add_form').addClass('ed-hidden');
            $('#coauthor_add_msg').addClass('ed-hidden');
            $('#coauthor_add').removeAttr('disabled');

            // Очищаем поля ввода формы добавления соавторов
            $('#d_fio').val('');
            $('#d_authorslist_identification').val('none');
            $('#d_authorslist_email').val('');
            $('#d_authorslist_phone').val('');
            $('#d_authorslist_snils').val('');
        });

        $('#d_authorslist_identification').change(function() {
            $('.d_identify').addClass('ed-hidden');
            if ($(this).val() == 'email')
                $('#d_identify_email').removeClass('ed-hidden');
            if ($(this).val() == 'phone')
                $('#d_identify_phone').removeClass('ed-hidden');
            if ($(this).val() == 'snils')
                $('#d_identify_snils').removeClass('ed-hidden');
        });

        $('#coauthor_add_btn').on('click', function() {
            $('#coauthors').append('<li>'+$('#d_fio').val()+'</li>');

            that.authorslist['0'] = {first_name: $('#d_fio').val(), last_name: '', second_name: '', isfilled: false};
            that.sendData('PublicationMenuBlock', {action: 'authorslist', authors: that.authorslist});


            // @TODO Это копия!
            // Прячем форму и делаем активной кнопку "Добавить автора"
            $('#coauthor_add_form').addClass('ed-hidden');
            $('#coauthor_add_msg').addClass('ed-hidden');
            $('#coauthor_add').removeAttr('disabled');

            // Очищаем поля ввода формы добавления соавторов
            $('#d_fio').val('');
            $('#d_authorslist_identification').val('none');
            $('#d_authorslist_email').val('');
            $('#d_authorslist_phone').val('');
            $('#d_authorslist_snils').val('');
        });


        $('#publication_authorslist_btn').on('click', function() {that.sendData('PublicationMenuBlock', {action: 'complete', item: 'authorslist'})});

    }

    this.reciveData = function(data) {
        if (data['action'] == 'authorslist')
            return that.authorslist;
    }
}

inheritsFrom(PublicationAuthorslistBlock, BasicBlock);


var PublicationAuthorEditBlock = function() {
    var that = this;

    this.block_name = 'publication_authoredit';
    this.model = 'publication';
    this.id = 1;

    this.onDraw = function() {
        that.loadTemplate();
    }
}

inheritsFrom(PublicationAuthorEditBlock, BasicBlock);


var PublicationFeesBlock = function() {
    var that = this;

    this.block_name = 'publication_fees';
    this.model = 'publication';
    this.id = 1;

    this.onDraw = function() {
        that.loadTemplate();

        $('#publication_fees_btn').on('click', function() {that.sendData('PublicationMenuBlock', {action: 'complete', item: 'fees'})});
    }
}

inheritsFrom(PublicationFeesBlock, BasicBlock);



var PublicationFilesBlock = function() {
    var that = this;

    this.block_name = 'publication_files';
    this.model = 'publication';
    this.id = 1;

    this.onDraw = function() {
        that.loadTemplate();

        $('#publication_files_btn').on('click', function() {that.sendData('PublicationMenuBlock', {action: 'complete', item: 'files'})});
    }
}

inheritsFrom(PublicationFilesBlock, BasicBlock);


var PublicationTextBlock = function() {
    var that = this;

    this.block_name = 'publication_text';
    this.model = 'publication';
    this.id = 1;

    this.onDraw = function() {
        that.loadTemplate();

        $('#publication_text_btn').on('click', function() {that.sendData('PublicationMenuBlock', {action: 'complete', item: 'text'})});
    }
}

inheritsFrom(PublicationTextBlock, BasicBlock);


var PublicationAgreementBlock = function() {
    var that = this;

    this.block_name = 'publication_agreement';
    this.model = 'publication';
    this.id = 1;

    this.onDraw = function() {
        that.loadTemplate();

        $('#publication_agreement_btn').on('click', function() {that.sendData('PublicationMenuBlock', {action: 'complete', item: 'agreement'})});
    }
}

inheritsFrom(PublicationAgreementBlock, BasicBlock);


var PublicationDescriptionBlock = function() {
    var that = this;

    this.block_name = 'publication_description';
    this.model = 'publication';
    this.id = 1;

    this.onDraw = function() {
        that.loadTemplate();

        form.useWordCount();

        $('#autotranslate').on('click', function() {alert('Виртуальный запуск автоперевода. Заменить реальным методом.');
            $('#i_keyeng').val('Lorem Ipsum');
            $('#i_annotationeng').val('Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vulputate cursus est et volutpat. Sed porttitor interdum augue, in tristique libero ullamcorper porta. Ut porta iaculis turpis, at posuere lorem rhoncus ut. Aenean et erat efficitur, pharetra dui eget, ultricies sem. Aenean semper orci vitae posuere placerat. Quisque et pulvinar nibh. Mauris in arcu odio. Sed id diam tincidunt nibh ornare consequat. Suspendisse quis eleifend ante, a consectetur diam. Vivamus hendrerit vitae erat eu volutpat. Mauris suscipit nibh erat, ut hendrerit metus maximus sed. Vestibulum congue purus erat, ut mollis purus tempus quis. Phasellus sagittis diam erat, facilisis fringilla turpis rhoncus in. Aenean vitae vulputate lacus, non porta massa. Etiam posuere porta sapien ut lacinia.');
        });

        $('#okso_button').on('click', function() {alert('Error #21. Server returns empty list')});
        $('#grnti_button').on('click', function() {alert('Error #21. Server returns empty list')});

        $('#publication_description_btn').on('click', function() {that.sendData('PublicationMenuBlock', {action: 'complete', item: 'description'})});
    }
}

inheritsFrom(PublicationDescriptionBlock, BasicBlock);


var PublicationCoverBlock= function() {
    var that = this;

    this.block_name = 'publication_cover';
    this.model = 'publication';
    this.id = 1;

    this.onDraw = function() {
        that.loadTemplate();

        $('#publication_cover_btn').on('click', function() {that.sendData('PublicationMenuBlock', {action: 'complete', item: 'cover'})});
    }
}

inheritsFrom(PublicationCoverBlock, BasicBlock);

var PublicationContentsBlock = function() {
    var that = this;

    this.block_name = 'publication_contents';
    this.model = 'publication';
    this.id = 1;

    this.onDraw = function() {
        that.loadTemplate();

        $('#publication_contents_btn').on('click', function() {that.sendData('PublicationMenuBlock', {action: 'complete', item: 'contents'})});
    }
}

inheritsFrom(PublicationContentsBlock, BasicBlock);


var PublicationReferencesBlock = function() {
    var that = this;

    this.block_name = 'publication_references';
    this.model = 'publication';
    this.id = 1;

    this.onDraw = function() {
        that.loadTemplate();

        $('#publication_references_btn').on('click', function() {that.sendData('PublicationMenuBlock', {action: 'complete', item: 'references'})});
    }
}

inheritsFrom(PublicationReferencesBlock, BasicBlock);


var PublicationTasksBlock = function() {
    var that = this;

    this.block_name = 'publication_tasks';
    this.model = 'publication';
    this.id = 1;

    this.onDraw = function() {
        that.loadTemplate();

        $('#tasktype').on('change', function() {
            if ($('#tasktype').val() != 0)
                $('#task_create').removeClass('disabled').addClass('btn-success');
            else
                $('#task_create').addClass('disabled').removeClass('btn-success');
        });

        $('#task_create').on('click', function(e) {
            $('#task_list').empty();
            $('#task_list').append('<tr><td>48</td><td>Шемятенков И. В.</td><td>Автор</td><td>Заполнить список соавторов</td><td>Не в работе</td></tr>');

            $('#tasknum').html('3');
            e.preventDefault();
        })
    }
}

inheritsFrom(PublicationTasksBlock, BasicBlock);

var PublicationTasklistBlock = function() {
    var that = this;

    this.block_name = 'publication_tasklist';
    this.model = 'publication';
    this.id = 1;

    this.onDraw = function() {
        that.loadTemplate();
    }
}

inheritsFrom(PublicationTasklistBlock, BasicBlock);


var PublicationPublishBlock = function() {
    var that = this;

    this.block_name = 'publication_publish';
    this.model = 'publication';
    this.id = 1;

    this.onDraw = function() {
        that.loadTemplate();

        $('#publish_editorum').on('click', function(e) {
            if ($('#publish_editorum').prop('checked'))
                $('#editorum_publish_details').removeClass('bf-hidden');
            else
                $('#editorum_publish_details').addClass('bf-hidden');
        });

        $('#publish_editorum_type').on('change', function() {
            if ($('#publish_editorum_type').val() != 0)
                $('#editorum_publish_do').removeClass('disabled').addClass('btn-success');
            else
                $('#editorum_publish_do').addClass('disabled').removeClass('btn-success');

        });

        $('#editorum_publish_do').on('click', function() {
            alert('Виртуальная эмуляция отправки произведения на сервер lib.editorum.ru')
            that.sendData('PublicationManageBlock', {action: 'published'});
        });


    }
}

inheritsFrom(PublicationPublishBlock, BasicBlock);


var PublicationManageBlock = function() {
    var that = this;

    this.block_name = 'publication_manage';
    this.model = 'publication';
    this.id = 1;

    this.onDraw = function() {
        that.loadTemplate();
    }

    this.reciveData = function(data) {
        $('#doi_msg').addClass('bf-hidden');
        $('#doi_form').removeClass('bf-hidden');

        $('#doi_source').on('change', function() {
            if ($('#doi_source').val() != 0)
                $('#doi_do').removeClass('disabled').addClass('btn-success');
            else
                $('#doi_do').addClass('disabled').removeClass('btn-success');
        });

        $('#doi_do').on('click', function() {alert('Виртуальная отправка данных в CrossRef')});
    }
}

inheritsFrom(PublicationManageBlock, BasicBlock);


