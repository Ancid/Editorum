$(function() {
    $(".list_elements_parent").nextUntil(".list_elements_parent").toggle();
    $('body').on('mouseenter','.list_elements_parent',function(){
        $toggleblock = $(this).nextUntil(".list_elements_parent");
        $('.list_elements_child').not($toggleblock).hide();

        $('.list_elements_parent').not(this).removeClass("list_elements_parent_click");

        $toggleblock.slideToggle("slow", function () {});
        if($(this).hasClass("list_elements_parent_click"))         $(this).removeClass("list_elements_parent_click");
        else                                                       $(this).addClass("list_elements_parent_click");
    });
});