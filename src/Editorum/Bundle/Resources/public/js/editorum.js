/**
 * Created by Xor on 23.06.2015.
 */
$(function() {
});

var form = {};

form.useButtons = function() {
    $('.btn-selector button').on('click', function(e) {
        $(this).parent().children('button').removeClass('btn-primary').addClass('btn-default');
        $(this).addClass('btn-primary');
    });
}

form.useWordCount = function() {
    // Инциализируем поля требующие ввода определенного количества слова
    $('.actWordCount').each(function() {
        $(this).bind('keydown', function countWords(e) {
            var m = getMessageBox($(this));
            var content = $(this).val();
            var min = $(this).attr('data-min');
            var max = $(this).attr('data-max');
            var num = content.split(' ').length;
            var msg = 'Вы указали '+num+' слов(о). ';

            if ((num > min) && (num < max))
                successBox(m, msg+' Количество слов достаточно.');
            if (num < min)
                errorBox(m, msg+' Слишком мало слов.');
            if (num > max)
                errorBox(m, msg+' Слишком много слов.');

        });
    })
}


/**
 * Помечает комментарий к полю как ошибку и задает ему текст
 * @param element
 * @param text
 */
function errorBox(element, text)
{
    element.addClass('ed-message-error');
    element.removeClass('ed-message-success');
    element.text(text);
}

/**
 * Помечает комментарий к полю, как успешно заполненный и задает текст
 * @param element
 * @param text
 */
function successBox(element, text)
{
    element.removeClass('ed-message-error');
    element.addClass('ed-message-success');
    element.text(text);
}

/**
 * Получить div для комментария к текущему полю
 * @param element
 * @returns {*|jQuery|HTMLElement}
 */
function getMessageBox(element)
{
    return $('#m_'+element.attr('id').split('_')[1]);
}

