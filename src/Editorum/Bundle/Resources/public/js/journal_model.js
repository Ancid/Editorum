/**
 * Created by xor on 16.06.15.
 */


/**
 * Модель данных для журнал
 * @constructor
 */
var JournalModel = function() {
    var that = this;
    this.entity_name = 'journal';


    this.getList = function(dataset_name, params) {
        return BasicModel.prototype.getList.apply(that, arguments);
    }

    this.read = function(id) {
        return BasicModel.prototype.read.apply(that, arguments);
    }

    this.create = function() {
        return BasicModel.prototype.create.apply(that, arguments);
    }

    this.save = function() {
        return BasicModel.prototype.save.apply(that);
    }
}

inheritsFrom(JournalModel, BasicModel);