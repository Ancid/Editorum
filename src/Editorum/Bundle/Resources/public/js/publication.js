/**
 * Created by xor on 09.06.15.
 */
$(function() {

        /**
         *
         *  Общая навигация
         *
         */
        $('#publication_menu ul li a').each(function() {$(this).bind('click touchstart', open_publication_menu)})


        /**
         * Функционирование отдельных форм
         */

        /** 1. Форма создания произведения */

        $('#i_title').bind('change keyup', function() {
                if ($(this).val().length > 0)
                        successBox(getMessageBox($(this)), 'ОК');
                testFormFill('publication_create');
                });

        $('#i_type').bind('change', function() {
                if ($(this).val() !== 'none')
                        successBox(getMessageBox($(this)), 'ОК');
                testFormFill('publication_create');
        });

        $('#publication_create_send').bind('click touchstart', function() {
                /**
                 * @TODO
                 * 1. Отправляем название и тип произведения на сервер
                 * 2. Устанавливаем ID
                 * 3. Устаналиваем тип произведения для запуска соответствующих форм
                 */
        });

        /** 2. Управление списком авторов */
        // Нажата кнопка добавить автора


        // Отмена добавления автора

        // Выбор типа авторизации


        /**
         *
         *  Управление элементами форм
         *
         */
        // Инциализируем поля требующие ввода определенного количества слова
        $('.actWordCount').each(function() {
                $(this).bind('keydown', function countWords(e) {
                        var m = getMessageBox($(this));
                        var content = $(this).val();
                        var min = $(this).attr('data-min');
                        var max = $(this).attr('data-max');
                        var num = content.split(' ').length;
                        var msg = 'Вы указали '+num+' слов(о). ';

                        if ((num > min) && (num < max))
                                successBox(m, msg+' Количество слов достаточно.');
                        if (num < min)
                                errorBox(m, msg+' Слишком мало слов.');
                        if (num > max)
                                errorBox(m, msg+' Слишком много слов.');

                });
        })
});


/**
 * Навигация. Открыть форму в зависимости от пункта меню
 * @param elem
 */
function open_publication_menu(e)
{

        $('.publication_forms').addClass('ed-hidden');
        var form_to_open = '#'+$(this).attr('data-open');
        console.log('nav.open: '+form_to_open);
        $(form_to_open).removeClass('ed-hidden');
}

/**
 * Проверяем форму на соблюдение условий
 */
function testFormFill(form_id)
{
        var total_fields = 0;
        var success_fields = 0;

        $('#'+form_id+' .ed-message').each(function() {
                total_fields++;
                if ($(this).hasClass('ed-message-success'))
                        success_fields++;
        });

        console.log('::testFormFill: '+form_id, success_fields, total_fields);

        var op_button = '#'+form_id+'_send';
        console.log($(op_button).length);

        if (total_fields == success_fields)
                $(op_button).removeAttr('disabled');
        else
                $(op_button).attr('disabled', 'true');
}

/**
 * Помечает комментарий к полю как ошибку и задает ему текст
 * @param element
 * @param text
 */
function errorBox(element, text)
{
        element.addClass('ed-message-error');
        element.removeClass('ed-message-success');
        element.text(text);
}

/**
 * Помечает комментарий к полю, как успешно заполненный и задает текст
 * @param element
 * @param text
 */
function successBox(element, text)
{
        element.removeClass('ed-message-error');
        element.addClass('ed-message-success');
        element.text(text);
}

/**
 * Получить div для комментария к текущему полю
 * @param element
 * @returns {*|jQuery|HTMLElement}
 */
function getMessageBox(element)
{
        return $('#m_'+element.attr('id').split('_')[1]);
}