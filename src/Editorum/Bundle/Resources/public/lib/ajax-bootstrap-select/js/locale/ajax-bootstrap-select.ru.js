/*!
 * Ajax Bootstrap Select
 *
 * Extends existing [Bootstrap Select] implementations by adding the ability to search via AJAX requests as you type. Originally for CROSCON.
 *
 * @version 1.3.7
 * @author Adam Heim - https://github.com/truckingsim
 * @link https://github.com/truckingsim/Ajax-Bootstrap-Select
 * @copyright 2016 Adam Heim
 * @license Released under the MIT license.
 *
 * Contributors:
 *   Mark Carver - https://github.com/markcarver
 *
 * Last build: 2016-06-24 1:36:43 PM EDT
 */
!(function ($) {
    /*!
     * English translation for the "en-US" and "en" language codes.
     * Mark Carver <mark.carver@me.com>
     */
    $.fn.ajaxSelectPicker.locale["ru-RU"] = {
        currentlySelected: "Ранее выбранное",
        emptyTitle: "Выберите из найденного или введите текст для уточнения результатов поиска",
        errorText: "Не удалось загрузить результаты поиска",
        searchPlaceholder: "Поиск...",
        statusInitialized: "Введите символы чтобы начать поиск...",
        statusNoResults: "Ничего не найдено",
        statusSearching: "Идет поиск..."
    }, $.fn.ajaxSelectPicker.locale.en = $.fn.ajaxSelectPicker.locale["ru-RU"];
})(jQuery);
