/**
 * Created by xor on 15.06.15.
 */
var PublicationController = function() {
    var that = this;

    /**
     * Список произведений
     */
    this.list = function() {
        bframe.loadLayout('fullscreen', {
            topmenu:        ['TopMenuBlock'],
//            left:       ['PublicationMenuBlock'],
            main:     ['PublicationListBlock']
        });
    }

    /**
     * Создать произведение
     */
    this.create = function() {
        bframe.loadLayout('fullscreen', {
            topmenu:        ['TopMenuBlock'],
//            left:       ['PublicationMenuBlock'],
            main:     ['PublicationCreateBlock']
        });
    }

    /**
     * Общие сведения о произведении
     */
    this.metadata = function() {
        bframe.loadLayout('twocolumn', {
            top:        ['TopMenuBlock'],
            left:       ['PublicationMenuBlock'],
            right:      ['PublicationMetadataBlock']
        });
        bframe.sendData('PublicationMenuBlock', {action: 'activate', item: 'metadata'});
    }

    this.authorslist = function() {
        bframe.loadLayout('twocolumn', {
            top:        ['TopMenuBlock'],
            left:       ['PublicationMenuBlock'],
            right:      ['PublicationAuthorslistBlock']
        });
        bframe.sendData('PublicationMenuBlock', {action: 'activate', item: 'authorslist'});
    }

    this.authoredit = function() {
        bframe.loadLayout('twocolumn', {
            top:        ['TopMenuBlock'],
            left:       ['PublicationMenuBlock'],
            right:      ['PublicationAuthorEditBlock']
        });
        bframe.sendData('PublicationMenuBlock', {action: 'activate', item: 'authoredit'});
    }

    this.fees = function() {
        bframe.loadLayout('twocolumn', {
            top:        ['TopMenuBlock'],
            left:       ['PublicationMenuBlock'],
            right:      ['PublicationFeesBlock']
        });
        bframe.sendData('PublicationMenuBlock', {action: 'activate', item: 'fees'});
    }

    this.files = function() {
        bframe.loadLayout('twocolumn', {
            top:        ['TopMenuBlock'],
            left:       ['PublicationMenuBlock'],
            right:      ['PublicationFilesBlock']
        });
        bframe.sendData('PublicationMenuBlock', {action: 'activate', item: 'files'});
    }

    this.text = function() {
        bframe.loadLayout('twocolumn', {
            top:        ['TopMenuBlock'],
            left:       ['PublicationMenuBlock'],
            right:      ['PublicationTextBlock']
        });
        bframe.sendData('PublicationMenuBlock', {action: 'activate', item: 'text'});
    }

    this.agreement = function() {
        bframe.loadLayout('twocolumn', {
            top:        ['TopMenuBlock'],
            left:       ['PublicationMenuBlock'],
            right:      ['PublicationAgreementBlock']
        });
        bframe.sendData('PublicationMenuBlock', {action: 'activate', item: 'agreement'});
    }

    this.description = function() {
        bframe.loadLayout('twocolumn', {
            top:        ['TopMenuBlock'],
            left:       ['PublicationMenuBlock'],
            right:      ['PublicationDescriptionBlock']
        });
        bframe.sendData('PublicationMenuBlock', {action: 'activate', item: 'description'});
    }

    this.cover = function() {
        bframe.loadLayout('twocolumn', {
            top:        ['TopMenuBlock'],
            left:       ['PublicationMenuBlock'],
            right:      ['PublicationCoverBlock']
        });
        bframe.sendData('PublicationMenuBlock', {action: 'activate', item: 'cover'});
    }

    this.contents = function() {
        bframe.loadLayout('twocolumn', {
            top:        ['TopMenuBlock'],
            left:       ['PublicationMenuBlock'],
            right:      ['PublicationContentsBlock']
        });
        bframe.sendData('PublicationMenuBlock', {action: 'activate', item: 'contents'});
    }

    this.references = function() {
        bframe.loadLayout('twocolumn', {
            top:        ['TopMenuBlock'],
            left:       ['PublicationMenuBlock'],
            right:      ['PublicationReferencesBlock']
        });
        bframe.sendData('PublicationMenuBlock', {action: 'activate', item: 'references'});
    }

    this.tasks = function() {
        bframe.loadLayout('twocolumn', {
            top:        ['TopMenuBlock'],
            left:       ['PublicationMenuBlock'],
            right:      ['PublicationTasksBlock']
        });
        bframe.sendData('PublicationMenuBlock', {action: 'activate', item: 'tasks'});
    }

    this.tasklist = function() {
        bframe.loadLayout('twocolumn', {
            top:        ['TopMenuBlock'],
            left:       ['PublicationMenuBlock'],
            right:      ['PublicationTasklistBlock']
        });
        bframe.sendData('PublicationMenuBlock', {action: 'activate', item: 'tasks'});
    }

    this.publish = function() {
        bframe.loadLayout('twocolumn', {
            top:        ['TopMenuBlock'],
            left:       ['PublicationMenuBlock'],
            right:      ['PublicationPublishBlock']
        });
        bframe.sendData('PublicationMenuBlock', {action: 'activate', item: 'publish'});
    }

    this.manage = function() {
        bframe.loadLayout('twocolumn', {
            top:        ['TopMenuBlock'],
            left:       ['PublicationMenuBlock'],
            right:      ['PublicationManageBlock']
        });
        bframe.sendData('PublicationMenuBlock', {action: 'activate', item: 'manage'});
    }

}

