<?php
class XCLMUser extends NodeItem
{
	public static function onGetStructure($structure)
	{
		$structure
			->add(new DTId('id'))
			->add(new DTString('login', array('human_name' => 'Email')))			
			->add(new DTString('password_encoded', array('human_name' => 'Пароль')))
			->add(new DTString('first_name', array('human_name' => 'Имя')))
			->add(new DTString('last_name', array('human_name' => 'Фамилия')))
			->add(new DTDatetime('last_update', array('human_name' => 'Последнее обращение')))
			->addLink('groups', 'users', new LinkMany2Many('XCLMUser', 'XCLMGroups', array()))
			->add(new DTInt('can_stat', array('human_name' => 'Права статистика')))
			->add(new DTInt('can_user', array('human_name' => 'Права управления пользователями')))
			->add(new DTInt('can_content', array('human_name' => 'Права управления контентом')))
			->add(new DTInt('disabled', array('human_name' => 'Пользователь заблокирован')))
			->add(new DTString('appversion', array('human_name' => 'Версия приложения')));
	}
	
	public function calcFio($dataset)
	{
		return $dataset->first_name.' '.$dataset->last_name;
	}
	
	public static function setPassword($user_id, $password_encoded)
	{
		Model::update('XCLMUser', array('password_encoded' => $password_encoded), array('id' => $user_id));
	}
	
	public static function deleteWithStat($user_id)
	{
		// Удаляем статистику
		Model::delete('XCLMStatVisit', array('user_id' => $user_id));
		Model::delete('XCLMUser', array('id' => $user_id));
	}
	
	public function calcHgroups()
	{
		$gr = $this->groups;
		if ($gr->id > 0)
		{
			$grn = array();
			foreach ($gr as $g)
			{
				$grn[] = $g->group_name;
			}
			return implode(', ', $grn);
		}
		else 
			return '';
	}
	
	public function calcHactive()
	{
		if ($this->disabled == 1)
			return '<span class="label label-important">Заблокирован</span>';
		else
			return '<span class="label label-success">Активен</span>';
	}
	
	public static function isAuthorized()
	{
		if ((isset($_SESSION['current_user'])) && ($_SESSION['current_user'] > 0))
			return true;
		
			return false;
	}
	
	public static function getCurrentUser()
	{
		if (self::isAuthorized())
		{
			$user = Model::getById('XCLMUser', $_SESSION['current_user']);
			if ($user->id > 0)
				return $user;
		}
		
		return false;
	}

	public static function login($email, $password, $appversion)
	{
		$res = array();
		$is_detected = false;
		
		$users = Model::getByKeys('XCLMUser', array(
			'login'				=> trim($email),
			'password_encoded'	=> trim($password)
		));
		
		if ($users->id > 0)
		{
			if ($users->disabled == 1)
			{
				$res['result']			= false;
				$res['error']			= 'Аккаунт заблокирован';
				$str = json_encode($res);
				echo str_replace('\/', '/', $str);
				die();
			}
			
			$res['result']			= true;
			$res['user_id'] 		= $users->id;
			$res['fio']				= $users->first_name.' '.$users->last_name;
			$res['app_version']		= ProjectConfig::$cur_app_version;
			$res['app_url']			= 'http://roche.xpractice.ru/CLM/';
			
			Model::update('XCLMUser', 
				array(
					'last_update' 	=> date('Y-m-d H:i:s', strtotime('-1 hour')),
					'appversion'	=> $appversion
				),
				array('id' => $users->id)
			);
			
			$presentation_ids = array();
			$groups = $users->groups;
			if ($groups->id != false)
				foreach ($groups as $g)
				{
					$presentations = $g->presentations;
					if ($presentations != false)
						foreach ($presentations as $p)
						{
							$presentation_ids[] = $p->id;
						}
				}
			$pres = Model::getList('NCLMPresentations', $presentation_ids, 'title+');
			$res['presentations'] = array();
			if ($pres->id > 0)
				foreach ($pres as $p)
				{
					$res['presentations'][] = array(
						'title'		=> $p->title,
						'sysname'	=> $p->systitle,
						'logo'		=> $p->logo,
						'version'	=> $p->version,
					); 
				}
				
			$is_detected = true;			
		}
		
		error_log($users->id);
		
		if (!$is_detected)
		{
			$res['result']			= false;
			$res['error']			= 'Неверный email или пароль';
		}
		
		$str = json_encode($res);
		echo str_replace('\/', '/', $str);
	}
}