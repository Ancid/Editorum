## Сортировка стран и областей

Для того, чтобы заработала сортировка стран и областей, Вам необходимо провести следующие манипуляции.

В консоли(терминале) выполнить команду:
```bash
php app/console asu:geography:position -d Country
php app/console asu:geography:position -d Region
```

Чтобы установить определенный элемент на нужную позицию можно с помощью команды в консоли(терминале):
```bash
# Установит Россию на первую позицию (позиции начинаются с 0)
php app/console asu:geography:sort -d Country -i 29 -p 0
# Установит Москву и Санкт-Петербург на первую и вторую позиции
php app/console asu:geography:sort -d Region -i 10 -p 0
php app/console asu:geography:sort -d Region -i 45 -p 1
```

Чтобы найти информацию об элементе (городе, регионе) можно выполнить команду:
```bash
php app/console asu:geography:find -d Country Россия
php app/console asu:geography:find -d Region Москва

```

Справку по используемым выше командам можно получить добавив --help в конце:
```bash
php app/console asu:geography:position --help
php app/console asu:geography:sort --help
php app/console asu:geography:find --help
```