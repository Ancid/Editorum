<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 05.11.2015
 * Time: 13:54
 */

namespace Editorum\Bundle\AgreementSystem;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use Editorum\Bundle\Logic\Model;
use Editorum\Bundle\AgreementSystem\Rights\GuestRights;
use Editorum\Bundle\AgreementSystem\Agreements\BasicAgreement;
use Editorum\Bundle\AgreementSystem\Agreements\EmployeeAgreement;
use Editorum\Bundle\AgreementSystem\Agreements\OrderAgreement;
use Editorum\Bundle\AgreementSystem\Agreements\OwnerAgreement;
use Editorum\Bundle\AgreementSystem\Agreements\PrintableAgreement;
use Editorum\Bundle\AgreementSystem\Agreements\PublishingAgreement;
use Editorum\Bundle\AgreementSystem\Agreements\UniversityManagerAgreement;
use Editorum\Bundle\AgreementSystem\Agreements\AdministratorAgreement;
use Editorum\Bundle\AgreementSystem\Agreements\ChiefEditorAgreement;
use Editorum\Bundle\AgreementSystem\Agreements\EditorAgreement;
use Editorum\Bundle\AgreementSystem\Agreements\EditoralManagerAgreement;
use Editorum\Bundle\AgreementSystem\Agreements\ELSAAdministratorAgreement;

class ASys
{
    /*
     * Объект Singleton
     */
    protected static $instance = false;

    protected $session;
    
    /**
     * Экземпляры объектов зарегистрированных договоров
     * @var array
     */
    protected $registered_agreements = [];

    /**
     * Конструктор Singleton
     */
    private function __construct($session)
    {
        /** @var \Symfony\Component\HttpFoundation\Session\Session $session */
        $this->session = $session;
        $this->registerAgreements();
    }

    /**
     * @return bool|ASys
     */
    public static function instance($session)
    {
        if (self::$instance === false)
            self::$instance = new ASys($session);

        return self::$instance;
    }

    /**
     * Создать новый договор
     * 
     * @param BasicAgreement $agreement
     * @param bool $cached
     * @return BasicAgreement
     */
    public function saveAgreement(BasicAgreement $agreement, $cached = false)
    {
        $dataset = $agreement->saveAgreement();
        if (is_null($agreement->__get('_id'))) {
            $agreement->_id = Model::insert($agreement, $dataset); 
        } else {
            Model::update($agreement, $dataset);
        }
        
        if ($cached) {
            $this->cacheAgreement($agreement);
        }
         
        return $agreement; 
    }

    /**
     * Execute current agreement. If it's possible..
     * 
     * @param BasicAgreement $agreement
     * @param bool $cached
     * @return BasicAgreement
     */
    public function executeAgreement(BasicAgreement $agreement, $cached = true) {
        try {
            $agreement->execute();
        } catch (Exception $e) {
            $this->session->getFlashBag()->add('error', $e->getMessage());
        }
        
        if ($agreement->isActive()) {
            $this->saveAgreement($agreement);
            
            if ($cached) {
                /*
                 * Save agreement data in the AgreementCache
                 */
                $this->cacheAgreement($agreement);

                /*
                 * Save agreement interface state in the InterfaceCache
                 */
                $this->cacheInterface($agreement);
            }
        }
        
        return $agreement;
    }

    /**
     * @param BasicAgreement $agreement
     * @return $this
     */
    protected function cacheAgreement(BasicAgreement $agreement) {
        // Сохранение прав договора в кэше
        $cache = new AgreementCache();
        $dataset = $cache->prepareToSave($agreement);
        foreach ((array)$dataset as $record) {
            $cache_object = Model::getByKeys($cache, ['user_id' => $record['user_id']]);
            if ($current_record = $cache_object->getCurrentRecord()) {
                if (!empty($record['rights']))
                    // @ToDo: remove after testing
//                    Model::update($cache_object, array_merge_recursive($current_record, $record));
                    $rights = $record['rights'];
                    if (!empty($current_record['rights'])) {
                        $rights = array_merge($current_record['rights'], $record['rights']);
                    }
                    Model::update(
                        $cache_object, 
                        [
                            '_id' => $current_record['_id'],
                            'user_id' => $record['user_id'],
                            'rights' =>  $rights 
                        ]
                    );
            } else {
                Model::insert($cache, $record);
            }
        }
    }
    
    protected function cacheInterface(BasicAgreement $agreement) {
        return true;
    } 

    /**
     * Зарегистрировать все существующие типы договоров
     */
    public function registerAgreements()
    {
        $this->addAgreement(new AdministratorAgreement($this));
        $this->addAgreement(new ChiefEditorAgreement($this));
        $this->addAgreement(new EditorAgreement($this));
        $this->addAgreement(new EditoralManagerAgreement($this));
        $this->addAgreement(new ELSAAdministratorAgreement($this));
        $this->addAgreement(new PublishingAgreement($this));
        $this->addAgreement(new EmployeeAgreement($this));
        $this->addAgreement(new OrderAgreement($this));
        $this->addAgreement(new OwnerAgreement($this));
        $this->addAgreement(new PrintableAgreement($this));
        $this->addAgreement(new UniversityManagerAgreement($this));
    }

    /**
     * Получить все договора пользователя определенного типа
     *
     * @param $user_id
     * @param null $agreement_type
     * @param null $side
     * @return array
     * @throws Exceptions\AgreementException
     */
    public function getAgreementsByUser($user_id, $agreement_type = null, $side = null)
    {
        $request = [];
        
        if ($agreement_type && $agreement = $this->getAgreementObject($agreement_type)) {
            $request['agreement_type'] = $agreement->getAgreementType(); 
        } else {
            $agreement = new BasicAgreement($this);            
        }
        
        $source = new BasicSide(BasicSide::SRC_SIDE);
        $source->addPerson($user_id);

        $destination = new BasicSide(BasicSide::DST_SIDE);
        $destination->addPerson($user_id);
        
        switch ($side) {
            case BasicSide::SRC_SIDE:
                $request = array_merge($request, $source->save([]));
                break;
            
            case BasicSide::DST_SIDE:
                $request = array_merge($request, $destination->save([]));
                break;
            
            case null:
                $request['$or'] = [
                    $source->save([]),
                    $destination->save([])
                ];
                break;
        }
        
        return Model::getAll($agreement, $request);
    }

    /**
     * @param $user_id
     * @param $agreement_types
     */
    public function getEntityByAgreementTypes($user_id, $agreement_types)
    {
        $agrs = $this->getAgreementsByUser($user_id, $agreement_types);

        $entity_ids = [];
        foreach ((array)$agrs as $a)
        {
            $entity_ids[] = array_merge($a->getEntityList()->getIds(), $entity_ids);
        }
    }

    /**
     * @param string $object_classname
     * @return BasicAgreement
     * @throws Exception
     */
    public function getAgreementObject($object_classname)
    {
        if (!isset($this->registered_agreements[$object_classname]))
            throw new Exception('Неизвестный класс '.$object_classname.' договора');

        return $this->registered_agreements[$object_classname];
    }

    /**
     * Добавить договор
     * @param BasicAgreement $obj
     */
    protected function addAgreement(BasicAgreement $obj)
    {
        $this->registered_agreements[$obj->getClass()] = $obj;
    }

    /**
     * Получить список договоров имеющих печатную форму
     * @return array
     */
    public function getPrintableAgreements()
    {
        $agreements = [];
        foreach ($this->registered_agreements as $class_name => $obj)
        {
            if ($obj->is_printable === true)
            {
                $agreements[$class_name] = $obj->title;
            }
        }
        return $agreements;
    }

    /**
     * @return array
     */
    public function getRegisteredAgreementsTitles() {
        $result = [];
        foreach ((array)$this->registered_agreements as $agr) {
            $result[$agr->getClass()] = $agr->title;
        }
        return $result;
    }

    /**
     * @param $user
     * @param Request $request
     * @return bool
     */
    public function checkAccess($user, Request $request) {
        $route_name = $request->get('_route');
        $route_params = $request->get('_route_params');
        $guest_rights = new GuestRights();
        if(in_array($route_name, $guest_rights->getAllRights()))
            return true;
        if (is_null($user)) {
            if(in_array($route_name, $guest_rights->getAllRights()))
                return true;
        } else {
            /** @var \UserBundle\Document\User $user */
            $user_id = $user->getId();
            /** @var \Editorum\Bundle\AgreementSystem\AgreementCache $agreementCache */
            $agreementCache = Model::getByKeys(new AgreementCache(), ['user_id' => $user_id]);
            if (!is_null($record = $agreementCache->current())) {
                $rights_array = $record->getCurrentRecord()['rights'];
                if (!empty($rights_array) && array_key_exists($route_name, $rights_array)) {
                    if (empty($route_params) || !array_key_exists('_id', $route_params)) {
                        return true;
                    } elseif (array_key_exists('entity', $route_params) && array_key_exists('_id', $route_params)) {

                    }
                }
            }
        }
        return false;
    }
}
