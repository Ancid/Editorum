<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 05.11.2015
 * Time: 14:20
 */

namespace Editorum\Bundle\AgreementSystem;

use Editorum\Bundle\AgreementSystem\Exceptions\AgreementException;

class BasicEntityList
{
    protected $entities = [];

    /**
     * @param $entity_object
     * @return BasicEntityList $this
     */
    public function addEntity($entity_object) {
        if (is_object($entity_object)) {
            $entity_name = $entity_object->getClass();
            if (!array_key_exists($entity_name, $this->entities)) {
                $this->entities[$entity_name] = [];
            }
            array_push($this->entities[$entity_name], $entity_object->__get('_id'));
        }

        return $this;
    }
    
    /**
     * Добавить к выборке публикации
     * @param $publication_id
     */
    public function addPublication($publication_id)
    {
        array_push($this->entities['publications'], $publication_id);
    }

    public function getIds()
    {
        return $this->entities;
    }

    public function save($dataset)
    {
        if (!is_array($dataset))
            throw new AgreementException('В качестве dataset должен быть передан массив');

        $dataset['entity_list'] = $this->entities;

        return $dataset;
    }

    public function load($dataset)
    {
        if (!isset($dataset['entity_list']))
            throw new AgreementException('В договоре отсутствует информация о списке сущностей "entity_list"');

        $this->entities = $dataset['entity_list'];
    }

}
