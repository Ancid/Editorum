<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 25.03.16
 * Time: 12:19
 */

namespace Editorum\Bundle\AgreementSystem\Conditions;

interface ConditionInterface
{
    /**
     * @param $dataset
     * @return mixed
     */
    public function load($dataset);

    /**
     * @return mixed
     */
    public function save();

    /**
     * @return mixed
     */
    public function check();
    
}
