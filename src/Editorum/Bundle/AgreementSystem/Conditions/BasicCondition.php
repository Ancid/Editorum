<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 05.11.2015
 * Time: 15:12
 */

namespace Editorum\Bundle\AgreementSystem\Conditions;

use ReflectionClass;

class BasicCondition implements ConditionInterface
{
    protected $condition = [];
    
    public function __construct($dataset = [])
    {
        $this->condition = $dataset;
    }

    /**
     * @param $dataset
     * @return $this
     */
    public function load($dataset)
    {
        $this->condition = (array) $dataset;
        
        return $this;
    }

    /**
     * @return array
     */
    public function save()
    {
        return $this->condition;
    }

    /**
     * @return bool
     */
    public function check()
    {
        return true;
    }

    /**
     * @return string
     */
    public function getClassName() {
        $class = new ReflectionClass($this);
        return $class->getName();
    }
}
