<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 31.03.16
 * Time: 11:07
 */

namespace Editorum\Bundle\AgreementSystem\Conditions;


use Editorum\Bundle\AgreementSystem\Agreements\BasicAgreement;
use Editorum\Bundle\AgreementSystem\Agreements\EmployeeAgreement;
use Editorum\Bundle\Logic\Model;

class ExistEmployeeAgreementCondition extends BasicCondition
{
    /* @var \Editorum\Bundle\AgreementSystem\Agreements\BasicAgreement $agreement */
    protected $agreement;

    /**
     * UniqueAgreementCondition constructor.
     * @param BasicAgreement $agreement
     */
    public function __construct(BasicAgreement $agreement)
    {
        parent::__construct();
        $this->agreement = $agreement;
    }

    public function check()
    {
        $agreement = new EmployeeAgreement();
        $conditions = array_merge(
            ['agreement_type' => $agreement->getAgreementType()],
            $this->agreement->getSourceSide()->save([]),
            $this->agreement->getDestinationSide()->save([]),
            ['is_active' => true]
        );
        /** @var \Editorum\Bundle\AgreementSystem\Agreements\EmployeeAgreement $agreement */
        $agreement = Model::getAll($agreement, $conditions)->current();
        if (is_null($agreement->getCurrentRecord()))
            return false;
        return true;
    }

}
