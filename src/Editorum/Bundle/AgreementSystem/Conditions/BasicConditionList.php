<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 05.11.2015
 * Time: 14:29
 */

namespace Editorum\Bundle\AgreementSystem\Conditions;

use Exception;
use ReflectionClass;
use Editorum\Bundle\AgreementSystem\Exceptions\AgreementException;

class BasicConditionList
{
    protected $conditions = [];

    /**
     * Добавить условия к договору
     * @param $condition
     */
    public function addCondition(BasicCondition $condition)
    {
        array_push($this->conditions, $condition);
    }

    /**
     * Проверить все условия договора
     * @return bool
     * @throws Exception
     */
    public function testConditions()
    {
        /** @var \Editorum\Bundle\AgreementSystem\Conditions\BasicCondition $condition */
        foreach ($this->conditions as $condition) {
            if ($condition->check() == false) {
                throw new Exception($condition->getClassName() . ' error');
            }
        }
        
        return true;
    }

    /**
     * Подготовить сведения об условиях к сохранению
     * @param $dataset
     * @return array
     * @throws AgreementException
     */
    public function save($dataset)
    {
        if (!is_array($dataset))
            throw new AgreementException('В качестве dataset должен быть передан массив');

        $conditions = [];
        foreach ($this->conditions as $c) {
            $ref = new ReflectionClass($c);
            $class_name = $ref->getShortName();

            $conditions[$class_name] = $c->save();
        }
        $dataset['condition_list'] = $conditions;
        return $dataset;
    }

    /**
     * Загрузить сведения об условиях
     * @param $dataset
     * @throws AgreementException
     */
    public function load($dataset)
    {
        if (!isset($dataset['condition_list']))
            throw new AgreementException('В договоре отсутствует информация о списке условий "condition_list"');

        foreach ($this->dataset['condition_list'] as $class_name => $class_params) {
            /* @var \Editorum\Bundle\AgreementSystem\Conditions\BasicCondition $condition */
            $condition = new $class_name();
            $condition->load($class_params);
            $this->addCondition($condition);
        }
    }

}
