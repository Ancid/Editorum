<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 17.03.16
 * Time: 11:43
 */

namespace Editorum\Bundle\AgreementSystem\Conditions;


use Editorum\Bundle\AgreementSystem\Agreements\BasicAgreement;
use Editorum\Bundle\Logic\Model;

class UniqueAgreementCondition extends BasicCondition
{
    /* @var \Editorum\Bundle\AgreementSystem\Agreements\BasicAgreement $agreement */
    protected $agreement;

    /**
     * UniqueAgreementCondition constructor.
     * @param BasicAgreement $agreement
     */
    public function __construct(BasicAgreement $agreement)        
    {
        parent::__construct();
        $this->agreement = $agreement;
    }

    public function check()
    {
        $conditions = array_merge(
            ['agreement_type' => $this->agreement->getAgreementType()],
            $this->agreement->getSourceSide()->save([]),
            $this->agreement->getDestinationSide()->save([]),
            ['is_active' => true]
        );
        $agreements = Model::getAll($this->agreement, $conditions);
        if (is_null($agreements->getCurrentRecord()))
            return true;
        return false;
    }
    
}
