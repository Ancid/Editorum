<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 05.11.2015
 * Time: 13:57
 *
 * Данный класс отвечает за список персон или организаций одной сторны договора
 *
 */

namespace Editorum\Bundle\AgreementSystem;

use Exception;
use Editorum\Bundle\AgreementSystem\Exceptions\AgreementException;

class BasicSide
{
    /**
     * Сторона, источник прав
     */
    const SRC_SIDE = 'src_side';

    /**
     * Сторона, получатель прав
     */
    const DST_SIDE = 'dst_side';

    protected $members = [];
    protected $side = false;

    /**
     * Инициализируем сторону
     * @param bool $side
     */
    public function __construct($side = false)
    {
        $this->side = $side;
        $this->members['persons'] = [];
        $this->members['organizations'] = [];
    }

    /**
     * Добавить к стороне физическое лицо
     * @param $person_id
     */
    public function addPerson($person_id)
    {
        array_push($this->members['persons'], $person_id);
    }

    /**
     * Добавить к стороне юридическое лицо
     * @param $org_id
     */
    public function addOrganization($org_id)
    {
        array_push($this->members['organizations'], $org_id);
    }
    
    /**
     * Подготавливаем выборку родителя к сохранению данных стороны
     * @param $dataset
     * @return array
     * @throws AgreementException
     */
    public function save($dataset)
    {
        if (!is_array($dataset))
            throw new AgreementException('В качестве dataset должен быть передан массив');

        $dataset[$this->side] = $this->members;
        return $dataset;
    }

    /**
     * Загружаем данные из родительской выборки
     * @param $dataset
     * @throws Exception
     */
    public function load($dataset)
    {
        if (!isset($dataset[$this->side]))
            throw new Exception('В договоре отсутствует информация о стороне договора "' . $this->side . '"');

        $this->members = $dataset[$this->side];
    }
}
