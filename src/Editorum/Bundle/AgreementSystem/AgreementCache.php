<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 29.02.16
 * Time: 11:54
 */

namespace Editorum\Bundle\AgreementSystem;

use Editorum\Bundle\Logic\MongoNode;
use Editorum\Bundle\AgreementSystem\Agreements\BasicAgreement;

class AgreementCache extends MongoNode
{
    protected $user_id;
    
    public function createEmpty() {
        // user_id : {rights : [{entity_list : [], conditions : {}}, {entity_list : [], conditions : {}}], ...}
        return [
            'user_id' => null,
            'rights' => [],
        ];
    }
    
    public function getCollectionName()
    {
        return 'agreementscache';
    }

    /**
     * @param BasicAgreement $agr_obj
     * @return array
     */
    public function prepareToSave(BasicAgreement $agr_obj) {
        $agreement_dataset = $agr_obj->saveAgreement();
        $user_ids = $agreement_dataset['dst_side']['persons'];
        $dataset = [];

        foreach ((array)$user_ids as $user_id) {
            $current_dataset = [];
            $current_dataset['user_id'] = $user_id;

            $current_dataset['rights'] = [];
            foreach ((array)$agreement_dataset['rights'] as $right_record) {
                $current_dataset['rights'][$right_record] = [
                    'entity_list' => $agreement_dataset['entity_list'],
                    'condition_list' => $agreement_dataset['condition_list']
                ];
            }
            array_push($dataset, $current_dataset);
        }
        
        return $dataset;
    } 
}
