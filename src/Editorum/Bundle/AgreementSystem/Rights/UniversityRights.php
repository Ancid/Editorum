<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 30.03.16
 * Time: 14:06
 */

namespace Editorum\Bundle\AgreementSystem\Rights;

class UniversityRights extends BasicRights
{
    const UNIVERSITY_LIST = 'university_index';
    const UNIVERSITY_ADD = 'university_add';
    const UNIVERSITY_EDIT = 'university_edit';
    const UNIVERSITY_SAVE = 'university_save';
    const UNIVERSITY_DELETE = 'university_delete';
}
