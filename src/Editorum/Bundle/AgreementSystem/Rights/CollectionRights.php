<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 31.03.16
 * Time: 9:19
 */

namespace Editorum\Bundle\AgreementSystem\Rights;


class CollectionRights extends BasicRights
{
    const COLLECTIONS = 'collections';
    const COLLECTION_SHOW = 'collection_show';
    const COLLECTION_EDIT = 'collection_edit';
    const COLLECTION_CREATE = 'collection_create';
    const COLLECTION_REMOVE = 'collection_remove';
    const COLLECTION_EDIT_COMPOSITION = 'collection_edit_composition';
    const COLLECTION_SAVE_COMPOSITION = 'collection_save_composition';
}
