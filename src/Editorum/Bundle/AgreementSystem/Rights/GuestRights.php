<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 22.03.16
 * Time: 16:39
 */

namespace Editorum\Bundle\AgreementSystem\Rights;


class GuestRights extends BasicRights
{
    const HOMEPAGE = 'homepage';
    const LOGIN = 'login';
}
