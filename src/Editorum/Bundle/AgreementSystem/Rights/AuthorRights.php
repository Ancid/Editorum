<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 31.03.16
 * Time: 9:36
 */

namespace Editorum\Bundle\AgreementSystem\Rights;


class AuthorRights extends BasicRights
{
    const AUTHOR_TYPEAHEAD = 'author_typeahead';
    const AUTHOR_CREATE = 'author_create';
    const AUTHOR_REMOVE = 'author_remove';
    const AUTHORS_LIST = 'authors_list';
    const AUTHOR_EDIT = 'author_edit';
    const AUTHOR_SHOW = 'author_show';
}
