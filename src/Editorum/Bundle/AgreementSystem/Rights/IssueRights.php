<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 31.03.16
 * Time: 9:19
 */

namespace Editorum\Bundle\AgreementSystem\Rights;


class IssueRights extends BasicRights
{
    const ISSUE_ALL_LIST = 'issue_all_list';
    const ISSUE_LIST = 'issue_list';
    const ISSUE_SHOW = 'issue_show';
    const ISSUE_EDIT = 'issue_edit';
    const ISSUE_SAVE = 'issue_save';
    const ISSUE_REMOVE = 'issue_remove';
    const ISSUE_COMPOSE = 'issue_compose';
    const ISSUE_CLASSIF = 'issue_classif';
    const ISSUE_COMPOSE_SAVE = 'issue_compose_save';
    const ISSUE_DOI = 'issue_doi';
    const ISSUE_SENDDOI = 'issue_senddoi';
}
