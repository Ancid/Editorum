<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 31.03.16
 * Time: 9:40
 */

namespace Editorum\Bundle\AgreementSystem\Rights;


class ChapterRights extends BasicRights
{
    const CHAPTER_CREATE = 'chapter_create';
    const CHAPTER_REMOVE = 'chapter_remove';
    const CHAPTER_EDIT = 'chapter_edit';
}
