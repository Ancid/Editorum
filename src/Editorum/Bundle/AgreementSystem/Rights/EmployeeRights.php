<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 16.03.16
 * Time: 10:07
 */

namespace Editorum\Bundle\AgreementSystem\Rights;


class EmployeeRights extends BasicRights
{
    const EMPLOYEE_INDEX = 'employee_index';
    const EMPLOYEE_EDIT = 'employee_edit';
    const EMPLOYEE_SAVE = 'employee_save';
    const EMPLOYEE_DROP = 'employee_drop';
}
