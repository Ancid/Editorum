<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 31.03.16
 * Time: 9:51
 */

namespace Editorum\Bundle\AgreementSystem\Rights;


class TestRights extends BasicRights
{
    const TEST = 'test';
    const TEST_AGREEMENT = 'test_agreement';
}
