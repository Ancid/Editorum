<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 16.03.16
 * Time: 9:47
 */

namespace Editorum\Bundle\AgreementSystem\Rights;

class PublicationRights extends BasicRights
{
    const PUBLICATION_LIST = 'publication_list';
    const PUBLICATION_SHOW = 'publication_show';
    const PUBLICATION_AUTHORS_SAVE = 'publication_authors_save';
    const PUBLICATION_AUTHORS = 'publication_authors';
    const PUBLICATION_CREATE_NEW = 'publication_create_new';
    const PUBLICATION_CREATE_SAVE = 'publication_create_save';
    const PUBLICATION_REMOVE = 'publication_remove';
    const PUBLICATION_METADATA = 'publication_metadata';
    const PUBLICATION_DOI = 'publication_doi';
    const PUBLICATION_SENDDOI = 'publication_senddoi';
    const PUBLICATION_DESCRIPTION = 'publication_description';
    const PUBLICATION_FILES = 'publication_files';
    const PUBLICATION_CHAPTERS = 'publication_chapters';
    const PUBLICATION_REFERENCES = 'publication_references';
}
