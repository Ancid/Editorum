<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 05.11.2015
 * Time: 14:18
 */

namespace Editorum\Bundle\AgreementSystem\Rights;

use ReflectionClass;

class BasicRights
{
    /**
     * Return all rights
     * @return array
     */
    public function getAllRights()
    {
        $ref = new ReflectionClass($this);
        return $ref->getConstants();
    }

}
