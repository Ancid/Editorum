<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 31.03.16
 * Time: 9:43
 */

namespace Editorum\Bundle\AgreementSystem\Rights;


class ConferenceRights extends BasicRights
{
    const CONFERENCES = 'conferences';
    const CONFERENCES_SHOW = 'conference_show';
    const CONFERENCES_EDIT = 'conference_edit';
    const CONFERENCE_CREATE = 'conference_create';
    const CONFERENCE_REMOVE = 'conference_remove';
}
