<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 31.03.16
 * Time: 9:41
 */

namespace Editorum\Bundle\AgreementSystem\Rights;


class LogRights extends BasicRights
{
    const LOG = 'log';
    const LOGS = 'logs';
}
