<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 31.03.16
 * Time: 9:20
 */

namespace Editorum\Bundle\AgreementSystem\Rights;


class SerieRights extends BasicRights
{
    const SERIE_ALL_LIST = 'serie_all_list';
    const SERIE_LIST = 'serie_list';
    const SERIE_SHOW = 'serie_show';
    const SERIE_EDIT = 'serie_edit';
    const SERIE_SAVE = 'serie_save';
    const SERIE_REMOVE = 'serie_remove';
}
