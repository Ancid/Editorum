<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 31.03.16
 * Time: 9:27
 */

namespace Editorum\Bundle\AgreementSystem\Rights;


class JournalRights extends BasicRights
{
    const JOURNAL_SENDOI = 'journal_sendoi';
    const JOURNAL_DOI = 'journal_doi';
    const JOURNAL_CLASSIF = 'journal_classif';
    const JOURNAL_LIST = 'journal_list';
    const JOURNAL_SHOW = 'journal_show';
    const JOURNAL_EDIT = 'journal_edit';
    const JOURNAL_SAVE = 'journal_save';
    const JOURNAL_REMOVE = 'journal_remove';
}
