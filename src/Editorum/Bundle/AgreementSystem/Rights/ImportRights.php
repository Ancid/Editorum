<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 31.03.16
 * Time: 9:21
 */

namespace Editorum\Bundle\AgreementSystem\Rights;


class ImportRights extends BasicRights
{
    const MAKEJSON = 'makejson';
    const IMPORT_RUBRIC = 'import_rubric';
    const IMPORT_CUSTOM = 'import_custom';
}
