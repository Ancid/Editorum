<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 17.03.16
 * Time: 17:33
 */

namespace Editorum\Bundle\AgreementSystem\Rights;

class OrganizationRights extends BasicRights
{
    const EDITORUM_ORGS = 'editorum_orgs';
    const EDITORUM_ORG_EDIT = 'editorum_org_edit';
    const EDITORUM_ORG_SAVE = 'editorum_org_save';
    const EDITORUM_EMPLOYEES = 'editorum_employees';
    const EDITORUM_EMPLOYEE_INVITE = 'editorum_employee_invite';
    const EDITORUM_EMPLOYEE_INVITATION_SEND = 'editorum_employee_invitation_send';
}
