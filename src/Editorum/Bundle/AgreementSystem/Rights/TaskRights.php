<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 31.03.16
 * Time: 10:09
 */

namespace Editorum\Bundle\AgreementSystem\Rights;


class TaskRights extends BasicRights
{
    const TASKS = 'tasks';
    const TASK_VIEW = 'task_view';
    const task_manage = 'task_manage';
    const task_create = 'task_create';
}
