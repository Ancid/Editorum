<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 31.03.16
 * Time: 8:55
 */

namespace Editorum\Bundle\AgreementSystem\Agreements;


use Editorum\Bundle\AgreementSystem\BasicSide;
use Editorum\Bundle\AgreementSystem\Conditions\BasicConditionList;
use Editorum\Bundle\AgreementSystem\Conditions\ExistEmployeeAgreementCondition;
use Editorum\Bundle\AgreementSystem\Conditions\UniqueAgreementCondition;
use Editorum\Bundle\AgreementSystem\Rights\AuthorRights;
use Editorum\Bundle\AgreementSystem\Rights\ChapterRights;
use Editorum\Bundle\AgreementSystem\Rights\CollectionRights;
use Editorum\Bundle\AgreementSystem\Rights\ConferenceRights;
use Editorum\Bundle\AgreementSystem\Rights\IssueRights;
use Editorum\Bundle\AgreementSystem\Rights\JournalRights;
use Editorum\Bundle\AgreementSystem\Rights\PublicationRights;
use Editorum\Bundle\AgreementSystem\Rights\RINCRights;
use Editorum\Bundle\AgreementSystem\Rights\SerieRights;
use Editorum\Bundle\AgreementSystem\Rights\TaskRights;
use Editorum\Bundle\AgreementSystem\StaticEntityList;

class ChiefEditorAgreement extends BasicAgreement
{
    /**
     * @inheritdoc
     */
    public $title = 'Назначение на должность главного редактора';

    /**
     * @inheritdoc
     * @param $organization_id integer // Организация, принимающая на работу
     * @param $user_id integer // Пользователь, принимаемый на работу
     * @return $this
     */
    public function create($organization_id, $user_id)
    {
        $from = new BasicSide(BasicSide::SRC_SIDE);
        $from->addOrganization($organization_id);

        $to = new BasicSide(BasicSide::DST_SIDE);
        $to->addPerson($user_id);

        $el = new StaticEntityList();
        $cond = new BasicConditionList();

        $this->registerRights();

        $preconds = new BasicConditionList();
        $preconds->addCondition(new ExistEmployeeAgreementCondition($this));
        $preconds->addCondition(new UniqueAgreementCondition($this));

        $this->_create($from, $to, $el, $preconds, $cond);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function registerRights()
    {
        parent::registerRights();

        $this->addRights(new AuthorRights());
        $this->addRights(new ChapterRights());
        $this->addRights(new CollectionRights());
        $this->addRights(new ConferenceRights());
        $this->addRights(new IssueRights());
        $this->addRights(new JournalRights());
        $this->addRights(new PublicationRights());
        $this->addRights(new RINCRights());
        $this->addRights(new SerieRights());
        $this->addRights(new TaskRights());

        return $this;
    }

}
