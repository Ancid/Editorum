<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 09.10.2015
 * Time: 14:35
 */

namespace Editorum\Bundle\AgreementSystem\Agreements;

use Editorum\Bundle\AgreementSystem\ASys;
use ReflectionClass;
use Editorum\Bundle\Logic\User;
use Editorum\Bundle\Logic\Model;
use Editorum\Bundle\Logic\Organization;
use Editorum\Bundle\Logic\MongoNode;
use Editorum\Bundle\AgreementSystem\BasicEntityList;
use Editorum\Bundle\AgreementSystem\BasicSide;
use Editorum\Bundle\AgreementSystem\Conditions\BasicConditionList;
use Editorum\Bundle\AgreementSystem\Rights\BasicRights;

class BasicAgreement extends MongoNode
{
    /**
     * Статус активного договора
     */
    const AGR_ENABLED = 'agr_active';

    /**
     * Статус неактивного договора
     */
    const AGR_FINISHED = 'agr_finished';

    /**
     * Статус приостановленного договора
     */
    const AGR_BANNED = 'agr_banned';

    /**
     * Статус черновика договора
     */
    const AGR_DRAFT = 'agr_draft';
    
    protected $ASys = null;
    
    private $collection = null;
    /**
     * Источник передаваемых по договору прав
     * @var null|BasicSide
     */
    protected $src_side = null;

    /**
     * Получатель передаваемых по договору прав
     * @var null|BasicSide
     */
    protected $dst_side = null;

    /**
     * Что передается по договору? Набор сущностей
     * @var null|BasicEntityList
     */
    protected $entity_list = null;

    /**
     * Условия для формирования договора
     * @var null|BasicConditionList
     */
    protected $precondition_list = null;

    /**
     * условия для набора сущностей, передаваемых по договору
     * @var null|BasicConditionList
     */
    protected $condition_list = null;

    /**
     * Права, передаваемые по договору
     * @var array
     */
    protected $rights = [];

    /**
     * Договор имеет печатную форму
     * @var bool
     */
    public $is_printable = null;

    /**
     * Название договора
     * @var string
     */
    public $title = '';

    /**
     * Дата вступления договора в силу
     * @var null|string
     */
    protected $begin_date = null;

    /**
     * Дата окончания действия договора
     * @var null|string
     */
    protected $end_date = null;

    /**
     * Состояние договора
     * @var null|string
     */
    protected $status = null;

    /**
     * Активен ли договор
     * @var null|string
     */
    protected $is_active = null;

    /**
     * BasicAgreement constructor.
     */
    public function __construct(ASys $ASys = null)
    {
        parent::__construct();
        if ($ASys)
            $this->ASys = $ASys;
        $this->collection = 'agreements';
        $this->is_active = false;
        $this->status = $this::AGR_DRAFT;
        $this->registerRights();
        return $this;
    }

    /**
     * @return array
     */
    public function createEmpty()
    {
        return [
            'agreement_type' => '',
//            'title' => '',
//            'status' => '',
//            'is_active' => '',
//            'is_printable' => '',
//            'begin_date' => '',
//            'end_date' => '',
//            'rights' => '',
        ];
    }

    /**
     * Создаем новый договор
     * @param BasicSide $src_side
     * @param BasicSide $dst_side
     * @param BasicEntityList $entity_list
     * @param BasicConditionList $condition_list
     */
    public function _create(
        $src_side,
        $dst_side,
        $entity_list,
        $precondition_list,
        $condition_list
    )
    {
        $this->src_side = $src_side;
        $this->dst_side = $dst_side;
        $this->entity_list = $entity_list;
        $this->precondition_list = $precondition_list;
        $this->condition_list = $condition_list;
    }

    /**
     * Register all rights of the agreement
     * @return $this
     */
    public function registerRights()
    {
        $this->addRights(new BasicRights());
        return $this;
    }

    /**
     * Add rights object into agreement
     * @param BasicAgreement $obj
     * @return $this
     */
    protected function addRights(BasicRights $obj)
    {
        $this->rights = array_merge($this->rights, $obj->getAllRights());
        return $this;
    }

    /**
     * @return BasicEntityList|null
     */
    public function getEntityList()
    {
        return $this->entity_list;
    }

    /**
     * Получить типизированный объект договора
     * 
     * @return BasicAgreement
     * @throws \Exception
     */
    public function getMutant()
    {
        $agreement = $this->ASys->getAgreementObject($this->__get('agreement_type'));
        $agreement->setDataset([0 => $this->current_record]);
        
        return $agreement;
    }

    /**
     * @return array
     * @throws \Editorum\Bundle\AgreementSystem\Exceptions\AgreementException
     */
    public function saveAgreement()
    {
        $dataset = [];
        $dataset['agreement_type'] = $this->getClass();

        if(isset($this->_id) && !is_null($this->_id)) $dataset['_id'] = $this->_id;
        if(!is_null($this->title)) $dataset['title'] = $this->title;
        if(!is_null($this->status)) $dataset['status'] = $this->status;
        if(!is_null($this->begin_date)) $dataset['begin_date'] = $this->begin_date;
        if(!is_null($this->end_date)) $dataset['end_date'] = $this->end_date;
        if(!is_null($this->is_active)) $dataset['is_active'] = $this->is_active;
        if(!is_null($this->is_printable)) $dataset['is_printable'] = $this->is_printable;
        if(!is_null($this->rights)) $dataset['rights'] = $this->rights;
        
        $dataset = $this->src_side->save($dataset);
        $dataset = $this->dst_side->save($dataset);
        $dataset = $this->entity_list->save($dataset);
        $dataset = $this->condition_list->save($dataset);
        
        return $dataset;
    }

    /**
     * Подменяем коллекцию, чтобы хранить разнородные объекты в одной
     * @return string
     */
    public function getCollectionName()
    {
        return 'agreements';
    }

    /**
     * @return string
     */
    public function getAgreementType()
    {
        $ref = new ReflectionClass($this);
        $class_name = $ref->getShortName();
        return $class_name;
    }
    
    
    public function getAgreementRights()
    {
        return $this->rights;
    }

    /**
     * Применить условия договора и внести сущности в кеш
     * @return $this
     */
    public function execute()
    {
        /**
         * Проверяем условия срабатывания договора
         */
        if ($this->precondition_list->testConditions()) {
            $this->is_active = true;
            $this->status = $this::AGR_ENABLED;
        }
        return $this;
    }

    /**
     * @return null|string
     */
    public function isActive() {
        if ($this->is_active)
            return true;
        return false;
    }

    /**
     * @return BasicSide|null
     */
    public function getSourceSide() {
        return $this->src_side;
    }
    
    /**
     * @return BasicSide|null
     */
    public function getDestinationSide() {
        return $this->dst_side;
    }

    /**
     * @return string
     */
    public function getSourceSideName() {
        $result = '';
        $record = $this->current()->getCurrentRecord();
        if (array_key_exists('src_side', $record)) {
            $source = $record['src_side'];
            $result .= $this->parseAndFindSideNames($source);
        }        
        return $result;
    }

    /**
     * @return string
     */
    public function getDestinationSideName() {
        $result = '';
        $record = $this->current()->getCurrentRecord();
        if (array_key_exists('dst_side', $record)) {
            $source = $record['dst_side'];
            $result .= $this->parseAndFindSideNames($source);
        }        
        return $result;
    }

    /**
     * @param array $dataset
     * @return mixed
     */
    protected function parseAndFindSideNames(array $dataset) {
        $result = [];
        if (array_key_exists('organizations', $dataset)) {
            foreach ((array) $dataset['organizations'] as $organization_id) {
                $organization = Model::getById(new Organization(), $organization_id);
                array_push($result, $organization->__get('name'));
            }
        }
        if (array_key_exists('persons', $dataset)) {
            foreach ((array) $dataset['persons'] as $user_id) {
                $user = Model::getById(new User(), $user_id);
                array_push($result, $user->__get('username'));
            }
        }
        return implode(', ', $result);
    }

    /**
     * @return array
     */
    public static function getStatusList() {
        $ref = new ReflectionClass(new BasicAgreement());
        return $ref->getConstants();
    }

    /**
     * @param $status
     * @return mixed|string
     */
    public function getStatusName() {
        $status = $this->__get('status');
        $nameStatusList = [
            self::AGR_ENABLED => 'Действует',
            self::AGR_FINISHED => 'Истек',
            self::AGR_BANNED => 'Приостановлен',
            self::AGR_DRAFT => 'Черновик'
        ];
        if (in_array($status, self::getStatusList()) && array_key_exists($status, $nameStatusList))
            return $nameStatusList[$status];
        return 'Неизвестно';
    }
}
