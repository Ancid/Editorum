<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 09.12.2015
 * Time: 14:42
 */

namespace Editorum\Bundle\AgreementSystem\Agreements;

use Editorum\Bundle\AgreementSystem\BasicSide;
use Editorum\Bundle\AgreementSystem\StaticEntityList;
use Editorum\Bundle\AgreementSystem\Conditions\BasicConditionList;
use Editorum\Bundle\AgreementSystem\Conditions\UniqueAgreementCondition;
use Editorum\Bundle\AgreementSystem\Rights\EmployeeRights;
use Editorum\Bundle\AgreementSystem\Rights\OrganizationRights;

class EmployeeAgreement extends BasicAgreement
{
    /**
     * @inheritdoc
     */
    public $title = 'Договор работы по найму';
    
    /**
     * @inheritdoc
     * @param $organization_id integer // Организация, принимающая на работу
     * @param $user_id integer // Пользователь, принимаемый на работу
     * @return $this
     */
    public function create($organization_id, $user_id)
    {
        $from = new BasicSide(BasicSide::SRC_SIDE);
        $from->addOrganization($organization_id);

        $to = new BasicSide(BasicSide::DST_SIDE);
        $to->addPerson($user_id);

        $el = new StaticEntityList();
        $cond = new BasicConditionList();

        $this->registerRights();

        $preconds = new BasicConditionList();
        $preconds->addCondition(new UniqueAgreementCondition($this));
        
        $this->_create($from, $to, $el, $preconds, $cond);
        
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function registerRights()
    {
        parent::registerRights();
        
        return $this;
    }
    
}
