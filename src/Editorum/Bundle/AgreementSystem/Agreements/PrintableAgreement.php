<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 06.11.2015
 * Time: 13:41
 *
 * Печатный договор
 *
 */

namespace Editorum\Bundle\AgreementSystem\Agreements;

use Editorum\Bundle\Logic\MongoNode;
use Editorum\Bundle\Logic\Publication;
use Editorum\Bundle\Logic\Model;

class PrintableAgreement extends BasicAgreement
{
    /**
     * @return array
     */
    public function createEmpty()
    {
        $obj = array(
            'number'        => '',  // Номер договора
            'text'          => '',  // Текст договора HTML
            'pdf_file'      => '',  // Путь к файлу PDF
            'pdf_url'       => '',  // URL файла PDF
            'agreement_id'  => 0,   // ID системного договора отражающего функционирование
            'pub_id'        => 0

        );

        return $obj;
    }

    /**
     * Получить связанный системный договор
     * @return mixed
     */
//    public function getAgreement()
//    {
//        return Model::getById(new Agreement(), $this->agreement_id);
//    }

    /**
     * @return mixed
     */
    public function calcPubname()
    {
        $pub = Model::getById(new Publication(), $this->__get('pub_id'));
        return $pub->title;
    }

    /**
     * @return string
     */
    public function calcTextlink()
    {
        return '/agreement/view/'.$this->__get('_id').'/';
    }

    /**
     * @return string
     */
    public function calcAuthorsList()
    {
        return 'Здесь должен быть список авторов';
    }

}
