<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 05.11.2015
 * Time: 14:11
 *
 * Виртуальный договор передачи прав издательству на рассмотрение заявки
 */

namespace Editorum\Bundle\AgreementSystem\Agreements;

class OrderAgreement extends BasicAgreement
{
    public $title = 'Договор-заявка на публикацию';
    
    public static function create($publication, $owner, $publisher)
    {
        $from = new BasicSide(BasicSide::SRC_SIDE);
        $from->addPerson($owner->_id);

        $to = new BasicSide(BasicSide::DST_SIDE);
        $to->addOrganization($publisher->_id);

        $el = new StaticEntityList();
        $el->addPublication($publication->_id);

        $cond = new EmptyCondition();

        $agreement = new BasicAgreement();
        $agreement->_create($from, $to, $el, $cond, array());
    }
}
