<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 05.11.2015
 * Time: 14:11
 *
 * Виртуальный договор передачи прав издательству на рассмотрение заявки
 */

namespace Editorum\Bundle\AgreementSystem\Agreements;

class OwnerAgreement extends BasicAgreement
{
    public $title = 'Договор прав собственности на публикацию';
    
    public function create($publication, $owner)
    {
        $from = new BasicSide(BasicSide::SRC_SIDE);
        $from->addPerson(0);

        $to = new BasicSide(BasicSide::DST_SIDE);
        $to->addPerson($owner->_id);

        $el = new StaticEntityList();
        $el->addPublication($publication->_id);

        $cond = new EmptyCondition();

        $agreement = new BasicAgreement();
        $agreement->_create($from, $to, $el, $cond, array());
    }
}
