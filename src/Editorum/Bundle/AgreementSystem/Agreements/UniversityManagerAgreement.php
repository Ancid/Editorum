<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 24.03.16
 * Time: 16:06
 */

namespace Editorum\Bundle\AgreementSystem\Agreements;

use Editorum\Bundle\AgreementSystem\BasicSide;
use Editorum\Bundle\AgreementSystem\Conditions\BasicCondition;
use Editorum\Bundle\AgreementSystem\Conditions\BasicConditionList;
use Editorum\Bundle\AgreementSystem\Conditions\UniqueAgreementCondition;
use Editorum\Bundle\AgreementSystem\Rights\UniversityRights;
use Editorum\Bundle\AgreementSystem\StaticEntityList;
use Editorum\Bundle\Logic\User;
use Editorum\Bundle\Logic\Model;
use Editorum\Bundle\Logic\Universities;

class UniversityManagerAgreement extends BasicAgreement
{
    /**
     * @inheritdoc
     */
    public $title = 'Договор администрирования учетной записи ВУЗа';

    /**
     * @inheritdoc
     * @param $organization_id integer // Организация, принимающая на работу
     * @param $user_id integer // Пользователь, принимаемый на работу
     * @return $this
     */
    public function create($organization_id, $user_id)
    {
        $from = new BasicSide(BasicSide::SRC_SIDE);
        $from->addOrganization($organization_id);

        $to = new BasicSide(BasicSide::DST_SIDE);
        $to->addPerson($user_id);

        $el = new StaticEntityList();
        $cond = new BasicConditionList();
        $cond->addCondition(new BasicCondition(['university_id' => $organization_id]));

        $this->registerRights();

        $preconds = new BasicConditionList();
        $preconds->addCondition(new UniqueAgreementCondition($this));

        $this->_create($from, $to, $el, $preconds, $cond);
        
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function registerRights()
    {
        parent::registerRights();
        
        $this->addRights(new UniversityRights());
        
        return $this;
    }

    /**
     * @param array $dataset
     * @return mixed
     */
    protected function parseAndFindSideNames(array $dataset) {
        $result = [];
        if (array_key_exists('organizations', $dataset)) {
            foreach ((array) $dataset['organizations'] as $organization_id) {
                $organization = Model::getById(new Universities(), $organization_id);
                array_push($result, $organization->__get('title'));
            }
        }
        if (array_key_exists('persons', $dataset)) {
            foreach ((array) $dataset['persons'] as $user_id) {
                $user = Model::getById(new User(), $user_id);
                array_push($result, $user->__get('username'));
            }
        }
        
        return implode(', ', $result);
    }
    
}
