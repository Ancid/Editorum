<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 06.11.2015
 * Time: 13:39
 *
 * Класс шаблона печатного договора
 */

namespace Editorum\Bundle\AgreementSystem\Agreements;

use Editorum\Bundle\Logic\MongoNode;

class PrintableAgreementTemplate extends MongoNode
{
    const T_INDIVIDUAL = 1;
    const T_COLLECTIVE = 2;

    public function createEmpty()
    {
        $obj = array(
            'title'             => '',      // Название шаблона
            'descr'             => '',      // Описание шаблона
            'organization_id'   => 0,       // Организация, которая создала шаблон
            'template'          => '',      // Шаблон Twig
            'is_collective'     => 0,       // Признак коллективного договора
            'master_class'      => ''       // Класс BasicAgreement от которого унаследован договор
        );

        return $obj;
    }

    /**
     * Текстовое значение поля коллективный договор
     */
    public function calcCollective()
    {
        $ar = array(
            self::T_INDIVIDUAL   => 'Индивидуальный',
            self::T_COLLECTIVE   => 'Коллективный'
        );
        return $ar[$this->current_record['is_collective']];
    }

}
