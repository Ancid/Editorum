<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 16.11.2015
 * Time: 12:19
 */

namespace Editorum\Bundle\AgreementSystem\Agreements;

class PublishingAgreement extends BasicAgreement
{
    public $is_printable = true;

    public $title = 'Договор передачи прав от автора издательству';

    public function create($publication, $owner)
    {
        $from = new BasicSide(BasicSide::SRC_SIDE);
        $from->addPerson(0);

        $to = new BasicSide(BasicSide::DST_SIDE);
        $to->addPerson($owner->_id);

        $el = new StaticEntityList();
        $el->addPublication($publication->_id);

        $cond = new EmptyCondition();

        $agreement = new BasicAgreement();
        $agreement->_create($from, $to, $el, $cond, array());
    }
}
