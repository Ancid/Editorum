<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 22.08.2016
 * Time: 17:17
 */

namespace Editorum\Bundle\Command;

use Editorum\Bundle\Document\Conference;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ConferenceStatusesCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('conference:statuses:update')
            ->setDescription('Update conference statuses')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $odm = $container->get('doctrine.odm.mongodb.document_manager');

        $qb = $odm->createQueryBuilder('AsuBundle:Conference');
        $now = new \DateTime();
        //Список конференций которые должны быть активными
        $qb
            ->addAnd($qb->expr()->field('statuses.status')->equals(Conference::ST_INACTIVE))
            ->addAnd($qb->expr()->field('date_start')->lte($now))
            ->addAnd($qb->expr()->field('date_finish')->gt($now));

        /** @var Conference[] $confs */
        $confs = $qb->getQuery()->execute();
        foreach ($confs as $c) {
            $c->setStatus('status', Conference::ST_ACTIVE);
        }

        $qb = $odm->createQueryBuilder('AsuBundle:Conference');
        //Список конференция которые должны быть неактивными
        $qb
            ->addAnd($qb->expr()->field('statuses.status')->equals(Conference::ST_ACTIVE))
            ->addAnd($qb->expr()->field('date_finish')->lt($now));

        /** @var Conference[] $confs */
        $confs = $qb->getQuery()->execute();
        foreach ($confs as $c) {
            $c->setStatus('status', Conference::ST_CLOSED);
        }

        $odm->flush();

        $output->writeln('<info>Conferences updated successfully!</info>');
    }
}
