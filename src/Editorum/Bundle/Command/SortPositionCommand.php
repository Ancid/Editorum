<?php

namespace Editorum\Bundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SortPositionCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('asu:geography:sort')
            ->addOption('document', 'd', InputOption::VALUE_OPTIONAL, 'ODM document name', 'Country')
            ->addOption('bundle', 'b', InputOption::VALUE_OPTIONAL, 'Bundle name', 'AsuBundle')
            ->addOption('id', 'i', InputOption::VALUE_REQUIRED, 'Item element id', null)
            ->addOption('position', 'p', InputOption::VALUE_REQUIRED, 'New item position order', 1)
            ->setDescription('Set item position in document');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $options = $input->getOptions();
        $container = $this->getContainer();
        $odm = $container->get('doctrine.odm.mongodb.document_manager');

        $document = $options['bundle'].':'.$options['document'];

        $object = $odm->getRepository($document)->findOneBy(['external_id' => (int)$options['id']]);
        $object->setPosition($options['position']);
        $odm->flush($object);

        $output->writeln(sprintf(
            'Position for item "#%s: %s" in %s was changed to %s.',
            $object->getId(),
            $object->getRu()['title'],
            $document,
            $options['position']
        ));
    }
}
