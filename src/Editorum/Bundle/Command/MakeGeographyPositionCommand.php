<?php

namespace Editorum\Bundle\Command;

use Editorum\Bundle\Document\Country;
use Editorum\Bundle\EventListener\SortableListener;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MakeGeographyPositionCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('asu:geography:positioning')
            ->addOption('bundle', 'b', InputOption::VALUE_OPTIONAL, 'Bundle name', 'AsuBundle')
            ->addOption('document', 'd', InputOption::VALUE_OPTIONAL, 'Collection document', 'Country')
            ->setDescription('Add position indexes to geography collections');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $mongoDb = $container->get('doctrine.odm.mongodb.document_manager');

        // Stopping sortable listener for making new indexes in positions
        SortableListener::stopListener(true);

        $document = $input->getOption('document');
        $bundle = $input->getOption('bundle');

        $countries = $mongoDb->getRepository($bundle.':'.$document)->findAllSortedBy('ru.title');

        // костыльная сортировка по алфавиту т.к. сортировка монги работает совсем некорректно
        $titles = [];
        foreach ($countries as $key => $country) {
            $titles[ $key ] = $country->getRu()['title'];
        }
        sort($titles, SORT_LOCALE_STRING);
        unset($countries);

        $progressBar = new ProgressBar($output, count($titles));
        $progressBar->setBarCharacter('<fg=magenta>=</>');
        $progressBar->setProgressCharacter(">");

        foreach ($titles as $key => $title) {
            $country = $mongoDb->getRepository($bundle . ':' . $document)
                ->createQueryBuilder()->field('ru.title')->equals($title)
                ->getQuery()->getSingleResult()
            ;
            $country->setPosition($key);
            $mongoDb->persist($country);
            $progressBar->advance();
        }

        $mongoDb->flush();
        // Resume sortable listener working
        SortableListener::stopListener(false);

        $progressBar->finish();

        $output->writeln('');
        $output->writeln(sprintf('%s:%s position reindexed.', $bundle, $document));
        $output->writeln(sprintf('There was %d entries.', count($titles)));
    }
}
