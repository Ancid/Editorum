<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 22.08.2016
 * Time: 17:17
 */

namespace Editorum\Bundle\Command;

use Editorum\Bundle\Document\Conference;
use Editorum\Bundle\Document\Doi;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DoiSendCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('doi:send')
            ->setDescription('Send doi queue')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $odm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
        $dois = $odm->getRepository('AsuBundle:Doi')->findBy(['status' => Doi::ST_QUEUE]);

        $count = 1;

        /** @var Doi $doi */
        foreach ($dois as $doi) {
            $doi->setStatus(Doi::ST_SEND);

            $path = 'web'.DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR.'xml'.DIRECTORY_SEPARATOR;
            $tmp_filename = date('Y-m-d-H-i-s').rand(0, 100000).'.xml';
            file_put_contents($path.$tmp_filename, $doi->getXml());
            $doi->setFileName($tmp_filename);

            $ch = curl_init();

            if (false === $ch) {
                $output->writeln('<info>failed to initialize curl!</info>');
                break;
            }

            $cFile = curl_file_create(realpath($tmp_filename));
            $header = ['Content-type: multipart/form-data'];
            curl_setopt($ch, CURLOPT_URL, 'https://doi.crossref.org/servlet/deposit');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, [
                'operation'    => 'doMDUpload',
                'login_id'     => 'editorum',
                'login_passwd' => 'editorum20151',
                'fname'        => $cFile,
            ]);

            $result = curl_exec($ch);

            if (false === $result) {
                $output->writeln('<info>'.curl_error($ch).'</info>');
                break;
            } else {
                $output->writeln('<info>'.$result.'</info>');
            }

            curl_close($ch);
            $count++;
        }

        $odm->flush();
        $output->writeln('<info>Successfully send '.$count.' requests!</info>');
    }
}
