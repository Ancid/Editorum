<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 22.08.2016
 * Time: 17:17
 */

namespace Editorum\Bundle\Command;

use Editorum\Bundle\Document\Conference;
use Editorum\Bundle\Document\Doi;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DoiCheckCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('doi:check')
            ->setDescription('Check doi for sended requests')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $odm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
        $dois = $odm->getRepository('AsuBundle:Doi')->findBy(['status' => Doi::ST_SEND]);

        $count = 1;

        /** @var Doi $doi */
        foreach ($dois as $doi) {
            if (empty($doi->getFileName())) {
                continue;
            }
            $doi->setStatus(Doi::ST_SEND);

            $ch = curl_init();

            if (false === $ch) {
                $output->writeln('<info>failed to initialize curl!</info>');
                break;
            }

            $link = 'https://doi.crossref.org/servlet/submissionDownload?usr=editorum&pwd=editorum2015&file_name=';
            curl_setopt($ch, CURLOPT_URL, $link.$doi->getFileName().'&type=result');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);

            if (false === $result) {
                $output->writeln('<info>'.curl_error($ch).'</info>');
                continue;
            } else {
                $xml = simplexml_load_string($result);
                if (isset($xml['status'])) {
                    switch (strtolower($xml['status'])) {
                        case 'success':
                            $doi->setStatus(Doi::ST_PROCESSED);
                            $count++;
                            break;
                        default:
                            break;
                    }
                }
            }

            curl_close($ch);
        }

        $odm->flush();
        $output->writeln('<info>Successfully update '.$count.' dois!</info>');
    }
}
