<?php

namespace Editorum\Bundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FindGeographyCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('asu:geography:find')
            ->addArgument('needle', InputArgument::REQUIRED, 'Search string')
            ->addOption('bundle', 'b', InputOption::VALUE_OPTIONAL, 'Bundle name', 'AsuBundle')
            ->addOption('document', 'd', InputOption::VALUE_OPTIONAL, 'Collection document', 'Country')
            ->setDescription('Find geography info')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $odm = $container->get('doctrine.odm.mongodb.document_manager');

        $needle = $input->getArgument('needle');
        $bundle = $input->getOption('bundle');
        $document = $input->getOption('document');

        $qb = $odm->getRepository($bundle . ':' . $document)->createQueryBuilder();
        $finded = $qb->field('ru.title')->equals(new \MongoRegex('/' . $needle . '/i'))->getQuery()->getSingleResult();

        if (null === $finded) {
            $output->writeln(
                sprintf('No items found this title "%s".', $needle)
            );

            exit;
        }

        $table = $this->getHelper('table');
        $table
            ->setRows(array(
                array('ID', $finded->getExternalId()),
                array('Title', $finded->getRu()['title']),
                array('Position', $finded->getPosition()),
                array('City', $finded->getCity()),
            ));

        $table->render($output);
    }
}
