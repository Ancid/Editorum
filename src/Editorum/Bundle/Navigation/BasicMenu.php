<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 19.12.2015
 * Time: 17:41
 */
namespace  Editorum\Bundle\Navigation;

use Exception;

class BasicMenu
{
    /**
     * Меню, которое мы строим
     * @var array
     */
    protected $menu = array();

    /**
     * Счетчик автоматически добавляемых элементов для генерации названий
     * @var int
     */
    private $element_numeration = 0;

    /**
     * @param $parent_node          Родительский узел. Если пункт на верхний уровень, то false
     * @param $system_name          Системное название узла. Служит для выделения текущего пункта меню
     *                              и для добавления потомков
     * @param $title                Название узла.
     * @param $url                  URL, куда осуществляется переход. Допустимы подстановки вида %id%,
     *                              заменяемые id из $id
     * @param bool $id
     * @param bool $active          Активен ли пункт в данном контексте исполнения системы
     * @param bool $class
     * @return $this
     * @throws Exception
     */
    public function add($parent_node, $system_name, $title, $url, $id = false, $active = true, $class = false)
    {
        $item = array(
            'title'         => $title,
            'url'           => str_replace('%id%', $id, $url),
            'active'        => $active,
            'class'         => $class
        );

        /**
         * Багфикс для шаблонов, просто проверяющих наличие атрибута class, сказать Андрею
         */
        if ($class === false) {
            unset($item['class']);
        }

        if ($parent_node === false) {
            if (isset($menu[$system_name])) {
                throw new Exception('Menu item with the same name "'.$system_name.'" already exists');
            }

            $this->menu[$system_name] = $item;
        } else {
            //@TODO Сейчас, для простоты, сделано только двухуровневое построение меню. Когда понадобится - перевести на рекурсивный обход
            if (!isset($menu[$parent_node])) {
                throw new Exception('Can not add menu item to non existing parent "'.$parent_node.'"');
            }

            if (isset($menu[$parent_node]['items'])) {
                if (isset($menu[$parent_node]['items'][$system_name])) {
                    throw new Exception(
                        'Menu subitem with the same name "'
                        . $system_name . '" already exists in section "' . $parent_node . '"'
                    );
                }
            } else {
                $menu[$parent_node]['items'] = array();
            }


            $menu[$parent_node]['items'][$system_name] = $item;
        }
        return $this;
    }

    /**
     * Добавить в меню разделитель
     * @param $parent_node  см выше
     * @return BasicMenu
     * @throws Exception
     */
    public function addDivider($parent_node)
    {
        return $this->add($parent_node, 'hr_'.$this->getFreeNumber(), '', '');
    }


    /**
     * Сгенерировать меню
     * @param bool $select_item
     * @return array
     */
    public function generate($select_item = false)
    {
        /**
         * @TODO Сразу предусматриваем возможность выделения пункта меню в массиве, для упрощения кода шаблона. Однако пока до актуальной необходимости не делаем
         */

        return $this->menu;
    }

    /**
     * Получить свободный номер для автоматически генерируемых элементов
     * @return int
     */
    protected function getFreeNumber()
    {
        $this->element_numeration++;
        return $this->element_numeration;
    }
}