<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 15.10.2015
 * Time: 14:09
 */
namespace Editorum\Bundle\FolderSystem;

class FolderFactory
{
    /**
     * копируем существующую папку
     *
     * @param $source_folder
     * @param $destination_folder
     * @return bool
     */
    public static function copyFolder($source_folder, $destination_folder)
    {
        return true;
    }


    /**
     * Создаем папку с результатами поиска
     *
     * @param $source_folder
     * @param $destination_folder
     * @param $query
     * @return Folder
     */
    public static function queryNewFolder($source_folder, $destination_folder, $query)
    {
        $folder = new Folder();
        return $folder;
    }
}