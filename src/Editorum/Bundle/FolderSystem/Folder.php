<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 15.10.2015
 * Time: 14:10
 */
namespace Editorum\Bundle\FolderSystem;

use Editorum\Bundle\Logic\Model;
use Editorum\Bundle\Logic\MongoNode;
use Editorum\Bundle\Logic\Publication;
use MongoCode;


class Folder extends MongoNode
{

    public function createEmpty()
    {
        $obj = array(
            'name'                          => '',
            'user_id'                       => '',
            'fname_uid'                     => '',
            'elements_collection'           => 'pubs',
            'items'                         => array(),
            'selected'                      => array(),
            'parent'                        => null,
            'system'                        => 0,
        );

        return $obj;
    }


    /**
     * Получить содержимое папки
     * @param $folder_id
     * @param $user_id
     * @param $user_id
     * @return array
     */
    public static function getContent($folder_id, $user_id)
    {
        $db = self::getDB();
        $response = $db->execute(
            new MongoCode('return getFolderContent(fid, uid)', array(
                'fid'           => $folder_id,
                'uid'           => $user_id,
            ))
        );
        /**
         * @TODO Create correct class instantiation from database class_name. Not constant "Publication" class
         */
        if (!is_null($response['retval']['class_name'])) {
            $class_name = 'Editorum\Bundle\Logic\\'.ucfirst($response['retval']['class_name']);
            $obj = new $class_name();
        } else {
            $obj = new Publication();
        }

        if (count($response['retval']['dataset']) > 0)
            $obj->setDataset($response['retval']['dataset']);
        $result = [
            'selected'      => $response['retval']['selected'],
            'obj'           => $obj,
            'name'          => $response['retval']['name'],
            'collection'    => $response['retval']['class_name']
        ];

        return $result;
    }


    /**
     * Возвращает все папки пользователя
     * @param $user_id
     * @return mixed
     */
    public static function getUserFolders($user_id)
    {
        $folders = Model::getByKeys(new Folder(), array('user_id' => $user_id), true, 1);
        return $folders;
    }


    /**
     * Возвращает пользовательские папки (system = 0)
     * @param $user_id
     * @return mixed
     */
    public static function getOwnUserFolders($user_id)
    {
        $folders = Model::getByKeys(new Folder(), array('user_id' => $user_id, 'system' => 0), true, 1);
        return $folders;
    }


    /**
     * Создать папку с результатами
     * @param string $fname
     * @param int $user_id
     * @param string $collection
     * @param array $items
     * @param null $parent
     * @param int $system
     * @return int
     */
    public static function queryNewFolder($fname, $user_id, $collection, $items, $parent = null, $system = 0)
    {
        $db = self::getDB();

        $response = $db->execute(
            new MongoCode('return queryNewFolder(fname, uid, collection, items, parent, system)', array(
                'fname'             => $fname,
                'uid'               => (int)$user_id,
                'collection'        => $collection,
                'items'             => $items,
                'parent'            => $parent,
                'system'            => $system,
            ))
        );
//        var_dump($db->lastError());
//        var_dump($response);
        return $response['retval'];
    }


    /**
     * Удаляет произведения из папки
     * @param $folder_id
     * @param $user_id
     * @return array
     */
    public static function removeFromFolder($folder_id, $user_id)
    {
        $response = [
            'success'   => true,
            'error'     => false,
            'msg'       => false
        ];
        $db = self::getDB();

        if (!Folder::isSystem($folder_id, $user_id)) {
            $selected = self::getSelected($folder_id, $user_id);
            if (!is_null($selected)) {
                $new_data = array(
                    '$pullAll' => array(
                        'items'         => $selected,
                        'selected'      => $selected,
                    )
                );
                $db->folder->update(array('_id' => $folder_id, 'user_id' => $user_id), $new_data);
            }
        } else {
            $response['error'] = 'Нельзя удалить из системной папки.';
        }

        return $response;
    }


    /**
     * Возвращает выбранные элементы
     * @param $folder_id
     * @param $user_id
     * @return array|null
     */
    public static function getSelected($folder_id, $user_id)
    {
        $db = self::getDB();
        $selected = $db->folder->findOne(array('_id' => $folder_id, 'user_id' => $user_id), array('selected'));
        if (!is_null($selected))
            return $selected['selected'];
        else
            return $selected;
    }


    /**
     * Проверяет системная ли папки
     * @param $folder_id
     * @param $user_id
     * @return bool
     */
    public static function isSystem($folder_id, $user_id)
    {
        $db = self::getDB();
        $system = $db->folder->findOne(array('_id' => $folder_id, 'user_id' => $user_id), array('system'));
        if (!is_null($system) && $system['system'] == 1)
            return true;
        else
            return false;
    }


    /**
     * Проверяет наличие папки
     * @param array $query
     * @param null $user_id
     * @return array|null
     */
    public static function getCachedFolder($query, $user_id = null)
    {
        $db = self::getDB();
        $folder = $db->folder->findOne($query);
        return $folder;
    }


    /**
     * Добавляем папку поиска
     * @param $search_txt
     * @param $user_id
     * @param $collection
     * @param $items
     * @return int|null
     */
    public static function addSearchFolder($search_txt, $user_id, $collection, $items)
    {
        $id = null;
        $limit = 10; //Лимит папок поиска
        $db = self::getDB();
        $exist = self::getCachedFolder(array(
            'name'          => $search_txt,
            'user_id'       => $user_id,
            'collection'    => $collection,
        ));
        if (is_null($exist)) {
            $parent_folder = $db->folder->findOne(array(
                'name'          => 'Папка поиска',
                'system'        => 1,
                'user_id'       => $user_id
            ));
            //Если нет поисковой папки, создаем
            if (is_null($parent_folder)) {
                self::createStartFolders($user_id);
                $parent_folder = $db->folder->findOne(array(
                    'name'          => 'Папка поиска',
                    'system'        => 1,
                    'user_id'       => $user_id
                ));
            }

            $search_folders = $db->folder->find(array(
                'user_id'           => $user_id,
                'parent'            => $parent_folder['_id']
            ));
            //Проверяем колчиество папок в mongo
            //Если 10 папок, удаляем 1
            if ($search_folders->count() > $limit-1) {
                $db->folder->findAndModify(
                    array(
                        'user_id'           => $user_id,
                        'parent'            => $parent_folder['_id']
                    ),
                    null,
                    null,
                    array(
                        'sort'      => array('_id' => 1),
                        'remove'    => true
                    )
                );
            }
            //Добавляем папку с результатом
            $id = self::queryNewFolder($search_txt, $user_id, $collection, $items, $parent_folder['_id'], 1);
        } else {
            $id = $exist['_id'];
        }
        return $id;
    }


    /**
     * Добавлят системные папки, если нету
     * @param $user_id
     */
    public static function createStartFolders($user_id)
    {
        $search = self::getCachedFolder(array(
            'fname_uid'          => 'Папка поиска_'.$user_id,
        ));
        if (is_null($search))
            self::queryNewFolder('Папка поиска', $user_id, null, array(), null, 1);

        $my_pubs = self::getCachedFolder(array(
            'fname_uid'          => 'Произведения_'.$user_id,
        ));
        if (is_null($my_pubs))
            self::queryNewFolder('Произведения', $user_id, null, array(), null, 1);

        $my_pubs = self::getCachedFolder(array(
            'fname_uid'          => 'Моя папка_'.$user_id,
        ));
        if (is_null($my_pubs))
            self::queryNewFolder('Моя папка', $user_id, null, array(), null, 0);
    }


    public static function getFolderCollection($folder_id)
    {
        $db = self::getDB();
        $collection = $db->folder->findOne(array('_id' => (int)$folder_id), array('collection'));
        if (!is_null($collection))
            return $collection['collection'];
        else
            return $collection;
    }


    /**
     * Копирует выделенные элементы в папку
     * @param $src_id
     * @param $dst_id
     * @param $user_id
     * @return array
     */
    public static function copyFromFolder($src_id, $dst_id, $user_id)
    {
        $response = [
            'success'   => true,
            'error'     => false,
            'msg'     => false

        ];
        $db = self::getDB();

        if (!self::isSystem($dst_id, $user_id)) {
            if (Folder::hasChildren($dst_id) == false) {
                $dst_coll = self::getFolderCollection($dst_id);
                $src_coll = Folder::getFolderCollection($src_id);
                $selected = Folder::getSelected($src_id, $user_id);

                if ($dst_coll == $src_coll || is_null($dst_coll)) {
                    $new_data = array(
                        '$set'      => array(
                            'collection' => $src_coll
                        ),
                        '$addToSet' => array(
                            'items' => array(
                                '$each' => $selected
                            )
                        )
                    );
                    $db->folder->update(array('_id' => $dst_id, 'user_id' => $user_id), $new_data);
                    $response['msg'] = 'Выбранные элементы скопированы.';
                }
            } else {
                $response['error'] = 'В выбранной папке есть папки!';
            }
        } else {
            $response['error'] = 'Нельзя скопировать в системную папку!';
        }

        return $response;
    }


    /**
     * Перемещает выделенные элементы в папку
     * @param $src_id
     * @param $dst_id
     * @param $user_id
     * @return array
     */
    public static function moveFromFolder($src_id, $dst_id, $user_id)
    {
        $response = self::copyFromFolder($src_id, $dst_id, $user_id);

        if ($response['msg'] != false && $response['error'] == false) {
            $response = self::removeFromFolder($src_id, $user_id);
            if ($response['error'] == false)
                $response['msg'] = 'Выбранные элементы перенесены';
            else
                $response['error'] = 'Нельзя переместить из системной папки! Содержимое будет скопировано.';
        }

        return $response;
    }


    /**
     * Выделяет содержимое папки
     * @param $dst_id
     * @param $selected
     * @return bool
     */
    public static function updateSelect($dst_id, $selected)
    {
        $db = self::getDB();
        $new_data = array(
            '$set' => array(
                'selected' => $selected
            )
        );
        $db->folder->update(array('_id' => $dst_id), $new_data);

        return true;
    }


    /**
     * Ассоциативное дерево папок из MongoCursor
     * @param $cursor
     * @return array
     */
    public static function buildTreeFromCursor($cursor)
    {
        $links = [];
        $tree = [];
        foreach ($cursor as $item) {
            $elem = $item->getCurrentRecord();
            unset($elem['user_id']);
            unset($elem['fname_uid']);
            unset($elem['elements_collection']);
            unset($elem['items']);
            if(array_key_exists('parent', $elem) && $elem['parent'] === null ) {
                $tree[$elem['_id']] = $elem;
                $links[$elem['_id']] = &$tree[$elem['_id']];
            } else {
                $links[(int)$elem['parent']]['childrens'][$elem['_id']] = $elem;
                $links[$elem['_id']] = &$links[(int)$elem['parent']]['childrens'][$elem['_id']];
            }
        }
        //Пробегаем по дочерним папкам и проверяем все ли вошли в дерево
        foreach ($links as $node) {
            if ($node['parent'] !== NULL) {
                self::insertInTree($tree, $node);
            }
        }

        return $tree;
    }

    public static function insertInTree($array, $node)
    {
        foreach ($array as &$item) {
            if ($item['_id'] == $node['parent']) {
                if (!isset($item['childrens'][$node['_id']])) {
                    $item['childrens'][$node['_id']] = $node;
                    return true;
                }
            }elseif (isset($item['childrens'])) {
                $res = self::insertInTree($item['childrens'], $node);
                if ($res !== false)
                    return true;
            }
        }

        return false;
    }


    /**
     * Удаляет папку по ID
     * @param int $id
     * @return array|bool
     */
	public static function removeFolder($id)
	{
		$db = self::getDB();
		

        $response = [
            'success'       => true,
            'error'         => false
        ];

        if (Folder::hasParent($id) == false) {
            $response['error'] = 'Нельзя удалить корневую папку';
        } else {
            if(Folder::hasParent($id)) {
                Folder::removeTree($id);
            } else {
                $db->selectCollection('folder')->remove(array('_id' => $id));
            }
        }
            
		return $response;
	}


    /**
     * Проверка папки на родительство
     * @param $id
     * @return bool
     */
	public static function hasChildren($id)
	{
		$db = self::getDB();
		$result = $db->selectCollection('folder')->find(array('parent' => $id));
		if($result->count() > 0) {
			return true;
		} else {
			return false;
		}
	}


    /**
     * Проверяет корневая ли папка
     * @param $id
     * @return bool
     */
    public static function hasParent($id)
    {
        $db = self::getDB();
        $res = $db->selectCollection('folder')->findOne(array('_id' => $id), array('parent' => 1));
        if (!is_null($res['parent']))
            return true;
        else 
            return false;
    }


	/*
	* Удаление вложенного дерева папок
	*/
	public static function removeTree($id)
	{
		$db = self::getDB();
		if(self::hasChildren($id)){
			$cursor = $db->selectCollection('folder')->find(array('parent'=> $id));
			foreach ($cursor as $res)
			{
				if (self::hasChildren($res['_id'])) {
					self::removeTree($res['_id']);
				} else {
					$db->selectCollection('folder')->remove(array('_id' => (int)$res['_id']));
				}
			}
			self::removeTree($id);
		} else {
			$db->selectCollection('folder')->remove(array('_id' => $id));
		}
	}


    /**
     * Проверка на контент
     * @param $id
     * @return bool
     */
	public static function countItems($id)
	{
		$db = self::getDB();
		$res = $db->selectCollection('folder')->findOne(array('_id' => $id), array('items' => 1));

        return count($res['items']);
	}


	/**
	 *Переименовать
	 */
	public static function changeFolder($id,$name)
	{
		$db = self::getDB();
		$res = $db->selectCollection('folder')->update(array('_id' => (int)$id),array('$set'=>array('name'=> $name)));

		return $res;
	}



    /**
     * Добавиление хранимых функций в MongoDB
     * Запустить 1 раз
     */
    public static function addFunctions()
    {
        $db = self::getDB();

        //Получение содержимого папки
        $db->system->js->save(array("_id"=>"getFolderContent",
            "value"=>new MongoCode('
function (fid, uid) {
    var folder = db.folder.findOne({
        "_id":          NumberInt(fid),
        "user_id":      NumberInt(uid)
    });

    var query_array = new Array();
    var dataset = new Array();
    if (folder.items.length > 0) {
        for (var i in folder.items) {
            query_array.push({
                "_id": folder.items[i]
            });
        }
        if (folder.collection == \'publication\')
            var fieldset = {\'ru\': 1};
        else
            var fieldset = {
                \'title\': 1,
                \'annotation\': 1
            }
        var cursor = db.getCollection(folder.collection).find({
            $or: query_array
        }, fieldset);
        dataset = cursor.toArray();
    }
    var result = {
        dataset:                dataset,
        class_name:             folder.collection,
        selected:               folder.selected,
        name:                   folder.name
    };

    return result;
}')
        ));

        //Создание папки запросом
        $db->system->js->save(array("_id"=>"queryNewFolder",
            "value"=>new MongoCode('
function (fname, uid, collection, items, parent, system) {
    var seq = db.sequences.findOne({"_id":"folder"});
    if (seq == null) {
        db.sequences.insert(
            {"_id":"folder", "seq": NumberInt(0) }
        );
        seq = db.sequences.findOne({"_id":"folder"});
    }

    var new_id = seq.seq+1;
    db.folder.insert(
        {
            _id:                NumberInt(new_id),
            name:               fname,
            user_id:            uid,
            collection:         collection,
            fname_uid:          fname + "_" + uid,
            items:              items,
            selected:           [],
            parent:             parent,
            system:             NumberInt(system)
        }
    );
    db.sequences.update(
        {"_id":"folder"},
        { $set: { "seq": NumberInt(new_id) } },
        { upsert: true }
    );

    return new_id;
}s')
                ));
    }

}