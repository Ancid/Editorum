<?php
/**
 * Created by PhpStorm.
 * User: Xor
 * Date: 05.11.2015
 * Time: 22:25
 *
 * Сервис для возврата данных о пользователе
 */
namespace Editorum\Bundle\Services;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage as TokenStorage;

class UserData
{
    protected $user = null;

    /**
     * UserData constructor.
     * @param \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage $security
     */
    public function __construct(TokenStorage $security)
    {
        if ($security->getToken() && is_object($security->getToken()->getUser()))            
            $this->user = $security->getToken()->getUser();
    }

    /**
     * Залогинен ли пользователь
     * @return bool
     */
    public function isLogged()
    {
        if (is_null($this->user))
            return false;
        return true;
    }

    /**
     * Получить ФИО пользователя для меню
     * @return string
     */
    public function getName()
    {
        if (!is_null($this->user)) {
            if (is_object($this->user))
                return $this->user->getUsername();
            else
                return $this->user;
        }
        return 'anon_nulll_user';
    }
}
