<?php
namespace Editorum\Bundle\Services;

use Editorum\Bundle\Services\Menu\MenuInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class Navigation
{
    /** @var MenuBuilder */
    private $builder;
    
    /** @var Router */
    private $router;
    
    /** @var AuthorizationCheckerInterface */
    private $authorizationChecker;

    /** @var array */
    private $menu = [];

    /**
     * Navigation constructor.
     *
     * @param MenuBuilder $builder
     * @param Router $router
     * @param AuthorizationCheckerInterface $authorizationChecker
     */
    public function __construct(
        MenuBuilder $builder,
        Router $router,
        AuthorizationCheckerInterface $authorizationChecker
    ) {
        $this->builder = $builder;
        $this->router = $router;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param $name
     *
     * @return MenuInterface
     * @throws \Exception
     */
    public function get($name)
    {
        if (!isset($this->menu[$name])) {
            $className = '\\Editorum\\Bundle\\Services\\Menu\\' . $name . 'Menu';

            if (!class_exists($className)) {
                throw new \Exception('Menu for ' . $name . ' does not exists!');
            }

            $class = new $className($this->builder, $this->router, $this->authorizationChecker);
            if (!$class instanceof MenuInterface) {
                throw new \Exception('MenuBuilder must implement BuilderInterface!');
            }

            $this->menu[$name] = $class;
        }


        return $this->menu[$name];
    }
}
