<?php
namespace Editorum\Bundle\Services\Menu;

use Editorum\Bundle\Services\MenuBuilder;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

abstract class AbstractMenu implements MenuInterface
{
    /** @var MenuBuilder */
    protected $builder;

    /** @var Router */
    protected $router;

    /** @var AuthorizationCheckerInterface */
    protected $authorizationChecker;

    /** @var array */
    protected $menu = [];

    /**
     * Navigation constructor.
     *
     * @param MenuBuilder                   $builder
     * @param Router                        $router
     * @param AuthorizationCheckerInterface $authorizationChecker
     */
    public function __construct(
        MenuBuilder $builder,
        Router $router,
        AuthorizationCheckerInterface $authorizationChecker
    ) {
        $this->builder = $builder;
        $this->router = $router;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param string     $role
     * @param mixed|null $object
     *
     * @return bool
     */
    protected function isGranted($role, $object = null)
    {
        return $this->authorizationChecker->isGranted($role, $object);
    }

    /**
     * @param string $route_name
     * @param array  $parameters
     *
     * @param bool   $referenceType
     *
     * @return string
     */
    protected function generateUrlByRouteName($route_name, $parameters = [], $referenceType = Router::ABSOLUTE_PATH)
    {
        return $this->router->generate($route_name, $parameters, $referenceType);
    }

    /**
     * @return MenuBuilder
     */
    protected function getBuilder()
    {
        return $this->builder;
    }

    /**
     * @return string
     */
    protected function getClassName()
    {
        return get_class($this);
    }

    /**
     * @param       $name
     * @param array $arguments
     *
     * @return mixed
     * @throws \Exception
     */
    public function getMenu($name, array $arguments = [])
    {
        $methodName = $name . 'Menu';
        if (!method_exists($this, $methodName)) {
            throw new \Exception('Menu "'.$name.'" in '. static::getClassName() .' does not exists');
        }

        return call_user_func_array([$this, $methodName], $arguments);
    }
}
