<?php
namespace Editorum\Bundle\Services\Menu;

interface MenuInterface
{
    public function getMenu($name, array $arguments = []);
}
