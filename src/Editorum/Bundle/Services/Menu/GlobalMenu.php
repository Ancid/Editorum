<?php
namespace Editorum\Bundle\Services\Menu;

use Editorum\Bundle\Services\MenuBuilder\GlobalMenuBuilder;

class GlobalMenu extends AbstractMenu
{
    const INT_READ = 'interface_read';
    const INT_SELFPUBLISHING = 'interface_selfpublishing';
    const INT_PUBLISHER = 'interface_publisher';
    const INT_EDITORUM = 'interface_editorum';

    protected $interface = null;

    /**
     * Установить текущий интерфейс сайта
     *
     * @param string $interface_mode
     */
    public function setInterface($interface_mode)
    {
        $this->interface = $interface_mode;
    }

    /**
     * Вернуть имя маршрута из routing.yml главной страницы данного интерфейса
     */
    public function getInterfaceHomepageUrl()
    {
        switch ($this->interface) {
            case self::INT_EDITORUM:
                return 'editorum_main';
                break;
            case self::INT_READ:
                return 'journals_index';
                break;
            case self::INT_PUBLISHER:
                return 'homepage';
                break;
            case self::INT_SELFPUBLISHING:
                return false;
                break;
        }

        return false;
    }

    /**
     * Отображение главного меню
     */
    public function mainMenu()
    {
        switch ($this->interface) {
            case self::INT_READ:
                return $this->getMenu('read');
                break;
            case self::INT_SELFPUBLISHING:
                return $this->getMenu('selfpublishing');
                break;
            case self::INT_PUBLISHER:
                return $this->getMenu('publisher');
                break;
            case self::INT_EDITORUM:
                return $this->getMenu('editorum');
                break;
        }

        return $this->getMenu('editorum');
    }

    /**
     * Главное меню сайта в режиме чтения
     */
    public function readMenu()
    {
        /** @var GlobalMenuBuilder $builder */
        $builder = $this->getBuilder()->get('Global');

        $builder->addRootItem('periodical', 'Периодика')
            ->addItem(
                'alphabet',
                'По алфавиту',
                $this->generateUrlByRouteName('journals_alphabet')
            )
            ->addItem(
                'okso',
                'По ОКСО',
                $this->generateUrlByRouteName('classif_list', ['type' => 'journal', 'classif_type' => 'okso_journal'])
            )
            ->addItem(
                'grnti',
                'По ГРНТИ',
                $this->generateUrlByRouteName('classif_list', ['type' => 'journal', 'classif_type' => 'grnti'])
            )
            ->addItem(
                'serie',
                'По сериям',
                $this->generateUrlByRouteName('journals_series')
            )
            ->addRootItem('nonperiodical', 'Непериодика')
            ->addItem(
                'okso',
                'По ОКСО',
                $this->generateUrlByRouteName(
                    'classif_list',
                    ['type' => 'publication', 'classif_type' => 'okso_journal']
                )
            )
            ->addItem(
                'grnti',
                'По ГРНТИ',
                $this->generateUrlByRouteName(
                    'classif_list',
                    ['type' => 'publication', 'classif_type' => 'grnti']
                )
            )
            ->addItem(
                'bbk',
                'По ББК',
                $this->generateUrlByRouteName(
                    'classif_list',
                    ['type' => 'publication', 'classif_type' => 'bbk']
                )
            )
            ->addRootItem('ebs', 'Энциклопедия ЭБС')
            ->addItem(
                'rus',
                'Новая Российская Энциклопедия',
                $this->generateUrlByRouteName('encyclopedia', [ 'type' => 'rus' ])
            )
            ->addItem(
                'urist',
                'Юридическая Энциклопедия',
                $this->generateUrlByRouteName('encyclopedia', ['type' => 'urist'])
            )
            ->addRootItem(
                'events',
                'События',
                '#'
            )
            ->addRootItem(
                'info',
                'Информация',
                '#'
            )
        ;
        if ($this->isGranted('ROLE_USER')) {
            $builder
                ->addRootItem(
                    'folders',
                    'Папки',
                    $this->generateUrlByRouteName('folders')
                )
                ->addRootItem(
                    'request',
                    'Заявка',
                    $this->generateUrlByRouteName('sylius_flow_start', [ 'scenarioAlias' => 'request' ])
                )
            ;
        }

        return $builder->getMenu();
    }


    /**
     * Главное меню сайта в режиме самопубликации
     */
    public function selfpublishingMenu()
    {
        $options = [
            'section1' => [
                'title' => 'Секция 1',
                'items' => [
                    'test1' => [
                        'title' => 'Тест1',
                        'url'   => '/test/'
                    ]
                ]
            ],
            'section2' => [
                'title' => 'Пункт меню',
                'url'   => '/test/'
            ]
        ];

        return $options;
    }

    /**
     * Главное меню в режиме издательства
     *
     * @return array
     */

    public function publisherMenu()
    {
        /** @var GlobalMenuBuilder $builder */
        $builder = $this->getBuilder()->get('Global');

        $builder
            ->addRootItem('content', 'Контент');

        #-------модуль журналов------#
        if ($this->authorizationChecker->isGranted('ROLE_JOURNAL_GROUP')) {
            $builder->addItem(
                'journals_list',
                'Модуль журналов',
                'empty',
                ['class' => 'list_elements_parent']
            )->addItem(
                'journals',
                'Журналы',
                $this->generateUrlByRouteName('journal_list'),
                ['class' => 'list_elements_child']
            );
            if ($this->authorizationChecker->isGranted('ROLE_ISSUE_GROUP')) {
                $builder->addItem(
                    'issues',
                    'Номера журналов',
                    $this->generateUrlByRouteName('issue_all_list'),
                    ['class' => 'list_elements_child']
                );
            }
            if ($this->authorizationChecker->isGranted('ROLE_ARTICLE_GROUP')) {
                $builder->addItem(
                    'articles',
                    'Статьи',
                    $this->generateUrlByRouteName('article_list'),
                    ['class' => 'list_elements_child']
                );
            }
        }

        #------модуль конференций----#
        if ($this->authorizationChecker->isGranted('ROLE_CONFERENCE_GROUP')) {
            $builder->addItem(
                'conf',
                'Модуль конференции',
                'empty',
                ['class' => 'list_elements_parent']
            )
                ->addItem(
                    'conferences',
                    'Конференции',
                    $this->generateUrlByRouteName('conference_list'),
                    ['class' => 'list_elements_child']
                );

            if ($this->authorizationChecker->isGranted('ROLE_COLLECTION_GROUP')) {
                $builder->addItem(
                    'collections',
                    'Сборники',
                    $this->generateUrlByRouteName('collection_list'),
                    ['class' => 'list_elements_child']
                );
            }

            if ($this->authorizationChecker->isGranted('ROLE_CONFERENCE_ARTICLE_GROUP')) {
                $builder->addItem(
                    'conference_articles',
                    'Статьи для конференций',
                    $this->generateUrlByRouteName('conferencearticle_list'),
                    ['class' => 'list_elements_child']
                );
            }
        }

        #-------модуль книг------#
        $builder->addItem(
            'books',
            'Модуль книги',
            'empty',
            ['class' => 'list_elements_parent']
        );
        if ($this->authorizationChecker->isGranted('ROLE_MONOGRAPHY_GROUP')) {
            $builder->addItem(
                'monography',
                'Монографии',
                $this->generateUrlByRouteName('monography_list'),
                ['class' => 'list_elements_child']
            );
        }
        $builder ->addItem(
            'textbook',
            'Учебные пособия',
            $this->generateUrlByRouteName('textbook_list'),
            ['class' => 'list_elements_child']
        );

        #------------модуль авторов--------#
        if ($this->authorizationChecker->isGranted('ROLE_AUTHOR_GROUP')) {
            $builder
                ->addItem(
                    'authors_list',
                    'Модуль авторов',
                    'empty',
                    ['class' => 'list_elements_parent']
                )
                ->addItem(
                    'authors',
                    'Авторы',
                    $this->generateUrlByRouteName('authors_list'),
                    ['class' => 'list_elements_child']
                );
        }


//        if ($this->authorizationChecker->isGranted('ROLE_TEXTBOOK_GROUP')) {
//            $builder->addItem(
//                'textbook',
//                'Учебники',
//                $this->generateUrlByRouteName('textbook_list'),
//                ['class' => 'list_elements_parent']
//            );
//        }
//
//        if ($this->authorizationChecker->isGranted('ROLE_JOURNAL_GROUP')) {
//            $builder->addItem(
//               'journals_list',
//               'Модуль журналов',
//               'empty',
//               ['class' => 'list_elements_parent']
//           )
//           ->addItem(
//               'journals',
//               'Журналы',
//               $this->generateUrlByRouteName('journal_list'),
//               ['class' => 'list_elements_child']
//            );
//        }
//        if ($this->authorizationChecker->isGranted('ROLE_ISSUE_GROUP')) {
//            $builder->addItem(
//                'issues',
//                'Номера журналов',
//                $this->generateUrlByRouteName('issue_all_list'),
//                ['class' => 'list_elements_child']
//            );
//        }
//


//        $builder
//            ->addRootItem('content', 'Издательство')
//            ->addItem(
//                'pubs',
//                'Заявки',
//                'empty',
//                ['class' => 'list_elements_parent']
//            )
//            ->addItem(
//                'all_pubs',
//                'Все',
//                'empty',
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'new_pubs',
//                'Новые',
//                'empty',
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'true_pubs',
//                'Согласовано',
//                'empty',
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'false_pubs',
//                'Отклонено',
//                'empty',
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'journals_list',
//                'Журналы',
//                'empty',
//                ['class' => 'list_elements_parent']
//            )
//            ->addItem(
//                'journals',
//                'Журналы',
//                $this->generateUrlByRouteName('journal_list'),
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'issues',
//                'Номера',
//                $this->generateUrlByRouteName('issue_all_list'),
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'series',
//                'Серии',
//                $this->generateUrlByRouteName('serie_all_list'),
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'articles',
//                'Статьи',
//                $this->generateUrlByRouteName('article_list'),
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'journal matter',
//                'Журнальные материалы',
//                $this->generateUrlByRouteName('publication_list', ['kind' => 'journal_matter']),
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'conf',
//                'Конференции',
//                'empty',
//                ['class' => 'list_elements_parent']
//            )
//            ->addItem(
//                'conference',
//                'Конференции',
//                $this->generateUrlByRouteName('conferences'),
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'collections',
//                'Сборники',
//                $this->generateUrlByRouteName('collections'),
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'conference_articles',
//                'Статьи для конференций',
//                $this->generateUrlByRouteName('conferencearticle_list'),
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'books',
//                'Книги',
//                'empty',
//                ['class' => 'list_elements_parent']
//            )
//            ->addItem(
//                'monography',
//                'Монографии',
//                $this->generateUrlByRouteName('monography_list'),
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'textbook',
//                'Учебные пособия',
//                $this->generateUrlByRouteName('textbook_list'),
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'vkr',
//                'ВКР',
//                'empty',
//                ['class' => 'list_elements_parent']
//            )
//            ->addItem(
//                'download_vkr',
//                'Загрузить ВКР',
//                'empty',
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'upr_vkr',
//                'Управление загруженными работами',
//                'empty',
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'vkr_to_user',
//                'Привязать ВКР к пользователю',
//                'empty',
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'encik',
//                'Энциклопедия',
//                'empty',
//                ['class' => 'list_elements_parent']
//            )
//            ->addItem(
//                'ebs_encik',
//                'ЭБС Энциклопедии',
//                'empty',
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'article_to_encik',
//                'Статьи в Энциклопедию',
//                'empty',
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'authors_list',
//                'Авторы',
//                'empty',
//                ['class' => 'list_elements_parent']
//            )
//            ->addItem(
//                'authors',
//                'Авторы',
//                $this->generateUrlByRouteName('authors_list'),
//                ['class' => 'list_elements_child']
//            )
//
//
//            ->addRootItem('settings', 'Настройки')
//            ->addItem(
//                'adminportal',
//                'Администрирование портала',
//                'empty',
//                ['class' => 'list_elements_parent']
//            )
//            ->addItem(
//                'organizations',
//                'Управление',
//                $this->generateUrlByRouteName('organization_list'),
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'role_list',
//                'Привелегии ',
//                $this->generateUrlByRouteName('role_list'),
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'logs',
//                'Логи запросов к CrossRef(DOI) и РИНЦ',
//                $this->generateUrlByRouteName('logs'),
//                ['class' => 'list_elements_child']
//            )
//            ->addItem(
//                'admin_moduls',
//                'Добавление модулей',
//                'empty',
//                ['class' => 'list_elements_parent']
//            )
//            ->addItem(
//                'admin_finans',
//                'Управление финансами',
//                'empty',
//                ['class' => 'list_elements_parent']
//            )
//            ->addItem(
//                'admin_users',
//                'Управление пользователями',
//                'empty',
//                ['class' => 'list_elements_parent']
//            )
//            ->addItem(
//                'admin_users_role',
//                'Назначение пользователей на роли',
//                'empty',
//                ['class' => 'list_elements_parent']
//            )
//            ->addItem(
//                'admin_statements',
//                'Управление отчетностью',
//                'empty',
//                ['class' => 'list_elements_parent']
//            );

//            ->addItem(
//                'journal matter',
//                'журнальный материал',
//                $this->generateUrlByRouteName('publication_list', ['kind' => 'journal_matter'])
//            )
//            ->addItem('divider', '', 'empty', ['class' => 'divider'])

//            ->addItem(
//                'series',
//                'Серии журналов',
//                $this->generateUrlByRouteName('serie_all_list')
//            )


        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $builder
                ->addRootItem('config', 'Управление')
                ->addItem(
                    'adminportal',
                    'Администрирование портала',
                    'empty',
                    ['class' => 'list_elements_parent']
                )
                ->addItem(
                    'organizations',
                    'Организации',
                    $this->generateUrlByRouteName('organization_list'),
                    ['class' => 'list_elements_child']
                )
                ->addItem(
                    'roles',
                    'Привилегии',
                    $this->generateUrlByRouteName('role_list'),
                    ['class' => 'list_elements_child']
                )
                ->addItem(
                    'doi',
                    'Управление DOI',
                    $this->generateUrlByRouteName('doi'),
                    ['class' => 'list_elements_child']
                )
                ->addItem(
                    'logs',
                    'Логи запросов к CrossRef(DOI) и РИНЦ',
                    $this->generateUrlByRouteName('logs'),
                    ['class' => 'list_elements_child']
                )
                ->addItem(
                    'admin_moduls',
                    'Добавление модулей',
                    'empty',
                    ['class' => 'list_elements_parent']
                )
                ->addItem(
                    'admin_finans',
                    'Управление финансами',
                    'empty',
                    ['class' => 'list_elements_parent']
                )
                ->addItem(
                    'admin_users',
                    'Управление пользователями',
                    'empty',
                    ['class' => 'list_elements_parent']
                )
                ->addItem(
                    'admin_users_role',
                    'Назначение пользователей на роли',
                    'empty',
                    ['class' => 'list_elements_parent']
                )
                ->addItem(
                    'admin_statements',
                    'Управление отчетностью',
                    'empty',
                    ['class' => 'list_elements_parent']
                );

        }

        if ($this->authorizationChecker->isGranted('ROLE_REQUEST')) {
            $builder->addRootItem('request', 'Заявки')->addItem(
                'conference',
                'На конференции',
                $this->generateUrlByRouteName('conference_request_index')
            );
        }
//        $options = [
//                    'company' => array(
//                        'title' => 'Сведения о компании',
//                        'url' => '/company/'
//                    ),
//                    'templates' => array(
//                        'title' => 'Шаблоны договоров',
//                        'url' => '/agreement/templates/'
//                    ),
//        ];

        return $builder->getMenu();
    }

    /**
     * Главное меню в режиме администрирования
     */
    public function editorumMenu()
    {
        $options = [
            'clients' => [
                'title' => 'Управление системой',
                'items' => [
                    'organizations' => [
                        'title' => 'Организации',
                        'url'   => $this->generateUrlByRouteName('organization_list')
                    ],
                    'users'         => [
                        'title' => 'Пользователи',
                        'url'   => $this->generateUrlByRouteName('employee_index')
                    ]
                ]
            ],
            'dicts'   => [
                'title' => 'Справочники',
                'items' => [
                    'educational' => [
                        'title' => 'Учебные заведения',
                        'url'   => '/editorum/dict/educational/' // FIX: нет роута
                    ]
                ]
            ]
        ];

        return $options;
    }

    /**
     * Возвращает меню пользователя в залогиненном состоянии
     */
    public function userMenu()
    {
//        $tn = new TaskNotification();
//        $task_notification = $tn->getAmount();

        //@TODO Выводить динамически меню:
        // 1. Сообщения - только зарегистрированным пользователям
        // 2. Управление издательством и вложенный в него список издательств в зависимости от наличия у пользователя
        // договоров, унаследованных от EmployeeAgreement
        // 3. Доступ к Editorum - по наличию договора с системной организацией Editorum

        $options = [
//            'messages' => array(
//                'title' => 'Мои сообщения (Непрочитанных: ' . $task_notification . ')',
//                'url' => '/tasks/'
//            ),
//            'hr_1' => array(
//                'title' => '',
//                'url' => ''
//            ),
//            'browse' => array(
//                'title' => 'База произведений',
//                'url' => '/nauka/'
//            ),
//            'selfpublishing' => array(
//                'title' => 'Мои произведения',
//                'url' => '/selfpublishing/'
//            ),
//            'asu' => array(
//                'title' => 'Управление издательством',
//                'url' => '/pubs/all/'
//            ),
//            'admin' => array(
//                'title' => 'Администрирование Editorum',
//                'url' => '/editorum/'
//            ),
//            'hr_2' => array(
//                'title' => '',
//                'url' => ''
//            ),
            'profile' => [
                'title' => 'Профиль',
                'url'   => $this->generateUrlByRouteName('user_profile')
            ],
            'logout'  => [
                'title' => 'Выход',
                'url'   => $this->generateUrlByRouteName('logout')
            ],
        ];

        return $options;
    }
}
