<?php
namespace Editorum\Bundle\Services\Menu;

use Editorum\Bundle\Document\Article;
use Editorum\Bundle\Document\Author;
use Editorum\Bundle\Document\Collection;
use Editorum\Bundle\Document\Common\AbstractCorporateEntity;
use Editorum\Bundle\Document\Common\AbstractPublication;
use Editorum\Bundle\Document\Conference;
use Editorum\Bundle\Document\ConferenceArticle;
use Editorum\Bundle\Document\Issue;
use Editorum\Bundle\Document\Journal;
use Editorum\Bundle\Document\Monography;
use Editorum\Bundle\Document\Organization;
use Editorum\Bundle\Document\Textbook;
use Editorum\Bundle\Services\MenuBuilder\SideMenuBuilder;

class SideMenu extends AbstractMenu
{
    /**
     * @param Monography $monography
     * @return array
     * @throws \Exception
     */
    public function monographyMenu(Monography $monography)
    {
        $id = $monography->getId() ?: 0;
        $is_published = $monography->getStatuses()['status'] == AbstractPublication::ST_IS_PUBLISHED ? true : false;

        /** @var SideMenuBuilder $builder */
        $builder = $this->getBuilder()->get('Side');
        if ($id > 0) {
            if (!$is_published) {
                $builder
                    ->addItem(
                        'common',
                        'menu.editorum.document.monography.common',
                        $this->generateUrlByRouteName('monography_edit', ['id' => $id]),
                        ['icon' => 'home']
                    )
                    ->addItem(
                        'authors',
                        'menu.editorum.document.monography.author',
                        $this->generateUrlByRouteName('monography_authors', ['id' => $id]),
                        ['icon' => 'users']
                    )
                    ->addItem(
                        'text',
                        'menu.editorum.document.monography.text',
                        $this->generateUrlByRouteName('monography_text', ['id' => $id]),
                        ['icon' => 'sticky-note-o']
                    )
                    ->addItem(
                        'chapters',
                        'menu.editorum.document.monography.chapter',
                        $this->generateUrlByRouteName('monography_chapters', ['id' => $id]),
                        ['icon' => 'book']
                    )
                    ->addItem(
                        'classifs',
                        'Классификаторы',
                        $this->generateUrlByRouteName('monography_classif', ['id' => $id]),
                        ['icon' => 'bars']
                    )
                    ->addItem(
                        'refs',
                        'menu.editorum.document.monography.literature_list',
                        $this->generateUrlByRouteName('monography_references', ['id' => $id]),
                        ['icon' => 'list']
                    )
                    ->addItem(
                        'files',
                        'menu.editorum.document.monography.file_upload',
                        $this->generateUrlByRouteName('monography_files', ['id' => $id]),
                        ['icon' => 'upload']
                    )
                    ->addItem(
                        'task',
                        'menu.editorum.document.monography.task',
                        '#',
                        ['icon' => 'tasks']
                    )->addItem(
                        'validate',
                        'Публикация',
                        $this->generateUrlByRouteName('publication_validate', [
                            'id' => $id,
                            'type' => $monography->getObjectName()
                        ]),
                        ['icon' => 'list']
                    )
                    ->addSeparator();
            }
            $builder
                ->addItem(
                    'rinc',
                    'menu.editorum.document.monography.rinc',
                    $this->generateUrlByRouteName('monography_rinc', ['id' => $id])
                )
                ->addItem(
                    'doi',
                    'menu.editorum.document.monography.doi',
                    $this->generateUrlByRouteName('document_doi_list', [
                        'type' => $monography->getObjectName(),
                        'id' => $id
                    ])
                )
                ->addSeparator()
                ->addItem(
                    'list',
                    'Список монографий',
                    $this->generateUrlByRouteName('monography_list')
                )
//                ->addItem(
//                    'delete',
//                    'menu.editorum.document.monography.remove',
//                    $this->generateUrlByRouteName('monography_remove', ['id' => $id]),
//                    ['class' => 'text-danger']
//                )
            ;
        }

        return $builder->getMenu();
    }


    /**
     * @param Author $author
     * @return array
     */
    public function authorMenu(Author $author)
    {
        $id = $author->getId() ?: 0;
        /** @var SideMenuBuilder $menu */
        $menu = $this->getBuilder()->get('Side');

        if ($id > 0) {
            $menu
                ->addItem(
                    'common',
                    'Общая информация',
                    $this->generateUrlByRouteName('author_edit', [ 'id' => $author->getId() ])
                )
                ->addItem(
                    'science',
                    'Научные степени',
                    $this->generateUrlByRouteName('author_degree_list', ['id' => $author->getId()])
                )
                ->addItem(
                    'job',
                    'Информация о работе',
                    $this->generateUrlByRouteName('author_job', [ 'id' => $author->getId() ])
                )
                ->addSeparator()
                ->addItem(
                    'list',
                    'Список авторов',
                    $this->generateUrlByRouteName('authors_list')
                )
//                ->addItem(
//                    'delete',
//                    'Удалить',
//                    $this->generateUrlByRouteName('author_remove', ['id' => $author->getId()]),
//                    ['class' => 'text-danger']
//                )
            ;
        }

        return $menu->getMenu();
    }


    /**
     * @param Textbook $textbook
     * @return array
     * @throws \Exception
     */
    public function textbookMenu(Textbook $textbook)
    {
        $id = $textbook->getId() ?: 0;
        $is_published = $textbook->getStatuses()['status'] == AbstractPublication::ST_IS_PUBLISHED ? true : false;

        /** @var SideMenuBuilder $builder */
        $builder = $this->getBuilder()->get('Side');
        if ($id > 0) {
            if (!$is_published) {
                $builder
                    ->addItem(
                        'common',
                        'Общие сведения',
                        $this->generateUrlByRouteName('textbook_edit', ['id' => $id]),
                        ['icon' => 'home']
                    )
                    ->addItem(
                        'authors',
                        'Авторы',
                        $this->generateUrlByRouteName('textbook_authors', ['id' => $id]),
                        ['icon' => 'users']
                    )
                    ->addItem(
                        'text',
                        'Текст',
                        $this->generateUrlByRouteName('textbook_text', ['id' => $id]),
                        ['icon' => 'sticky-note-o']
                    )
                    ->addItem(
                        'chapters',
                        'Главы',
                        $this->generateUrlByRouteName('textbook_chapters', ['id' => $id]),
                        ['icon' => 'book']
                    )
                    ->addItem(
                        'classifs',
                        'Классификаторы',
                        $this->generateUrlByRouteName('textbook_classif', ['id' => $id]),
                        ['icon' => 'bars']
                    )
                    ->addItem(
                        'refs',
                        'Список литературы',
                        $this->generateUrlByRouteName('textbook_references', ['id' => $id]),
                        ['icon' => 'list']
                    )
                    ->addItem(
                        'files',
                        'Загрузка файлов',
                        $this->generateUrlByRouteName('textbook_files', ['id' => $id]),
                        ['icon' => 'upload']
                    )->addItem(
                        'validate',
                        'Публикация',
                        $this->generateUrlByRouteName('publication_validate', [
                            'id' => $id,
                            'type' => $textbook->getObjectName()
                        ]),
                        ['icon' => 'list']
                    )
                    ->addSeparator();
            }
            $builder
                ->addItem(
                    'rinc',
                    'Отправка в РИНЦ',
                    $this->generateUrlByRouteName('textbook_rinc', ['id' => $id])
                )

                ->addItem(
                    'doi',
                    'menu.editorum.document.monography.doi',
                    $this->generateUrlByRouteName('document_doi_list', [
                        'type' => $textbook->getObjectName(),
                        'id' => $id
                    ])
                )
                ->addSeparator()
                ->addItem(
                    'list',
                    'Список учебников',
                    $this->generateUrlByRouteName('textbook_list')
                )
//                ->addItem(
//                    'delete',
//                    'Удалить',
//                    '#',
//                    ['class' => 'text-danger']
//                )
            ;
        }

        return $builder->getMenu();
    }


    /**
     * @param Journal $journal
     * @return array
     * @throws \Exception
     */
    public function journalMenu(Journal $journal)
    {
        $id = $journal->getId() ?: 0;

        /** @var SideMenuBuilder $builder */
        $builder = $this->getBuilder()->get('Side');
        if ($id > 0) {
            $builder
                ->addItem(
                    'edit',
                    'Сведения о журнале',
                    $this->generateUrlByRouteName('journal_edit', ['id' => $id]),
                    ['icon' => 'home']
                )
                ->addItem(
                    'rubrics',
                    'Рубрики',
                    $this->generateUrlByRouteName('journal_rubrics', ['id' => $id]),
                    ['icon' => 'bars']
                )
                ->addItem(
                    'issues',
                    'Номера',
                    $this->generateUrlByRouteName('journal_issues', ['id' => $id]),
                    ['icon' => 'file-text-o']
                )
                ->addItem(
                    'issues_composer',
                    'Формирование номеров',
                    $this->generateUrlByRouteName('issues_multiply_compose', ['id' => $id]),
                    ['icon' => 'exchange']
                )
                ->addItem(
                    'editorialCouncil',
                    'Ред. совет',
                    $this->generateUrlByRouteName('editorial_council_list', ['id' => $id]),
                    ['icon' => 'users']
                )
                ->addItem(
                    'editorialTeam',
                    'Ред. коллегия',
                    $this->generateUrlByRouteName('editorial_team_list', ['id' => $id]),
                    ['icon' => 'users']
                )
                ->addItem(
                    'articles_pull',
                    'Пул статей',
                    $this->generateUrlByRouteName('journal_articles', ['id' => $id]),
                    ['icon' => 'bars']
                )
                ->addItem(
                    'classifs',
                    'Классификаторы',
                    $this->generateUrlByRouteName('journal_classif', ['id' => $id]),
                    ['icon' => 'bars']
                )
                ->addSeparator()
                ->addItem(
                    'doi',
                    'Получение DOI',
                    $this->generateUrlByRouteName('document_doi_list', [
                        'type' => $journal->getObjectName(),
                        'id' => $id
                    ]),
                    ['icon' => 'bars']
                )
                ->addSeparator()
                ->addItem(
                    'list',
                    'Список журналов',
                    $this->generateUrlByRouteName('journal_list')
                )
//                ->addItem(
//                    'delete',
//                    'Удалить',
//                    '#',
//                    ['class' => 'text-danger']
//                )
            ;
        }

        return $builder->getMenu();
    }


    /**
     * @param Article $article
     * @return array
     * @throws \Exception
     */
    public function articleMenu(Article $article)
    {
        $id = $article->getId() ?: 0;
        $is_published = $article->getStatuses()['status'] == Journal::ST_IS_PUBLISHED ? true : false;

        /** @var SideMenuBuilder $builder */
        $builder = $this->getBuilder()->get('Side');
        if (!$is_published) {
            $builder
                ->addItem(
                    'edit',
                    'Сведения о статье',
                    $this->generateUrlByRouteName('article_edit', ['id' => $id]),
                    ['icon' => 'home']
                )
                ->addItem(
                    'authors',
                    'Авторы',
                    $this->generateUrlByRouteName('article_authors', ['id' => $id]),
                    ['icon' => 'users']
                )
                ->addItem(
                    'text',
                    'Текст',
                    $this->generateUrlByRouteName('article_text', ['id' => $id]),
                    ['icon' => 'sticky-note-o']
                )
                ->addItem(
                    'classif',
                    'Классификаторы',
                    $this->generateUrlByRouteName('article_classif', ['id' => $id]),
                    ['icon' => 'sticky-note-o']
                )
                ->addItem(
                    'files_upload',
                    'Файлы',
                    $this->generateUrlByRouteName('article_files', ['id' => $id]),
                    ['icon' => 'file']
                )
                ->addItem(
                    'refs',
                    'Список литературы',
                    $this->generateUrlByRouteName('article_references', ['id' => $id]),
                    ['icon' => 'list']
                )
                ->addItem(
                    'validate',
                    'Публикация',
                    $this->generateUrlByRouteName('publication_validate', [
                        'id' => $id,
                        'type' => $article->getObjectName()
                    ]),
                    ['icon' => 'list']
                )
                ->addSeparator();
        }
        $builder
            ->addItem(
                'doi',
                'Получение DOI',
                $this->generateUrlByRouteName('document_doi_list', [
                    'type' => $article->getObjectName(),
                    'id' => $id
                ])
            )
            ->addItem(
                'rinc',
                'Отправка в РИНЦ',
                $this->generateUrlByRouteName('article_rinc', ['id' => $id])
            )
            ->addSeparator()
            ->addItem(
                'list',
                'Список статей',
                $this->generateUrlByRouteName('article_list')
            )
        ;

        return $builder->getMenu();
    }


    /**
     * @param ConferenceArticle $article
     * @return array
     * @throws \Exception
     */
    public function conferencearticleMenu(ConferenceArticle $article)
    {
        $id = $article->getId() ?: 0;
        $is_published = $article->getStatuses()['status'] == Conference::ST_IS_PUBLISHED ? true : false;

        /** @var SideMenuBuilder $builder */
        $builder = $this->getBuilder()->get('Side');
        if (!$is_published) {
            $builder
                ->addItem(
                    'edit',
                    'Сведения о статье',
                    $this->generateUrlByRouteName('conferencearticle_edit', ['id' => $id]),
                    ['icon' => 'home']
                )
                ->addItem(
                    'authors',
                    'Авторы',
                    $this->generateUrlByRouteName('conferencearticle_authors', ['id' => $id]),
                    ['icon' => 'users']
                )
                ->addItem(
                    'text',
                    'Текст',
                    $this->generateUrlByRouteName('conferencearticle_text', ['id' => $id]),
                    ['icon' => 'sticky-note-o']
                )
                ->addItem(
                    'classifs',
                    'Классификаторы',
                    $this->generateUrlByRouteName('conferencearticle_classif', ['id' => $id]),
                    ['icon' => 'bars']
                )
                ->addItem(
                    'files_upload',
                    'Файлы',
                    $this->generateUrlByRouteName('conferencearticle_files', ['id' => $id]),
                    ['icon' => 'file']
                )
                ->addItem(
                    'refs',
                    'Список литературы',
                    $this->generateUrlByRouteName('conferencearticle_references', ['id' => $id]),
                    ['icon' => 'list']
                )->addItem(
                    'validate',
                    'Публикация',
                    $this->generateUrlByRouteName('publication_validate', [
                        'id' => $id,
                        'type' => $article->getObjectName()
                    ]),
                    ['icon' => 'list']
                )
                ->addSeparator();
        }
        $builder
            ->addItem(
                'files',
                'Получение DOI',
                $this->generateUrlByRouteName('document_doi_list', [
                    'type' => $article->getObjectName(),
                    'id' => $id
                ])
            )
            ->addItem(
                'rinc',
                'Отправка в РИНЦ',
                $this->generateUrlByRouteName('conferencearticle_rinc', ['id' => $id])
            )
            ->addSeparator()
            ->addItem(
                'list',
                'Список статей для конференций',
                $this->generateUrlByRouteName('conferencearticle_list')
            )
        ;

        return $builder->getMenu();
    }


    /**
     * @param Organization $organization
     * @return array
     */
    public function organizationMenu(Organization $organization)
    {
        $id = $organization->getId() ?: 0;

        /** @var SideMenuBuilder $builder */
        $builder = $this->getBuilder()->get('Side');
        $builder
            ->addItem(
                'edit',
                'Сведения об организации',
                $this->generateUrlByRouteName('organization_edit', ['id' => $id])
            )
            ->addItem(
                'corporates',
                'Юр. лица',
                $this->generateUrlByRouteName('organization_corporates', ['id' => $id])
            );

        return $builder->getMenu();
    }


    /**
     * @param AbstractCorporateEntity $corporate
     * @return array
     */
    public function corporateMenu(AbstractCorporateEntity $corporate)
    {
        $id = $corporate->getId() ?: 0;

        /** @var SideMenuBuilder $builder */
        $builder = $this->getBuilder()->get('Side');
        $builder
            ->addItem(
                'edit',
                'Сведения об юр.лице',
                $this->generateUrlByRouteName('corporate_'.$corporate->corporateName().'_edit', ['id' => $id])
            )
            ->addItem(
                'corporates',
                'Сотрудники',
                $this->generateUrlByRouteName('corporate_employees', [
                    'id' => $id,
                    'type' => $corporate->corporateName()
                ])
            );

        return $builder->getMenu();
    }


    /**
     * @param Issue $issue
     * @return array
     */
    public function issueMenu(Issue $issue)
    {
        $id = $issue->getId() ?: 0;
        $is_published = $issue->getStatuses()['status'] == Journal::ST_IS_PUBLISHED ? true : false;

        /** @var SideMenuBuilder $builder */
        $builder = $this->getBuilder()->get('Side');
        $builder
            ->addItem(
                'overview',
                'Сведения о номере',
                $this->generateUrlByRouteName('issue_edit', [ 'id' => $id ])
            );
        if (!$is_published) {
            $builder
                ->addItem(
                    'rubrics',
                    'Рубрики номера',
                    $this->generateUrlByRouteName('issue_rubrics', [ 'id' => $id ])
                )
                ->addItem(
                    'compose',
                    'Формирование номера',
                    $this->generateUrlByRouteName('issue_compose', [ 'id' => $id ])
                )
                ->addItem(
                    'classif',
                    'Классификаторы номера',
                    $this->generateUrlByRouteName('issue_classif', [ 'id' => $id ])
                )
                ->addItem(
                    'validate',
                    'Публикация',
                    $this->generateUrlByRouteName('publication_validate', [
                        'id' => $id,
                        'type' => $issue->getObjectName()
                    ]),
                    ['icon' => 'list']
                )
            ;
        }
        $builder
            ->addItem(
                'articles',
                'Статьи номера',
                $this->generateUrlByRouteName('issue_articles', [ 'id' => $id ])
            )
            ->addSeparator()
            ->addItem(
                'doi',
                'Получение DOI',
                $this->generateUrlByRouteName('document_doi_list', [
                    'type' => $issue->getObjectName(),
                    'id' => $id
                ])
            )
            ->addItem(
                'rinc',
                'Файл для РИНЦ',
                $this->generateUrlByRouteName('issue_rinc', ['id' => $id])
            )
            ->addSeparator()
        ;

        if ($issue->getJournal()) {
            $builder->addItem(
                'back',
                'Вернуться в журнал',
                $this->generateUrlByRouteName('journal_edit', [ 'id' => $issue->getJournal()->getId() ]),
                ['class' => 'text-success']
            );
        }
        $builder
            ->addItem(
                'list',
                'Список номеров',
                $this->generateUrlByRouteName('issue_list')
            )
        ;

//        if ($id > 0) {
//            $builder
//                ->addItem(
//                    'delete',
//                    'Удалить',
//                    $this->generateUrlByRouteName('issue_remove', [ 'id' => $id ]),
//                    ['class' => 'text-danger']
//                );
//        }

        return $builder->getMenu();
    }


    /**
     * @param Conference $conference
     * @return array
     */
    public function conferenceMenu(Conference $conference)
    {
        $id = $conference->getId() ?: 0;

        /** @var SideMenuBuilder $builder */
        $builder = $this->getBuilder()->get('Side');
        $builder
            ->addItem(
                'edit',
                'Сведения о конференции',
                $this->generateUrlByRouteName('conference_edit', [ 'id' => $id ])
            )
            ->addItem(
                'conference_council',
                'Предсовет',
                $this->generateUrlByRouteName('conference_council_list', [ 'id' => $id ])
            )
            ->addItem(
                'sections',
                'Секции',
                $this->generateUrlByRouteName('conference_sections', [ 'id' => $id ])
            )
            ->addItem(
                'classifs',
                'Классификаторы',
                $this->generateUrlByRouteName('conference_classif', ['id' => $id]),
                ['icon' => 'bars']
            )
            ->addItem(
                'collections',
                'Сборники',
                $this->generateUrlByRouteName('conference_collections', [ 'id' => $id ])
            )
            ->addSeparator()
            ->addItem(
                'doi',
                'Получение DOI',
                $this->generateUrlByRouteName('document_doi_list', [
                    'type' => $conference->getObjectName(),
                    'id' => $id
                ])
            )
        ;
        $builder
            ->addSeparator()
            ->addItem(
                'list',
                'Список конференций',
                $this->generateUrlByRouteName('conference_list')
            )
        ;

//        if ($id > 0) {
//            $builder
//                ->addItem(
//                    'delete',
//                    'Удалить',
//                    $this->generateUrlByRouteName('conference_remove', [ 'id' => $id ]),
//                    ['class' => 'text-danger']
//                );
//        }

        return $builder->getMenu();
    }


    /**
     * @param Collection $collection
     * @return array
     */
    public function collectionMenu(Collection $collection)
    {
        $id = $collection->getId() ?: 0;
        $is_published = $collection->getStatuses()['status'] == AbstractPublication::ST_IS_PUBLISHED ? true : false;

        /** @var SideMenuBuilder $builder */
        $builder = $this->getBuilder()->get('Side');
        if (!$is_published) {
            $builder
                ->addItem(
                    'edit',
                    'Общие сведения',
                    $this->generateUrlByRouteName('collection_edit', [ 'id' => $id ]),
                    ['icon' => 'bars']
                )
                ->addItem(
                    'sections',
                    'Секции',
                    $this->generateUrlByRouteName('collection_sections', [ 'id' => $id ]),
                    ['icon' => 'bars']
                )
                ->addItem(
                    'collection_compose',
                    'Состав сборника',
                    $this->generateUrlByRouteName('collection_compose', [ 'id' => $id ]),
                    ['icon' => 'bars']
                )
                ->addItem(
                    'classifs',
                    'Классификаторы',
                    $this->generateUrlByRouteName('collection_classif', ['id' => $id]),
                    ['icon' => 'bars']
                )
                ->addItem(
                    'validate',
                    'Публикация',
                    $this->generateUrlByRouteName('publication_validate', [
                        'id' => $id,
                        'type' => $collection->getObjectName()
                    ]),
                    ['icon' => 'list']
                )
            ;
        }
        $builder
            ->addItem(
                'articles',
                'Статьи',
                $this->generateUrlByRouteName('collection_articles', [ 'id' => $id ]),
                ['icon' => 'bars']
            )
            ->addSeparator()
            ->addItem(
                'rinc',
                'Отправка РИНЦ',
                $this->generateUrlByRouteName('collection_rinc', [ 'id' => $id ])
            )
            ->addItem(
                'doi',
                'Получение DOI',
                $this->generateUrlByRouteName('document_doi_list', [
                    'type' => $collection->getObjectName(),
                    'id' => $id
                ])
            )
            ->addSeparator()
        ;

        if ($collection->getConference()) {
            $builder->addItem(
                'back',
                'Перейти в конференцию',
                $this->generateUrlByRouteName('conference_edit', ['id' => $collection->getConference()->getId()]),
                ['class' => 'text-success']
            );
        }

        $builder
            ->addItem(
                'list',
                'Список сборников',
                $this->generateUrlByRouteName('collection_list')
            )
        ;


//        if ($id > 0) {
//            $builder
//                ->addItem(
//                    'delete',
//                    'Удалить',
//                    $this->generateUrlByRouteName('collection_remove', [ 'id' => $id ]),
//                    ['class' => 'text-danger']
//                );
//        }

        return $builder->getMenu();
    }
}
