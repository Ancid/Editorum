<?php
namespace Editorum\Bundle\Services\Menu;

use Editorum\Bundle\Document\Journal;
use Editorum\Bundle\Document\Monography;
use Editorum\Bundle\Document\Textbook;
use Editorum\Bundle\Services\MenuBuilder\SideMenuBuilder;

class NaukaSideMenu extends AbstractMenu
{
    /**
     * @param Journal $journal
     * @return array
     * @throws \Exception
     */
    public function journalMenu(Journal $journal)
    {
        $id = $journal->getId() ? $journal->getId() : 0;

        /** @var SideMenuBuilder $builder */
        $builder = $this->getBuilder()->get('Side');
        $builder
            ->addItem(
                'edit',
                'Сведения о журнале',
                $this->generateUrlByRouteName('nauka_journal', ['id' => $id])
            )
            ->addItem(
                'ethics',
                'Этика научных публикаций',
                '#'
            )
            ->addItem(
                'article_order',
                'Порядок опубликования статей',
                '#'
            )
            ->addItem(
                'require',
                'Требования к статьям',
                '#'
            )
            ->addItem(
                'editorial',
                'Редакционный совет и (или) редколлегия',
                '#'
            )
            ->addItem(
                'editorial_geo',
                'География редсовета и (или) редколлегии журнала',
                '#'
            )
            ->addItem(
                'authors_geo',
                'География авторов журнала',
                '#'
            )
            ->addItem(
                'authors',
                'Аторы',
                '#'
            )
            ->addItem(
                'archieve',
                'Архив номеров',
                '#'
            )
            ->addSeparator();

        $builder
            ->addItem(
                'subscribe',
                'Оформить подписку',
                '#'
            )
            ->addItem(
                'subscribe_news',
                'Подписаться на новости журнала',
                '#'
            )
        ;

        return $builder->getMenu();
    }

    public function orderMenu()
    {
        /** @var SideMenuBuilder $builder */
        $builder = $this->getBuilder()->get('Side');
        $builder
            ->addItem(
                'subscribe',
                'Оформить подписку',
                '#'
            )
            ->addItem(
                'subscribe_news',
                'Подписаться на новости журнала',
                '#'
            )
        ;

        return $builder->getMenu();
    }
}
