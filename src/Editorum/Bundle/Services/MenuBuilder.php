<?php
namespace Editorum\Bundle\Services;

use Editorum\Bundle\Services\MenuBuilder\BuilderInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class MenuBuilder
 *
 * @package Editorum\Bundle\Services
 */
class MenuBuilder
{
    /** @var TranslatorInterface */
    private $translator;

    /**
     * MenuBuilder constructor.
     *
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param $name
     *
     * @return BuilderInterface
     * @throws \Exception
     */
    public function get($name)
    {
        $className = '\\Editorum\\Bundle\\Services\\MenuBuilder\\' . $name . 'MenuBuilder';

        if (!class_exists($className)) {
            throw new \Exception('MenuBuilder for '.$name.' does not exists!');
        }

        $class = new $className($this->translator);
        if (!$class instanceof BuilderInterface) {
            throw new \Exception('MenuBuilder must implement BuilderInterface!');
        }

        return $class;
    }
}
