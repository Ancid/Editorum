<?php
///**
// * Created by PhpStorm.
// * User: Xor
// * Date: 25.08.2015
// * Time: 20:45
// */
//namespace Editorum\Bundle\Services;
//
//use Editorum\Bundle\Document\Collection;
//use Editorum\Bundle\Document\Common\AbstractCorporateEntity;
//use Editorum\Bundle\Document\Issue;
//use Editorum\Bundle\Document\Journal;
//use Editorum\Bundle\Document\Monography;
//use Editorum\Bundle\Document\Organization;
//use Editorum\Bundle\Document\Publication;
//use Editorum\Bundle\Services\MenuBuilder\SideMenuBuilder;
//use Symfony\Bundle\FrameworkBundle\Routing\Router;
//
//
///**
// * @deprecated
// * Class SideMenu
// * @package Editorum\Bundle\Services
// */
//class SideMenu
//{
//    protected $menu = [];
//
//    private $router;
//    private $builder;
//
//    public function __construct(MenuBuilder $builder, Router $router)
//    {
//        $this->router = $router;
//        $this->builder = $builder;
//    }
//
//    protected function generateUrlByRouteName($route_name, $parameters = [])
//    {
//        return $this->router->generate($route_name, $parameters);
//    }
//
//    /**
//     * Боковое меню журнального номера
//     * TODO: отрефакторить под doctrine
//     * @param Issue $issue
//     * @return array
//     */
////    public function issueMenu(Issue $issue)
////    {
////        $id = $issue->getId();
////        $collection = substr(strrchr(get_class($issue), '\\'), 1);
////
////        $menu = [
////            'overview' => [
////                'title' => 'Сведения о номере',
////                'url'   => $this->generateUrlByRouteName('issue_edit', [ 'id' => $id ]),
////            ],
////            'rubrics'  => [
////                'title' => 'Рубрики номера',
////                'url'   => $this->generateUrlByRouteName('issue_rubrics', [ 'id' => $id ]),
////            ],
////            'compose'  => [
////                'title' => 'Формирование номера',
////                'url'   => $this->generateUrlByRouteName('issue_compose', [ 'id' => $id ]),
////            ],
////            'articles'  => [
////                'title' => 'Статьи номера',
////                'url'   => $this->generateUrlByRouteName('issue_articles', [ 'id' => $id ]),
////            ],
////            'classif'  => [
////                'title' => 'Классификаторы номера',
////                'url'   => $this->generateUrlByRouteName('issue_classif', [ 'id' => $id ]),
////            ],
////            'doi'      => [
////                'title' => 'Присвоить DOI',
////                'url'   => $this->generateUrlByRouteName('issue_doi', [ 'id' => $id ]),
////            ],
////            'rinc'     => [
////                'title' => 'Файл для РИНЦ',
////                'url'   => $this->generateUrlByRouteName('issue_rinc', ['id' => $id]),
////            ],
////            'back'     => [
////                'title' => 'Вернуться в журнал',
////                'url'   => $this->generateUrlByRouteName('journal_edit', [ 'id' => $issue->getJournal()->getId() ]),
////                'class' => 'text-success',
////            ],
////        ];
////        if ($id !== null) {
////            $menu['delete'] = [
////                'title' => 'Удалить',
////                'url'   => $this->generateUrlByRouteName('issue_remove', [ 'id' => $id ]),
////                'class' => 'text-danger',
////            ];
////        }
////
////        return $menu;
////    }
//
//    /**
//     * TODO: отрефакторить для doctrine
//     * @param $journal
//     * @return array
//     */
////    public function journalMenu(Journal $journal)
////    {
////        $id = empty($journal->getId()) ? 0 : $journal->getId();
////
////        return [
////            'edit'     => [
////                'title' => 'Сведения о журнале',
////                'url'   => $this->generateUrlByRouteName('journal_edit', [ 'id' => $id ]),
////            ],
////            'rubrics'   => [
////                'title' => 'Рубрики',
////                'url'   => $this->generateUrlByRouteName('journal_rubrics', [ 'id' => $id ]),
////            ],
////            'issues'   => [
////                'title' => 'Номера',
////                'url'   => $this->generateUrlByRouteName('journal_issues', [ 'id' => $id ]),
////            ],
////            'issues_composer'   => [
////                'title' => 'Формирование номеров',
////                'url'   => $this->generateUrlByRouteName('issues_multiply_compose', [ 'id' => $id ]),
////            ],
////            'editorialCouncil' => [
////                'title' => 'Ред. совет',
////                'url' => $this->generateUrlByRouteName('editorial_council_list', ['id' => $id]),
////            ],
////            'editorialTeam' => [
////                'title' => 'Ред. коллегия',
////                'url' => $this->generateUrlByRouteName('editorial_team_list', ['id' => $id]),
////            ],
////            'articles_pull' => [
////                'title' => 'Пул статей',
////                'url'   => $this->generateUrlByRouteName('journal_articles', ['id' => $id]),
////            ],
////            'classifs'      => [
////                'title' => 'Классификаторы',
////                'url'   => $this->generateUrlByRouteName('journal_classif', [ 'id' => $id ]),
////            ],
////            'doi'      => [
////                'title' => 'Присвоить DOI',
////                'url'   => $this->generateUrlByRouteName('journal_doi', [ 'id' => $id ]),
////            ],
////            'delete'   => [
////                'title' => 'Удалить',
////                'url'   => $this->generateUrlByRouteName('journal_remove', [ 'id' => $id ]),
////                'class' => 'text-danger',
////            ],
////        ];
////    }
//
//    /**
//     * @param $emp
//     * @return array
//     */
////    public function employeeMenu($emp)
////    {
////        if (null !== $emp->__get('_id')) {
////            $id = $emp->__get('_id');
////        } else {
////            $id = 0;
////        }
////
////        return [
////            'edit' => [
////                'title' => 'Сведения о сотруднике',
////                'url'   => $this->generateUrlByRouteName('employee_edit', [ 'id' => $id ]),
////            ],
////        ];
////    }
//
////    public function monographyMenu(Monography $monography)
////    {
////        $id = $monography->getId() ? $monography->getId() : 0;
////
////        /** @var SideMenuBuilder $builder */
////        $builder = $this->builder->get('Side');
////        $builder
////            ->addItem(
////                'common',
////                'Общие сведения',
////                $this->generateUrlByRouteName('monography_edit', [ 'id' => $id ]),
////                [ 'icon' => 'home' ]
////            )
////            ->addItem(
////                'authors',
////                'Авторы',
////                '#',
////                [ 'icon' => 'users']
////            )
////            ->addItem(
////                'text',
////                'Текст',
////                '#',
////                [ 'icon' => 'sticky-note-o' ]
////            )
////            ->addItem(
////                'chapters',
////                'Главы',
////                '#',
////                [ 'icon' => 'book' ]
////            )
////            ->addItem(
////                'refs',
////                'Список литературы',
////                '#',
////                [ 'icon' => 'list' ]
////            )
////            ->addItem(
////                'files',
////                'Загрузка файлов',
////                '#',
////                [ 'icon' => 'upload' ]
////            )
////            ->addSeparator()
////            ->addItem(
////                'rinc',
////                'Отправка в РИНЦ',
////                '#'
////            )
////            ->addSeparator()
////        ;
////
////        $builder->addItem(
////            'delete',
////            'Удалить',
////            '#',
////            [ 'class' => 'text-danger' ]
////        );
////
////        return $builder->getMenu();
////    }
//
//    /**
//     * Сгенерировать меню произведения в режиме издателя
//     *
//     * @deprecated
//     * @param $pub
//     * @return array
//     */
//    public function pubMenu($pub, $kind = false)
//    {
//        $id = $pub->getId() ?: 0;
//        $collection = substr(strrchr(get_class($pub), '\\'), 1);
//
//        $menu1 = [
//            'metadata' => [
//                'title' => 'Общие сведения',
//                'url' => $this->generateUrlByRouteName('publication_metadata', [ 'id' => $id ])
//            ],
//            'authors' => [
//                'title' => 'Авторы',
//                'url' => $this->generateUrlByRouteName('publication_authors', [ 'id' => $id ])
//            ],
//            'text' => [
//                'title' => 'Текст',
//                'url'   => $this->generateUrlByRouteName('publication_text', [ 'publication_id' => $id ])
//            ]
//        ];
//        if ($kind != Publication::KIND_ARTICLE) {
//            $menu1['chapters'] = [
//                'title' => 'Главы',
//                'url' => $this->generateUrlByRouteName('publication_chapters', ['publication_id' => $id])
//            ];
//        }
//        if ($kind == Publication::KIND_CONFERENCE_ARTICLE) {
//            $menu1['conference'] = [
//                'title' => 'Подать статью в конференцию',
//                'url' => $this->generateUrlByRouteName('publication_conference', ['id' => $id])
//            ];
//        }
//
//        $menu2 = [
//            'refs' => [
//                'title' => 'Список литературы',
//                'url' => $this->generateUrlByRouteName('publication_references', [ 'publication_id' => $id ])
//            ],
//            'pdf' => [
//                'title' => 'Загрузка файлов',
//                'url' => $this->generateUrlByRouteName('publication_files', [ 'id' => $id ])
//            ],
//            'hr_6' => [
//                'title' => null,
//                'url' => null
//            ],
//            'tasks' => [
//                'title' => 'Управление задачами',
//                'url' => $this->generateUrlByRouteName('task_manage', [ 'id' => $id ])
//            ],
//            'doi' => [
//                'title' => 'Присвоение DOI',
//                'url' => $this->generateUrlByRouteName('publication_doi', [ 'id' => $id ])
//            ],
//            'rinc' => [
//                'title' => 'Отправка в РИНЦ',
//                'url' => $this->generateUrlByRouteName('get_rinc', [
//                    'id' => $id,
//                    'collection' => lcfirst($collection)
//                ])
//            ],
//            'delete' => [
//                'title' => 'Удалить',
//                'url' => $this->generateUrlByRouteName('publication_remove', [ 'id' => $id ]),
//                'class' => 'text-danger',
//            ],
//        ];
//        return array_merge($menu1, $menu2);
//    }
//
//
//    /**
//     * TODO: отрефакторить для doctrine
//     * @param Organization $org
//     * @return array
//     */
////    public function organizationMenu(Organization $org)
////    {
////        $id = $org->getId() ?: 0;
////
////        $ar = [
////            'org' => [
////                'title' => 'Сведения об организации',
////                'url'   => $this->generateUrlByRouteName('organization_edit', ['id' => $id]),
////            ],
////            'corporates' => [
////                'title' => 'Юр. лица',
////                'url'   => $this->generateUrlByRouteName('organization_corporates', ['id' => $id]),
////            ]
////        ];
////
////        return $ar;
////    }
//
//
//    /**
//     * @param AbstractCorporateEntity $entity
//     *
//     * @return array
//     */
////    public function corporateMenu(AbstractCorporateEntity $entity)
////    {
////        $id = $entity->getId() ?: 0;
////        $org_id = $entity->getOrganization() ? $entity->getOrganization()->getId() : 0;
////
////        $ar = [
////            'edit' => [
////                'title' => 'Сведения об юр.лице',
////                'url'   => $this->generateUrlByRouteName('corporate_edit', ['organization_id' => $org_id, 'id' => $id]),
////            ]
////        ];
////        if ($id > 0) {
////            $ar['employee'] = [
////                'title' => 'Сотрудники',
////                'url'   => $this->generateUrlByRouteName('corporate__employees', ['id' => $id]),
////            ];
////        }
////
////        return $ar;
////    }
//
//
//    /**
//     * @param  $collection
//     * @return array
//     */
////    public function collectionMenu($collection)
////    {
////        $id = null !== $collection ? $collection->getId() : 0;
////        $conf_id = $collection->getConference() ? $collection->getConference()->getId() : 0;
////
////        if (null == $id) {
////            return null;
////        }
////
////        $menu =  [
////            'edit'        => [
////                'title' => 'Общие сведения',
////                'url'   => $this->generateUrlByRouteName('collection_edit', [ 'id' => $id ])
////            ],
////            'sections' => [
////                'title' => 'Секции',
////                'url'   => $this->generateUrlByRouteName('collection_sections', [ 'id' => $id ]),
////            ],
////            'collection_compose' => [
////                'title' => 'Состав сборника',
////                'url'   => $this->generateUrlByRouteName('collection_compose', [ 'id' => $id ]),
////            ],
////            'rinc'        => [
////                'title' => 'Отправка РИНЦ',
////                'url'   => $this->generateUrlByRouteName('collection_rinc', [ 'id' => $id]),
////            ],
////            'articles'  =>[
////                'title' => 'Статьи',
////                'url' => $this->generateUrlByRouteName('collection_articles', ['id' => $id])
////            ],
////
//////            'classifs'  =>[
//////                'title' => 'Классификаторы',
//////                'url' => $this->generateUrlByRouteName('collection_classifs',['id' => $id])
//////            ]
////        ];
////        if ($conf_id > 0) {
////            $menu['return'] = [
////                'title' => 'Перейти в конференцию',
////                'url' => $this->generateUrlByRouteName('conference_show', ['id' => $conf_id])
////            ];
////        }
////        return $menu;
////    }
//
//
//    /**
//     * TODO: отрефакторить для doctrine
//     * @param $conference
//     * @return array
//     */
////    public function conferenceMenu($conference)
////    {
////        $id = $conference->getId() ?: 0;
////
////        $menu = [
////            'edit'        => [
////                'title' => 'Сведения о конференции',
////                'url'   => $this->generateUrlByRouteName('conference_edit', ['id' => $id]),
////            ],
////            'conference_council'   => [
////                'title' => 'Предсовет',
////                'url'   => $this->generateUrlByRouteName('conference_council_list', ['id' => $id]),
////            ],
////            'sections'    => [
////                'title' => 'Секции',
////                'url'   => $this->generateUrlByRouteName('conference_sections', ['id' => $id]),
////            ],
////            'collections' => [
////                'title' => 'Сборники',
////                'url'   => $this->generateUrlByRouteName('conference_collections', ['id' => $id]),
////            ],
//////            'create_collections' => [
//////                'title' => 'Формирование сборников',
//////                'url'   => '#',
//////            ],
//////            'classifs'        => [
//////                'title' => 'Классификаторы',
//////                'url'   => '#'
//////            ],
////            'doi'        => [
////                'title' => 'Присвоить DOI',
////                'url'   => '#'
////            ],
////        ];
////        if ($id > 0) {
////            $menu['delete'] = [
////                'title' => 'Удалить',
////                'url' => $this->generateUrlByRouteName('conference_remove', ['id' => $id]),
////                'class' => 'text-danger',
////            ];
////        }
////
////        return $menu;
////    }
//
//
//    /**
//     * Сгенерировать меню произведения в режиме издателя
//     *
//     * @param $article
//     * @return array
//     */
////    public function articleMenu($article)
////    {
////        $id = $article->getId() ?: 0;
////
////        $menu1 = [
////            'metadata' => [
////                'title' => 'Общие сведения',
////                'url' => $this->generateUrlByRouteName('publication_metadata', [ 'id' => $id ])
////            ],
////            'authors' => [
////                'title' => 'Авторы',
////                'url' => $this->generateUrlByRouteName('publication_authors', [ 'id' => $id ])
////            ],
////            'text' => [
////                'title' => 'Текст',
////                'url'   => $this->generateUrlByRouteName('publication_text', [ 'publication_id' => $id ])
////            ]
////        ];
////        if (strpos(get_class($article), 'ConferenceArticle')) {
////            $menu1['conference'] = [
////                'title' => 'Подать статью в конференцию',
////                'url' => $this->generateUrlByRouteName('publication_conference', ['id' => $id])
////            ];
////        }
////        $menu2 = [
////            'refs' => [
////                'title' => 'Список литературы',
////                'url' => $this->generateUrlByRouteName('publication_references', [ 'publication_id' => $id ])
////            ],
////            'pdf' => [
////                'title' => 'Загрузка файлов',
////                'url' => $this->generateUrlByRouteName('publication_files', [ 'id' => $id ])
////            ],
////            'hr_6' => [
////                'title' => null,
////                'url' => null
////            ],
////            'tasks' => [
////                'title' => 'Управление задачами',
////                'url' => $this->generateUrlByRouteName('task_manage', [ 'id' => $id ])
////            ],
////            'doi' => [
////                'title' => 'Присвоение DOI',
////                'url' => $this->generateUrlByRouteName('publication_doi', [ 'id' => $id ])
////            ],
////            'rinc' => [
////                'title' => 'Отправка в РИНЦ',
////                'url' => $this->generateUrlByRouteName('article_rinc', ['id' => $id])
////            ],
////            'delete' => [
////                'title' => 'Удалить',
////                'url' => $this->generateUrlByRouteName('publication_remove', [ 'id' => $id ]),
////                'class' => 'text-danger',
////            ],
////        ];
////        return array_merge($menu1, $menu2);
////    }
//}
