<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 01.10.2015
 * Time: 12:17.
 */

namespace Editorum\Bundle\Services\Extension;

use Editorum\Bundle\Logic\Author;
use Editorum\Bundle\Logic\Journal;
use Editorum\Bundle\Logic\Publication;
use Editorum\Bundle\Services\AclManager;
use Symfony\Bridge\Twig\TokenParser\DumpTokenParser;
use Symfony\Component\VarDumper\Cloner\ClonerInterface;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;

class TwigExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    /** @var AclManager */
    protected $acl;

    /**
     * TwigExtension constructor.
     *
     * @param AclManager $acl
     */
    public function __construct(AclManager $acl)
    {
        $this->acl = $acl;
    }

    /**
     * Return array of globals variables
     *
     * @return array
     */
    public function getGlobals()
    {
        return [];
    }

    /**
     * Return the functions registered as twig extensions.
     *
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('is_in_array', [$this, 'isInArray']),
            new \Twig_SimpleFunction('getResultType', [$this, 'getResultType']),
            new \Twig_SimpleFunction('translitIt', [$this, 'translitIt']),
            new \Twig_SimpleFunction('is_acl_granted', [$this->acl, 'isAclGranted']),
        ];
    }

    public function getFilters()
    {
        return [
            'url_decode' => new \Twig_Filter_Method($this, 'urlDecode')];
//            'url_decode' => new \Twig_SimpleFilter  ($this, 'urlDecode')];
    }

    /**
     * URL Decode a string.
     *
     * @param string $url
     *
     * @return string The decoded URL
     */
    public function urlDecode($url)
    {
        return urldecode($url);
    }

    public function getResultType($const = 0)
    {
        switch ($const) {
            case Publication::KIND_PUB:
                $type = 'Произведения';
                break;
            case Journal::KIND_JOURNAL:
                $type = 'Журналы';
                break;
            case Author::KIND_AUTHOR:
                $type = 'Авторы';
                break;
            case 5000:
                $type = 'Энциклопедия ЭБС';
                break;
            default:
                $type = 'Остальное';
        }

        return $type;
    }

    public function isInArray($item, $array)
    {
        return in_array((string)$item, $array);
    }

    public function translitIt($str)
    {
        $tr = [
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'yo',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'j',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'x',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'shh',
            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'\'',
            'э' => 'e\'',   'ю' => 'yu',  'я' => 'ya',
            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'YO',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'J',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'X',   'Ц' => 'C',
            'Ч' => 'CH',  'Ш' => 'SH',  'Щ' => 'SHH',
            'Ь' => '\'',  'Ы' => 'Y\'',   'Ъ' => '\'\'',
            'Э' => 'E\'',   'Ю' => 'YU',  'Я' => 'YA',
        ];

        $urlstr = strtr($str, $tr);

        return $urlstr;
    }

    public function getName()
    {
        return 'twig_extension';
    }
}
