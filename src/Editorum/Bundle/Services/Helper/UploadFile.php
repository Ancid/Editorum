<?php
namespace Editorum\Bundle\Services\Helper;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RequestStack;

class UploadFile
{
    protected $request;
    protected $dir;

    /**
     * UploadFile constructor.
     *
     * @param $requestStack
     */
    public function __construct(RequestStack $requestStack, $dir = 'upload/')
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->dir = $dir;
    }

    /**
     * @param string $dir
     *
     * @return UploadFile
     */
    public function setDir($dir)
    {
        $this->dir = $dir;

        return $this;
    }

    /**
     * @param $fieldName
     *
     * @return array|null|string
     */
    public function upload($fieldName, $formName)
    {
        /** @var UploadedFile|UploadedFile[]|null $file */
        $file = $this->request->files->get($formName)[$fieldName];

        if ($file === null) {
            return null;
        }

        if (!is_array($file)) {
            return $this->uploadOneFile($file);
        }

        $array = [];
        foreach ($file as $uploadedFile) {
            $array[] = $this->uploadOneFile($uploadedFile);
        }

        return $array;
    }

    /**
     * @param UploadedFile $file
     *
     * @return string
     */
    private function uploadOneFile(UploadedFile $file)
    {
        $filename = (new \DateTime())->format('d_m_Y_H_i_s') . '_' . $file->getClientOriginalName();
        $file->move($this->dir, $filename);

        return $this->dir . $filename;
    }
}
