<?php
namespace Editorum\Bundle\Services\Helper;

use Doctrine\ODM\MongoDB\DocumentManager;
use Editorum\Bundle\Document\FileStorage;
use Editorum\Bundle\Form\FileStorage as FileStorageForm;
use Gedmo\Exception\UploadableInvalidFileException;
use Stof\DoctrineExtensionsBundle\Uploadable\UploadableManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RequestStack;

class FileStorageHelper
{
    /** @var  UploadableManager */
    private $uploadableManager;

    /** @var  RequestStack */
    private $requestStack;

    /** @var  DocumentManager */
    private $odm;

    /** @var  Filesystem */
    private $filesystem;

    /** @var array */
    private $allowedMimeTypes = [];

    /**
     * FileStorageHelper constructor.
     * @param UploadableManager $uploadableManager
     * @param RequestStack $requestStack
     * @param DocumentManager $odm
     */
    public function __construct(
        UploadableManager $uploadableManager,
        RequestStack $requestStack,
        DocumentManager $odm,
        Filesystem $filesystem
    ) {
        $this->uploadableManager = $uploadableManager;
        $this->requestStack      = $requestStack;
        $this->odm               = $odm;
        $this->filesystem        = $filesystem;
    }

    /**
     * @param UploadedFile $uploadedFile
     * @param FileStorage $fileStorage
     * @param null|object $document
     * @param callable|null $callback
     * @return FileStorage
     */
    public function uploadOneFile(
        UploadedFile $uploadedFile,
        FileStorage $fileStorage,
        $document = null,
        callable $callback = null
    ) {
        if (!$this->isAllowedMimeType($uploadedFile)) {
            throw new UploadableInvalidFileException('Not allowed mime type');
        }

        if ($callback) {
            $fileStorage = $callback($fileStorage);
        }

        $this->getOdm()->persist($fileStorage);
        $this->uploadableManager->markEntityToUpload($fileStorage, $uploadedFile);
        $fileStorage->setOriginalName($uploadedFile->getClientOriginalName());

        if ($document) {
            $fileStorage->setDocument($document);
        }

        $this->getOdm()->persist($fileStorage);
        $this->getOdm()->flush();

        return $fileStorage;
    }

    /**
     * Загрузка файла
     *
     * @param Form $form
     * @param bool $multi
     * @param string $fieldName
     * @return UploadedFile[]|null
     */
    public function upload(Form &$form, $multi = true, $fieldName = 'path', callable $callback = null)
    {
        $manager = $this->getManager();
        $request = $this->getRequest();
        $odm     = $this->getOdm();

        $type = $form->getConfig()->getType()->getInnerType();
        if (!$type instanceof FileStorageForm) {
            throw new UnexpectedTypeException(
                $type,
                'Editorum\Bundle\From\FileStorage'
            );
        }

        if ($request->getMethod() !== "POST") {
            return null;
        }

        $form->handleRequest($request);

        $errors = null;
        if ($form->isValid()) {
            $data = $form->getData();

            if (!isset($data['document'])) {
                $data['document'] = null;
            }

            /** @var UploadedFile $file */
            if ($multi) {
                foreach ($data[$fieldName] as $file) {
                    if ($this->isAllowedMimeType($file)) {
                        $fileStorage = new FileStorage();
                        $fileStorage->setDocument($data['document']);
                        $odm->persist($fileStorage);

                        $manager->markEntityToUpload($fileStorage, $file);

                        $fileStorage->setOriginalName($file->getClientOriginalName());
                        $odm->persist($fileStorage);

                        if (!is_null($callback)) {
                            $callback($file);
                        }
                    } else {
                        $errors[] = $file;
                    }
                }

                $odm->flush();
            } else {
                $file = $data[$fieldName];
                if ($this->isAllowedMimeType($file)) {
                    $fileStorage = new FileStorage();
                    $fileStorage->setDocument($data['document']);
                    $odm->persist($fileStorage);

                    $manager->markEntityToUpload($fileStorage, $file);

                    $fileStorage->setOriginalName($file->getClientOriginalName());
                    $odm->flush($fileStorage);

                    if (!is_null($callback)) {
                        $callback($file);
                    }
                } else {
                    $errors[] = $file;
                }
            }
        }

        return $errors;
    }

    /**
     * @param UploadedFile $file
     * @return bool
     */
    private function isAllowedMimeType(UploadedFile $file)
    {
        $type = $file->getClientMimeType();
        $allowedMimeTypes = $this->getAllowedMimeTypes();

        return count($allowedMimeTypes) > 0 ? in_array($type, $allowedMimeTypes) : true;
    }

    /**
     * Удаление файла
     *
     * @param $id
     * @return bool
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function remove($id)
    {
        $odm = $this->getOdm();
        $fs = $this->getFilesystem();

        $file = $odm->getRepository('AsuBundle:FileStorage')->find($id);

        if (null === $file) {
            return false;
        }

        // Удаляем файл физически, если он существует
        if ($fs->exists($file->getPath())) {
            $fs->remove($file->getPath());
        }

        // Удаляем запись в базе
        $odm->remove($file);
        $odm->flush();

        return true;
    }

    /**
     * @return Filesystem
     */
    private function getFilesystem()
    {
        return $this->filesystem;
    }

    /**
     * @return UploadableManager
     */
    private function getManager()
    {
        return $this->uploadableManager;
    }

    /**
     * @return null|\Symfony\Component\HttpFoundation\Request
     */
    private function getRequest()
    {
        return $this->requestStack->getCurrentRequest();
    }

    /**
     * @return DocumentManager
     */
    private function getOdm()
    {
        return $this->odm;
    }

    /**
     * @return array
     */
    public function getAllowedMimeTypes()
    {
        return $this->allowedMimeTypes;
    }

    /**
     * @param array $allowedMimeTypes
     * @return self
     */
    public function setAllowedMimeTypes(array $allowedMimeTypes)
    {
        $this->allowedMimeTypes = $allowedMimeTypes;
        return $this;
    }
}
