<?php
namespace Editorum\Bundle\Services\MenuBuilder;

use Symfony\Component\Translation\TranslatorInterface;

abstract class AbstractBuilder implements BuilderInterface
{
    protected $menu = [];

    /** @var TranslatorInterface */
    protected $translator;

    /**
     * AbstractBuilder constructor.
     *
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @inheritDoc
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * @inheritDoc
     */
    abstract public function addItem($name, $title, $url, array $options = []);

    /**
     * @param string $key
     * @param array  $options
     * @param string $domain
     *
     * @return string
     */
    public function trans($key, array $options = [], $domain = 'messages')
    {
        return $this->translator->trans($key, $options, $domain);
    }
}
