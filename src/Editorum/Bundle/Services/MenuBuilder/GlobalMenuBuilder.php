<?php
namespace Editorum\Bundle\Services\MenuBuilder;

/**
 * Class GlobalMenuBuilder
 * @package Editorum\Bundle\Services
 */
class GlobalMenuBuilder extends AbstractBuilder
{
    /**
     * @var null
     */
    protected $lastRootName = null;

    /**
     * Add root item
     * @param $name
     * @param $title
     * @return $this
     */
    public function addRootItem($name, $title, $url = null)
    {
        $this->menu[$name]['title'] = $title;
        if (null != $url) {
            $this->menu[ $name ]['url'] = $url;
        }
        $this->lastRootName = $name;

        return $this;
    }

    /**
     * Add item to menu
     *
     * @param       $name
     * @param       $title
     * @param       $url
     * @param array $options
     *
     * @return $this
     */
    public function addItem($name, $title, $url, array $options = [])
    {
        if (!isset($options['rootName'])) {
            $root = &$this->menu[ $this->lastRootName ]['items'];
        } else {
            $root = &$this->menu[ $options['rootName']]['items'];
            unset($options['rootName']);
        }

        $root[$name] = $options;
        if ($url !== 'empty') {
            $root[$name]['url'] = $url;
        }
        $root[$name]['title'] = $this->trans($title);

        return $this;
    }
}
