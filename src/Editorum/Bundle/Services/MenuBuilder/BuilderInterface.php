<?php
namespace Editorum\Bundle\Services\MenuBuilder;

interface BuilderInterface
{
    /**
     * @param string $name
     * @param string $title
     * @param string $url
     * @param array  $options
     *
     * @return mixed
     */
    public function addItem($name, $title, $url, array $options = []);

    /**
     * @return array
     */
    public function getMenu();
}
