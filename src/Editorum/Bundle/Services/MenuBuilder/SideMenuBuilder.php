<?php
namespace Editorum\Bundle\Services\MenuBuilder;

/**
 * Class SideMenuBuilder
 *
 * @package Editorum\Bundle\Services
 */
class SideMenuBuilder extends AbstractBuilder
{
    /** @var int */
    private $separatorCount = 0;

    /**
     * Add menu item
     *
     * @param string      $name
     * @param string      $title
     * @param string      $url
     * @param array       $options
     *
     * @return $this
     */
    public function addItem($name, $title, $url, array $options = [])
    {
        $this->menu[$name] = [
            'title' => $this->trans($title),
            'url'   => $url,
        ];

        if (isset($options['class'])) {
            $this->menu[$name]['class'] = $options['class'];
        }

        if (isset($options['icon'])) {
            $this->menu[ $name ]['icon'] = $options['icon'];
        }

        return $this;
    }

    /**
     * Add separator
     *
     * @return $this
     */
    public function addSeparator()
    {
        $this->menu['hr_'.$this->separatorCount++] = null;

        return $this;
    }
}
