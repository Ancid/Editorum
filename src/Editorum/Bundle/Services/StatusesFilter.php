<?php
namespace Editorum\Bundle\Services;

use Doctrine\ODM\MongoDB\Query\Builder;

class StatusesFilter
{
    private $parameter;
    private $value;
    private $prefix = 'statuses';

    public function makeFilter(Builder &$query)
    {
        if ((bool) $this->parameter) {
            $prefix = null;

            if ($this->getPrefix()) {
                $prefix = $this->getPrefix() . '.';
            }
            $query->addAnd(
                $query->expr()->field($prefix . $this->parameter)->equals($this->value)
            );
        }
    }

    /**
     * @return mixed
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @param mixed $prefix
     *
     * @return StatusesFilter
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;

        return $this;
    }

    /**
     * @param mixed $parameter
     * @return StatusesFilter
     */
    public function setParameter($parameter)
    {
        $this->parameter = $parameter;
        return $this;
    }

    /**
     * @param mixed $value
     * @return StatusesFilter
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @param $data
     * @param string $delimiter
     * @return $this
     */
    public function prepareData($data, $delimiter = ':')
    {
        $parameters = explode($delimiter, $data);
        $typeFunc = (isset($parameters[2]) ? $parameters[2] : 'str') . 'val';
        if (!function_exists($typeFunc)) {
            $typeFunc = 'strval';
        }

        $this
            ->setParameter(isset($parameters[0]) ? $parameters[0] : null)
            ->setValue(isset($parameters[1]) ? $typeFunc($parameters[1]) : null)
        ;

        return $this;
    }
}
