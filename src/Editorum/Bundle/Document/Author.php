<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 31.03.2016
 * Time: 11:50
 */
namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Doctrine\AsuId;
use Editorum\Bundle\Doctrine\Multilanguage;
use Editorum\Bundle\Doctrine\ObjectName;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableDocument;
use Gedmo\Timestampable\Traits\TimestampableDocument;
use Gedmo\Mapping\Annotation as Gedmo;
use Editorum\Bundle\Document\Common\AbstractPublication as AbstractPublication;
use Editorum\Bundle\Validator\Constraints as CustomAssert;

/**
 * @ODM\Document(collection="author", repositoryClass="Editorum\Bundle\Document\Repository\Author")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @CustomAssert\Region(region="region", country="country")
 * @CustomAssert\Region(region="reg_region", country="reg_country")
 */
class Author
{
    use TimestampableDocument;
    use SoftDeleteableDocument;
    use Multilanguage;
    use AsuId;
    use ObjectName;

    /** @ODM\Id(type="int", strategy="INCREMENT") */
    protected $id;

    /** @ODM\Field(type="string") */
    protected $first_name;

    /** @ODM\Field(type="string") */
    protected $last_name;

    /** @ODM\Field(type="string") */
    protected $second_name;

    /** @ODM\Field(type="string") */
    protected $eng_first_name;

    /** @ODM\Field(type="string") */
    protected $eng_last_name;

    /** @ODM\Field(type="string") */
    protected $adder_id;

    /** @ODM\Field(type="string") */
    protected $snils;

    /** @ODM\Field(type="string") */
    protected $inn;

    /** @ODM\Field(type="string") */
    protected $pass_seria;

    /** @ODM\Field(type="string") */
    protected $pass_number;

    /** @ODM\Field(type="string") */
    protected $pass_dt;

    /** @ODM\Field(type="string") */
    protected $pass_code;

    /** @ODM\Field(type="string") */
    protected $pass_given;

    /** @ODM\Field(type="string") */
    protected $reg_adress;

    /** @ODM\Field(type="string") */
    protected $fact_adress;

    /** @ODM\Field(type="string") */
    protected $bank_reciver;

    /** @ODM\Field(type="string") */
    protected $bank_name;

    /** @ODM\Field(type="string") */
    protected $bank_account;

    /** @ODM\Field(type="string") */
    protected $bank_coaccount;

    /** @ODM\Field(type="string") */
    protected $bank_bik;

    /** @ODM\Field(type="date") */
    protected $birth;

    /** @ODM\Field(type="string") */
    protected $academic_title;

    /** @ODM\Field(type="string") */
    protected $eng_academic_title;

    /** @ODM\Field(type="string") */
    protected $degree;

    /** @ODM\Field(type="string") */
    protected $eng_degree;

    /** @ODM\Field(type="string") */
    protected $department;

    /** @ODM\Field(type="string") */
    protected $eng_department;

    /** @ODM\Field(type="bool") */
    protected $is_reviewer;

    /** @ODM\Field(type="string") */
    protected $city;

    /** @ODM\ReferenceOne(targetDocument="Region", simple=true) */
    protected $region;

    /** @ODM\ReferenceOne(targetDocument="Country", simple=true) */
    protected $country;

    /** @ODM\Field(type="string") */
    protected $reg_city;

    /** @ODM\ReferenceOne(targetDocument="Region", simple=true) */
    protected $reg_region;

    /** @ODM\ReferenceOne(targetDocument="Country", simple=true) */
    protected $reg_country;

    /** @ODM\Field(type="string") */
    protected $reg_index;

    /** @ODM\Field(type="string") */
    protected $fact_index;

    /** @ODM\Field(type="string") */
    protected $adv_title;

    /** @ODM\Field(type="string") */
    protected $eng_adv_title;

    /** @ODM\Field(type="string") */
    protected $scopus;

    /** @ODM\Field(type="string") */
    protected $researcherid;

    /** @ODM\Field(type="string") */
    protected $rinc;

    /** @ODM\Field(type="string") */
    protected $work_country;

    /** @ODM\Field(type="date") */
    protected $date_end;

    /** @ODM\Field(type="string") */
    protected $email;

    /** @ODM\Field(type="string") */
    protected $orcid_id;

    /** @ODM\Field(type="string") */
    protected $cell_phone;

    /** @ODM\Field(type="bool") */
    protected $hasFactAddr;

    /** @ODM\ReferenceMany(targetDocument="Editorum\Bundle\Document\Author\DegreeList", simple=true) */
    protected $degree_list;

    /** @ODM\ReferenceOne(targetDocument="ListStorage", simple=true) */
    protected $science_rang;

    /** @ODM\Field(type="bool") */
    protected $is_work;

    /** @ODM\ReferenceOne(targetDocument="User", inversedBy="author") */
    protected $user;

    /** @ODM\ReferenceMany(
     *     targetDocument="Editorum\Bundle\Document\Common\AbstractPublication",
     *     mappedBy="authors",
     *     cascade={"persist"},
     *     simple=true
     *  ) */
    protected $publications;

    /**
     * @ODM\ReferenceMany(targetDocument="Editorum\Bundle\Document\Author\JobList", simple=true)
     */
    protected $workplaces;

    /** @ODM\ReferenceOne(targetDocument="University", simple=true) */
    protected $university;

    public function __construct()
    {
        $this->publications = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function setFio()
    {
        $ru = $this->getRu();
        $en = $this->getEn();
        $ru['fio'] = trim($ru['last_name'] . ' ' .$ru['first_name']);
        if (!empty($ru['second_name'])) {
            $ru['fio'] .= ' ' . $ru['second_name'];
        }
        if (empty($en['last_name'])) {
            $en['last_name'] = AbstractPublication::transliterate($ru['last_name']);
        }
        if (empty($en['first_name'])) {
            $en['first_name'] = AbstractPublication::transliterate($ru['first_name']);
        }
        $en['fio'] = trim($en['last_name'] . ' ' . $en['first_name']);
        if (empty($en['second_name']) && !empty($ru['second_name'])) {
            $en['second_name'] = AbstractPublication::transliterate($ru['second_name']);
            $en['fio'] .= ' ' . $en['second_name'];
        }
        $this->setRu($ru);
        $this->setEn($en);
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return self
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;
        return $this;
    }

    /**
     * Get firstName
     *
     * @return string $firstName
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return self
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;
        return $this;
    }

    /**
     * Get lastName
     *
     * @return string $lastName
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set secondName
     *
     * @param string $secondName
     * @return self
     */
    public function setSecondName($secondName)
    {
        $this->second_name = $secondName;
        return $this;
    }

    /**
     * Get secondName
     *
     * @return string $secondName
     */
    public function getSecondName()
    {
        return $this->second_name;
    }

    /**
     * Set engFirstName
     *
     * @param string $engFirstName
     * @return self
     */
    public function setEngFirstName($engFirstName)
    {
        $this->eng_first_name = $engFirstName;
        return $this;
    }

    /**
     * Get engFirstName
     *
     * @return string $engFirstName
     */
    public function getEngFirstName()
    {
        return $this->eng_first_name;
    }

    /**
     * Set engLastName
     *
     * @param string $engLastName
     * @return self
     */
    public function setEngLastName($engLastName)
    {
        $this->eng_last_name = $engLastName;
        return $this;
    }

    /**
     * Get engLastName
     *
     * @return string $engLastName
     */
    public function getEngLastName()
    {
        return $this->eng_last_name;
    }

    /**
     * Set adderId
     *
     * @param string $adderId
     * @return self
     */
    public function setAdderId($adderId)
    {
        $this->adder_id = $adderId;
        return $this;
    }

    /**
     * Get adderId
     *
     * @return string $adderId
     */
    public function getAdderId()
    {
        return $this->adder_id;
    }

    /**
     * Set snils
     *
     * @param string $snils
     * @return self
     */
    public function setSnils($snils)
    {
        $this->snils = $snils;
        return $this;
    }

    /**
     * Get snils
     *
     * @return string $snils
     */
    public function getSnils()
    {
        return $this->snils;
    }

    /**
     * Set inn
     *
     * @param string $inn
     * @return self
     */
    public function setInn($inn)
    {
        $this->inn = $inn;
        return $this;
    }

    /**
     * Get inn
     *
     * @return string $inn
     */
    public function getInn()
    {
        return $this->inn;
    }

    /**
     * Set passSeria
     *
     * @param string $passSeria
     * @return self
     */
    public function setPassSeria($passSeria)
    {
        $this->pass_seria = $passSeria;
        return $this;
    }

    /**
     * Get passSeria
     *
     * @return string $passSeria
     */
    public function getPassSeria()
    {
        return $this->pass_seria;
    }

    /**
     * Set passNumber
     *
     * @param string $passNumber
     * @return self
     */
    public function setPassNumber($passNumber)
    {
        $this->pass_number = $passNumber;
        return $this;
    }

    /**
     * Get passNumber
     *
     * @return string $passNumber
     */
    public function getPassNumber()
    {
        return $this->pass_number;
    }

    /**
     * Set passDt
     *
     * @param string $passDt
     * @return self
     */
    public function setPassDt($passDt)
    {
        $this->pass_dt = $passDt;
        return $this;
    }

    /**
     * Get passDt
     *
     * @return string $passDt
     */
    public function getPassDt()
    {
        return $this->pass_dt;
    }

    /**
     * Set passCode
     *
     * @param string $passCode
     * @return self
     */
    public function setPassCode($passCode)
    {
        $this->pass_code = $passCode;
        return $this;
    }

    /**
     * Get passCode
     *
     * @return string $passCode
     */
    public function getPassCode()
    {
        return $this->pass_code;
    }

    /**
     * Set passGiven
     *
     * @param string $passGiven
     * @return self
     */
    public function setPassGiven($passGiven)
    {
        $this->pass_given = $passGiven;
        return $this;
    }

    /**
     * Get passGiven
     *
     * @return string $passGiven
     */
    public function getPassGiven()
    {
        return $this->pass_given;
    }

    /**
     * Set regAdress
     *
     * @param string $regAdress
     * @return self
     */
    public function setRegAdress($regAdress)
    {
        $this->reg_adress = $regAdress;
        return $this;
    }

    /**
     * Get regAdress
     *
     * @return string $regAdress
     */
    public function getRegAdress()
    {
        return $this->reg_adress;
    }

    /**
     * Set factAdress
     *
     * @param string $factAdress
     * @return self
     */
    public function setFactAdress($factAdress)
    {
        $this->fact_adress = $factAdress;
        return $this;
    }

    /**
     * Get factAdress
     *
     * @return string $factAdress
     */
    public function getFactAdress()
    {
        return $this->fact_adress;
    }

    /**
     * Set bankReciver
     *
     * @param string $bankReciver
     * @return self
     */
    public function setBankReciver($bankReciver)
    {
        $this->bank_reciver = $bankReciver;
        return $this;
    }

    /**
     * Get bankReciver
     *
     * @return string $bankReciver
     */
    public function getBankReciver()
    {
        return $this->bank_reciver;
    }

    /**
     * Set bankName
     *
     * @param string $bankName
     * @return self
     */
    public function setBankName($bankName)
    {
        $this->bank_name = $bankName;
        return $this;
    }

    /**
     * Get bankName
     *
     * @return string $bankName
     */
    public function getBankName()
    {
        return $this->bank_name;
    }

    /**
     * Set bankAccount
     *
     * @param string $bankAccount
     * @return self
     */
    public function setBankAccount($bankAccount)
    {
        $this->bank_account = $bankAccount;
        return $this;
    }

    /**
     * Get bankAccount
     *
     * @return string $bankAccount
     */
    public function getBankAccount()
    {
        return $this->bank_account;
    }

    /**
     * Set bankCoaccount
     *
     * @param string $bankCoaccount
     * @return self
     */
    public function setBankCoaccount($bankCoaccount)
    {
        $this->bank_coaccount = $bankCoaccount;
        return $this;
    }

    /**
     * Get bankCoaccount
     *
     * @return string $bankCoaccount
     */
    public function getBankCoaccount()
    {
        return $this->bank_coaccount;
    }

    /**
     * Set bankBik
     *
     * @param string $bankBik
     * @return self
     */
    public function setBankBik($bankBik)
    {
        $this->bank_bik = $bankBik;
        return $this;
    }

    /**
     * Get bankBik
     *
     * @return string $bankBik
     */
    public function getBankBik()
    {
        return $this->bank_bik;
    }

    /**
     * Set birth
     *
     * @param date $birth
     * @return self
     */
    public function setBirth($birth)
    {
        $this->birth = $birth;
        return $this;
    }

    /**
     * Get birth
     *
     * @return date $birth
     */
    public function getBirth()
    {
        return $this->birth;
    }

    /**
     * Set academicTitle
     *
     * @param string $academicTitle
     * @return self
     */
    public function setAcademicTitle($academicTitle)
    {
        $this->academic_title = $academicTitle;
        return $this;
    }

    /**
     * Get academicTitle
     *
     * @return string $academicTitle
     */
    public function getAcademicTitle()
    {
        return $this->academic_title;
    }

    /**
     * Set engAcademicTitle
     *
     * @param string $engAcademicTitle
     * @return self
     */
    public function setEngAcademicTitle($engAcademicTitle)
    {
        $this->eng_academic_title = $engAcademicTitle;
        return $this;
    }

    /**
     * Get engAcademicTitle
     *
     * @return string $engAcademicTitle
     */
    public function getEngAcademicTitle()
    {
        return $this->eng_academic_title;
    }

    /**
     * Set degree
     *
     * @param string $degree
     * @return self
     */
    public function setDegree($degree)
    {
        $this->degree = $degree;
        return $this;
    }

    /**
     * Get degree
     *
     * @return string $degree
     */
    public function getDegree()
    {
        return $this->degree;
    }

    /**
     * Set engDegree
     *
     * @param string $engDegree
     * @return self
     */
    public function setEngDegree($engDegree)
    {
        $this->eng_degree = $engDegree;
        return $this;
    }

    /**
     * Get engDegree
     *
     * @return string $engDegree
     */
    public function getEngDegree()
    {
        return $this->eng_degree;
    }

    /**
     * Set department
     *
     * @param string $department
     * @return self
     */
    public function setDepartment($department)
    {
        $this->department = $department;
        return $this;
    }

    /**
     * Get department
     *
     * @return string $department
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set engDepartment
     *
     * @param string $engDepartment
     * @return self
     */
    public function setEngDepartment($engDepartment)
    {
        $this->eng_department = $engDepartment;
        return $this;
    }

    /**
     * Get engDepartment
     *
     * @return string $engDepartment
     */
    public function getEngDepartment()
    {
        return $this->eng_department;
    }

    /**
     * Set isReviewer
     *
     * @param bool $isReviewer
     * @return self
     */
    public function setIsReviewer($isReviewer)
    {
        $this->is_reviewer = $isReviewer;
        return $this;
    }

    /**
     * Get isReviewer
     *
     * @return bool $isReviewer
     */
    public function getIsReviewer()
    {
        return $this->is_reviewer;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set region
     *
     * @param Region|null $region
     * @return self
     */
    public function setRegion($region)
    {
        $this->region = $region;
        return $this;
    }

    /**
     * Get region
     *
     * @return Region $region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set country
     *
     * @param Country $country
     * @return self
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * Get country
     *
     * @return Country $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set regCity
     *
     * @param string $regCity
     * @return self
     */
    public function setRegCity($regCity)
    {
        $this->reg_city = $regCity;
        return $this;
    }

    /**
     * Get regCity
     *
     * @return string $regCity
     */
    public function getRegCity()
    {
        return $this->reg_city;
    }

    /**
     * Set regRegion
     *
     * @param Region|null $regRegion
     * @return self
     */
    public function setRegRegion($regRegion)
    {
        $this->reg_region = $regRegion;
        return $this;
    }

    /**
     * Get regRegion
     *
     * @return Region $regRegion
     */
    public function getRegRegion()
    {
        return $this->reg_region;
    }

    /**
     * Set regCountry
     *
     * @param Country $regCountry
     * @return self
     */
    public function setRegCountry(Country $regCountry)
    {
        $this->reg_country = $regCountry;
        return $this;
    }

    /**
     * Get regCountry
     *
     * @return Country $regCountry
     */
    public function getRegCountry()
    {
        return $this->reg_country;
    }

    /**
     * Set regIndex
     *
     * @param string $regIndex
     * @return self
     */
    public function setRegIndex($regIndex)
    {
        $this->reg_index = $regIndex;
        return $this;
    }

    /**
     * Get regIndex
     *
     * @return string $regIndex
     */
    public function getRegIndex()
    {
        return $this->reg_index;
    }

    /**
     * Set factIndex
     *
     * @param string $factIndex
     * @return self
     */
    public function setFactIndex($factIndex)
    {
        $this->fact_index = $factIndex;
        return $this;
    }

    /**
     * Get factIndex
     *
     * @return string $factIndex
     */
    public function getFactIndex()
    {
        return $this->fact_index;
    }

    /**
     * Set advTitle
     *
     * @param string $advTitle
     * @return self
     */
    public function setAdvTitle($advTitle)
    {
        $this->adv_title = $advTitle;
        return $this;
    }

    /**
     * Get advTitle
     *
     * @return string $advTitle
     */
    public function getAdvTitle()
    {
        return $this->adv_title;
    }

    /**
     * Set engAdvTitle
     *
     * @param string $engAdvTitle
     * @return self
     */
    public function setEngAdvTitle($engAdvTitle)
    {
        $this->eng_adv_title = $engAdvTitle;
        return $this;
    }

    /**
     * Get engAdvTitle
     *
     * @return string $engAdvTitle
     */
    public function getEngAdvTitle()
    {
        return $this->eng_adv_title;
    }

    /**
     * Set scopus
     *
     * @param string $scopus
     * @return self
     */
    public function setScopus($scopus)
    {
        $this->scopus = $scopus;
        return $this;
    }

    /**
     * Get scopus
     *
     * @return string $scopus
     */
    public function getScopus()
    {
        return $this->scopus;
    }

    /**
     * Set researcherid
     *
     * @param string $researcherid
     * @return self
     */
    public function setResearcherid($researcherid)
    {
        $this->researcherid = $researcherid;
        return $this;
    }

    /**
     * Get researcherid
     *
     * @return string $researcherid
     */
    public function getResearcherid()
    {
        return $this->researcherid;
    }

    /**
     * Set rinc
     *
     * @param string $rinc
     * @return self
     */
    public function setRinc($rinc)
    {
        $this->rinc = $rinc;
        return $this;
    }

    /**
     * Get rinc
     *
     * @return string $rinc
     */
    public function getRinc()
    {
        return $this->rinc;
    }

    /**
     * Set workCountry
     *
     * @param string $workCountry
     * @return self
     */
    public function setWorkCountry($workCountry)
    {
        $this->work_country = $workCountry;
        return $this;
    }

    /**
     * Get workCountry
     *
     * @return string $workCountry
     */
    public function getWorkCountry()
    {
        return $this->work_country;
    }

    /**
     * Set dateEnd
     *
     * @param date $dateEnd
     * @return self
     */
    public function setDateEnd($dateEnd)
    {
        $this->date_end = $dateEnd;
        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return date $dateEnd
     */
    public function getDateEnd()
    {
        return $this->date_end;
    }

        /**
     * Set email
     *
     * @param string $email
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set orcidId
     *
     * @param string $orcidId
     * @return self
     */
    public function setOrcidId($orcidId)
    {
        $this->orcid_id = $orcidId;
        return $this;
    }

    /**
     * Get orcidId
     *
     * @return string $orcidId
     */
    public function getOrcidId()
    {
        return $this->orcid_id;
    }

    /**
     * Set cellPhone
     *
     * @param string $cellPhone
     * @return self
     */
    public function setCellPhone($cellPhone)
    {
        $this->cell_phone = $cellPhone;
        return $this;
    }

    /**
     * Get cellPhone
     *
     * @return string $cellPhone
     */
    public function getCellPhone()
    {
        return $this->cell_phone;
    }

    /**
     * Set hasFactAddr
     *
     * @param bool $hasFactAddr
     * @return self
     */
    public function setHasFactAddr($hasFactAddr)
    {
        $this->hasFactAddr = $hasFactAddr;
        return $this;
    }

    /**
     * Get hasFactAddr
     *
     * @return bool $hasFactAddr
     */
    public function getHasFactAddr()
    {
        return $this->hasFactAddr;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return self
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return User $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set isWork
     *
     * @param bool $isWork
     * @return self
     */
    public function setIsWork($isWork)
    {
        $this->is_work = $isWork;
        return $this;
    }

    /**
     * Get isWork
     *
     * @return bool $isWork
     */
    public function getIsWork()
    {
        return $this->is_work;
    }
    
    /**
     * Set university
     *
     * @param University $university
     * @return $this
     */
    public function setUniversity(University $university)
    {
        $this->university = $university;
        return $this;
    }

    /**
     * Get university
     *
     * @return University $university
     */
    public function getUniversity()
    {
        return $this->university;
    }

    /**
     * Add publication
     *
     * @param AbstractPublication $publication
     */
    public function addPublication(AbstractPublication $publication)
    {
        $this->publications[] = $publication;
    }

    /**
     * Remove publication
     *
     * @param AbstractPublication $publication
     */
    public function removePublication(AbstractPublication $publication)
    {
        $this->publications->removeElement($publication);
    }

    /**
     * Get publications
     *
     * @return \Doctrine\Common\Collections\Collection $publications
     */
    public function getPublications()
    {
        return $this->publications;
    }

    /**
     * Set scienceRang
     *
     * @param Editorum\Bundle\Document\ListStorage $scienceRang
     * @return $this
     */
    public function setScienceRang(\Editorum\Bundle\Document\ListStorage $scienceRang)
    {
        $this->science_rang = $scienceRang;
        return $this;
    }

    /**
     * Get scienceRang
     *
     * @return Editorum\Bundle\Document\ListStorage $scienceRang
     */
    public function getScienceRang()
    {
        return $this->science_rang;
    }

    /**
     * Add workplace
     *
     * @param $workplace
     */
    public function addWorkplace($workplace)
    {
        $this->workplaces[] = $workplace;
    }

    /**
     * Remove workplace
     *
     * @param $workplace
     */
    public function removeWorkplace($workplace)
    {
        $this->workplaces->removeElement($workplace);
    }

    /**
     * Get workplaces
     *
     * @return \Doctrine\Common\Collections\Collection $workplaces
     */
    public function getWorkplaces()
    {
        return $this->workplaces;
    }

    public function clearWorkplaces()
    {
        $this->workplaces = [];
        return $this;
    }

    /**
     * Set degreeList
     *
     * @param Editorum\Bundle\Document\Author\DegreeList $degreeList
     * @return $this
     */
    public function setDegreeList(\Editorum\Bundle\Document\Author\DegreeList $degreeList)
    {
        $this->degree_list = $degreeList;
        return $this;
    }

    /**
     * Get degreeList
     *
     * @return Editorum\Bundle\Document\Author\DegreeList $degreeList
     */
    public function getDegreeList()
    {
        return $this->degree_list;
    }

    /**
     * Add degreeList
     *
     * @param Editorum\Bundle\Document\Author\DegreeList $degreeList
     */
    public function addDegreeList(\Editorum\Bundle\Document\Author\DegreeList $degreeList)
    {
        $this->degree_list[] = $degreeList;
    }

    /**
     * Remove degreeList
     *
     * @param Editorum\Bundle\Document\Author\DegreeList $degreeList
     */
    public function removeDegreeList(\Editorum\Bundle\Document\Author\DegreeList $degreeList)
    {
        $this->degree_list->removeElement($degreeList);
    }


    public function clearDegreeList()
    {
        $this->degree_list = [];

        return $this;
    }
}
