<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 16.05.2016
 * Time: 12:03
 */

namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Document\Common\AbstractClassificator;

/**
 * Class ClassificatorUdk
 * @package Editorum\Bundle\Document
 *
 * @ODM\Document(collection="classif_grnti")
 */
class ClassificatorGrnti extends AbstractClassificator
{
    public function __construct()
    {
        $this->name = parent::CL_GRNTI;
    }



    /**
     * Set systemName
     *
     * @param string $systemName
     * @return $this
     */
    public function setSystemName($systemName)
    {
        $this->system_name = $systemName;
        return $this;
    }

    /**
     * Get systemName
     *
     * @return string $systemName
     */
    public function getSystemName()
    {
        return $this->system_name;
    }
}
