<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 16.05.2016
 * Time: 12:03
 */

namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Document\Common\AbstractClassificator;

/**
 * Class ClassificatorTbk
 * @package Editorum\Bundle\Document
 *
 * @ODM\Document(collection="classif_tbk")
 */
class ClassificatorTbk extends AbstractClassificator
{
    public function __construct()
    {
        $this->name = parent::CL_TBK;
    }



    /**
     * Set systemName
     *
     * @param string $systemName
     * @return $this
     */
    public function setSystemName($systemName)
    {
        $this->system_name = $systemName;
        return $this;
    }

    /**
     * Get systemName
     *
     * @return string $systemName
     */
    public function getSystemName()
    {
        return $this->system_name;
    }
}
