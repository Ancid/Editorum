<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 12.05.16
 * Time: 13:14
 */

namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\Bundle\MongoDBBundle\Validator\Constraints\Unique as UniqueDocument;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\Document(collection="contact_type", repositoryClass="Editorum\Bundle\Document\Repository\ContactType")
 * @UniqueDocument(
 *     fields={"code"},
 *     repositoryMethod="findUniqueBy"
 * )
 */
class ContactType
{
    /** @MongoDB\Id(type="int", strategy="INCREMENT", name="_id") */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     * @Assert\NotNull()
     */
    protected $name;

    /**
     * @MongoDB\Field(type="string")
     * @Assert\NotNull()
     */
    protected $code;

    /** @MongoDB\Field(type="string") */
    protected $description;

    /** @MongoDB\Field(type="string") */
    protected $icon;

    /** @MongoDB\Field(type="string") */
    protected $validate_regexp;
    
    /** @MongoDB\Field(type="boolean") */
    protected $is_enabled = true;

    /** @MongoDB\ReferenceMany(targetDocument="Contact", simple=true) */
    protected $contacts = [];
    
    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * Get code
     *
     * @return string $code
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return self
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * Get icon
     *
     * @return string $icon
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set isEnabled
     *
     * @param boolean $isEnabled
     * @return self
     */
    public function setIsEnabled($isEnabled)
    {
        $this->is_enabled = $isEnabled;
        return $this;
    }

    /**
     * Get isEnabled
     *
     * @return boolean $isEnabled
     */
    public function getIsEnabled()
    {
        return $this->is_enabled;
    }

    /**
     * Set validateRegexp
     *
     * @param string $validateRegexp
     * @return self
     */
    public function setValidateRegexp($validateRegexp)
    {
        $this->validate_regexp = $validateRegexp;
        return $this;
    }

    /**
     * Get validateRegexp
     *
     * @return string $validateRegexp
     */
    public function getValidateRegexp()
    {
        return $this->validate_regexp;
    }
    
    public function __construct()
    {
        $this->contacts = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add contact
     *
     * @param Editorum\Bundle\Document\Contact $contact
     */
    public function addContact(\Editorum\Bundle\Document\Contact $contact)
    {
        $this->contacts[] = $contact;
    }

    /**
     * Remove contact
     *
     * @param Editorum\Bundle\Document\Contact $contact
     */
    public function removeContact(\Editorum\Bundle\Document\Contact $contact)
    {
        $this->contacts->removeElement($contact);
    }

    /**
     * Get contacts
     *
     * @return \Doctrine\Common\Collections\Collection $contacts
     */
    public function getContacts()
    {
        return $this->contacts;
    }
}
