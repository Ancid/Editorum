<?php
namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Doctrine\AsuId;
use Editorum\Bundle\Doctrine\Multilanguage;
use Editorum\Bundle\Document\Common\AbstractCorporateEntity;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableDocument;
use Gedmo\Timestampable\Traits\TimestampableDocument;


/**
 * @ODM\Document(collection="publisher")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
*/
class Publisher extends AbstractCorporateEntity
{
    /** @ODM\Id(type="int", strategy="INCREMENT") */
    protected $id;

    /** @ODM\Field(type="string") */
    protected $head_fio;

    /** @ODM\Field(type="string") */
    protected $head_fio_genitive;

    /** @ODM\Field(type="string") */
    protected $detail;

    /** @ODM\Field(type="string") */
    protected $head_post;

    /** @ODM\Field(type="string") */
    protected $head_post_genitive;

    /** @ODM\Field(type="string") */
    protected $address;

    /** @ODM\Field(type="string") */
    protected $agreement_count;

    /** @ODM\Field(type="string") */
    protected $system_code;

    /** @ODM\ReferenceMany(targetDocument="Article", mappedBy="publisher", simple=true) */
    protected $articles;

    /** @ODM\ReferenceMany(targetDocument="Monography", mappedBy="publisher", simple=true) */
    protected $monographys;



    public function __construct()
    {
        parent::__construct();
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set headFio
     *
     * @param string $headFio
     * @return self
     */
    public function setHeadFio($headFio)
    {
        $this->head_fio = $headFio;
        return $this;
    }

    /**
     * Get headFio
     *
     * @return string $headFio
     */
    public function getHeadFio()
    {
        return $this->head_fio;
    }

    /**
     * Set headFioGenitive
     *
     * @param string $headFioGenitive
     * @return self
     */
    public function setHeadFioGenitive($headFioGenitive)
    {
        $this->head_fio_genitive = $headFioGenitive;
        return $this;
    }

    /**
     * Get headFioGenitive
     *
     * @return string $headFioGenitive
     */
    public function getHeadFioGenitive()
    {
        return $this->head_fio_genitive;
    }

    /**
     * Set detail
     *
     * @param string $detail
     * @return self
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;
        return $this;
    }

    /**
     * Get detail
     *
     * @return string $detail
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * Set headPost
     *
     * @param string $headPost
     * @return self
     */
    public function setHeadPost($headPost)
    {
        $this->head_post = $headPost;
        return $this;
    }

    /**
     * Get headPost
     *
     * @return string $headPost
     */
    public function getHeadPost()
    {
        return $this->head_post;
    }

    /**
     * Set headPostGenitive
     *
     * @param string $headPostGenitive
     * @return self
     */
    public function setHeadPostGenitive($headPostGenitive)
    {
        $this->head_post_genitive = $headPostGenitive;
        return $this;
    }

    /**
     * Get headPostGenitive
     *
     * @return string $headPostGenitive
     */
    public function getHeadPostGenitive()
    {
        return $this->head_post_genitive;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return self
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * Get address
     *
     * @return string $address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set agreementCount
     *
     * @param string $agreementCount
     * @return self
     */
    public function setAgreementCount($agreementCount)
    {
        $this->agreement_count = $agreementCount;
        return $this;
    }

    /**
     * Get agreementCount
     *
     * @return string $agreementCount
     */
    public function getAgreementCount()
    {
        return $this->agreement_count;
    }

    /**
     * Set systemCode
     *
     * @param string $systemCode
     * @return self
     */
    public function setSystemCode($systemCode)
    {
        $this->system_code = $systemCode;
        return $this;
    }

    /**
     * Get systemCode
     *
     * @return string $systemCode
     */
    public function getSystemCode()
    {
        return $this->system_code;
    }

    /**
     * Set organization
     *
     * @param Organization $organization
     * @return self
     */
    public function setOrganization(Organization $organization)
    {
        $this->organization = $organization;
        return $this;
    }

    /**
     * Get organization
     *
     * @return Organization $organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Add user
     *
     * @param User $user
     */
    public function addUser(User $user)
    {
        $this->users[] = $user;
    }

    /**
     * Remove user
     *
     * @param User $user
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection $users
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add article
     *
     * @param Article $article
     */
    public function addArticle(Article $article)
    {
        $this->articles[] = $article;
    }

    /**
     * Remove article
     *
     * @param Article $article
     */
    public function removeArticle(Article $article)
    {
        $this->articles->removeElement($article);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection $articles
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Add monography
     *
     * @param Monography $monography
     */
    public function addMonography(Monography $monography)
    {
        $this->monographys[] = $monography;
    }

    /**
     * Remove monography
     *
     * @param Monography $monography
     */
    public function removeMonography(Monography $monography)
    {
        $this->monographys->removeElement($monography);
    }

    /**
     * Get monographys
     *
     * @return \Doctrine\Common\Collections\Collection $monographys
     */
    public function getMonographys()
    {
        return $this->monographys;
    }

    /**
     * Set doi
     *
     * @param string $doi
     * @return $this
     */
    public function setDoi($doi)
    {
        $this->doi = $doi;
        return $this;
    }

    /**
     * Get doi
     *
     * @return string $doi
     */
    public function getDoi()
    {
        return $this->doi;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set doiPrefix
     *
     * @param string $doiPrefix
     * @return $this
     */
    public function setDoiPrefix($doiPrefix)
    {
        $this->doi_prefix = $doiPrefix;
        return $this;
    }

    /**
     * Get doiPrefix
     *
     * @return string $doiPrefix
     */
    public function getDoiPrefix()
    {
        return $this->doi_prefix;
    }

    /**
     * Set doiLimit
     *
     * @param int $doiLimit
     * @return $this
     */
    public function setDoiLimit($doiLimit)
    {
        $this->doi_limit = $doiLimit;
        return $this;
    }

    /**
     * Get doiLimit
     *
     * @return int $doiLimit
     */
    public function getDoiLimit()
    {
        return $this->doi_limit;
    }

    /**
     * Set doiLogin
     *
     * @param string $doiLogin
     * @return $this
     */
    public function setDoiLogin($doiLogin)
    {
        $this->doi_login = $doiLogin;
        return $this;
    }

    /**
     * Get doiLogin
     *
     * @return string $doiLogin
     */
    public function getDoiLogin()
    {
        return $this->doi_login;
    }

    /**
     * Set doiPass
     *
     * @param string $doiPass
     * @return $this
     */
    public function setDoiPass($doiPass)
    {
        $this->doi_pass = $doiPass;
        return $this;
    }

    /**
     * Get doiPass
     *
     * @return string $doiPass
     */
    public function getDoiPass()
    {
        return $this->doi_pass;
    }
}
