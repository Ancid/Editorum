<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 28.06.2016
 * Time: 15:36
 */

namespace Editorum\Bundle\Document\Common;

use Editorum\Bundle\Doi\DoiFactory;
use Editorum\Bundle\Rinc\RincFactory;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use ZipArchive;

trait DoiXml
{
    /**
     * Генерирует Doi XML для объекта
     * @throws \Exception
     */
    public function createDoiXml()
    {
        try {
//            $doi_object = DoiFactory::build($this);
            $doi_object = $this->get('doi.factory')->build($this);
            $xml = $doi_object->generateXml();
        } catch (\ErrorException $e) {
            throw new \Exception($e->getMessage());
        }

        return [
            'xml' => $xml,
            'doi_batch_id' => $doi_object->doi_batch_id
        ];
    }
}
