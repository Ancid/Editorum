<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 31.03.2016
 * Time: 15:11
 */
namespace Editorum\Bundle\Document\Common;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class AbstractClassificator
 * @package Editorum\Bundle\Document\Common
 * @ODM\Document(repositoryClass="Editorum\Bundle\Document\Repository\Classificator")
 */
abstract class AbstractClassificator
{
    const CL_UDK = 'УДК';
    const CL_GRNTI = 'ГРНТИ';
    const CL_OKSO = 'ОКСО';
    const CL_BBK = 'ББК';
    const CL_TBK = 'ТБК';

    /** @ODM\Id(type="int", strategy="INCREMENT") */
    protected $id;

    /** @ODM\Field(type="string") */
    protected $name;

    /** @ODM\Field(type="string") */
    protected $system_name;

    /** @ODM\Field(type="hash") */
    protected $children = [];


    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set children
     *
     * @param array $children
     * @return $this
     */
    public function setChildren($children)
    {
        $this->children = $children;
        return $this;
    }

    /**
     * Get children
     *
     * @return array $children
     */
    public function getChildren()
    {
        return $this->children;
    }


    /**
     * @param $child
     * @return $this
     */
    public function addChild($child)
    {
        if (!array_search($child, $this->children)) {
            $this->children[] = $child;
        }
        return $this;
    }

    /**
     * Set systemName
     *
     * @param string $systemName
     * @return $this
     */
    public function setSystemName($systemName)
    {
        $this->system_name = $systemName;
        return $this;
    }

    /**
     * Get systemName
     *
     * @return string $systemName
     */
    public function getSystemName()
    {
        return $this->system_name;
    }
}
