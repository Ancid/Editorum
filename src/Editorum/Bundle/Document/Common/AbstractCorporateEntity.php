<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 20.05.2016
 * Time: 11:38
 */

namespace Editorum\Bundle\Document\Common;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Doctrine\AsuId;
use Editorum\Bundle\Doctrine\Multilanguage;
use Editorum\Bundle\Doctrine\Statuses;
use Editorum\Bundle\Document\Organization;
use Editorum\Bundle\Document\User;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableDocument;
use Gedmo\Timestampable\Traits\TimestampableDocument;
use ReflectionClass;

/**
 * @ODM\Document
 */
abstract class AbstractCorporateEntity
{
    const KIND_PUBLISHER = 'publisher'; // Издательство
    const KIND_UNIVERSITY = 'university'; //Учебное заведение
    const KIND_RESEARCH = 'research'; //Научно исследовательский институт
    const KIND_COMMERCIAL = 'commercial'; //Коммерческая организация

    use TimestampableDocument;
    use SoftDeleteableDocument;
    use Multilanguage;
    use AsuId;
    use Statuses;

    /** @ODM\Id(type="int", strategy="INCREMENT") */
    protected $id;

    /** @ODM\Field(type="string") */
    protected $kind;

    /** @ODM\Field(type="string") */
    protected $short_title;

    /** @ODM\Field(type="string") */
    protected $town;

    /** @ODM\Field(type="string") */
    protected $email;

    /** @ODM\Field(type="int") */
    protected $doi_limit;

    /** @ODM\Field(type="string") */
    protected $doi_prefix;

    /** @ODM\Field(type="string") */
    protected $doi_login;

    /** @ODM\Field(type="string") */
    protected $doi_pass;

    /** @ODM\Field(type="string") */
    protected $rinc_title;

    /** @ODM\Field(type="int") */
    protected $rinc_id;

    /** @ODM\ReferenceOne(
     *     targetDocument="Editorum\Bundle\Document\Organization",
     *     mappedBy="corporates",
     *     cascade={"persist"},
     *  ) */
    protected $organization;

    /** @ODM\ReferenceMany(
     *     targetDocument="Editorum\Bundle\Document\User",
     *     mappedBy="corporate",
     *     cascade={"persist", "remove"}
     * ) */
    protected $users;


    
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public static function getKinds()
    {
        return [
            AbstractCorporateEntity::KIND_PUBLISHER     => 'Издательство',
            AbstractCorporateEntity::KIND_UNIVERSITY    => 'Учебное заведение',
            AbstractCorporateEntity::KIND_RESEARCH      => 'Научно исследовательский институт',
            AbstractCorporateEntity::KIND_COMMERCIAL    => 'Коммерческая организация',
        ];
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set kind
     *
     * @param string $kind
     * @return self
     */
    public function setKind($kind)
    {
        $this->kind = $kind;
        return $this;
    }

    /**
     * Get kind
     *
     * @return string  $kind
     */
    public function getKind()
    {
        return $this->kind;
    }

    /**
     * Set shortTitle
     *
     * @param string $shortTitle
     * @return self
     */
    public function setShortTitle($shortTitle)
    {
        $this->short_title = $shortTitle;
        return $this;
    }

    /**
     * Get shortTitle
     *
     * @return string $shortTitle
     */
    public function getShortTitle()
    {
        return $this->short_title;
    }

    /**
     * Add user
     *
     * @param User $user
     */
    public function addUser(User $user)
    {
        $this->users[] = $user;
    }

    /**
     * Remove user
     *
     * @param User $user
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection $users
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set town
     *
     * @param string $town
     * @return self
     */
    public function setTown($town)
    {
        $this->town = $town;
        return $this;
    }

    /**
     * Get town
     *
     * @return string $town
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Set organization
     *
     * @param Organization $organization
     * @return $this
     */
    public function setOrganization(Organization $organization)
    {
        $this->organization = $organization;
        return $this;
    }

    /**
     * Get organization
     *
     * @return Organization $organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @return string
     */
    public function corporateName()
    {
        $reflection = new ReflectionClass($this);
        return strtolower($reflection->getShortName());
    }

    /**
     * Set email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set doiPrefix
     *
     * @param string $doiPrefix
     * @return $this
     */
    public function setDoiPrefix($doiPrefix)
    {
        $this->doi_prefix = $doiPrefix;
        return $this;
    }

    /**
     * Get doiPrefix
     *
     * @return string $doiPrefix
     */
    public function getDoiPrefix()
    {
        return $this->doi_prefix;
    }

    /**
     * Set doiLimit
     *
     * @param int $doiLimit
     * @return $this
     */
    public function setDoiLimit($doiLimit)
    {
        $this->doi_limit = $doiLimit;
        return $this;
    }

    /**
     * Get doiLimit
     *
     * @return int $doiLimit
     */
    public function getDoiLimit()
    {
        return $this->doi_limit;
    }

    /**
     * Set doiLogin
     *
     * @param string $doiLogin
     * @return $this
     */
    public function setDoiLogin($doiLogin)
    {
        $this->doi_login = $doiLogin;
        return $this;
    }

    /**
     * Get doiLogin
     *
     * @return string $doiLogin
     */
    public function getDoiLogin()
    {
        return $this->doi_login;
    }

    /**
     * Set doiPass
     *
     * @param string $doiPass
     * @return $this
     */
    public function setDoiPass($doiPass)
    {
        $this->doi_pass = $doiPass;
        return $this;
    }

    /**
     * Get doiPass
     *
     * @return string $doiPass
     */
    public function getDoiPass()
    {
        return $this->doi_pass;
    }

    /**
     * Set rinc_id
     *
     * @param integer $rinc_id
     * @return self
     */
    public function setRincId($rinc_id)
    {
        $this->rinc_id = $rinc_id;
        return $this;
    }

    /**
     * Get rinc_id
     *
     * @return integer $rinc_id
     */
    public function getRincId()
    {
        return $this->rinc_id;
    }

    /**
     * Set rinc_title
     *
     * @param integer $rinc_title
     * @return self
     */
    public function setRincTitle($rinc_title)
    {
        $this->rinc_title = $rinc_title;
        return $this;
    }

    /**
     * Get rinc_title
     *
     * @return integer $rinc_title
     */
    public function getRincTitle()
    {
        return $this->rinc_title;
    }
}
