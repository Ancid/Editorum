<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 16.05.2016
 * Time: 13:08
 */

namespace Editorum\Bundle\Document\Common;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

use Editorum\Bundle\Doctrine\Multilanguage;
use Editorum\Bundle\Document\Country;
use Editorum\Bundle\Document\Region;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableDocument;
use Gedmo\Timestampable\Traits\TimestampableDocument;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AbstractCouncil
 * @package Editorum\Bundle\Document
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
abstract class AbstractCouncil
{
    use TimestampableDocument;
    use SoftDeleteableDocument;
    use Multilanguage;

    /**
     * ID
     * @ODM\Id(type="int", strategy="INCREMENT")
     */
    protected $id;

    /**
     * Страна
     * @Assert\NotNull()
     * @ODM\ReferenceOne(targetDocument="Country", simple=true, name="country_id", nullable=false)
     */
    protected $country;

    /**
     * Регион
     * @ODM\ReferenceOne(targetDocument="Region", simple=true, name="region_id", nullable=true)
     */
    protected $region;

    /**
     * Позиция
     * @Gedmo\SortablePosition()
     * @ODM\Field(type="int")
     */
    protected $order;



    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set country
     *
     * @param Country $country
     * @return self
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * Get country
     *
     * @return \Editorum\Bundle\Document\Country $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set region
     *
     * @param Region $region
     * @return self
     */
    public function setRegion(Region $region = null)
    {
        $this->region = $region;
        return $this;
    }

    /**
     * Get region
     *
     * @return \Editorum\Bundle\Document\Region $region
     */
    public function getRegion()
    {
        return $this->region;
    }


    /**
     * Set order
     *
     * @param $order
     * @return static
     */
    public function setOrder($order)
    {
        $this->order = (int)$order;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }
}
