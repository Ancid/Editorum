<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 28.06.2016
 * Time: 15:36
 */

namespace Editorum\Bundle\Document\Common;

use Editorum\Bundle\Rinc\RincFactory;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use ZipArchive;

trait RincArchieve
{
    /**
     * Генерирует Rinc XML для объекта
     * @return BinaryFileResponse
     * @throws \Exception
     */
    public function createRincArchieve()
    {
        $fs = new Filesystem();
        if (!$fs->exists('temp')) {
            try {
                $fs->mkdir('temp');
            } catch (\ErrorException $e) {
                throw new \Exception($e->getMessage());
            }
        }

        try {
            $rinc_object = RincFactory::build($this);
            $xml = $rinc_object->generateXml();
        } catch (\ErrorException $e) {
            throw new \Exception($e->getMessage());
        }

        $bom = chr(239).chr(187).chr(191);
        $content = iconv('UTF-8', 'UTF-16LE', $bom. $xml);
        $dir = 'temp';
        $zip_name = $rinc_object->getZipTitle() . '.zip';
        $xml_name = $rinc_object->getXmlTitle() . '.xml';
        $zip_file = $dir . DIRECTORY_SEPARATOR . $zip_name;
        $xml_file = $dir . DIRECTORY_SEPARATOR . $xml_name;

        file_put_contents($xml_file, $content);
        file_put_contents($zip_file, '');

        $zip = new ZipArchive();
        if (!$zip->open($zip_file, ZipArchive::CREATE)) {
            exit("Невозможно открыть <$zip_file>\n");
        }
        $zip->addFile($xml_file, $xml_name);

        $response = new BinaryFileResponse($zip_file);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $zip_name);
        $response->headers->set('Content-Type', 'application/zip');
        clearstatcache(false, $zip_file);

        return $response;
    }
}
