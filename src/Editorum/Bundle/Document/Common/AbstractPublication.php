<?php

namespace Editorum\Bundle\Document\Common;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Doctrine\AsuId;
use Editorum\Bundle\Doctrine\Classificators;
use Editorum\Bundle\Doctrine\DatePublished;
use Editorum\Bundle\Doctrine\DoiFields;
use Editorum\Bundle\Doctrine\Multilanguage;
use Editorum\Bundle\Doctrine\ObjectName;
use Editorum\Bundle\Doctrine\PreviouslyPublished;
use Editorum\Bundle\Doctrine\RincUrl;
use Editorum\Bundle\Doctrine\Statuses;
use Editorum\Bundle\Document\Author;
use Editorum\Bundle\Document\FileStorage;
use Editorum\Bundle\Document\Reference;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableDocument;
use Gedmo\Timestampable\Traits\TimestampableDocument;
use ReflectionClass;

/**
 * @ODM\Document(repositoryClass="Editorum\Bundle\Document\Repository\Publication")
 */
abstract class AbstractPublication
{
    const LANG_RU = 'ru';
    const LANG_EN = 'en';
    const LANG_RUS = 'RUS';
    const LANG_ENG = 'ENG';

    const ST_IN_WORK = 100;
    const ST_IS_PUBLISHED = 300;
    const ST_IN_REQUEST = 700;

    const TRANS = [
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'yo',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'j',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'x',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'shh',
        'ь' => '\'',  'ы' => 'y',   'ъ' => '\'\'',
        'э' => 'e\'',   'ю' => 'yu',  'я' => 'ya',
        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'YO',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'J',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'X',   'Ц' => 'C',
        'Ч' => 'CH',  'Ш' => 'SH',  'Щ' => 'SHH',
        'Ь' => '\'',  'Ы' => 'Y\'',   'Ъ' => '\'\'',
        'Э' => 'E\'',   'Ю' => 'YU',  'Я' => 'YA',
    ];

    use TimestampableDocument;
    use SoftDeleteableDocument;
    use Multilanguage;
    use AsuId;
    use DatePublished;
    use RincArchieve;
    use RincUrl;
    use DoiXml;
    use DoiFields;
    use ObjectName;
    use PreviouslyPublished;
    use Statuses;
    use Classificators;

    /** @ODM\Id(type="int", strategy="INCREMENT") */
    protected $id;

    /** @ODM\Field(type="date") */
    protected $date_agree;

    /** @ODM\Field(type="string") */
    protected $depofile;

    /** @ODM\Field(type="string") */
    protected $comment;

    /** @ODM\Field(type="string") */
    protected $text;

    /** @ODM\Field(type="int") */
    protected $author_pages;

    /** @ODM\Field(type="string") */
    protected $lang;

    /** @ODM\Field(type="collection") */
    protected $languages = [ self::LANG_RU ];

    /** @ODM\ReferenceMany(targetDocument="Editorum\Bundle\Document\FileStorage", mappedBy="document", simple=true) */
    protected $files;

    /** @ODM\ReferenceOne() */
    protected $publisher;

    /** @ODM\ReferenceMany(
     *     targetDocument="Editorum\Bundle\Document\Author",
     *     inversedBy="publications",
     *     cascade={"persist"},
     *     simple=true
     *  ) */
    protected $authors;

    /** @ODM\ReferenceMany(
     *     targetDocument="Editorum\Bundle\Document\Reference",
     *     cascade={"persist"},
     *     simple=true
     *  ) */
    protected $references;


    public function __construct()
    {
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Возвращает транслитерированную строку
     * @param string $value
     * @return string
     */
    public static function transliterate($value)
    {
        return strtr($value, self::TRANS);
    }

    public static function getKinds()
    {
        return [
            self::ST_IN_WORK => [
                'class' => 'label-primary',
                'label' => 'В работе',
            ],
            self::ST_IS_PUBLISHED => [
                'class' => 'label-success',
                'label' => 'Опубликован',
            ],
            self::ST_IN_REQUEST => [
                'class' => 'label-default',
                'label' => 'В заявке',
            ],
        ];
    }



    /**
     * Set publisher
     *
     * @param AbstractCorporateEntity $publisher
     * @return self
     */
    public function setPublisher(AbstractCorporateEntity $publisher)
    {
        $this->publisher = $publisher;
        return $this;
    }

    /**
     * Get publisher
     *
     * @return AbstractCorporateEntity $publisher
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateAgree
     *
     * @param date $dateAgree
     * @return self
     */
    public function setDateAgree($dateAgree)
    {
        $this->date_agree = $dateAgree;
        return $this;
    }

    /**
     * Get dateAgree
     *
     * @return date $dateAgree
     */
    public function getDateAgree()
    {
        return $this->date_agree;
    }

    /**
     * Set depofile
     *
     * @param string $depofile
     * @return self
     */
    public function setDepofile($depofile)
    {
        $this->depofile = $depofile;
        return $this;
    }

    /**
     * Get depofile
     *
     * @return string $depofile
     */
    public function getDepofile()
    {
        return $this->depofile;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return self
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * Get comment
     *
     * @return string $comment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return self
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * Get text
     *
     * @return string $text
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set authorPages
     *
     * @param string $authorPages
     * @return self
     */
    public function setAuthorPages($authorPages)
    {
        $this->author_pages = $authorPages;
        return $this;
    }

    /**
     * Get authorPages
     *
     * @return string $authorPages
     */
    public function getAuthorPages()
    {
        return $this->author_pages;
    }

    /**
     * Set lang
     *
     * @param string $lang
     * @return self
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
        return $this;
    }

    /**
     * Get lang
     *
     * @return string $lang
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Set languages
     *
     * @param array $languages
     * @return $this
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;

        $langArray = [
            self::LANG_RU => self::LANG_RUS,
            self::LANG_EN => self::LANG_ENG,
        ];

        if (isset($languages[0])) {
            $this->lang = $langArray[$languages[0]];
        }

        return $this;
    }

    /**
     * Get languages
     *
     * @return array $languages
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * Add file
     *
     * @param FileStorage $file
     */
    public function addFile(FileStorage $file)
    {
        $this->files[] = $file;
    }

    /**
     * Remove file
     *
     * @param FileStorage $file
     */
    public function removeFile(FileStorage $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection $files
     */
    public function getFiles()
    {
        return $this->files;
    }


    /**
     * Add author
     *
     * @param Author $author
     */
    public function addAuthor(Author $author)
    {
        $this->authors[] = $author;
    }

    /**
     * Clear references
     */
    public function clearReferences()
    {
        $this->references = [];

        return $this;
    }

    /**
     * Clear author
     */
    public function clearAuthors()
    {
        $this->authors = [];

        return $this;
    }

    /**
     * Remove author
     *
     * @param Author $author
     */
    public function removeAuthor(Author $author)
    {
        $this->authors->removeElement($author);
    }

    /**
     * Get authors
     *
     * @return \Doctrine\Common\Collections\Collection $authors
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * Add reference
     *
     * @param Reference $reference
     */
    public function addReference(Reference $reference)
    {
        $this->references[] = $reference;
    }

    /**
     * Remove reference
     *
     * @param Reference $reference
     */
    public function removeReference(Reference $reference)
    {
        $this->references->removeElement($reference);
    }

    /**
     * Get references
     *
     * @return \Doctrine\Common\Collections\Collection $references
     */
    public function getReferences()
    {
        return $this->references;
    }

    /**
     * @return string
     */
    public function publicationName()
    {
        $reflection = new ReflectionClass($this);
        return $reflection->getShortName();
    }
}
