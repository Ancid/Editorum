<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 29.04.2016
 * Time: 10:51
 */

namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Doctrine\Multilanguage;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Region
 * @package Editorum\Bundle\Document
 *
 * @ODM\Document(collection="section", repositoryClass="Editorum\Bundle\Document\Repository\Section")
 */

class Section
{
    use Multilanguage;

    /**
     * @ODM\Id(type="int", strategy="INCREMENT")
     */
    protected $id;

    /**
     * Конференция секции
     * @ODM\ReferenceOne(targetDocument="Conference",
     *     inversedBy="sections",
     *     cascade={"persist"},
     *     simple=true
     *     )
     */
    protected $conference;


    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set conference
     *
     * @param Conference $conference
     * @return self
     */
    public function setConference(Conference $conference)
    {
        $this->conference = $conference;
        return $this;
    }

    /**
     * Get conference
     *
     * @return Conference $conference
     */
    public function getConference()
    {
        return $this->conference;
    }
}
