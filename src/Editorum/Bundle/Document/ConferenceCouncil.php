<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 16.05.2016
 * Time: 12:03
 */

namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Document\Common\AbstractCouncil;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class EditorialTeam
 * @package Editorum\Bundle\Document
 *
 * @ODM\Document(collection="council", repositoryClass="Editorum\Bundle\Document\Repository\ConferenceCouncil")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ConferenceCouncil extends AbstractCouncil
{
    /**
     * Конференция
     * @Gedmo\SortableGroup()
     * @ODM\ReferenceOne(targetDocument="Conference", simple=true, name="conference_id")
     */
    protected $conference;

    

    /**
     * Set conference
     *
     * @param \Editorum\Bundle\Document\Conference $conference
     * @return self
     */
    public function setConference(Conference $conference)
    {
        $this->conference = $conference;
        return $this;
    }

    /**
     * Get conference
     *
     * @return \Editorum\Bundle\Document\Conference $conference
     */
    public function getConference()
    {
        return $this->conference;
    }


    /**
     * Set order
     *
     * @param int $order
     * @return self
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * Get order
     *
     * @return int $order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set fio
     *
     * @param string $fio
     * @return $this
     */
    public function setFio($fio)
    {
        $this->fio = $fio;
        return $this;
    }

    /**
     * Get fio
     *
     * @return string $fio
     */
    public function getFio()
    {
        return $this->fio;
    }
}
