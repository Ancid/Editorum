<?php
namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @package Editorum\Bundle\Document
 * @ODM\Document(collection="collection_section")
 */
class CollectionSectionList
{
    /** @ODM\Id(type="int", strategy="INCREMENT") */
    protected $id;

    /** @ODM\ReferenceOne(targetDocument="Section", simple=true) */
    protected $section;

    /**
     * @ODM\ReferenceOne(
     *     targetDocument="Collection",
     *     inversedBy="collection_section_list",
     *     cascade={"persist"},
     *     simple=true,
     * )
     * @Gedmo\SortableGroup()
     */
    protected $collection;

    /**
     * @ODM\Field(type="int")
     * @Gedmo\SortablePosition()
     */
    protected $order;

    
    
    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set section
     *
     * @param Section $section
     * @return self
     */
    public function setSection(Section $section)
    {
        $this->section = $section;
        return $this;
    }

    /**
     * Get section
     *
     * @return Section $section
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set collection
     *
     * @param Collection $collection
     * @return self
     */
    public function setCollection(Collection $collection)
    {
        $this->collection = $collection;
        return $this;
    }

    /**
     * Get collection
     *
     * @return Collection $collection
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * Set order
     *
     * @param int $order
     * @return self
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * Get order
     *
     * @return int $order
     */
    public function getOrder()
    {
        return $this->order;
    }
}
