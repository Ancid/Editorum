<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 12.05.16
 * Time: 13:24
 */

namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Timestampable\Traits\TimestampableDocument;

/**
 * @ODM\Document(collection="doi", repositoryClass="Editorum\Bundle\Document\Repository\Doi")
 */
class Doi
{
    const ST_QUEUE = 100;
    const ST_SEND = 200;
    const ST_PROCESSED = 300;

    use TimestampableDocument;

    /** @ODM\Id(type="int", strategy="INCREMENT", name="_id") */
    protected $id;

    /** @ODM\Field(type="string") */
    protected $doi_batch_id;

    /** @ODM\Field(type="string") */
    protected $file_name;

    /** @ODM\Field(type="string") */
    protected $xml;

    /** @ODM\Field(type="int") */
    protected $status = self::ST_QUEUE;

    /** @ODM\ReferenceOne(nullable=true) */
    protected $document;


    public static function getKinds()
    {
        return [
            self::ST_QUEUE => [
                'class' => 'label-success',
                'label' => 'В очереди',
            ],
            self::ST_SEND => [
                'class' => 'label-primary',
                'label' => 'Отправлен',
            ],
            self::ST_PROCESSED => [
                'class' => 'label-default',
                'label' => 'Присвоен',
            ],
        ];
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set xml
     *
     * @param string $xml
     * @return $this
     */
    public function setXml($xml)
    {
        $this->xml = $xml;
        return $this;
    }

    /**
     * Get xml
     *
     * @return string $xml
     */
    public function getXml()
    {
        return $this->xml;
    }

    /**
     * Set document
     *
     * @param $document
     * @return $this
     */
    public function setDocument($document = null)
    {
        $this->document = $document;
        return $this;
    }

    /**
     * Get document
     *
     * @return $document
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set status
     *
     * @param int $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return int $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set doiBatchId
     *
     * @param string $doiBatchId
     * @return $this
     */
    public function setDoiBatchId($doiBatchId)
    {
        $this->doi_batch_id = $doiBatchId;
        return $this;
    }

    /**
     * Get doiBatchId
     *
     * @return string $doiBatchId
     */
    public function getDoiBatchId()
    {
        return $this->doi_batch_id;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     * @return $this
     */
    public function setFileName($fileName)
    {
        $this->file_name = $fileName;
        return $this;
    }

    /**
     * Get fileName
     *
     * @return string $fileName
     */
    public function getFileName()
    {
        return $this->file_name;
    }
}
