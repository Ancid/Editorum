<?php

namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Doctrine\AsuId;
use Editorum\Bundle\Doctrine\Classificators;
use Editorum\Bundle\Doctrine\DatePublished;
use Editorum\Bundle\Doctrine\DoiFields;
use Editorum\Bundle\Doctrine\Multilanguage;
use Editorum\Bundle\Doctrine\ObjectName;
use Editorum\Bundle\Doctrine\RincUrl;
use Editorum\Bundle\Doctrine\Statuses;
use Editorum\Bundle\Document\Common\AbstractCorporateEntity;
use Editorum\Bundle\Document\Common\DoiXml;
use Editorum\Bundle\Document\Common\RincArchieve;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableDocument;
use Gedmo\Timestampable\Traits\TimestampableDocument;
use Gedmo\Mapping\Annotation as Gedmo;
use Editorum\Bundle\Document\Common\AbstractPublication;

/**
 * @ODM\Document(collection="collection", repositoryClass="Editorum\Bundle\Document\Repository\Collection")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Collection
{
    use TimestampableDocument;
    use SoftDeleteableDocument;
    use Multilanguage;
    use AsuId;
    use RincArchieve;
    use RincUrl;
    use DoiXml;
    use DoiFields;
    use Statuses;
    use Classificators;
    use ObjectName;
    use DatePublished;

    /** @ODM\Id(type="int", strategy="INCREMENT") */
    protected $id;

    /** @ODM\ReferenceOne(targetDocument="User", simple=true) */
    protected $user;

    /** @ODM\Field(type="int") */
    protected $author_pages;

    /** @ODM\Field(type="string") */
    protected $isbn;

    /** @ODM\Field(type="string") */
    protected $cover;

    /** @ODM\Field(type="string") */
    protected $isbn_online;
    
    /** @ODM\Field(type="int") */
    protected $published_page_total;

    /** @ODM\Field(type="date") */
    protected $date_reg_certificate;

    /** @ODM\Field(type="date") */
    protected $date_agree;

    /** @ODM\Field(type="date") */
    protected $date_create;

    /** @ODM\ReferenceOne() */
    protected $publisher;

    /** @ODM\ReferenceMany(
     *     targetDocument="CollectionSectionList",
     *     mappedBy="collection",
     *     cascade={"persist"},
     *     simple=true
     * ) */
    protected $collection_section_list;

    /** @ODM\ReferenceOne(
     *     targetDocument="Conference",
     *     inversedBy="collections",
     *     cascade={"persist"},
     *     simple=true
     * ) */
    protected $conference;

    /** @ODM\ReferenceMany(
     *     targetDocument="ConferenceArticle",
     *     mappedBy="collection",
     *     cascade={"persist"},
     *     simple=true
     * ) */
    protected $articles;
    
    

    public function __construct()
    {
        $this->articles = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * @param Section $section
     * @return array
     */
    public function getArticlesBySection(Section $section)
    {
        $articles = [];
        foreach ($this->getArticles() as $article) {
            if (null != $article->getSection() && $article->getSection()->getId() == $section->getId()) {
                $articles[] = $article;
            }
        }
        return $articles;
    }


    public function publicate()
    {
        $this->setStatus('is_published', 1);
        $this->setDatePublished(new \DateTime());
        $this->setStatus('status', AbstractPublication::ST_IS_PUBLISHED);
        //При публикации сборника всем его статьям статус "Опубликован"
        foreach ($this->getArticles() as $art) {
            /** @var ConferenceArticle $art */
            $art->setStatus('status', Conference::ST_IS_PUBLISHED);
        }
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return User $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set authorPages
     *
     * @param int $authorPages
     * @return $this
     */
    public function setAuthorPages($authorPages)
    {
        $this->author_pages = $authorPages;
        return $this;
    }

    /**
     * Get authorPages
     *
     * @return int $authorPages
     */
    public function getAuthorPages()
    {
        return $this->author_pages;
    }

    /**
     * Set publishedPageTotal
     *
     * @param string $publishedPageTotal
     * @return $this
     */
    public function setPublishedPageTotal($publishedPageTotal)
    {
        $this->published_page_total = $publishedPageTotal;
        return $this;
    }

    /**
     * Get publishedPageTotal
     *
     * @return string $publishedPageTotal
     */
    public function getPublishedPageTotal()
    {
        return $this->published_page_total;
    }

    /**
     * Set dateAgree
     *
     * @param date $dateAgree
     * @return $this
     */
    public function setDateAgree($dateAgree)
    {
        $this->date_agree = $dateAgree;
        return $this;
    }

    /**
     * Get dateAgree
     *
     * @return date $dateAgree
     */
    public function getDateAgree()
    {
        return $this->date_agree;
    }

    /**
     * Set dateCreate
     *
     * @param date $dateCreate
     * @return $this
     */
    public function setDateCreate($dateCreate)
    {
        $this->date_create = $dateCreate;
        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return date $dateCreate
     */
    public function getDateCreate()
    {
        return $this->date_create;
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection $articles
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Set publisher
     *
     * @param AbstractCorporateEntity $publisher
     * @return $this
     */
    public function setPublisher(AbstractCorporateEntity $publisher)
    {
        $this->publisher = $publisher;
        return $this;
    }

    /**
     * Get publisher
     *
     * @return AbstractCorporateEntity $publisher
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * Set conference
     *
     * @param Conference $conference
     * @return $this
     */
    public function setConference(Conference $conference)
    {
        $this->conference = $conference;
        return $this;
    }

    /**
     * Get conference
     *
     * @return Conference $conference
     */
    public function getConference()
    {
        return $this->conference;
    }

    /**
     * Add article
     *
     * @param ConferenceArticle $article
     */
    public function addArticle(ConferenceArticle $article)
    {
        $this->articles[] = $article;
    }

    /**
     * Remove article
     *
     * @param ConferenceArticle $article
     */
    public function removeArticle(ConferenceArticle $article)
    {
        $this->articles->removeElement($article);
    }

    /**
     * Add collectionSectionList
     *
     * @param CollectionSectionList $collectionSectionList
     */
    public function addCollectionSectionList(CollectionSectionList $collectionSectionList)
    {
        $this->collection_section_list[] = $collectionSectionList;
    }

    /**
     * Remove collectionSectionList
     *
     * @param CollectionSectionList $collectionSectionList
     */
    public function removeCollectionSectionList(CollectionSectionList $collectionSectionList)
    {
        $this->collection_section_list->removeElement($collectionSectionList);
    }

    /**
     * Get collectionSectionList
     *
     * @return \Doctrine\Common\Collections\Collection $collectionSectionList
     */
    public function getCollectionSectionList()
    {
        return $this->collection_section_list;
    }

    /**
     * Set isbn
     *
     * @param string $isbn
     * @return $this
     */
    public function setIsbn($isbn)
    {
        $this->isbn = $isbn;
        return $this;
    }

    /**
     * Get isbn
     *
     * @return string $isbn
     */
    public function getIsbn()
    {
        return $this->isbn;
    }

    /**
     * @return string
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * @param string $cover
     *
     * @return Collection
     */
    public function setCover($cover)
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * Set isbnOnline
     *
     * @param string $isbnOnline
     * @return $this
     */
    public function setIsbnOnline($isbnOnline)
    {
        $this->isbn_online = $isbnOnline;
        return $this;
    }

    /**
     * Get isbnOnline
     *
     * @return string $isbnOnline
     */
    public function getIsbnOnline()
    {
        return $this->isbn_online;
    }

    /**
     * Set dateRegCertificate
     *
     * @param date $dateRegCertificate
     * @return self
     */
    public function setDateRegCertificate($dateRegCertificate)
    {
        $this->date_reg_certificate = $dateRegCertificate;
        return $this;
    }

    /**
     * Get dateRegCertificate
     *
     * @return date $dateRegCertificate
     */
    public function getDateRegCertificate()
    {
        return $this->date_reg_certificate;
    }
}
