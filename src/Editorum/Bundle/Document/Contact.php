<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 12.05.16
 * Time: 13:24
 */

namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Bundle\MongoDBBundle\Validator\Constraints\Unique as UniqueDocument;

/**
 * @MongoDB\Document(collection="contact", repositoryClass="Editorum\Bundle\Document\Repository\Contact")
 * @UniqueDocument(
 *     fields={"type", "value"},
 *     repositoryMethod="findUniqueBy"
 * )
 */
class Contact
{
    /** @MongoDB\Id(type="int", strategy="INCREMENT", name="_id") */
    protected $id;

    /**
     * Type of the contact data
     * @Assert\NotNull()
     * @MongoDB\ReferenceOne(targetDocument="ContactType", simple=true, name="type_id", nullable=false)
     */
    protected $type;

    /**
     * User ID
     * @Assert\NotNull()
     * @MongoDB\ReferenceOne(
     *     targetDocument="Editorum\Bundle\Document\User",
     *     simple=true, name="user_id", nullable=false
     * )
     */
    protected $user;

    /**
     * Value of the contact data
     * @MongoDB\Field(type="string")
     * @Assert\NotNull()
     */
    protected $value;

    /**
     * Flag of the default contact data
     * @MongoDB\Field(type="boolean")
     */
    protected $is_default;

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param ContactType $type
     * @return self
     */
    public function setType(ContactType $type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return ContactType $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return self
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return User $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set value
     *
     * @param $value
     * @return self
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Get value
     *
     * @return string $value
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set isDefault
     *
     * @param bool $isDefault
     * @return self
     */
    public function setIsDefault($isDefault)
    {
        $this->is_default = $isDefault;
        return $this;
    }

    /**
     * Get isDefault
     *
     * @return bool $isDefault
     */
    public function getIsDefault()
    {
        return $this->is_default;
    }
    
}
