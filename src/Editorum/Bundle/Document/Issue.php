<?php
namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Doctrine\AsuId;
use Editorum\Bundle\Doctrine\Classificators;
use Editorum\Bundle\Doctrine\DatePublished;
use Editorum\Bundle\Doctrine\DoiFields;
use Editorum\Bundle\Doctrine\ObjectName;
use Editorum\Bundle\Doctrine\RincUrl;
use Editorum\Bundle\Doctrine\Statuses;
use Editorum\Bundle\Document\Common\AbstractCorporateEntity;
use Editorum\Bundle\Document\Common\DoiXml;
use Editorum\Bundle\Document\Common\RincArchieve;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableDocument;
use Gedmo\Timestampable\Traits\TimestampableDocument;
use Editorum\Bundle\Document\Journal as JournalDocument;

/**
 * Class Issue
 * @package Editorum\Bundle\Document
 *
 * @ODM\Document(collection="issue", repositoryClass="Editorum\Bundle\Document\Repository\Issue")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Issue
{
    use TimestampableDocument;
    use SoftDeleteableDocument;
    use AsuId;
    use DatePublished;
    use RincUrl;
    use RincArchieve;
    use DoiFields;
    use DoiXml;
    use Statuses;
    use Classificators;
    use ObjectName;

    /** @ODM\Id(type="int", strategy="INCREMENT") */
    protected $id;

    /** @ODM\Field(type="string") */
    protected $volume;

    /** @ODM\Field(type="int") */
    protected $issue;

    /** @ODM\Field(type="int") */
    protected $page_count;
    
    /** @ODM\Field(type="int") */
    protected $type_access;
    
    /** @ODM\Field(type="string") */
    protected $part;

    /** @ODM\ReferenceOne(targetDocument="Journal", simple=true, cascade={"persist"}) */
    protected $journal;

    /** @ODM\ReferenceOne() */
    protected $publisher;

    /** @ODM\ReferenceMany(
     *     targetDocument="IssueRubricList",
     *     mappedBy="issue",
     *     cascade={"persist"},
     *     simple=true
     * ) */
    protected $issue_rubric_list;

    /** @ODM\ReferenceMany(
     *     targetDocument="Article",
     *     mappedBy="issue",
     *     cascade={"persist"},
     *     simple=true
     * ) */
    protected $articles;



    public function __construct()
    {
        $this->issue_rubric_list = new \Doctrine\Common\Collections\ArrayCollection();
        $this->articles = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Метод публикации номера
     */
    public function publicate()
    {
        $this->setDatePublished(new \DateTime());
        $this->setStatus('is_published', 1);
        $this->setStatus('status', JournalDocument::ST_IS_PUBLISHED);
        $this->getJournal()->setStatus('status', JournalDocument::ST_IS_PUBLISHING);
        //При публикации номера всем его статьям статус "Опубликован"
        foreach ($this->getArticles() as $art) {
            /** @var Article $art */
            $art->setStatus('status', JournalDocument::ST_IS_PUBLISHED);
        }
    }

    /**
     * @param $rubric
     * @return array
     */
    public function getArticlesByRubric($rubric)
    {
        $articles = [];
        foreach ($this->getArticles() as $article) {
            if ($article->getRubric()->getId() == $rubric->getId()) {
                $articles[] = $article;
            }
        }
        return $articles;
    }

    /**
     * Set id
     *
     * @param int $id
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set volume
     *
     * @param string $volume
     * @return self
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;
        return $this;
    }

    /**
     * Get volume
     *
     * @return string $volume
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * Set issue
     *
     * @param int $issue
     * @return self
     */
    public function setIssue($issue)
    {
        $this->issue = $issue;
        return $this;
    }

    /**
     * Get issue
     *
     * @return int $issue
     */
    public function getIssue()
    {
        return $this->issue;
    }

    /**
     * Set journal
     *
     * @param Journal $journal
     * @return self
     */
    public function setJournal(Journal $journal)
    {
        $this->journal = $journal;
        return $this;
    }

    /**
     * Get journal
     *
     * @return Journal $journal
     */
    public function getJournal()
    {
        return $this->journal;
    }

    /**
     * Set pageCount
     *
     * @param int $pageCount
     * @return self
     */
    public function setPageCount($pageCount)
    {
        $this->page_count = $pageCount;
        return $this;
    }

    /**
     * Get pageCount
     *
     * @return int $pageCount
     */
    public function getPageCount()
    {
        return $this->page_count;
    }

    /**
     * Set typeAccess
     *
     * @param int $typeAccess
     * @return self
     */
    public function setTypeAccess($typeAccess)
    {
        $this->type_access = $typeAccess;
        return $this;
    }

    /**
     * Get typeAccess
     *
     * @return int $typeAccess
     */
    public function getTypeAccess()
    {
        return $this->type_access;
    }

    /**
     * Set part
     *
     * @param string $part
     * @return self
     */
    public function setPart($part)
    {
        $this->part = $part;
        return $this;
    }

    /**
     * Get part
     *
     * @return string $part
     */
    public function getPart()
    {
        return $this->part;
    }

    /**
     * Set publisher
     *
     * @param AbstractCorporateEntity $publisher
     * @return $this
     */
    public function setPublisher(AbstractCorporateEntity $publisher)
    {
        $this->publisher = $publisher;
        return $this;
    }

    /**
     * Get publisher
     *
     * @return AbstractCorporateEntity $publisher
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * Add article
     *
     * @param Article $article
     */
    public function addArticle(Article $article)
    {
        $this->articles[] = $article;
    }

    /**
     * Remove article
     *
     * @param Article $article
     */
    public function removeArticle(Article $article)
    {
        $this->articles->removeElement($article);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection $articles
     */
    public function getArticles()
    {
        return $this->articles;
    }


    /**
     * Add issuerubriclist
     *
     * @param IssueRubricList $issuerubriclist
     */
    public function addIssuerubriclist(IssueRubricList $issuerubriclist)
    {
        $this->issue_rubric_list[] = $issuerubriclist;
    }

    /**
     * Remove issuerubriclist
     *
     * @param IssueRubricList $issuerubriclist
     */
    public function removeIssuerubriclist(IssueRubricList $issuerubriclist)
    {
        $this->issue_rubric_list->removeElement($issuerubriclist);
    }

    /**
     * Get issuerubriclist
     *
     * @return \Doctrine\Common\Collections\Collection $issuerubriclist
     */
    public function getIssuerubriclist()
    {
        return $this->issue_rubric_list;
    }
}
