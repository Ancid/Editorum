<?php
namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Document\Common\AbstractCorporateEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\Document(collection="research")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
*/
class Research extends AbstractCorporateEntity
{
    
    /** @ODM\Id(type="int", strategy="INCREMENT") */
    protected $id;

    /** @ODM\ReferenceOne(targetDocument="Country", simple=true) */
    protected $country;



    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    

    /**
     * Set country
     *
     * @param Country $country
     * @return $this
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * Get country
     *
     * @return Country $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set doi
     *
     * @param string $doi
     * @return $this
     */
    public function setDoi($doi)
    {
        $this->doi = $doi;
        return $this;
    }

    /**
     * Get doi
     *
     * @return string $doi
     */
    public function getDoi()
    {
        return $this->doi;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set doiPrefix
     *
     * @param string $doiPrefix
     * @return $this
     */
    public function setDoiPrefix($doiPrefix)
    {
        $this->doi_prefix = $doiPrefix;
        return $this;
    }

    /**
     * Get doiPrefix
     *
     * @return string $doiPrefix
     */
    public function getDoiPrefix()
    {
        return $this->doi_prefix;
    }

    /**
     * Set doiLimit
     *
     * @param int $doiLimit
     * @return $this
     */
    public function setDoiLimit($doiLimit)
    {
        $this->doi_limit = $doiLimit;
        return $this;
    }

    /**
     * Get doiLimit
     *
     * @return int $doiLimit
     */
    public function getDoiLimit()
    {
        return $this->doi_limit;
    }

    /**
     * Set doiLogin
     *
     * @param string $doiLogin
     * @return $this
     */
    public function setDoiLogin($doiLogin)
    {
        $this->doi_login = $doiLogin;
        return $this;
    }

    /**
     * Get doiLogin
     *
     * @return string $doiLogin
     */
    public function getDoiLogin()
    {
        return $this->doi_login;
    }

    /**
     * Set doiPass
     *
     * @param string $doiPass
     * @return $this
     */
    public function setDoiPass($doiPass)
    {
        $this->doi_pass = $doiPass;
        return $this;
    }

    /**
     * Get doiPass
     *
     * @return string $doiPass
     */
    public function getDoiPass()
    {
        return $this->doi_pass;
    }
}
