<?php
namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Doctrine\Multilanguage;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableDocument;
use Gedmo\Timestampable\Traits\TimestampableDocument;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ListStorage
 *
 * @package Editorum\Bundle\Document
 * @ODM\Document(collection="list_storage")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ListStorage
{
    use SoftDeleteableDocument;
    use TimestampableDocument;
    use Multilanguage;

    /**
     * @ODM\Id(type="int", strategy="INCREMENT")
     */
    private $id;

    /**
     * @ODM\Field(type="string", nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Gedmo\SortableGroup()
     */
    private $listName;

    /**
     * @ODM\Field(type="int")
     * @Gedmo\SortablePosition()
     */
    private $position;

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set listName
     *
     * @param string $listName
     * @return $this
     */
    public function setListName($listName)
    {
        $this->listName = $listName;
        return $this;
    }

    /**
     * Get listName
     *
     * @return string $listName
     */
    public function getListName()
    {
        return $this->listName;
    }

    /**
     * Set position
     *
     * @param int $position
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * Get position
     *
     * @return int $position
     */
    public function getPosition()
    {
        return $this->position;
    }
}
