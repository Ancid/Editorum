<?php

namespace Editorum\Bundle\Document\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Editorum\Bundle\Document\Issue as IssueDocument;
use Editorum\Bundle\Document\IssueRubricList;
use Editorum\Bundle\Document\Rubric;

/**
 * Issue repository
 */
class Issue extends DocumentRepository
{
    /**
     * @param IssueDocument $issue
     * @param Rubric $rubric
     * @return IssueRubricList
     */
    public function addRubric(IssueDocument $issue, Rubric $rubric)
    {
        $qb = $this->getDocumentManager()
            ->getRepository('AsuBundle:IssueRubricList')
            ->createQueryBuilder();

        $qb
            ->addAnd($qb->expr()->field('rubric')->references($rubric))
            ->addAnd($qb->expr()->field('issue')->references($issue));

        $issueRubricList = $qb->getQuery()->getSingleResult();

        if (!empty($issueRubricList)) {
            return $issueRubricList;
        }

        $max_order = 0;
        foreach ($issue->getIssuerubriclist() as $rubriclist) {
            if ($max_order < $rubriclist->getOrder()) {
                $max_order = $rubriclist->getOrder();
            }
        }

        $issueRubricList = new IssueRubricList();
        $issueRubricList
            ->setOrder($max_order + 1)
            ->setRubric($rubric)
            ->setIssue($issue)
        ;
        $this->getDocumentManager()->persist($issueRubricList);
        $this->getDocumentManager()->flush();

        return $issueRubricList;
    }


    /**
     * @param IssueDocument $issue
     * @param Rubric $rubric
     * @return bool
     */
    public function removeRubric(IssueDocument $issue, Rubric $rubric)
    {
        $qb = $this->getDocumentManager()
            ->getRepository('AsuBundle:IssueRubricList')
            ->createQueryBuilder();

        $issueRubricList = $qb
            ->addAnd($qb->expr()->field('rubric')->references($rubric))
            ->addAnd($qb->expr()->field('issue')->references($issue))
            ->getQuery()->getSingleResult();

        if (empty($issueRubricList)) {
            return false;
        }

        $this->getDocumentManager()->remove($issueRubricList);
        $this->getDocumentManager()->flush();
        
        return true;
    }


    /**
     * @param IssueDocument $issue
     * @param string $order
     * @return array|\Editorum\Bundle\Document\IssueRubricList[]
     */
    public function getOrderRubricList(IssueDocument $issue, $order = 'ASC')
    {
        $issue_ribric_list = $this->getDocumentManager()
            ->getRepository('AsuBundle:IssueRubricList')
            ->findBy([
                'issue' => $issue->getId()
            ], [
                'order' => $order
            ]);

        return $issue_ribric_list;
    }

    public function getArticlesByRubric($rubric_id, $journal_id)
    {
        $articles = $this->getDocumentManager()->getRepository('AsuBundle:Article')->createQueryBuilder()
            ->field('rubric')->equals($rubric_id)
            ->field('journal')->equals($journal_id);

        return $articles->getQuery()->execute();
    }


    /**
     * @ToDo Метод-костыль для поиска по статусу любого типа (bool|int)
     * @ToDo пофиксить в форме или в fixtures
     *
     * @param string $status
     * @param bool $value
     * @param int $limit
     * @return \Doctrine\ODM\MongoDB\Query\Builder
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function getByStatus($status = 'is_show_nauka', $value = true, $limit = 5)
    {
        $field = 'statuses.'.$status;
        $qb = $this->createQueryBuilder();
        $qb->addAnd(
            $qb->expr()
                ->addOr($qb->expr()->field($field)->equals($value))
                ->addOr($qb->expr()->field($field)->equals((int)$value))
        )
        ->limit((int)$limit);

        return $qb->getQuery()->execute();
    }
}
