<?php
/**
 * Created by PhpStorm.
 * User: vgfilippov
 * Date: 16.06.16
 * Time: 18:37
 */

namespace Editorum\Bundle\Document\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class IssueRepository extends DocumentRepository
{
    public function getArticlesByRubric($rubric_id, $journal_id)
    {
        $articles = $this->getDocumentManager()->getRepository('AsuBundle:Article')->createQueryBuilder()
            ->field('rubric')->equals($rubric_id)
            ->field('journal')->equals($journal_id);

        return $articles->getQuery()->execute();
    }
}