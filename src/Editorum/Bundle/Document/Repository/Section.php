<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 12.05.2016
 * Time: 17:36
 */

namespace Editorum\Bundle\Document\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class Section extends DocumentRepository
{
    /**
     * @param $collection_id
     * @param $section_id
     * @return mixed
     */
    public function getArticlesByCollection($collection_id, $section_id)
    {
        $articles = $this->dm->getRepository('AsuBundle:Publication')->createQueryBuilder()
            ->field('section.$id')->equals($section_id)
            ->field('collection.$id')->equals($collection_id)
            ->getQuery()->execute();

        return  $articles;
    }
}