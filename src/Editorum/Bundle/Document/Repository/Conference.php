<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 28.06.2016
 * Time: 16:05
 */

namespace Editorum\Bundle\Document\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Editorum\Bundle\Document\Collection as CollectionDocument;
use Editorum\Bundle\Document\Conference as ConferenceDocument;
use Editorum\Bundle\Document\ConferenceArticle;

class Conference extends DocumentRepository
{
    /**
     * Get available articles of journal
     * @param ConferenceDocument|null $conference
     * @return mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function getAvailableArticles(ConferenceDocument $conference = null)
    {
        if (null == $conference) {
            return [];
        }
        $qb = $this->getDocumentManager()->getRepository('AsuBundle:ConferenceArticle')->createQueryBuilder();
        $qb
            ->addAnd(
                $qb->expr()
                    ->addOr($qb->expr()->field('collection')->equals(null))
                    ->addOr($qb->expr()->field('collection')->exists(false))
            )
            ->addAnd($qb->expr()->field('conference')->references($conference))
        ;

        return $qb->getQuery()->execute();
    }


    /**
     * Обновляем издателя у всех связанных с конференцией сущностей
     * @param ConferenceDocument $conference
     * @return bool
     */
    public function updatePublisher(ConferenceDocument $conference)
    {
        if (null !== $conference->getCollections()) {
            /** @var CollectionDocument $coll */
            foreach ($conference->getCollections() as $coll) {
                if ($coll->getPublisher() !== $conference->getPublisher()) {
                    $coll->setPublisher($conference->getPublisher());
                }
            }
        }
        if (null != $conference->getArticles()) {
            /** @var ConferenceArticle $art */
            foreach ($conference->getArticles() as $art) {
                if ($art->getPublisher() !== $conference->getPublisher()) {
                    $art->setPublisher($conference->getPublisher());
                }
            }
        }

        return true;
    }
}
