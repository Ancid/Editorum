<?php
/**
 * Created by PhpStorm.
 * User: vgfilippov
 * Date: 12.05.16
 * Time: 19:30
 */

namespace Editorum\Bundle\Document\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Editorum\Bundle\Document\CollectionSectionList;
use Editorum\Bundle\Document\Section as SectionDocument;
use Editorum\Bundle\Document\Collection as CollectionDocument;

class Collection extends DocumentRepository
{
    /**
     * Пока используется аналог функции в самом объекте
     * @param $section
     * @return mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function getArticlesBySection(SectionDocument $section)
    {
        $articles = $this->getDocumentManager()->getRepository('AsuBundle:ConferenceArticle')->createQueryBuilder()
            ->field('section')->references($section)
        ;

        return $articles->getQuery()->execute();
    }


    /**
     * @param CollectionDocument $collection
     * @param SectionDocument $section
     * @return array|object
     */
    public function addSection(CollectionDocument $collection, SectionDocument $section)
    {
        $qb = $this->getDocumentManager()
            ->getRepository('AsuBundle:CollectionSectionList')
            ->createQueryBuilder();

        $qb
            ->addAnd($qb->expr()->field('section')->references($section))
            ->addAnd($qb->expr()->field('collection')->references($collection));

        $collectionSectionList = $qb->getQuery()->getSingleResult();

        if (!empty($collectionSectionList)) {
            return $collectionSectionList;
        }

        $max_order = 0;
        if (null != $collection->getCollectionSectionList()) {
            foreach ($collection->getCollectionSectionList() as $sectionlist) {
                if ($max_order < $sectionlist->getOrder()) {
                    $max_order = $sectionlist->getOrder();
                }
            }
        }

        $collectionSectionList = new CollectionSectionList();
        $collectionSectionList
            ->setOrder($max_order + 1)
            ->setSection($section)
            ->setCollection($collection)
        ;
        $this->getDocumentManager()->persist($collectionSectionList);
        $this->getDocumentManager()->flush();

        return $collectionSectionList;
    }


    /**
     * @param CollectionDocument $collection
     * @param SectionDocument $section
     * @return bool
     */
    public function removeSection(CollectionDocument $collection, SectionDocument $section)
    {
        $qb = $this->getDocumentManager()
            ->getRepository('AsuBundle:CollectionSectionList')
            ->createQueryBuilder();

        $collSectionList = $qb
            ->addAnd($qb->expr()->field('section')->references($section))
            ->addAnd($qb->expr()->field('collection')->references($collection))
            ->getQuery()->getSingleResult();

        if (empty($collSectionList)) {
            return false;
        }

        $this->getDocumentManager()->remove($collSectionList);
        $this->getDocumentManager()->flush();

        return true;
    }


    /**
     * @param CollectionDocument $collection
     * @param string $order
     * @return array|\Editorum\Bundle\Document\CollectionSectionList[]
     */
    public function getOrderSectionList(CollectionDocument $collection, $order = 'ASC')
    {
        $collection_section_list = $this->getDocumentManager()
            ->getRepository('AsuBundle:CollectionSectionList')
            ->findBy([
                'collection' => $collection->getId()
            ], [
                'order' => $order
            ]);

        return $collection_section_list;
    }
}
