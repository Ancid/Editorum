<?php
/**
 * Created by PhpStorm.
 * User: vgfilippov
 * Date: 16.06.16
 * Time: 18:37
 */

namespace Editorum\Bundle\Document\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Editorum\Bundle\Document\Author as AuthorDocument;

class Author extends DocumentRepository
{
    /**
     * @param AuthorDocument $author
     * @return mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function getArticles(AuthorDocument $author)
    {
        $qb = $this->getDocumentManager()->getRepository('AsuBundle:Article')->createQueryBuilder()
            ->field('authors')->equals($author->getId());

        return $qb->getQuery()->execute();
    }


    /**
     * @param AuthorDocument $author
     * @return mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function getTextbooks(AuthorDocument $author)
    {
        $qb = $this->getDocumentManager()->getRepository('AsuBundle:Textbook')->createQueryBuilder()
            ->field('authors')->equals($author->getId());

        return $qb->getQuery()->execute();
    }


    /**
     * @param AuthorDocument $author
     * @return mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function getMonographys(AuthorDocument $author)
    {
        $qb = $this->getDocumentManager()->getRepository('AsuBundle:Monography')->createQueryBuilder()
            ->field('authors')->equals($author->getId());

        return $qb->getQuery()->execute();
    }

    public function getAllPublications(AuthorDocument $author)
    {
        return [
            'articles'    => $this->getArticles($author),
            'monographys' => $this->getMonographys($author),
            'textbooks'   => $this->getTextbooks($author),
        ];
    }
}
