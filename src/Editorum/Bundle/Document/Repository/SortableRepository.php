<?php
namespace Editorum\Bundle\Document\Repository;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\DocumentRepository;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadata;
use Doctrine\ODM\MongoDB\UnitOfWork;
use Gedmo\Exception\InvalidMappingException;
use Gedmo\Sortable\SortableListener;
use InvalidArgumentException;

/**
 * Адаптация репозитория сортировки gedmo для работы с ODM
 * Class SortableRepository
 * @package Editorum\Bundle\Document\Repository
 */
abstract class SortableRepository extends DocumentRepository
{
    /**
     * Sortable listener on event manager
     *
     * @var SortableListener
     */
    protected $listener = null;

    protected $config = null;
    protected $meta = null;

    /**
     * SortableRepository constructor.
     * @param DocumentManager $dm
     * @param UnitOfWork $uow
     * @param ClassMetadata $class
     */
    public function __construct(DocumentManager $dm, UnitOfWork $uow, ClassMetadata $class)
    {
        parent::__construct($dm, $uow, $class);
        $sortableListener = null;
        foreach ($dm->getEventManager()->getListeners() as $event => $listeners) {
            foreach ($listeners as $hash => $listener) {
                if ($listener instanceof SortableListener) {
                    $sortableListener = $listener;
                    break;
                }
            }
            if ($sortableListener) {
                break;
            }
        }

        if (is_null($sortableListener)) {
            throw new InvalidMappingException('This repository can be attached only to ODM sortable listener');
        }

        $this->listener = $sortableListener;
        $this->meta = $this->getClassMetadata();
        $this->config = $this->listener->getConfiguration($this->dm, $this->meta->name);
    }

    /**
     * @param array $groupValues
     * @return \Doctrine\ODM\MongoDB\Query\Query
     */
    public function getBySortableGroupsQuery(array $groupValues = array())
    {
        return $this->getBySortableGroupsQueryBuilder($groupValues)->getQuery();
    }

    /**
     * @param array $groupValues
     * @return \Doctrine\ODM\MongoDB\Query\Builder
     */
    public function getBySortableGroupsQueryBuilder(array $groupValues = array())
    {
        $groups = isset($this->config['groups']) ?
            array_flip($this->config['groups']) : [];
        foreach ($groupValues as $name => $value) {
            if (!in_array($name, $this->config['groups'])) {
                throw new InvalidArgumentException(
                    'Sortable group "'.$name.'" is not defined in Document '.$this->meta->name
                );
            }
            unset($groups[$name]);
        }
        if (count($groups) > 0) {
            throw new InvalidArgumentException(
                'You need to specify values for the following groups to select by sortable groups: '
                .implode(", ", array_keys($groups))
            );
        }

        $qb = $this->createQueryBuilder();
        $qb->sort($this->config['position'], 'ASC');
        foreach ($groupValues as $group => $value) {
            $qb->field($group)->equals($value);
        }

        return $qb;
    }

    /**
     * @param array $groupValues
     * @return mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function getBySortableGroups(array $groupValues = array())
    {
        $query = $this->getBySortableGroupsQuery($groupValues);

        return $query->execute();
    }
}
