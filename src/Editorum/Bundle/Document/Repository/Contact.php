<?php
namespace Editorum\Bundle\Document\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class Contact extends DocumentRepository
{
    /**
     * Find Contact record by type and eq value
     *
     * @param $criteria
     * @return null|mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function findUniqueBy($criteria)
    {
        $qb = $this->createQueryBuilder();

        $result = $qb->find()
            ->field('type')->references($criteria['type'])
            ->field('value')->equals($criteria['value'])
            ->getQuery();

        return $result->execute();
    }

}
