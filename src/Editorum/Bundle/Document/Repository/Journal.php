<?php

namespace Editorum\Bundle\Document\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Editorum\Bundle\Document\Journal as JournalDocument;
use Editorum\Bundle\Document\Issue as IssueDocument;
use Editorum\Bundle\Document\Rubric;
use Editorum\Bundle\Document\Article;

/**
 * Journal repository
 */
class Journal extends DocumentRepository
{
    /**
     * @param JournalDocument $journal
     * @return \Doctrine\ODM\MongoDB\Query\Builder
     */
    public function getAvailableArticlesQuery(JournalDocument $journal)
    {
        $qb = $this->dm->getRepository('AsuBundle:Article')->createQueryBuilder();
        $qb
            ->addAnd(
                $qb->expr()
                    ->addOr($qb->expr()->field('issue')->equals(null))
                    ->addOr($qb->expr()->field('issue')->exists(false))
            )
            ->addAnd($qb->expr()->field('journal')->references($journal))
        ;

        return $qb;
    }
    /**
     * @param JournalDocument $journal
     * @return \Doctrine\ODM\MongoDB\Query\Builder
     */
    public function getAvailableIssues(JournalDocument $journal)
    {
        $qb = $this->dm->getRepository('AsuBundle:Issue')->createQueryBuilder();
        $qb
            ->addAnd($qb->expr()->field('journal')->references($journal))
            ->addAnd($qb->expr()->field('statuses.status')->notEqual(JournalDocument::ST_IS_PUBLISHED))
        ;

        return $qb->getQuery()->execute();
    }


    /**
     * Get available articles of journal
     * @param JournalDocument $journal
     * @return mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function getAvailableArticles(JournalDocument $journal)
    {
        return $this->getAvailableArticlesQuery($journal)->getQuery()->execute();
    }


    /**
     * Add default empty rubric
     * @param JournalDocument $journal
     * @return Rubric
     */
    public function addEmptyRubric(JournalDocument $journal)
    {
        $order = 0;
        foreach ($journal->getRubrics() as $rubric) {
            if (Rubric::EMPTY_RUBRIC_RU == $rubric->getRu()['title']) {
                return $rubric;
            }
            $order++;
        }

        $new_rubric = new Rubric();
        $new_rubric
            ->setRu(['title' => Rubric::EMPTY_RUBRIC_RU])
            ->setEn(['title' => Rubric::EMPTY_RUBRIC_EN])
        ;
        $journal->addRubric($new_rubric);
        $this->dm->persist($new_rubric);
        $this->dm->flush();


        return $new_rubric;
    }


    /**
     * Has journal empty rubric or not
     * @param JournalDocument $journal
     * @return bool
     */
    public function hasEmptyRubric(JournalDocument $journal)
    {
        foreach ($journal->getRubrics() as $rubric) {
            if (JournalDocument::EMPTY_RUBRIC_RU == $rubric->getRu()['title']) {
                return true;
            }
        }
        return false;
    }


    /**
     * @ToDo Метод-костыль для поиска по статусу любого типа (bool|int)
     * @ToDo пофиксить в форме или в fixtures
     *
     * @param string $status
     * @param bool $value
     * @return \Doctrine\ODM\MongoDB\Query\Builder
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function getByStatus($status = 'is_show_nauka', $value = true)
    {
        $field = 'statuses.'.$status;
        $qb = $this->createQueryBuilder();
        $qb->addAnd(
            $qb->expr()
                ->addOr($qb->expr()->field($field)->equals($value))
                ->addOr($qb->expr()->field($field)->equals((int)$value))
        );

        return $qb->getQuery()->execute();
    }


    /**
     * Обновляем издателя у всех связанных с журналом сущностей
     * @param JournalDocument $journal
     * @return bool
     */
    public function updatePublisher(JournalDocument $journal)
    {
        if (null !== $journal->getIssues()) {
            /** @var IssueDocument $issue */
            foreach ($journal->getIssues() as $issue) {
                if ($issue->getPublisher() !== $journal->getPublisher()) {
                    $issue->setPublisher($journal->getPublisher());
                }
            }
        }
        if (null != $journal->getArticles()) {
            /** @var Article $art */
            foreach ($journal->getArticles() as $art) {
                if ($art->getPublisher() !== $journal->getPublisher()) {
                    $art->setPublisher($journal->getPublisher());
                }
            }
        }

        return true;
    }
}
