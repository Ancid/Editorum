<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 12.05.16
 * Time: 13:40
 */

namespace Editorum\Bundle\Document\Repository;


use Doctrine\ODM\MongoDB\DocumentRepository;

class ContactType extends DocumentRepository
{
    /**
     * @param $criteria
     * @return array
     */
    public function findUniqueBy($criteria)
    {
        return $this->findBy([
            'code' => $criteria['code'],
        ]);
    }

}
