<?php
/**
 * Created by PhpStorm.
 * User: vgfilippov
 * Date: 31.05.16
 * Time: 14:09
 */

namespace Editorum\Bundle\Document\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Editorum\Bundle\Document\Publication as PublicationEntity;

class Publication extends DocumentRepository
{
    /**
     * @param $from
     * @param $to
     */
    public function inheritClassifs($from, $to)
    {
        $to->setGrnti($from->getGrnti());
        $to->setUdk($from->getUdk());
        $to->setBbk($from->getBbk());
        $to->setTbk($from->getTbk());
        $to->setOkso($from->getOkso());
    }
}
