<?php

namespace Editorum\Bundle\Document\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Editorum\Bundle\Document\Doi as DoiDocument;

/**
 * Doi repository
 */
class Doi extends DocumentRepository
{
    /**
     * Find available DOI request
     * @param $document
     * @return mixed
     */
    public function findAvailableDoi($document)
    {
        $qb = $this->createQueryBuilder();
        $qb
            ->addAnd(
                $qb->expr()->addOr($qb->expr()->field('status')->notEqual(DoiDocument::ST_QUEUE))
            )
            ->addAnd($qb->expr()->field('document')->references($document))
            ->limit(1)
        ;

        return $qb->getQuery()->execute();
    }
}
