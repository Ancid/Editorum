<?php

namespace Editorum\Bundle\Document\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Editorum\Bundle\Document\Reference as ReferenceDocument;

/**
 * Reference repository
 */
class Reference extends DocumentRepository
{
    /**
     * @param $data
     * @param $publication
     * @param $validator
     */
    public function addReferences($data, $publication, $validator)
    {
        $reference = [];
        $reference['ru'] = explode("\n", str_replace("\r", '', $data['list_ru']));
        $reference['en'] = explode("\n", str_replace("\r", '', $data['list_en']));
        $tr = [
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'yo',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'j',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'x',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'shh',
            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'\'',
            'э' => 'e\'',   'ю' => 'yu',  'я' => 'ya',
            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'YO',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'J',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'X',   'Ц' => 'C',
            'Ч' => 'CH',  'Ш' => 'SH',  'Щ' => 'SHH',
            'Ь' => '\'',  'Ы' => 'Y\'',   'Ъ' => '\'\'',
            'Э' => 'E\'',   'Ю' => 'YU',  'Я' => 'YA',
        ];

        // парсим строки по очереди для совмещения языка по позиции и записываем результат в $array
        $array = [];
        foreach ($reference['ru'] as $line) {
            if (preg_match('/^(\d+)\. (.*)/', $line, $match)) {
                $array[$match[1]]['ru'] = $match[2];
            }
        }

        foreach ($reference['en'] as $line) {
            if (preg_match('/^(\d+)\. (.*)/', $line, $match)) {
                $array[$match[1]]['en'] = $match[2];
            }
        }

        $dataCount = count($array);
        if ($dataCount > 0) {
            $haveErrors = false;

            $referenceCount = iterator_count($publication->getReferences());

            $i = 1;
            foreach ($array as $key => $value) {
                // если записи есть, записываем в конец списка
                if ($referenceCount > 0) {
                    $key = $referenceCount + $i;
                }

                $ref = new ReferenceDocument();
                $ref->setPosition($key);

                if (isset($value['ru'])) {
                    $ref->setRu([
                        'text' => $value['ru']
                    ]);
                }

                if (isset($value['en'])) {
                    $ref->setEn([
                        'text' => $value['en']
                    ]);
                } else {
                    $ref->setEn([
                        'text' => strtr($value['ru'], $tr)
                    ]);
                }

                $errors = $validator->validate($ref);

                if (count($errors) > 0) {
                    $haveErrors = true;
                    $session = $this->get('session');
                    foreach ($errors as $error) {
                        $session->getFlashBag()->add('error', $error->getMessage());
                    }
                } else {
                    $publication->addReference($ref);
                    $this->getDocumentManager()->persist($ref);
                    $this->getDocumentManager()->flush();
                }

                ++$i;
            }

            if (!$haveErrors) {
                $this->getDocumentManager()->flush();
            }
        }
    }
}
