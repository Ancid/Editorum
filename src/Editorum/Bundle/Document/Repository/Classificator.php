<?php
/**
 * Created by PhpStorm.
 * User: vgfilippov
 * Date: 16.06.16
 * Time: 18:37
 */

namespace Editorum\Bundle\Document\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Symfony\Component\HttpFoundation\Request;
use Editorum\Bundle\Document;

class Classificator extends DocumentRepository
{
    /**
     * @param Request $request
     * @return bool
     */
    public function updateClassif(Request $request)
    {
        $classif_name = ucfirst($request->get('name'));
        $classif_class = 'Classificator'.$classif_name;
        $object = 'Editorum\\Bundle\\Document\\'.$classif_class;
        $class = 'Editorum\\Bundle\\Document\\'.$request->get('object_class');

        if (class_exists($class) && class_exists($object)) {
            $entity = $this->getDocumentManager()->getRepository('AsuBundle:'.$request->get('object_class'))
                ->find($request->get('object_id'));
            $method = 'set'.ucfirst($classif_name);
            if (method_exists($entity, $method)) {
                $entity->$method($request->get('value'));
                if (method_exists($entity, 'updateClassif')) {
                    $entity->updateClassif($classif_name);
                }
                $this->getDocumentManager()->flush();
                return true;
            }
        }

        return false;
    }
}
