<?php
namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Doctrine\AsuId;
use Editorum\Bundle\Doctrine\Multilanguage;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableDocument;
use Gedmo\Timestampable\Traits\TimestampableDocument;

/**
 * Class Region
 * @package Editorum\Bundle\Document
 * 
 * @ODM\Document(collection="regions", repositoryClass="Editorum\Bundle\Document\Repository\Region")
 */
class Region
{
    use Multilanguage;
    use AsuId;

    /** @ODM\Id(type="int", strategy="INCREMENT") */
    protected $id;

    /** @ODM\Field(type="string") */
    protected $federal;

    /** @ODM\Field(type="string") */
    protected $city;

    /** @ODM\Field(type="float") */
    protected $coord1;

    /** @ODM\Field(type="float") */
    protected $coord2;

    /**
     * @Gedmo\SortablePosition()
     * @ODM\Field(type="int")
     */
    protected $position;


    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set coord2
     *
     * @param float $coord2
     * @return self
     */
    public function setCoord2($coord2)
    {
        $this->coord2 = $coord2;
        return $this;
    }

    /**
     * Get coord2
     *
     * @return float $coord2
     */
    public function getCoord2()
    {
        return $this->coord2;
    }

    /**
     * Set position
     *
     * @param int $position
     * @return self
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * Get position
     *
     * @return int $position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set federal
     *
     * @param string $federal
     * @return self
     */
    public function setFederal($federal)
    {
        $this->federal = $federal;
        return $this;
    }

    /**
     * Get federal
     *
     * @return string $federal
     */
    public function getFederal()
    {
        return $this->federal;
    }

    /**
     * Set coord1
     *
     * @param float $coord1
     * @return self
     */
    public function setCoord1($coord1)
    {
        $this->coord1 = $coord1;
        return $this;
    }

    /**
     * Get coord1
     *
     * @return float $coord1
     */
    public function getCoord1()
    {
        return $this->coord1;
    }
}
