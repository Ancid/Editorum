<?php
namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class IssueRubricList
 * @package Editorum\Bundle\Document
 *
 * @ODM\Document(collection="issue_rubric")
 */
class IssueRubricList
{
    /** @ODM\Id(type="int", strategy="INCREMENT") */
    protected $id;

    /** @ODM\ReferenceOne(targetDocument="Rubric", simple=true) */
    protected $rubric;

    /**
     * @ODM\ReferenceOne(targetDocument="Issue", inversedBy="issue_rubric_list", simple=true, cascade={"persist"})
     * @Gedmo\SortableGroup()
     */
    protected $issue;

    /**
     * @ODM\Field(type="int")
     * @Gedmo\SortablePosition()
     */
    protected $order;

    
    
    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rubric
     *
     * @param Rubric $rubric
     * @return self
     */
    public function setRubric(Rubric $rubric)
    {
        $this->rubric = $rubric;
        return $this;
    }

    /**
     * Get rubric
     *
     * @return Rubric $rubric
     */
    public function getRubric()
    {
        return $this->rubric;
    }

    /**
     * Set issue
     *
     * @param Issue $issue
     * @return self
     */
    public function setIssue(Issue $issue)
    {
        $this->issue = $issue;
        return $this;
    }

    /**
     * Get issue
     *
     * @return Issue $issue
     */
    public function getIssue()
    {
        return $this->issue;
    }

    /**
     * Set order
     *
     * @param int $order
     * @return self
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * Get order
     *
     * @return int $order
     */
    public function getOrder()
    {
        return $this->order;
    }
}
