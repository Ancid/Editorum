<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 31.03.2016
 * Time: 15:11
 */
namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Doctrine\AsuId;
use Editorum\Bundle\Doctrine\Multilanguage;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\Document(collection="rubric")
 */
class Rubric
{
    const EMPTY_RUBRIC_RU = 'Без рубрики';
    const EMPTY_RUBRIC_EN = 'Without rubric';
    
    use Multilanguage;
    use AsuId;

    /** @ODM\Id(type="int", strategy="INCREMENT") */
    protected $id;

    /**
     * @ODM\ReferenceOne(
     *     targetDocument="Journal",
     *     inversedBy="rubrics",
     *     cascade={"persist"},
     *     simple=true
     * ) */
    protected $journal;



    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set journal
     *
     * @param Journal $journal
     * @return self
     */
    public function setJournal(Journal $journal)
    {
        $this->journal = $journal;
        return $this;
    }

    /**
     * Get journal
     *
     * @return Journal $journal
     */
    public function getJournal()
    {
        return $this->journal;
    }
}
