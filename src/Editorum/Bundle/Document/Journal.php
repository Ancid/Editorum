<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 31.03.2016
 * Time: 15:11
 */
namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Doctrine\DoiFields;
use Editorum\Bundle\Doctrine\RincUrl;
use Symfony\Component\Validator\Constraints as Assert;
use Editorum\Bundle\Doctrine\AsuId;
use Editorum\Bundle\Doctrine\Classificators;
use Editorum\Bundle\Doctrine\Multilanguage;
use Editorum\Bundle\Doctrine\ObjectName;
use Editorum\Bundle\Doctrine\Statuses;
use Editorum\Bundle\Document\Common\AbstractCorporateEntity;
use Editorum\Bundle\Document\Common\DoiXml;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableDocument;
use Gedmo\Timestampable\Traits\TimestampableDocument;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\Document(collection="journal", repositoryClass="Editorum\Bundle\Document\Repository\Journal")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Journal
{
    const LANG_RU = 'ru';
    const LANG_EN = 'en';
    const LANG_RUS = 'RUS';
    const LANG_ENG = 'ENG';

    const ST_NEW = 100; // Новый
    const ST_IN_PROGRESS = 200; //В работе
    const ST_IS_PUBLISHED = 300;  //Опубликован
    const ST_IS_PUBLISHING = 400; //Выходит (конечный статус)
    const ST_IN_REQUEST = 700;

    use TimestampableDocument;
    use SoftDeleteableDocument;
    use Multilanguage;
    use AsuId;
    use RincUrl;
    use DoiFields;
    use DoiXml;
    use Statuses;
    use ObjectName;
    use Classificators;

    /** @ODM\Id(type="int", strategy="INCREMENT") */
    protected $id;

    /** @ODM\Field() */
    protected $cover;

    /** @ODM\Field(type="string") */
    protected $sregister;

    /** @ODM\Field(type="date") */
    protected $sdate;

    /** @ODM\Field(type="string") */
    protected $translit_title;

    /** @ODM\Field(type="string") */
    protected $agreement_code;

    /** @ODM\Field(type="int") */
    protected $st_rubricated;

    /** @ODM\Field(type="string") */
    protected $tarrif_id;

    /** @ODM\Field(type="string") */
    protected $price;

    /** @ODM\Field(type="string") */
    protected $founder;

    /** @ODM\Field(type="string") */
    protected $university_id;

    /** @ODM\Field(type="string") */
    protected $issn_online;

    /** @ODM\Field(type="string") */
    protected $issn_print;
    
    /** @ODM\ReferenceOne(targetDocument="Serie", simple=true) */
    protected $serie;

    /** @ODM\ReferenceOne() */
    protected $publisher;

    /** @ODM\Field(type="string") */
    protected $co_publisher;

    /** @ODM\Field(type="string") */
    protected $about;

    /** @ODM\Field(type="string") */
    protected $lang;

    /** @ODM\Field(type="collection") */
    protected $languages = [ self::LANG_RU ];

    /** @ODM\Field(type="int") */
    protected $rinc_id;

    /**
     * @ODM\ReferenceMany(
     *     targetDocument="Rubric",
     *     inversedBy="journal",
     *     cascade={"persist", "remove"},
     *     simple=true
     * ) */
    protected $rubrics;

    /** @ODM\ReferenceMany(
     *     targetDocument="Article",
     *     mappedBy="journal",
     *     cascade={"persist", "remove"},
     *     simple=true
     * ) */
    protected $articles;
    
    /** @ODM\ReferenceMany(
     *     targetDocument="Issue",
     *     mappedBy="journal",
     *     cascade={"persist", "remove"},
     *     simple=true
     * ) */
    protected $issues;


    public function __construct()
    {
        $this->rubrics = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Обновляет переданный класификатор связанных сущностей
     * @param $classif_name
     */
    public function updateClassif($classif_name)
    {
        if (null != $this->getIssues()) {
            $get = 'get'.$classif_name;
            $set = 'set'.$classif_name;
            /** @var Issue $issue */
            foreach ($this->getIssues() as $issue) {
                if ($issue->getStatuses()['status'] != Journal::ST_IS_PUBLISHED) {
                    $arr = [];
                    if (null != $this->$get()) {
                        $arr = $this->$get();
                        if (null != $issue->$get()) {
                            $arr = array_unique(array_merge($issue->$get(), $this->$get()));
                        }
                    } elseif (null != $issue->$get()) {
                        $arr = $issue->$get();
                    }
                    $issue->$set($arr);
                }
            }
            /** @var Article $art */
            foreach ($this->getArticles() as $art) {
                if ($art->getStatuses()['status'] != Journal::ST_IS_PUBLISHED) {
                    $arr = [];
                    if (null != $this->$get()) {
                        $arr = $this->$get();
                        if (null != $art->$get()) {
                            $arr = array_unique(array_merge($art->$get(), $this->$get()));
                        }
                    } elseif (null != $art->$get()) {
                        $arr = $art->$get();
                    }
                    $art->$set($arr);
                }
            }
        }
    }

    public static function getKinds()
    {
        return [
            self::ST_NEW => [
                'class' => 'label-default',
                'label' => 'Новый',
            ],
            self::ST_IN_PROGRESS => [
                'class' => 'label-primary',
                'label' => 'В работе',
            ],
            self::ST_IS_PUBLISHING => [
                'class' => 'label-success',
                'label' => 'Выходит',
            ],
            self::ST_IS_PUBLISHED => [
                'class' => 'label-success',
                'label' => 'Опубликован',
            ],
        ];
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rinc_id
     *
     * @param integer $rinc_id
     * @return self
     */
    public function setRincId($rinc_id)
    {
        $this->rinc_id = $rinc_id;
        return $this;
    }

    /**
     * Get rinc_id
     *
     * @return integer $rinc_id
     */
    public function getRincId()
    {
        return $this->rinc_id;
    }

    /**
     * Set cover
     *
     * @param string $cover
     * @return $this
     */
    public function setCover($cover)
    {
        $this->cover = $cover;
        return $this;
    }

    /**
     * Get cover
     *
     * @return string $cover
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * Set sregister
     *
     * @param string $sregister
     * @return $this
     */
    public function setSregister($sregister)
    {
        $this->sregister = $sregister;
        return $this;
    }

    /**
     * Get sregister
     *
     * @return string $sregister
     */
    public function getSregister()
    {
        return $this->sregister;
    }

    /**
     * Set sdate
     *
     * @param date $sdate
     * @return $this
     */
    public function setSdate($sdate)
    {
        $this->sdate = $sdate;
        return $this;
    }

    /**
     * Get sdate
     *
     * @return date $sdate
     */
    public function getSdate()
    {
        return $this->sdate;
    }

    /**
     * Set translitTitle
     *
     * @param string $translitTitle
     * @return $this
     */
    public function setTranslitTitle($translitTitle)
    {
        $this->translit_title = $translitTitle;
        return $this;
    }

    /**
     * Get translitTitle
     *
     * @return string $translitTitle
     */
    public function getTranslitTitle()
    {
        return $this->translit_title;
    }

    /**
     * Set agreementCode
     *
     * @param string $agreementCode
     * @return $this
     */
    public function setAgreementCode($agreementCode)
    {
        $this->agreement_code = $agreementCode;
        return $this;
    }

    /**
     * Get agreementCode
     *
     * @return string $agreementCode
     */
    public function getAgreementCode()
    {
        return $this->agreement_code;
    }

    /**
     * Set stRubricated
     *
     * @param int $stRubricated
     * @return $this
     */
    public function setStRubricated($stRubricated)
    {
        $this->st_rubricated = $stRubricated;
        return $this;
    }

    /**
     * Get stRubricated
     *
     * @return int $stRubricated
     */
    public function getStRubricated()
    {
        return $this->st_rubricated;
    }

    /**
     * Set tarrifId
     *
     * @param string $tarrifId
     * @return $this
     */
    public function setTarrifId($tarrifId)
    {
        $this->tarrif_id = $tarrifId;
        return $this;
    }

    /**
     * Get tarrifId
     *
     * @return string $tarrifId
     */
    public function getTarrifId()
    {
        return $this->tarrif_id;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * Get price
     *
     * @return string $price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set founder
     *
     * @param string $founder
     * @return $this
     */
    public function setFounder($founder)
    {
        $this->founder = $founder;
        return $this;
    }

    /**
     * Get founder
     *
     * @return string $founder
     */
    public function getFounder()
    {
        return $this->founder;
    }

    /**
     * Set universityId
     *
     * @param string $universityId
     * @return $this
     */
    public function setUniversityId($universityId)
    {
        $this->university_id = $universityId;
        return $this;
    }

    /**
     * Get universityId
     *
     * @return string $universityId
     */
    public function getUniversityId()
    {
        return $this->university_id;
    }

    /**
     * Set issnOnline
     *
     * @param string $issnOnline
     * @return $this
     */
    public function setIssnOnline($issnOnline)
    {
        $this->issn_online = $issnOnline;
        return $this;
    }

    /**
     * Get issnOnline
     *
     * @return string $issnOnline
     */
    public function getIssnOnline()
    {
        return $this->issn_online;
    }

    /**
     * Set issnPrint
     *
     * @param string $issnPrint
     * @return $this
     */
    public function setIssnPrint($issnPrint)
    {
        $this->issn_print = $issnPrint;
        return $this;
    }

    /**
     * Get issnPrint
     *
     * @return string $issnPrint
     */
    public function getIssnPrint()
    {
        return $this->issn_print;
    }

    /**
     * Set serie
     *
     * @param Serie $serie
     * @return $this
     */
    public function setSerie(Serie $serie)
    {
        $this->serie = $serie;
        return $this;
    }

    /**
     * Get serie
     *
     * @return Serie $serie
     */
    public function getSerie()
    {
        return $this->serie;
    }

    /**
     * Set publisher
     *
     * @param AbstractCorporateEntity $publisher
     * @return $this
     */
    public function setPublisher(AbstractCorporateEntity $publisher)
    {
        $this->publisher = $publisher;
        return $this;
    }

    /**
     * Get publisher
     *
     * @return AbstractCorporateEntity $publisher
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * Set coPublisher
     *
     * @param string $coPublisher
     * @return $this
     */
    public function setCoPublisher($coPublisher)
    {
        $this->co_publisher = $coPublisher;
        return $this;
    }

    /**
     * Get coPublisher
     *
     * @return string $coPublisher
     */
    public function getCoPublisher()
    {
        return $this->co_publisher;
    }

    /**
     * Add rubric
     *
     * @param Rubric $rubric
     */
    public function addRubric(Rubric $rubric)
    {
        $this->rubrics[] = $rubric;
    }

    /**
     * Remove rubric
     *
     * @param Rubric $rubric
     */
    public function removeRubric(Rubric $rubric)
    {
        $this->rubrics->removeElement($rubric);
    }

    /**
     * Get rubrics
     *
     * @return \Doctrine\Common\Collections\Collection $rubrics
     */
    public function getRubrics()
    {
        return $this->rubrics;
    }

    /**
     * Add article
     *
     * @param Article $article
     */
    public function addArticle(Article $article)
    {
        $this->articles[] = $article;
    }

    /**
     * Remove article
     *
     * @param Article $article
     */
    public function removeArticle(Article $article)
    {
        $this->articles->removeElement($article);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection $articles
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Add issue
     *
     * @param Issue $issue
     */
    public function addIssue(Issue $issue)
    {
        $this->issues[] = $issue;
    }

    /**
     * Remove issue
     *
     * @param Issue $issue
     */
    public function removeIssue(Issue $issue)
    {
        $this->issues->removeElement($issue);
    }

    /**
     * Get issues
     *
     * @return \Doctrine\Common\Collections\Collection $issues
     */
    public function getIssues()
    {
        return $this->issues;
    }

    /**
     * Set about
     *
     * @param string $about
     * @return $this
     */
    public function setAbout($about)
    {
        $this->about = $about;
        return $this;
    }

    /**
     * Get about
     *
     * @return string $about
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * Set lang
     *
     * @param string $lang
     * @return self
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
        return $this;
    }

    /**
     * Get lang
     *
     * @return string $lang
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Set languages
     *
     * @param array $languages
     * @return $this
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;

        $langArray = [
            self::LANG_RU => self::LANG_RUS,
            self::LANG_EN => self::LANG_ENG,
        ];

        if (isset($languages[0])) {
            $this->lang = $langArray[$languages[0]];
        }

        return $this;
    }

    /**
     * Get languages
     *
     * @return array $languages
     */
    public function getLanguages()
    {
        return $this->languages;
    }
}
