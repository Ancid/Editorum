<?php
namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

use Editorum\Bundle\Document\Common\AbstractCouncil;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Редколлегия
 * Class EditorialTeam
 * @package Editorum\Bundle\Document
 *
 * @ODM\Document(collection="editorial_team", repositoryClass="Editorum\Bundle\Document\Repository\EditorialTeam")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class EditorialTeam extends AbstractCouncil
{
    /**
     * Журнал
     * @Gedmo\SortableGroup()
     * @ODM\ReferenceOne(targetDocument="Journal", simple=true, name="journal_id")
     */
    protected $journal;



    /**
     * Set journal
     *
     * @param \Editorum\Bundle\Document\Journal $journal
     * @return self
     */
    public function setJournal(Journal $journal)
    {
        $this->journal = $journal;
        return $this;
    }

    /**
     * Get journal
     *
     * @return \Editorum\Bundle\Document\Journal $journal
     */
    public function getJournal()
    {
        return $this->journal;
    }

    /**
     * Set fio
     *
     * @param string $fio
     * @return $this
     */
    public function setFio($fio)
    {
        $this->fio = $fio;
        return $this;
    }

    /**
     * Get fio
     *
     * @return string $fio
     */
    public function getFio()
    {
        return $this->fio;
    }
}
