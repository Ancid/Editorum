<?php
namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

use Editorum\Bundle\Doctrine\AsuId;
use Editorum\Bundle\Document\Common\AbstractCouncil;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Редсовет
 * Class EditorialTeam
 * @package Editorum\Bundle\Document
 *
 * @ODM\Document(collection="council", repositoryClass="Editorum\Bundle\Document\Repository\EditorialCouncil")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class EditorialCouncil extends AbstractCouncil
{
    use AsuId;
    
    /**
     * Журнал
     * @Gedmo\SortableGroup()
     * @ODM\ReferenceOne(targetDocument="Journal", simple=true, name="journal_id")
     */
    protected $journal;



    /**
     * Set journal
     *
     * @param \Editorum\Bundle\Document\Journal $journal
     * @return self
     */
    public function setJournal(Journal $journal)
    {
        $this->journal = $journal;
        return $this;
    }

    /**
     * Get journal
     *
     * @return \Editorum\Bundle\Document\Journal $journal
     */
    public function getJournal()
    {
        return $this->journal;
    }
}
