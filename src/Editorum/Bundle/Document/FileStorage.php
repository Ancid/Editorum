<?php
namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class FileStorage
 * @package Editorum\Bundle\Document
 *
 * @ODM\Document(collection="file_storage", repositoryClass="Editorum\Bundle\Document\Repository\FileStorage")
 * @Gedmo\Uploadable(
 *     allowOverwrite=false,
 *     filenameGenerator="SHA1",
 *     path="upload/",
 *     maxSize="15728640"
 * )
 */
class FileStorage
{
    /**
     * @var int
     * @ODM\Id(type="int", strategy="INCREMENT")
     */
    private $id;

    /**
     * @ODM\ReferenceOne(nullable=true)
     */
    private $document;

    /**
     * @Gedmo\UploadableFilePath()
     * @ODM\Field()
     */
    private $path;

    /**
     * @Gedmo\UploadableFileName()
     * @ODM\Field()
     */
    private $name;

    /**
     * @ODM\Field()
     */
    private $originalName;

    /**
     * @Gedmo\UploadableFileSize()
     * @ODM\Field(type="float")
     */
    private $size;

    /**
     * @Gedmo\UploadableFileMimeType()
     * @ODM\Field()
     */
    private $mimeType;

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set document
     *
     * @param object $document
     * @return self
     */
    public function setDocument($document = null)
    {
        $this->document = $document;
        return $this;
    }

    /**
     * Get document
     *
     * @return object $document
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return self
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * Get path
     *
     * @return string $path
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set size
     *
     * @param float $size
     * @return self
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * Get size
     *
     * @return float $size
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set mimeType
     *
     * @param string $mimeType
     * @return self
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;
        return $this;
    }

    /**
     * Get mimeType
     *
     * @return string $mimeType
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * Set originalName
     *
     * @param string $originalName
     * @return self
     */
    public function setOriginalName($originalName)
    {
        $this->originalName = $originalName;
        return $this;
    }

    /**
     * Get originalName
     *
     * @return string $originalName
     */
    public function getOriginalName()
    {
        return $this->originalName;
    }
}
