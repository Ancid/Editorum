<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 06.04.16
 * Time: 11:15
 */

namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Doctrine\AsuId;
use Editorum\Bundle\Doctrine\Multilanguage;

/**
 * @ODM\Document(collection="serie")
 */
class Serie
{
    use Multilanguage;
    use AsuId;

    /** @ODM\Id(type="int", strategy="INCREMENT") */
    protected $id;


    /**
     * Set id
     *
     * @param int $id
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }
}
