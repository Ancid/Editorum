<?php

namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Document\Common\AbstractPublication;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\Document(collection="monography")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Monography extends AbstractPublication
{
    /** @ODM\Field(type="string") */
    protected $isbn;

    /** @ODM\Field(type="string") */
    protected $isbn_online;

    /** @ODM\Field(type="date") */
    protected $date_max;

    /** @ODM\Field(type="int") */
    protected $page_total;

    /** @ODM\Field(type="string") */
    protected $cover;

    /** @ODM\Field(type="int") */
    protected $publication_number;

    /** @ODM\ReferenceMany(
     *     targetDocument="Chapter",
     *     simple=true,
     *     cascade={"remove"}
     *  ) */
    protected $chapters;

    /** @ODM\Field(type="string") */
    protected $additional_information;

    /** @ODM\Field(type="string") */
    protected $copublisher;


    /**
     * Set PageTotal
     *
     * @param string $pageTotal
     * @return self
     */
    public function setPageTotal($pageTotal)
    {
        $this->page_total = $pageTotal;
        return $this;
    }

    /**
     * Get PageTotal
     *
     * @return string $pageTotal
     */
    public function getPageTotal()
    {
        return $this->page_total;
    }

    /**
     * Set isbn
     *
     * @param string $isbn
     * @return self
     */
    public function setIsbn($isbn)
    {
        $this->isbn = $isbn;
        return $this;
    }

    /**
     * Get isbn
     *
     * @return string $isbn
     */
    public function getIsbn()
    {
        return $this->isbn;
    }

    /**
     * Set isbnOnline
     *
     * @param string $isbnOnline
     * @return self
     */
    public function setIsbnOnline($isbnOnline)
    {
        $this->isbn_online = $isbnOnline;
        return $this;
    }

    /**
     * Get isbnOnline
     *
     * @return string $isbnOnline
     */
    public function getIsbnOnline()
    {
        return $this->isbn_online;
    }

    /**
     * Set dateMax
     *
     * @param date $dateMax
     * @return self
     */
    public function setDateMax($dateMax)
    {
        $this->date_max = $dateMax;
        return $this;
    }

    /**
     * Get dateMax
     *
     * @return date $dateMax
     */
    public function getDateMax()
    {
        return $this->date_max;
    }

    /**
     * Set cover
     *
     * @param string $cover
     * @return self
     */
    public function setCover($cover)
    {
        $this->cover = $cover;
        return $this;
    }

    /**
     * Get cover
     *
     * @return string $cover
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * Set publicationNumber
     *
     * @param int $publicationNumber
     * @return self
     */
    public function setPublicationNumber($publicationNumber)
    {
        $this->publication_number = $publicationNumber;
        return $this;
    }

    /**
     * Get publicationNumber
     *
     * @return int $publicationNumber
     */
    public function getPublicationNumber()
    {
        return $this->publication_number;
    }

    /**
     * Add chapter
     *
     * @param Chapter $chapter
     */
    public function addChapter(Chapter $chapter)
    {
        $this->chapters[] = $chapter;
    }

    /**
     * Remove chapter
     *
     * @param Chapter $chapter
     */
    public function removeChapter(Chapter $chapter)
    {
        $this->chapters->removeElement($chapter);
    }

    /**
     * @return $this
     */
    public function clearChapters()
    {
        $this->chapters = [];
        return $this;
    }

    /**
     * Get chapters
     *
     * @return \Doctrine\Common\Collections\Collection $chapters
     */
    public function getChapters()
    {
        return $this->chapters;
    }

    /**
     * Set additionalInformation
     *
     * @param string $additionalInformation
     * @return self
     */
    public function setAdditionalInformation($additionalInformation)
    {
        $this->additional_information = $additionalInformation;
        return $this;
    }

    /**
     * Get additionalInformation
     *
     * @return string $additionalInformation
     */
    public function getAdditionalInformation()
    {
        return $this->additional_information;
    }

    /**
     * Set copublisher
     *
     * @param string $copublisher
     * @return self
     */
    public function setCopublisher($copublisher)
    {
        $this->copublisher = $copublisher;
        return $this;
    }

    /**
     * Get copublisher
     *
     * @return string $copublisher
     */
    public function getCopublisher()
    {
        return $this->copublisher;
    }

    /**
     * Set udk
     *
     * @param collection $udk
     * @return $this
     */
    public function setUdk($udk)
    {
        $this->udk = $udk;
        return $this;
    }

    /**
     * Get udk
     *
     * @return collection $udk
     */
    public function getUdk()
    {
        return $this->udk;
    }

    /**
     * Set okso
     *
     * @param collection $okso
     * @return $this
     */
    public function setOkso($okso)
    {
        $this->okso = $okso;
        return $this;
    }

    /**
     * Get okso
     *
     * @return collection $okso
     */
    public function getOkso()
    {
        return $this->okso;
    }

    /**
     * Set bbk
     *
     * @param collection $bbk
     * @return $this
     */
    public function setBbk($bbk)
    {
        $this->bbk = $bbk;
        return $this;
    }

    /**
     * Get bbk
     *
     * @return collection $bbk
     */
    public function getBbk()
    {
        return $this->bbk;
    }

    /**
     * Set tbk
     *
     * @param collection $tbk
     * @return $this
     */
    public function setTbk($tbk)
    {
        $this->tbk = $tbk;
        return $this;
    }

    /**
     * Get tbk
     *
     * @return collection $tbk
     */
    public function getTbk()
    {
        return $this->tbk;
    }

    public function publicate()
    {
        $this->setStatus('is_published', 1);
        $this->setDatePublished(new \DateTime());
        $this->setStatus('status', AbstractPublication::ST_IS_PUBLISHED);
    }
}
