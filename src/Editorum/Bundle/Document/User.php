<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 25.01.2016
 * Time: 16:51
 * @ToDO: отрефакторить при слиянии с веткой VKR
 */

namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Document\Common\AbstractCorporateEntity;
use SecurityBundle\Document\AbstractUser;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ODM\Document(collection="users")
 */
class User extends AbstractUser
{
    /**
     * @ODM\Id(type="int", strategy="INCREMENT")
     */
    protected $id;
    
    /**
     * @ODM\Field(type="bool")
     */
    protected $is_admin = false;

    /**
     * @ODM\String(name="last_name")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    protected $last_name;

    /**
     * @ODM\String(name="first_name")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    protected $first_name;

    /**
     * @ODM\String(name="second_name")
     */
    protected $second_name;

    /**
     * @ODM\String(name="email")
     * @Assert\Email()
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    protected $email;

    /**
     * @ODM\ReferenceOne()
     */
    protected $corporate;

    /**
     * @ODM\ReferenceOne(targetDocument="Author", cascade={"persist"}, mappedBy="user")
     */
    protected $author;

    

    /**
     * Set isAdmin
     *
     * @param bool $isAdmin
     * @return self
     */
    public function setIsAdmin($isAdmin)
    {
        $this->is_admin = $isAdmin;
        return $this;
    }

    /**
     * Get isAdmin
     *
     * @return bool $isAdmin
     */
    public function getIsAdmin()
    {
        return $this->is_admin;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return self
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;
        return $this;
    }

    /**
     * Get lastName
     *
     * @return string $lastName
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return self
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;
        return $this;
    }

    /**
     * Get firstName
     *
     * @return string $firstName
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set secondName
     *
     * @param string $secondName
     * @return self
     */
    public function setSecondName($secondName)
    {
        $this->second_name = $secondName;
        return $this;
    }

    /**
     * Get secondName
     *
     * @return string $secondName
     */
    public function getSecondName()
    {
        return $this->second_name;
    }

    /**
     * Set corporate
     *
     * @param AbstractCorporateEntity $corporate
     * @return self
     */
    public function setCorporate(AbstractCorporateEntity $corporate)
    {
        $this->corporate = $corporate;
        return $this;
    }

    /**
     * Get corporate
     *
     * @return AbstractCorporateEntity $corporate
     */
    public function getCorporate()
    {
        return $this->corporate;
    }

    /**
     * Set author
     *
     * @param Author $author
     * @return self
     */
    public function setAuthor(Author $author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * Get author
     *
     * @return Author $author
     */
    public function getAuthor()
    {
        return $this->author;
    }

    public function getEnabled()
    {
        return $this->enabled;
    }
}
