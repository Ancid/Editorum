<?php

namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Doctrine\AsuId;
use Editorum\Bundle\Doctrine\Classificators;
use Editorum\Bundle\Doctrine\DoiFields;
use Editorum\Bundle\Doctrine\Multilanguage;
use Editorum\Bundle\Doctrine\ObjectName;
use Editorum\Bundle\Doctrine\Statuses;
use Editorum\Bundle\Document\Common\AbstractCorporateEntity;
use Editorum\Bundle\Document\Common\DoiXml;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableDocument;
use Gedmo\Timestampable\Traits\TimestampableDocument;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\Document(collection="conference", repositoryClass="Editorum\Bundle\Document\Repository\Conference")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Conference
{
    const LANG_RU = 'ru';
    const LANG_EN = 'en';
    const LANG_RUS = 'RUS';
    const LANG_ENG = 'ENG';
    const DEFAULT_TITLE = 'Empty';

    const ST_NEW = 100;
    const ST_IN_PROGRESS = 200;
    const ST_IS_PUBLISHED = 300;
    const ST_INACTIVE = 400;
    const ST_ACTIVE = 500;
    const ST_CLOSED = 600;
    const ST_IN_REQUEST = 700;

    use TimestampableDocument;
    use SoftDeleteableDocument;
    use Multilanguage;
    use AsuId;
    use Statuses;
    use Classificators;
    use ObjectName;
    use DoiXml;
    use DoiFields;

    /** @ODM\Id(type="int", strategy="INCREMENT") */
    protected $id;

    /** @ODM\Field(type="string") */
    protected $html;

    /** @ODM\Field(type="date") */
    protected $date_start;

    /** @ODM\Field(type="date") */
    protected $date_finish;

    /** @ODM\Field(type="date") */
    protected $request_start;

    /** @ODM\Field(type="date") */
    protected $request_finish;

    /** @ODM\Field(type="int") */
    protected $number;

    /** @ODM\Field(type="string") */
    protected $lang;

    /** @ODM\Field(type="collection") */
    protected $languages = [ self::LANG_RU ];

    /** @ODM\ReferenceOne(
     *     targetDocument="Country",
     *     simple=true
     * ) */
    protected $country;

    /** @ODM\ReferenceOne() */
    protected $publisher;

    /** @ODM\ReferenceMany(targetDocument="Collection",
     *     mappedBy="conference",
     *     cascade={"persist", "remove"},
     *     simple=true
     * ) */
    protected $collections;

    /** @ODM\ReferenceMany(
     *     targetDocument="ConferenceArticle",
     *     mappedBy="conference",
     *     cascade={"persist", "remove"},
     *     simple=true
     * ) */
    protected $articles;

    /**
     * @ODM\ReferenceMany(
     *     targetDocument="Section",
     *     inversedBy="conference",
     *     cascade={"persist", "remove"},
     *     simple=true,
     * ) */
    protected $sections;



    public function __construct()
    {
        $this->collections = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public static function getKinds()
    {
        return [
            self::ST_INACTIVE => [
                'class' => 'label-default',
                'label' => 'Неактивная',
            ],
            self::ST_ACTIVE => [
                'class' => 'label-success',
                'label' => 'Активная',
            ],
            self::ST_CLOSED => [
                'class' => 'label-default',
                'label' => 'Закрыта',
            ],
            self::ST_IN_PROGRESS => [
                'class' => 'label-primary',
                'label' => 'В работе',
            ],
            self::ST_IS_PUBLISHED => [
                'class' => 'label-success',
                'label' => 'Опубликовано',
            ],
            self::ST_NEW => [
                'class' => 'label-primary',
                'label' => 'Новая',
            ],
        ];
    }

    /**
     * Обновляет переданный класификатор связанных сущностей
     * @param $classif_name
     */
    public function updateClassif($classif_name)
    {
        if (null != $this->getCollections()) {
            $get = 'get'.$classif_name;
            $set = 'set'.$classif_name;
            /** @var Collection $col */
            foreach ($this->getCollections() as $col) {
                if ($col->getStatuses()['status'] != Conference::ST_IS_PUBLISHED) {
                    $arr = [];
                    if (null != $this->$get()) {
                        $arr = $this->$get();
                        if (null != $col->$get()) {
                            $arr = array_unique(array_merge($col->$get(), $this->$get()));
                        }
                    } elseif (null != $col->$get()) {
                        $arr = $col->$get();
                    }
                    $col->$set($arr);
                }
            }
            /** @var ConferenceArticle $art */
            foreach ($this->getArticles() as $art) {
                if ($art->getStatuses()['status'] != Conference::ST_IS_PUBLISHED) {
                    $arr = [];
                    if (null != $this->$get()) {
                        $arr = $this->$get();
                        if (null != $art->$get()) {
                            $arr = array_unique(array_merge($art->$get(), $this->$get()));
                        }
                    } elseif (null != $art->$get()) {
                        $arr = $art->$get();
                    }
                    $art->$set(array_unique($arr));
                }
            }
        }
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set html
     *
     * @param string $html
     * @return self
     */
    public function setHtml($html)
    {
        $this->html = $html;
        return $this;
    }

    /**
     * Get html
     *
     * @return string $html
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * Set dateStart
     *
     * @param date $dateStart
     * @return self
     */
    public function setDateStart($dateStart)
    {
        $this->date_start = $dateStart;
        return $this;
    }

    /**
     * Get dateStart
     *
     * @return date $dateStart
     */
    public function getDateStart()
    {
        return $this->date_start;
    }

    /**
     * Set dateFinish
     *
     * @param date $dateFinish
     * @return self
     */
    public function setDateFinish($dateFinish)
    {
        $this->date_finish = $dateFinish;
        return $this;
    }

    /**
     * Get dateFinish
     *
     * @return date $dateFinish
     */
    public function getDateFinish()
    {
        return $this->date_finish;
    }


    /**
     * Set requestStart
     *
     * @param $requestStart
     * @return static
     */
    public function setRequestStart($requestStart)
    {
        $this->request_start = $requestStart;
        return $this;
    }

    /**
     * Get requestStart
     *
     * @return date $dateStart
     */
    public function getRequestStart()
    {
        return $this->request_start;
    }


    /**
     * Set requestFinish
     *
     * @param $requestFinish
     * @return static
     */
    public function setRequestFinish($requestFinish)
    {
        $this->request_finish = $requestFinish;
        return $this;
    }

    /**
     * Get requestFinish
     *
     * @return date $dateFinish
     */
    public function getRequestFinish()
    {
        return $this->request_finish;
    }

    /**
     * Set number
     *
     * @param int $number
     * @return self
     */
    public function setNumber($number)
    {
        $this->number = $number;
        return $this;
    }

    /**
     * Get number
     *
     * @return int $number
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set lang
     *
     * @param string $lang
     * @return self
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
        return $this;
    }

    /**
     * Get lang
     *
     * @return string $lang
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Set languages
     *
     * @param array $languages
     * @return $this
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;

        $langArray = [
            self::LANG_RU => self::LANG_RUS,
            self::LANG_EN => self::LANG_ENG,
        ];

        if (isset($languages[0])) {
            $this->lang = $langArray[$languages[0]];
        }

        return $this;
    }

    /**
     * Get languages
     *
     * @return array $languages
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * Set country
     *
     * @param Country $country
     * @return self
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * Get country
     *
     * @return Country $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Get collections
     *
     * @return \Doctrine\Common\Collections\Collection $collections
     */
    public function getCollections()
    {
        return $this->collections;
    }

    /**
     * Add article
     *
     * @param ConferenceArticle $article
     */
    public function addArticle(ConferenceArticle $article)
    {
        $this->articles[] = $article;
    }

    /**
     * Remove article
     *
     * @param ConferenceArticle $article
     */
    public function removeArticle(ConferenceArticle $article)
    {
        $this->articles->removeElement($article);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection $articles
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Add section
     *
     * @param Section $section
     */
    public function addSection(Section $section)
    {
        $this->sections[] = $section;
    }

    /**
     * Remove section
     *
     * @param Section $section
     */
    public function removeSection(Section $section)
    {
        $this->sections->removeElement($section);
    }

    /**
     * Get sections
     *
     * @return \Doctrine\Common\Collections\Collection $sections
     */
    public function getSections()
    {
        return $this->sections;
    }

    /**
     * Set publisher
     *
     * @param AbstractCorporateEntity|null $publisher
     * @return self
     */
    public function setPublisher(AbstractCorporateEntity $publisher)
    {
        $this->publisher = $publisher;
        return $this;
    }

    /**
     * Get publisher
     *
     * @return AbstractCorporateEntity $publisher
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * Add collection
     *
     * @param Collection $collection
     */
    public function addCollection(Collection $collection)
    {
        $this->collections[] = $collection;
    }

    /**
     * Remove collection
     *
     * @param Collection $collection
     */
    public function removeCollection(Collection $collection)
    {
        $this->collections->removeElement($collection);
    }
}
