<?php

namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Document\Common\AbstractCorporateEntity;
use Editorum\Bundle\Document\Common\AbstractPublication;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\Document(collection="article")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Article extends AbstractPublication
{
    /** @ODM\Field(type="int") */
    protected $first_page;

    /** @ODM\Field(type="int") */
    protected $last_page;

    /** @ODM\Field(type="string") */
    protected $published_journal;

    /** @ODM\Field(type="int") */
    protected $published_issue;

    /** @ODM\Field(type="int") */
    protected $issue_order;

    /** @ODM\ReferenceOne(
     *     targetDocument="Issue",
     *     inversedBy="articles",
     *     cascade={"persist"},
     *     simple=true,
     *     nullable=true
     * ) */
    protected $issue;

    /** @ODM\ReferenceOne(
     *     targetDocument="Rubric",
     *     simple=true,
     *     nullable=true
     * ) */
    protected $rubric;

    /** @ODM\ReferenceOne(
     *     targetDocument="Journal",
     *     inversedBy="articles",
     *     cascade={"persist"},
     *     simple=true
     * ) */
    protected $journal;


    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Set firstPage
     *
     * @param int $firstPage
     * @return $this
     */
    public function setFirstPage($firstPage)
    {
        $this->first_page = $firstPage;
        return $this;
    }

    /**
     * Get firstPage
     *
     * @return int $firstPage
     */
    public function getFirstPage()
    {
        return $this->first_page;
    }

    /**
     * Set lastPage
     *
     * @param int $lastPage
     * @return $this
     */
    public function setLastPage($lastPage)
    {
        $this->last_page = $lastPage;
        return $this;
    }

    /**
     * Get lastPage
     *
     * @return int $lastPage
     */
    public function getLastPage()
    {
        return $this->last_page;
    }

    /**
     * Set publishedJournal
     *
     * @param string $publishedJournal
     * @return $this
     */
    public function setPublishedJournal($publishedJournal)
    {
        $this->published_journal = $publishedJournal;
        return $this;
    }

    /**
     * Get publishedJournal
     *
     * @return string $publishedJournal
     */
    public function getPublishedJournal()
    {
        return $this->published_journal;
    }



    /**
     * Set publishedIssue
     *
     * @param int $publishedIssue
     * @return $this
     */
    public function setPublishedIssue($publishedIssue)
    {
        $this->published_issue = $publishedIssue;
        return $this;
    }

    /**
     * Get publishedIssue
     *
     * @return int $publishedIssue
     */
    public function getPublishedIssue()
    {
        return $this->published_issue;
    }

    /**
     * Set issueOrder
     *
     * @param int $issueOrder
     * @return $this
     */
    public function setIssueOrder($issueOrder)
    {
        $this->issue_order = $issueOrder;
        return $this;
    }

    /**
     * Get issueOrder
     *
     * @return int $issueOrder
     */
    public function getIssueOrder()
    {
        return $this->issue_order;
    }

    /**
     * Set rubric
     *
     * @param Rubric $rubric
     * @return $this
     */
    public function setRubric(Rubric $rubric = null)
    {
        $this->rubric = $rubric;
        return $this;
    }

    /**
     * Get rubric
     *
     * @return Rubric $rubric
     */
    public function getRubric()
    {
        return $this->rubric;
    }

    /**
     * Set journal
     *
     * @param Journal $journal
     * @return $this
     */
    public function setJournal(Journal $journal)
    {
        $this->journal = $journal;
        return $this;
    }

    /**
     * Get journal
     *
     * @return Journal $journal
     */
    public function getJournal()
    {
        return $this->journal;
    }

    /**
     * Set issue
     *
     * @param Issue|null $issue
     * @return $this
     */
    public function setIssue(Issue $issue = null)
    {
        $this->issue = $issue;
        return $this;
    }

    /**
     * Get issue
     *
     * @return Issue $issue
     */
    public function getIssue()
    {
        return $this->issue;
    }

    /**
     * Set udk
     *
     * @param collection $udk
     * @return $this
     */
    public function setUdk($udk)
    {
        $this->udk = $udk;
        return $this;
    }

    /**
     * Get udk
     *
     * @return collection $udk
     */
    public function getUdk()
    {
        return $this->udk;
    }

    /**
     * Set okso
     *
     * @param collection $okso
     * @return $this
     */
    public function setOkso($okso)
    {
        $this->okso = $okso;
        return $this;
    }

    /**
     * Get okso
     *
     * @return collection $okso
     */
    public function getOkso()
    {
        return $this->okso;
    }

    /**
     * Set bbk
     *
     * @param collection $bbk
     * @return $this
     */
    public function setBbk($bbk)
    {
        $this->bbk = $bbk;
        return $this;
    }

    /**
     * Get bbk
     *
     * @return collection $bbk
     */
    public function getBbk()
    {
        return $this->bbk;
    }

    /**
     * Set tbk
     *
     * @param collection $tbk
     * @return $this
     */
    public function setTbk($tbk)
    {
        $this->tbk = $tbk;
        return $this;
    }

    /**
     * Get tbk
     *
     * @return collection $tbk
     */
    public function getTbk()
    {
        return $this->tbk;
    }
}
