<?php

namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Document\Common\AbstractPublication;
use Gedmo\Mapping\Annotation as Gedmo;
use ReflectionClass;

/**
 * @ODM\Document(collection="conference_article")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ConferenceArticle extends AbstractPublication
{
    /** @ODM\Field(type="int") */
    protected $first_page;

    /** @ODM\Field(type="int") */
    protected $last_page;

    /** @ODM\Field(type="int") */
    protected $order;

    /** @ODM\ReferenceOne(
     *     targetDocument="Collection",
     *     inversedBy="articles",
     *     cascade={"persist"},
     *     simple=true,
     *     nullable=true
     * ) */
    protected $collection;

    /** @ODM\ReferenceOne(
     *     targetDocument="Section",
     *     simple=true,
     *     nullable=true
     * ) */
    protected $section;

    /** @ODM\ReferenceOne(
     *     targetDocument="Conference",
     *     inversedBy="articles",
     *     cascade={"persist"},
     *     simple=true
     * ) */
    protected $conference;



    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Set firstPage
     *
     * @param int $firstPage
     * @return $this
     */
    public function setFirstPage($firstPage)
    {
        $this->first_page = $firstPage;
        return $this;
    }

    /**
     * Get firstPage
     *
     * @return int $firstPage
     */
    public function getFirstPage()
    {
        return $this->first_page;
    }

    /**
     * Set lastPage
     *
     * @param int $lastPage
     * @return $this
     */
    public function setLastPage($lastPage)
    {
        $this->last_page = $lastPage;
        return $this;
    }

    /**
     * Get lastPage
     *
     * @return int $lastPage
     */
    public function getLastPage()
    {
        return $this->last_page;
    }

    /**
     * Set collection
     *
     * @param Collection $collection
     * @return $this
     */
    public function setCollection(Collection $collection = null)
    {
        $this->collection = $collection;
        return $this;
    }

    /**
     * Get collection
     *
     * @return Collection $collection
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * Set section
     *
     * @param Section $section
     * @return $this
     */
    public function setSection(Section $section = null)
    {
        $this->section = $section;
        return $this;
    }

    /**
     * Get section
     *
     * @return Section $section
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set conference
     *
     * @param Conference $conference
     * @return $this
     */
    public function setConference(Conference $conference)
    {
        $this->conference = $conference;
        return $this;
    }

    /**
     * Get conference
     *
     * @return Conference $conference
     */
    public function getConference()
    {
        return $this->conference;
    }

    /**
     * Set languages
     *
     * @param collection $languages
     * @return $this
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;
        return $this;
    }

    /**
     * Get languages
     *
     * @return collection $languages
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * Set order
     *
     * @param int $order
     * @return $this
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * Get order
     *
     * @return int $order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set udk
     *
     * @param collection $udk
     * @return $this
     */
    public function setUdk($udk)
    {
        $this->udk = $udk;
        return $this;
    }

    /**
     * Get udk
     *
     * @return collection $udk
     */
    public function getUdk()
    {
        return $this->udk;
    }

    /**
     * Set okso
     *
     * @param collection $okso
     * @return $this
     */
    public function setOkso($okso)
    {
        $this->okso = $okso;
        return $this;
    }

    /**
     * Get okso
     *
     * @return collection $okso
     */
    public function getOkso()
    {
        return $this->okso;
    }

    /**
     * Set bbk
     *
     * @param collection $bbk
     * @return $this
     */
    public function setBbk($bbk)
    {
        $this->bbk = $bbk;
        return $this;
    }

    /**
     * Get bbk
     *
     * @return collection $bbk
     */
    public function getBbk()
    {
        return $this->bbk;
    }

    /**
     * Set tbk
     *
     * @param collection $tbk
     * @return $this
     */
    public function setTbk($tbk)
    {
        $this->tbk = $tbk;
        return $this;
    }

    /**
     * Get tbk
     *
     * @return collection $tbk
     */
    public function getTbk()
    {
        return $this->tbk;
    }
}
