<?php


namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Doctrine\AsuId;
use Editorum\Bundle\Doctrine\DatePublished;
use Editorum\Bundle\Doctrine\DoiFields;
use Editorum\Bundle\Doctrine\Multilanguage;
use Editorum\Bundle\Doctrine\ObjectName;
use Editorum\Bundle\Doctrine\RincUrl;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableDocument;

/**
 * @ODM\Document(collection="chapter")
 */
class Chapter
{
    use TimestampableDocument;
    use Multilanguage;
    use AsuId;
    use DatePublished;
    use ObjectName;
    use DoiFields;

    /** @ODM\Id(type="int", strategy="INCREMENT") */
    protected $id;

    /** @ODM\Field(type="int") */
    protected $number;

    /** @ODM\Field(type="string") */
    protected $text;

    /** @ODM\Field(type="string") */
    protected $znanium_link;
    
    /** @ODM\Field(type="int") */
    protected $first_page;

    /** @ODM\Field(type="int") */
    protected $last_page;

    /** @ODM\Field(type="hash") */
    protected $statuses = [];

    /**
     * @ODM\ReferenceMany(
     *     targetDocument="Author",
     *     simple=true
     * ) */
    protected $authors = [];

    /** @ODM\ReferenceOne() */
    protected $publication;


    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param int $number
     * @return $this
     */
    public function setNumber($number)
    {
        $this->number = $number;
        return $this;
    }

    /**
     * Get number
     *
     * @return int $number
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return $this
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * Get text
     *
     * @return string $text
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set znaniumLink
     *
     * @param string $znaniumLink
     * @return $this
     */
    public function setZnaniumLink($znaniumLink)
    {
        $this->znanium_link = $znaniumLink;
        return $this;
    }

    /**
     * Get znaniumLink
     *
     * @return string $znaniumLink
     */
    public function getZnaniumLink()
    {
        return $this->znanium_link;
    }

    /**
     * Set firstPage
     *
     * @param int $firstPage
     * @return $this
     */
    public function setFirstPage($firstPage)
    {
        $this->first_page = $firstPage;
        return $this;
    }

    /**
     * Get firstPage
     *
     * @return int $firstPage
     */
    public function getFirstPage()
    {
        return $this->first_page;
    }

    /**
     * Set lastPage
     *
     * @param int $lastPage
     * @return $this
     */
    public function setLastPage($lastPage)
    {
        $this->last_page = $lastPage;
        return $this;
    }

    /**
     * Get lastPage
     *
     * @return int $lastPage
     */
    public function getLastPage()
    {
        return $this->last_page;
    }

    /**
     * Set statuses
     *
     * @param hash $statuses
     * @return $this
     */
    public function setStatuses($statuses)
    {
        $this->statuses = $statuses;
        return $this;
    }

    /**
     * Get statuses
     *
     * @return hash $statuses
     */
    public function getStatuses()
    {
        return $this->statuses;
    }
    public function __construct()
    {
        $this->authors = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add author
     *
     * @param Author $author
     */
    public function addAuthor(Author $author)
    {
        $this->authors[] = $author;
    }

    /**
     * Remove author
     *
     * @param Author $author
     */
    public function removeAuthor(Author $author)
    {
        $this->authors->removeElement($author);
    }

    /**
     * Get authors
     *
     * @return \Doctrine\Common\Collections\Collection $authors
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * Set publication
     *
     * @param $publication
     * @return $this
     */
    public function setPublication($publication)
    {
        $this->publication = $publication;
        return $this;
    }

    /**
     * Get publication
     *
     * @return $publication
     */
    public function getPublication()
    {
        return $this->publication;
    }
}
