<?php


namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Doctrine\AsuId;
use Editorum\Bundle\Doctrine\Multilanguage;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class Country
 * @ODM\Document(collection="countries", repositoryClass="Editorum\Bundle\Document\Repository\Country")
 * @package Editorum\Bundle\Document
 */
class Country
{
    use Multilanguage;
    use AsuId;

    /** @ODM\Id(type="int", strategy="INCREMENT") */
    protected $id;

    /** @ODM\Field(type="string", name="cont") */
    protected $cont;

    /** @ODM\Field(type="string", name="city") */
    protected $city;

    /** @ODM\Field(type="float", name="coord1") */
    protected $coord1;

    /** @ODM\Field(type="float", name="coord2") */
    protected $coord2;

    /**
     * @Gedmo\SortablePosition()
     * @ODM\Field(type="int", name="position")
     */
    protected $position;

    /**
     * Set cont
     *
     * @param string $cont
     * @return self
     */
    public function setCont($cont)
    {
        $this->cont = $cont;
        return $this;
    }

    /**
     * Get cont
     *
     * @return string $cont
     */
    public function getCont()
    {
        return $this->cont;
    }

    /**
     * Set coord2
     *
     * @param float $coord2
     * @return self
     */
    public function setCoord2($coord2)
    {
        $this->coord2 = $coord2;
        return $this;
    }

    /**
     * Get coord2
     *
     * @return float $coord2
     */
    public function getCoord2()
    {
        return $this->coord2;
    }

    /**
     * Set position
     *
     * @param int $position
     * @return self
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * Get position
     *
     * @return int $position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set coord1
     *
     * @param float $coord1
     * @return self
     */
    public function setCoord1($coord1)
    {
        $this->coord1 = $coord1;
        return $this;
    }

    /**
     * Get coord1
     *
     * @return float $coord1
     */
    public function getCoord1()
    {
        return $this->coord1;
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getRu()['title'];
    }
}
