<?php
namespace Editorum\Bundle\Document\Author;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Doctrine\Multilanguage;

/**
 * Class JobPlace
 *
 * @package Editorum\Bundle\Document\Author
 * @ODM\Document(collection="job_place")
 */
class JobPlace
{
    use Multilanguage;

    /**
     * @ODM\Id(type="int", strategy="INCREMENT")
     */
    protected $id;

    /**
     * @ODM\ReferenceOne(targetDocument="Editorum\Bundle\Document\Country", simple=true)
     */
    protected $country;

    /**
     * @ODM\Field(type="hash")
     */
    protected $city;

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set country
     *
     * @param \Editorum\Bundle\Document\Country $country
     * @return $this
     */
    public function setCountry(\Editorum\Bundle\Document\Country $country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * Get country
     *
     * @return \Editorum\Bundle\Document\Country $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set city
     *
     * @param array $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Get city
     *
     * @return array $city
     */
    public function getCity()
    {
        return $this->city;
    }
}
