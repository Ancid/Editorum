<?php
namespace Editorum\Bundle\Document\Author;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class JobList
 *
 * @package Editorum\Bundle\Document\Author
 * @ODM\Document(collection="job_list")
 */
class JobList
{
    /**
     * @ODM\Id(type="int", strategy="INCREMENT")
     */
    protected $id;

    /**
     * @ODM\ReferenceOne()
     */
    protected $place;

    /**
     * @ODM\Field(type="string")
     */
    protected $department;

    /**
     * @ODM\Field(type="string")
     */
    protected $position;

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set place
     *
     * @param $place
     * @return $this
     */
    public function setPlace($place)
    {
        $this->place = $place;
        return $this;
    }

    /**
     * Get place
     *
     * @return $place
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set department
     *
     * @param string $department
     * @return $this
     */
    public function setDepartment($department)
    {
        $this->department = $department;
        return $this;
    }

    /**
     * Get department
     *
     * @return string $department
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set position
     *
     * @param string $position
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * Get position
     *
     * @return string $position
     */
    public function getPosition()
    {
        return $this->position;
    }
}
