<?php
namespace Editorum\Bundle\Document\Author;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class DegreeList
 *
 * @package Editorum\Bundle\Document\Author
 * @ODM\Document(collection="degree_list")
 */
class DegreeList
{
    /**
     * @ODM\Id(type="int", strategy="INCREMENT")
     */
    protected $id;

    /**
     * @ODM\ReferenceOne(targetDocument="Editorum\Bundle\Document\ListStorage", simple=true)
     */
    protected $degree;

    /**
     * @ODM\ReferenceOne(targetDocument="Editorum\Bundle\Document\ListStorage", simple=true)
     */
    protected $science;

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set degree
     *
     * @param Editorum\Bundle\Document\ListStorage $degree
     * @return $this
     */
    public function setDegree(\Editorum\Bundle\Document\ListStorage $degree)
    {
        $this->degree = $degree;
        return $this;
    }

    /**
     * Get degree
     *
     * @return Editorum\Bundle\Document\ListStorage $degree
     */
    public function getDegree()
    {
        return $this->degree;
    }

    /**
     * Set science
     *
     * @param Editorum\Bundle\Document\ListStorage $science
     * @return $this
     */
    public function setScience(\Editorum\Bundle\Document\ListStorage $science)
    {
        $this->science = $science;
        return $this;
    }

    /**
     * Get science
     *
     * @return Editorum\Bundle\Document\ListStorage $science
     */
    public function getScience()
    {
        return $this->science;
    }
}
