<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 19.05.2016
 * Time: 13:30
 */

namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Document\Common\AbstractCorporateEntity;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableDocument;
use Gedmo\Timestampable\Traits\TimestampableDocument;
use Gedmo\Translatable\Translatable;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ODM\Document(
 *     collection="organization",
 *      repositoryClass="Editorum\Bundle\Document\Repository\Organization"
 * )
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Organization implements Translatable
{
    const EDITORUM_DOI_PREFIX = '10.21610';

    use TimestampableDocument;
    use SoftDeleteableDocument;
    
    /** @ODM\Id(type="int", strategy="INCREMENT") */
    protected $id;

    /** @ODM\Field(type="int") */
    protected $doi_limit;

    /** @ODM\Field(type="int") */
    protected $doi_current = 0;

    /**
     * Краткое название
     * @ODM\Field(type="string")
     */
    protected $name;

    /**
     * Полное название
     * @ODM\Field(type="string")
     */
    protected $fullname;

    /**
     * Организация-депозитор CrossRef
     * @ODM\Field(type="string")
     */
    protected $depositor;

    /** @ODM\ReferenceMany() */
    protected $corporates;


    public function __construct()
    {
        $this->corporates = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Increment doiLimit
     *
     * @return $this
     */
    public function incrementDoiLimit()
    {
        $this->doi_limit++;
        return $this;
    }

    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     * @return self
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
        return $this;
    }

    /**
     * Get fullname
     *
     * @return string $fullname
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Set depositor
     *
     * @param string $depositor
     * @return self
     */
    public function setDepositor($depositor)
    {
        $this->depositor = $depositor;
        return $this;
    }

    /**
     * Get depositor
     *
     * @return string $depositor
     */
    public function getDepositor()
    {
        return $this->depositor;
    }

    /**
     * Add corporate
     *
     * @param AbstractCorporateEntity $corporate
     */
    public function addCorporate(AbstractCorporateEntity $corporate)
    {
        $this->corporates[] = $corporate;
    }

    /**
     * Remove corporate
     *
     * @param AbstractCorporateEntity $corporate
     */
    public function removeCorporate(AbstractCorporateEntity $corporate)
    {
        $this->corporates->removeElement($corporate);
    }

    /**
     * Get corporates
     *
     * @return \Doctrine\Common\Collections\Collection $corporates
     */
    public function getCorporates()
    {
        return $this->corporates;
    }

    /**
     * Set doiLimit
     *
     * @param int $doiLimit
     * @return $this
     */
    public function setDoiLimit($doiLimit)
    {
        $this->doi_limit = $doiLimit;
        return $this;
    }

    /**
     * Get doiLimit
     *
     * @return int $doiLimit
     */
    public function getDoiLimit()
    {
        return $this->doi_limit;
    }

    /**
     * Set doiCurrent
     *
     * @param int $doiCurrent
     * @return $this
     */
    public function setDoiCurrent($doiCurrent)
    {
        $this->doi_current = $doiCurrent;
        return $this;
    }

    /**
     * Get doiCurrent
     *
     * @return int $doiCurrent
     */
    public function getDoiCurrent()
    {
        return $this->doi_current;
    }
}
