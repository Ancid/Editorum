<?php
namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Doctrine\AsuId;
use Editorum\Bundle\Doctrine\Multilanguage;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableDocument;
use Gedmo\Timestampable\Traits\TimestampableDocument;

/**
 * Class Reference
 * @package Editorum\Bundle\Document
 *
 * @ODM\Document(collection="reference", repositoryClass="Editorum\Bundle\Document\Repository\Reference")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Reference
{
    use TimestampableDocument;
    use SoftDeleteableDocument;
    use AsuId;
    use Multilanguage;

    /** @ODM\Id(type="int", strategy="INCREMENT") */
    protected $id;

//    /**
//     * Gedmo\SortableGroup()
//     * ODM\ReferenceOne(targetDocument="Publication", simple=true, inversedBy="references")
//     */
//    private $publication;

    /**
     * @Gedmo\SortablePosition()
     * @ODM\Field(type="int")
     */
    protected $position;


    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param int $position
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * Get position
     *
     * @return int $position
     */
    public function getPosition()
    {
        return $this->position;
    }
}
