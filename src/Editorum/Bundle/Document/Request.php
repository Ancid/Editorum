<?php
namespace Editorum\Bundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Editorum\Bundle\Document\Common\AbstractCorporateEntity;
use Editorum\Bundle\Document\Common\AbstractPublication;
use Gedmo\Timestampable\Traits\TimestampableDocument;

/**
 * Class Request
 *
 * @package Editorum\Bundle\Document
 * @ODM\Document(collection="request")
 */
class Request
{
    /** Новая */
    const STATUS_NEW      = 0;
    /** Согласована/принята */
    const STATUS_APPROVED = 1;
    /** Отклонена */
    const STATUS_DECLINED = 2;
    /** Возвращена на доработку */
    const STATUS_RETURNED = 3;

    use TimestampableDocument;

    /**
     * @ODM\Id(type="int", strategy="INCREMENT")
     */
    private $id;

    /**
     * @ODM\ReferenceOne()
     */
    private $reference;

    /**
     * @var AbstractPublication
     * @ODM\ReferenceOne()
     */
    private $publication;

    /**
     * @var User
     * @ODM\ReferenceOne(targetDocument="User")
     */
    private $user;

    /**
     * @var int
     * @ODM\Field(type="int")
     */
    private $status;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $comment;

    /** @ODM\ReferenceOne() */
    protected $publisher;


    /**
     * Get id
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reference
     *
     * @param $reference
     * @return $this
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * Get reference
     *
     * @return $reference
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set publication
     *
     * @param $publication
     * @return $this
     */
    public function setPublication($publication)
    {
        $this->publication = $publication;
        return $this;
    }

    /**
     * Get publication
     *
     * @return $publication
     */
    public function getPublication()
    {
        return $this->publication;
    }

    /**
     * Set user
     *
     * @param \Editorum\Bundle\Document\User $user
     * @return $this
     */
    public function setUser(\Editorum\Bundle\Document\User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return \Editorum\Bundle\Document\User $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set status
     *
     * @param int $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return int $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return $this
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * Get comment
     *
     * @return string $comment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set publisher
     *
     * @param AbstractCorporateEntity|null $publisher
     * @return self
     */
    public function setPublisher(AbstractCorporateEntity $publisher)
    {
        $this->publisher = $publisher;
        return $this;
    }

    /**
     * Get publisher
     *
     * @return AbstractCorporateEntity $publisher
     */
    public function getPublisher()
    {
        return $this->publisher;
    }
}
