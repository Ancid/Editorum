<?php
namespace JournalsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;

class StepType extends AbstractType
{
    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'just_step';
    }
}
