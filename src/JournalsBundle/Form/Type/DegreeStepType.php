<?php
namespace JournalsBundle\Form\Type;

use Editorum\Bundle\Form\Author\DegreeType;

class DegreeStepType extends DegreeType
{
    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'degree';
    }
}
