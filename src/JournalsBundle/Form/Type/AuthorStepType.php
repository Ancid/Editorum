<?php
namespace JournalsBundle\Form\Type;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AuthorStepType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                $builder->create('ru', 'form')
                    ->add('last_name', 'text', [
                        'required'  => true
                    ])
                    ->add('first_name', 'text', [
                        'required' => true
                    ])
                    ->add('second_name', 'text', [
                        'required' => true
                    ])
            )
            ->add(
                $builder->create('en', 'form')
                    ->add('last_name', 'text', [
                        'required' => false
                    ])
                    ->add('first_name', 'text', [
                        'required' => false
                    ])
                    ->add('second_name', 'text', [
                        'required' => false
                    ])
            )
            ->add('email')
            ->add('reg_country', 'document', [
                'class' => 'Editorum\Bundle\Document\Country',
                'query_builder' => function (DocumentRepository $repository) {
                    return $repository->createQueryBuilder()
                        ->sort('position', 'asc');
                },
                'required' => true
            ])
            ->add('reg_city', 'text', [
                'required' => false
            ])
            ->add('science_rang', 'document', [
                'class'         => 'Editorum\Bundle\Document\ListStorage',
                'choice_label'  => 'ru[title]',
                'query_builder' => function (DocumentRepository $repository) {
                    $qb = $repository->createQueryBuilder();

                    return $qb->field('listName')->equals('rang');
                },
                'required'      => false,
            ])
            ->add('academic_title', 'text', [
                'required' => false
            ])
        ;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class'    => 'Editorum\Bundle\Document\Author'
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'author_step';
    }
}
