<?php
namespace JournalsBundle\Form\Type;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class ConferenceStepType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('active_conference', 'document', [
                'class'         => 'Editorum\Bundle\Document\Conference',
                'label'         => 'form.editorum.document.conference.title',
                'choice_label'  => 'ru[title]',
                'query_builder' => function (DocumentRepository $repository) {
                    $qb = $repository->createQueryBuilder();

                    $now = new \DateTime();
                    $qb
                        ->addAnd($qb->expr()->field('request_start')->lte($now))
                        ->addAnd($qb->expr()->field('request_finish')->gte($now));

                    return $qb;
                },
                'constraints'   => [
                    new NotBlank(),
                    new NotNull()
                ]
            ])
            ->add('section', 'document', [
                'class'         => 'Editorum\Bundle\Document\Section',
                'choice_label'  => 'ru[title]',
                'label'         => 'form.editorum.document.section.title',
                'required'      => true,
            ])
        ;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'conference_step';
    }
}
