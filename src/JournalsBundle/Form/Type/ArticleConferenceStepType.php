<?php
namespace JournalsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleConferenceStepType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                $builder->create('ru', 'form')
                    ->add('title', 'text', [
                        'label' => 'form.editorum.document.article.title',
                        'required'  => false
                    ])
                    ->add('keywords', 'text', [
                        'label' => 'form.editorum.document.article.keywords',
                        'required' => false
                    ])
                    ->add('annotation', 'textarea', [
                        'label' => 'form.editorum.document.article.annotation',
                        'required' => false
                    ])
            )
            ->add(
                $builder->create('en', 'form')
                    ->add('title', 'text', [
                        'label' => 'form.editorum.document.article.title',
                        'required' => true
                    ])
                    ->add('keywords', 'text', [
                        'label' => 'form.editorum.document.article.keywords',
                        'required' => true
                    ])
                    ->add('annotation', 'textarea', [
                        'label' => 'form.editorum.document.article.annotation',
                        'required' => true
                    ])
            )
            ->add('depofile', 'file', [
                'label' => 'form.editorum.document.article.depofile',
                'required' => false
            ])
//            ->add('section', 'document', [
//                'class' => 'Editorum\Bundle\Document\Section',
//                'choice_label'  => 'ru[title]',
//                'label' => 'form.editorum.document.article.section',
//                'required' => false
//            ])
        ;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults([
            'data_class' => 'Editorum\Bundle\Document\ConferenceArticle'
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'article_conference_step';
    }
}
