<?php
namespace JournalsBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class SyliusFlowPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('sylius.controller.process');

        $definition->setClass('JournalsBundle\Controller\ProcessController');
        $definition->addMethodCall('setContainer', [ new Reference('service_container') ]);
    }
}
