<?php
/**
 * Created by PhpStorm.
 * User: snowman
 * Date: 15.04.16
 * Time: 16:40
 */

namespace JournalsBundle\Controller;

use Editorum\Bundle\Controller\BasicController as EditorumBasicController;
use Editorum\Bundle\Services\Menu\GlobalMenu;

class BasicController extends EditorumBasicController
{
    /**
     * @throws \Exception
     */
    public function preExecute()
    {
        parent::preExecute();
        // set top menu
        $this->globalMenu->setInterface(GlobalMenu::INT_READ);
    }
}
