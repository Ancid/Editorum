<?php
namespace JournalsBundle\Controller;

use Editorum\Bundle\Services\Menu\GlobalMenu;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Sylius\Bundle\FlowBundle\Controller\ProcessController as BaseController;

/**
 * Class ProcessController
 *
 * @package JournalsBundle\Controller
 * @Security("is_granted('ROLE_USER')")
 */
class ProcessController extends BaseController
{
    use ContainerAwareTrait;

    /** @var GlobalMenu */
    protected $globalMenu;

    /**
     * @throws \Exception
     */
    public function preExecute()
    {
        // set top menu
        $this->globalMenu = $this->container->get('navigation.menu')->get('Global');
        $this->globalMenu->setInterface(GlobalMenu::INT_READ);
    }
}
