<?php

namespace JournalsBundle\Controller;

use Editorum\Bundle\Document\Conference;
use Editorum\Bundle\Document\Section;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProcessAjaxController extends Controller
{
    public function sectionsByConferenceAction(Conference $conference)
    {
        /** @var Section[] $sections */
        $sections = $conference->getSections();
        $msg = [];

        foreach ($sections as $section) {
            $msg[$section->getId()] = $section->getRu()['title'];
        }

        $response = new JsonResponse($msg);
        return $response;
    }
}
