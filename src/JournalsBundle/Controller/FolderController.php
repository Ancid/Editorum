<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 23.10.2015
 * Time: 14:15
 */
namespace JournalsBundle\Controller;


use Symfony\Component\HttpFoundation\Response;
use JournalsBundle\Logic\SideMenu;

class FolderController extends BasicController
{
    public function indexAction()
    {
        return $this->render('JournalsBundle:Default:folders.html.twig', array(
            'sidemenu' => SideMenu::indexMenu(),
            'active' => 'main',
        ));
    }

    public function testAction()
    {
        die();
    }


    /**
     * Переход к содержимому папки
     * @param $id
     * @return Response
     */
    public function gotofolderAction($id)
    {
        return $this->render('JournalsBundle:Default:folders.html.twig', array(
            'sidemenu' => SideMenu::indexMenu(),
            'active' => 'main',
            'goto' => $id
        ));
    }


}
