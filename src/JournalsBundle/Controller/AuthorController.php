<?php
/**
 * Created by PhpStorm.
 * User: vgfilippov
 * Date: 28.10.15
 * Time: 16:29
 */

namespace JournalsBundle\Controller;

use Editorum\Bundle\Document\Author;

class AuthorController extends BasicController
{

    /**
     * @param Author $author
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function authorAction(Author $author)
    {
        $pubs = $this->getODM()->getRepository('AsuBundle:Author')->getAllPublications($author);

        return $this->render('JournalsBundle:Default:author.html.twig', array(
            'sidemenu'  => '',
            'active'    => '',
            'author'    => $author,
            'pubs'      => $pubs,
        ));
    }

}
