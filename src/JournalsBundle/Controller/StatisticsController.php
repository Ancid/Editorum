<?php
/**
 * Created by PhpStorm.
 * User: vgfilippov
 * Date: 20.11.15
 * Time: 14:01
 */

namespace JournalsBundle\Controller;

class StatisticsController extends BasicController
{
    public function indexAction($id)
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Главная', $this->get('router')->generate('journals_index'));
        $breadcrumbs->addItem('Статистика', $this->get('router')->generate('statistics_page'));

        return $this->render('@Journals/Default/statistics.html.twig',array('sidemenu' => null,'active' => null));
    }
    
}
