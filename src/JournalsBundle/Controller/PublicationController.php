<?php
/**
 * Created by PhpStorm.
 * User: vgfilippov
 * Date: 15.10.15
 * Time: 14:10
 */

namespace JournalsBundle\Controller;

use Editorum\Bundle\Logic\Model;
use Editorum\Bundle\Logic\Publication;
use JournalsBundle\Logic\SideMenu;

class PublicationController extends BasicController
{

    public function publicationAction($id)
    {

        $publication = Model::getById(new Publication(), $id);

        if ($publication->__get('kind') == Publication::KIND_MONOGRAPHY) {
            return $this->render('@Journals/Default/monography.html.twig', array('sidemenu' => null, 'active' => null));
        } else {
            $authors = $publication->getAuthors();
            $issue = $publication->getIssue($id);
            $journal = $publication->getJournal($issue->__get('journal_id'));

            return $this->render('@Journals/Default/article.html.twig', array(
                'sidemenu' => SideMenu::journalMenu($journal),
                'article' => $publication,
                'authors' => $authors,
                'issue' => $issue,
                'journal' => $journal,
                'active' => 'main',
            ));
        }
    }

}
