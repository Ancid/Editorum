<?php

namespace JournalsBundle\Controller;

use Editorum\Bundle\Logic\Issue;
use Editorum\Bundle\Logic\Journal;
use JournalsBundle\Logic\SideMenu;
use Editorum\Bundle\Logic\Model;

class DefaultController extends BasicController
{
    public function indexAction()
    {
        $journals = $this->getODM()->getRepository('AsuBundle:Journal')->getByStatus();
        $issues = $this->getODM()->getRepository('AsuBundle:Issue')->getByStatus();
//        $issues = $this->getODM()->getRepository('AsuBundle:Issue')->findBy([
//            'statuses.is_show_nauka'    => 1
//        ], [
//            'title' => 'ASC'
//        ]);

        return $this->render('JournalsBundle:Default:index.html.twig', [
            'sidemenu'  => SideMenu::indexMenu(),
            'active'    => null,
            'journals'  => $journals,
            'issues'    => $issues,
        ]);
    }

    public function demoUserAction()
    {
        return $this->render('JournalsBundle:Default:ebs_user.html.twig', [
            'sidemenu' => SideMenu::indexMenu(),
            'active' => 'main',
        ]);
    }

    public function agreementAction()
    {
        return $this->render('@Journals/Default/agreement.html.twig', ['sidemenu' => null, 'active' => null]);
    }


    /**
     * @return \Doctrine\ODM\MongoDB\DocumentManager
     */
    public function getODM()
    {
        return $this->get('doctrine.odm.mongodb.document_manager');
    }
}
