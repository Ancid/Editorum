<?php
/**
 * Created by PhpStorm.
 * User: rakap
 * Date: 23.11.15
 * Time: 13:59
 */

namespace JournalsBundle\Controller;


class ReaderController extends BasicController
{
    public function pdfReaderAction($id)
    {
        return $this->render('@Journals/Default/reader_pdf.html.twig',array('sidemenu' => null,'active' => null));
    }

    public function epubReaderAction($id)
    {
        return $this->render('@Journals/Default/reader_epub.html.twig',array('sidemenu' => null,'active' => null));
    }
}
