<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 23.10.2015
 * Time: 14:15
 */
namespace JournalsBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Editorum\Bundle\FolderSystem\Folder;

class AjaxfolderController extends BasicController
{
    /**
     * Получает список папок пользователя
     * @return array|Response
     */
    public function ajaxfoldersAction()
    {
        $request = $this->get('request')->request->all();
        try {
            $folders = Folder::getUserFolders((int)$request['params']['user_id']);
            $tree = Folder::buildTreeFromCursor($folders);
            $response = [
                'success' => true,
                'data' => $tree,
            ];

            $pdo = null;

        } catch (Exception $e) {
            return $response = [
                'success' => false,
                'error' => "Error!: " . $e->getMessage() . "<br/>"
            ];
        }

        return new Response(json_encode($response));
    }


    /**
     * Получает содержимое папки
     * @return Response
     */
    public function ajaxcontentAction()
    {
        $request = $this->get('request')->request->all();
        $content = Folder::getContent((int)$request['params']['folder_id'], 1);

        $items = [];
        if (count($content['obj']->getDataset()) > 0) {
            foreach ($content['obj'] as $c) {
                $items[] = $c->getCurrentRecord();
            }
        }

        $response = [
            'success' => true,
            'data' => $items,
            'selected' => $content['selected'],
            'current_id' => $request['params']['folder_id'],
            'name' => $content['name'],
            'collection' => $content['collection']
        ];

        return new Response(json_encode($response));
    }


    /**
     * Отмечает выбранные элементы в MongoDB
     * @return Response
     */
    public function ajaxselectAction()
    {
        $request = $this->get('request')->request->all();
        if (array_key_exists('selected', $request['params']))
            $selected = array_map('intval', $request['params']['selected']);
        else
            $selected = array();
        Folder::updateSelect((int)$request['params']['folder_id'], $selected);
        $response = [
            'success' => true,
        ];

        return new Response(json_encode($response));
    }


    /**
     * Удаляет выбранные элементы из папки
     * @return Response
     */
    public function ajaxremovefromAction()
    {
        $request = $this->get('request')->request->all();
        $response = Folder::removeFromFolder((int)$request['params']['folder_id'], (int)$request['params']['user_id']);

        return new Response(json_encode($response));
    }


    /*
     * Возвращает список пользовательскиз папок
     */
    public function ajaxuserfoldersAction()
    {
        $request = $this->get('request')->request->all();
        $user_id = (int)$request['params']['user_id'];
        $folders = Folder::getOwnUserFolders($user_id);

        $res_folders = array();
        foreach ($folders as $f) {
            $res_folders[$f->__get('_id')] = [
                '_id' => $f->__get('_id'),
                'name' => $f->__get('name'),
            ];
        }

        $response = [
            'success' => true,
            'data' => $res_folders,
        ];
        return new Response(json_encode($response));
    }


    /**
     * Копирует из папки
     * @return Response
     */
    public function ajaxcopyfromAction()
    {
        $request = $this->get('request')->request->all();
        $dst_id = (int)$request['params']['dst_id'];
        $src_id = (int)$request['params']['src_id'];
        $user_id = (int)$request['params']['user_id'];
        $response = Folder::copyFromFolder($src_id, $dst_id, $user_id);

        return new Response(json_encode($response));
    }


    /**
     * Перемещает из папки
     * @return Response
     */
    public function ajaxmovefromAction()
    {
        $request = $this->get('request')->request->all();
        $dst_id = (int)$request['params']['dst_id'];
        $src_id = (int)$request['params']['src_id'];
        $user_id = (int)$request['params']['user_id'];
        $response = Folder::moveFromFolder($src_id, $dst_id, $user_id);

        return new Response(json_encode($response));
    }


    /**
     * Создание папки
     */
    public function ajaxCreateFolderAction()
    {
        $request = $this->get('request')->request->all();
        $id = (int)$request['params']['folder_id'];
        if (Folder::countItems($id) > 0) {
            $response['error'] = 'В папке есть произведения, нельзя создать.';
        } else {
            $response = [
                'success' => true,
                'error' => false
            ];
            Folder::queryNewFolder(
                $request['params']['folder_name'],
                (int)$request['params']['user_id'],
                null,
                array(),
                (int)$request['params']['folder_id']);
        }

        return new Response(json_encode($response));
    }


    /**
     * Удаление папки
     */
    public function ajaxRemoveFolderAction()
    {
        $request = $this->get('request')->request->all();
        $id = (int)$request['params']['folder_id'];
        $response = Folder::removeFolder($id);

        return new Response(json_encode($response));
    }

    /**
     * Создание папки
     */
    public function ajaxChangeFolderAction()
    {
        $request = $this->get('request')->request->all();
        $id = (int)$request['params']['folder_id'];
        $name = $request['params']['folder_name'];

        $response = Folder::changeFolder($id, $name);

        return new Response(json_encode($response));
    }

}
