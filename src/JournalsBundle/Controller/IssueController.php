<?php
/**
 * Created by PhpStorm.
 * User: vgfilippov
 * Date: 14.10.15
 * Time: 17:12
 */

namespace JournalsBundle\Controller;

use Editorum\Bundle\Document\Issue;

class IssueController extends BasicController
{
    /**
     * @param Issue $issue
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function issueAction(Issue $issue)
    {
        return $this->render('JournalsBundle:Default:issue.html.twig', [
                'sidemenu'  => $this->get('navigation.menu')->get('NaukaSide')
                    ->getMenu('journal', [$issue->getJournal()]), 'active'   => 'main', 'issue' => $issue,
        ]);
    }

}
