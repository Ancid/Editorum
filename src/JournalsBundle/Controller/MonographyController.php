<?php
/**
 * Created by PhpStorm.
 * User: vgfilippov
 * Date: 20.11.15
 * Time: 14:01
 */

namespace JournalsBundle\Controller;


class MonographyController extends BasicController
{
    public function indexAction($id)
    {
        return $this->render('@Journals/Default/monography.html.twig', array('sidemenu' => null, 'active' => null));
    }
}
