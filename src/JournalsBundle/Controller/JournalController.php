<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 22.09.2015
 * Time: 15:38
 */

namespace JournalsBundle\Controller;

use Editorum\Bundle\Logic\Author;
use Editorum\Bundle\Logic\Model;
use Editorum\Bundle\Logic\Issue;
use Editorum\Bundle\Logic\Serie;
use JournalsBundle\Logic\SideMenu;
use Editorum\Bundle\Document\Journal;

class JournalController extends BasicController
{
    /**
     * @param Journal $journal
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function journalAction(Journal $journal)
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Главная', $this->get('router')->generate('journals_index'));
        $breadcrumbs->addItem('Журналы', $this->get('router')->generate('nauka_journal'));

        return $this->render('@Journals/Default/journal/journal.html.twig', array(
            'sidemenu'  => $this->get('navigation.menu')->get('NaukaSide')->getMenu('journal', [$journal]),
            'active'    => 'main',
            'journal'   => $journal,
        ));
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function alphabetAction()
    {
        $journals = $this->getODM()->getRepository('AsuBundle:Journal')->findBy([], ['title' => 'ASC']);

        return $this->render('JournalsBundle:Default:order_alphabet.html.twig', array(
            'sidemenu' => $this->get('navigation.menu')->get('NaukaSide')->getMenu('order', []),
            'journals' => $journals,
        ));
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function seriesAction()
    {
        $series = Model::getByKeys(new Serie(), array(), 'title', 1);

        return $this->render('JournalsBundle:Default:series_order.html.twig', array(
            'series' => $series,
            'sidemenu' => $this->get('navigation.menu')->get('NaukaSide')->getMenu('order', []),
        ));
    }


    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function archiveAction($id)
    {
        $journal = Model::getById(new Journal(), $id);
        $list = Issue::loadList($id, false, 'dt_published', -1);
        $issues = Model::getList(new Issue(), $list);

        return $this->render('JournalsBundle:Default:journal.html.twig', array(
            'sidemenu' => SideMenu::journalMenu($journal),
            'active' => 'main',
            'journal' => $journal,
            'issues' => $issues,
        ));
    }


    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function journalAuthorsAction($id)
    {
        $journal = Model::getById(new Journal(), $id);
        $authors = $journal->getAuthors();
        $object = new Author();

        return $this->render('JournalsBundle:Default:journal_authors.html.twig', array(
            'sidemenu' => SideMenu::journalMenu($journal),
            'active' => 'main',
            'authors' => $authors,
            'object' => $object,
        ));

    }


    /**
     * @param $f1
     * @param $f2
     * @return int
     */
    protected function mySort($f1, $f2)
    {
        if ($f1['fio'] < $f2['fio']) return -1;
        elseif ($f1['fio'] > $f2['fio']) return 1;
        else return 0;
    }

    /**
     * @return \Doctrine\ODM\MongoDB\DocumentManager
     */
    public function getODM()
    {
        return $this->get('doctrine.odm.mongodb.document_manager');
    }
}
