<?php
/**
 * Created by PhpStorm.
 * User: rak_ap
 * Date: 07.12.2015
 * Time: 16:52
 */

namespace JournalsBundle\Controller;

use Editorum\Bundle\Logic\Temporary;
use Editorum\Bundle\Logic\Model;

class EncyclopediaController extends BasicController
{
    public function encyclopediaAction($type)
    {
        switch ($type) {
            case 'rus':
                $title = 'Новая Российская Энциклопедия';
                break;
            case 'urist':
                $title = 'Юридическая Энциклопедия';
                break;
        }
        return $this->render('JournalsBundle:Default:encyclopedia.html.twig', array(
            'articles' => Temporary::loadList(),
            'title' => $title,
            'type' => $type
        ));
    }

    public function articleAction($id)
    {
        $article = Model::getById(new Temporary(), (int)$id);
        return $this->render('JournalsBundle:Default:encyclopedia_article.html.twig', array(
            'article' => $article
        ));
    }

}
