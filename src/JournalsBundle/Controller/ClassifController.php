<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 09.11.2015
 * Time: 13:06
 */
namespace JournalsBundle\Controller;

use Editorum\Bundle\Logic\Publication;
use Editorum\Bundle\Logic\Classif;

class ClassifController extends BasicController
{
    public function listAction($type, $classif_type)
    {
        if (!is_null($classif_type) && !is_null($type)) {
            switch ($classif_type) {
                case 'grnti':
                    $title = 'по ГРНТИ';
                    break;
                case 'okso_journal':
                    $title = 'по ОКСО';
                    break;
                case 'bbk':
                    $title = 'по ББК';
                    break;
                default:
                    $title = 'по чему-то';
            }
            switch ($type) {
                case 'publication':
                    $pre_title = 'Список произведений ';
                    break;
                case 'journal':
                    $pre_title = 'Список журналов ';
                    break;
                default:
                    $pre_title = 'Список чего-то ';
            }
            $classifs = Classif::loadClassif([$classif_type]);

            return $this->render('JournalsBundle:Default:classif_list.html.twig', array(
                'title' => $pre_title . $title,
                'sidemenu' => null,
                'active' => null,
                'classif' => $classifs[0],
                'object' => new Classif(),
                'collection' => $type,
            ));
        }
    }

    public function codeAction($type, $classif_type, $code)
    {
        if (!is_null($classif_type) && !is_null($code)) {
            switch ($classif_type) {
                case 'grnti':
                    $title = 'ГРНТИ';
                    break;
                case 'okso_journal':
                    $title = 'ОКСО';
                    break;
                case 'bbk':
                    $title = 'ББК';
                    break;
                default:
                    $title = 'какой-то код';
            }
            switch ($type) {
                case 'publication':
                    $pre_title = 'Список произведений ';
                    break;
                case 'journal':
                    $pre_title = 'Список журналов ';
                    break;
                default:
                    $pre_title = 'Список чего-то ';
            }
            return $this->render('JournalsBundle:Default:classif_code.html.twig', array(
                'title' => $pre_title . 'по коду ' . $code . ' (' . $title . ')',
                'sidemenu' => null,
                'active' => null,
                'classif_type' => $classif_type,
                'code' => $code,
                'object' => new Classif(),
                'collection' => $type,
                'kinds' => Publication::$Kinds,
            ));
        }
    }
}
