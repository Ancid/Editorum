<?php
/**
 * Created by PhpStorm.
 * User: rak_ap
 * Date: 28.01.2016
 * Time: 14:52
 */

namespace JournalsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ErrorsController extends Controller
{
    public function notfoundAction()
    {
        return $this->render('@Journals/Default/404.html.twig',array('sidemenu' => null,'active' => null));
    }
}