<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 22.09.2015
 * Time: 15:38
 */

namespace JournalsBundle\Controller;

use Editorum\Bundle\Logic\Author;
use Editorum\Bundle\Logic\Model;
use Editorum\Bundle\Logic\Issue;
use Editorum\Bundle\Logic\Serie;
use JournalsBundle\Logic\SideMenu;
use Editorum\Bundle\Document\Journal;

class ChapterController extends BasicController
{
    public function indexAction($id)
    {
        return $this->render('@Journals/Default/monography.html.twig', array('sidemenu' => null, 'active' => null));
    }
}
