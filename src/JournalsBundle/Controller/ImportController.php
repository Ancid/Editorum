<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 01.12.2015
 * Time: 10:43
 */

namespace JournalsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ImportController extends Controller
{
    public function jsonUppAction()
    {
        $pdo = $this->getPdo();
        $stmt = $pdo->query(<<<'SQL'

            SELECT * from upp_product
                    p.id,
                    p.user_id,
                    p.has_images_owned,
                    p.depofile,
                    p.comment,
                    p.has_reviewers,
                    p.has_published,
                    p.has_agreement,
                    p.issue_id,
                    p.issue_rubric_id,
                    p.issue_iorder,
                    p.is_finalized,
                    p.max_dt,
                    p.author_pages,
                    p.grnti,
                    p.show_znanium,
                    p.publisher_id,
                    p.st_agreement_printed,
                    p.published_journal,
                    p.published_year,
                    p.published_issue,
                    p.published_url,
                    p.edu_type,
                    p.grif,
                    p.show_nauka,
                    p.isbn_online,
                    p.parent_id,
                    p.parent_order,
                    p.published_page_total,
                    p.udk,
                    p.rinc_url,
                    p.worktitle as title,
                    p.eng_worktitle as eng_title,
                    p.lang, p.isbn,
                    p.dt_published as dt_publication_print,
                    p.published_page_from as first_page, p.published_page_to as last_page, p.doi, p.url as uri, p.is_published,
                    p.keywords, p.eng_keywords, p.annotation, p.eng_annotation,p.dt_create as date_create,p.dt_agree as date_agree,p.text,
                    p.journal, p.kind,
                    null as authors from NPub as p where p.kind in (100, 102) group by p.id) as t group by id
SQL
);

        $pubs = $stmt->fetchAll(PDO::FETCH_ASSOC);

        file_put_contents('rubricator/authors.json', json_encode($pubs));

        var_dump('Success!');
        die();
    }


    protected function getPdo()
    {
        $host = $this->container->getParameter('database_host');
        $db_name = $this->container->getParameter('upp_database_name');
        $user = $this->container->getParameter('database_user');
        $pass = $this->container->getParameter('database_password');

        try {
            $pdo = new PDO("mysql:host={$host};dbname={$db_name}", $user, $pass);
            $pdo->exec("set names utf8");
        } catch (PDOException $e) {
            $pdo = "Error!: " . $e->getMessage() . "<br/>";
        }

        return $pdo;
    }
}
