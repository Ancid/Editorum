<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 19.10.2015
 * Time: 14:45
 */

namespace JournalsBundle\Logic;

use Editorum\Bundle\FolderSystem\Folder;
use Editorum\Bundle\Logic\Author;
use Editorum\Bundle\Logic\Journal;
use Editorum\Bundle\Logic\Model;
use Editorum\Bundle\Logic\Publication;
use JournalsBundle\Search\SphinxResult;

class Search {

    /**
     * @param $search_txt
     * @return array
     */
    public static function smartSearch($search_txt)
    {
        $response = [
            'success'       => true,
            'errors'        => null
        ];
        $res = SphinxResult::getSphinxResult($search_txt, 500);

        $response['all_count'] = count($res['suggestions']);

        $new_res = array();
        $kind = Journal::KIND_JOURNAL;

        if (!array_key_exists('suggestions', $res)) {
            $response['errors'] = 'empty results!';
            return $response;
        }

        //Разбираем результаты по группам
        foreach ($res['suggestions'] as $item) {
            //Сохраняем только произведения
//            if (in_array($item['kind'], [101, 102, 200])) {
                if (in_array($item['kind'], [Publication::KIND_ARTICLE, Publication::KIND_MONOGRAPHY])) {
                    $new_res[Publication::KIND_PUB]['items'][] = $item;
                } elseif ($item['kind'] == $kind) {
                    $new_res[$kind]['items'][] = $item;
                } elseif ($item['kind'] == 5000) {
                    $new_res[5000]['items'][] = $item;
                } else {
                    $new_res[$item['kind']]['items'][] = $item;
                }
//            }
        }

        foreach ($new_res as $kind => &$group) {
            $group['count'] = count($group['items']);
            switch ($kind) {
                case Publication::KIND_PUB:
                    $collection = 'publication';
                    break;
                case Publication::KIND_ARTICLE:
                    $collection = 'publication';
                    break;
                case Publication::KIND_MONOGRAPHY:
                    $collection = 'publication';
                    break;
                case Journal::KIND_JOURNAL:
                    $collection = 'journal';
                    break;
                case Author::KIND_AUTHOR:
                    $collection = 'author';
                    break;
            }
            $items = array();
            foreach ($group['items'] as $item) {
                $items[] = $item['id'];
            }
            //Создаем папки только с произведениями
            if (in_array($kind, [Publication::KIND_PUB, Journal::KIND_JOURNAL])) {
                $id = Folder::addSearchFolder($search_txt, 1, $collection, $items);
                $group['link'] = '/folder/search/'.$id.'/';
            }
            array_splice($group['items'], 10);
        }
        $response['data'] = $new_res;

        return $response;
    }


    public static function pubFilter($search_txt)
    {
        $response = [
            'success'       => true,
            'errors'        => null,
            'matches'       => false
        ];
        $res = SphinxResult::getPubFilter($search_txt);

        if ($res !== false) {
            $response['matches'] = array_keys($res);
        }
        return $response;
    }


    public static function getIds($search_txt, $kind = false)
    {
        $response = [
            'success'       => true,
            'errors'        => null,
            'matches'       => [false]
        ];
        $res = SphinxResult::getPubFilter($search_txt, $kind);

        if ($res !== false) {
            foreach ($res as $item) {
                $response['matches'][] = $item['attrs']['_id'];
            }
        }
        return $response;
    }

    
    public static function getAuthors($search_txt)
    {
        $response = [
            'success'       => true,
            'errors'        => null,
            'matches'       => [false]
        ];
        $res = SphinxResult::getPubFilter($search_txt);
        // @ToDo Индекс не работает
//        $res = SphinxResult::getAuthorFilter($search_txt);

        if ($res !== false) {
            foreach ($res as $item) {
                $response['matches'][] = $item['attrs']['_id'];
            }
        }
        return $response;
    }
}