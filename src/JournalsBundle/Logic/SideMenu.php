<?php
/**
 * Created by PhpStorm.
 * User: sidorov_ar
 * Date: 22.09.2015
 * Time: 15:24
 */
namespace JournalsBundle\Logic;

class SideMenu {

    public static function journalMenu($journal)
    {
        $blocks = array();

        if (NULL !== $journal->__get('_id'))
            $id = $journal->__get('_id');
        else
            $id = 0;

        $blocks[] = array(
            'block_title' => 'Информация о журнале',
            'links' => array(
                'main' => array(
                    'title'         => 'О журнале',
                    'url'           => str_replace('%id%', $id, '/nauka/%id%/')
                ),
                'ethics' => array(
                    'title'         => 'Этика научных публикаций',
                    'url'           => '#'
                ),
                'article_order' => array(
                    'title'         => 'Порядок опубликования статей',
                    'url'           => '#'
                ),
                'requirements' => array(
                    'title'         => 'Требования к статьям',
                    'url'           => '#'
                ),
                'editorial' => array(
                    'title'         => 'Редакционный совет и (или) редколлегия',
                    'url'           => '#'
                ),
                'editorial_geo' => array(
                    'title'         => 'География редсовета и (или) редколлегии журнала',
                    'url'           => '#'
                ),
                'author_geo' => array(
                    'title'         => 'География авторов журнала',
                    'url'           => '#'
                ),
                'all_authors' => array(
                    'title'         => 'Все авторы журнала',
                    'url'           => str_replace('%id%', $id, '/nauka/%id%/authors')
                ),
                'issue_archive' => array(
                    'title'         => 'Архив номеров',
                    'url'           => str_replace('%id%', $id, '/nauka/%id%/archive')
                )
            )
        );

        $blocks[] = array(
            'block_title' => 'Услуги',
            'links' => array(
                'subscribe' => array(
                    'title'         => 'Оформить подписку',
                    'url'           => '#'
                ),
                'subscribe_news' => array(
                    'title'         => 'Подписаться на новости журнала',
                    'url'           => '#'
                )
            )
        );

        return $blocks;
    }

    public function orderMenu()
    {
        $blocks = array();

        $blocks[] = array(
            'block_title' => 'Услуги',
            'links' => array(
                'subscribe' => array(
                    'title'         => 'Оформить подписку',
                    'url'           => '#'
                ),
                'subscribe_news' => array(
                    'title'         => 'Подписаться на новости журнала',
                    'url'           => '#'
                )
            )
        );

        return $blocks;
    }

    public static function indexMenu()
    {
        $blocks = array();

        $blocks[] = array(
            'block_title' => 'Наши услуги',
            'links' => array(
                'subscribe' => array(
                    'title'         => 'Оформить подписку',
                    'url'           => '#'
                ),
                'subscribe_news' => array(
                    'title'         => 'Подписаться на новости журнала',
                    'url'           => '#'
                )
            )
        );

        return $blocks;
    }
}
