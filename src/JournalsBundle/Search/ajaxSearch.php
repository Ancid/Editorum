<?php

include('SphinxAPI.php');
include('sphinxSearch.php');


function connect()
{
    $host = 'localhost';
    $db_name = 'znanium';
    $user = 'root';
    $pass = '023901';

    try {

        $pdo = new PDO("mysql:host={$host};dbname={$db_name}", $user, $pass);

    } catch (PDOException $e) {

        $pdo = "Error!: " . $e->getMessage() . "<br/>";
    }
    return $pdo;
}


function disconnect($pdo)
{
    $pdo = null;
}


function getResponse($data)
{
    $response = [
        'success' => true,
        'data' => false
    ];

    $pdo = connect();


    /**
     * Создает временную таблицу с результатами поиска
     */
    $limit = ($data['type'] == 'auto') ? 100 : $data['limit'];
    $count = sphinxResultTemporary($pdo, $data['query'], $limit);

        /**
         * Выбираем результат из временной таблицы
         */
        $stmt = $pdo->query('
SELECT p.title, p.discr, p.id
FROM publication p
JOIN temporary_data td on p.id = td.any_id
');
        $result = $stmt->fetchAll();
        $response['count'] = $count;

    if ($data['type'] === 'auto') {
        //Если это запрос с подсказками, возвращаем массив с названиями
        foreach ($result as $publication) {
            $response['suggestions'][] = [
                'value' => $publication['title'],
                'data' => [
                    'category' => $publication['discr']
                ]
            ];
        }
    } elseif ($data['type'] === 'smart_search') {
        //Если это поисковой запрос
        $now = new DateTime('now');

        //Проверяем был ил уже такой поиск
        $stmt = $pdo->prepare('
SELECT id
FROM folders
WHERE name = :title
LIMIT 1
');
        $stmt->bindValue(':title', $data['query'], PDO::PARAM_STR);
        $stmt->execute([':title' => $data['query']]);
        $already = $stmt->fetch(PDO::FETCH_COLUMN);

        //Если такой запрос уже есть,просто возвращаем id папки
        if ($already === false) {

            $stmt = $pdo->prepare('
SELECT COUNT(id)
FROM folders
WHERE parent_id = :folder_id
');
            $stmt->execute([':folder_id' => $data['sfolder_id']]);
            $folder_count = $stmt->fetch(PDO::FETCH_COLUMN);

            if (9 < $folder_count) {

                //Удаляем данные 1 поиска
                $stmt = $pdo->prepare('
SELECT f.id
FROM folders f
JOIN search_info se ON f.id = se.folder_id
WHERE f.parent_id = :folder_id
ORDER BY se.search_date ASC
LIMIT 1
');
                $stmt->execute([':folder_id' => $data['sfolder_id']]);
                $folder_id = $stmt->fetch(PDO::FETCH_COLUMN);

                //Удаляем инфо сиарого запроса
                $stmt = $pdo->prepare('
DELETE FROM search_info
WHERE folder_id = :folder_id
');
                $stmt->execute([':folder_id' => $folder_id]);

                //Удаляем старые рузельтаты поиска
                $stmt = $pdo->prepare('
DELETE FROM publication_folders
WHERE folder_id = :folder_id
    ');
                $stmt->execute([':folder_id' => $folder_id]);

                //Удаляем папку
                $stmt = $pdo->prepare('
DELETE from folders
WHERE id = :folder_id
');
                $stmt->bindValue(':folder_id', $folder_id, PDO::PARAM_INT);
                $stmt->execute();
            }

            //Добавляем новую папку
            $stmt = $pdo->prepare('
INSERT INTO folders (name, user_id, parent_id, system) VALUES
(:title, :user_id, :parent_id, 1)
');
            $stmt->bindValue(':title', $data['query'], PDO::PARAM_STR);
            $stmt->bindValue(':user_id', $data['user_id'], PDO::PARAM_INT);
            $stmt->bindValue(':parent_id', $data['sfolder_id'], PDO::PARAM_INT);
            $stmt->execute();
            $folder_id = $pdo->lastInsertId();

            //Сохраняем данные поиска
            $stmt = $pdo->prepare('
INSERT INTO search_info (folder_id, search_text, search_date, results_count) VALUES
(:folder_id, :search_text, :datetime, :results)
');
            $stmt->bindValue(':folder_id', $folder_id, PDO::PARAM_INT);
            $stmt->bindValue(':search_text', $data['query'], PDO::PARAM_STR);
            $stmt->bindValue(':datetime', date('Y-m-d H:i:s', $now->getTimestamp()), PDO::PARAM_STR);
            $stmt->bindValue(':results', $count, PDO::PARAM_INT);
            $stmt->execute();

            //Сохраняем результат в папку
            $query = '
INSERT INTO publication_folders (publication_id, folder_id, selected) VALUES
';

            $response['last_insert_id'] = $folder_id;
            //Удаляем лишние результаты,если есть
            array_splice($result, $data['limit']);
            $first = true;
            foreach ($result as $publication) {

                if (!$first) {
                    $query .= ", ";
                }
                $query .= " (".$publication['id'].", ".$folder_id.", 0) \n";
                $first = false;
            }
            $pdo->query($query);

        } else {
            $response['last_insert_id'] = $already;
        }
    }

    disconnect($pdo);

    return $response;
}


if (isset($_POST) && !empty($_POST['query'])) {
    echo json_encode(getResponse($_POST));
}