<?php
namespace JournalsBundle\Search;

use Editorum\Bundle\Logic\Author;
use Editorum\Bundle\Logic\Classif;
use Editorum\Bundle\Logic\Journal;
use Editorum\Bundle\Logic\Publication;
use Symfony\Component\Serializer\Tests\Model;


include('SphinxAPI.php');

class SphinxResult
{
    /**
     * @param $search_text
     * @param int $limit
     * @return array
     */
    static function getSphinxResult($search_text, $limit = 30)
    {
        $cl = new \SphinxClient();
        $cl->SetMatchMode(SPH_MATCH_ALL);
        $cl->SetRankingMode(SPH_RANK_PROXIMITY_BM25);
        $cl->SetSortMode(SPH_SORT_RELEVANCE);
//            $cl->SetGroupBy('discr', SPH_GROUPBY_ATTR); //Оставляет по 1 экземпляру группы???
        $cl->setLimits(0, $limit);

        $result = $cl->Query($search_text, 'editorum');

//	    var_dump($result);die();

        $category = [
            Publication::KIND_ARTICLE       => 'Статьи',
            Publication::KIND_MONOGRAPHY    => 'Монографии',
            Publication::KIND_ISBN          => 'ISBN',
            Journal::KIND_JOURNAL           => 'Журналы',
            Journal::KIND_ISSN              => 'ISSN',
            Author::KIND_AUTHOR             => 'Авторы',
            Classif::KIND_GRNTI             => 'Классификатор ГРНТИ',
            Classif::KIND_OKSO_J            => 'Классификатор ОКСО',
            5000                            => 'Эниклопедия ЭБС',
        ];

//	    var_dump($search_text);die();


        $response = [
            'suggestions' => []
        ];
        if (isset($result["matches"]) && count($result["matches"]) > 0) {
            $matches = $result['matches'];

            usort($matches, function ($b, $c){
                if ($b['attrs']['kind'] == $c['attrs']['kind']) {
                    return 0;
                }
                return ($b['attrs']['kind'] < $c['attrs']['kind']) ? -1 : 1;
            });

            foreach ($matches as $key => $match) {
                $response['suggestions'][] = [
                    'id' => (int)$match['attrs']['_id'],
                    'value' => $match['attrs']['title'],
                    'kind' => (int)$match['attrs']['kind'],
                    'data' => [
                        'category' => $category[$match['attrs']['kind']]
                    ]
                ];
            }
        }

        return $response;
    }


    public static function getPubFilter($search_text, $kind = false)
    {
        $cl = new \SphinxClient();
        $cl->SetMatchMode(SPH_MATCH_ALL);
        $cl->SetRankingMode(SPH_RANK_PROXIMITY_BM25);
        $cl->SetSortMode(SPH_SORT_RELEVANCE);

        if ($key = array_search($kind, Publication::$Kinds)) {
            $cl->SetFilter('kind', [$key]);
        }
//        $cl->setLimits(0, $limit);

        $result = $cl->Query($search_text, 'editorum');
        if (isset($result["matches"]) && count($result["matches"]) > 0) {
            return $result["matches"];
        } else {
            return false;
        }
    }


    public static function getAuthorFilter($search_text)
    {
        $cl = new \SphinxClient();
        $cl->SetMatchMode(SPH_MATCH_ALL);
        $cl->SetRankingMode(SPH_RANK_PROXIMITY_BM25);
        $cl->SetSortMode(SPH_SORT_RELEVANCE);

        $result = $cl->Query($search_text, 'editorum_authors');
        if (isset($result["matches"]) && count($result["matches"]) > 0) {
            return $result["matches"];
        } else {
            return false;
        }
    }
}