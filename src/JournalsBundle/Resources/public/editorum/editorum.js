/**
 * Created by sidorov_ar on 23.10.2015.
 */

var Editorum = function() {

    var that = this;

    /**
     * Инициализация приложения
     */
    this.init = function () {
        //that.current_page = 1;
        //that.current_search_id = 0;
        //this.undo = function () {
        //};
        //this.use_scrolling = false;
        //this.in_work = false;

        //this.user = new User();
        //this.user.authorize();

        this.data = new Data();
        this.folders = new Folders();
        //this.data.clear();
        //this.db = new Db();
        //this.folderActions = new Actions();
        //
        //this.search = new Search();
    }


    this.setHandlers = function()
    {
        $('#folder_content').find('input').on('click', function(){
            that.folders.checkPublications(this.value, this.checked);
        });

        $('.folderTitle').on('click', function(e){
            e.preventDefault();
        });

        $('#appSelectAll').off();
        $('#appSelectAll').on('click', function(){
            that.folders.selectAll(true);
        });

        $('#appDeselectAll').off();
        $('#appDeselectAll').on('click', function(){
            that.folders.selectAll(false);
        });

        $('#appFolderRemove').off();
        $('#appFolderRemove').on('click', function(){
            that.folders.removeFromFolder();
        });

        that.folders.setUserFolders();

        $('#appCopyTo').children().each( function(){
            $(this).off();
            $(this).on('click', function(){
                that.folders.copyFromFolder($(this).attr('id'));
            });
        });
        $('#appMoveTo').children().each( function(){
            $(this).off();
            $(this).on('click', function(){
                that.folders.moveFromFolder($(this).attr('id'));
            });
        });

        that.folders.setUserFoldersButton('appUserFolders');
    }
}