/**
 * Created by sidorov_ar on 23.10.2015.
 */
var Folders = function() {

    var that = this;

    this.url_get_folders = '/ajaxfolders';
    this.url_content = '/ajaxcontent';
    this.url_select = '/ajaxselect';
    this.url_rem_folder = '/ajaxremovefrom';
    this.url_copy_folder = '/ajaxcopyfrom';
    this.url_move_folder = '/ajaxmovefrom';
    this.url_user_folder = '/ajaxuserfolders';
    this.url_create_folder = '/ajaxcreatefolder';
    this.url_remove_folder = '/ajaxremovefolder';
    this.url_change_folder = '/ajaxchangefolder';
    this.current_folder_id = false;
    this.current_user_id = 1;
    this.folder_name = '';

    var connected = false;

    var tree = $('#tree'),
        folderContent = $('#folder_content'),
        folderTitle = $('#appFolderTitle'),
        counter = $('#appSelectedPub');

    this.folderActions = new Actions(this);

    /**
     * Функция для отправки ajaz- запроса
     */
    this.customAjax = function(url, parameters, callback)
    {
        $.ajax({
            url: url,
            async: false,
            type: 'POST',
            data: {
                params: parameters
            },
            dataType: 'json'
        }).done(function(data){
            if (callback && typeof(callback) === "function") {
                callback(data);
            }
        });
    }


    /**
     * Задаем список папок
     */
    this.setFolders = function()
    {
        var parameters = {
            user_id: 1
        };
        that.customAjax(that.url_get_folders, parameters, function(data){
            editorum.data.save('folders', JSON.stringify(data));
        });
    }


    /**
     * Получает список папок
     */
    this.getFolders = function()
    {
        //var folders = JSON.parse(editorum.data.load('folders'));
        //if (folders == null) {
            that.setFolders();
        //}

        return JSON.parse(editorum.data.load('folders'));
    }


    this.treeInit = function () {
        //Инициализируем дерево папок
        tree.treed({openedClass: 'glyphicon-folder-open', closedClass: 'glyphicon-folder-close'});

        //запрос по клику только если нет дочерних папок
        $('.e_folder').each( function(){
            $(this).on('click', function(){
                that.showFolderContent($(this).attr('id'));
            });
        });
    }


    /**
     * Отображает список папок
     */
    this.showFolders = function () {
        var types = {
            publication: 'произведения',
            journal: 'журналы'
        };
        //znanium.clearPage();
        tree.empty();
        //znanium.clearTitle();

        var folder_len = 1;
        var search_parent_id = false;

        function listFolders(my_array) {
            var folder_list = '';
            //var tmp_arr = [];

            for (var i in my_array) {
                if (my_array[i]) {
                    var item = my_array[i];
                    if (item.parent == null) search_parent_id = false;
                    if (item.name == 'Папка поиска') search_parent_id = item.id;

                    if (typeof item['childrens'] !== 'undefined' && item['system'] !== '0') {
                        folder_len = Object.keys(item['childrens']).length;
                    }

                    if (typeof item['childrens'] === 'undefined') {

                            folder_list += '<li><a href="#" class="folderTitle e_folder" id="' + item['_id'] + '">' + item['name']

                        //if (search_parent_id != false && item.system == 0)
                        if (search_parent_id != false)
                            folder_list += ' (' + types[item['collection']] +')';
                        if(item['system'] === 0) {
                            folder_list += '</a><span class = "glyphicon glyphicon-cog" data = ' + item['_id'] + '></span></li>';
                        }else{
                            folder_list += '</a></li>';
                        }
                    } else {
                        if(item['system'] === 0) {
                            folder_list += '<li><a href="#" class="folderTitle" id="' + item['_id'] + '">' + item['name'] + '</a><span class = "glyphicon glyphicon-cog" data = ' + item['_id'] + '></span>';
                        }else {
                            folder_list += '<li><a href="#" class="folderTitle" id="' + item['_id'] + '">' + item['name']
                        }
                        if (typeof item['childrens'] !== 'undefined') {
                            folder_list += '<ul>';
                            folder_list += listFolders(item['childrens']);
                            folder_list += '</ul>';
                        }
                        folder_list += '</li>';
                    }
                }
            }
            return folder_list;
        }

        var folders = that.getFolders();
        var folders_html = listFolders(folders.data);
        tree.text();
        tree.append(folders_html);
    }


    this.showFolderContent = function(folder_id)
    {
        //this.closeMoveFolderList();
        //this.closeCopyFolderList();
        //if (typeof znanium.current_folder === 'undefined') {
        //    alert('Неопределена папка current_folder');
        //}
        var parameters =  {
            folder_id: folder_id
            //'page': znanium.current_page
        };

        that.customAjax(that.url_content, parameters, function(pubs){
            if (pubs.success == true) {
                that.showTitle(pubs.name);
                if (pubs.data.length === 0) {
                    that.showEmpty();
                } else {
                    that.showPublications(pubs);
                    editorum.setHandlers();
                }
            }
        });
    }


    /**
     * Загружаем результаты конкретной папки
     * @param folder
     */

    this.showPublications = function(publications)
    {
        if (typeof publications.data != 'undefined' && publications.data.length > 0) {
            var tmpl = $('#template-folder-item').html();

            function showContent(my_array, collection) {
                $.each(my_array, function(index, value){
                    var row = $(tmpl),
                        id = my_array[index]._id,
                        checked;

                    publications.selected.indexOf(my_array[index].title)>= 0 ? checked = true : checked = false;

                    var title = 'must be title';
                    if (collection == 'publication')
                        title = my_array[index].ru.title;
                    else
                        title = my_array[index].title;

                    row.attr('id', id)
                        .find(':checkbox').val(id).prop('checked', checked).end()
                        .find('a').attr('href', '/nauka/' + collection + '/' + id).text(title);
                    folderContent.append(row);
                });
            }

            that.current_folder_id = publications.current_id;
            //publications.data.forEach(showContent, publications.collection);
            folderContent.empty();
            showContent(publications.data, publications.collection);
            that.showPubCount();
            counter.removeClass('hide');
            this.uncheck();
        }
    }


    /**
     * Функция срабатывает на checkbox в списке произведений, отмечает или снимает отметку
     * @param id
     * @param checekd
     */
    this.checkPublications = function()
    {
        that.showPubCount();

        var selected = new Array();
        folderContent.find('input:checked').each(function(){
            selected.push(this.value);
        });
        var parameters = {
            folder_id: that.current_folder_id,
            selected: selected
        };
        that.customAjax(that.url_select, parameters);
    }


    this.showEmpty = function()
    {
        folderContent.empty();
        counter.empty().addClass('hide');
        folderContent.append('<p>Папка пуста</p>');
    }


    this.showTitle = function(title)
    {
        folderTitle.empty();
        folderTitle.append('<h3>Папка "' + title + '"</h3>');
    }


    /**
     * Выделить все видимые произведения
     */
    this.selectAll = function(checked)
    {
        folderContent.find(':checkbox').prop('checked', checked).change();
        //Добавляем класс к выделенным
        //if (checked) {
        //    folderContent.find('div[id*="item"]').addClass('alert-success');
        //} else {
        //    folderContent.find('div[id*="item"]').removeClass('alert-success'');
        //}
        that.checkPublications();
    }

    /**
     * Снять выделение
     */
    this.uncheck = function(){
        folderContent.find(':checkbox').change(function(){
            var checkbox = $(this).closest('.checkbox'),
                parent = $(this).closest('li');

            if($(this).is(':checked')){
                checkbox.addClass('checked');
                parent.addClass('alert-success');
            }else{
                checkbox.removeClass('checked');
                parent.removeClass('alert-success');
            }
        });
    }


    /**
     * Считает и выводит количество выделенных произведений в списке результатов
     */
    this.showPubCount = function()
    {
        var selected = that.getSelected();
        var checked_count = selected.length;
        counter.text('Выделено ' + checked_count + ' объектов');
    }


    /**
     * Возвращает массив выбранных объектов в папке
     */
    this.getSelected = function()
    {
        var selected = new Array();
        folderContent.find('input:checked').each(function(){
            selected.push(parseInt(this.value));
        });
        return selected;
    }

    
    /**
     * удаляет выбранные объекты из папки
     */
    this.removeFromFolder = function()
    {
        var parameters = {
            folder_id: that.current_folder_id,
            user_id: that.current_user_id
        }
        that.customAjax(that.url_rem_folder, parameters, function(data) {
            if (data.error != false) alert(data.error);
        });
        that.showFolderContent(that.current_folder_id);
    }


    /**
     * Копирует выбранные объекты в папку
     */
    this.copyFromFolder = function(dst_id)
    {
        var parameters = {
            src_id: that.current_folder_id,
            dst_id: dst_id,
            user_id: that.current_user_id
        }
        that.customAjax(that.url_copy_folder, parameters, function(data){
            if (data.msg != false){
                $.bootstrapGrowl(data.msg, {
                    type: 'success',
                    align: 'right',
                    width: 300,
                    stackup_spacing: 30
                });
            }
            if (data.error != false) {
                $.bootstrapGrowl(data.error, {
                    type: 'danger',
                    align: 'right',
                    width: 300,
                    stackup_spacing: 30
                });
            }
        });
    }

    /**
     * Перемещает выбранные объекты в папку
     */
    this.moveFromFolder = function(dst_id)
    {
        var parameters = {
            src_id: that.current_folder_id,
            dst_id: dst_id,
            user_id: that.current_user_id
        }
        that.customAjax(that.url_move_folder, parameters, function(data){
            if (data.msg != false){
                $.bootstrapGrowl(data.msg, {
                    type: 'success',
                    align: 'right',
                    width: 300,
                    stackup_spacing: 30
                });
            }
            if (data.error != false) {
                $.bootstrapGrowl(data.error, {
                    type: 'danger',
                    align: 'right',
                    width: 300,
                    stackup_spacing: 30
                });
            }
        });
        that.showFolderContent(that.current_folder_id);
    }


    /**
     * Генерирует список папок польователя
     */
    this.loadUserFolders = function()
    {
        var parameters = {
            user_id: 1
        }
        var folders = false;
        that.customAjax(that.url_user_folder, parameters, function(data){
            folders = data;
        });

        function listUserFolders(my_array) {
            var folder_list = '';

            for (var i in my_array) {
                if (my_array[i]) {
                    var item = my_array[i];
                    folder_list += '<li id="' + item._id + '"><a href="#">' + item.name + '</a></li>'
                }
            }
            return folder_list;
        }

        var user_folders = listUserFolders(folders.data);

        return user_folders;
    }

    var copyTo = $('#appCopyTo'),
        moveTo = $('#appMoveTo');

    this.setUserFolders = function()
    {
        var user_folders = that.loadUserFolders();
        copyTo.empty();
        copyTo.append(user_folders);
        moveTo.empty();
        moveTo.append(user_folders);
    }


    this.setUserFoldersButton = function(id)
    {
        var user_folders = that.loadUserFolders();
        $('#'+id).empty();
        $('#'+id).append(user_folders);
    }


    this.subMenu = function()
    {
        /*sub menu*/
        // that.clearActions($(".show_manager"));
        var remove_folder = $('.remove_folder');
        var create_folder = $('.create_folder');
        var change_folder = $('.change_folder');
        var shadow_block = $('.shadow_block');
        //var change_btn = $('.change_btn');
        //var closer = $('.glyphicon.red');
        //var folder_name_input = $('.folder_name_input');
        //var FPosSelect = $('#changeFolderPos');
        //closer.off();
        //change_btn.off();
        $('.glyphicon-cog').on('click', function (event) {
            //alert($(this).attr('data'));
            that.folder_name = $(this).prev().text();//Имя папки с которой работаем
            that.folder_id = $(this).attr('data');
            event.stopPropagation();
            var data = $(this).attr('data');
            //alert($(this).position().left);
            //exit(1);
            var left = $(this).offset().left + 18;
            var top = $(this).offset().top + 18;
            var submenu = $('.sub_menu');
            var that_obj = $(this);
            if (submenu.css('display') == 'none') {
                $(this).removeClass("glyphicon-cog");
                $(this).addClass("glyphicon-collapse-down");
                submenu.css({"top": top, "left": left});
                submenu.removeClass('hide');
                submenu.show('fast');
                var actions = submenu.children();
                actions.each(function () {
                    $(this).attr('data', data)
                });
                // alert($(this).attr('data'))
            } else {
                if (that_obj.attr('class') == 'glyphicon glyphicon-collapse-down') {
                    that_obj.removeClass("glyphicon-collapse-down").addClass("glyphicon-cog");
                    submenu.hide('fast');
                } else {
                    $('.glyphicon-collapse-down').removeClass("glyphicon-collapse-down").addClass("glyphicon-cog");
                    submenu.hide('fast', function () {
                        that_obj.removeClass("glyphicon-cog");
                        that_obj.addClass("glyphicon-collapse-down");
                        submenu.css({"top": top, "left": left});
                        submenu.show('fast');
                        var actions = submenu.children();
                        actions.each(function () {
                            $(this).attr('data', data)
                        });
                    });
                }


            }
        });
        $(document).click(function (event) {
            if ($(event.target).closest(".sub_menu").length) return;
            $(".glyphicon-collapse-down").removeClass("glyphicon-collapse-down").addClass("glyphicon-cog");
            $(".sub_menu").hide();
            event.stopPropagation();
        });

        /*создать папку*/
        create_folder.off();
        create_folder.on('click', function () {
            var folder_id = $(this).attr('data');
            //alert(folder_id); exit(1);
            $(this).parent().hide('fast', function () {
                that.folderActions.create(folder_id);
            })
       });

        /*Удалить папку*/
        remove_folder.off();
        remove_folder.on('click', function () {
            var folder_id = $(this).attr('data');

            $(this).parent().hide('fast', function () {
                that.folderActions.remove(folder_id);
            })
        });

        /*Удалить папку*/
        change_folder.off();
        change_folder.on('click', function () {
            var folder_id = $(this).attr('data');
            //alert(that.folder_name);
            $(this).parent().hide('fast', function () {
                that.folderActions.change(folder_id,that.folder_name);
            })
        });

    }

}