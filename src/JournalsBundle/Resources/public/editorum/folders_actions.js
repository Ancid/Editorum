var Actions = function(folder)
{
    var that = this,
        folderNameConfirm = $('#foldername-modal'),
        folderNameInput = $('#foldername'),
        folderNameSave = folderNameConfirm.find('.btn-primary');

    that.error = function(){
        var alertError = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><p>Имя не введено</p></div>';
        if(folderNameConfirm.find('.alert-danger').length == 0){
            folderNameConfirm.find('.modal-body').append(alertError);
        }
    }

    this.create = function(folder_id)
    {
        folderNameInput.val('');
        folderNameConfirm.modal('show');
        folderNameSave.click(function(){
            var folderName = folderNameInput.val();
            if(folderName != null && folderName != '')
            {
                var parameters = {
                    'folder_id':folder_id,
                    'folder_name':folderName,
                    'user_id':folder.current_user_id
                };
                //folder.customAjax(folder.url_check_folder, parameters, function(data){
                folder.customAjax(folder.url_create_folder, parameters, function(data){
                    if (data.error != false) {
                        alert(data.error);
                    } else {
                        folder.showFolders();
                        folder.treeInit();
                        folder.subMenu();
                    }
                });
            }else{
                that.error();
                return false;
            }
        });
    };
    this.remove = function(folder_id)
    {
        $('#removge-folder-modal').modal('show').find('.btn-primary').click(function(){
            var parameters = {
                'folder_id':folder_id
            };
            folder.customAjax(folder.url_remove_folder, parameters, function(data){
                if (data.error != false) alert(data.error);
            });
            folder.showFolders();
            folder.treeInit();
            folder.subMenu();
        });
    };

    this.change = function(folder_id,folder_name)
    {
        folderNameInput.val(folder_name);
        folderNameConfirm.modal('show');

        folderNameSave.click(function(){
            var folderName = folderNameInput.val();
            if(folderName != null && folderName != '')
            {
                var parameters = {
                    'folder_id':folder_id,
                    'folder_name':folderName
                };
                folder.customAjax(folder.url_change_folder, parameters, function(data){
                    console.log(data);
                });
                folder.showFolders();
                folder.treeInit();
                folder.subMenu();
            }else{
                that.error();
                return false;
            }
        });
    };

    folderNameConfirm.on('hidden.bs.modal', function(){
        folderNameConfirm.find('.alert-danger').remove();
    });

    //this.sendInvite = function(folder_id,folder_name,emails)
    //{
    //       // console.log(emails);
    //       // znanium.db.sendInvite(folder_id,folder_name,emails);
    //        console.log(znanium.db.sendInvite(folder_id,folder_name,emails));
    //        alert('Приглашение отправлено')
    //        znanium.db.showFolders();
    //        znanium.setHelpers();
    //        znanium.treeInit();
    //};
    //
    //this.optionTree = function(data){
    //
    //}
}