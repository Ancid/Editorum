/**
 * Created by sidorov_ar on 23.10.2015.
 */

var Data = function()
{
    //var that = this;

    this.save = function(paramName, value)
    {
        localStorage.setItem(paramName, value);
    }


    this.load = function(paramName)
    {
        return localStorage.getItem(paramName);
    }

    this.clear = function()
    {
        localStorage.clear();
    }


    /**
     * Проверка поддержки localstorage
     * @returns {boolean}
     */
    this.check = function() {
        try {
            return 'localStorage' in window && window['localStorage'] !== null;
        } catch (e) {
            return false;
        }
    }
}