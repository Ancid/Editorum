/**
 * Created by sidorov_ar on 26.10.2015.
 */
var config = {
    serviceUrl: '/ajaxsearch', // Страница для обработки запросов автозаполнения
    minChars: 3, // Минимальная длина запроса для срабатывания автозаполнения
    maxHeight: 400, // Максимальная высота списка подсказок, в пикселях
    width: 522, // Ширина списка
    zIndex: 9999, // z-index списка
    deferRequestBy: 100, // Задержка запроса (мсек), на случай, если мы не хотим слать миллион запросов, пока пользователь печатает.
    groupBy: 'category',
    type: 'POST',
//                    params: param,
    onSelect: function (suggestion) {
        console.log(suggestion);
        if (suggestion.kind == 101) {
            //Статья
            window.location.href = '/nauka/publication/' + suggestion.id + '/';
        } else if (suggestion.kind == 102) {
            //Монография
            window.location.href = '/nauka/publication/' + suggestion.id + '/';
        } else if (suggestion.kind == 199) {
            //ISBN произведения
            window.location.href = '/nauka/publication/' + suggestion.id + '/';
        } else if (suggestion.kind == 200) {
            //Журнал
            window.location.href = '/nauka/' + suggestion.id + '/';
        } else if (suggestion.kind == 299) {
            //ISSN журнала
            window.location.href = '/nauka/' + suggestion.id + '/';
        } else if (suggestion.kind == 300) {
            //Автор
            window.location.href = '/nauka/author/' + suggestion.id + '/';
        } else if (suggestion.kind == 400) {
            //GRNTI
            //window.location.href = '/author/edit/' + (suggestion.id) + '/';
        } else if (suggestion.kind == 450) {
            //OKSO_J
            //window.location.href = '/author/edit/' + (suggestion.id) + '/';
        }
    }
};
$('#search_input').autocomplete(config);