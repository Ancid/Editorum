<?php
namespace JournalsBundle\Process;

use Sylius\Bundle\FlowBundle\Process\Context\ProcessContextInterface;
use Sylius\Bundle\FlowBundle\Process\Step\AbstractControllerStep;

abstract class AbstractStep extends AbstractControllerStep
{
    /**
     * @return \Doctrine\ODM\MongoDB\DocumentManager|object
     */
    public function getODM()
    {
        return $this->get('doctrine.odm.mongodb.document_manager');
    }

    /**
     * @param ProcessContextInterface $context
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectToCurrentStep(ProcessContextInterface  $context)
    {
        return $this->redirectToRoute('sylius_flow_display', [
            'scenarioAlias' => $context->getProcess()->getScenarioAlias(),
            'stepName'      => $context->getCurrentStep()->getName(),
        ]);
    }

    /**
     * @return null|\Symfony\Component\HttpFoundation\Request
     */
    public function getRequest()
    {
        return $this->get('request_stack')->getCurrentRequest();
    }
}
