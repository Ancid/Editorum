<?php
namespace JournalsBundle\Process;

use JournalsBundle\Process\Step\AuthorStep;
use JournalsBundle\Process\Step\ConferenceArticleStep;
use JournalsBundle\Process\Step\ConferenceStep;
use JournalsBundle\Process\Step\DegreeStep;
use JournalsBundle\Process\Step\JobStep;
use Sylius\Bundle\FlowBundle\Process\Builder\ProcessBuilderInterface;
use Sylius\Bundle\FlowBundle\Process\Scenario\ProcessScenarioInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class RequestScenario
 *
 * @package JournalsBundle\Process
 */
class RequestScenario implements ProcessScenarioInterface
{
    use ContainerAwareTrait;
    /**
     * Builds the whole process.
     *
     * @param ProcessBuilderInterface $builder
     */
    public function build(ProcessBuilderInterface $builder)
    {
        $builder
            ->add('conference_article', new ConferenceArticleStep())
            ->add('author', new AuthorStep())
            ->add('job_info', new JobStep())
            ->add('degree_info', new DegreeStep())
            ->add('conference', new ConferenceStep())
        ;
    }
}
