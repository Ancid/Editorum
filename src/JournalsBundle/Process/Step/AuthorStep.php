<?php
namespace JournalsBundle\Process\Step;

use Editorum\Bundle\Document\Author;
use Editorum\Bundle\Document\User;
use JournalsBundle\Form\Type\AuthorStepType;
use JournalsBundle\Process\AbstractStep;
use Sylius\Bundle\FlowBundle\Process\Context\ProcessContextInterface;
use Sylius\Bundle\FlowBundle\Process\Step\ActionResult;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AuthorStep
 *
 * @package JournalsBundle\Process\Step
 */
class AuthorStep extends AbstractStep
{
    /**
     * Display action.
     *
     * @param ProcessContextInterface $context
     *
     * @return ActionResult|Response
     */
    public function displayAction(ProcessContextInterface $context)
    {
        /** @var User $user */
        $user = $this->getUser();

        $author = $user->getAuthor();

        $form = $this->createForm(new AuthorStepType(), $author);
        return $this->render('JournalsBundle:Process/Step:author.html.twig', [
            'step_context' => $context,
            'form'         => $form->createView(),
        ]);
    }

    public function forwardAction(ProcessContextInterface $context)
    {
        /** @var User $user */
        $user = $this->getUser();
        $request = $this->get('request_stack')->getCurrentRequest();
        $author = null == $user->getAuthor() ? new Author() : $user->getAuthor();
        $form = $this->createForm(new AuthorStepType(), $author);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->getUser();
            $author->setUser($user);
            $this->getODM()->persist($author);

            $user->setAuthor($author);

            $this->getODM()->persist($user);
            $this->getODM()->flush();

            $context->getStorage()->set('Author', $author);

            return $this->complete();
        }

        return $this->render('JournalsBundle:Process/Step:author.html.twig', [
            'step_context' => $context,
            'form'         => $form->createView(),
        ]);
    }
}
