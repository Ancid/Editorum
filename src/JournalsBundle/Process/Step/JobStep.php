<?php
namespace JournalsBundle\Process\Step;

use Editorum\Bundle\Document\Author;
use Editorum\Bundle\Document\Author\JobList;
use Editorum\Bundle\Document\Author\JobPlace;
use JournalsBundle\Form\Type\JobStepType;
use JournalsBundle\Form\Type\StepType;
use JournalsBundle\Process\AbstractStep;
use Sylius\Bundle\FlowBundle\Process\Context\ProcessContextInterface;
use Sylius\Bundle\FlowBundle\Process\Step\ActionResult;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class JobStep extends AbstractStep
{
    /**
     * Display action.
     *
     * @param ProcessContextInterface $context
     *
     * @return ActionResult|Response
     */
    public function displayAction(ProcessContextInterface $context)
    {
        $request = $this->getRequest();
        $form = $this->createForm(new JobStepType());
        $stepForm = $this->createForm(new StepType());

        /** @var Author $author */
        $author = $this->getUser()->getAuthor();
        $jobs = $author->getWorkplaces()->toArray();

        $form->handleRequest($request);
        if ($form->isValid()) {
            $data = $form->getData();
            $company = null;

            if ((bool)$data['is_exists']) {
                $company = $data['corporate'];

                if ((bool)$data['is_vuz']) {
                    $company = $data['vuz'];
                }
            } else {
                $company = new JobPlace();
                $company
                    ->setRu(['title' => $data['hand']['title']])
                    ->setEn(['title' => null])
                    ->setCountry($data['hand']['country'])
                    ->setCity($data['hand']['city']);

                $this->getODM()->persist($company);
            }

            $entry = new JobList();
            $entry
                ->setPlace($company)
                ->setDepartment($data['department'])
                ->setPosition($data['position']);

            $this->getODM()->persist($entry);
            $author->addWorkplace($entry);
            $this->getODM()->persist($author);
            $this->getODM()->flush();

            $context->getStorage()->set('Author', $author);

            return $this->redirectToCurrentStep($context);
        }

        return $this->render('JournalsBundle:Process/Step:job.html.twig', [
            'step_context'   => $context,
            'job_form'       => $form->createView(),
            'form'           => $stepForm->createView(),
            'jobs'           => $jobs
        ]);
    }

    /**
     * @inheritDoc
     */
    public function forwardAction(ProcessContextInterface $context)
    {
        return $this->complete();
    }
}
