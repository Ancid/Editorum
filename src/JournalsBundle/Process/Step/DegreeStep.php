<?php
namespace JournalsBundle\Process\Step;

use Editorum\Bundle\Document\Author;
use Editorum\Bundle\Document\Author\DegreeList;
use JournalsBundle\Form\Type\DegreeStepType;
use JournalsBundle\Form\Type\StepType;
use JournalsBundle\Process\AbstractStep;
use Sylius\Bundle\FlowBundle\Process\Context\ProcessContextInterface;
use Sylius\Bundle\FlowBundle\Process\Step\ActionResult;
use Symfony\Component\HttpFoundation\Response;

class DegreeStep extends AbstractStep
{
    /**
     * Display action.
     *
     * @param ProcessContextInterface $context
     *
     * @return ActionResult|Response
     */
    public function displayAction(ProcessContextInterface $context)
    {
        $request = $this->getRequest();

        /** @var Author $author */
        $author = $this->getUser()->getAuthor();

        /** @var DegreeList[] $degreeList */
        $degreeList = $author->getDegreeList();

        // формы
        $stepForm = $this->createForm(new StepType());
        $degreeForm = $this->createForm(new DegreeStepType());

        $degreeForm->handleRequest($request);
        if ($degreeForm->isSubmitted() && $degreeForm->isValid()) {
            $degree = $degreeForm->getData();
            $this->getODM()->persist($degree);
            $author->addDegreeList($degree);

            $this->getODM()->persist($author);
            $this->getODM()->flush();
            return $this->redirectToCurrentStep($context);
        }

        return $this->render('JournalsBundle:Process/Step:degree.html.twig', [
            'step_context'  => $context,
            'form'          => $stepForm->createView(),
            'degree_form'   => $degreeForm->createView(),
            'degree_list'   => $degreeList,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function forwardAction(ProcessContextInterface $context)
    {
        return parent::forwardAction($context);
    }
}
