<?php
namespace JournalsBundle\Process\Step;

use Editorum\Bundle\Document\Common\AbstractPublication;
use Editorum\Bundle\Document\ConferenceArticle;
use Editorum\Bundle\Document\Request;
use JournalsBundle\Form\Type\ConferenceStepType;
use JournalsBundle\Process\AbstractStep;
use Sylius\Bundle\FlowBundle\Process\Context\ProcessContextInterface;
use Sylius\Bundle\FlowBundle\Process\Step\ActionResult;
use Symfony\Component\HttpFoundation\Response;

class ConferenceStep extends AbstractStep
{
    /**
     * Display action.
     *
     * @param ProcessContextInterface $context
     *
     * @return ActionResult|Response
     */
    public function displayAction(ProcessContextInterface $context)
    {

        $form = $this->createForm(new ConferenceStepType());

        return $this->render('JournalsBundle:Process/Step:conference.html.twig', [
            'step_context'  => $context,
            'form'          => $form->createView(),
        ]);
    }

    /**
     * @inheritDoc
     */
    public function forwardAction(ProcessContextInterface $context)
    {
        $request = $this->getRequest();
        $storage = $context->getStorage();
        $form = $this->createForm(new ConferenceStepType());
        $form->handleRequest($request);

        $response = $this->render('JournalsBundle:Base/RequestStep:finish.html.twig');

        if ($form->isSubmitted()) {
            if (!$form->isValid()) {
                $response = $this->redirectToCurrentStep($context);
                $this->addFlash('error', 'Для завершения выберите конференцию!');

                goto response;
            }

            /** @var ConferenceArticle $publication */
            $publication = $storage->get('ConferenceArticle');
            $publication->addAuthor($this->getUser()->getAuthor());
            $publication
                ->setSection($form->getData()['section'])
                ->setStatus('status', AbstractPublication::ST_IN_REQUEST);

            $request = new Request();
            $request
                ->setStatus(Request::STATUS_NEW)
                ->setPublication($publication)
                ->setPublisher($form->getData()['active_conference']->getPublisher())
                ->setReference($form->getData()['active_conference'])
                ->setUser($this->getUser());
            $this->getODM()->persist($publication);
            $this->getODM()->persist($request);

            $this->getODM()->flush();
            $storage->clear();
        }

        goto response;

        response:
            return $response;
    }
}
