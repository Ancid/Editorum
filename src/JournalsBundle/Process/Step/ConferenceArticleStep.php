<?php
namespace JournalsBundle\Process\Step;

use Editorum\Bundle\Document\ConferenceArticle;
use Editorum\Bundle\Document\FileStorage;
use JournalsBundle\Form\Type\ArticleConferenceStepType;
use JournalsBundle\Process\AbstractStep;
use Sylius\Bundle\FlowBundle\Process\Context\ProcessContextInterface;
use Sylius\Bundle\FlowBundle\Process\Step\ActionResult;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;

class ConferenceArticleStep extends AbstractStep
{
    /**
     * Display action.
     *
     * @param ProcessContextInterface $context
     *
     * @return ActionResult|Response
     */
    public function displayAction(ProcessContextInterface $context)
    {
        $conferenceArticle = $context->getStorage()->get('ConferenceArticle');

        if ($conferenceArticle != null) {
            $this->get('filesystem')->remove($conferenceArticle->getDepofile());
            $conferenceArticle->setDepofile(null);
        }

        $form = $this->createForm(new ArticleConferenceStepType(), $conferenceArticle);


        return $this->render('JournalsBundle:Process/Step:article_conference.html.twig', [
            'step_context' => $context,
            'form'         => $form->createView(),
        ]);
    }

    /**
     * Forward action
     *
     * @param ProcessContextInterface $context
     *
     * @return ActionResult
     */
    public function forwardAction(ProcessContextInterface $context)
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        $conferenceArticle = new ConferenceArticle();
        $form = $this->createForm(new ArticleConferenceStepType(), $conferenceArticle);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $depofile = $conferenceArticle->getDepofile();
            if ($depofile instanceof UploadedFile) {
                $filename = 'upload' . DIRECTORY_SEPARATOR;

                if (in_array($depofile->getClientOriginalExtension(), [
                    'odt', 'rtf', 'pdf', 'doc', 'docx'
                ])) {
                    $depofile->move($filename, $depofile->getClientOriginalName());
                    $conferenceArticle->setDepofile($filename . $depofile->getClientOriginalName());
                } else {
                    $this->addFlash('error', 'Неверный формат депозитного файла!');

                    return $this->render('JournalsBundle:Process/Step:article_conference.html.twig', [
                        'step_context' => $context,
                        'form'         => $form->createView(),
                    ]);
                }
            }

            $context->getStorage()->set('ConferenceArticle', $conferenceArticle);

//            $this->getODM()->persist($conferenceArticle);
//            $this->getODM()->flush();

            return $this->complete();
        }

        return $this->render('JournalsBundle:Process/Step:article_conference.html.twig', [
            'step_context' => $context,
            'form'         => $form->createView(),
        ]);
    }
}
