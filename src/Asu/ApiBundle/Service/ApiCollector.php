<?php
namespace Asu\ApiBundle\Service;

class ApiCollector
{
    private $ids = [];

    public function addId($id, $alias = null)
    {
        $this->ids[$alias] = $id;

        return $this;
    }

    public function getId($alias)
    {
        if (!isset($this->ids[$alias])) {
            return null;
        }

        return $this->ids[$alias];
    }
}