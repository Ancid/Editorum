<?php
namespace Asu\ApiBundle\Service;

use Doctrine\ODM\MongoDB\DocumentManager;
use JMS\Serializer\Context;
use JMS\Serializer\JsonDeserializationVisitor;
use JMS\Serializer\JsonSerializationVisitor;

final class RelationsHandler
{
    /**
     * @var DocumentManager
     */
    private $manager;

    /**
     * RelationsHandler constructor.
     *
     * @param DocumentManager $manager
     */
    public function __construct(DocumentManager $manager)
    {
        $this->manager = $manager;
    }


    public function serializeRelation(JsonSerializationVisitor $visitor, $relation, array $type, Context $context)
    {
        if ($relation instanceof \Traversable) {
            $relation = iterator_to_array($relation);
        }

        if (is_array($relation)) {
            return array_map([$this, 'getSingleEntityRelation'], $relation);
        }

        return $this->getSingleDocumentRelation($relation);
    }

    /**
     * @param $relation
     *
     * @return array|mixed
     */
    protected function getSingleDocumentRelation($relation)
    {
        $metadata = $this->manager->getMetadataFactory()->getMetadataFor(get_class($relation));
        $ids = $metadata->getIdentifierValues($relation);

        return $ids;
    }

    public function deserializeRelation(JsonDeserializationVisitor $visitor, $relation, array $type, Context $context)
    {
        $className = isset($type['params'][0]['name']) ? $type['params'][0]['name'] : null;

        if (!class_exists($className, false)) {
            throw new \InvalidArgumentException('Class name should be explicitly set for deserialization');
        }

        if (!is_array($relation)) {
            return $this->manager->getRepository($className)->find($relation);
        }

        $objects = [];
        foreach ($relation as $idSet) {
            $objects[] = $this->manager->getRepository($className)->find($idSet);
        }

        return $objects;
    }
}
