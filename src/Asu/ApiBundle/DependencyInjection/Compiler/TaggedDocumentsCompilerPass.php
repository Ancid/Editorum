<?php
namespace Asu\ApiBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class TaggedDocumentsCompilerPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $collector = $container->getDefinition('asu_api.api.collector');
        foreach ($container->findTaggedServiceIds('asu.api') as $id => $tag) {
            $collector->addMethodCall('addId', [ $container->getDefinition($id)->getClass(), $tag[0]['alias'] ]);
            $container->removeDefinition($id);
        }
    }
}