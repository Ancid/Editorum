<?php
namespace Asu\ApiBundle\Subscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class ExceptionSubscriber
 * @package Asu\ApiBundle\Subscriber
 */
class ApiSubscriber implements EventSubscriberInterface
{
    /** @var LoggerInterface */
    private $logger;
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /**
     * ExceptionSubscriber constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger, TokenStorageInterface $tokenStorage)
    {
        $this->logger = $logger;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
            KernelEvents::REQUEST   => 'onKernelRequest'
        ];
    }

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $e = $event->getException();
        $request = $event->getRequest();

        $uri = $request->getRequestUri();
        if (!preg_match('/\/api(\/)+/', $uri)) {
            return;
        }

        $message = [
            'success' => false,
            'message' => $e->getMessage(),
        ];

        if ($e->getCode() !== 0) {
            $message['code'] = $e->getCode();
        }

        $response = new JsonResponse($message);
        $response->headers->set('Content-Type', 'application/problem+json');

        $context = [
            'ip'            => $request->getClientIp(),
            'query'         => $request->query->all(),
            'request'       => $request->request->all(),
            'content'       => $request->getContent(),
            'files'         => $request->files->all(),
            'attributes'    => $request->attributes->all(),
            'method'        => $request->getMethod(),
            'referef'       => $request->headers->get('referer'),
            'file'          => $e->getFile(),
            'line'          => $e->getLine(),
        ];

        $token = $this->tokenStorage->getToken();
        if ($token && $token->getUser() instanceof UserInterface) {
            $context = [ 'user' => $token->getUser()->getEmail() ] + $context;
        }

        $this->logger->error($e->getMessage(), $context);
        $event->setResponse($response);
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        $uri = $request->getRequestUri();
        if (!preg_match('/\/api(\/)+/', $uri)) {
            return;
        }

        $context = [
            'ip'         => $request->getClientIp(),
            'uri'        => $uri,
            'query'      => $request->query->all(),
            'request'    => $request->request->all(),
            'content'    => $request->getContent(),
            'files'      => $request->files->all(),
            'attributes' => $request->attributes->all(),
            'method'     => $request->getMethod()
        ];

        $token = $this->tokenStorage->getToken();
        if ($token && $token->getUser() instanceof UserInterface) {
            $context = ['user' => $token->getUser()->getEmail()] + $context;
        }

        $this->logger->info('Success request', $context);
    }
}
