<?php

namespace Asu\ApiBundle;

use Asu\ApiBundle\DependencyInjection\Compiler\TaggedDocumentsCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AsuApiBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new TaggedDocumentsCompilerPass());
        parent::build($container);
    }
}
