<?php
namespace Asu\ApiBundle\Controller;

use Doctrine\ODM\MongoDB\DocumentNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Class EditableController
 * @package Asu\ApiBundle\Controller
 */
class EditableController extends Controller
{
    /**
     * @param $alias
     * @param Request $request
     * @return Response
     * @throws DocumentNotFoundException
     * @throws \Doctrine\ODM\MongoDB\LockException
     * @throws \Throwable
     * @throws \TypeError
     */
    public function fieldUpdateAction($alias, Request $request)
    {
        if ($request->headers->get('Content-Type') != 'application/json') {
            throw new BadRequestHttpException('Bad request content-type!');
        }

        $class = $this->get('asu_api.api.collector')->getId($alias);

        if ($class === null) {
            throw new ServiceNotFoundException($alias);
        }

        $odm = $this->get('doctrine.odm.mongodb.document_manager');

        $json = $request->getContent();
        $data = json_decode($json, 1);

        $document = $odm->getRepository($class)->find((int) $data['pk']);

        if (null === $document) {
            throw new DocumentNotFoundException();
        }

        $value = $data['value'];

        $properties = explode('.', $data['name']);
        $mainProperty = $properties[0];

        $path = implode('.', $properties);
        $data = null;
        $this->get('asu_api.path.helper')->assignArrayByPath($data, $path, $value);

        $propertyAccessor = new PropertyAccessor();

        $setted = $propertyAccessor->getValue($document, $mainProperty);
        foreach ($data[$mainProperty] as $key => $value) {
            if (isset($setted[$key])) {
                $setted[$key] = $value;
            }
        }

        $propertyAccessor->setValue($document, $mainProperty, $setted);

        $odm->flush($document);

        $result = [
            'success'   => true,
            'data'      => $document
        ];

        $response = new Response(
            $this->get('jms_serializer')->serialize($result, 'json')
        );

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
