<?php

namespace Asu\ApiBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 * @package Asu\ApiBundle\Controller
 * @Security("is_granted('ROLE_USER')")
 */
class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        /** @var Definition $definition */
//        $class = $this->get('asu_api.api.collector')->getId($request->get('alias'));
//        $repository = $this->get('doctrine.odm.mongodb.document_manager')->getRepository($class)->find(1);
        return new JsonResponse(['Hello!']);
    }
}
