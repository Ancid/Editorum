<?php
namespace Asu\ApiBundle\Controller;

use Asu\ApiBundle\Exception\ValidationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ApiGeneratorController extends Controller
{
    /**
     * @param $alias
     * @param Request $request
     * @return Response
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function getAction($alias, Request $request)
    {
        $class = $this->get('asu_api.api.collector')->getId($alias);

        if ($class === null) {
            throw new NotFoundHttpException('Api for ' . $alias . ' does not exists!');
        }

        $query = $this->get('doctrine.odm.mongodb.document_manager')
            ->getRepository($class)->createQueryBuilder()
            ->getQuery();

        $query->setHydrate(true);
        $data = $query->execute();

        $result = [
            'success' => true,
            'items'   => [],
            'uri'     => $this->get('router')->generate(
                $request->attributes->get('_route'),
                $request->attributes->get('_route_params'),
                Router::ABSOLUTE_URL
            )
        ];

        foreach ($data as $item) {
            $result['items'][] = [
                'data'  => $item,
                'uri'   => $this->get('router')->generate('asu_api_generated_one', [
                    'alias' => $alias,
                    'id'    => $item->getId()
                ], Router::ABSOLUTE_URL)
            ];
        }

        $serializer = $this->get('jms_serializer');

        $response = new Response($serializer->serialize($result, 'json'));
        $response->headers->add([
            'Content-type' => 'application/json'
        ]);
        return $response;
    }

    /**
     * @param $alias
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function getOneAction($alias, $id, Request $request)
    {
        $class = $this->get('asu_api.api.collector')->getId($alias);

        if ($class === null) {
            throw new NotFoundHttpException('Api for '.$alias.' does not exists!');
        }

        $query = $this->get('doctrine.odm.mongodb.document_manager')
            ->getRepository($class)->createQueryBuilder()
            ->field('_id')->equals($id)
            ->getQuery();

        $query->setHydrate(true);
        $data = $query->getSingleResult();

        if ($data === null) {
            throw new NotFoundHttpException(
                sprintf('Object %s in api "%s" not found!', $id, $alias)
            );
        }

        $result = [
            'success'   => true,
            'data'      => $data,
            'uri'       => $this->get('router')->generate(
                $request->attributes->get('_route'),
                $request->attributes->get('_route_params'),
                Router::ABSOLUTE_URL
            )
        ];


        $serializer = $this->get('jms_serializer');

        $response = new Response($serializer->serialize($result, 'json'));
        $response->headers->add([
            'Content-type'  => 'application/json'
        ]);
        return $response;
    }

    /**
     * @param $alias
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function updateAction($alias, $id, Request $request)
    {
        if ($request->headers->get('Content-Type') != 'application/json') {
            throw new BadRequestHttpException('Bad request content-type!');
        }

        $class = $this->get('asu_api.api.collector')->getId($alias);

        if ($class === null) {
            throw new ServiceNotFoundException($alias);
        }

        $odm = $this->get('doctrine.odm.mongodb.document_manager');

        $json = $request->getContent();
        $array = json_decode($json, 1);
        $json = json_encode(array_merge_recursive(["id" => $id], $array));

        $serializer = $this->get('jms_serializer');
        $document = $serializer->deserialize($json, $class, 'json');

        $violations = $this->get('validator')->validate($document);

        if (count($violations) != 0) {
            $errors = [];

            foreach ($violations as $violation) {
                $errors[$violation->getPropertyPath()] = $violation->getMessage();
            }

            throw new ValidationException($errors);
        }

        $odm->persist($document);
        $odm->flush();

        $result = [
            'success' => true,
            'data' => $document,
            'uri' => $this->get('router')->generate('asu_api_generated_one', [
                'alias' => $alias,
                'id' => $document->getId()
            ], Router::ABSOLUTE_URL),
        ];

        $response = new Response($serializer->serialize($result, 'json'));
        $response->headers->add([
            'Content-type' => 'application/json'
        ]);
        return $response;
    }

    public function createAction($alias, Request $request)
    {
        if ($request->headers->get('Content-Type') != 'application/json') {
            throw new BadRequestHttpException('Bad request content-type!');
        }

        $class = $this->get('asu_api.api.collector')->getId($alias);

        if ($class === null) {
            throw new ServiceNotFoundException($alias);
        }

        $odm = $this->get('doctrine.odm.mongodb.document_manager');

        $json = $request->getContent();

        $serializer = $this->get('jms_serializer');

        $document = $serializer->deserialize($json, $class, 'json');

        $violations = $this->get('validator')->validate($document);

        if (count($violations) != 0) {
            $errors = [];

            foreach ($violations as $violation) {
                $errors[$violation->getPropertyPath()] = $violation->getMessage();
            }

            throw new ValidationException($errors);
        }

        $document = $odm->merge($document);
        $odm->flush();

        $result = [
            'success' => true,
            'data'    => $document,
            'uri'     => $this->get('router')->generate('asu_api_generated_one', [
                'alias' => $alias,
                'id' => $document->getId()
            ], Router::ABSOLUTE_URL),
        ];

        $response = new Response($serializer->serialize($result, 'json'));
        $response->headers->add([
            'Content-type' => 'application/json'
        ]);
        return $response;
    }

    public function removeAction($alias, $id)
    {
        $class = $this->get('asu_api.api.collector')->getId($alias);

        if ($class === null) {
            throw new ServiceNotFoundException($alias);
        }

        $odm = $this->get('doctrine.odm.mongodb.document_manager');

        $document = $odm->getRepository($class)->find($id);

        if ($document !== null) {
            $odm->remove($document);
            $odm->flush();
        }

        $result = [
            'success' => true,
            'uri' => $this->get('router')->generate('asu_api_generated_many', [
                'alias' => $alias
            ], Router::ABSOLUTE_URL),
        ];

        $serializer = $this->get('jms_serializer');
        $response = new Response($serializer->serialize($result, 'json'));
        $response->headers->add([
            'Content-type' => 'application/json'
        ]);

        return $response;
    }
}
