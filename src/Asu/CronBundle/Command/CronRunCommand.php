<?php

namespace Asu\CronBundle\Command;

use Asu\CronBundle\Annotation\CronTask;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CronRunCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('asu_cron:run')
            ->setDescription('Runs any currently schedule cron jobs');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $reader = $this->getContainer()->get('annotation_reader');

        $progress = new ProgressBar($output);
        $progress->start();
        $progress->setFormat('%message% %current%/%max% [%bar%] %elapsed:6s% %memory:6s%' . "\n");
        $progress->setMessage('Start executing commands...');
        $count = 0;
        foreach ($this->getApplication()->all() as $command) {
            $reflClass = new \ReflectionClass($command);
            foreach ($reader->getClassAnnotations($reflClass) as $task) {
                if ($task instanceof CronTask) {
                    if ($task->enabled) {
                        $progress->setMessage(
                            sprintf('Trying to run command "%s"', $command->getName())
                        );
                        $execCommand = $this->getApplication()->find($command->getName());
                        $returnCode = $execCommand->run($input, $output);

                        if ($returnCode !== 0) {
                            --$count;
                            $this->getContainer()->get('logger')->error(
                                sprintf('Unable to run "%s" command by cron', $command->getName())
                            );
                        }

                        $this->getContainer()->get('logger')->info(
                            sprintf('Succesfully executed "%s" command by cron', $command->getName())
                        );
                        $progress->setMessage(
                            sprintf('Command "%s" successfully executed', $command->getName())
                        );
                        ++$count;
                        $progress->advance();
                    }
                }
            }
        }

        $progress->finish();

        $output->writeln('Totaly Executed ' . $count . ' commands');
    }
}
