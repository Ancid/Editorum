<?php
namespace Asu\CronBundle\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation()
 */
class CronTask extends Annotation
{
    /**
     * @var bool
     */
    public $enabled = true;
}
