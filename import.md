### Импорт данных из старой системы.

*При импорте данных на облачном сервере пункты 1-4 пропустить.*

1. Данные вытягиваются из базы старого АСУ [http://asu.infra-m.ru/asu/](http://asu.infra-m.ru/asu/). Дамп доступен по ссылке: [скачать]
(http://redmine.infra-m.ru:3000/attachments/download/553/asudump%2002%2016%202016.rar).
2. Необходимо скачать и развернуть дамп локально.
3. Далее необходимо прописать настройки подключения к бд в файле parameters.yml, который находится в директории 
проекта *app/config*.
4. Добавить директорию *web/jsons_asu/** в .gitignore (создавать директорию не надо).
5. Прописать в браузере *%your_host%/makejson* и запустить. Выполняется ~2мин.( Скрипт сам создаст директорию и 
положит в неё сгенерированные json-файлы коллекций.
6. Если всё прошло успешно, то запускаем *%your_host%/import/rubricators*. Скрипт запишет в Mongo классификаторы 
произведений, сами json-файлы классификаторов лежат в директории *web/rubricator*.
7. Затем в строгой последовательности запускать:
	* *%your_host%/custom_import/authors/*
	* *%your_host%/custom_import/pubs/*
	* *%your_host%/custom_import/journals/*
	* *%your_host%/custom_import/conferences/*
	* *%your_host%/custom_import/chapters/*
	* *%your_host%/custom_import/references/*
	* *%your_host%/custom_import/rubrics/*
	* *%your_host%/custom_import/publishers/*
	* *%your_host%/custom_import/countries/*
	* *%your_host%/custom_import/regions/*
	* *%your_host%/custom_import/universities/*
	* *%your_host%/custom_import/collections/*

Во избежании ошибок импорта, просьба не менять типы данных, т. к. в некоторых из 
них заложена логика соблюдения целостности данных. 
